﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="dashboard.aspx.cs" Inherits="dashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="UpdDashboard" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Panel ID="PnlDashboard" runat="server"></asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>