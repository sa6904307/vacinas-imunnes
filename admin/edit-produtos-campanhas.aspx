﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="edit-produtos-campanhas.aspx.cs" Inherits="edit_produtos_campanhas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js" type="text/javascript"></script>
    <link href="../vendors/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <script src="../vendors/global/jquery.min.js"></script>
    <script type="text/javascript" src="../vendors/select2/js/select2.full.min.js"></script>
    <script type="text/javascript">
        var select$ = jQuery.noConflict();
        select$(function () {
            //Initialize Select2 Elements
            select$('.select2').select2()
        })
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row">
        <div class="col-md-12">
            <ul class="breadcrumb">
                <li><a href="dashboard.aspx"><i class="fa fa-home"></i>&nbsp;Home</a></li>
                <li>
                    <asp:HyperLink ID="LnkCampanhas" runat="server" Text="Campanhas"></asp:HyperLink>
                </li>
                <li>
                    <asp:HyperLink ID="LnkProdutos" runat="server" Text="Produtos - Campanhas"></asp:HyperLink>
                </li>
                <li class="active">Inserção/Edição</li>
            </ul>
        </div>
    </div>
    <h3><i class="fa fa-medkit"></i>&nbsp;<asp:Label ID="LblForms" runat="server"></asp:Label></h3>
    <asp:UpdatePanel ID="UpdMain" runat="server">
        <ContentTemplate>
            <script type="text/javascript">
                Sys.Application.add_load(SetSelectBox);
                var select$ = jQuery.noConflict();
                function SetSelectBox() {
                    select$('.select2').select2();
                }
            </script>
            <div class="box box-success">
                <div class="box-body">
                    <div class="row">
                        <asp:Panel ID="PnlPesquisa" runat="server" DefaultButton="BtnPesquisar">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                <label>Campanha *</label>
                                <asp:DropDownList ID="DdlCampanhas" runat="server" CssClass="form-control text-uppercase select2" DataValueField="ValueMember" DataTextField="DisplayMember" Enabled="false"></asp:DropDownList>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                <asp:TextBox ID="TxtPesquisa" runat="server" CssClass="form-control" placeHolder="PESQUISAR..."></asp:TextBox>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 pull-right">
                                <asp:LinkButton ID="BtnPesquisar" runat="server" CssClass="btn btn-block btn-success" data-toggle="tooltip" data-container="body" ToolTip="Pesquisar registros" CausesValidation="false" OnClick="BtnPesquisar_Click">
                                        <i class="fa fa-search"></i>&nbsp;Pesquisar
                                </asp:LinkButton>
                            </div>
                        </asp:Panel>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 box-resultados text-right conteudo-centro">
                                    <asp:Label ID="LblRegistros" runat="server"></asp:Label>
                                </div>
                            </div>
                            <div class="dv-spc-10"></div>
                            <div class="box-grids">
                                <div class="dv-grid-f-header">
                                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="display: inline-block; float: none;">
                                        Ações
                                    </div>
                                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1" style="display: inline-block; float: none;">
                                        Preço
                                    </div>
                                    <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10" style="display: inline-block; float: none;">
                                        Vacinas
                                    </div>
                                </div>
                                <div class="dv-grid-f">
                                    <asp:Repeater ID="RptPrincipal" runat="server" OnItemDataBound="RptPrincipal_ItemDataBound">
                                        <ItemTemplate>
                                            <div id="dvItem" runat="server">
                                                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-left" style="display: inline-block; float: none;">
                                                    <div style="margin-top: -45px; position: absolute;">
                                                        <asp:CheckBox ID="ChkSelecionar" runat="server" CssClass="checkbox" Text="SELECIONAR" />
                                                    </div>
                                                </div>
                                                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1" style="display: inline-block; float: none;">
                                                    <div style="margin-top: -35px; position: absolute;">
                                                        <asp:TextBox ID="TxtPreco" runat="server" Text="0,00" CssClass="form-control text-right" MaxLength="15" onkeypress="mascara(this, mvalor)"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10" style="display: inline-block; float: none;">
                                                    <div class="truncate">
                                                        <asp:HiddenField ID="HdfCodigo" runat="server" />
                                                        <asp:Label ID="LblNome" runat="server" CssClass="text-uppercase"></asp:Label>
                                                    </div>
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="fixed-button-box">
                <div class="text-right">
                    <asp:LinkButton ID="BtnSalvar" runat="server" CssClass="btn btn-success btn-lg"
                        data-toggle="tooltip" data-container="body" ToolTip="Salvar informações"
                        ValidationGroup="edicao" OnClientClick="return validar();" OnClick="BtnSalvar_Click">
                    <i class="fa fa-save"></i>
                    </asp:LinkButton>
                    &nbsp;
                <asp:LinkButton ID="BtnCancelar" runat="server"
                    CssClass="btn btn-danger btn-lg pull-right" data-toggle="tooltip" data-container="body" ToolTip="Cancelar operação"
                    Text="Cancelar" CausesValidation="false">
                    <i class="fa fa-times"></i>
                </asp:LinkButton>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
