﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="usuarios.aspx.cs" Inherits="usuarios" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        // Chamada de alerta para confirmação de exclusão
        function ConfirmacaoExclucao(ctl, event, id) {
            var defaultAction = $(ctl).prop("href");
            event.preventDefault();
            swal({
                title: "Deseja excluir?",
                text: 'Uma vez excluído, o registro não poderá ser recuperado.',
                type: 'question',
                showCancelButton: true,
                confirmButtonText: "<i class='fa fa-check'></i>&nbsp;Sim!",
                cancelButtonText: "<i class='fa fa-times'></i>&nbsp;Não!",
                confirmButtonColor: '#00a65a',
                cancelButtonColor: '#dd4b39',
                showLoaderOnConfirm: true,
                animation: 'slide-from-top',
                allowEscapeKey: false,
                allowOutsideClick: false
            }).then(result => {
                if (result.value) {
                    document.getElementById('<%=HdfCodigo.ClientID%>').value = id;
                    var btn = document.getElementById('<%=BtnExcluirItens.ClientID%>');
                    btn.click();
                    return true;
                }
            })
        }

        // Chamada de alerta para confirmação de troca de Situação
        function ConfirmacaoSituacao(ctl, event, id) {
            var defaultAction = $(ctl).prop("href");
            event.preventDefault();
            swal({
                title: "Deseja alterar a situação?",
                text: 'A alteração pode fazer com que algumas funcionalidades deixem de funcionar corretamente.',
                type: 'question',
                showCancelButton: true,
                confirmButtonText: "<i class='fa fa-check'></i>&nbsp;Sim!",
                cancelButtonText: "<i class='fa fa-times'></i>&nbsp;Não!",
                confirmButtonColor: '#00a65a',
                cancelButtonColor: '#dd4b39',
                showLoaderOnConfirm: true,
                animation: 'slide-from-top',
                allowEscapeKey: false,
                allowOutsideClick: false
            }).then(result => {
                if (result.value) {
                    document.getElementById('<%=HdfCodigo.ClientID%>').value = id;
                    var btn = document.getElementById('<%=BtnAlterarSituacao.ClientID%>');
                    btn.click();
                    return true;
                }
            })
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row">
        <div class="col-md-12">
            <ul class="breadcrumb">
                <li><a href="dashboard.aspx"><i class="fa fa-home"></i>&nbsp;Home</a></li>
                <li class="active">Usuários</li>
            </ul>
            <h1 class="conteudo-centro"><i class="fa fa-users"></i>&nbsp;Usuários</h1>
        </div>
    </div>
    <asp:UpdatePanel ID="UpdMain" runat="server">
        <ContentTemplate>
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-header with-border">
                            <h3 class="box-title"><i class="fa fa-sliders"></i>&nbsp;Filtros</h3>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" data-container="body" title="Ocultar/exibir filtros">
                                    <i class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="box-body" style="display: block;">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 form-group">
                                    <asp:DropDownList ID="DdlSituacoes" runat="server" CssClass="form-control text-uppercase" DataValueField="ValueMember" DataTextField="DisplayMember"></asp:DropDownList>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7 form-group">
                                    <asp:DropDownList ID="DdlTipos" runat="server" CssClass="form-control text-uppercase" DataValueField="ValueMember" DataTextField="DisplayMember"></asp:DropDownList>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                    <asp:TextBox ID="TxtPesquisa" runat="server" CssClass="form-control" placeHolder="PESQUISAR..."></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer" style="display: block;">
                            <div class="row">
                                <div class="col-lg-12 text-right">
                                    <asp:LinkButton ID="BtnPesquisar" runat="server" CssClass="btn btn-success" data-toggle="tooltip" data-container="body" ToolTip="Pesquisar registros" CausesValidation="false" OnClick="BtnPesquisar_Click">
                                        <i class="fa fa-search"></i>&nbsp;Pesquisar
                                    </asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 text-left bloco-legenda">
                                    <a href="#" class="dropdown-toggle btn btn-block btn-success" data-toggle="dropdown">
                                        <i class="fa fa-info-circle"></i>&nbsp;Legenda
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <div style="padding: 8px;">
                                                <i class="fa fa-pencil text-regular"></i>&nbsp;Editar registro
                                            </div>
                                        </li>
                                        <li>
                                            <div style="padding: 8px;">
                                                <i class="fa fa-trash text-regular"></i>&nbsp;Excluir registro
                                            </div>
                                        </li>
                                    </ul>
                                    <div class="dv-spc-10"></div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10 box-resultados text-right text-blue conteudo-centro">
                                    <asp:Label ID="LblRegistros" runat="server"></asp:Label>
                                </div>
                            </div>
                            <div class="dv-spc-10"></div>
                            <div class="box-grids">
                                <div class="dv-grid-f-header">
                                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="display: inline-block; float: none;">
                                        Ações
                                    </div>
                                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="display: inline-block; float: none;">
                                        Situação
                                    </div>
                                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="display: inline-block; float: none;">
                                        Login
                                    </div>
                                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1" style="display: inline-block; float: none;">
                                        Tipo
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" style="display: inline-block; float: none;">
                                        Nome
                                    </div>
                                </div>
                                <div class="dv-grid-f">
                                    <asp:Repeater ID="RptPrincipal" runat="server" OnItemDataBound="RptPrincipal_ItemDataBound" OnItemCommand="RptPrincipal_ItemCommand">
                                        <ItemTemplate>
                                            <div id="dvItem" runat="server">
                                                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="display: inline-block; float: none;">
                                                    <div class="truncate">
                                                        <asp:LinkButton ID="LnkEditList" runat="server" CommandName="edit" data-toggle="tooltip" data-container="body" ToolTip="Editar registro">
                                                        <i class="fa fa-pencil text-regular"></i>
                                                        </asp:LinkButton>
                                                        <asp:LinkButton ID="LnkDeleteList" runat="server" data-toggle="tooltip" data-container="body" ToolTip="Excluir registro" CommandName="delete">
                                                        <i class="fa fa-trash text-regular"></i>
                                                        </asp:LinkButton>
                                                    </div>
                                                </div>
                                                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="display: inline-block; float: none;">
                                                    <div class="truncate" style="margin-left: -20px; margin-top: -36px !important; position: absolute;">
                                                        <asp:LinkButton ID="LnkStatus" runat="server" CommandName="status" CssClass="btn btn-sm btn-success" OnClientClick="return confirm('Deseja alterar o status do registro?');">
                                                            <asp:Label ID="LblStatus" runat="server" CssClass="label" Text="ATIVO"></asp:Label>
                                                        </asp:LinkButton>
                                                    </div>
                                                </div>
                                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="display: inline-block; float: none;">
                                                    <div class="truncate">
                                                        <asp:Label ID="lblLogin" runat="server" CssClass="text-lowercase"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1" style="display: inline-block; float: none;">
                                                    <div class="truncate">
                                                        <asp:Label ID="lblTipo" runat="server" CssClass="text-uppercase"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" style="display: inline-block; float: none;">
                                                    <div class="truncate">
                                                        <asp:Label ID="lblNome" runat="server" CssClass="text-uppercase"></asp:Label>
                                                    </div>
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                        </div>
                        <div class="dv-spc-10">
                        </div>
                        <div class="box-footer">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                                    <asp:Label ID="LblInfo" runat="server"></asp:Label>
                                    <br />
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                                    <label>Página selecionada</label>&nbsp;
                                    <asp:DropDownList ID="DdlPaginas" runat="server" CssClass="form-control max-drop"
                                        AutoPostBack="true" OnSelectedIndexChanged="DdlPaginas_SelectedIndexChanged" data-toggle="tooltip" data-container="body" ToolTip="Escolher página para exibição">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="fixed-button-box">
                <asp:LinkButton ID="BtnNovo" runat="server" CssClass="btn btn-success btn-lg" CausesValidation="False" OnClick="BtnNovo_Click" data-toggle="tooltip">
                    <i class="fa fa-plus"></i>
                </asp:LinkButton>
            </div>
            <asp:LinkButton ID="BtnExcluirItens" runat="server" CssClass="hidden" OnClick="BtnExcluirItens_Click">
            </asp:LinkButton>
            <asp:LinkButton ID="BtnAlterarSituacao" runat="server" CssClass="hidden" OnClick="BtnAlterarSituacao_Click">
            </asp:LinkButton>
            <asp:HiddenField ID="HdfCodigo" runat="server" />
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="BtnPesquisar" />
            <asp:AsyncPostBackTrigger ControlID="RptPrincipal" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

