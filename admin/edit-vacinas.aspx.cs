﻿using System;
using System.Collections.Specialized;
using System.Text;
using System.Web.UI;

public partial class edit_vacinas : System.Web.UI.Page
{
    private StringBuilder url = new StringBuilder();
    private string textoMensagem = String.Empty;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            IniciarPagina();
    }

    /// <summary>
    /// 
    /// </summary>
    private void IniciarPagina()
    {
        NameValueCollection vacina = new NameValueCollection();
        int codigoRegistro = -1;
        string comando = String.Empty;

        if (Request.QueryString["vacina"] != null)
        {
            vacina = ImunneVacinas.Utils.RecuperarQueryString(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["vacina"]));

            url = new StringBuilder();
            url.AppendFormat("~/admin/vacinas.aspx?vacina={0}", Request.QueryString["vacina"]);
            LnkPai.NavigateUrl = url.ToString();
            BtnCancelar.OnClientClick = "return CancelarFormulario(this, event,'" + url.Replace("~/admin/", "") + "');";

            codigoRegistro = Convert.ToInt32(vacina["codigo"]);
            comando = vacina["acao"].ToString();

            PopularCampos();

            LblForms.Text = ImunneVacinas.Utils.MontarTituloFormulario(comando);

            switch (comando)
            {
                case "edit":
                    {
                        ConsultarDetalhes(codigoRegistro);
                    }
                    break;
            }

            Page.Title = "S&A Imunizações | " + LblForms.Text;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void PopularCampos()
    {
        TxtNome.MaxLength = (int)ImunneVacinas.Vacina.GetInfoNome().Max_length;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="codigo"></param>
    private void ConsultarDetalhes(int codigo)
    {
        ImunneVacinas.Vacina registroSelecionado = ImunneVacinas.Vacina.ConsultarUnico(codigo);

        TxtNome.Text = registroSelecionado.Nome;
        TxtObservacoes.InnerText = registroSelecionado.Observacao;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BtnSalvar_Click(object sender, EventArgs e)
    {
        try
        {
            NameValueCollection vacina = new NameValueCollection();

            vacina = ImunneVacinas.Utils.RecuperarQueryString(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["vacina"]));

            int codigoRegistro = ImunneVacinas.Parser.parseInteiro(vacina["codigo"]);
            string comando = vacina["acao"].ToString();

            ImunneVacinas.Vacina novoRegistro;
            ImunneVacinas.Vacina registroOriginal = new ImunneVacinas.Vacina();
            if (codigoRegistro > 0)
            {
                novoRegistro = ImunneVacinas.Vacina.ConsultarUnico(codigoRegistro);
                registroOriginal = ImunneVacinas.Vacina.ConsultarUnico(codigoRegistro);
            }
            else
                novoRegistro = new ImunneVacinas.Vacina();

            novoRegistro.Nome = TxtNome.Text;
            novoRegistro.Observacao = TxtObservacoes.InnerText;

            int resultado = 0;

            switch (comando)
            {
                case "insert":
                    {
                        novoRegistro.DataCriacao = DateTime.Now;
                        resultado = novoRegistro.Incluir();
                    }
                    break;
                case "edit":
                    {
                        ImunneVacinas.Vacina dadosAtuais = ImunneVacinas.Vacina.ConsultarUnico(codigoRegistro);
                        novoRegistro.Codigo = Convert.ToInt32(codigoRegistro);
                        novoRegistro.DataAlteracao = DateTime.Now;
                        resultado = novoRegistro.Alterar();
                    }
                    break;
            }

            if (resultado <= 0)
            {
                textoMensagem = "Lamentamos mas ocorreu um erro e não foi possível concluir a operação. Tente novamente ou informe o suporte.";
                ImunneVacinas.Alerta.showMensagemErro(this.Page, null, textoMensagem);
                return;
            }

            #region Geração de Logs

            //// Log descritivo
            //ImunneVacinas.Log logAcao = new ImunneVacinas.Log();
            //logAcao.Tela = Path.GetFileName(Request.PhysicalPath);
            //logAcao.IdVacina.Codigo = this.Page.ConsultarSessionCodigoVacina();
            //logAcao.TabelaAcao = "Vacinas";

            //switch (comando)
            //{
            //    case "insert":
            //        {
            //            logAcao.TipoAcao = ImunneVacinas.Utils.AcaoLog.Insercao;
            //            logAcao.Registro = resultado;
            //            logAcao.Resumo = String.Format("INSERÇÃO DE NOVO REGISTRO {0}.", logAcao.Registro);
            //            logAcao.DataAcao = DateTime.Now;
            //            logAcao.Campo = String.Empty;
            //            logAcao.ValorOriginal = String.Empty;
            //            logAcao.ValorNovo = String.Empty;
            //            logAcao.Incluir();
            //        }
            //        break;
            //    case "edit":
            //        {
            //            logAcao.TipoAcao = ImunneVacinas.Utils.AcaoLog.Edicao;
            //        }
            //        break;
            //}

            #endregion

            url = new StringBuilder();
            url.AppendFormat("~/admin/vacinas.aspx?vacina={0}", Request.QueryString["vacina"]);
            Response.Redirect(url.ToString(), false);
            Context.ApplicationInstance.CompleteRequest();
        }
        catch (Exception ex)
        {
            textoMensagem = "Lamentamos mas ocorreu um erro e não foi possível concluir a operação. Tente novamente ou informe o suporte.";
            ImunneVacinas.Alerta.showMensagemErro(this.Page, null, textoMensagem);
            this.SalvarException(ex);
        }
    }
}