﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.IO;
using System.Text;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

/// <summary>
/// 
/// </summary>
public partial class aplicacoes : System.Web.UI.Page
{
    private StringBuilder url = new StringBuilder();
    private StringBuilder parametros = new StringBuilder();

    /// <summary>
    /// 
    /// </summary>
    private List<string> itensBaixa
    {
        get
        {
            if (Session["listaItensBaixa"] == null)
                Session["listaItensBaixa"] = new List<string>();
            return (List<string>)Session["listaItensBaixa"];
        }
        set
        {
            Session["listaItensBaixa"] = value;
        }
    }


    #region Private Proprieties

    private int PaginaAtual
    {
        get
        {
            object objPage = ViewState["_PaginaAtual"];
            int _PaginaAtual = 0;
            if (objPage == null)
            {
                _PaginaAtual = 0;
            }
            else
            {
                _PaginaAtual = (int)objPage;
            }
            return _PaginaAtual;
        }
        set
        {
            ViewState["_PaginaAtual"] = value;
        }
    }
    private int PrimeiroIndice
    {
        get
        {

            int _PrimeiroIndice = 0;
            if (ViewState["_PrimeiroIndice"] == null)
            {
                _PrimeiroIndice = 0;
            }
            else
            {
                _PrimeiroIndice = Convert.ToInt32(ViewState["_PrimeiroIndice"]);
            }
            return _PrimeiroIndice;
        }
        set
        {
            ViewState["_PrimeiroIndice"] = value;
        }
    }
    private int UltimoIndice
    {
        get
        {

            int _UltimoIndice = 0;
            if (ViewState["_UltimoIndice"] == null)
            {
                _UltimoIndice = 0;
            }
            else
            {
                _UltimoIndice = Convert.ToInt32(ViewState["_UltimoIndice"]);
            }
            return _UltimoIndice;
        }
        set
        {
            ViewState["_UltimoIndice"] = value;
        }
    }

    #endregion

    #region PagedDataSource

    PagedDataSource _PagedDataSource = new PagedDataSource();

    #endregion

    #region Private Methods

    /// <summary>
    /// 
    /// </summary>
    private void VincularItens()
    {
        try
        {
            List<ImunneVacinas.Compra> retorno = ConsultarRegistros();

            _PagedDataSource.DataSource = retorno;
            _PagedDataSource.AllowPaging = true;
            _PagedDataSource.PageSize = Convert.ToInt32(DdlItens.SelectedValue);
            _PagedDataSource.CurrentPageIndex = PaginaAtual;
            ViewState["TotalPages"] = _PagedDataSource.PageCount;

            DdlPaginas.Items.Clear();
            for (int x = 0; x < _PagedDataSource.PageCount; x++)
            {
                DdlPaginas.Items.Add(new ListItem((x + 1).ToString(), x.ToString()));
            }

            DdlPaginas.SelectedValue = PaginaAtual.ToString();

            if (DdlUtilizacoes.SelectedValue == "1")
                PnlAplicacoes.Visible = false;
            else PnlAplicacoes.Visible = true;

            RptPrincipal.DataSource = _PagedDataSource;
            RptPrincipal.DataBind();
            CriarPaginacao();

            LblRegistros.Text = ImunneVacinas.Utils.MontarLinhaTotalizadora(retorno.Count);
            LblInfo.Text = "Exibindo página " + (PaginaAtual + 1) + " de " + _PagedDataSource.PageCount + ".";
        }
        catch (Exception Ex)
        {
            this.SalvarException(Ex);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void CriarPaginacao()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("PageIndex");
        dt.Columns.Add("PageText");

        PrimeiroIndice = PaginaAtual - 5;


        if (PaginaAtual > 5)
        {
            UltimoIndice = PaginaAtual + 5;
        }
        else
        {
            UltimoIndice = 10;
        }
        if (UltimoIndice > Convert.ToInt32(ViewState["TotalPages"]))
        {
            UltimoIndice = Convert.ToInt32(ViewState["TotalPages"]);
            PrimeiroIndice = UltimoIndice - 10;
        }

        if (PrimeiroIndice < 0)
        {
            PrimeiroIndice = 0;
        }

        for (int i = PrimeiroIndice; i < UltimoIndice; i++)
        {
            DataRow dr = dt.NewRow();
            dr[0] = i;
            dr[1] = i + 1;
            dt.Rows.Add(dr);
        }

        DdlPaginas.SelectedValue = PaginaAtual.ToString();
    }

    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = "S&A Imunizações | Aplicações";
        if (!IsPostBack)
            IniciarPagina();
    }

    /// <summary>
    /// 
    /// </summary>
    private void IniciarPagina()
    {
        itensBaixa = new List<string>();

        int codigoClinica = this.Page.ConsultarClinicaUsuario();
        int codigoCampanha = -1;
        int roleUsuario = this.Page.ConsultarSessionTipoUsuario();
        int paginaAtual = 0;
        int itens = 20;

        string inicioData = DateTime.Now.ToString("dd/MM/yyyy");
        string terminoData = DateTime.Now.ToString("dd/MM/yyyy");
        string pesquisa = String.Empty;
        string categoria = "-1";
        string situacao = "3";
        string aplicacao = "-1";

        LnkMarcarTodos.Text = "<i class='fa fa-check'></i>&nbsp;Marcar Toda Página";
        ChkTodos.Checked = false;

        try
        {
            NameValueCollection compra = new NameValueCollection();

            #region PARÂMETROS DE RETORNO DE TELA

            if (!String.IsNullOrEmpty(Request.QueryString["compra"]))
            {
                compra = ImunneVacinas.Utils.RecuperarQueryString(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["compra"]));
                inicioData = compra["iniciodata"];
                terminoData = compra["terminodata"];
                situacao = compra["situacao"];
                aplicacao = compra["aplicacao"];
                categoria = compra["categoria"];
                pesquisa = compra["pesquisa"];
                codigoClinica = Convert.ToInt32(compra["clinica"]);
                codigoCampanha = Convert.ToInt32(compra["campanha"]);
                itens = Convert.ToInt32(compra["itens"]);
                paginaAtual = Convert.ToInt32(compra["current_page"]);
            }
            else
            {
                if (roleUsuario == 7)
                {
                    inicioData = String.Format("01/01/{0}", DateTime.Now.Year);
                    terminoData = DateTime.Now.ToString("dd/MM/yyyy");
                }
            }

            #endregion

            DdlClinicas.DataSource = ImunneVacinas.Clinica.ConsultarComboBox(true, "CLINICAS");
            DdlClinicas.DataBind();
            DdlClinicas.SelectedValue = codigoClinica.ToString();

            DdlSituacoesCompras.Items.Add(new ListItem("SITUAÇÕES", "-1"));
            DdlSituacoesCompras.Items.Add(new ListItem("NÃO REALIZADAS", "1"));
            DdlSituacoesCompras.Items.Add(new ListItem("NÃO AUTORIZADAS", "2"));
            DdlSituacoesCompras.Items.Add(new ListItem("AUTORIZADAS", "3"));
            DdlSituacoesCompras.SelectedValue = situacao;

            DdlCategorias.Items.Add(new ListItem("CATEGORIAS", "-1"));
            DdlCategorias.Items.Add(new ListItem("PAGAS", "1"));
            DdlCategorias.Items.Add(new ListItem("IMPORTADAS", "3"));
            DdlCategorias.SelectedValue = categoria;

            DdlUtilizacoes.Items.Add(new ListItem("APLICAÇÕES", "-1"));
            DdlUtilizacoes.Items.Add(new ListItem("NÃO APLICADAS", "0"));
            DdlUtilizacoes.Items.Add(new ListItem("APLICADAS", "1"));
            DdlUtilizacoes.SelectedValue = aplicacao;

            DdlItens.Items.Add(new ListItem("MOSTRAR 10", "10"));
            DdlItens.Items.Add(new ListItem("MOSTRAR 20", "20"));
            DdlItens.Items.Add(new ListItem("MOSTRAR 30", "30"));
            DdlItens.Items.Add(new ListItem("MOSTRAR 40", "40"));
            DdlItens.Items.Add(new ListItem("MOSTRAR 50", "50"));
            DdlItens.Items.Add(new ListItem("MOSTRAR 100", "100"));
            DdlItens.SelectedValue = itens.ToString();

            DdlCampanhas.Items.Clear();

            #region CAMPANHAS DA CLÍNICA DO USUÁRIO

            if (roleUsuario == 7)
            {
                DdlClinicas.SelectedValue = codigoClinica.ToString();
                DdlClinicas.Enabled = false;

                codigoClinica = Convert.ToInt32(DdlClinicas.SelectedValue);
                ImunneVacinas.PesquisaCampanhaClinica pesquisaCampanha = new ImunneVacinas.PesquisaCampanhaClinica()
                {
                    Clinica = codigoClinica,
                    Ordenacao = ImunneVacinas.Utils.TipoOrdenacao.porNome
                };

                DdlCampanhas.DataSource = ImunneVacinas.CampanhaClinica.ConsultarCampanhasComboBox(pesquisaCampanha, true, "CAMPANHAS");
                DdlCampanhas.DataBind();
            }
            else
            {
                DdlCampanhas.Items.Clear();
                DdlCampanhas.DataSource = ImunneVacinas.Campanha.ConsultarComboBox(true, "CAMPANHAS");
                DdlCampanhas.DataBind();

                if (codigoClinica > 0)
                {
                    DdlClinicas.SelectedValue = codigoClinica.ToString();

                    ImunneVacinas.PesquisaCampanhaClinica pesquisaCampanha = new ImunneVacinas.PesquisaCampanhaClinica()
                    {
                        Clinica = codigoClinica,
                        Ordenacao = ImunneVacinas.Utils.TipoOrdenacao.porNome
                    };

                    DdlCampanhas.DataSource = ImunneVacinas.CampanhaClinica.ConsultarCampanhasComboBox(pesquisaCampanha, true, "CAMPANHAS");
                    DdlCampanhas.DataBind();
                    DdlCampanhas.Enabled = true;
                    DdlCampanhas.SelectedValue = codigoCampanha.ToString();
                }
            }

            #endregion

            HdfFinal.Value = terminoData;
            HdfInicio.Value = inicioData;

            DdlCampanhas.SelectedValue = codigoCampanha.ToString();

            if (!String.IsNullOrEmpty(pesquisa))
                TxtPesquisa.Text = pesquisa;

            if (DdlUtilizacoes.SelectedValue != "1")
                PnlAplicacoes.Visible = true;
            else PnlAplicacoes.Visible = false;

            PaginaAtual = paginaAtual;
            VincularItens();
        }
        catch (Exception ex)
        {
            this.SalvarException(ex);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private List<ImunneVacinas.Compra> ConsultarRegistros()
    {
        DateTime dataInicio = ImunneVacinas.Utils.dataPadrao;
        DateTime dataFinal = ImunneVacinas.Utils.dataPadrao;

        if (!String.IsNullOrEmpty(HdfInicio.Value) && !String.IsNullOrEmpty(HdfFinal.Value))
        {
            dataInicio = Convert.ToDateTime(HdfInicio.Value);
            dataFinal = Convert.ToDateTime(HdfFinal.Value).AddDays(1);
        }

        ImunneVacinas.PesquisaCompra pesquisa = new ImunneVacinas.PesquisaCompra
        {
            Clinica = Convert.ToInt32(DdlClinicas.SelectedValue),
            Campanha = Convert.ToInt32(DdlCampanhas.SelectedValue),
            CampoLivre = TxtPesquisa.Text,
            Situacao = (ImunneVacinas.Compra.SituacaoCompra)Enum.Parse(typeof(ImunneVacinas.Compra.SituacaoCompra), DdlSituacoesCompras.SelectedValue),
            Categoria = (ImunneVacinas.Compra.CategoriaCompra)Enum.Parse(typeof(ImunneVacinas.Compra.CategoriaCompra), DdlCategorias.SelectedValue),
            Aplicacao = (ImunneVacinas.Compra.Aplicacao)Enum.Parse(typeof(ImunneVacinas.Compra.Aplicacao), DdlUtilizacoes.SelectedValue),
            Ordenacao = ImunneVacinas.Utils.TipoOrdenacao.porDataDesc,
            DataFim = dataFinal,
            DataInicio = dataInicio,
            Valor = true
        };

        return ImunneVacinas.Compra.ConsultarCompras(pesquisa);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void RptPrincipal_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        try
        {
            ImunneVacinas.Compra registroSelecionado = (ImunneVacinas.Compra)e.Item.DataItem;

            #region ITENS DE TELA

            HiddenField HdfCodigoCompra = (HiddenField)e.Item.FindControl("HdfCodigoCompra");

            CheckBox ChkUtilizacao = (CheckBox)e.Item.FindControl("ChkUtilizacao");

            Label LblCampanha = (Label)e.Item.FindControl("LblCampanha");
            Label LblData = (Label)e.Item.FindControl("LblData");
            Label LblValor = (Label)e.Item.FindControl("LblValor");
            Label LblParticipante = (Label)e.Item.FindControl("LblParticipante");
            Label LblClinica = (Label)e.Item.FindControl("LblClinica");
            Label LblTotalItens = (Label)e.Item.FindControl("LblTotalItens");
            Label LblComprovante = (Label)e.Item.FindControl("LblComprovante");
            Label LblPedido = (Label)e.Item.FindControl("LblPedido");
            Label LblCidade = (Label)e.Item.FindControl("LblCidade");
            Label LblUf = (Label)e.Item.FindControl("LblUf");
            Label LblAplicacao = (Label)e.Item.FindControl("LblAplicacao");

            LinkButton LnkTransacao = (LinkButton)e.Item.FindControl("LnkTransacao");

            HtmlControl LnkDetalhes = (HtmlControl)e.Item.FindControl("LnkDetalhes");

            #endregion

            LblCampanha.Text = registroSelecionado.IdCampanha.Identificacao.ToUpper();
            LblData.Text = registroSelecionado.DataCompra.ToString("dd/MM/yyyy HH:mm");
            LblParticipante.Text = registroSelecionado.IdParticipante.Nome.ToUpper();
            LblPedido.Text = registroSelecionado.Codigo.ToString().PadLeft(5, '0');
            LnkTransacao.CommandArgument = registroSelecionado.Codigo.ToString();

            HdfCodigoCompra.Value = registroSelecionado.Codigo.ToString();

            LnkDetalhes.Attributes.Add("data-id", "../popups/detalhes-compras.aspx?&compra=" + registroSelecionado.Codigo);

            #region APLICAÇÕES

            switch (registroSelecionado.AplicacaoCompra)
            {
                case ImunneVacinas.Compra.Aplicacao.Aplicado:
                    {
                        ChkUtilizacao.Checked = true;
                        ChkUtilizacao.Enabled = false;
                        ChkUtilizacao.Style.Add("opacity", ".6");

                        LnkTransacao.Enabled = false;
                        LnkTransacao.Style.Add("opacity", ".6");
                        LblAplicacao.Text = String.Format("APLICADO | {0}", registroSelecionado.DataAplicacao.ToString("dd/MM/yyyy HH:ss"));
                        LblAplicacao.CssClass = "label-success lbl-status";
                    }
                    break;
                default:
                    {
                        int totalUtilizados = 0;
                        int totalGeral = 0;

                        #region UTILIZADOS

                        ImunneVacinas.PesquisaVoucher pesquisaVouchers = new ImunneVacinas.PesquisaVoucher()
                        {
                            Situacao = ImunneVacinas.Voucher.SituacaoVoucher.Utilizado,
                            Compra = registroSelecionado.Codigo,
                            Ordenacao = ImunneVacinas.Utils.TipoOrdenacao.porData
                        };
                        totalUtilizados = ImunneVacinas.Voucher.ContabilizarVouchers(pesquisaVouchers);

                        #endregion

                        #region GERAL

                        pesquisaVouchers = new ImunneVacinas.PesquisaVoucher()
                        {
                            Compra = registroSelecionado.Codigo,
                            Ordenacao = ImunneVacinas.Utils.TipoOrdenacao.porData
                        };
                        totalGeral = ImunneVacinas.Voucher.ContabilizarVouchers(pesquisaVouchers);

                        #endregion

                        ChkUtilizacao.Checked = false;
                        ChkUtilizacao.Enabled = true;
                        ChkUtilizacao.Style.Add("opacity", "1");

                        LnkTransacao.Enabled = true;
                        LnkTransacao.Style.Add("opacity", "1");

                        if (totalUtilizados == 0)
                        {
                            LblAplicacao.Text = String.Format("NÃO APLICADO");
                            LblAplicacao.CssClass = "label-danger lbl-status";

                            foreach (string opcao in itensBaixa)
                            {
                                if (opcao == registroSelecionado.Codigo.ToString())
                                {
                                    ChkUtilizacao.Checked = true;
                                    break;
                                }
                            }
                        }
                        else if (totalGeral != totalUtilizados)
                        {
                            LblAplicacao.Text = String.Format("UTILIZADOS {0} DE {1}", totalUtilizados, totalGeral);
                            LblAplicacao.CssClass = "label-warning lbl-status";
                        }
                        else
                        {
                            LblAplicacao.Text = String.Format("UTILIZADOS {0} DE {1}", totalUtilizados, totalGeral);
                            LblAplicacao.CssClass = "label-success lbl-status";
                        }
                    }
                    break;
            }

            #endregion

            LblClinica.Text = "--";
            LblCidade.Text = "--";
            LblUf.Text = "--";

            if (!String.IsNullOrEmpty(registroSelecionado.IdClinica.NomeFantasia))
            {
                LblClinica.Text = registroSelecionado.IdClinica.NomeFantasia;
                LblCidade.Text = registroSelecionado.IdClinica.IdCidade.Nome;
                LblUf.Text = registroSelecionado.IdClinica.IdUf.Sigla;
            }

            if (!String.IsNullOrEmpty(registroSelecionado.NumeroComprovante))
                LblComprovante.Text = registroSelecionado.NumeroComprovante;
            else LblComprovante.Text = "--";

            #region ITENS

            ImunneVacinas.PesquisaItemCompra pesquisa = new ImunneVacinas.PesquisaItemCompra()
            {
                Compra = registroSelecionado.Codigo
            };

            decimal valoresItens = 0;
            int totalItens = 0;

            List<ImunneVacinas.ItemCompra> listaItens = ImunneVacinas.ItemCompra.ConsultarItens(pesquisa);
            foreach (ImunneVacinas.ItemCompra item in listaItens)
            {
                totalItens += item.Quantidade;
                valoresItens += item.Valor;
            }

            LblTotalItens.Text = totalItens.ToString();
            LblValor.Text = ImunneVacinas.Utils.ToMoeda(valoresItens);

            #endregion

            #region SITUAÇÕES

            switch (registroSelecionado.Situacao)
            {
                case ImunneVacinas.Compra.SituacaoCompra.Aberta:
                    {
                        LnkDetalhes.Style.Add("opacity", "0");
                        LblComprovante.Text = "NÃO REALIZADA";

                        ChkUtilizacao.Enabled = false;
                        ChkUtilizacao.Style.Add("opacity", ".6");
                    }
                    break;
                case ImunneVacinas.Compra.SituacaoCompra.Cancelada:
                    {
                        ChkUtilizacao.Checked = false;
                        ChkUtilizacao.Enabled = false;
                        ChkUtilizacao.Style.Add("opacity", ".6");

                        LblAplicacao.Text = "NÃO AUTORIZADA";
                        LnkAplicacoes.Visible = false;
                        LnkTransacao.Visible = false;
                    }
                    break;
                case ImunneVacinas.Compra.SituacaoCompra.Concluida:
                    {

                    }
                    break;
            }

            #endregion
        }
        catch (Exception ex)
        {
            this.SalvarException(ex);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BtnPesquisar_Click(object sender, EventArgs e)
    {
        itensBaixa = new List<string>();

        PaginaAtual = 0;
        int role = this.Page.ConsultarSessionTipoUsuario();

        try
        {
            VincularItens();
        }
        catch (Exception ex)
        {
            this.SalvarException(ex);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void LnkMarcarTodos_Click(object sender, EventArgs e)
    {
        itensBaixa = new List<string>();

        if (ChkTodos.Checked == true)
        {
            ChkTodos.Checked = false;
            LnkMarcarTodos.Text = "<i class='fa fa-check'></i>&nbsp;Marcar Toda Página";

            foreach (RepeaterItem item in RptPrincipal.Items)
            {
                CheckBox ChkUtilizacao = (CheckBox)item.FindControl("ChkUtilizacao");
                HiddenField HdfCodigoCompra = (HiddenField)item.FindControl("HdfCodigoCompra");

                if (ChkUtilizacao.Enabled == true && ChkUtilizacao.Checked == true)
                    ChkUtilizacao.Checked = false;
            }
        }
        else
        {
            ChkTodos.Checked = true;
            LnkMarcarTodos.Text = "<i class='fa fa-check'></i>&nbsp;Desmarcar Toda Página";

            foreach (RepeaterItem item in RptPrincipal.Items)
            {
                CheckBox ChkUtilizacao = (CheckBox)item.FindControl("ChkUtilizacao");
                HiddenField HdfCodigoCompra = (HiddenField)item.FindControl("HdfCodigoCompra");

                if (ChkUtilizacao.Enabled == true && ChkUtilizacao.Checked == false)
                {
                    ChkUtilizacao.Checked = true;
                    itensBaixa.Add(HdfCodigoCompra.Value);
                }
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void LnkAplicacoes_Click(object sender, EventArgs e)
    {
        StringBuilder mensagemConclusao = new StringBuilder();

        int contador = 0;

        // NENHUM ITEM NA LISTA DE BAIXAS
        if (itensBaixa == null || itensBaixa.Count <= 0)
        {
            ImunneVacinas.Alerta.showMensagemAviso(this.Page, "Aviso", "Nenhuma compra foi selecionada para realizar a baixa.");
            return;
        }

        // CORRENDO OS ITENS A SEREM BAIXADOS
        foreach (string opcao in itensBaixa)
        {
            ImunneVacinas.Compra compraSelecionada = ImunneVacinas.Compra.ConsultarUnico(Convert.ToInt32(opcao));
            if (BaixarCompra(compraSelecionada.Codigo) > 0)
            {
                try
                {
                    compraSelecionada.DataAplicacao = DateTime.Now;
                    compraSelecionada.AplicacaoCompra = ImunneVacinas.Compra.Aplicacao.Aplicado;
                    compraSelecionada.Alterar();

                    contador += 1;

                    if (mensagemConclusao.Length > 0)
                        mensagemConclusao.Append(" | ");
                    else mensagemConclusao.Append("COMPRAS BAIXADAS ");

                    mensagemConclusao.AppendFormat("{0}", compraSelecionada.NumeroComprovante);
                }
                catch (Exception ex)
                {
                    this.SalvarException(ex);
                }
            }
        }

        // CONCLUSÃO
        if (contador == 0)
        {
            ImunneVacinas.Alerta.showMensagemErro(this.Page, "Aviso", "Nenhuma compra foi selecionada ou não foi possível realizar a baixa das compras selecionadas!");
            return;
        }

        #region LOG SISTEMA

        ImunneVacinas.Log novoLog = new ImunneVacinas.Log()
        {
            Tela = Path.GetFileName(Request.PhysicalPath),
            TabelaAcao = "COMPRAS",
            TipoAcao = ImunneVacinas.Utils.AcaoLog.Acesso,
            Resumo = mensagemConclusao.ToString(),
            DataAcao = DateTime.Now,
            Campo = "BAIXAS",
            ValorOriginal = String.Empty,
            ValorNovo = String.Empty
        };
        novoLog.IdUsuario.Codigo = this.Page.ConsultarSessionCodigoUsuario();
        novoLog.Incluir();

        #endregion

        ImunneVacinas.Alerta.showMensagemSucesso(this.Page, "Sucesso!", "Todas as compras selecionadas foram aplicadas com sucesso!");

        if (DdlUtilizacoes.SelectedValue != "-1")
            PaginaAtual = 0;

        itensBaixa = new List<string>();
        VincularItens();
    }

    /// <summary>
    /// Baixa as compras selecionadas
    /// </summary>
    /// <param name="codigoCompra">VALUE: Código de identificação da compra selecionada</param>
    /// <returns></returns>
    private int BaixarCompra(int codigoCompra)
    {
        int totalRetornos = 0;

        try
        {
            ImunneVacinas.Compra compraSelecionada = ImunneVacinas.Compra.ConsultarUnico(codigoCompra);
            ImunneVacinas.Campanha campanhaSelecionada = ImunneVacinas.Campanha.ConsultarUnico(compraSelecionada.IdCampanha.Codigo);
            ImunneVacinas.Participante participanteSelecionado = ImunneVacinas.Participante.ConsultarUnico(compraSelecionada.IdParticipante.Codigo);

            int totalDoses = 0;

            ImunneVacinas.PesquisaVoucher pesquisaVoucher = new ImunneVacinas.PesquisaVoucher()
            {
                Compra = codigoCompra,
                Ordenacao = ImunneVacinas.Utils.TipoOrdenacao.porData
            };

            List<ImunneVacinas.Voucher> listaVouchers = ImunneVacinas.Voucher.ConsultarVouchers(pesquisaVoucher);
            foreach (ImunneVacinas.Voucher item in listaVouchers)
            {
                if (item.Situacao != ImunneVacinas.Voucher.SituacaoVoucher.Utilizado)
                {
                    item.DataRetirada = DateTime.Now;
                    item.Situacao = ImunneVacinas.Voucher.SituacaoVoucher.Utilizado;
                    item.Alterar();

                    totalDoses += 1;
                }
            }

            #region BAIXA EM ESTOQUE

            if (totalDoses > 0)
            {
                ImunneVacinas.PesquisaItemCompra pesquisaItem = new ImunneVacinas.PesquisaItemCompra()
                {
                    Campanha = campanhaSelecionada.Codigo,
                    Compra = compraSelecionada.Codigo,
                };

                List<ImunneVacinas.ItemCompra> listaItens = ImunneVacinas.ItemCompra.ConsultarItens(pesquisaItem);
                foreach (ImunneVacinas.ItemCompra item in listaItens)
                {
                    ImunneVacinas.MovimentacaoEstoque novaMovimentacao = new ImunneVacinas.MovimentacaoEstoque();
                    novaMovimentacao.Tipo = ImunneVacinas.Utils.TipoMovimentacao.Saida;

                    novaMovimentacao.Usuario.Codigo = this.Page.ConsultarSessionCodigoUsuario();
                    if (novaMovimentacao.Usuario.Codigo <= 0)
                        novaMovimentacao.Usuario.Codigo = 14;

                    novaMovimentacao.Clinica.Codigo = compraSelecionada.IdClinica.Codigo;
                    novaMovimentacao.DataCriacao = DateTime.Now;
                    novaMovimentacao.Quantidade = totalDoses;
                    novaMovimentacao.Produto.Codigo = item.IdVacina.Codigo;
                    novaMovimentacao.Historico = (String.Format("{0} DOS(ES) APLICADA(S) NA CAMPANHA {1}, PRODUTO {3}, COMPRADOR {2}.", totalDoses, campanhaSelecionada.Identificacao, participanteSelecionado.Nome, item.IdVacina.Nome)).ToUpper();

                    novaMovimentacao.Incluir();
                }

                totalRetornos = totalDoses;
            }

            #endregion
        }
        catch (Exception ex)
        {
            totalRetornos = 0;
            this.SalvarException(ex);
        }

        return totalRetornos;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void DdlClinicas_SelectedIndexChanged(object sender, EventArgs e)
    {
        int codigoClinica = Convert.ToInt32(DdlClinicas.SelectedValue);
        int codigoCampanha = Convert.ToInt32(DdlCampanhas.SelectedValue);

        DdlCampanhas.Items.Clear();

        if (codigoClinica > 0)
        {
            ImunneVacinas.PesquisaCampanhaClinica pesquisa = new ImunneVacinas.PesquisaCampanhaClinica()
            {
                Clinica = codigoClinica,
                Ordenacao = ImunneVacinas.Utils.TipoOrdenacao.porNome
            };

            DdlCampanhas.DataSource = ImunneVacinas.CampanhaClinica.ConsultarCampanhasComboBox(pesquisa, true, "CAMPANHAS");
            DdlCampanhas.DataBind();
            DdlCampanhas.Enabled = true;
        }
        else
        {
            DdlCampanhas.DataSource = ImunneVacinas.Campanha.ConsultarComboBox(true, "CAMPANHAS");
            DdlCampanhas.DataBind();
        }

        DdlCampanhas.SelectedValue = codigoCampanha.ToString();

        itensBaixa = new List<string>();

        PaginaAtual = 0;
        VincularItens();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void DdlCampanhas_SelectedIndexChanged(object sender, EventArgs e)
    {
        itensBaixa = new List<string>();

        PaginaAtual = 0;
        VincularItens();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void RptPrincipal_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        int codigoRegistro = Convert.ToInt32(e.CommandArgument);
        string comando = e.CommandName;

        ImunneVacinas.Compra registroSelecionado = ImunneVacinas.Compra.ConsultarUnico(codigoRegistro);

        switch (comando)
        {
            case "baixar":
                {
                    url = new StringBuilder();
                    parametros = new StringBuilder();

                    parametros.AppendFormat("acao:{0}|", "baixar");
                    parametros.AppendFormat("codigo:{0}|", codigoRegistro);
                    parametros.AppendFormat("clinica:{0}|", DdlClinicas.SelectedValue);
                    parametros.AppendFormat("campanha:{0}|", DdlCampanhas.SelectedValue);
                    parametros.AppendFormat("categoria:{0}|", DdlCategorias.SelectedValue);
                    parametros.AppendFormat("situacao:{0}|", DdlSituacoesCompras.SelectedValue);
                    parametros.AppendFormat("aplicacao:{0}|", DdlUtilizacoes.SelectedValue);
                    parametros.AppendFormat("pesquisa:{0}|", TxtPesquisa.Text);
                    parametros.AppendFormat("iniciodata:{0}|", HdfInicio.Value);
                    parametros.AppendFormat("terminodata:{0}|", HdfFinal.Value);
                    parametros.AppendFormat("itens:{0}|", DdlItens.SelectedValue);
                    parametros.AppendFormat("current_page:{0}", PaginaAtual);

                    url.AppendFormat("~/admin/baixar-vales.aspx?compra={0}", ImunneVacinas.Criptografia.Criptografar(parametros.ToString()));

                    Response.Redirect(url.ToString(), false);
                    Context.ApplicationInstance.CompleteRequest();
                }
                break;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BtnExportar_Click(object sender, EventArgs e)
    {
        List<ImunneVacinas.Compra> listaCompras = ConsultarRegistros();

        if (listaCompras.Count > 0)
            ImunneVacinas.ExportacoesExcel.ExportarCompras(listaCompras);
        else
        {
            ImunneVacinas.Alerta.showMensagemAviso(this.Page, "Aviso", "Sua pesquisa não retornou nenhum resultado para exportação.");
            return;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BtnExportarItens_Click(object sender, EventArgs e)
    {
        List<ImunneVacinas.Compra> listaCompras = ConsultarRegistros();

        if (listaCompras.Count > 0)
            ImunneVacinas.ExportacoesExcel.ExportarItensCompras(listaCompras);
        else
        {
            ImunneVacinas.Alerta.showMensagemAviso(this.Page, "Aviso", "Sua pesquisa não retornou nenhum resultado para exportação.");
            return;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ChkUtilizacao_CheckedChanged(object sender, EventArgs e)
    {
        GerenciarSelecoes();
    }

    /// <summary>
    /// 
    /// </summary>
    private void GerenciarSelecoes()
    {
        // ADIÇÃO
        foreach (RepeaterItem item in RptPrincipal.Items)
        {
            CheckBox ChkUtilizacao = (CheckBox)item.FindControl("ChkUtilizacao");
            HiddenField HdfCodigoCompra = (HiddenField)item.FindControl("HdfCodigoCompra");

            if (ChkUtilizacao.Enabled == true && ChkUtilizacao.Checked == true)
            {
                int verificadorExistencia = 0;

                foreach (string opcao in itensBaixa)
                {
                    if (opcao == HdfCodigoCompra.Value)
                    {
                        verificadorExistencia = 1;
                        break;
                    }
                }

                if (verificadorExistencia == 0)
                    itensBaixa.Add(HdfCodigoCompra.Value);
            }
        }

        // REMOÇÃO
        List<string> itensTemp = itensBaixa;

        foreach (RepeaterItem item in RptPrincipal.Items)
        {
            CheckBox ChkUtilizacao = (CheckBox)item.FindControl("ChkUtilizacao");
            HiddenField HdfCodigoCompra = (HiddenField)item.FindControl("HdfCodigoCompra");

            if (ChkUtilizacao.Enabled == true && ChkUtilizacao.Checked == false)
            {
                foreach (string opcao in itensBaixa)
                {
                    if (opcao == HdfCodigoCompra.Value)
                    {
                        itensTemp.Remove(opcao);
                        break;
                    }
                }
            }
        }

        // CONFIRMAÇÃO
        //itensBaixa.Clear();
        itensBaixa = itensTemp;
    }

    #region Paginação

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void DdlPaginas_SelectedIndexChanged(object sender, EventArgs e)
    {
        PaginaAtual = Convert.ToInt32(DdlPaginas.SelectedValue);
        VincularItens();
    }

    #endregion
}