﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="edit-locais-adesao.aspx.cs" Inherits="admin_edit_locais_adesao" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js" type="text/javascript"></script>
    <link href="../vendors/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <script src="../vendors/global/jquery.min.js"></script>
    <script type="text/javascript" src="../vendors/select2/js/select2.full.min.js"></script>
    <script type="text/javascript">
        var select$ = jQuery.noConflict();
        select$(function () {
            //Initialize Select2 Elements
            select$('.select2').select2()
        })
    </script>
    <script type="text/javascript">
        function validar() {
            // Empresa
            if (document.getElementById("<%=DdlEmpresas.ClientID%>").value == "-1") {
                MensagemGenerica("Aviso!", "Por favor, informe a empresa a quem pertence este local.", "warning");
                return false;
            }

            // Campanha
            if (document.getElementById("<%=DdlCampanhas.ClientID%>").value == "-1") {
                MensagemGenerica("Aviso!", "Por favor, informe a campanha a quem pertence este local.", "warning");
                return false;
            }

            // Nome
            if (document.getElementById("<%=TxtNome.ClientID%>").value == "") {
                MensagemGenerica("Aviso!", "Por favor, informe o nome do local.", "warning");
                return false;
            }

            // Endereço
            if (document.getElementById("<%=TxtEndereco.ClientID%>").value == "") {
                MensagemGenerica("Aviso!", "Por favor, informe o endereço do local.", "warning");
                return false;
            }
        };
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <ul class="breadcrumb">
        <li><a href="dashboard.aspx"><i class="fa fa-home"></i>&nbsp;Home</a> </li>
        <li>
            <asp:HyperLink ID="LnkCampanhas" runat="server" Text="Campanhas"></asp:HyperLink>
        </li>
        <li>
            <asp:HyperLink ID="LnkLocais" runat="server" Text="Locais p/ Adesões"></asp:HyperLink>
        </li>
        <li class="active">Inserção/Edição</li>
    </ul>
    <h3><i class="fa fa-map-marker"></i>&nbsp;<asp:Label ID="LblForms" runat="server"></asp:Label></h3>
    <asp:UpdatePanel ID="UpdMain" runat="server">
        <ContentTemplate>
            <script type="text/javascript">
                Sys.Application.add_load(SetSelectBox);
                var select$ = jQuery.noConflict();
                function SetSelectBox() {
                    select$('.select2').select2();
                }
            </script>
            <div class="box box-success">
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                            <label>Empresa *</label>
                            <asp:DropDownList ID="DdlEmpresas" runat="server" CssClass="form-control select2" DataValueField="ValueMember" DataTextField="DisplayMember" Enabled="false"></asp:DropDownList>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                            <label>Campanhas *</label>
                            <asp:DropDownList ID="DdlCampanhas" runat="server" CssClass="form-control select2" DataValueField="ValueMember" DataTextField="DisplayMember" Enabled="false"></asp:DropDownList>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                            <label>Nome *</label>
                            <asp:TextBox ID="TxtNome" runat="server" CssClass="form-control text-uppercase"></asp:TextBox>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                            <label>Endereço *</label>
                            <asp:TextBox ID="TxtEndereco" runat="server" CssClass="form-control text-uppercase"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
            <div class="fixed-button-box">
                <div class="text-right">
                    <asp:LinkButton ID="BtnSalvar" runat="server" CssClass="btn btn-success btn-lg"
                        data-toggle="tooltip" data-container="body" ToolTip="Salvar informações"
                        ValidationGroup="edicao" OnClientClick="return validar();" OnClick="BtnSalvar_Click">
                    <i class="fa fa-save"></i>
                    </asp:LinkButton>
                    &nbsp;
                <asp:LinkButton ID="BtnCancelar" runat="server"
                    CssClass="btn btn-danger btn-lg pull-right" data-toggle="tooltip" data-container="body" ToolTip="Cancelar operação"
                    Text="Cancelar" CausesValidation="false">
                    <i class="fa fa-times"></i>
                </asp:LinkButton>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
