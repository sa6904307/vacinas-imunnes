﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="estoque.aspx.cs" Inherits="admin_estoque" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js" type="text/javascript"></script>
    <link href="../vendors/datepicker/css/datepicker3.css" rel="stylesheet" type="text/css" />
    <link href="../vendors/daterangepicker/css/daterangepicker-bs3.css" rel="stylesheet"
        type="text/css" />
    <link href="../vendors/timeticker/css/bootstrap-timepicker.min.css" rel="stylesheet"
        type="text/css" />
    <script src="../vendors/daterangepicker/js/daterangepicker.js" type="text/javascript"></script>
    <script src="../vendors/datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
    <script src="../vendors/timeticker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
    <script src="../vendors/datepicker/locales/bootstrap-datepicker.pt-BR.js" type="text/javascript"></script>
    <link href="../vendors/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <script src="../vendors/global/jquery.min.js"></script>
    <script type="text/javascript" src="../vendors/select2/js/select2.full.min.js"></script>
    <script type="text/javascript">
        var select$ = jQuery.noConflict();
        select$(function () {
            //Initialize Select2 Elements
            select$('.select2').select2()
        })
    </script>
    <script type="text/javascript">
        function validar() {
            // Tipo de entrada
            if (document.getElementById("<%=DdlTiposForm.ClientID%>").value == "-1") {
                MensagemGenerica("Aviso!", "Por favor, informe o tipo da entrada.", "warning");
                return false;
            }

            // Quantidade de entrada
            if (document.getElementById("<%=txtQtdeForm.ClientID%>").value == "") {
                MensagemGenerica("Aviso!", "Por favor, informe a quantidade do evento.", "warning");
                return false;
            }

            // Data de entrada
            if (document.getElementById("<%=TxtDataForm.ClientID%>").value == "") {
                MensagemGenerica("Aviso!", "Por favor, informe a data do evento.", "warning");
                return false;
            }

            // Histórico
            if (document.getElementById("<%=TxtHistoricoForm.ClientID%>").value == "") {
                MensagemGenerica("Aviso!", "Por favor, informe um breve resumo sobre o evento.", "warning");
                return false;
            }
        };
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="UpdMain" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <%--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>--%>
            <script type="text/javascript">
                function openFormulario() {
                    $("#MdlFormulario").modal('show');
                }
            </script>
            <script type="text/javascript">
                Sys.Application.add_load(SetDatePicker);
                function SetDatePicker() {
                    $('.datepicker').datepicker({
                        todayHighlight: true,
                        language: "pt-BR"
                    });
                }
            </script>
            <script type="text/javascript">
                Sys.Application.add_load(SetSelectBox);
                var select$ = jQuery.noConflict();
                function SetSelectBox() {
                    select$('.select2').select2();
                }
            </script>
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumb">
                        <li><a href="dashboard.aspx"><i class="fa fa-home"></i>&nbsp;Home</a></li>
                        <li>
                            <asp:HyperLink ID="LnkClinicias" runat="server" Text="Clínicas"></asp:HyperLink>
                        </li>
                        <li class="active">Estoques</li>
                    </ul>
                    <h1 class="conteudo-centro"><i class="fa fa-cubes"></i>&nbsp;Estoques</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <asp:Panel ID="PnlPesquisar" runat="server" DefaultButton="LnkPesquisar">
                        <div class="box box-success">
                            <div class="box-header with-border">
                                <h3 class="box-title"><i class="fa fa-sliders"></i>&nbsp;Filtros</h3>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" data-container="body" title="Ocultar/exibir filtros">
                                        <i class="fa fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="box-body" style="display: block;">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                        <asp:DropDownList ID="DdlClinicas" runat="server" CssClass="form-control text-uppercase select2" DataValueField="ValueMember" DataTextField="DisplayMember"></asp:DropDownList>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 form-group">
                                        <asp:DropDownList ID="DdlProdutos" runat="server" CssClass="form-control text-uppercase select2" DataValueField="ValueMember" DataTextField="DisplayMember"></asp:DropDownList>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 form-group">
                                        <asp:DropDownList ID="DdlTipos" runat="server" CssClass="form-control text-uppercase select2" DataValueField="ValueMember" DataTextField="DisplayMember"></asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer">
                                <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 pull-right">
                                    <asp:LinkButton ID="BtnExportar" runat="server" CssClass="btn btn-block btn-success" data-toggle="tooltip" data-container="body" ToolTip="Exportar registros" CausesValidation="false" OnClick="BtnExportar_Click">
                                        <i class="fa fa-download"></i>&nbsp;Exportar
                                    </asp:LinkButton>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 pull-right">
                                    <asp:LinkButton ID="LnkPesquisar" runat="server" CssClass="btn btn-block btn-success" OnClick="LnkPesquisar_Click">
                                    <i class="fa fa-search"></i>&nbsp;Pesquisar
                                    </asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 text-left bloco-legenda">
                                    <a href="#" class="dropdown-toggle btn btn-block btn-primary hidden" data-toggle="dropdown">
                                        <i class="fa fa-info-circle"></i>&nbsp;Legenda
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <div style="padding: 8px;">
                                                <i class="fa fa-eye text-regular"></i>&nbsp;Visualizar registro
                                            </div>
                                        </li>
                                    </ul>
                                    <div class="dv-spc-10"></div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10 box-resultados text-right conteudo-centro">
                                    <asp:Label ID="LblRegistros" runat="server"></asp:Label>
                                </div>
                            </div>
                            <div class="dv-spc-10"></div>
                            <asp:UpdatePanel ID="UpdGrid" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div class="box-grids">
                                        <div class="dv-grid-f-header">
                                            <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center hidden" style="display: inline-block; float: none;">
                                                Ações
                                            </div>
                                            <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="display: inline-block; float: none;">
                                                Tipo
                                            </div>
                                            <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="display: inline-block; float: none;">
                                                Data
                                            </div>
                                            <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="display: inline-block; float: none;">
                                                Qtde
                                            </div>
                                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="display: inline-block; float: none;">
                                                Produto
                                            </div>
                                            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" style="display: inline-block; float: none;">
                                                Usuário
                                            </div>
                                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="display: inline-block; float: none;">
                                                Histórico
                                            </div>
                                        </div>
                                        <div class="dv-grid-f">
                                            <asp:Repeater ID="RptPrincipal" runat="server" OnItemDataBound="RptPrincipal_ItemDataBound" OnItemCommand="RptPrincipal_ItemCommand">
                                                <ItemTemplate>
                                                    <div id="dvItem" runat="server">
                                                        <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center hidden" style="display: inline-block; float: none;">
                                                            <div class="truncate">
                                                                <asp:LinkButton ID="LnkEditList" runat="server" data-toggle="tooltip" data-container="body" ToolTip="Visualizar registro" CommandName="edit">
                                                        <i class="fa fa-eye text-regular"></i>
                                                                </asp:LinkButton>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="display: inline-block; float: none;">
                                                            <div class="truncate">
                                                                <asp:Label ID="LblTipo" runat="server" CssClass="text-uppercase"></asp:Label>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="display: inline-block; float: none;">
                                                            <div class="truncate">
                                                                <asp:Label ID="LblData" runat="server" CssClass="text-uppercase"></asp:Label>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="display: inline-block; float: none;">
                                                            <div class="truncate">
                                                                <asp:Label ID="LblQtde" runat="server" CssClass="text-uppercase"></asp:Label>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="display: inline-block; float: none;">
                                                            <div class="truncate">
                                                                <asp:Label ID="LblProduto" runat="server" CssClass="text-uppercase"></asp:Label>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" style="display: inline-block; float: none;">
                                                            <div class="truncate">
                                                                <asp:Label ID="LblUsuario" runat="server" CssClass="text-uppercase"></asp:Label>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="display: inline-block; float: none;">
                                                            <div class="truncate">
                                                                <asp:Label ID="LblHistorico" runat="server" CssClass="text-uppercase"></asp:Label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </div>
                                    </div>
                                    <div class="dv-spc-10"></div>
                                    <div class="box-footer">
                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 form-group">
                                            <label>Total entradas (+)</label>
                                            <asp:TextBox ID="TxtTotalEntradas" runat="server" Enabled="false" CssClass="form-control text-right text-blue text-bold" Text="0" Font-Size="18"></asp:TextBox>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 form-group">
                                            <label>Total saídas (-)</label>
                                            <asp:TextBox ID="TxtTotalSaidas" runat="server" Enabled="false" CssClass="form-control text-right text-red text-bold" Text="0" Font-Size="18"></asp:TextBox>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 form-group">
                                            <label>Saldo final</label>
                                            <asp:TextBox ID="TxtSaldoFinal" runat="server" Enabled="false" CssClass="form-control text-right text-blue text-bold" Text="0" Font-Size="18"></asp:TextBox>
                                        </div>
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="BtnExportar" />
                                    <asp:AsyncPostBackTrigger ControlID="LnkPesquisar" />
                                    <asp:AsyncPostBackTrigger ControlID="RptPrincipal" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
            <asp:HiddenField ID="HdfCodigo" runat="server" />
            <!-- MODAL DE DADOS DO EVENTO -->
            <div class="modal fade" id="MdlFormulario">
                <div class="modal-dialog" role="document">
                    <div class="modal-content" style="min-height: 360px; height: auto;">
                        <asp:UpdatePanel ID="UpdFormulario" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <script type="text/javascript">
                                    Sys.Application.add_load(SetDatePicker);
                                    function SetDatePicker() {
                                        $('.datepicker').datepicker({
                                            todayHighlight: true,
                                            language: "pt-BR"
                                        });
                                    }
                                </script>
                                <div class="modal-header titulo-modal conteudo-centro">
                                    <button class="close" data-dismiss="modal">×</button>
                                    <h3>
                                        <asp:Label ID="LblTituloFormulario" runat="server"></asp:Label>
                                    </h3>
                                </div>
                                <div class="modal-body" style="padding: 0px 15px 0px 15px !important; min-height: 360px; height: auto;">
                                    <div class="embed-responsive" style="min-height: 360px; height: auto;">
                                        <br />
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                            <label>Produtos</label>
                                            <asp:DropDownList ID="DdlProdutosForm" runat="server" style="width: 100%;" CssClass="form-control text-uppercase select2" DataValueField="ValueMember" DataTextField="DisplayMember"></asp:DropDownList>
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                                            <label>Tipo *</label>
                                            <asp:DropDownList ID="DdlTiposForm" runat="server" style="width: 100%;" CssClass="form-control text-uppercase select2" DataValueField="ValueMember" DataTextField="DisplayMember"></asp:DropDownList>
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 form-group">
                                            <label>Quantidade *</label>
                                            <asp:TextBox ID="txtQtdeForm" runat="server" CssClass="form-control text-uppercase text-center" OnKeyPress="return SomenteNumero(event)"></asp:TextBox>
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 form-group">
                                            <label>Data *</label>
                                            <asp:TextBox ID="TxtDataForm" runat="server" CssClass="form-control text-uppercase text-center datepicker" data-date-format="dd/mm/yyyy" OnKeyPress="mascara(this, mdata)" MaxLength="10"></asp:TextBox>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                            <label>Histórico *</label>
                                            <asp:TextBox ID="TxtHistoricoForm" runat="server" CssClass="form-control text-uppercase" TextMode="MultiLine" Height="150px"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 pull-right">
                                        <button type="button" class="btn btn-block btn-danger" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i>&nbsp;Fechar</button>
                                    </div>
                                    <div id="DvSalvar" runat="server" class="col-xs-6 col-sm-6 col-md-3 col-lg-3 pull-right">
                                        <asp:LinkButton ID="LnkSalvar" runat="server" CssClass="btn btn-block btn-success" data-toggle="tooltip" ToolTip="Salvar registro" CausesValidation="false" OnClientClick="return validar();" OnClick="LnkSalvar_Click">
                                    <i class="fa fa-check" aria-hidden="true"></i>&nbsp;Salvar
                                        </asp:LinkButton>
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="LnkPesquisar" />
                                <asp:PostBackTrigger ControlID="BtnExportar" />
                                <asp:AsyncPostBackTrigger ControlID="RptPrincipal" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
            <div class="fixed-button-box">
                <asp:LinkButton ID="BtnNovo" runat="server" CssClass="btn btn-success btn-lg" CausesValidation="False" OnClick="BtnNovo_Click" data-toggle="tooltip">
                    <i class="fa fa-plus"></i>
                </asp:LinkButton>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="BtnExportar" />
            <asp:AsyncPostBackTrigger ControlID="LnkPesquisar" />
            <asp:AsyncPostBackTrigger ControlID="RptPrincipal" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
