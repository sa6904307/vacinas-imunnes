﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_edit_links : System.Web.UI.Page
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        LblForms.Text = ImunneVacinas.Utils.MontarTituloFormulario("insert");
        Page.Title = "S&A Imunizações | " + LblForms.Text;

        if (!IsPostBack)
            IniciarPagina();
    }

    /// <summary>
    /// 
    /// </summary>
    private void IniciarPagina()
    {
        ImunneVacinas.Alerta.showMensagemAviso(this.Page, "Aviso!", "O link da campanha será gerado automaticamente e estará visível na listagem.");

        int codigoClinica = -1;
        int codigoCampanha = -1;

        try
        {
            NameValueCollection clinica = new NameValueCollection();
            NameValueCollection campanha = new NameValueCollection();
            NameValueCollection link = new NameValueCollection();

            campanha = ImunneVacinas.Utils.RecuperarQueryString(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["campanha"]));
            clinica = ImunneVacinas.Utils.RecuperarQueryString(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["clinica"]));
            link = ImunneVacinas.Utils.RecuperarQueryString(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["link"]));

            codigoClinica = Convert.ToInt32(campanha["codigo"]);
            codigoClinica = Convert.ToInt32(campanha["clinica"]);

            DdlClinicas.DataSource = ImunneVacinas.Clinica.ConsultarComboBox(false, "");
            DdlClinicas.DataBind();
            DdlClinicas.SelectedValue = codigoClinica.ToString();

            DdlCampanhas.DataSource = ImunneVacinas.Campanha.ConsultarComboBox(false, "");
            DdlCampanhas.DataBind();
            DdlCampanhas.SelectedValue = codigoCampanha.ToString();

            for (int i = 0; i <= 59; i++)
            {
                DdlMinutos.Items.Add(new ListItem(i.ToString().PadLeft(2, '0'), i.ToString()));
            }

            for (int i = 0; i <= 23; i++)
            {
                DdlHoras.Items.Add(new ListItem(i.ToString().PadLeft(2, '0'), i.ToString()));
            }

            DdlHoras.SelectedValue = DateTime.Now.Hour.ToString();
            DdlMinutos.SelectedValue = DateTime.Now.Minute.ToString();
        }
        catch (Exception ex)
        {

        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BtnSalvar_Click(object sender, EventArgs e)
    {

    }
}