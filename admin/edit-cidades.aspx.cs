﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_edit_cidades : System.Web.UI.Page
{
    private StringBuilder url = new StringBuilder();
    private string textoMensagem = String.Empty;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            IniciarPagina();
    }

    /// <summary>
    /// 
    /// </summary>
    private void IniciarPagina()
    {
        NameValueCollection cidade = new NameValueCollection();
        int codigoRegistro = -1;
        string comando = String.Empty;

        if (Request.QueryString["cidade"] != null)
        {
            cidade = ImunneVacinas.Utils.RecuperarQueryString(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["cidade"]));

            url = new StringBuilder();
            url.AppendFormat("~/admin/cidades.aspx?cidade={0}", Request.QueryString["cidade"]);
            LnkPai.NavigateUrl = url.ToString();
            BtnCancelar.OnClientClick = "return CancelarFormulario(this, event,'" + url.Replace("~/admin/", "") + "');";

            codigoRegistro = Convert.ToInt32(cidade["codigo"]);
            comando = cidade["acao"].ToString();

            DdlUfs.DataSource = ImunneVacinas.Uf.ConsultarComboBox(true, "UF");
            DdlUfs.DataBind();

            PopularCampos();

            LblForms.Text = ImunneVacinas.Utils.MontarTituloFormulario(comando);

            switch (comando)
            {
                case "edit":
                    {
                        ConsultarDetalhes(codigoRegistro);
                    }
                    break;
            }

            Page.Title = "S&A Imunizações | " + LblForms.Text;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void PopularCampos()
    {
        TxtNome.MaxLength = (int)ImunneVacinas.Cidade.GetInfoNome().Max_length;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="codigo"></param>
    private void ConsultarDetalhes(int codigo)
    {
        ImunneVacinas.Cidade registroSelecionado = ImunneVacinas.Cidade.ConsultarUnico(codigo);

        TxtNome.Text = registroSelecionado.Nome;

        DdlUfs.SelectedValue = registroSelecionado.Estado;

        if (!String.IsNullOrEmpty(registroSelecionado.CepInicial) && registroSelecionado.CepInicial == "00000000")
            TxtCEPInicial.Text = "";
        else
            TxtCEPInicial.Text = registroSelecionado.CepInicial;

        if (!String.IsNullOrEmpty(registroSelecionado.CepFinal) && registroSelecionado.CepFinal == "00000000")
            TxtCEPFinal.Text = "";
        else
            TxtCEPFinal.Text = registroSelecionado.CepFinal;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BtnSalvar_Click(object sender, EventArgs e)
    {
        try
        {
            NameValueCollection cidade = new NameValueCollection();

            cidade = ImunneVacinas.Utils.RecuperarQueryString(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["cidade"]));

            int codigoRegistro = ImunneVacinas.Parser.parseInteiro(cidade["codigo"]);
            string comando = cidade["acao"].ToString();

            ImunneVacinas.Cidade novoRegistro;
            ImunneVacinas.Cidade registroOriginal = new ImunneVacinas.Cidade();
            if (codigoRegistro > 0)
            {
                novoRegistro = ImunneVacinas.Cidade.ConsultarUnico(codigoRegistro);
                registroOriginal = ImunneVacinas.Cidade.ConsultarUnico(codigoRegistro);
            }
            else
                novoRegistro = new ImunneVacinas.Cidade();

            novoRegistro.Estado = DdlUfs.SelectedValue;
            novoRegistro.Nome = TxtNome.Text;
            novoRegistro.CepInicial = TxtCEPInicial.Text;
            novoRegistro.CepFinal = TxtCEPFinal.Text;

            int resultado = 0;

            switch (comando)
            {
                case "insert":
                    {
                        resultado = novoRegistro.Incluir();
                    }
                    break;
                case "edit":
                    {
                        ImunneVacinas.Cidade dadosAtuais = ImunneVacinas.Cidade.ConsultarUnico(codigoRegistro);
                        novoRegistro.Codigo = Convert.ToInt32(codigoRegistro);
                        resultado = novoRegistro.Alterar();
                    }
                    break;
            }

            if (resultado <= 0)
            {
                textoMensagem = "Lamentamos mas ocorreu um erro e não foi possível concluir a operação. Tente novamente ou informe o suporte.";
                ImunneVacinas.Alerta.showMensagemErro(this.Page, null, textoMensagem);
                return;
            }

            #region Geração de Logs

            // Log descritivo
            ImunneVacinas.Log logAcao = new ImunneVacinas.Log();
            logAcao.Tela = Path.GetFileName(Request.PhysicalPath);
            //logAcao.IdCidade.Codigo = this.Page.ConsultarSessionCodigoCidade();
            logAcao.TabelaAcao = "Cidades";

            switch (comando)
            {
                case "insert":
                    {
                        logAcao.TipoAcao = ImunneVacinas.Utils.AcaoLog.Insercao;
                        logAcao.Registro = resultado;
                        logAcao.Resumo = String.Format("INSERÇÃO DE NOVO REGISTRO {0}.", logAcao.Registro);
                        logAcao.DataAcao = DateTime.Now;
                        logAcao.Campo = String.Empty;
                        logAcao.ValorOriginal = String.Empty;
                        logAcao.ValorNovo = String.Empty;
                        logAcao.Incluir();
                    }
                    break;
                case "edit":
                    {
                        logAcao.TipoAcao = ImunneVacinas.Utils.AcaoLog.Edicao;
                        //ImunneVacinas.RegistraLog.CompararCidade(logAcao, registroOriginal, novoRegistro);
                    }
                    break;
            }

            #endregion

            url = new StringBuilder();
            url.AppendFormat("~/admin/cidades.aspx?cidade={0}", Request.QueryString["cidade"]);
            Response.Redirect(url.ToString(), false);
            Context.ApplicationInstance.CompleteRequest();
        }
        catch (Exception ex)
        {
            textoMensagem = "Lamentamos mas ocorreu um erro e não foi possível concluir a operação. Tente novamente ou informe o suporte.";
            ImunneVacinas.Alerta.showMensagemErro(this.Page, null, textoMensagem);
            this.SalvarException(ex);
        }
    }
}