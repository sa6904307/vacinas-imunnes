﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="edit-usuarios.aspx.cs" Inherits="edit_usuarios" MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js" type="text/javascript"></script>
    <link href="../vendors/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <script src="../vendors/global/jquery.min.js"></script>
    <script type="text/javascript" src="../vendors/select2/js/select2.full.min.js"></script>
    <script type="text/javascript">
        var select$ = jQuery.noConflict();
        select$(function () {
            //Initialize Select2 Elements
            select$('.select2').select2()
        })
    </script>
    <script type="text/javascript">
        function validar() {
            var hiddenStatusFlag = document.getElementById('<%=HdfAcao.ClientID%>');

            // Clínica
            if (document.getElementById("<%=DdlTipos.ClientID%>").value == "7") {
                if (document.getElementById("<%=DdlClinicas.ClientID%>").value == "-1") {
                    MensagemGenerica("Aviso!", "Por favor, selecione a clínica do usuário.", "warning");
                    return false;
                }
            }

            // Tipo
            if (document.getElementById("<%=DdlTipos.ClientID%>").value == "-1") {
                MensagemGenerica("Aviso!", "Por favor, informe o tipo do usuário.", "warning");
                return false;
            }

            // Nome
            if (document.getElementById("<%=TxtNome.ClientID%>").value == "") {
                MensagemGenerica("Aviso!", "Por favor, informe o nome do usuário.", "warning");
                return false;
            }

            //Email
            if (document.getElementById("<%=TxtEmail.ClientID %>").value == "") {
                MensagemGenerica("Aviso!", "Por favor, informe o e-mail do usuário.", "warning");
                return false;
            }
            var emailid = document.getElementById("<%=TxtEmail.ClientID %>").value;
            var checkTLD = 1;
            var knownDomsPat = /^(com|net|org|edu|int|mil|gov|arpa|biz|aero|name|coop|info|pro|museum)$/;
            var emailPat = /^(.+)@(.+)$/;
            var specialChars = "\\(\\)><@,;:\\\\\\\"\\.\\[\\]";
            var validChars = "\[^\\s" + specialChars + "\]";
            var quotedUser = "(\"[^\"]*\")";
            var ipDomainPat = /^\[(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})\]$/;
            var atom = validChars + '+';

            var word = "(" + atom + "|" + quotedUser + ")";
            var userPat = new RegExp("^" + word + "(\\." + word + ")*$");
            var domainPat = new RegExp("^" + atom + "(\\." + atom + ")*$");
            var matchArray = emailid.match(emailPat);
            if (matchArray == null) {
                MensagemGenerica("Aviso!", "Por favor, informe um e-mail válido.", "warning");
                return false;
            }
        };
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <ul class="breadcrumb">
        <li><a href="dashboard.aspx"><i class="fa fa-home"></i>&nbsp;Home</a> </li>
        <li>
            <asp:HyperLink ID="LnkPai" runat="server" Text="Usuários"></asp:HyperLink>
        </li>
        <li class="active">Inserção/Edição</li>
    </ul>
    <h3><i class="fa fa-users"></i>&nbsp;<asp:Label ID="LblForms" runat="server"></asp:Label></h3>
    <asp:UpdatePanel ID="UpdMain" runat="server">
        <ContentTemplate>
            <script type="text/javascript">
                Sys.Application.add_load(SetSelectBox);
                var select$ = jQuery.noConflict();
                function SetSelectBox() {
                    select$('.select2').select2();
                }
            </script>
            <div class="box box-success">
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-group">
                            <label>Tipo *</label>
                            <asp:DropDownList ID="DdlTipos" runat="server" CssClass="form-control text-uppercase select2" DataValueField="ValueMember" AutoPostBack="true" DataTextField="DisplayMember" OnSelectedIndexChanged="DdlTipos_SelectedIndexChanged"></asp:DropDownList>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9 form-group">
                            <label>Nome *</label>
                            <asp:HiddenField ID="HdfAcao" runat="server" />
                            <asp:TextBox ID="TxtNome" runat="server" CssClass="form-control text-uppercase"></asp:TextBox>
                        </div>
                        <asp:UpdatePanel ID="UpdClinicas" runat="server">
                            <ContentTemplate>
                                <asp:Panel ID="PnlGrupoClinicas" runat="server" Visible="false">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                        <label>Clínica *</label>
                                        <asp:DropDownList ID="DdlClinicas" runat="server" CssClass="form-control text-uppercase select2" DataValueField="ValueMember" DataTextField="DisplayMember"></asp:DropDownList>
                                    </div>
                                </asp:Panel>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="DdlTipos" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 form-group">
                            <label>Login *</label>
                            <asp:TextBox ID="TxtLogin" runat="server" CssClass="form-control text-lowercase"></asp:TextBox>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 form-group">
                            <label>E-mail *</label>
                            <asp:TextBox ID="TxtEmail" runat="server" CssClass="form-control text-lowercase"></asp:TextBox>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-group">
                            <label>Senha *</label>
                            <asp:TextBox ID="TxtSenha" runat="server" CssClass="form-control text-lowercase" TextMode="Password"></asp:TextBox>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-group">
                            <label>Confirmar *</label>
                            <asp:TextBox ID="TxtConfirmar" runat="server" CssClass="form-control text-lowercase" TextMode="Password"></asp:TextBox>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 form-group">
                            <label>Dica</label>
                            <asp:TextBox ID="TxtDica" runat="server" CssClass="form-control text-uppercase"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
            <div class="fixed-button-box">
                <div class="text-right">
                    <asp:LinkButton ID="BtnSalvar" runat="server" CssClass="btn btn-success btn-lg"
                        data-toggle="tooltip" data-container="body" ToolTip="Salvar informações"
                        ValidationGroup="edicao" OnClientClick="return validar();" OnClick="BtnSalvar_Click">
                    <i class="fa fa-save"></i>
                    </asp:LinkButton>
                    &nbsp;
                <asp:LinkButton ID="BtnCancelar" runat="server"
                    CssClass="btn btn-danger btn-lg pull-right" data-toggle="tooltip" data-container="body" ToolTip="Cancelar operação"
                    Text="Cancelar" CausesValidation="false">
                    <i class="fa fa-times"></i>
                </asp:LinkButton>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
