﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="baixar-vales.aspx.cs" Inherits="admin_baixar_vales" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        // Chamada de alerta para confirmação de exclusão
        function ConfirmarConclusao(ctl, event, id) {
            var defaultAction = $(ctl).prop("href");
            event.preventDefault();
            swal({
                title: "Deseja concluir a operação?",
                text: 'Os vales marcados como utilizados não poderão mais ser editados.',
                type: 'question',
                showCancelButton: true,
                confirmButtonText: "<i class='fa fa-check'></i>&nbsp;Sim!",
                cancelButtonText: "<i class='fa fa-times'></i>&nbsp;Não!",
                confirmButtonColor: '#00a65a',
                cancelButtonColor: '#dd4b39',
                showLoaderOnConfirm: true,
                animation: 'slide-from-top',
                allowEscapeKey: false,
                allowOutsideClick: false
            }).then(result => {
                if (result.value) {
                    document.getElementById("<%=BtnSalvar.ClientID%>").click();
                    return true;
                }
            })
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row">
        <div class="col-md-12">
            <ul class="breadcrumb">
                <li><a href="dashboard.aspx"><i class="fa fa-home"></i>&nbsp;Home</a></li>
                <li>
                    <asp:HyperLink ID="LnkPai" runat="server" Text="Aplicações"></asp:HyperLink>
                </li>
                <li class="active">Baixar Aplicações</li>
            </ul>
            <h1 class="conteudo-centro"><i class="fa fa-ambulance"></i>&nbsp;Baixar Aplicações</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-success">
                <div class="box-body">
                    <div class="box-grids">
                        <div class="dv-grid-f-header">
                            <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1" style="width: 215px !important; display: inline-block; float: none;">
                                SITUAÇÃO
                            </div>
                            <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="width: 130px !important; display: inline-block; float: none;">
                                DATA
                            </div>
                            <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="width: 130px !important; display: inline-block; float: none;">
                                CAMPANHA
                            </div>
                            <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5" style="width: 760px !important; display: inline-block; float: none;">
                                BENEFICIÁRIO
                            </div>
                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="width: 765px !important; display: inline-block; float: none;">
                                ADQUIRENTE
                            </div>
                        </div>
                        <div class="dv-grid-f">
                            <asp:Repeater ID="RptPrincipal" runat="server" OnItemDataBound="RptPrincipal_ItemDataBound" OnItemCommand="RptPrincipal_ItemCommand">
                                <ItemTemplate>
                                    <div id="dvItem" runat="server">
                                        <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1" style="width: 215px !important; display: inline-block; float: none;">
                                            <div class="truncate">
                                                <asp:CheckBox ID="ChkUtilizado" runat="server" />
                                            </div>
                                        </div>
                                        <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="width: 130px !important; display: inline-block; float: none;">
                                            <div class="truncate">
                                                <asp:HiddenField ID="HdfCodigo" runat="server" />
                                                <asp:Label ID="LblData" runat="server" CssClass="text-uppercase"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="width: 130px !important; display: inline-block; float: none;">
                                            <div class="truncate">
                                                <asp:Label ID="LblCampanha" runat="server" CssClass="text-uppercase"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5" style="width: 760px !important; display: inline-block; float: none;">
                                            <div class="truncate">
                                                <asp:Label ID="LblBeneficiario" runat="server" CssClass="text-uppercase"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="width: 765px !important; display: inline-block; float: none;">
                                            <div class="truncate">
                                                <asp:Label ID="LblNome" runat="server" CssClass="text-uppercase"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>
                <div class="dv-spc-10">
                </div>
            </div>
            <div class="fixed-button-box">
                <div class="text-right">
                    <asp:LinkButton ID="BtnConcluirVale" runat="server" data-toggle="tooltip" data-container="body" ToolTip="Salvar informações" CssClass="btn btn-success btn-lg" OnClientClick="return ConfirmarConclusao(this, event)">
                        <i class="fa fa-save"></i>
                    </asp:LinkButton>
                    <asp:LinkButton ID="BtnSalvar" runat="server" CssClass=" hidden" OnClick="BtnSalvar_Click">
                    </asp:LinkButton>
                    &nbsp;
                <asp:LinkButton ID="BtnCancelar" runat="server"
                    CssClass="btn btn-danger btn-lg pull-right" data-toggle="tooltip" data-container="body" ToolTip="Cancelar operação"
                    Text="Cancelar" CausesValidation="false">
                    <i class="fa fa-times"></i>
                </asp:LinkButton>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
