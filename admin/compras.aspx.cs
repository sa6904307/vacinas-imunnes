﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class admin_compras : System.Web.UI.Page
{
    private StringBuilder url = new StringBuilder();
    private StringBuilder parametros = new StringBuilder();

    /// <summary>
    /// Lista para armazenar elementos para impressão em lote
    /// </summary>
    private List<string> itensImpressao
    {
        get
        {
            if (Session["listaItensImpressao"] == null)
                Session["listaItensImpressao"] = new List<string>();
            return (List<string>)Session["listaItensImpressao"];
        }
        set
        {
            Session["listaItensImpressao"] = value;
        }
    }

    /// <summary>
    /// Lista para armazenar elementos para exclusão em lote
    /// </summary>
    private List<string> itensExclusao
    {
        get
        {
            if (Session["listaItensExclusao"] == null)
                Session["listaItensExclusao"] = new List<string>();
            return (List<string>)Session["listaItensExclusao"];
        }
        set
        {
            Session["listaItensExclusao"] = value;
        }
    }

    #region Private Proprieties

    private int PaginaAtual
    {
        get
        {
            object objPage = ViewState["_PaginaAtual"];
            int _PaginaAtual = 0;
            if (objPage == null)
            {
                _PaginaAtual = 0;
            }
            else
            {
                _PaginaAtual = (int)objPage;
            }
            return _PaginaAtual;
        }
        set
        {
            ViewState["_PaginaAtual"] = value;
        }
    }
    private int PrimeiroIndice
    {
        get
        {

            int _PrimeiroIndice = 0;
            if (ViewState["_PrimeiroIndice"] == null)
            {
                _PrimeiroIndice = 0;
            }
            else
            {
                _PrimeiroIndice = Convert.ToInt32(ViewState["_PrimeiroIndice"]);
            }
            return _PrimeiroIndice;
        }
        set
        {
            ViewState["_PrimeiroIndice"] = value;
        }
    }
    private int UltimoIndice
    {
        get
        {

            int _UltimoIndice = 0;
            if (ViewState["_UltimoIndice"] == null)
            {
                _UltimoIndice = 0;
            }
            else
            {
                _UltimoIndice = Convert.ToInt32(ViewState["_UltimoIndice"]);
            }
            return _UltimoIndice;
        }
        set
        {
            ViewState["_UltimoIndice"] = value;
        }
    }

    #endregion

    #region PagedDataSource

    PagedDataSource _PagedDataSource = new PagedDataSource();

    #endregion

    #region Private Methods

    /// <summary>
    /// 
    /// </summary>
    private void VincularItens()
    {
        try
        {
            List<ImunneVacinas.Compra> retorno = ConsultarRegistros();

            _PagedDataSource.DataSource = retorno;
            _PagedDataSource.AllowPaging = true;
            _PagedDataSource.PageSize = Convert.ToInt32(DdlItens.SelectedValue);
            _PagedDataSource.CurrentPageIndex = PaginaAtual;
            ViewState["TotalPages"] = _PagedDataSource.PageCount;

            DdlPaginas.Items.Clear();
            for (int x = 0; x < _PagedDataSource.PageCount; x++)
            {
                DdlPaginas.Items.Add(new ListItem((x + 1).ToString(), x.ToString()));
            }

            DdlPaginas.SelectedValue = PaginaAtual.ToString();

            RptPrincipal.DataSource = _PagedDataSource;
            RptPrincipal.DataBind();
            CriarPaginacao();

            LblRegistros.Text = ImunneVacinas.Utils.MontarLinhaTotalizadora(retorno.Count);
            LblInfo.Text = "Exibindo página " + (PaginaAtual + 1) + " de " + _PagedDataSource.PageCount + ".";
        }
        catch (Exception Ex)
        {
            this.SalvarException(Ex);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void CriarPaginacao()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("PageIndex");
        dt.Columns.Add("PageText");

        PrimeiroIndice = PaginaAtual - 5;


        if (PaginaAtual > 5)
        {
            UltimoIndice = PaginaAtual + 5;
        }
        else
        {
            UltimoIndice = 10;
        }
        if (UltimoIndice > Convert.ToInt32(ViewState["TotalPages"]))
        {
            UltimoIndice = Convert.ToInt32(ViewState["TotalPages"]);
            PrimeiroIndice = UltimoIndice - 10;
        }

        if (PrimeiroIndice < 0)
        {
            PrimeiroIndice = 0;
        }

        for (int i = PrimeiroIndice; i < UltimoIndice; i++)
        {
            DataRow dr = dt.NewRow();
            dr[0] = i;
            dr[1] = i + 1;
            dt.Rows.Add(dr);
        }

        DdlPaginas.SelectedValue = PaginaAtual.ToString();
    }

    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = "S&A Imunizações | Compras";
        if (!IsPostBack)
            IniciarPagina();
    }

    /// <summary>
    /// 
    /// </summary>
    private void IniciarPagina()
    {
        itensImpressao = new List<string>();
        itensExclusao = new List<string>();

        int codigoClinica = this.Page.ConsultarClinicaUsuario();
        int codigoCampanha = -1;
        int roleUsuario = this.Page.ConsultarSessionTipoUsuario();
        int paginaAtual = 0;
        int itens = 20;
        int liberacoes = -1;

        string inicioData = DateTime.Now.ToString("dd/MM/yyyy");
        string terminoData = DateTime.Now.ToString("dd/MM/yyyy");
        string pesquisa = String.Empty;
        string categoria = "-1";
        string situacao = "3";
        string aplicacao = "-1";

        LnkMarcarTodos.Text = "<i class='fa fa-envelope'></i>&nbsp;Marcar todos";
        ChkTodos.Checked = false;

        LnkMarcarTodosLixo.Text = "<i class='fa fa-trash'></i>&nbsp;Marcar todos";
        ChkTodosLixo.Checked = false;

        try
        {
            NameValueCollection compra = new NameValueCollection();

            #region PARÂMETROS DE RETORNO DE TELA

            if (!String.IsNullOrEmpty(Request.QueryString["compra"]))
            {
                compra = ImunneVacinas.Utils.RecuperarQueryString(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["compra"]));
                inicioData = compra["iniciodata"];
                terminoData = compra["terminodata"];
                situacao = compra["situacao"];
                aplicacao = compra["aplicacao"];
                liberacoes = Convert.ToInt32(compra["liberacoes"]);
                categoria = compra["categoria"];
                pesquisa = compra["pesquisa"];
                codigoClinica = Convert.ToInt32(compra["clinica"]);
                codigoCampanha = Convert.ToInt32(compra["campanha"]);
                itens = Convert.ToInt32(compra["itens"]);
                paginaAtual = Convert.ToInt32(compra["current_page"]);
            }
            else
            {
                if (roleUsuario == 7)
                {
                    inicioData = String.Format("01/01/{0}", DateTime.Now.Year);
                    terminoData = DateTime.Now.ToString("dd/MM/yyyy");
                }
            }

            #endregion

            DdlLiberacoes.Items.Add(new ListItem("LIBERAÇÕES", "-1"));
            DdlLiberacoes.Items.Add(new ListItem("À LIBERAR", "1"));
            DdlLiberacoes.Items.Add(new ListItem("LIBERADOS", "2"));
            DdlLiberacoes.SelectedValue = liberacoes.ToString();


            DdlClinicas.DataSource = ImunneVacinas.Clinica.ConsultarComboBox(true, "CLINICAS");
            DdlClinicas.DataBind();
            DdlClinicas.SelectedValue = codigoClinica.ToString();

            DdlSituacoesCompras.Items.Add(new ListItem("SITUAÇÕES", "-1"));
            DdlSituacoesCompras.Items.Add(new ListItem("NÃO REALIZADAS", "1"));
            DdlSituacoesCompras.Items.Add(new ListItem("NÃO AUTORIZADAS", "2"));
            DdlSituacoesCompras.Items.Add(new ListItem("AUTORIZADAS", "3"));
            DdlSituacoesCompras.SelectedValue = situacao;

            DdlCategorias.Items.Add(new ListItem("CATEGORIAS", "-1"));
            DdlCategorias.Items.Add(new ListItem("PAGAS", "1"));
            DdlCategorias.Items.Add(new ListItem("IMPORTADAS", "3"));
            DdlCategorias.SelectedValue = categoria;

            DdlUtilizacoes.Items.Add(new ListItem("APLICAÇÕES", "-1"));
            DdlUtilizacoes.Items.Add(new ListItem("NÃO APLICADAS", "0"));
            DdlUtilizacoes.Items.Add(new ListItem("APLICADAS", "1"));
            DdlUtilizacoes.SelectedValue = aplicacao;

            HdfFinal.Value = terminoData;
            HdfInicio.Value = inicioData;

            DdlItens.Items.Add(new ListItem("MOSTRAR 10", "10"));
            DdlItens.Items.Add(new ListItem("MOSTRAR 20", "20"));
            DdlItens.Items.Add(new ListItem("MOSTRAR 30", "30"));
            DdlItens.Items.Add(new ListItem("MOSTRAR 40", "40"));
            DdlItens.Items.Add(new ListItem("MOSTRAR 50", "50"));
            DdlItens.Items.Add(new ListItem("MOSTRAR 100", "100"));
            DdlItens.SelectedValue = itens.ToString();

            DdlCampanhas.Items.Clear();

            #region CAMPANHAS DA CLÍNICA DO USUÁRIO

            if (roleUsuario == 7)
            {
                DdlClinicas.SelectedValue = codigoClinica.ToString();
                DdlClinicas.Enabled = false;

                codigoClinica = Convert.ToInt32(DdlClinicas.SelectedValue);
                ImunneVacinas.PesquisaCampanhaClinica pesquisaCampanha = new ImunneVacinas.PesquisaCampanhaClinica()
                {
                    Clinica = codigoClinica,
                    Ordenacao = ImunneVacinas.Utils.TipoOrdenacao.porNome
                };

                DdlCampanhas.DataSource = ImunneVacinas.CampanhaClinica.ConsultarCampanhasComboBox(pesquisaCampanha, true, "CAMPANHAS");
                DdlCampanhas.DataBind();
            }
            else
            {
                DdlCampanhas.Items.Clear();
                DdlCampanhas.DataSource = ImunneVacinas.Campanha.ConsultarComboBox(true, "CAMPANHAS");
                DdlCampanhas.DataBind();

                if (codigoClinica > 0)
                {
                    DdlClinicas.SelectedValue = codigoClinica.ToString();

                    ImunneVacinas.PesquisaCampanhaClinica pesquisaCampanha = new ImunneVacinas.PesquisaCampanhaClinica()
                    {
                        Clinica = codigoClinica,
                        Ordenacao = ImunneVacinas.Utils.TipoOrdenacao.porNome
                    };

                    DdlCampanhas.DataSource = ImunneVacinas.CampanhaClinica.ConsultarCampanhasComboBox(pesquisaCampanha, true, "CAMPANHAS");
                    DdlCampanhas.DataBind();
                    DdlCampanhas.Enabled = true;
                    DdlCampanhas.SelectedValue = codigoCampanha.ToString();
                }
            }

            #endregion

            DdlCampanhas.SelectedValue = codigoCampanha.ToString();

            if (!String.IsNullOrEmpty(pesquisa))
                TxtPesquisa.Text = pesquisa;

            if (categoria == "1")
                DdlLiberacoes.Enabled = true;
            else DdlLiberacoes.Enabled = false;

            if (liberacoes == 1)
            {
                DdlUtilizacoes.SelectedValue = "0";
                DdlUtilizacoes.Enabled = false;
            }
            else
            {
                DdlUtilizacoes.SelectedValue = "1";
                DdlUtilizacoes.Enabled = true;
            }

            PaginaAtual = paginaAtual;
            VincularItens();

            LnkExclusoes.OnClientClick = "return ConfirmacaoExclucao(this, event,'-1');";
        }
        catch (Exception ex)
        {
            this.SalvarException404(ex);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    private List<ImunneVacinas.Compra> ConsultarRegistros()
    {
        DateTime dataInicio = ImunneVacinas.Utils.dataPadrao;
        DateTime dataFinal = ImunneVacinas.Utils.dataPadrao;

        if (!String.IsNullOrEmpty(HdfInicio.Value) && !String.IsNullOrEmpty(HdfFinal.Value))
        {
            dataInicio = Convert.ToDateTime(HdfInicio.Value);
            dataFinal = Convert.ToDateTime(HdfFinal.Value).AddDays(1);
        }

        ImunneVacinas.PesquisaCompra pesquisa = new ImunneVacinas.PesquisaCompra()
        {
            Clinica = Convert.ToInt32(DdlClinicas.SelectedValue),
            Campanha = Convert.ToInt32(DdlCampanhas.SelectedValue),
            CampoLivre = TxtPesquisa.Text,
            Liberados = Convert.ToInt32(DdlLiberacoes.SelectedValue),
            Situacao = (ImunneVacinas.Compra.SituacaoCompra)Enum.Parse(typeof(ImunneVacinas.Compra.SituacaoCompra), DdlSituacoesCompras.SelectedValue),
            Categoria = (ImunneVacinas.Compra.CategoriaCompra)Enum.Parse(typeof(ImunneVacinas.Compra.CategoriaCompra), DdlCategorias.SelectedValue),
            Aplicacao = (ImunneVacinas.Compra.Aplicacao)Enum.Parse(typeof(ImunneVacinas.Compra.Aplicacao), DdlUtilizacoes.SelectedValue),
            Ordenacao = ImunneVacinas.Utils.TipoOrdenacao.porDataDesc,
            DataFim = dataFinal,
            DataInicio = dataInicio,
            Valor = true
        };

        return ImunneVacinas.Compra.ConsultarCompras(pesquisa);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void RptPrincipal_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        try
        {
            ImunneVacinas.Compra registroSelecionado = (ImunneVacinas.Compra)e.Item.DataItem;

            Label LblCampanha = (Label)e.Item.FindControl("LblCampanha");
            Label LblData = (Label)e.Item.FindControl("LblData");
            Label LblValor = (Label)e.Item.FindControl("LblValor");
            Label LblParticipante = (Label)e.Item.FindControl("LblParticipante");
            Label LblClinica = (Label)e.Item.FindControl("LblClinica");
            Label LblTotalItens = (Label)e.Item.FindControl("LblTotalItens");
            Label LblComprovante = (Label)e.Item.FindControl("LblComprovante");
            Label LblCidade = (Label)e.Item.FindControl("LblCidade");
            Label LblUf = (Label)e.Item.FindControl("LblUf");
            Label LblAplicacao = (Label)e.Item.FindControl("LblAplicacao");

            HiddenField HdfCodigoCompra = (HiddenField)e.Item.FindControl("HdfCodigoCompra");

            CheckBox ChkUtilizacao = (CheckBox)e.Item.FindControl("ChkUtilizacao");
            CheckBox ChkExclusao = (CheckBox)e.Item.FindControl("ChkExclusao");

            HyperLink LnkPrintList = (HyperLink)e.Item.FindControl("LnkPrintList");

            LinkButton LnkImpressoesList = (LinkButton)e.Item.FindControl("LnkImpressoesList");

            LblCampanha.Text = registroSelecionado.IdCampanha.Identificacao.ToUpper();
            LblData.Text = registroSelecionado.DataCompra.ToString("dd/MM/yyyy HH:mm");

            LblParticipante.Text = registroSelecionado.IdParticipante.Nome.ToUpper();

            LnkImpressoesList.CommandArgument = registroSelecionado.Codigo.ToString();

            LnkPrintList.NavigateUrl = "~/prints/compra.aspx?compra=" + ImunneVacinas.Criptografia.Criptografar(registroSelecionado.Codigo.ToString());

            HdfCodigoCompra.Value = registroSelecionado.Codigo.ToString();

            LblClinica.Text = "--";
            LblCidade.Text = "--";
            LblUf.Text = "--";

            if (!String.IsNullOrEmpty(registroSelecionado.IdClinica.NomeFantasia))
            {
                LblClinica.Text = registroSelecionado.IdClinica.NomeFantasia;
                LblCidade.Text = registroSelecionado.IdClinica.IdCidade.Nome;
                LblUf.Text = registroSelecionado.IdClinica.IdUf.Sigla;
            }

            if (!String.IsNullOrEmpty(registroSelecionado.NumeroComprovante))
                LblComprovante.Text = registroSelecionado.NumeroComprovante;
            else LblComprovante.Text = "--";

            #region VALORES E ITENS

            ImunneVacinas.PesquisaItemCompra pesquisa = new ImunneVacinas.PesquisaItemCompra()
            {
                Compra = registroSelecionado.Codigo
            };

            decimal valoresItens = 0;
            int totalItens = 0;

            List<ImunneVacinas.ItemCompra> listaItens = ImunneVacinas.ItemCompra.ConsultarItens(pesquisa);
            foreach (ImunneVacinas.ItemCompra item in listaItens)
            {
                totalItens += item.Quantidade;
                valoresItens += item.Valor;
            }

            LblTotalItens.Text = totalItens.ToString();
            LblValor.Text = ImunneVacinas.Utils.ToMoeda(valoresItens);

            #endregion

            ((HtmlControl)e.Item.FindControl("LnkDetalhes")).Attributes.Add("data-id", "../popups/detalhes-compras.aspx?&compra=" + registroSelecionado.Codigo);

            switch (registroSelecionado.Situacao)
            {
                case ImunneVacinas.Compra.SituacaoCompra.Aberta:
                    {
                        LblComprovante.Text = "NÃO REALIZADA";
                        LnkImpressoesList.Visible = false;
                    }
                    break;
                case ImunneVacinas.Compra.SituacaoCompra.Cancelada:
                    {
                        LnkImpressoesList.Visible = false;
                    }
                    break;
            }

            switch (registroSelecionado.Categoria)
            {
                case ImunneVacinas.Compra.CategoriaCompra.Importada:
                    {
                        ChkUtilizacao.Style.Add("opacity", ".6");
                        ChkUtilizacao.Text = String.Format("ENVIO {0}", registroSelecionado.DataLiberacao.ToString("dd/MM/yyyy"));
                        ChkUtilizacao.Checked = true;
                        ChkUtilizacao.Enabled = false;
                        LnkPrintList.Visible = false;

                        if (registroSelecionado.AplicacaoCompra == ImunneVacinas.Compra.Aplicacao.Aplicado)
                        {
                            LblAplicacao.Text = String.Format("APLICADO | {0}", registroSelecionado.DataAplicacao.ToString("dd/MM/yyyy HH:ss"));
                            LblAplicacao.CssClass = "label-success lbl-status";
                        }
                        else
                        {
                            LblAplicacao.Text = String.Format("NÃO APLICADO");
                            LblAplicacao.CssClass = "label-danger lbl-status";
                        }
                    }
                    break;
                case ImunneVacinas.Compra.CategoriaCompra.Paga:
                    {
                        // ALTERAR APÓS SUBIR A ROTINA DE SURTOS
                        switch (registroSelecionado.AplicacaoCompra)
                        {
                            case ImunneVacinas.Compra.Aplicacao.Aplicado:
                                {
                                    if (registroSelecionado.DataLiberacao != ImunneVacinas.Utils.dataPadrao)
                                    {
                                        ChkUtilizacao.Style.Add("opacity", ".6");
                                        ChkUtilizacao.Text = String.Format("ENVIO {0}", registroSelecionado.DataLiberacao.ToString("dd/MM/yyyy"));
                                        ChkUtilizacao.Checked = true;
                                        ChkUtilizacao.Enabled = false;
                                    }
                                    else if (registroSelecionado.DataCompra != ImunneVacinas.Utils.dataPadrao)
                                    {
                                        ChkUtilizacao.Style.Add("opacity", ".6");
                                        ChkUtilizacao.Text = String.Format("ENVIO {0}", registroSelecionado.DataCompra.ToString("dd/MM/yyyy"));
                                        ChkUtilizacao.Checked = true;
                                        ChkUtilizacao.Enabled = false;
                                    }

                                    LnkPrintList.Visible = false;

                                    LblAplicacao.Text = String.Format("APLICADO | {0}", registroSelecionado.DataAplicacao.ToString("dd/MM/yyyy HH:ss"));
                                    LblAplicacao.CssClass = "label-success lbl-status";
                                }
                                break;
                            case ImunneVacinas.Compra.Aplicacao.NaoAplicado:
                                {
                                    if (registroSelecionado.DataLiberacao != ImunneVacinas.Utils.dataPadrao)
                                    {
                                        ChkUtilizacao.Style.Add("opacity", ".6");
                                        ChkUtilizacao.Text = String.Format("ENVIO {0}", registroSelecionado.DataLiberacao.ToString("dd/MM/yyyy"));
                                        ChkUtilizacao.Checked = true;
                                        ChkUtilizacao.Enabled = false;
                                    }
                                    else
                                    {
                                        if (!String.IsNullOrEmpty(registroSelecionado.IdParticipante.Email))
                                        {
                                            if (registroSelecionado.AplicacaoCompra == ImunneVacinas.Compra.Aplicacao.NaoAplicado)
                                                ChkUtilizacao.Text = String.Format("À LIBERAR");
                                            else ChkUtilizacao.Text = String.Format("ENVIO {0}", registroSelecionado.DataAplicacao.ToString("dd/MM/yyyy HH:ss"));
                                        }
                                        else ChkUtilizacao.Text = String.Format("SEM E-MAIL");
                                    }

                                    int totalUtilizados = 0;
                                    int totalGeral = 0;

                                    #region UTILIZADOS

                                    ImunneVacinas.PesquisaVoucher pesquisaVouchers = new ImunneVacinas.PesquisaVoucher()
                                    {
                                        Situacao = ImunneVacinas.Voucher.SituacaoVoucher.Utilizado,
                                        Compra = registroSelecionado.Codigo,
                                        Ordenacao = ImunneVacinas.Utils.TipoOrdenacao.porData
                                    };
                                    totalUtilizados = ImunneVacinas.Voucher.ContabilizarVouchers(pesquisaVouchers);

                                    #endregion

                                    #region GERAL

                                    pesquisaVouchers = new ImunneVacinas.PesquisaVoucher()
                                    {
                                        Compra = registroSelecionado.Codigo,
                                        Ordenacao = ImunneVacinas.Utils.TipoOrdenacao.porData
                                    };
                                    totalGeral = ImunneVacinas.Voucher.ContabilizarVouchers(pesquisaVouchers);

                                    #endregion

                                    if (totalUtilizados == 0)
                                    {
                                        LblAplicacao.Text = String.Format("NÃO APLICADO");
                                        LblAplicacao.CssClass = "label-danger lbl-status";
                                    }
                                    else if (totalGeral != totalUtilizados)
                                    {
                                        LblAplicacao.Text = String.Format("UTILIZADOS {0} DE {1}", totalUtilizados, totalGeral);
                                        LblAplicacao.CssClass = "label-warning lbl-status";
                                    }
                                    else
                                    {
                                        LblAplicacao.Text = String.Format("UTILIZADOS {0} DE {1}", totalUtilizados, totalGeral);
                                        LblAplicacao.CssClass = "label-success lbl-status";
                                    }
                                }
                                break;
                        }

                        if (String.IsNullOrEmpty(registroSelecionado.IdParticipante.Email))
                        {
                            ChkUtilizacao.Style.Add("opacity", ".6");
                            ChkUtilizacao.Text = String.Format("SEM E-MAIL");
                            ChkUtilizacao.Checked = true;
                            ChkUtilizacao.Enabled = false;
                        }

                    }
                    break;
                default:
                    {
                        switch (registroSelecionado.AplicacaoCompra)
                        {
                            case ImunneVacinas.Compra.Aplicacao.Aplicado:
                                {
                                    if (registroSelecionado.DataLiberacao != ImunneVacinas.Utils.dataPadrao)
                                    {
                                        ChkUtilizacao.Style.Add("opacity", ".6");
                                        ChkUtilizacao.Text = String.Format("ENVIO {0}", registroSelecionado.DataLiberacao.ToString("dd/MM/yyyy"));
                                        ChkUtilizacao.Checked = true;
                                        ChkUtilizacao.Enabled = false;
                                    }
                                    else if (registroSelecionado.DataCompra != ImunneVacinas.Utils.dataPadrao)
                                    {
                                        ChkUtilizacao.Style.Add("opacity", ".6");
                                        ChkUtilizacao.Text = String.Format("ENVIO {0}", registroSelecionado.DataCompra.ToString("dd/MM/yyyy"));
                                        ChkUtilizacao.Checked = true;
                                        ChkUtilizacao.Enabled = false;
                                    }

                                    LnkPrintList.Visible = false;

                                    LblAplicacao.Text = String.Format("APLICADO | {0}", registroSelecionado.DataAplicacao.ToString("dd/MM/yyyy HH:ss"));
                                    LblAplicacao.CssClass = "label-success lbl-status";
                                }
                                break;
                            case ImunneVacinas.Compra.Aplicacao.NaoAplicado:
                                {
                                    LblAplicacao.Text = String.Format("NÃO APLICADO");
                                    LblAplicacao.CssClass = "label-danger lbl-status";
                                }
                                break;
                        }

                    }
                    break;
            }

            if (ChkUtilizacao.Checked == false)
            {
                foreach (string opcao in itensImpressao)
                {
                    if (opcao == registroSelecionado.Codigo.ToString())
                    {
                        ChkUtilizacao.Checked = true;
                        break;
                    }
                }
            }

            if (ChkExclusao.Checked == false)
            {
                foreach (string opcao in itensExclusao)
                {
                    if (opcao == registroSelecionado.Codigo.ToString())
                    {
                        ChkExclusao.Checked = true;
                        break;
                    }
                }
            }

            ChkExclusao.Enabled = ImunneVacinas.Compra.VerificarExclucao(registroSelecionado);
        }
        catch (Exception ex)
        {
            this.SalvarException(ex);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void RptPrincipal_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        int codigoRegistro = Convert.ToInt32(e.CommandArgument);
        string comando = e.CommandName;

        ImunneVacinas.Compra registroSelecionado = ImunneVacinas.Compra.ConsultarUnico(codigoRegistro);

        switch (comando)
        {
            case "print":
                {
                    url = new StringBuilder();
                    parametros = new StringBuilder();

                    parametros.AppendFormat("acao:{0}|", "edit");
                    parametros.AppendFormat("codigo:{0}|", codigoRegistro);
                    parametros.AppendFormat("clinica:{0}|", DdlClinicas.SelectedValue);
                    parametros.AppendFormat("campanha:{0}|", DdlCampanhas.SelectedValue);
                    parametros.AppendFormat("liberacoes:{0}|", DdlLiberacoes.SelectedValue);
                    parametros.AppendFormat("categoria:{0}|", DdlCategorias.SelectedValue);
                    parametros.AppendFormat("situacao:{0}|", DdlSituacoesCompras.SelectedValue);
                    parametros.AppendFormat("aplicacao:{0}|", DdlUtilizacoes.SelectedValue);
                    parametros.AppendFormat("pesquisa:{0}|", TxtPesquisa.Text);
                    parametros.AppendFormat("iniciodata:{0}|", HdfInicio.Value);
                    parametros.AppendFormat("terminodata:{0}|", HdfFinal.Value);
                    parametros.AppendFormat("itens:{0}|", DdlItens.SelectedValue);
                    parametros.AppendFormat("current_page:{0}", PaginaAtual);

                    url.AppendFormat("~/admin/impressoes.aspx?compra={0}", ImunneVacinas.Criptografia.Criptografar(parametros.ToString()));

                    Response.Redirect(url.ToString(), false);
                    Context.ApplicationInstance.CompleteRequest();
                }
                break;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BtnPesquisar_Click(object sender, EventArgs e)
    {
        itensImpressao = new List<string>();
        itensExclusao = new List<string>();

        PaginaAtual = 0;
        int role = this.Page.ConsultarSessionTipoUsuario();

        try
        {
            VincularItens();
        }
        catch (Exception ex)
        {
            this.SalvarException(ex);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BtnExportar_Click(object sender, EventArgs e)
    {
        #region ROTINA NOVA

        if (RptPrincipal.Items.Count > 0)
        {
            #region PESQUISA

            DateTime dataInicio = ImunneVacinas.Utils.dataPadrao;
            DateTime dataFinal = ImunneVacinas.Utils.dataPadrao;

            if (!String.IsNullOrEmpty(HdfInicio.Value) && !String.IsNullOrEmpty(HdfFinal.Value))
            {
                dataInicio = Convert.ToDateTime(HdfInicio.Value);
                dataFinal = Convert.ToDateTime(HdfFinal.Value).AddDays(1);
            }

            ImunneVacinas.PesquisaCompra pesquisa = new ImunneVacinas.PesquisaCompra()
            {
                Clinica = Convert.ToInt32(DdlClinicas.SelectedValue),
                Campanha = Convert.ToInt32(DdlCampanhas.SelectedValue),
                CampoLivre = TxtPesquisa.Text,
                Liberados = Convert.ToInt32(DdlLiberacoes.SelectedValue),
                Situacao = (ImunneVacinas.Compra.SituacaoCompra)Enum.Parse(typeof(ImunneVacinas.Compra.SituacaoCompra), DdlSituacoesCompras.SelectedValue),
                Categoria = (ImunneVacinas.Compra.CategoriaCompra)Enum.Parse(typeof(ImunneVacinas.Compra.CategoriaCompra), DdlCategorias.SelectedValue),
                Aplicacao = (ImunneVacinas.Compra.Aplicacao)Enum.Parse(typeof(ImunneVacinas.Compra.Aplicacao), DdlUtilizacoes.SelectedValue),
                Ordenacao = ImunneVacinas.Utils.TipoOrdenacao.porDataDesc,
                DataFim = dataFinal,
                DataInicio = dataInicio,
                Valor = true
            };

            #endregion

            try
            {
                List<ImunneVacinas.ListagemCompra> listaItens = ImunneVacinas.Compra.ConsultaComprasListagem(pesquisa);
                ImunneVacinas.ExportacoesExcel.NovoExportarCompras(listaItens);
            }
            catch (Exception ex)
            {
                ImunneVacinas.Erro erro = new ImunneVacinas.Erro();
                erro.addArquivo(ex);
            }
        }
        else
        {
            ImunneVacinas.Alerta.showMensagemAviso(this.Page, "Aviso", "Sua pesquisa não retornou nenhum resultado para exportação.");
            return;
        }

        #endregion

        #region ROTINA ATUAL

        //if (RptPrincipal.Items.Count > 0)
        //{
        //    List<ImunneVacinas.Compra> listaCompras = ConsultarRegistros();
        //    ImunneVacinas.ExportacoesExcel.ExportarCompras(listaCompras);
        //}
        //else
        //{
        //    try
        //    {
        //        ImunneVacinas.Alerta.showMensagemAviso(this.Page, "Aviso", "Sua pesquisa não retornou nenhum resultado para exportação.");
        //        return;
        //    }
        //    catch (Exception ex)
        //    {
        //        ImunneVacinas.Erro erro = new ImunneVacinas.Erro();
        //        erro.addArquivo(ex);
        //    }
        //}

        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BtnExportarItens_Click(object sender, EventArgs e)
    {
        if (RptPrincipal.Items.Count > 0)
        {
            #region PESQUISA

            DateTime dataInicio = ImunneVacinas.Utils.dataPadrao;
            DateTime dataFinal = ImunneVacinas.Utils.dataPadrao;

            if (!String.IsNullOrEmpty(HdfInicio.Value) && !String.IsNullOrEmpty(HdfFinal.Value))
            {
                dataInicio = Convert.ToDateTime(HdfInicio.Value);
                dataFinal = Convert.ToDateTime(HdfFinal.Value).AddDays(1);
            }

            ImunneVacinas.PesquisaCompra pesquisa = new ImunneVacinas.PesquisaCompra()
            {
                Clinica = Convert.ToInt32(DdlClinicas.SelectedValue),
                Campanha = Convert.ToInt32(DdlCampanhas.SelectedValue),
                CampoLivre = TxtPesquisa.Text,
                Liberados = Convert.ToInt32(DdlLiberacoes.SelectedValue),
                Situacao = (ImunneVacinas.Compra.SituacaoCompra)Enum.Parse(typeof(ImunneVacinas.Compra.SituacaoCompra), DdlSituacoesCompras.SelectedValue),
                Categoria = (ImunneVacinas.Compra.CategoriaCompra)Enum.Parse(typeof(ImunneVacinas.Compra.CategoriaCompra), DdlCategorias.SelectedValue),
                Aplicacao = (ImunneVacinas.Compra.Aplicacao)Enum.Parse(typeof(ImunneVacinas.Compra.Aplicacao), DdlUtilizacoes.SelectedValue),
                Ordenacao = ImunneVacinas.Utils.TipoOrdenacao.porDataDesc,
                DataFim = dataFinal,
                DataInicio = dataInicio,
                Valor = true
            };

            #endregion

            try
            {
                List<ImunneVacinas.ListagemItemCompra> listaItens = ImunneVacinas.ItemCompra.ConsultaItensComprasListagem(pesquisa);
                ImunneVacinas.ExportacoesExcel.NovoExportarItensCompras(listaItens);
            }
            catch (Exception ex)
            {
                ImunneVacinas.Erro erro = new ImunneVacinas.Erro();
                erro.addArquivo(ex);
            }
        }
        else
        {
            ImunneVacinas.Alerta.showMensagemAviso(this.Page, "Aviso", "Sua pesquisa não retornou nenhum resultado para exportação.");
            return;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void DdlClinicas_SelectedIndexChanged(object sender, EventArgs e)
    {
        int codigoClinica = Convert.ToInt32(DdlClinicas.SelectedValue);
        int codigoCampanha = Convert.ToInt32(DdlCampanhas.SelectedValue);

        DdlCampanhas.Items.Clear();

        if (codigoClinica > 0)
        {
            ImunneVacinas.PesquisaCampanhaClinica pesquisa = new ImunneVacinas.PesquisaCampanhaClinica()
            {
                Clinica = codigoClinica,
                Ordenacao = ImunneVacinas.Utils.TipoOrdenacao.porNome
            };

            DdlCampanhas.DataSource = ImunneVacinas.CampanhaClinica.ConsultarCampanhasComboBox(pesquisa, true, "CAMPANHAS");
            DdlCampanhas.DataBind();
            DdlCampanhas.Enabled = true;
        }
        else
        {
            DdlCampanhas.DataSource = ImunneVacinas.Campanha.ConsultarComboBox(true, "CAMPANHAS");
            DdlCampanhas.DataBind();
        }

        DdlCampanhas.SelectedValue = codigoCampanha.ToString();

        itensImpressao = new List<string>();
        itensExclusao = new List<string>();

        PaginaAtual = 0;
        VincularItens();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void DdlCampanhas_SelectedIndexChanged(object sender, EventArgs e)
    {
        itensImpressao = new List<string>();
        itensExclusao = new List<string>();

        PaginaAtual = 0;
        VincularItens();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BtnExcluirItens_Click(object sender, EventArgs e)
    {
        ExcluirRegistros();
    }

    /// <summary>
    /// 
    /// </summary>
    private void ExcluirRegistro()
    {
        int codigoRegistro = Convert.ToInt32(HdfCodigo.Value);
        ImunneVacinas.Compra registroSelecionado = ImunneVacinas.Compra.ConsultarUnico(codigoRegistro);

        if (registroSelecionado == null)
            ImunneVacinas.Alerta.showMensagemErro(this.Page, "Oops!", "Nenhum registro foi localizado com o código informado!");
        else
        {
            try
            {
                int resultado = registroSelecionado.Excluir();
                if (resultado > 0)
                {
                    // Transação
                    ImunneVacinas.PesquisaTransacaoRealizada pesquisaTransacoes = new ImunneVacinas.PesquisaTransacaoRealizada()
                    {
                        Compra = codigoRegistro,
                    };

                    List<ImunneVacinas.TransacaoRealizada> listaTransacoes = ImunneVacinas.TransacaoRealizada.ConsultarTransacoes(pesquisaTransacoes);
                    foreach (ImunneVacinas.TransacaoRealizada item in listaTransacoes)
                    {
                        item.Excluir();
                    }

                    // Itens
                    ImunneVacinas.PesquisaItemCompra pesquisaItensCompra = new ImunneVacinas.PesquisaItemCompra()
                    {
                        Compra = codigoRegistro,
                    };

                    List<ImunneVacinas.ItemCompra> listaItensCompra = ImunneVacinas.ItemCompra.ConsultarItens(pesquisaItensCompra);
                    foreach (ImunneVacinas.ItemCompra item in listaItensCompra)
                    {
                        item.Excluir();
                    }

                    // Vouchers
                    ImunneVacinas.PesquisaVoucher pesquisaVouchers = new ImunneVacinas.PesquisaVoucher()
                    {
                        Compra = codigoRegistro,
                    };

                    List<ImunneVacinas.Voucher> ListaVouchers = ImunneVacinas.Voucher.ConsultarVouchers(pesquisaVouchers);
                    foreach (ImunneVacinas.Voucher item in ListaVouchers)
                    {
                        item.Excluir();
                    }

                    // Log descritivo
                    ImunneVacinas.Log logAcao = new ImunneVacinas.Log()
                    {
                        Tela = Path.GetFileName(Request.PhysicalPath),
                        TabelaAcao = "Compras",
                        TipoAcao = ImunneVacinas.Utils.AcaoLog.Exclusao,
                        Registro = registroSelecionado.Codigo,
                        Resumo = String.Format("EXCLUSÃO DE REGISTRO {0}.", registroSelecionado.Codigo),
                        DataAcao = DateTime.Now,
                        Campo = String.Empty,
                        ValorOriginal = String.Empty,
                        ValorNovo = String.Empty,
                    };

                    logAcao.Incluir();

                    ImunneVacinas.Alerta.showMensagemSucesso(this.Page, "Sucesso!", "Registro excluído com sucesso!");
                    HdfCodigo.Value = "";

                    VincularItens();
                }
                else ImunneVacinas.Alerta.showMensagemErro(this.Page, "Oops!", "Infelizmente não foi possível concluir a solicitação, tente novamente ou informe o suporte.");
            }
            catch (Exception ex)
            {
                this.SalvarException(ex);
                ImunneVacinas.Alerta.showMensagemErro(this.Page, "Oops!", "Infelizmente não foi possível concluir a solicitação, tente novamente ou informe o suporte.");
            }
        }
    }

    /// <summary>
    /// Exclui todos os registros selecionados do list
    /// </summary>
    private void ExcluirRegistros()
    {
        StringBuilder mensagemConclusao = new StringBuilder();

        int contador = 0;

        // NENHUM ITEM NA LISTA DE BAIXAS
        if (itensExclusao == null || itensExclusao.Count <= 0)
        {
            ImunneVacinas.Alerta.showMensagemAviso(this.Page, "Aviso", "Nenhuma compra foi selecionada para realizar a exclusão.");
            return;
        }


        // CORRENDO OS ITENS A SEREM BAIXADOS
        foreach (string opcao in itensExclusao)
        {
            ImunneVacinas.Compra compraSelecionada = ImunneVacinas.Compra.ConsultarUnico(Convert.ToInt32(opcao));
            bool retornoOperacao = ExcluirCompras(compraSelecionada.Codigo);

            if (retornoOperacao == true)
            {
                if (mensagemConclusao.Length > 0)
                    mensagemConclusao.Append(" | ");
                else mensagemConclusao.Append("COMPRAS EXCLUÍDAS ");

                mensagemConclusao.AppendFormat("{0}", compraSelecionada.NumeroComprovante);
                contador += 1;
            }

        }

        // CONCLUSÃO
        if (contador == 0)
        {
            ImunneVacinas.Alerta.showMensagemErro(this.Page, "Aviso", "Nenhuma compra foi selecionada ou não foi possível realizar a exclusão das compras selecionadas!");
            return;
        }

        #region LOG SISTEMA

        ImunneVacinas.Log novoLog = new ImunneVacinas.Log()
        {
            Tela = Path.GetFileName(Request.PhysicalPath),
            TabelaAcao = "COMPRAS",
            TipoAcao = ImunneVacinas.Utils.AcaoLog.Exclusao,
            Resumo = mensagemConclusao.ToString(),
            DataAcao = DateTime.Now,
            Campo = "EXCLUSOES",
            ValorOriginal = String.Empty,
            ValorNovo = String.Empty
        };
        novoLog.IdUsuario.Codigo = this.Page.ConsultarSessionCodigoUsuario();
        novoLog.Incluir();

        #endregion

        ImunneVacinas.Alerta.showMensagemSucesso(this.Page, "Sucesso!", "Todas as compras selecionadas foram excluídas com sucesso!");

        ChkTodosLixo.Checked = false;
        LnkMarcarTodosLixo.Text = "<i class='fa fa-trash'></i>&nbsp;Marcar todos";

        itensExclusao = new List<string>();
        VincularItens();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void LnkAplicacoes_Click(object sender, EventArgs e)
    {
        StringBuilder mensagemConclusao = new StringBuilder();

        int contador = 0;

        // NENHUM ITEM NA LISTA DE BAIXAS
        if (itensImpressao == null || itensImpressao.Count <= 0)
        {
            ImunneVacinas.Alerta.showMensagemAviso(this.Page, "Aviso", "Nenhuma compra foi selecionada para realizar a baixa.");
            return;
        }


        // CORRENDO OS ITENS A SEREM BAIXADOS
        foreach (string opcao in itensImpressao)
        {
            ImunneVacinas.Compra compraSelecionada = ImunneVacinas.Compra.ConsultarUnico(Convert.ToInt32(opcao));
            bool retornoOperacao = ReenviarEmail(compraSelecionada.Codigo);

            if (retornoOperacao == true)
            {
                if (mensagemConclusao.Length > 0)
                    mensagemConclusao.Append(" | ");
                else mensagemConclusao.Append("COMPRAS BAIXADAS ");

                mensagemConclusao.AppendFormat("{0}", compraSelecionada.NumeroComprovante);
                contador += 1;
            }

        }

        // CONCLUSÃO
        if (contador == 0)
        {
            ImunneVacinas.Alerta.showMensagemErro(this.Page, "Aviso", "Nenhuma compra foi selecionada ou não foi possível realizar o envio da compras selecionadas!");
            return;
        }

        #region LOG SISTEMA

        ImunneVacinas.Log novoLog = new ImunneVacinas.Log()
        {
            Tela = Path.GetFileName(Request.PhysicalPath),
            TabelaAcao = "COMPRAS",
            TipoAcao = ImunneVacinas.Utils.AcaoLog.Envio_Email,
            Resumo = mensagemConclusao.ToString(),
            DataAcao = DateTime.Now,
            Campo = "ENVIOS",
            ValorOriginal = String.Empty,
            ValorNovo = String.Empty
        };
        novoLog.IdUsuario.Codigo = this.Page.ConsultarSessionCodigoUsuario();
        novoLog.Incluir();

        #endregion

        ImunneVacinas.Alerta.showMensagemSucesso(this.Page, "Sucesso!", "Todas as compras selecionadas foram enviadas com sucesso!");

        itensImpressao = new List<string>();
        VincularItens();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void LnkExclusoes_Click(object sender, EventArgs e)
    {

    }

    /// <summary>
    /// 
    /// </summary>
    private bool ReenviarEmail(int codigoCompra)
    {
        bool retorno = false;

        ImunneVacinas.Compra compraSelecionada = ImunneVacinas.Compra.ConsultarUnico(codigoCompra);

        ImunneVacinas.PesquisaVoucher pesquisaVoucher = new ImunneVacinas.PesquisaVoucher()
        {
            Campanha = compraSelecionada.IdCampanha.Codigo,
            Clinica = compraSelecionada.IdClinica.Codigo,
            Compra = codigoCompra
        };

        ImunneVacinas.Participante participanteSelecionado = ImunneVacinas.Participante.ConsultarUnico(compraSelecionada.IdParticipante.Codigo);

        List<ImunneVacinas.Voucher> listaVales = ImunneVacinas.Voucher.ConsultarVouchers(pesquisaVoucher);

        // Validando o e-mail do participante
        if (!String.IsNullOrEmpty(participanteSelecionado.Email) && ImunneVacinas.Utils.ValidarEmail(participanteSelecionado.Email))
        {
            // 04/12/2019 - GUILHERME PETRACA
            // SÃO DUAS ESTRUTURAS DE ENVIO, UMA PARA SURTOS COM A LIBERAÇÃO JÁ PRONTA E UMA PARA CAMPANHAS QUE TERÃO A LIBERAÇÃO
            // PELA CLÍNICA.
            try
            {
                StringBuilder nomesBeneficiarios = new StringBuilder();
                StringBuilder tituloEmail = new StringBuilder();

                // Verificando todos os vales da compra
                foreach (ImunneVacinas.Voucher item in listaVales)
                {
                    ImunneVacinas.Participante beneficiario = ImunneVacinas.Participante.ConsultarUnico(item.IdParticipante.Codigo);
                    nomesBeneficiarios.AppendFormat("{0} | {1}<br />", beneficiario.Nome.ToUpper(), beneficiario.Parentesco.Nome.ToUpper());
                }

                StringBuilder conteudoCorpo = new StringBuilder();
                bool envioEmail = false;

                envioEmail = ImunneVacinas.Email.EnviarEmail(participanteSelecionado.Email, "S&A Imunizações | Liberação de Aplicações", ImunneVacinas.Email.CompraLiberada(compraSelecionada, nomesBeneficiarios.ToString()));
                envioEmail = true;

                if (envioEmail)
                {
                    compraSelecionada.DataLiberacao = DateTime.Now;
                    compraSelecionada.Alterar();

                    retorno = true;
                }
            }
            catch (Exception ex)
            {
                ImunneVacinas.Erro erro = new ImunneVacinas.Erro();
                erro.addArquivo(ex);

                retorno = false;
            }
        }

        return retorno;
    }

    /// <summary>
    /// Exclui as compra selecionada e todos os itens relacionados a mesma
    /// </summary>
    /// <param name="codigoCompra"></param>
    /// <returns></returns>
    private bool ExcluirCompras(int codigoCompra)
    {
        bool retorno = false;

        ImunneVacinas.Compra compraSelecionada = ImunneVacinas.Compra.ConsultarUnico(codigoCompra);
        if (compraSelecionada != null)
        {
            #region ITENS DA COMPRA

            ImunneVacinas.PesquisaItemCompra pesquisaItens = new ImunneVacinas.PesquisaItemCompra()
            {
                Compra = codigoCompra
            };

            List<ImunneVacinas.ItemCompra> listaItens = ImunneVacinas.ItemCompra.ConsultarItens(pesquisaItens);
            foreach (ImunneVacinas.ItemCompra item in listaItens)
            {
                try
                {
                    item.Excluir();
                }
                catch (Exception ex)
                {
                    this.SalvarException(ex);
                }
            }

            #endregion

            #region VOUCHERS DA COMPRA

            ImunneVacinas.PesquisaVoucher pesquisaVouchers = new ImunneVacinas.PesquisaVoucher()
            {
                Compra = codigoCompra
            };

            List<ImunneVacinas.Voucher> listaVouchers = ImunneVacinas.Voucher.ConsultarVouchers(pesquisaVouchers);
            foreach (ImunneVacinas.Voucher item in listaVouchers)
            {
                try
                {
                    item.Excluir();
                }
                catch (Exception ex)
                {
                    this.SalvarException(ex);
                }
            }

            #endregion

            retorno = Convert.ToBoolean(compraSelecionada.Excluir());
        }

        return retorno;
    }

    #region Paginação

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void DdlPaginas_SelectedIndexChanged(object sender, EventArgs e)
    {
        PaginaAtual = Convert.ToInt32(DdlPaginas.SelectedValue);
        VincularItens();
    }

    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ChkUtilizacao_CheckedChanged(object sender, EventArgs e)
    {
        GerenciarSelecoes();
    }

    /// <summary>
    /// 
    /// </summary>
    private void GerenciarSelecoes()
    {
        #region ENVIOS DE COMPRAS

        // ADIÇÃO
        foreach (RepeaterItem item in RptPrincipal.Items)
        {
            CheckBox ChkUtilizacao = (CheckBox)item.FindControl("ChkUtilizacao");
            HiddenField HdfCodigoCompra = (HiddenField)item.FindControl("HdfCodigoCompra");

            if (ChkUtilizacao.Enabled == true && ChkUtilizacao.Checked == true)
            {
                int verificadorExistencia = 0;

                foreach (string opcao in itensImpressao)
                {
                    if (opcao == HdfCodigoCompra.Value)
                    {
                        verificadorExistencia = 1;
                        break;
                    }
                }

                if (verificadorExistencia == 0)
                    itensImpressao.Add(HdfCodigoCompra.Value);
            }
        }

        // REMOÇÃO
        List<string> itensTemp = itensImpressao;

        foreach (RepeaterItem item in RptPrincipal.Items)
        {
            CheckBox ChkUtilizacao = (CheckBox)item.FindControl("ChkUtilizacao");
            HiddenField HdfCodigoCompra = (HiddenField)item.FindControl("HdfCodigoCompra");

            if (ChkUtilizacao.Enabled == true && ChkUtilizacao.Checked == false)
            {
                foreach (string opcao in itensImpressao)
                {
                    if (opcao == HdfCodigoCompra.Value)
                    {
                        itensTemp.Remove(opcao);
                        break;
                    }
                }
            }
        }

        // CONFIRMAÇÃO
        itensImpressao = itensTemp;

        #endregion

        #region EXCLUSÃO DE COMPRAS

        // ADIÇÃO
        foreach (RepeaterItem item in RptPrincipal.Items)
        {
            CheckBox ChkExclusao = (CheckBox)item.FindControl("ChkExclusao");
            HiddenField HdfCodigoCompra = (HiddenField)item.FindControl("HdfCodigoCompra");

            if (ChkExclusao.Enabled == true && ChkExclusao.Checked == true)
            {
                int verificadorExistencia = 0;

                foreach (string opcao in itensExclusao)
                {
                    if (opcao == HdfCodigoCompra.Value)
                    {
                        verificadorExistencia = 1;
                        break;
                    }
                }

                if (verificadorExistencia == 0)
                    itensExclusao.Add(HdfCodigoCompra.Value);
            }
        }

        // REMOÇÃO
        List<string> itensTempExclusao = itensExclusao;

        foreach (RepeaterItem item in RptPrincipal.Items)
        {
            CheckBox ChkExclusao = (CheckBox)item.FindControl("ChkExclusao");
            HiddenField HdfCodigoCompra = (HiddenField)item.FindControl("HdfCodigoCompra");

            if (ChkExclusao.Enabled == true && ChkExclusao.Checked == false)
            {
                foreach (string opcao in itensExclusao)
                {
                    if (opcao == HdfCodigoCompra.Value)
                    {
                        itensTempExclusao.Remove(opcao);
                        break;
                    }
                }
            }
        }

        // CONFIRMAÇÃO
        itensExclusao = itensTempExclusao;

        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void LnkMarcarTodos_Click(object sender, EventArgs e)
    {
        if (RptPrincipal.Items.Count <= 0)
        {
            ImunneVacinas.Alerta.showMensagemAviso(this.Page, "Aviso!", "Não existe nenhum registro para seleção! Verifique sua pesquisa.");
            return;
        }

        itensImpressao = new List<string>();

        if (ChkTodos.Checked == true)
        {
            ChkTodos.Checked = false;
            LnkMarcarTodos.Text = "<i class='fa fa-envelope'></i>&nbsp;Marcar todos";

            foreach (RepeaterItem item in RptPrincipal.Items)
            {
                CheckBox ChkUtilizacao = (CheckBox)item.FindControl("ChkUtilizacao");
                HiddenField HdfCodigoCompra = (HiddenField)item.FindControl("HdfCodigoCompra");

                if (ChkUtilizacao.Enabled == true && ChkUtilizacao.Checked == true)
                    ChkUtilizacao.Checked = false;
            }
        }
        else
        {
            ChkTodos.Checked = true;
            LnkMarcarTodos.Text = "<i class='fa fa-envelope'></i>&nbsp;Desmarcar todos";

            foreach (RepeaterItem item in RptPrincipal.Items)
            {
                CheckBox ChkUtilizacao = (CheckBox)item.FindControl("ChkUtilizacao");
                HiddenField HdfCodigoCompra = (HiddenField)item.FindControl("HdfCodigoCompra");

                if (ChkUtilizacao.Enabled == true && ChkUtilizacao.Checked == false)
                {
                    ChkUtilizacao.Checked = true;
                    itensImpressao.Add(HdfCodigoCompra.Value);
                }
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void LnkMarcarTodosLixo_Click(object sender, EventArgs e)
    {
        if (RptPrincipal.Items.Count <= 0)
        {
            ImunneVacinas.Alerta.showMensagemAviso(this.Page, "Aviso!", "Não existe nenhum registro para seleção! Verifique sua pesquisa.");
            return;
        }

        itensExclusao = new List<string>();

        if (ChkTodosLixo.Checked == true)
        {
            ChkTodosLixo.Checked = false;
            LnkMarcarTodosLixo.Text = "<i class='fa fa-trash'></i>&nbsp;Marcar todos";

            foreach (RepeaterItem item in RptPrincipal.Items)
            {
                CheckBox ChkExclusao = (CheckBox)item.FindControl("ChkExclusao");
                HiddenField HdfCodigoCompra = (HiddenField)item.FindControl("HdfCodigoCompra");

                if (ChkExclusao.Enabled == true && ChkExclusao.Checked == true)
                    ChkExclusao.Checked = false;
            }
        }
        else
        {
            ChkTodosLixo.Checked = true;
            LnkMarcarTodosLixo.Text = "<i class='fa fa-envelope'></i>&nbsp;Desmarcar todos";

            foreach (RepeaterItem item in RptPrincipal.Items)
            {
                CheckBox ChkExclusao = (CheckBox)item.FindControl("ChkExclusao");
                HiddenField HdfCodigoCompra = (HiddenField)item.FindControl("HdfCodigoCompra");

                if (ChkExclusao.Enabled == true && ChkExclusao.Checked == false)
                {
                    ChkExclusao.Checked = true;
                    itensExclusao.Add(HdfCodigoCompra.Value);
                }
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void DdlCategorias_SelectedIndexChanged(object sender, EventArgs e)
    {
        int codigoCategoria = Convert.ToInt32(DdlCategorias.SelectedValue);

        if (codigoCategoria == 1)
            DdlLiberacoes.Enabled = true;
        else
        {
            DdlLiberacoes.SelectedValue = "-1";
            DdlLiberacoes.Enabled = false;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void DdlLiberacoes_SelectedIndexChanged(object sender, EventArgs e)
    {
        int codigoLiberacao = Convert.ToInt32(DdlLiberacoes.SelectedValue);

        if (codigoLiberacao == 1)
        {
            DdlUtilizacoes.Enabled = false;
            DdlUtilizacoes.SelectedValue = "0";
        }
        else
        {
            DdlUtilizacoes.Enabled = true;
            DdlUtilizacoes.SelectedValue = "-1";
        }
    }
}