﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="edit-vacinas.aspx.cs" Inherits="edit_vacinas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        function validar() {
            // Nome
            if (document.getElementById("<%=TxtNome.ClientID%>").value == "-1") {
                alert("Por favor, informe o nome da vacina.");
                return false;
            }
        };
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <ul class="breadcrumb">
        <li><a href="dashboard.aspx"><i class="fa fa-home"></i>&nbsp;Home</a> </li>
        <li>
            <asp:HyperLink ID="LnkPai" runat="server" Text="Usuários"></asp:HyperLink>
        </li>
        <li class="active">Inserção/Edição</li>
    </ul>
    <h3><i class="fa fa-medkit"></i>&nbsp;<asp:Label ID="LblForms" runat="server"></asp:Label></h3>
    <asp:UpdatePanel ID="UpdMain" runat="server">
        <ContentTemplate>
            <div class="box box-success">
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                            <label>Nome *</label>
                            <asp:TextBox ID="TxtNome" runat="server" CssClass="form-control text-uppercase"></asp:TextBox>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                            <label>Observações</label>
                            <textarea ID="TxtObservacoes" runat="server" maxlength="1000"  class="form-control text-uppercase" onkeyup="countChar(this)" style="height: 80px;"></textarea>
                            <b>
                                <div id="charNum">Caracteres restantes 1000</div>
                            </b>
                        </div>
                        <script>
                            function countChar(val) {
                                var len = val.value.length;
                                var resultado = "Caracteres restantes " + (max - len);
                                var max = document.getElementById("<%=TxtObservacoes.ClientID%>").maxLength;
                                if (len >= max) {
                                    val.value = val.value.substring(0, max);
                                } else {
                                    var resultado = "Caracteres restantes " + (max - len);
                                    $('#charNum').text(resultado);
                                }
                            };
                        </script>
                    </div>
                </div>
            </div>
            <div class="fixed-button-box">
                <div class="text-right">
                    <asp:LinkButton ID="BtnSalvar" runat="server" CssClass="btn btn-success btn-lg"
                        data-toggle="tooltip" data-container="body" ToolTip="Salvar informações"
                        ValidationGroup="edicao" OnClientClick="return validar();" OnClick="BtnSalvar_Click">
                    <i class="fa fa-save"></i>
                    </asp:LinkButton>
                    &nbsp;
                <asp:LinkButton ID="BtnCancelar" runat="server"
                    CssClass="btn btn-danger btn-lg pull-right" data-toggle="tooltip" data-container="body" ToolTip="Cancelar operação"
                    Text="Cancelar" CausesValidation="false">
                    <i class="fa fa-times"></i>
                </asp:LinkButton>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
