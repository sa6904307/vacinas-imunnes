﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="edit-cidades.aspx.cs" Inherits="admin_edit_cidades" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js" type="text/javascript"></script>
    <link href="../vendors/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <script src="../vendors/global/jquery.min.js"></script>
    <script type="text/javascript" src="../vendors/select2/js/select2.full.min.js"></script>
    <script type="text/javascript">
        var select$ = jQuery.noConflict();
        select$(function () {
            //Initialize Select2 Elements
            select$('.select2').select2()
        })
    </script>
    <script type="text/javascript">
        function validar() {
            // Nome
            if (document.getElementById("<%=DdlUfs.ClientID%>").value == "-1") {
                MensagemGenerica("Aviso!", "Por favor, informe o estado da cidade.", "warning");
                return false;
            }

            // Nome
            if (document.getElementById("<%=TxtNome.ClientID%>").value == "") {
                MensagemGenerica("Aviso!", "Por favor, informe o nome da cidade.", "warning");
                return false;
            }

        };
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <ul class="breadcrumb">
        <li><a href="dashboard.aspx"><i class="fa fa-briefcase"></i>&nbsp;Home</a> </li>
        <li>
            <asp:HyperLink ID="LnkPai" runat="server" Text="Cidades"></asp:HyperLink>
        </li>
        <li class="active">Inserção/Edição</li>
    </ul>
    <h3><i class="fa fa-map-marker"></i>&nbsp;<asp:Label ID="LblForms" runat="server"></asp:Label></h3>
    <asp:UpdatePanel ID="UpdMain" runat="server">
        <ContentTemplate>
            <script type="text/javascript">
                Sys.Application.add_load(SetSelectBox);
                var select$ = jQuery.noConflict();
                function SetSelectBox() {
                    select$('.select2').select2();
                }
            </script>
            <div class="box box-success">
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                            <label>Nome *</label>
                            <asp:TextBox ID="TxtNome" runat="server" CssClass="form-control text-uppercase"></asp:TextBox>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 form-group">
                            <label>UF *</label>
                            <asp:DropDownList ID="DdlUfs" runat="server" CssClass="form-control text-uppercase select2" DataValueField="ValueMember" DataTextField="DisplayMember"></asp:DropDownList>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 form-group">
                            <label>CEP Inicial</label>
                            <asp:TextBox ID="TxtCEPInicial" runat="server" CssClass="form-control text-uppercase" OnKeyPress="mascara(this, mcep)" MaxLength="9"></asp:TextBox>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 form-group">
                            <label>CEP Final</label>
                            <asp:TextBox ID="TxtCEPFinal" runat="server" CssClass="form-control text-uppercase" OnKeyPress="mascara(this, mcep)" MaxLength="9"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
            <div class="fixed-button-box">
                <div class="text-right">
                    <asp:LinkButton ID="BtnSalvar" runat="server" CssClass="btn btn-success btn-lg"
                        data-toggle="tooltip" data-container="body" ToolTip="Salvar informações"
                        ValidationGroup="edicao" OnClientClick="return validar();" OnClick="BtnSalvar_Click">
                    <i class="fa fa-save"></i>
                    </asp:LinkButton>
                    &nbsp;
                <asp:LinkButton ID="BtnCancelar" runat="server"
                    CssClass="btn btn-danger btn-lg pull-right" data-toggle="tooltip" data-container="body" ToolTip="Cancelar operação"
                    Text="Cancelar" CausesValidation="false">
                    <i class="fa fa-times"></i>
                </asp:LinkButton>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
