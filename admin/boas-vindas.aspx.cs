﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_boas_vindas : System.Web.UI.Page
{
    private StringBuilder url = new StringBuilder();
    private string textoMensagem = String.Empty;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = "S&A Imunizações | Envio de Boas-Vindas";

        if (!IsPostBack)
            IniciarPagina();
    }

    /// <summary>
    /// 
    /// </summary>
    private void IniciarPagina()
    {
        NameValueCollection clinica = new NameValueCollection();
        int codigoRegistro = -1;

        try
        {
            if (Request.QueryString["clinica"] != null)
            {
                clinica = ImunneVacinas.Utils.RecuperarQueryString(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["clinica"]));
                url = new StringBuilder();
                url.AppendFormat("~/admin/clinicas.aspx?clinica={0}", Request.QueryString["clinica"]);
                LnkPai.NavigateUrl = url.ToString();
                LnkVoltar.NavigateUrl = url.ToString();

                codigoRegistro = Convert.ToInt32(clinica["codigo"]);

                ImunneVacinas.Clinica registroSelecionado = ImunneVacinas.Clinica.ConsultarUnico(codigoRegistro);

                if (!String.IsNullOrEmpty(registroSelecionado.EmailGeral))
                    TxtDestinatarios.Text = registroSelecionado.EmailGeral.ToLower() + ";";

                TxtConteudo.Text = Resources.Site.ConteudoBoasVindas.Replace("#NOMEEMPRESA", registroSelecionado.NomeFantasia.ToUpper());
            }
        }
        catch (Exception ex)
        {

        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BtnEnviar_Click(object sender, EventArgs e)
    {
        try
        {
            NameValueCollection clinica = new NameValueCollection();
            clinica = ImunneVacinas.Utils.RecuperarQueryString(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["clinica"]));
            int codigoRegistro = Convert.ToInt32(clinica["codigo"]);

            string conteudoMensagem = TxtConteudo.Text.Replace("\r\n", "<br /><br />").Replace("\n\n", "<br /><br />");

            bool flagEnvio = ImunneVacinas.Email.EnviarEmailBoasVindas(TxtDestinatarios.Text, "S&A Imunizações | Boas-Vindas", conteudoMensagem);

            if (flagEnvio)
            {
                ImunneVacinas.Clinica registroSelecionado = ImunneVacinas.Clinica.ConsultarUnico(codigoRegistro);
                registroSelecionado.EnvioBoasVindas = 1;
                //registroSelecionado.DataEnvio = DateTime.Now;
                //registroSelecionado.Alterar();

                LnkVoltar.Visible = true;
                ImunneVacinas.Alerta.showMensagemSucesso(this.Page, "Envio realizado!", "Sua mensagem foi enviada com sucesso para todos os destinatários.");
            }
            else ImunneVacinas.Alerta.showMensagemAviso(this.Page, "Aviso!", "Infelizmente sua mensagem não pode ser enviada, tente novamente.");
        }
        catch (Exception ex)
        {
            ImunneVacinas.Erro erro = new ImunneVacinas.Erro();
            erro.AdicionarLog(ex);
            ImunneVacinas.Alerta.showMensagemAviso(this.Page, "Aviso!", "Infelizmente sua mensagem não pode ser enviada, tente novamente.");
        }
    }
}