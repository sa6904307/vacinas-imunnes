﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="locais-adesao.aspx.cs" Inherits="admin_locais_adesao" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js" type="text/javascript"></script>
    <link href="../vendors/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <script src="../vendors/global/jquery.min.js"></script>
    <script type="text/javascript" src="../vendors/select2/js/select2.full.min.js"></script>
    <script type="text/javascript">
        var select$ = jQuery.noConflict();
        select$(function () {
            //Initialize Select2 Elements
            select$('.select2').select2()
        })
    </script>
    <script type="text/javascript">
        // Chamada de alerta para confirmação de exclusão
        function ConfirmacaoExclucao(ctl, event, id) {
            var defaultAction = $(ctl).prop("href");
            event.preventDefault();
            swal({
                title: "Deseja excluir?",
                text: 'Uma vez excluído, o registro não poderá ser recuperado.',
                type: 'question',
                showCancelButton: true,
                confirmButtonText: "<i class='fa fa-check'></i>&nbsp;Sim!",
                cancelButtonText: "<i class='fa fa-times'></i>&nbsp;Não!",
                confirmButtonColor: '#00a65a',
                cancelButtonColor: '#dd4b39',
                showLoaderOnConfirm: true,
                animation: 'slide-from-top',
                allowEscapeKey: false,
                allowOutsideClick: false
            }).then(result => {
                if (result.value) {
                    document.getElementById('<%=HdfCodigo.ClientID%>').value = id;
                    var btn = document.getElementById('<%=BtnExcluirItens.ClientID%>');
                    btn.click();
                    return true;
                }
            })
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <ul class="breadcrumb">
        <li><a href="dashboard.aspx"><i class="fa fa-home"></i>&nbsp;Home</a> </li>
        <li>
            <asp:HyperLink ID="LnkCampanhas" runat="server" Text="Campanhas"></asp:HyperLink>
        </li>
        <li class="active">Locais de Adesão</li>
    </ul>
    <h3><i class="fa fa-map-marker"></i>&nbsp;Locais de Adesão</h3>
    <asp:UpdatePanel ID="UpdMain" runat="server">
        <ContentTemplate>
            <script type="text/javascript">
                Sys.Application.add_load(SetSelectBox);
                var select$ = jQuery.noConflict();
                function SetSelectBox() {
                    select$('.select2').select2();
                }
            </script>
            <div class="row">
                <div class="col-md-12">
                    <asp:Panel ID="PnlPesquisa" runat="server" DefaultButton="BtnPesquisar">
                        <div class="box box-success">
                            <div class="box-header with-border">
                                <h3 class="box-title"><i class="fa fa-sliders"></i>&nbsp;Filtros</h3>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" data-container="body" title="Ocultar/exibir filtros">
                                        <i class="fa fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="box-body" style="display: block;">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                        <label>Empresa *</label>
                                        <asp:DropDownList ID="DdlEmpresas" runat="server" CssClass="form-control select2" Enabled="false" DataValueField="ValueMember" DataTextField="DisplayMember"></asp:DropDownList>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                        <label>Campanha *</label>
                                        <asp:DropDownList ID="DdlCampanhas" runat="server" CssClass="form-control select2" Enabled="false" DataValueField="ValueMember" DataTextField="DisplayMember"></asp:DropDownList>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                        <asp:TextBox ID="TxtPesquisa" runat="server" CssClass="form-control" placeHolder="PESQUISA..."></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer" style="display: block;">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 pull-right">
                                        <asp:LinkButton ID="BtnPesquisar" runat="server" CssClass="btn btn-block btn-success" data-toggle="tooltip" data-container="body" ToolTip="Pesquisar registros" CausesValidation="false" OnClick="BtnPesquisar_Click">
                                        <i class="fa fa-search"></i>&nbsp;Pesquisar
                                        </asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 text-left bloco-legenda">
                                    <a href="#" class="dropdown-toggle btn btn-block btn-primary" data-toggle="dropdown">
                                        <i class="fa fa-info-circle"></i>&nbsp;Legenda
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <div style="padding: 8px;">
                                                <i class="fa fa-pencil text-regular"></i>&nbsp;Editar registro
                                            </div>
                                        </li>
                                        <li>
                                            <div style="padding: 8px;">
                                                <i class="fa fa-trash text-regular"></i>&nbsp;Excluir registro
                                            </div>
                                        </li>
                                    </ul>
                                    <div class="dv-spc-10"></div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10 box-resultados text-right conteudo-centro">
                                    <asp:Label ID="LblRegistros" runat="server"></asp:Label>
                                </div>
                            </div>
                            <div class="dv-spc-10"></div>
                            <div class="box-grids">
                                <div class="dv-grid-f-header">
                                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="display: inline-block; float: none;">
                                        Ações
                                    </div>
                                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="display: inline-block; float: none;">
                                        Adesões
                                    </div>
                                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="display: inline-block; float: none;">
                                        Nome
                                    </div>
                                    <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7" style="display: inline-block; float: none;">
                                        Local
                                    </div>
                                </div>
                                <div class="dv-grid-f">
                                    <asp:Repeater ID="RptPrincipal" runat="server" OnItemDataBound="RptPrincipal_ItemDataBound" OnItemCommand="RptPrincipal_ItemCommand">
                                        <ItemTemplate>
                                            <div id="dvItem" runat="server">
                                                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="display: inline-block; float: none;">
                                                    <div class="truncate">
                                                        <asp:LinkButton ID="LnkEditList" runat="server" data-toggle="tooltip" data-container="body" ToolTip="Editar registro" CommandName="edit">
                                                        <i class="fa fa-pencil text-regular"></i>
                                                        </asp:LinkButton>
                                                        <asp:LinkButton ID="LnkDeleteList" runat="server" data-toggle="tooltip" data-container="body" ToolTip="Excluir registro" CommandName="delete">
                                                        <i class="fa fa-trash text-regular"></i>
                                                        </asp:LinkButton>
                                                    </div>
                                                </div>
                                                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="display: inline-block; float: none;">
                                                    <div class="truncate">
                                                        <asp:Label ID="LblTotais" runat="server" Text="0"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="display: inline-block; float: none;">
                                                    <div class="truncate">
                                                        <asp:Label ID="LblNome" runat="server" Text="0"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7" style="display: inline-block; float: none;">
                                                    <div class="truncate">
                                                        <asp:Label ID="LblLocal" runat="server" Text="0"></asp:Label>
                                                    </div>
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                        </div>
                        <div class="dv-spc-10">
                        </div>
                        <div class="box-footer">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                                    <asp:Label ID="LblInfo" runat="server"></asp:Label>
                                    <br />
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                                    <label>Página selecionada</label>&nbsp;
                                    <asp:DropDownList ID="DdlPaginas" runat="server" CssClass="form-control max-drop select2"
                                        AutoPostBack="true" OnSelectedIndexChanged="DdlPaginas_SelectedIndexChanged" data-toggle="tooltip" data-container="body" ToolTip="Escolher página para exibição">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="fixed-button-box">
                <asp:LinkButton ID="BtnNovo" runat="server" CssClass="btn btn-success btn-lg" CausesValidation="False" OnClick="BtnNovo_Click" data-toggle="tooltip">
                    <i class="fa fa-plus"></i>
                </asp:LinkButton>
            </div>
            <asp:LinkButton ID="BtnExcluirItens" runat="server" CssClass="hidden" OnClick="BtnExcluirItens_Click">
            </asp:LinkButton>
            <asp:HiddenField ID="HdfCodigo" runat="server" />
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="BtnPesquisar" />
            <asp:AsyncPostBackTrigger ControlID="RptPrincipal" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
