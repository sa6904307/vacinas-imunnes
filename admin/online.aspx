﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="online.aspx.cs" Inherits="restrict_online" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row">
        <div class="col-md-12">
            <ul class="breadcrumb">
                <li><a href="home.aspx"><i class="fa fa-home" aria-hidden="true"></i>&nbsp;Home</a></li>
                <li class="active">Usuários Online</li>
            </ul>
            <h1><i class="fa fa-circle text-green" aria-hidden="true"></i>&nbsp;Usuários Online</h1>
        </div>
    </div>
    <asp:Timer ID="tmrUsuariosOnline" runat="server" Interval="60000" OnTick="tmrUsuariosOnline_Tick"></asp:Timer>
    <div class="dv-spc-20"></div>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-body" style="display: block;">
                    <div class="row">
                        <asp:UpdatePanel ID="updPrincipal" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:DataList ID="dtlUsuariosOnline" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal">
                                    <ItemTemplate>
                                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                            <div class="box box-widget widget-user-2  bloco-usuario">
                                                <!-- Add the bg color to the header using any of the bg-* classes -->
                                                <div class="widget-user-header bg-green">
                                                    <div class="widget-user-image">
                                                        <img class="img-circle" src="../img/icons/generic.png" alt="User Avatar" height="40">
                                                    </div>
                                                    <!-- /.widget-user-image -->
                                                    <h3 class="widget-user-username">
                                                        <div class="truncate">
                                                            <asp:Label ID="lblNomeUsuario" runat="server" Text='<%# Eval("nomeuser") %>' data-toggle="tooltip" ToolTip='<%# Eval("nomeuser") %>'></asp:Label>
                                                        </div>
                                                    </h3>
                                                    <h5 class="widget-user-desc">
                                                        <div class="truncate">
                                                            <asp:Label ID="lblRole" runat="server" Text='<%# Eval("role") %>'></asp:Label>
                                                        </div>
                                                    </h5>
                                                </div>
                                                <div class="box-footer font-12">
                                                    <div class="col-lg-12">
                                                        <small>
                                                            <asp:Label ID="lblNomeCliente" runat="server" Text='<%# Eval("imobiliaria") %>' CssClass="text-uppercase"></asp:Label></small>
                                                    </div>
                                                    <div class="dv-spc-10 with-border"></div>
                                                    <div class="col-lg-12">
                                                        <small><b>Tempo de sessão</b></small><br />
                                                        <div class="truncate">
                                                            <%#Eval("timeout") %> minutos (<%#Eval("minutosrestantes")%> min. restantes)
                                                        </div>
                                                    </div>
                                                    <div class="dv-spc-10 with-border"></div>
                                                    <div class="col-lg-12">
                                                        <small><b>Página</b></small><br />
                                                        <asp:Label ID="lblPaginaAcesso" runat="server" Text='<%# Eval("ultimapagina") %>'></asp:Label>
                                                    </div>
                                                    <div class="dv-spc-10 with-border"></div>
                                                    <div class="col-lg-12">
                                                        <small><b>Navegador</b></small><br />
                                                        <asp:Label ID="lblNavegador" runat="server" Text='<%# Eval("browser") %>'></asp:Label>
                                                    </div>
                                                    <div class="dv-spc-10 with-border"></div>
                                                    <div class="col-lg-12">
                                                        <small><b>IP</b></small><br />
                                                        <asp:Label ID="lblIP" runat="server" Text='<%# Eval("ip") %>'></asp:Label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:DataList>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="tmrUsuariosOnline" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
