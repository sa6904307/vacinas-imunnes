﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.Web.UI.WebControls;

public partial class admin_baixar_vales : System.Web.UI.Page
{
    protected List<ImunneVacinas.HorarioSurto> listaHorarios = new List<ImunneVacinas.HorarioSurto>();
    private StringBuilder url = new StringBuilder();
    protected int totalMarcados = 0;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = "S&A Imunizações | Baixar Aplicações";

        if (!IsPostBack)
            IniciarPagina();
    }

    /// <summary>
    /// 
    /// </summary>
    private void IniciarPagina()
    {
        NameValueCollection compra = new NameValueCollection();
        int codigoCompra = -1;

        try
        {
            url = new StringBuilder();
            url.AppendFormat("~/admin/aplicacoes.aspx?compra={0}", Request.QueryString["compra"]);
            LnkPai.NavigateUrl = url.ToString();
            BtnCancelar.OnClientClick = "return CancelarFormulario(this, event,'" + url.Replace("~/admin/", "") + "');";

            if (Request.QueryString["compra"] != null)
            {
                compra = ImunneVacinas.Utils.RecuperarQueryString(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["compra"]));
                codigoCompra = Convert.ToInt32(compra["codigo"]);

                ConsultarRegistros(codigoCompra);
            }
        }
        catch (Exception ex)
        {
            this.SalvarException404(ex);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="codigoCompra"></param>
    private void ConsultarRegistros(int codigoCompra)
    {
        ImunneVacinas.PesquisaVoucher pesquisa = new ImunneVacinas.PesquisaVoucher()
        {
            Compra = codigoCompra,
            Ordenacao = ImunneVacinas.Utils.TipoOrdenacao.porDataDesc
        };

        #region DADOS HORÁRIOS

        ImunneVacinas.PesquisaHorarioSurto pesquisaHorarios = new ImunneVacinas.PesquisaHorarioSurto()
        {
            Compra = codigoCompra,
            Situacao = ImunneVacinas.Utils.SituacaoHorario.Confirmado
        };

        listaHorarios = ImunneVacinas.HorarioSurto.ConsultarHorarios(pesquisaHorarios);

        #endregion

        RptPrincipal.DataSource = ImunneVacinas.Voucher.ConsultarVouchers(pesquisa);
        RptPrincipal.DataBind();

        ContarItens();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void RptPrincipal_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        int codigoRegistro = Convert.ToInt32(e.CommandArgument);
        string comando = e.CommandName;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void RptPrincipal_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        try
        {
            ImunneVacinas.Voucher registroSelecionado = (ImunneVacinas.Voucher)e.Item.DataItem;
            ImunneVacinas.Compra compraSelecionada = ImunneVacinas.Compra.ConsultarUnico(registroSelecionado.IdCompra.Codigo);
            ImunneVacinas.Campanha campanhaSelecionada = ImunneVacinas.Campanha.ConsultarUnico(compraSelecionada.IdCampanha.Codigo);
            ImunneVacinas.Participante participanteSelecionado = ImunneVacinas.Participante.ConsultarUnico(compraSelecionada.IdParticipante.Codigo);
            ImunneVacinas.TransacaoRealizada transacaoSelecionada = ImunneVacinas.TransacaoRealizada.ConsultarUnicoCompra(compraSelecionada.Codigo);

            Label LblData = (Label)e.Item.FindControl("LblData");
            Label LblBeneficiario = (Label)e.Item.FindControl("LblBeneficiario");
            Label LblCampanha = (Label)e.Item.FindControl("LblCampanha");
            Label LblNome = (Label)e.Item.FindControl("LblNome");
            Label LblComprovante = (Label)e.Item.FindControl("LblComprovante");

            HiddenField HdfCodigo = (HiddenField)e.Item.FindControl("HdfCodigo");
            LinkButton LnkTransacao = (LinkButton)e.Item.FindControl("LnkTransacao");
            CheckBox ChkUtilizado = (CheckBox)e.Item.FindControl("ChkUtilizado");

            HdfCodigo.Value = registroSelecionado.Codigo.ToString();

            if (registroSelecionado.DataRetirada == ImunneVacinas.Utils.dataPadrao)
            {
                LblData.Text = "--";
                ChkUtilizado.Text = "DISPONIVEL";
            }
            else
            {
                LblData.Text = registroSelecionado.DataRetirada.ToString("dd/MM/yyyy HH:mm");
                ChkUtilizado.Text = "UTILIZADO";
                ChkUtilizado.Checked = true;
                ChkUtilizado.Enabled = false;
                totalMarcados += 1;
            }

            LblCampanha.Text = campanhaSelecionada.Identificacao;

            StringBuilder nomeParticipante = new StringBuilder();
            nomeParticipante.Append(registroSelecionado.IdParticipante.Nome);

            foreach (ImunneVacinas.HorarioSurto horario in listaHorarios)
            {
                if (horario.Participante == registroSelecionado.IdParticipante.Codigo)
                {
                    nomeParticipante.AppendFormat(" | AGENDADO PARA {0}", horario.InfoHoraro.ToString("dd/MM/yyyy HH:mm"));
                    break;
                }
            }

            LblBeneficiario.Text = nomeParticipante.ToString(); //registroSelecionado.IdParticipante.Nome;
            LblNome.Text = participanteSelecionado.Nome;

            if (transacaoSelecionada != null)
            {
                if (!String.IsNullOrEmpty(transacaoSelecionada.numeroComprovanteVenda))
                    LblComprovante.Text = transacaoSelecionada.numeroComprovanteVenda;
            }
        }
        catch (Exception ex)
        {
            this.SalvarException(ex);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BtnSalvar_Click(object sender, EventArgs e)
    {
        NameValueCollection compra = new NameValueCollection();
        int codigoCompra = -1;

        if (Request.QueryString["compra"] != null)
        {
            compra = ImunneVacinas.Utils.RecuperarQueryString(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["compra"]));
            codigoCompra = Convert.ToInt32(compra["codigo"]);

            ImunneVacinas.Compra compraSelecionada = ImunneVacinas.Compra.ConsultarUnico(codigoCompra);
            ImunneVacinas.Campanha campanhaSelecionada = ImunneVacinas.Campanha.ConsultarUnico(compraSelecionada.IdCampanha.Codigo);
            ImunneVacinas.Participante participanteSelecionado = ImunneVacinas.Participante.ConsultarUnico(compraSelecionada.IdParticipante.Codigo);

            int totalDoses = 0;

            #region DADOS DA COMPRA

            int contadorTotalItens = RptPrincipal.Items.Count;
            int contatorTotalBaixas = 0;

            foreach (RepeaterItem item in RptPrincipal.Items)
            {
                HiddenField HdfCodigo = (HiddenField)item.FindControl("HdfCodigo");
                CheckBox ChkUtilizado = (CheckBox)item.FindControl("ChkUtilizado");

                if (ChkUtilizado.Enabled == true && ChkUtilizado.Checked == true)
                {
                    try
                    {
                        ImunneVacinas.Voucher voucherSelecionado = ImunneVacinas.Voucher.ConsultarUnico(Convert.ToInt32(HdfCodigo.Value));
                        voucherSelecionado.DataRetirada = DateTime.Now;
                        voucherSelecionado.Situacao = ImunneVacinas.Voucher.SituacaoVoucher.Utilizado;
                        voucherSelecionado.Alterar();

                        totalDoses += 1;
                        contadorTotalItens += 1;
                    }
                    catch (Exception ex)
                    {
                        this.SalvarException(ex);
                    }
                }
            }

            #endregion

            #region BAIXA EM ESTOQUE

            if (totalDoses > 0)
            {
                ImunneVacinas.PesquisaItemCompra pesquisaItem = new ImunneVacinas.PesquisaItemCompra()
                {
                    Campanha = campanhaSelecionada.Codigo,
                    Compra = compraSelecionada.Codigo
                };

                List<ImunneVacinas.ItemCompra> listaItens = ImunneVacinas.ItemCompra.ConsultarItens(pesquisaItem);
                foreach (ImunneVacinas.ItemCompra item in listaItens)
                {
                    ImunneVacinas.MovimentacaoEstoque novaMovimentacao = new ImunneVacinas.MovimentacaoEstoque();
                    novaMovimentacao.Tipo = ImunneVacinas.Utils.TipoMovimentacao.Saida;

                    novaMovimentacao.Usuario.Codigo = this.Page.ConsultarSessionCodigoUsuario();
                    if (novaMovimentacao.Usuario.Codigo <= 0)
                        novaMovimentacao.Usuario.Codigo = 14;

                    novaMovimentacao.Clinica.Codigo = compraSelecionada.IdClinica.Codigo;
                    novaMovimentacao.DataCriacao = DateTime.Now;
                    novaMovimentacao.Quantidade = totalDoses;
                    novaMovimentacao.Produto.Codigo = item.IdVacina.Codigo;
                    novaMovimentacao.Historico = (String.Format("{0} DOS(ES) APLICADA(S) NA CAMPANHA {1}, PRODUTO {3}, COMPRADOR {2}.", totalDoses, campanhaSelecionada.Identificacao, participanteSelecionado.Nome, item.IdVacina.Nome)).ToUpper();
                    novaMovimentacao.Incluir();
                }
            }
            else
            {
                ImunneVacinas.Alerta.showMensagemAviso(this.Page, "Aviso!", "Nenhum item foi baixado. Tente novamente ou informe ao suporte.");
                return;
            }

            #endregion

            #region BAIXA DA COMPRA GERAL

            BaixarCompra(codigoCompra, RptPrincipal.Items.Count);

            #endregion

            Response.Redirect(LnkPai.NavigateUrl, false);
            Context.ApplicationInstance.CompleteRequest();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void ContarItens()
    {
        int totalItens = RptPrincipal.Items.Count;
        int totalSelecoes = 0;

        foreach (RepeaterItem item in RptPrincipal.Items)
        {
            CheckBox ChkUtilizado = (CheckBox)item.FindControl("ChkUtilizado");

            if (ChkUtilizado.Enabled == false)
                totalSelecoes += 1;
        }

        if (totalItens == totalSelecoes)
            BtnConcluirVale.Visible = false;
    }

    /// <summary>
    /// Baixa a compra completa, em caso de todos os itens selecionados
    /// </summary>
    /// <param name="codigoCompra"></param>
    /// <param name="totalRegistros"></param>
    private void BaixarCompra(int codigoCompra, int totalRegistros)
    {
        ImunneVacinas.Compra compraSelecionada = ImunneVacinas.Compra.ConsultarUnico(codigoCompra);

        try
        {
            ImunneVacinas.PesquisaVoucher pesquisa = new ImunneVacinas.PesquisaVoucher()
            {
                Situacao = ImunneVacinas.Voucher.SituacaoVoucher.Utilizado,
                Compra = codigoCompra,
                Ordenacao = ImunneVacinas.Utils.TipoOrdenacao.porData
            };
            List<ImunneVacinas.Voucher> listaVouchers = ImunneVacinas.Voucher.ConsultarVouchers(pesquisa);
            if (listaVouchers != null && listaVouchers.Count > 0)
            {
                if (listaVouchers.Count == totalRegistros)
                {
                    compraSelecionada.AplicacaoCompra = ImunneVacinas.Compra.Aplicacao.Aplicado;
                    compraSelecionada.DataAplicacao = DateTime.Now;
                    compraSelecionada.Alterar();
                }
            }
        }
        catch (Exception ex)
        {
            this.SalvarException(ex);
        }
    }
}