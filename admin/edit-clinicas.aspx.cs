﻿using System;
using System.Collections.Specialized;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class edit_clinicas : System.Web.UI.Page
{
    private StringBuilder url = new StringBuilder();
    private string textoMensagem = String.Empty;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            IniciarPagina();
    }

    /// <summary>
    /// 
    /// </summary>
    private void IniciarPagina()
    {
        NameValueCollection clinica = new NameValueCollection();
        int codigoRegistro = -1;
        string comando = String.Empty;

        if (Request.QueryString["clinica"] != null)
        {
            clinica = ImunneVacinas.Utils.RecuperarQueryString(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["clinica"]));

            url = new StringBuilder();
            url.AppendFormat("~/admin/clinicas.aspx?clinica={0}", Request.QueryString["clinica"]);
            LnkPai.NavigateUrl = url.ToString();
            BtnCancelar.OnClientClick = "return CancelarFormulario(this, event,'" + url.Replace("~/admin/", "") + "');";

            codigoRegistro = Convert.ToInt32(clinica["codigo"]);
            comando = clinica["acao"].ToString();

            DdlUfs.DataSource = ImunneVacinas.Uf.ConsultarComboBox(true, "UF");
            DdlUfs.DataBind();

            DdlCidades.Items.Add(new ListItem("CIDADES", "-1"));
            DdlCidades.Enabled = false;

            TxtCodigo.Text = codigoRegistro.ToString();

            PopularCampos();

            LblForms.Text = ImunneVacinas.Utils.MontarTituloFormulario(comando);

            switch (comando)
            {
                case "edit":
                    {
                        ConsultarDetalhes(codigoRegistro);
                    }
                    break;
            }

            Page.Title = "S&A Imunizações | " + LblForms.Text;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void PopularCampos()
    {
        TxtNome.MaxLength = (int)ImunneVacinas.Clinica.GetInfoNomeFantasia().Max_length;
        TxtBairro.MaxLength = (int)ImunneVacinas.Clinica.GetInfoBairro().Max_length;
        TxtNumero.MaxLength = (int)ImunneVacinas.Clinica.GetInfoNumero().Max_length;
        TxtComplemento.MaxLength = (int)ImunneVacinas.Clinica.GetInfoComplemento().Max_length;
        TxtEndereco.MaxLength = (int)ImunneVacinas.Clinica.GetInfoEndereco().Max_length;
        TxtContatoComercial.MaxLength = (int)ImunneVacinas.Clinica.GetInfoContatoComercial().Max_length;
        TxtContatoOperacional.MaxLength = (int)ImunneVacinas.Clinica.GetInfoContatoOperacional().Max_length;
        TxtEmailFinanceiro.MaxLength = (int)ImunneVacinas.Clinica.GetInfoEmailFinanceiro().Max_length;
        TxtEmailGeral.MaxLength = (int)ImunneVacinas.Clinica.GetInfoEmailGeral().Max_length;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="codigo"></param>
    private void ConsultarDetalhes(int codigo)
    {
        ImunneVacinas.Clinica registroSelecionado = ImunneVacinas.Clinica.ConsultarUnico(codigo);

        TxtBairro.Text = registroSelecionado.Bairro;
        TxtCEP.Text = registroSelecionado.Cep;
        TxtCodigo.Text = codigo.ToString();
        TxtComplemento.Text = registroSelecionado.Complemento;
        TxtCpfCnpj.Text = ImunneVacinas.Utils.FormatarCpfCnpj(registroSelecionado.CpfCnpj);
        TxtEndereco.Text = registroSelecionado.Endereco;
        TxtNome.Text = registroSelecionado.NomeFantasia;
        TxtNumero.Text = registroSelecionado.Numero;
        TxtEmailFinanceiro.Text = registroSelecionado.EmailFinanceiro;
        TxtEmailGeral.Text = registroSelecionado.EmailGeral;
        TxtContatoComercial.Text = registroSelecionado.ContatoComercial;
        TxtContatoOperacional.Text = registroSelecionado.ContatoOperacional;
        TxtTelefone.Text = registroSelecionado.Telefone;
        TxtObservacoes.Text = registroSelecionado.Observacoes;

        DdlUfs.SelectedValue = registroSelecionado.IdUf.Sigla;

        if (!String.IsNullOrEmpty(registroSelecionado.IdUf.Sigla) && registroSelecionado.IdUf.Sigla != "-1")
        {
            DdlCidades.DataSource = ImunneVacinas.Cidade.ConsultarComboBox(registroSelecionado.IdUf.Sigla, true, "SELECIONE...");
            DdlCidades.DataBind();
            DdlCidades.Enabled = true;
            DdlCidades.SelectedValue = registroSelecionado.IdCidade.Codigo.ToString();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void TxtCEP_TextChanged(object sender, EventArgs e)
    {
        try
        {
            ImunneVacinas.Cep cepRetorno = ImunneVacinas.Cep.ConsultarCep(TxtCEP.Text.Replace("-", ""));

            if (cepRetorno != null)
            {
                string[] arrayLogradouro = cepRetorno.logradouro.Split(',');

                if (!String.IsNullOrEmpty(cepRetorno.estado.sigla))
                {
                    DdlUfs.SelectedValue = cepRetorno.estado.sigla;
                    DdlUfs.Enabled = false;

                    if (!String.IsNullOrEmpty(cepRetorno.cidade.nome))
                    {
                        DdlCidades.Items.Clear();
                        DdlCidades.DataSource = ImunneVacinas.Cidade.ConsultarComboBox(cepRetorno.estado.sigla, false, "");
                        DdlCidades.DataBind();
                        string nomeCidade = ImunneVacinas.Utils.RemoverAcentos(cepRetorno.cidade.nome).ToUpper();

                        DdlCidades.SelectedValue = DdlCidades.Items.FindByText(nomeCidade).Value;
                        DdlCidades.Enabled = false;
                    }
                }

                // Bairro
                if (!String.IsNullOrEmpty(cepRetorno.bairro))
                {
                    TxtBairro.Text = ImunneVacinas.Utils.RemoverAcentos(cepRetorno.bairro.ToUpper());
                    TxtBairro.Enabled = false;

                    // Logradouro
                    if (!String.IsNullOrEmpty(arrayLogradouro[0]))
                        TxtEndereco.Text = ImunneVacinas.Utils.RemoverAcentos(arrayLogradouro[0].ToUpper());
                }
                else
                {
                    TxtBairro.Text = "";
                    TxtEndereco.Text = "";
                    TxtComplemento.Text = "";
                }

                // Coordenadas
                HdfLatitude.Value = cepRetorno.latitude;
                HdfLongitude.Value = cepRetorno.longitude;

                TxtNumero.Focus();
            }
            else return;
        }
        catch (Exception ex)
        {
            //this.salvaException(ex);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void DdlUfs_SelectedIndexChanged(object sender, EventArgs e)
    {
        string sigalUf = DdlUfs.SelectedValue;

        DdlCidades.Items.Clear();
        DdlCidades.Items.Add(new ListItem("SELECIONE...", "-1"));
        DdlCidades.Enabled = false;

        if (!String.IsNullOrEmpty(sigalUf) && sigalUf != "-1")
        {
            DdlCidades.DataSource = ImunneVacinas.Cidade.ConsultarComboBox(sigalUf, true, "SELECIONE...");
            DdlCidades.DataBind();
            DdlCidades.Enabled = true;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BtnSalvar_Click(object sender, EventArgs e)
    {
        try
        {
            NameValueCollection clinica = new NameValueCollection();

            clinica = ImunneVacinas.Utils.RecuperarQueryString(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["clinica"]));

            int codigoRegistro = ImunneVacinas.Parser.parseInteiro(clinica["codigo"]);
            string comando = clinica["acao"].ToString();

            ImunneVacinas.Clinica novoRegistro;
            ImunneVacinas.Clinica registroOriginal = new ImunneVacinas.Clinica();
            if (codigoRegistro > 0)
            {
                novoRegistro = ImunneVacinas.Clinica.ConsultarUnico(codigoRegistro);
                registroOriginal = ImunneVacinas.Clinica.ConsultarUnico(codigoRegistro);
            }
            else
                novoRegistro = new ImunneVacinas.Clinica();

            novoRegistro.NomeFantasia = TxtNome.Text;
            novoRegistro.CpfCnpj = ImunneVacinas.Utils.LimparCpfCnpj(TxtCpfCnpj.Text);
            novoRegistro.Cep = TxtCEP.Text;
            novoRegistro.Complemento = TxtComplemento.Text;
            novoRegistro.ContatoComercial = TxtContatoComercial.Text;
            novoRegistro.ContatoComercial = TxtContatoOperacional.Text;
            novoRegistro.EmailFinanceiro = TxtEmailFinanceiro.Text;
            novoRegistro.EmailGeral = TxtEmailGeral.Text;
            novoRegistro.Endereco = TxtEndereco.Text;
            novoRegistro.IdCidade.Codigo = Convert.ToInt32(DdlCidades.SelectedValue);
            novoRegistro.IdUf.Sigla = DdlUfs.SelectedValue;
            novoRegistro.Latitude = HdfLatitude.Value;
            novoRegistro.Longitude = HdfLongitude.Value;
            novoRegistro.Numero = TxtNumero.Text;
            novoRegistro.Bairro = TxtBairro.Text;
            novoRegistro.Telefone = TxtTelefone.Text;
            novoRegistro.Observacoes = TxtObservacoes.Text;

            int resultado = 0;

            switch (comando)
            {
                case "insert":
                    {
                        novoRegistro.DataCriacao = DateTime.Now;
                        resultado = novoRegistro.Incluir();
                    }
                    break;
                case "edit":
                    {
                        ImunneVacinas.Clinica dadosAtuais = ImunneVacinas.Clinica.ConsultarUnico(codigoRegistro);
                        novoRegistro.Codigo = Convert.ToInt32(codigoRegistro);
                        novoRegistro.DataAlteracao = DateTime.Now;
                        resultado = novoRegistro.Alterar();
                    }
                    break;
            }

            if (resultado <= 0)
            {
                textoMensagem = "Lamentamos mas ocorreu um erro e não foi possível concluir a operação. Tente novamente ou informe o suporte.";
                ImunneVacinas.Alerta.showMensagemErro(this.Page, null, textoMensagem);
                return;
            }

            #region Geração de Logs

            // Log descritivo
            ImunneVacinas.Log logAcao = new ImunneVacinas.Log();
            logAcao.Tela = Path.GetFileName(Request.PhysicalPath);
            //logAcao.IdClinica.Codigo = this.Page.ConsultarSessionCodigoClinica();
            logAcao.TabelaAcao = "Clinicas";

            switch (comando)
            {
                case "insert":
                    {
                        logAcao.TipoAcao = ImunneVacinas.Utils.AcaoLog.Insercao;
                        logAcao.Registro = resultado;
                        logAcao.Resumo = String.Format("INSERÇÃO DE NOVO REGISTRO {0}.", logAcao.Registro);
                        logAcao.DataAcao = DateTime.Now;
                        logAcao.Campo = String.Empty;
                        logAcao.ValorOriginal = String.Empty;
                        logAcao.ValorNovo = String.Empty;
                        logAcao.Incluir();
                    }
                    break;
                case "edit":
                    {
                        logAcao.TipoAcao = ImunneVacinas.Utils.AcaoLog.Edicao;
                        //ImunneVacinas.RegistraLog.CompararClinica(logAcao, registroOriginal, novoRegistro);
                    }
                    break;
            }

            #endregion

            url = new StringBuilder();
            url.AppendFormat("~/admin/clinicas.aspx?clinica={0}", Request.QueryString["clinica"]);
            Response.Redirect(url.ToString(), false);
            Context.ApplicationInstance.CompleteRequest();
        }
        catch (Exception ex)
        {
            textoMensagem = "Lamentamos mas ocorreu um erro e não foi possível concluir a operação. Tente novamente ou informe o suporte.";
            ImunneVacinas.Alerta.showMensagemErro(this.Page, null, textoMensagem);
            this.SalvarException(ex);
        }
    }
}