﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_importacao_vouchers : System.Web.UI.Page
{
    private StringBuilder url = new StringBuilder();
    private string textoMensagem = String.Empty;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = "S&A Imunizações | Processar Vendas";
        ScriptManager.GetCurrent(Page).RegisterPostBackControl(LnkUpload);

        if (!IsPostBack)
            IniciarPagina();
    }

    /// <summary>
    /// 
    /// </summary>
    private void IniciarPagina()
    {
        NameValueCollection clinica = new NameValueCollection();
        int role = this.Page.ConsultarSessionTipoUsuario();
        int codigoClinica = this.Page.ConsultarClinicaUsuario();

        try
        {
            DdlClinicas.DataSource = ImunneVacinas.Clinica.ConsultarComboBox(true, "CLINICAS");
            DdlClinicas.DataBind();

            if (role == 7)
            {
                DdlClinicas.SelectedValue = codigoClinica.ToString();
                DdlClinicas.Enabled = false;
            }
            else DdlClinicas.Enabled = true;
        }
        catch (Exception ex)
        {
            this.SalvarException404(ex);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void RptResumoImportacoes_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        try
        {
            ImunneVacinas.RetornoProcesso item = (ImunneVacinas.RetornoProcesso)e.Item.DataItem;

            Label LblLinha = (Label)e.Item.FindControl("LblLinha");
            Label LblClassificacao = (Label)e.Item.FindControl("LblClassificacao");
            Label LblResumo = (Label)e.Item.FindControl("LblResumo");

            LblLinha.Text = item.Linha;
            LblClassificacao.Text = item.Classificacao;
            LblResumo.Text = item.Resumo;

            switch (item.Classificacao)
            {
                case "ERRO":
                    {
                        LblLinha.CssClass = "text-red";
                        LblClassificacao.CssClass = "text-red";
                        LblResumo.CssClass = "text-red";
                    }
                    break;
                case "AVISO":
                    {
                        LblLinha.CssClass = "text-orange";
                        LblClassificacao.CssClass = "text-orange";
                        LblResumo.CssClass = "text-orange";
                    }
                    break;
                case "SUCESSO":
                    {
                        LblLinha.CssClass = "text-green";
                        LblClassificacao.CssClass = "text-green";
                        LblResumo.CssClass = "text-green";
                    }
                    break;
            }
        }
        catch (Exception ex)
        {
            this.SalvarException(ex);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BtnExportarResumo_Click(object sender, EventArgs e)
    {
        try
        {
            if (RptResumoImportacoes.Items.Count <= 0)
            {
                ImunneVacinas.Alerta.showMensagemAviso(this.Page, "Aviso", "Infelizmente não é possível gerar a carga de retornos pois não existe nenhuma informação.");
                return;
            }

            ImunneVacinas.ExportacoesExcel.GerarExcelRetornosOperacoes(RptResumoImportacoes.Items);
        }
        catch (Exception ex)
        {
            ImunneVacinas.Alerta.showMensagemAviso(this.Page, "Aviso", "Infezlimente não foi possível exportar os dados.");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void LnkUpload_Click(object sender, EventArgs e)
    {
        ProcessarVendas();
    }

    /// <summary>
    /// 
    /// </summary>
    protected void ProcessarVendas()
    {
        int tamanhoArquivo = 47784566;
        string mensagemArquivo = "Para uma melhor utilização da plataforma, o arquivo a ser processado precisa ter no máximo 40MB.";

        bool flagCvs = true;

        int codigoClinica = Convert.ToInt32(DdlClinicas.SelectedValue);
        int contador = 1;

        string linhaArquivo = String.Empty;
        string caminhoArquivo = Server.MapPath("~/files/");
        string nomeArquivo = String.Empty;
        string extensaoArquivo = String.Empty;

        DateTime dataOperacao = DateTime.Now;

        List<ImunneVacinas.RetornoProcesso> listaRetornos = new List<ImunneVacinas.RetornoProcesso>();
        ImunneVacinas.RetornoProcesso linhaRetorno;

        // Para leitura de XLS ou XLSX
        DataSet dataSet = new DataSet();
        DataTable dtArquivo = new DataTable();

        try
        {
            if (UplArquivo.FileBytes.Length > tamanhoArquivo)
            {
                ImunneVacinas.Alerta.showMensagemAviso(this.Page, "Aviso!", mensagemArquivo);
                return;
            }

            // Consultando dados do cliente que está realizando o upload do arquivo.
            // Montando o nome do arquivo com os dados de Data e Hora (ddMMyyyyHHmmss),
            // e realizando o salvamento físico do arquivo na pasta destinao
            extensaoArquivo = Path.GetExtension(UplArquivo.FileName);
            nomeArquivo = String.Format("COMPRAS_{0}{1}", DateTime.Now.ToString("ddMMyyyyHHmmss"), extensaoArquivo);
            UplArquivo.SaveAs(caminhoArquivo + nomeArquivo);

            Encoding encodeArquivo = ImunneVacinas.Utils.ConsultarEncodeArquivo(caminhoArquivo + nomeArquivo);

            if (extensaoArquivo != ".xls" && extensaoArquivo != ".xlsx")
                ImunneVacinas.Alerta.showMensagemAlerta(this.Page, "Aviso", "Somente arquivos dos tipos XLS e XLSX serão aceitos neste processo!");
            else
            {
                // Lendo os arquivos de fontes de dados, transformando em DataTable para otimização da leitura
                switch (extensaoArquivo)
                {
                    case ".xls":
                    case ".xlsx":
                        {
                            dtArquivo = ImunneVacinas.Importacao.ConverterXLSParaDataTable(caminhoArquivo + nomeArquivo);
                        }
                        break;
                }

                listaRetornos = ImunneVacinas.Importacao.ImportarCompras(codigoClinica, dtArquivo);
                flagCvs = false;
                ImunneVacinas.Alerta.showMensagemSucesso(this.Page, "Processamento Concluído", "Carga de dados processada com sucesso!");
            }
        }
        catch (Exception ex)
        {
            linhaRetorno = new ImunneVacinas.RetornoProcesso()
            {
                Linha = String.Format("LINHA {0}", contador),
                Classificacao = "ERRO",
                Resumo = ex.Message
            };

            ImunneVacinas.Erro erro = new ImunneVacinas.Erro();
            erro.addArquivo(ex);
        }

        PnlResumoImportacoes.Visible = true;
        RptResumoImportacoes.DataSource = listaRetornos;
        RptResumoImportacoes.DataBind();

        if (flagCvs == false)
            File.Delete(caminhoArquivo + nomeArquivo);
    }
}