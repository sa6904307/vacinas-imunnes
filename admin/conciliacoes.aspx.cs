﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_conciliacoes : System.Web.UI.Page
{
    private StringBuilder url = new StringBuilder();
    private StringBuilder parametros = new StringBuilder();

    #region Private Proprieties

    private int PaginaAtual
    {
        get
        {
            object objPage = ViewState["_PaginaAtual"];
            int _PaginaAtual = 0;
            if (objPage == null)
            {
                _PaginaAtual = 0;
            }
            else
            {
                _PaginaAtual = (int)objPage;
            }
            return _PaginaAtual;
        }
        set
        {
            ViewState["_PaginaAtual"] = value;
        }
    }
    private int PrimeiroIndice
    {
        get
        {

            int _PrimeiroIndice = 0;
            if (ViewState["_PrimeiroIndice"] == null)
            {
                _PrimeiroIndice = 0;
            }
            else
            {
                _PrimeiroIndice = Convert.ToInt32(ViewState["_PrimeiroIndice"]);
            }
            return _PrimeiroIndice;
        }
        set
        {
            ViewState["_PrimeiroIndice"] = value;
        }
    }
    private int UltimoIndice
    {
        get
        {

            int _UltimoIndice = 0;
            if (ViewState["_UltimoIndice"] == null)
            {
                _UltimoIndice = 0;
            }
            else
            {
                _UltimoIndice = Convert.ToInt32(ViewState["_UltimoIndice"]);
            }
            return _UltimoIndice;
        }
        set
        {
            ViewState["_UltimoIndice"] = value;
        }
    }

    #endregion

    #region PagedDataSource

    PagedDataSource _PagedDataSource = new PagedDataSource();

    #endregion

    #region Private Methods

    /// <summary>
    /// 
    /// </summary>
    private void VincularItens()
    {
        try
        {
            List<ImunneVacinas.Compra> retorno = ConsultarRegistros();

            _PagedDataSource.DataSource = retorno;
            _PagedDataSource.AllowPaging = true;
            _PagedDataSource.PageSize = Convert.ToInt32(DdlItens.SelectedValue);
            _PagedDataSource.CurrentPageIndex = PaginaAtual;
            ViewState["TotalPages"] = _PagedDataSource.PageCount;

            DdlPaginas.Items.Clear();
            for (int x = 0; x < _PagedDataSource.PageCount; x++)
            {
                DdlPaginas.Items.Add(new ListItem((x + 1).ToString(), x.ToString()));
            }

            DdlPaginas.SelectedValue = PaginaAtual.ToString();

            RptPrincipal.DataSource = _PagedDataSource;
            RptPrincipal.DataBind();
            CriarPaginacao();

            LblRegistros.Text = ImunneVacinas.Utils.MontarLinhaTotalizadora(retorno.Count);
            LblInfo.Text = "Exibindo página " + (PaginaAtual + 1) + " de " + _PagedDataSource.PageCount + ".";
        }
        catch (Exception Ex)
        {
            this.SalvarException(Ex);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void CriarPaginacao()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("PageIndex");
        dt.Columns.Add("PageText");

        PrimeiroIndice = PaginaAtual - 5;


        if (PaginaAtual > 5)
        {
            UltimoIndice = PaginaAtual + 5;
        }
        else
        {
            UltimoIndice = 10;
        }
        if (UltimoIndice > Convert.ToInt32(ViewState["TotalPages"]))
        {
            UltimoIndice = Convert.ToInt32(ViewState["TotalPages"]);
            PrimeiroIndice = UltimoIndice - 10;
        }

        if (PrimeiroIndice < 0)
        {
            PrimeiroIndice = 0;
        }

        for (int i = PrimeiroIndice; i < UltimoIndice; i++)
        {
            DataRow dr = dt.NewRow();
            dr[0] = i;
            dr[1] = i + 1;
            dt.Rows.Add(dr);
        }

        DdlPaginas.SelectedValue = PaginaAtual.ToString();
    }

    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = "S&A Imunizações | Conciliações";
        if (!IsPostBack)
            IniciarPagina();
    }

    /// <summary>
    /// 
    /// </summary>
    private void IniciarPagina()
    {
        int codigoClinica = this.Page.ConsultarClinicaUsuario();
        int codigoCampanha = -1;
        int roleUsuario = this.Page.ConsultarSessionTipoUsuario();
        int paginaAtual = 0;
        int itens = 20;
        int liberacoes = -1;

        string inicioData = DateTime.Now.ToString("dd/MM/yyyy");
        string terminoData = DateTime.Now.ToString("dd/MM/yyyy");
        string pesquisa = String.Empty;
        string categoria = "-1";
        string situacao = "3";
        string aplicacao = "-1";
        string ordenacao = "porDataDesc";

        try
        {
            NameValueCollection compra = new NameValueCollection();

            #region PARÂMETROS DE RETORNO DE TELA

            if (!String.IsNullOrEmpty(Request.QueryString["compra"]))
            {
                compra = ImunneVacinas.Utils.RecuperarQueryString(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["compra"]));
                inicioData = compra["iniciodata"];
                terminoData = compra["terminodata"];
                situacao = compra["situacao"];
                aplicacao = compra["aplicacao"];
                liberacoes = Convert.ToInt32(compra["liberacoes"]);
                categoria = compra["categoria"];
                pesquisa = compra["pesquisa"];
                codigoClinica = Convert.ToInt32(compra["clinica"]);
                codigoCampanha = Convert.ToInt32(compra["campanha"]);
                itens = Convert.ToInt32(compra["itens"]);
                paginaAtual = Convert.ToInt32(compra["current_page"]);
            }
            else
            {
                if (roleUsuario == 7)
                {
                    inicioData = String.Format("01/01/{0}", DateTime.Now.Year);
                    terminoData = DateTime.Now.ToString("dd/MM/yyyy");
                }
            }

            terminoData = DateTime.Now.ToString("dd/MM/yyyy");
            inicioData = DateTime.Now.AddDays(-30).ToString("dd/MM/yyyy"); //String.Format("01/01/{0}", DateTime.Now.Year);

            #endregion

            #region Ordenações

            foreach (ImunneVacinas.Utils.TipoOrdenacao r in Enum.GetValues(typeof(ImunneVacinas.Utils.TipoOrdenacao)))
            {
                ListItem item = new ListItem(Enum.GetName(typeof(ImunneVacinas.Utils.TipoOrdenacao), r), r.ToString());
                DdlOrdenacoes.Items.Add(item);
            }

            DdlOrdenacoes.SelectedValue = ordenacao;

            #endregion
            DdlLiberacoes.Items.Add(new ListItem("LIBERAÇÕES", "-1"));
            DdlLiberacoes.Items.Add(new ListItem("À LIBERAR", "1"));
            DdlLiberacoes.Items.Add(new ListItem("LIBERADOS", "2"));
            DdlLiberacoes.SelectedValue = "-1";

            DdlClinicas.DataSource = ImunneVacinas.Clinica.ConsultarComboBox(true, "CLINICAS");
            DdlClinicas.DataBind();
            DdlClinicas.SelectedValue = codigoClinica.ToString();

            DdlSituacoesCompras.Items.Add(new ListItem("SITUAÇÕES", "-1"));
            DdlSituacoesCompras.Items.Add(new ListItem("NÃO REALIZADAS", "1"));
            DdlSituacoesCompras.Items.Add(new ListItem("NÃO AUTORIZADAS", "2"));
            DdlSituacoesCompras.Items.Add(new ListItem("AUTORIZADAS", "3"));
            DdlSituacoesCompras.SelectedValue = "3";

            DdlCategorias.Items.Add(new ListItem("CATEGORIAS", "-1"));
            DdlCategorias.Items.Add(new ListItem("PAGAS", "1"));
            DdlCategorias.Items.Add(new ListItem("IMPORTADAS", "3"));
            DdlCategorias.SelectedValue = "1";

            DdlUtilizacoes.Items.Add(new ListItem("APLICAÇÕES", "-1"));
            DdlUtilizacoes.Items.Add(new ListItem("NÃO APLICADAS", "0"));
            DdlUtilizacoes.Items.Add(new ListItem("APLICADAS", "1"));
            DdlUtilizacoes.SelectedValue = "-1";

            HdfFinal.Value = terminoData;
            HdfInicio.Value = inicioData;

            DdlItens.Items.Add(new ListItem("MOSTRAR 10", "10"));
            DdlItens.Items.Add(new ListItem("MOSTRAR 20", "20"));
            DdlItens.Items.Add(new ListItem("MOSTRAR 30", "30"));
            DdlItens.Items.Add(new ListItem("MOSTRAR 40", "40"));
            DdlItens.Items.Add(new ListItem("MOSTRAR 50", "50"));
            DdlItens.Items.Add(new ListItem("MOSTRAR 100", "100"));
            DdlItens.SelectedValue = itens.ToString();

            DdlCampanhas.Items.Clear();

            #region CAMPANHAS DA CLÍNICA DO USUÁRIO

            if (roleUsuario == 7)
            {
                DdlClinicas.SelectedValue = codigoClinica.ToString();
                DdlClinicas.Enabled = false;

                codigoClinica = Convert.ToInt32(DdlClinicas.SelectedValue);
                ImunneVacinas.PesquisaCampanhaClinica pesquisaCampanha = new ImunneVacinas.PesquisaCampanhaClinica()
                {
                    Clinica = codigoClinica,
                    Ordenacao = ImunneVacinas.Utils.TipoOrdenacao.porNome
                };

                DdlCampanhas.DataSource = ImunneVacinas.CampanhaClinica.ConsultarCampanhasComboBox(pesquisaCampanha, true, "CAMPANHAS");
                DdlCampanhas.DataBind();
            }
            else
            {
                DdlCampanhas.Items.Clear();
                DdlCampanhas.DataSource = ImunneVacinas.Campanha.ConsultarComboBox(true, "CAMPANHAS");
                DdlCampanhas.DataBind();

                if (codigoClinica > 0)
                {
                    DdlClinicas.SelectedValue = codigoClinica.ToString();

                    ImunneVacinas.PesquisaCampanhaClinica pesquisaCampanha = new ImunneVacinas.PesquisaCampanhaClinica()
                    {
                        Clinica = codigoClinica,
                        Ordenacao = ImunneVacinas.Utils.TipoOrdenacao.porNome
                    };

                    DdlCampanhas.DataSource = ImunneVacinas.CampanhaClinica.ConsultarCampanhasComboBox(pesquisaCampanha, true, "CAMPANHAS");
                    DdlCampanhas.DataBind();
                    DdlCampanhas.Enabled = true;
                    DdlCampanhas.SelectedValue = codigoCampanha.ToString();
                }
            }

            #endregion

            DdlCampanhas.SelectedValue = codigoCampanha.ToString();

            if (!String.IsNullOrEmpty(pesquisa))
                TxtPesquisa.Text = pesquisa;

            //if (categoria == "1")
            //    DdlLiberacoes.Enabled = true;
            //else DdlLiberacoes.Enabled = false;

            //if (liberacoes == 1)
            //{
            //    DdlUtilizacoes.SelectedValue = "0";
            //    DdlUtilizacoes.Enabled = false;
            //}
            //else
            //{
            //    DdlUtilizacoes.SelectedValue = "1";
            //    DdlUtilizacoes.Enabled = true;
            //}

            PaginaAtual = paginaAtual;
            VincularItens();
        }
        catch (Exception ex)
        {
            this.SalvarException404(ex);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    private List<ImunneVacinas.Compra> ConsultarRegistros()
    {
        DateTime dataInicio = ImunneVacinas.Utils.dataPadrao;
        DateTime dataFinal = ImunneVacinas.Utils.dataPadrao;

        if (!String.IsNullOrEmpty(HdfInicio.Value) && !String.IsNullOrEmpty(HdfFinal.Value))
        {
            dataInicio = Convert.ToDateTime(HdfInicio.Value);
            dataFinal = Convert.ToDateTime(HdfFinal.Value).AddDays(1);
        }

        ImunneVacinas.PesquisaCompra pesquisa = new ImunneVacinas.PesquisaCompra()
        {
            Clinica = Convert.ToInt32(DdlClinicas.SelectedValue),
            Campanha = Convert.ToInt32(DdlCampanhas.SelectedValue),
            CampoLivre = TxtPesquisa.Text,
            Liberados = Convert.ToInt32(DdlLiberacoes.SelectedValue),
            Situacao = (ImunneVacinas.Compra.SituacaoCompra)Enum.Parse(typeof(ImunneVacinas.Compra.SituacaoCompra), DdlSituacoesCompras.SelectedValue),
            Categoria = (ImunneVacinas.Compra.CategoriaCompra)Enum.Parse(typeof(ImunneVacinas.Compra.CategoriaCompra), DdlCategorias.SelectedValue),
            Aplicacao = (ImunneVacinas.Compra.Aplicacao)Enum.Parse(typeof(ImunneVacinas.Compra.Aplicacao), DdlUtilizacoes.SelectedValue),
            DataFim = dataFinal,
            DataInicio = dataInicio,
            Valor = true,
            Ordenacao = (ImunneVacinas.Utils.TipoOrdenacao)Enum.Parse(typeof(ImunneVacinas.Utils.TipoOrdenacao), DdlOrdenacoes.SelectedValue)
        };

        return ImunneVacinas.Compra.ConsultarCompras(pesquisa);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void DdlClinicas_SelectedIndexChanged(object sender, EventArgs e)
    {
        int codigoClinica = Convert.ToInt32(DdlClinicas.SelectedValue);
        int codigoCampanha = Convert.ToInt32(DdlCampanhas.SelectedValue);

        DdlCampanhas.Items.Clear();

        if (codigoClinica > 0)
        {
            ImunneVacinas.PesquisaCampanhaClinica pesquisa = new ImunneVacinas.PesquisaCampanhaClinica()
            {
                Clinica = codigoClinica,
                Ordenacao = ImunneVacinas.Utils.TipoOrdenacao.porNome
            };

            DdlCampanhas.DataSource = ImunneVacinas.CampanhaClinica.ConsultarCampanhasComboBox(pesquisa, true, "CAMPANHAS");
            DdlCampanhas.DataBind();
            DdlCampanhas.Enabled = true;
        }
        else
        {
            DdlCampanhas.DataSource = ImunneVacinas.Campanha.ConsultarComboBox(true, "CAMPANHAS");
            DdlCampanhas.DataBind();
        }

        DdlCampanhas.SelectedValue = codigoCampanha.ToString();

        PaginaAtual = 0;
        VincularItens();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void DdlCampanhas_SelectedIndexChanged(object sender, EventArgs e)
    {
        PaginaAtual = 0;
        VincularItens();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void DdlPaginas_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BtnExportar_Click(object sender, EventArgs e)
    {
        List<ImunneVacinas.Compra> listaCompras = ConsultarRegistros();
        if (listaCompras != null && listaCompras.Count > 0)
            ImunneVacinas.ExportacoesExcel.ExportarConciliacoes(listaCompras);
        else
        {
            ImunneVacinas.Alerta.showMensagemAviso(this.Page, "Aviso!", "Sua pesquisa não possui retornos para exportar a listagem.");
            return;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BtnPesquisar_Click(object sender, EventArgs e)
    {
        PaginaAtual = 0;
        VincularItens();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void RptPrincipal_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        StringBuilder numeroCartao = new StringBuilder();
        StringBuilder nomeCartao = new StringBuilder();

        try
        {
            Label LblData = (Label)e.Item.FindControl("LblData");
            Label LblNumeroCartao = (Label)e.Item.FindControl("LblNumeroCartao");
            Label LblCPF = (Label)e.Item.FindControl("LblCPF");
            Label LblNome = (Label)e.Item.FindControl("LblNome");
            Label LblNomeCartao = (Label)e.Item.FindControl("LblNomeCartao");
            Label LblDescricao = (Label)e.Item.FindControl("LblDescricao");
            Label LblAutorizacao = (Label)e.Item.FindControl("LblAutorizacao");
            Label LblValor = (Label)e.Item.FindControl("LblValor");
            Label LblParcelas = (Label)e.Item.FindControl("LblParcelas");

            ImunneVacinas.Compra registroSelecionado = (ImunneVacinas.Compra)e.Item.DataItem;
            ImunneVacinas.TransacaoRealizada transacaoCompra = ImunneVacinas.TransacaoRealizada.ConsultarUnicoCompra(registroSelecionado.Codigo);

            if (transacaoCompra != null)
            {
                if (!String.IsNullOrEmpty(transacaoCompra.numeroCartao))
                {
                    nomeCartao.AppendFormat(ImunneVacinas.Cartao.RetornarNomeCartao(transacaoCompra.numeroCartao));

                    string cartaoTrabalho = transacaoCompra.numeroCartao.Replace(" ", "").Trim();

                    for (int i = 0; i < cartaoTrabalho.Trim().Length; i++)
                    {
                        if (i <= 3 || i >= 12)
                            numeroCartao.Append(cartaoTrabalho[i]);
                        else numeroCartao.AppendFormat("*");
                    }

                }

                LblAutorizacao.Text = transacaoCompra.autorizacao.ToString();

                if (registroSelecionado.Parcelas > 1)
                    LblDescricao.Text = String.Format("PARCELADO EM {0}X", registroSelecionado.Parcelas);
                else LblDescricao.Text = "COMPRA À VISTA";
            }

            LblParcelas.Text = registroSelecionado.Parcelas.ToString();
            LblValor.Text = ImunneVacinas.Utils.ToMoeda(registroSelecionado.Valor);
            LblData.Text = registroSelecionado.DataCompra.ToString("dd/MM/yyyy HH:mm");
            LblNumeroCartao.Text = numeroCartao.ToString();
            LblCPF.Text = ImunneVacinas.Utils.FormatarCpfCnpj(registroSelecionado.IdParticipante.Cpf);
            LblNome.Text = registroSelecionado.IdParticipante.Nome.ToUpper();
            LblNomeCartao.Text = nomeCartao.ToString().ToUpper();
        }
        catch (Exception ex)
        {
            this.SalvarException(ex);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void LnkData_Click(object sender, EventArgs e)
    {
        PrepararOrdenacoes();
        DdlOrdenacoes.SelectedValue = LnkData.CommandArgument;

        if (LnkData.CommandArgument.Contains("Desc"))
        {
            LnkData.CommandArgument = "porData";
            IcnData.Attributes.Add("class", "fa fa-caret-down");
        }
        else
        {
            LnkData.CommandArgument = "porDataDesc";
            IcnData.Attributes.Add("class", "fa fa-caret-up");
        }

        LnkData.Text = "Início";
        VincularItens();
    }

    /// <summary>
    /// 
    /// </summary>
    private void PrepararOrdenacoes()
    {
        IcnData.Attributes.Add("class", "fa fa-sort");
    }
}