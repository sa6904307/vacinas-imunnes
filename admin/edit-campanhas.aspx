﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="edit-campanhas.aspx.cs" Inherits="edit_campanhas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        function validar() {
            // Empresa
            if (document.getElementById("<%=DdlEmpresas.ClientID%>").value == "-1") {
                MensagemGenerica("Aviso!", "Por favor, informe a empresa a quem pertence a campanha.", "warning");
                return false;
            }

            // ID
            if (document.getElementById("<%=TxtIdCampanha.ClientID%>").value == "") {
                MensagemGenerica("Aviso!", "Por favor, informe a ID da campanha.", "warning");
                return false;
            }

            // Nome
            if (document.getElementById("<%=TxtNomeCampanha.ClientID%>").value == "") {
                MensagemGenerica("Aviso!", "Por favor, informe o nome da campanha.", "warning");
                return false;
            }

            // Início
            if (document.getElementById("<%=TxtInicioCampanha.ClientID%>").value == "") {
                MensagemGenerica("Aviso!", "Por favor, informe a data de início da campanha.", "warning");
                return false;
            }

            // Término
            if (document.getElementById("<%=TxtTerminoCampanha.ClientID%>").value == "") {
                MensagemGenerica("Aviso!", "Por favor, informe a data de término da campanha.", "warning");
                return false;
            }

            // Abertura
            if (document.getElementById("<%=TxtAberturaCampanha.ClientID%>").value == "") {
                MensagemGenerica("Aviso!", "Por favor, informe a data em que se iniciam as aplicações da campanha.", "warning");
                return false;
            }

            // Validade
            if (document.getElementById("<%=TxtValidadeCampanha.ClientID%>").value == "") {
                MensagemGenerica("Aviso!", "Por favor, informe a data em que se encerram as aplicações da campanha.", "warning");
                return false;
            }
        };
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js" type="text/javascript"></script>
    <link href="../vendors/datepicker/css/datepicker3.css" rel="stylesheet" type="text/css" />
    <link href="../vendors/daterangepicker/css/daterangepicker-bs3.css" rel="stylesheet"
        type="text/css" />
    <link href="../vendors/timeticker/css/bootstrap-timepicker.min.css" rel="stylesheet"
        type="text/css" />
    <script src="../vendors/daterangepicker/js/daterangepicker.js" type="text/javascript"></script>
    <script src="../vendors/datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
    <script src="../vendors/timeticker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
    <script src="../vendors/datepicker/locales/bootstrap-datepicker.pt-BR.js" type="text/javascript"></script>
    <link href="../vendors/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <script src="../vendors/global/jquery.min.js"></script>
    <script type="text/javascript" src="../vendors/select2/js/select2.full.min.js"></script>
    <script type="text/javascript">
        var select$ = jQuery.noConflict();
        select$(function () {
            //Initialize Select2 Elements
            select$('.select2').select2()
        })
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <ul class="breadcrumb">
        <li><a href="dashboard.aspx"><i class="fa fa-home"></i>&nbsp;Home</a> </li>
        <li>
            <asp:HyperLink ID="LnkPai" runat="server" Text="Campanhas"></asp:HyperLink>
        </li>
        <li class="active">Inserção/Edição</li>
    </ul>
    <h3><i class="fa fa-ambulance"></i>&nbsp;<asp:Label ID="LblForms" runat="server"></asp:Label></h3>
    <asp:UpdatePanel ID="UpdMain" runat="server">
        <ContentTemplate>
            <script type="text/javascript">
                Sys.Application.add_load(SetDatePicker);
                function SetDatePicker() {
                    $('.datepicker').datepicker({
                        todayHighlight: true,
                        language: "pt-BR"
                    });
                }
            </script>
            <script type="text/javascript">
                Sys.Application.add_load(SetSelectBox);
                var select$ = jQuery.noConflict();
                function SetSelectBox() {
                    select$('.select2').select2();
                }
            </script>
            <div class="box box-success">
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                            <asp:CheckBox ID="ChkUnicaVenda" runat="server" Text="Somente uma venda por CPF/CNPJ" />
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                            <asp:CheckBox ID="ChkTitular" runat="server" Text="Incluir titular na compra?" />
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                            <label>Empresa *</label>
                            <asp:DropDownList ID="DdlEmpresas" runat="server" CssClass="form-control select2" DataValueField="ValueMember" DataTextField="DisplayMember"></asp:DropDownList>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-group" style="height: 40px;">
                            <label>ID *</label>
                            <div class="input-group">
                                <asp:TextBox ID="TxtIdCampanha" runat="server" CssClass="form-control text-uppercase"></asp:TextBox>
                                <asp:LinkButton ID="LnkGerarCampanha" runat="server" CssClass="input-group-addon" OnClick="LnkGerarCampanha_Click" Style="border: solid 1px #cccccc;">
                                    <i class="fa fa-refresh" aria-hidden="true"></i>
                                </asp:LinkButton>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 form-group">
                            <label>Nome *</label>
                            <asp:TextBox ID="TxtNomeCampanha" runat="server" CssClass="form-control text-uppercase"></asp:TextBox>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-group">
                            <label>Contato WhatsApp</label>
                            <asp:TextBox ID="TxtWhatsApp" runat="server" CssClass="form-control text-uppercase" DataValueField="ValueMember" DataTextField="DisplayMember" onkeypress="mascara(this, mtel)" MaxLength="15"></asp:TextBox>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-group">
                            <label>Início *</label>
                            <asp:TextBox ID="TxtInicioCampanha" runat="server" CssClass="form-control text-center datepicker" data-date-format="dd/mm/yyyy" OnKeyPress="mascara(this, mdata)" MaxLength="10"></asp:TextBox>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-group">
                            <label>Término *</label>
                            <asp:TextBox ID="TxtTerminoCampanha" runat="server" CssClass="form-control text-center datepicker" data-date-format="dd/mm/yyyy" OnKeyPress="mascara(this, mdata)" MaxLength="10"></asp:TextBox>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-group">
                            <label>Início Aplicações *</label>
                            <asp:TextBox ID="TxtAberturaCampanha" runat="server" CssClass="form-control text-center datepicker" data-date-format="dd/mm/yyyy" OnKeyPress="mascara(this, mdata)" MaxLength="10"></asp:TextBox>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-group">
                            <label>Término Aplicações *</label>
                            <asp:TextBox ID="TxtValidadeCampanha" runat="server" CssClass="form-control text-center datepicker" data-date-format="dd/mm/yyyy" OnKeyPress="mascara(this, mdata)" MaxLength="10"></asp:TextBox>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 form-group">
                            <label>Parcelas *</label>
                            <asp:DropDownList ID="DdlParcelas" runat="server" CssClass="form-control text-center select2"></asp:DropDownList>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10 form-group">
                            <label>Contato </label>
                            <asp:TextBox ID="TxtContatoCampanha" runat="server" CssClass="form-control text-uppercase"></asp:TextBox>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                            <label>Termos da campanha</label>
                            <asp:TextBox ID="TxtTermosCampanha" runat="server" CssClass="form-control" TextMode="MultiLine" Height="180px"></asp:TextBox>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                            <label>
                                Imagem p/ termos
                                <asp:Label ID="LblTermosAtual" runat="server" Text="Nenhum"></asp:Label></label>
                            <asp:FileUpload ID="FupTermos" runat="server" CssClass="btn btn-success" />
                        </div>
                        <asp:UpdatePanel ID="UpdSurtos" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                    <asp:CheckBox ID="ChkSurtos" runat="server" Text="Campanhas de surtos" AutoPostBack="true" OnCheckedChanged="ChkSurtos_CheckedChanged" />
                                </div>
                                <asp:Panel ID="PnlSurtos" runat="server" Visible="false">
                                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-group">
                                        <label>Atendimento p/ hora</label>
                                        <asp:DropDownList ID="DdlTotaisAtendimentos" runat="server" CssClass="form-control text-center select2" DataValueField="ValueMember" DataTextField="DisplayMember" />
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-group">
                                        <label>Hora Inicio</label>
                                        <asp:DropDownList ID="DdlHorasInicio" runat="server" CssClass="form-control text-center select2" DataValueField="ValueMember" DataTextField="DisplayMember" />
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-group">
                                        <label>Hora Final</label>
                                        <asp:DropDownList ID="DdlHorasFim" runat="server" CssClass="form-control text-center select2" DataValueField="ValueMember" DataTextField="DisplayMember" />
                                    </div>
                                </asp:Panel>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ChkSurtos" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
            <div class="fixed-button-box">
                <div class="text-right">
                    <asp:LinkButton ID="BtnSalvar" runat="server" CssClass="btn btn-success btn-lg"
                        data-toggle="tooltip" data-container="body" ToolTip="Salvar informações"
                        ValidationGroup="edicao" OnClientClick="return validar();" OnClick="BtnSalvar_Click">
                    <i class="fa fa-save"></i>
                    </asp:LinkButton>
                    &nbsp;
                <asp:LinkButton ID="BtnCancelar" runat="server"
                    CssClass="btn btn-danger btn-lg pull-right" data-toggle="tooltip" data-container="body" ToolTip="Cancelar operação"
                    Text="Cancelar" CausesValidation="false">
                    <i class="fa fa-times"></i>
                </asp:LinkButton>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="LnkGerarCampanha" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
