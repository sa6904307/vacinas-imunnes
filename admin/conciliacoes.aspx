﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="conciliacoes.aspx.cs" Inherits="admin_conciliacoes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js" type="text/javascript"></script>
    <link href="../vendors/datepicker/css/datepicker3.css" rel="stylesheet" type="text/css" />
    <link href="../vendors/daterangepicker/css/daterangepicker-bs3.css" rel="stylesheet"
        type="text/css" />
    <link href="../vendors/timeticker/css/bootstrap-timepicker.min.css" rel="stylesheet"
        type="text/css" />
    <script src="../vendors/daterangepicker/js/daterangepicker.js" type="text/javascript"></script>
    <script src="../vendors/datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
    <script src="../vendors/timeticker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
    <script src="../vendors/datepicker/locales/bootstrap-datepicker.pt-BR.js" type="text/javascript"></script>
    <script type="text/javascript">
        var main = document.getElementById('treinamentos');
        document.addEventListener('DOMContentLoaded', function () {
            var inicio = document.getElementById("<%=HdfInicio.ClientID%>");
            var final = document.getElementById("<%=HdfFinal.ClientID%>");

            if (inicio.value == null || inicio.value == '')
                $('#daterange-btn span').html("<i class='fa fa-calendar'></i>&nbsp;Datas");
            else
                $('#daterange-btn span').html(inicio.value + ' - ' + final.value);
        });
    </script>
    <link href="../vendors/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <script src="../vendors/global/jquery.min.js"></script>
    <script type="text/javascript" src="../vendors/select2/js/select2.full.min.js"></script>
    <script type="text/javascript">
        var select$ = jQuery.noConflict();
        select$(function () {
            //Initialize Select2 Elements
            select$('.select2').select2()
        })
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- HEADER -->
    <div class="row">
        <div class="col-md-12">
            <ul class="breadcrumb">
                <li><a href="dashboard.aspx"><i class="fa fa-home"></i>&nbsp;Home</a></li>
                <li class="active">Conciliações</li>
            </ul>
            <h1 class="conteudo-centro"><i class="fa fa-usd"></i>&nbsp;Conciliações</h1>
        </div>
    </div>
    <!-- PESQUISA -->
    <div class="row">
        <asp:Panel ID="PnlPesquisa" runat="server" DefaultButton="BtnPesquisar">
            <div class="col-md-12">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-sliders"></i>&nbsp;Filtros</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" data-container="body" title="Ocultar/exibir filtros">
                                <i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="box-body" style="display: block;">
                        <div class="row">
                            <asp:UpdatePanel ID="UpdClinicas" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <script type="text/javascript">
                                        Sys.Application.add_load(SetSelectBox);
                                        var select$ = jQuery.noConflict();
                                        function SetSelectBox() {
                                            select$('.select2').select2();
                                        }
                                    </script>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                        <asp:DropDownList ID="DdlClinicas" runat="server" CssClass="form-control text-uppercase select2" DataValueField="ValueMember" DataTextField="DisplayMember" AutoPostBack="true" OnSelectedIndexChanged="DdlClinicas_SelectedIndexChanged"></asp:DropDownList>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                        <asp:DropDownList ID="DdlCampanhas" runat="server" CssClass="form-control text-uppercase select2" DataValueField="ValueMember" DataTextField="DisplayMember" AutoPostBack="true" OnSelectedIndexChanged="DdlCampanhas_SelectedIndexChanged"></asp:DropDownList>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 hidden form-group">
                                        <asp:DropDownList ID="DdlSituacoesCompras" runat="server" CssClass="form-control text-uppercase select2" DataValueField="ValueMember" DataTextField="DisplayMember"></asp:DropDownList>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 hidden form-group">
                                        <asp:DropDownList ID="DdlCategorias" runat="server" CssClass="form-control text-uppercase select2" DataValueField="ValueMember" DataTextField="DisplayMember"></asp:DropDownList>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 hidden form-group">
                                        <asp:DropDownList ID="DdlUtilizacoes" runat="server" CssClass="form-control text-uppercase select2" DataValueField="ValueMember" DataTextField="DisplayMember"></asp:DropDownList>
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="DdlClinicas" />
                                    <asp:AsyncPostBackTrigger ControlID="DdlCategorias" />
                                    <asp:AsyncPostBackTrigger ControlID="DdlLiberacoes" />
                                </Triggers>
                            </asp:UpdatePanel>
                            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-group">
                                <div id="DvDatas" runat="server">
                                    <button type="button" class="btn btn-block btn-primary pull-left" id="daterange-btn">
                                        <span>
                                            <i class="fa fa-calendar"></i>&nbsp;Datas
                                        </span>
                                        <i class="fa fa-chevron-down"></i>
                                    </button>
                                </div>
                                <script type="text/javascript">
                                    //Date range picker
                                    $('#reservation').daterangepicker()
                                    //Date range as a button
                                    $('#daterange-btn').daterangepicker(
                                        {
                                            "locale": {
                                                "format": "MM/DD/YYYY",
                                                "separator": " - ",
                                                "applyLabel": "Aplicar",
                                                "cancelLabel": "Cancelar",
                                                "fromLabel": "Início",
                                                "toLabel": "Final",
                                                "customRangeLabel": "Personalizado",
                                                "daysOfWeek": [
                                                    "Dom",
                                                    "Seg",
                                                    "Ter",
                                                    "Qua",
                                                    "Qui",
                                                    "Sex",
                                                    "Sab"
                                                ],
                                                "monthNames": [
                                                    "Janeiro",
                                                    "Fevereiro",
                                                    "Março",
                                                    "Abril",
                                                    "Maio",
                                                    "Junho",
                                                    "Julho",
                                                    "Agosto",
                                                    "Setembro",
                                                    "Outubro",
                                                    "Novembro",
                                                    "Dezembro"
                                                ],
                                                "firstDay": 1
                                            },
                                            ranges: {
                                                'Limpar': [new Date(1900, 01, 01), new Date(1900, 01, 01)],
                                                'Hoje': [moment(), moment()],
                                                'Ontem': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                                                'Última semana': [moment().subtract(6, 'days'), moment()],
                                                'Últimos 30 dias': [moment().subtract(29, 'days'), moment()],
                                                'Este mês': [moment().startOf('month'), moment().endOf('month')]
                                                //'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                                            },
                                            startDate: moment().subtract(29, 'days'),
                                            endDate: moment()
                                        },
                                        function (start, end) {
                                            if (start.format('DD/MM/YYYY') == "01/02/1900") {
                                                $('#daterange-btn span').html("<i class='fa fa-calendar'></i>&nbsp;Datas");
                                                document.getElementById("<%=HdfInicio.ClientID%>").value = "";
                                                document.getElementById("<%=HdfFinal.ClientID%>").value = "";
                                            }
                                            else {
                                                $('#daterange-btn span').html(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
                                                document.getElementById("<%=HdfInicio.ClientID%>").value = start.format('DD/MM/YYYY');
                                                document.getElementById("<%=HdfFinal.ClientID%>").value = end.format('DD/MM/YYYY');
                                            }
                                        }
                                    )
                                </script>
                            </div>
                            <asp:UpdatePanel ID="UpdLiberacoes" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 hidden form-group">
                                        <asp:DropDownList ID="DdlLiberacoes" runat="server" CssClass="form-control text-uppercase select2" DataValueField="ValueMember" DataTextField="DisplayMember"></asp:DropDownList>
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="DdlCategorias" />
                                    <asp:AsyncPostBackTrigger ControlID="DdlLiberacoes" />
                                </Triggers>
                            </asp:UpdatePanel>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 form-group">
                                <asp:HiddenField ID="HdfInicio" runat="server" />
                                <asp:HiddenField ID="HdfFinal" runat="server" />
                                <asp:TextBox ID="TxtPesquisa" runat="server" CssClass="form-control" placeHolder="PESQUISAR..."></asp:TextBox>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-group">
                                <asp:DropDownList ID="DdlItens" runat="server" CssClass="form-control text-uppercase select2" DataValueField="ValueMember" DataTextField="DisplayMember"></asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer" style="display: block;">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 pull-right">
                                <asp:LinkButton ID="BtnExportar" runat="server" CssClass="btn btn-block btn-success" data-toggle="tooltip" data-container="body" ToolTip="Exportar registros" CausesValidation="false" OnClick="BtnExportar_Click">
                                        <i class="fa fa-download"></i>&nbsp;Exportar
                                </asp:LinkButton>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 pull-right">
                                <asp:DropDownList ID="DdlOrdenacoes" runat="server" CssClass="form-control hidden"></asp:DropDownList>
                                <asp:LinkButton ID="BtnPesquisar" runat="server" CssClass="btn btn-block btn-success" data-toggle="tooltip" data-container="body" ToolTip="Pesquisar registros" CausesValidation="false" OnClick="BtnPesquisar_Click">
                                        <i class="fa fa-search"></i>&nbsp;Pesquisar
                                </asp:LinkButton>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>
    </div>
    <!-- LISTAGEM -->
    <asp:UpdatePanel ID="UpdMain" runat="server">
        <ContentTemplate>
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 box-resultados text-right conteudo-centro">
                                    <asp:Label ID="LblRegistros" runat="server"></asp:Label>
                                </div>
                            </div>
                            <div class="dv-spc-10"></div>
                            <div class="box-grids">
                                <div class="dv-grid-f-header">
                                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="width: 150px !important; display: inline-block; float: none;">
                                        <asp:LinkButton ID="LnkData" runat="server" Text="Início" OnClick="LnkData_Click" CommandArgument="porDataDesc">
                                        </asp:LinkButton>
                                        <i id="IcnData" runat="server" class="fa fa-caret-down" aria-hidden="true"></i>
                                    </div>
                                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="width: 220px !important; display: inline-block; float: none;">
                                        CPF/CNPJ
                                    </div>
                                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1" style="width: 310px !important; display: inline-block; float: none;">
                                        COMPRADOR
                                    </div>
                                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="width: 220px !important; display: inline-block; float: none;">
                                        Nº. CARTÃO
                                    </div>
                                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1" style="width: 280px !important; display: inline-block; float: none;">
                                        CARTÃO
                                    </div>
                                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="width: 240px !important; display: inline-block; float: none;">
                                        AUTORIZAÇÃO
                                    </div>
                                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1" style="width: 280px !important; display: inline-block; float: none;">
                                        DESCRIÇÃO
                                    </div>
                                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="width: 150px !important; display: inline-block; float: none;">
                                        PARCELAS
                                    </div>
                                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="width: 150px !important; display: inline-block; float: none;">
                                        VALOR
                                    </div>
                                </div>
                                <div class="dv-grid-f" style="height: auto; min-height: 330px;">
                                    <asp:Repeater ID="RptPrincipal" runat="server" OnItemDataBound="RptPrincipal_ItemDataBound">
                                        <ItemTemplate>
                                            <div id="dvItem" runat="server">
                                                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="width: 150px !important; display: inline-block; float: none;">
                                                    <div class="truncate">
                                                        <asp:Label ID="LblData" runat="server" CssClass="text-uppercase"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="width: 220px !important; display: inline-block; float: none;">
                                                    <div class="truncate">
                                                        <asp:Label ID="LblCPF" runat="server" CssClass="text-uppercase"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1" style="width: 310px !important; display: inline-block; float: none;">
                                                    <div class="truncate">
                                                        <asp:Label ID="LblNome" runat="server" CssClass="text-uppercase"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="width: 220px !important; display: inline-block; float: none;">
                                                    <div class="truncate">
                                                        <asp:Label ID="LblNumeroCartao" runat="server" CssClass="text-uppercase"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1" style="width: 280px !important; display: inline-block; float: none;">
                                                    <div class="truncate">
                                                        <asp:Label ID="LblNomeCartao" runat="server" CssClass="text-uppercase"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="width: 240px !important; display: inline-block; float: none;">
                                                    <div class="truncate">
                                                        <asp:Label ID="LblAutorizacao" runat="server" CssClass="text-uppercase text-bold"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1" style="width: 280px !important; display: inline-block; float: none;">
                                                    <div class="truncate">
                                                        <asp:Label ID="LblDescricao" runat="server" CssClass="text-uppercase"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="width: 150px !important; display: inline-block; float: none;">
                                                    <div class="truncate">
                                                        <asp:Label ID="LblParcelas" runat="server" CssClass="text-uppercase"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="width: 150px !important; display: inline-block; float: none;">
                                                    <div class="truncate">
                                                        <asp:Label ID="LblValor" runat="server" CssClass="text-uppercase"></asp:Label>
                                                    </div>
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                                    <asp:Label ID="LblInfo" runat="server"></asp:Label>
                                    <br />
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                                    <label>Página selecionada</label>&nbsp;
                                    <asp:DropDownList ID="DdlPaginas" runat="server" CssClass="form-control max-drop select2"
                                        AutoPostBack="true" OnSelectedIndexChanged="DdlPaginas_SelectedIndexChanged" data-toggle="tooltip" data-container="body" ToolTip="Escolher página para exibição">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="RptPrincipal" />
            <asp:AsyncPostBackTrigger ControlID="BtnPesquisar" />
            <asp:AsyncPostBackTrigger ControlID="DdlClinicas" />
            <asp:AsyncPostBackTrigger ControlID="DdlCampanhas" />
            <asp:AsyncPostBackTrigger ControlID="DdlPaginas" />
            <asp:PostBackTrigger ControlID="BtnExportar" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
