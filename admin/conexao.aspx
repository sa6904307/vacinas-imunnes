﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="conexao.aspx.cs" Inherits="conexao" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <style>
        pre {
            background-color: #eaeaea;
            color: #000;
            height: 400px;
            width: 100%;
        }

        .col-3 {
            float: left;
            width: 33.33%;
            padding: 10px;
            margin-bottom: 20px;
        }

        .form-control {
            margin: 0px 5px;
            width: 100%;
            border: solid 1px #666;
            height: 600px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Button ID="BtnCode" runat="server" Text="Chamar Via Código" OnClick="BtnCode_Click" /><br />
            <br />
            <asp:Button ID="BtnYapay" runat="server" Text="Chamar DLL" OnClick="BtnYapay_Click" /><br />
            <br />
            <asp:Label ID="LblResultado" runat="server"></asp:Label><br />
            <br />
            <div class="col-3">
                <pre>
                    <code id="cdJson" runat="server" class="json">
                    </code>
                </pre>
            </div>
            <div class="col-3">
                <pre>
                    <code id="cdJsonRetorno" runat="server" class="json">
                    </code>
                </pre>
            </div>
            <div class="col-3">
            </div>
        </div>
    </form>
</body>
</html>
