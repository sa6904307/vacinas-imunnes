﻿using System;
using System.IO;
using System.Web;
using System.Web.Security;

public partial class _default : System.Web.UI.Page
{
    protected string textoMensagem = String.Empty;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BtnLogin_Click(object sender, EventArgs e)
    {
        TxtLogin.Attributes["value"] = TxtLogin.Text;
        TxtPassword.Attributes["value"] = TxtPassword.Text;
        AutenticarUsuario(TxtLogin.Text, ImunneVacinas.Criptografia.Criptografar(TxtPassword.Text));
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void LnkRecover_Click(object sender, EventArgs e)
    {
        //try
        //{
        //    ImunneVacinas.PesquisaUsuario pesquisaUsuario = new ImunneVacinas.PesquisaUsuario()
        //    {
        //        Quantidade = 1,
        //        CampoLivre = TxtLogin.Text,
        //        Ordenacao = ImunneVacinas.Utils.TipoOrdenacao.porCodigo
        //    };

        //    List<ImunneVacinas.Usuario> listaUsuarios = ImunneVacinas.Usuario.ConsultarUsuarios(pesquisaUsuario);
        //    if (listaUsuarios != null && listaUsuarios.Count > 0)
        //    {
        //        /// Iniciando o envio do e-mail
        //        ImunneVacinas.Mail emailRecuperacao = new ImunneVacinas.Mail();

        //        emailRecuperacao.oEmail.To.Add(listaUsuarios[0].Email);
        //        emailRecuperacao.oEmail.From = new System.Net.Mail.MailAddress("contato@gpetraca.com.br", "S&A Imunizações | Recuperação de Senha");
        //        emailRecuperacao.oEmail.ReplyToList.Add(new System.Net.Mail.MailAddress("contato@jacto.com.br"));
        //        emailRecuperacao.oEmail.Subject = "S&A Imunizações | Recuperação de Senha";
        //        emailRecuperacao.oEmail.Body = ImunneVacinas.TemplateEmail.ConteudoRecuperacaoSenha(listaUsuarios[0]);
        //        emailRecuperacao.oEmail.IsBodyHtml = true;
        //        emailRecuperacao.oEnviar.Host = "mail.gpetraca.com.br";
        //        emailRecuperacao.oEnviar.Credentials = new System.Net.NetworkCredential("contato@gpetraca.com.br", "gM030604");
        //        emailRecuperacao.oEnviar.Port = 587;

        //        bool confirmacaoEnvio = emailRecuperacao.EnviarEmail();

        //        if (confirmacaoEnvio)
        //            ImunneVacinas.Alerta.showMensagemSucesso(this.Page, "E-mail enviado!", String.Format("O e-mail de recuperação de senha foi enviado com sucesso para {0}. Verifique sua caixa de mensagens.", listaUsuarios[0].Email));
        //        else
        //            ImunneVacinas.Alerta.showMensagemErro(this.Page, "Aviso", "Não foi possível concluir o envio. Tente novamente ou informe nosso suporte.");
        //    }
        //    else ImunneVacinas.Alerta.showMensagemErro(this.Page, "Aviso", "Não foi localizado nenhum usuário com este e-mail.");
        //}
        //catch (Exception ex)
        //{
        //    this.SalvarException(ex);
        //    ImunneVacinas.Alerta.showMensagemErro(this.Page, "Aviso", "Não foi possível concluir o envio. Tente novamente ou informe nosso suporte.");
        //}
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="login"></param>
    /// <param name="senha"></param>
    private void AutenticarUsuario(string login, string senha)
    {
        try
        {
            ImunneVacinas.Usuario usuarioLogin = ImunneVacinas.Usuario.ConsultarUnico(login, senha);

            if (usuarioLogin == null)
            {
                textoMensagem = "Lamentamos mas nenhum usuário foi encontrado com estes dados de login e senha. Verifique novamente os dados digitados.";
                ImunneVacinas.Alerta.showMensagemErro(this.Page, null, textoMensagem);
            }
            else if (usuarioLogin.Situacao == ImunneVacinas.Utils.SituacaoRegistro.Inativo)
            {
                textoMensagem = "Caro usuário, informamos que seu cadasto se encontra inativo. Por favor, entre em contato com o administrador do sistema.";
                ImunneVacinas.Alerta.showMensagemErro(this.Page, null, textoMensagem);
            }
            else
            {
                Session["codigoUsuario"] = usuarioLogin.Codigo.ToString();
                Session["nomeUsuario"] = usuarioLogin.Nome.ToString();
                Session["loginUsuario"] = usuarioLogin.Email.ToString();
                Session["tipoUsuario"] = usuarioLogin.Tipo.Codigo.ToString();
                Session["clinica"] = usuarioLogin.Clinica.ToString();

                Session.Timeout = 180;

                Application[Session.SessionID] = Session;

                Session["ip"] = Request.UserHostAddress;

                FormsAuthentication.Initialize();

                // Log descritivo
                ImunneVacinas.Log logAcao = new ImunneVacinas.Log()
                {
                    Tela = Path.GetFileName(Request.PhysicalPath),
                    TabelaAcao = "Usuarios",
                    TipoAcao = ImunneVacinas.Utils.AcaoLog.Acesso,
                    Registro = usuarioLogin.Codigo,
                    Resumo = String.Format("ACESSO AO SISTEMA - USUÁRIO {0}.", usuarioLogin.Codigo),
                    DataAcao = DateTime.Now,
                    Campo = "LOGIN",
                    ValorOriginal = String.Empty,
                    ValorNovo = String.Empty,
                };

                logAcao.IdUsuario.Codigo = this.Page.ConsultarSessionCodigoUsuario();
                //logAcao.Incluir();

                // Aqui é determinado o tempo em que a sessão ficará aberta no site até que seja realizado o logoff
                FormsAuthenticationTicket fat = new FormsAuthenticationTicket(1, usuarioLogin.Email, DateTime.Now, DateTime.Now.AddHours(4), false, usuarioLogin.Tipo.Codigo.ToString(), FormsAuthentication.FormsCookiePath);
                Response.Cookies.Add(new HttpCookie(FormsAuthentication.FormsCookieName, FormsAuthentication.Encrypt(fat)));
                Response.Redirect("~/admin/dashboard.aspx", false);
                Context.ApplicationInstance.CompleteRequest();
            }
        }
        catch (Exception ex)
        {
            this.SalvarException(ex);
            textoMensagem = "Lamentamos mas não foi possível concluir seu login. Verifique os dados digitados e se o erro persistir, entre em contato com o administrador.";
            ImunneVacinas.Alerta.showMensagemErro(this.Page, null, textoMensagem);
        }
    }
}