﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="edit-clinicas.aspx.cs" Inherits="edit_clinicas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js" type="text/javascript"></script>
    <link href="../vendors/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <script src="../vendors/global/jquery.min.js"></script>
    <script type="text/javascript" src="../vendors/select2/js/select2.full.min.js"></script>
    <script type="text/javascript">
        var select$ = jQuery.noConflict();
        select$(function () {
            //Initialize Select2 Elements
            select$('.select2').select2()
        })
    </script>
    <script type="text/javascript">
        function validar() {
            // Nome
            if (document.getElementById("<%=TxtNome.ClientID%>").value == "") {
                MensagemGenerica("Aviso!", "Por favor, informe o nome fantasia da clínica.", "warning");
                return false;
            }

            // CEP
            if (document.getElementById("<%=TxtCEP.ClientID%>").value == "") {
                MensagemGenerica("Aviso!", "Por favor, informe endereço da clínica.", "warning");
                return false;
            }

            //Email
            if (document.getElementById("<%=TxtEmailFinanceiro.ClientID %>").value != "") {
                var emailid = document.getElementById("<%=TxtEmailFinanceiro.ClientID %>").value;
                var checkTLD = 1;
                var knownDomsPat = /^(com|net|org|edu|int|mil|gov|arpa|biz|aero|name|coop|info|pro|museum)$/;
                var emailPat = /^(.+)@(.+)$/;
                var specialChars = "\\(\\)><@,;:\\\\\\\"\\.\\[\\]";
                var validChars = "\[^\\s" + specialChars + "\]";
                var quotedUser = "(\"[^\"]*\")";
                var ipDomainPat = /^\[(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})\]$/;
                var atom = validChars + '+';

                var word = "(" + atom + "|" + quotedUser + ")";
                var userPat = new RegExp("^" + word + "(\\." + word + ")*$");
                var domainPat = new RegExp("^" + atom + "(\\." + atom + ")*$");
                var matchArray = emailid.match(emailPat);
                if (matchArray == null) {
                    MensagemGenerica("Aviso!", "Por favor, informe um e-mail válido.", "warning");
                    return false;
                }
            }

            //Email
            if (document.getElementById("<%=TxtEmailGeral.ClientID %>").value != "") {
                var emailid = document.getElementById("<%=TxtEmailFinanceiro.ClientID %>").value;
                var checkTLD = 1;
                var knownDomsPat = /^(com|net|org|edu|int|mil|gov|arpa|biz|aero|name|coop|info|pro|museum)$/;
                var emailPat = /^(.+)@(.+)$/;
                var specialChars = "\\(\\)><@,;:\\\\\\\"\\.\\[\\]";
                var validChars = "\[^\\s" + specialChars + "\]";
                var quotedUser = "(\"[^\"]*\")";
                var ipDomainPat = /^\[(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})\]$/;
                var atom = validChars + '+';

                var word = "(" + atom + "|" + quotedUser + ")";
                var userPat = new RegExp("^" + word + "(\\." + word + ")*$");
                var domainPat = new RegExp("^" + atom + "(\\." + atom + ")*$");
                var matchArray = emailid.match(emailPat);
                if (matchArray == null) {
                    MensagemGenerica("Aviso!", "Por favor, informe um e-mail válido.", "warning");
                    return false;
                }
            }
        };
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <ul class="breadcrumb">
        <li><a href="dashboard.aspx"><i class="fa fa-home"></i>&nbsp;Home</a> </li>
        <li>
            <asp:HyperLink ID="LnkPai" runat="server" Text="Clínicas"></asp:HyperLink>
        </li>
        <li class="active">Inserção/Edição</li>
    </ul>
    <h3><i class="fa fa-hospital-o"></i>&nbsp;<asp:Label ID="LblForms" runat="server"></asp:Label></h3>
    <asp:UpdatePanel ID="UpdMain" runat="server">
        <ContentTemplate>
            <script type="text/javascript">
                Sys.Application.add_load(SetSelectBox);
                var select$ = jQuery.noConflict();
                function SetSelectBox() {
                    select$('.select2').select2();
                }
            </script>
            <div class="box box-success">
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-group">
                            <label>CPF/CNPJ</label>
                            <asp:TextBox ID="TxtCodigo" runat="server" CssClass="hidden"></asp:TextBox>
                            <asp:TextBox ID="TxtCpfCnpj" runat="server" CssClass="form-control text-uppercase" OnKeyPress="mascara(this,cpfCnpj)" MaxLength="18"></asp:TextBox>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9 form-group">
                            <label>Nome *</label>
                            <asp:HiddenField ID="HdfAcao" runat="server" />
                            <asp:TextBox ID="TxtNome" runat="server" CssClass="form-control text-uppercase"></asp:TextBox>
                        </div>
                        <div class="col-xs-12 col-sm-3 col-md-2 col-lg-2 form-group">
                            <label>CEP *</label>
                            <asp:TextBox ID="TxtCEP" runat="server" CssClass="form-control text-uppercase" OnTextChanged="TxtCEP_TextChanged" AutoPostBack="true" OnKeyPress="mascara(this, mcep)" MaxLength="9"></asp:TextBox>
                        </div>
                        <div class="col-xs-12 col-sm-9 col-md-10 col-lg-10 form-group">
                            <label>Endereço</label>
                            <asp:TextBox ID="TxtEndereco" runat="server" CssClass="form-control text-uppercase"></asp:TextBox>
                            <asp:HiddenField ID="HdfLatitude" runat="server" />
                            <asp:HiddenField ID="HdfLongitude" runat="server" />
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-2 col-lg-2 form-group">
                            <label>Nº.</label>
                            <asp:TextBox ID="TxtNumero" runat="server" CssClass="form-control text-uppercase"></asp:TextBox>
                        </div>
                        <div class="col-xs-12 col-sm-8 col-md-4 col-lg-4 form-group">
                            <label>Complemento</label>
                            <asp:TextBox ID="TxtComplemento" runat="server" CssClass="form-control text-uppercase"></asp:TextBox>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 form-group">
                            <label>Bairro</label>
                            <asp:TextBox ID="TxtBairro" runat="server" CssClass="form-control text-uppercase"></asp:TextBox>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 form-group">
                            <label>UF</label>
                            <asp:DropDownList ID="DdlUfs" runat="server" CssClass="form-control text-uppercase select2" DataValueField="ValueMember" AutoPostBack="true" DataTextField="DisplayMember" OnSelectedIndexChanged="DdlUfs_SelectedIndexChanged"></asp:DropDownList>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 form-group">
                            <label>Cidade</label>
                            <asp:DropDownList ID="DdlCidades" runat="server" CssClass="form-control text-uppercase select2" DataValueField="ValueMember" DataTextField="DisplayMember"></asp:DropDownList>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 form-group">
                            <label>Telefone</label>
                            <asp:TextBox ID="TxtTelefone" runat="server" CssClass="form-control text-uppercase" DataValueField="ValueMember" DataTextField="DisplayMember" onkeypress="mascara(this, mtel)" MaxLength="15"></asp:TextBox>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 form-group">
                            <label>E-mail Geral</label>
                            <asp:TextBox ID="TxtEmailGeral" runat="server" CssClass="form-control text-lowercase"></asp:TextBox>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 form-group">
                            <label>E-mail Financeiro</label>
                            <asp:TextBox ID="TxtEmailFinanceiro" runat="server" CssClass="form-control text-lowercase"></asp:TextBox>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 form-group">
                            <label>Contato Operacional</label>
                            <asp:TextBox ID="TxtContatoOperacional" runat="server" CssClass="form-control text-uppercase"></asp:TextBox>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 form-group">
                            <label>Contato Comercial</label>
                            <asp:TextBox ID="TxtContatoComercial" runat="server" CssClass="form-control text-uppercase"></asp:TextBox>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <label>Observações</label>
                            <asp:TextBox ID="TxtObservacoes" runat="server" CssClass="form-control" TextMode="MultiLine" Height="120px"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="fixed-button-box">
                    <div class="text-right">
                        <asp:LinkButton ID="BtnSalvar" runat="server" CssClass="btn btn-success btn-lg"
                            data-toggle="tooltip" data-container="body" ToolTip="Salvar informações"
                            ValidationGroup="edicao" OnClientClick="return validar();" OnClick="BtnSalvar_Click">
                    <i class="fa fa-save"></i>
                        </asp:LinkButton>
                        &nbsp;
                <asp:LinkButton ID="BtnCancelar" runat="server"
                    CssClass="btn btn-danger btn-lg pull-right" data-toggle="tooltip" data-container="body" ToolTip="Cancelar operação"
                    Text="Cancelar" CausesValidation="false">
                    <i class="fa fa-times"></i>
                </asp:LinkButton>
                    </div>
                </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
