﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class edit_campanhas : System.Web.UI.Page
{
    private StringBuilder url = new StringBuilder();
    private string textoMensagem = String.Empty;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        ScriptManager.GetCurrent(Page).RegisterPostBackControl(BtnSalvar);

        if (!IsPostBack)
            IniciarPagina();
    }

    /// <summary>
    /// 
    /// </summary>
    private void IniciarPagina()
    {
        NameValueCollection campanha = new NameValueCollection();
        int codigoRegistro = -1;
        int codigoEmpresa = -1;
        string comando = String.Empty;

        if (Request.QueryString["campanha"] != null)
        {
            campanha = ImunneVacinas.Utils.RecuperarQueryString(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["campanha"]));

            url = new StringBuilder();
            url.AppendFormat("~/admin/campanhas.aspx?campanha={0}", Request.QueryString["campanha"]);
            LnkPai.NavigateUrl = url.ToString();
            BtnCancelar.OnClientClick = "return CancelarFormulario(this, event,'" + url.Replace("~/admin/", "") + "');";

            codigoRegistro = Convert.ToInt32(campanha["codigo"]);
            codigoEmpresa = Convert.ToInt32(campanha["empresa"]);
            comando = campanha["acao"].ToString();

            //DdlHorasInicio.Items.Add(new ListItem("SELECIONE", "-1"));
            //DdlHorasFim.Items.Add(new ListItem("SELECIONE", "-1"));

            for (int i = 6; i <= 20; i++)
            {
                DdlHorasFim.Items.Add(new ListItem(i.ToString().PadLeft(2, '0'), i.ToString()));
                DdlHorasInicio.Items.Add(new ListItem(i.ToString().PadLeft(2, '0'), i.ToString()));
            }

            //DdlTotaisAtendimentos.Items.Add(new ListItem("SELECIONE", "-1"));
            DdlTotaisAtendimentos.Items.Add(new ListItem("1", "1"));
            DdlTotaisAtendimentos.Items.Add(new ListItem("2", "2"));
            DdlTotaisAtendimentos.Items.Add(new ListItem("3", "3"));
            DdlTotaisAtendimentos.Items.Add(new ListItem("4", "4"));
            DdlTotaisAtendimentos.Items.Add(new ListItem("5", "5"));
            DdlTotaisAtendimentos.Items.Add(new ListItem("6", "6"));
            //DdlTotaisAtendimentos.Items.Add(new ListItem("10", "10"));
            //DdlTotaisAtendimentos.Items.Add(new ListItem("12", "12"));
            //DdlTotaisAtendimentos.Items.Add(new ListItem("15", "15"));
            //DdlTotaisAtendimentos.Items.Add(new ListItem("20", "20"));
            //DdlTotaisAtendimentos.Items.Add(new ListItem("30", "30"));

            PopularCampos();

            LblForms.Text = ImunneVacinas.Utils.MontarTituloFormulario(comando);

            DdlEmpresas.DataSource = ImunneVacinas.Empresa.ConsultarComboBox(true, "SELECIONE");
            DdlEmpresas.DataBind();
            DdlEmpresas.SelectedValue = codigoEmpresa.ToString();

            for (int i = 1; i <= 10; i++)
            {
                DdlParcelas.Items.Add(new ListItem(i.ToString().PadLeft(2, '0'), i.ToString()));
            }

            if (codigoEmpresa > 0)
                DdlEmpresas.Enabled = false;

            switch (comando)
            {
                case "insert":
                    {
                        TxtIdCampanha.Text = "IMN" + ImunneVacinas.Utils.GerarIdRandomica(5);
                    }
                    break;
                case "edit":
                    {
                        ConsultarDetalhes(codigoRegistro);
                    }
                    break;
            }

            Page.Title = "S&A Imunizações | " + LblForms.Text;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void PopularCampos()
    {
        //TxtIdCampanha.Enabled = false;
        TxtNomeCampanha.MaxLength = (int)ImunneVacinas.Campanha.GetInfoNome().Max_length;
        TxtContatoCampanha.MaxLength = (int)ImunneVacinas.Campanha.GetInfoNomeContato().Max_length;
        TxtTermosCampanha.MaxLength = (int)ImunneVacinas.Campanha.GetInfoTermosTexto().Max_length;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="codigo"></param>
    private void ConsultarDetalhes(int codigo)
    {
        try
        {
            ImunneVacinas.Campanha registroSelecionado = ImunneVacinas.Campanha.ConsultarUnico(codigo);

            DdlEmpresas.SelectedValue = registroSelecionado.IdEmpresa.Codigo.ToString();
            TxtIdCampanha.Text = registroSelecionado.Identificacao;
            TxtNomeCampanha.Text = registroSelecionado.Nome;
            TxtContatoCampanha.Text = registroSelecionado.NomeContato;
            TxtInicioCampanha.Text = registroSelecionado.DataInicio.ToString("dd/MM/yyyy");
            TxtTerminoCampanha.Text = registroSelecionado.DataTermino.ToString("dd/MM/yyyy");
            TxtValidadeCampanha.Text = registroSelecionado.DataLimite.ToString("dd/MM/yyyy");
            TxtAberturaCampanha.Text = registroSelecionado.DataAbertura.ToString("dd/MM/yyyy");
            TxtTermosCampanha.Text = registroSelecionado.TermosTexto;
            DdlParcelas.SelectedValue = registroSelecionado.Parcelas.ToString();

            ChkUnicaVenda.Checked = Convert.ToBoolean(registroSelecionado.UnicaVenda);
            ChkTitular.Checked = Convert.ToBoolean(registroSelecionado.IncluirTitular);
            TxtWhatsApp.Text = registroSelecionado.ContatoWhatsApp;

            if (!String.IsNullOrEmpty(registroSelecionado.TermosImagem))
                LblTermosAtual.Text = registroSelecionado.TermosImagem;

            ChkSurtos.Checked = Convert.ToBoolean(registroSelecionado.Surto);
            PnlSurtos.Visible = Convert.ToBoolean(registroSelecionado.Surto);

            if (ChkSurtos.Checked == true)
            {
                DdlTotaisAtendimentos.SelectedValue = registroSelecionado.TotalAtendimentos.ToString();
                DdlHorasFim.SelectedValue = registroSelecionado.HoraTermino.ToString();
                DdlHorasInicio.SelectedValue = registroSelecionado.HoraInicio.ToString();
            }
        }
        catch (Exception ex)
        {
            this.SalvarException404(ex);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BtnSalvar_Click(object sender, EventArgs e)
    {
        NameValueCollection campanha = new NameValueCollection();

        try
        {
            campanha = ImunneVacinas.Utils.RecuperarQueryString(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["campanha"]));

            int codigoRegistro = ImunneVacinas.Parser.parseInteiro(campanha["codigo"]);
            string comando = campanha["acao"].ToString();

            ImunneVacinas.Campanha novoRegistro;
            ImunneVacinas.Campanha registroOriginal = new ImunneVacinas.Campanha();
            if (codigoRegistro > 0)
            {
                novoRegistro = ImunneVacinas.Campanha.ConsultarUnico(codigoRegistro);
                registroOriginal = ImunneVacinas.Campanha.ConsultarUnico(codigoRegistro);
            }
            else
                novoRegistro = new ImunneVacinas.Campanha();

            novoRegistro.IdEmpresa.Codigo = Convert.ToInt32(DdlEmpresas.SelectedValue);
            novoRegistro.Nome = TxtNomeCampanha.Text;
            novoRegistro.Identificacao = TxtIdCampanha.Text;
            novoRegistro.NomeContato = TxtContatoCampanha.Text;
            novoRegistro.DataInicio = Convert.ToDateTime(TxtInicioCampanha.Text);
            novoRegistro.DataTermino = Convert.ToDateTime(TxtTerminoCampanha.Text);
            novoRegistro.DataLimite = Convert.ToDateTime(TxtValidadeCampanha.Text);
            novoRegistro.DataAbertura = Convert.ToDateTime(TxtAberturaCampanha.Text);
            novoRegistro.TermosTexto = TxtTermosCampanha.Text;
            novoRegistro.Parcelas = Convert.ToInt32(DdlParcelas.SelectedValue);
            novoRegistro.UnicaVenda = Convert.ToInt32(ChkUnicaVenda.Checked);
            novoRegistro.IncluirTitular = Convert.ToInt32(ChkTitular.Checked);
            novoRegistro.ContatoWhatsApp = TxtWhatsApp.Text;
            novoRegistro.Surto = Convert.ToInt32(ChkSurtos.Checked);
            novoRegistro.TotalAtendimentos = Convert.ToInt32(DdlTotaisAtendimentos.SelectedValue);

            if (Convert.ToInt32(DdlHorasFim.SelectedValue) < Convert.ToInt32(DdlHorasInicio.SelectedValue))
            {
                novoRegistro.HoraInicio = Convert.ToInt32(DdlHorasFim.SelectedValue);
                novoRegistro.HoraTermino = Convert.ToInt32(DdlHorasInicio.SelectedValue);
            }
            else
            {
                novoRegistro.HoraTermino = Convert.ToInt32(DdlHorasFim.SelectedValue);
                novoRegistro.HoraInicio = Convert.ToInt32(DdlHorasInicio.SelectedValue);
            }

            if (FupTermos.HasFile)
            {
                novoRegistro.TermosImagem = DateTime.Now.ToString("yyyyMMddHHmmssfff") + System.IO.Path.GetFileNameWithoutExtension(FupTermos.FileName);
                int tam = (int)ImunneVacinas.Empresa.GetInfoLogotipo().Max_length - 4;
                novoRegistro.TermosImagem = ImunneVacinas.Utils.TruncarString(novoRegistro.TermosImagem, tam) + System.IO.Path.GetExtension(FupTermos.FileName);
            }
            else
            {
                if (LblTermosAtual.Text != "Nenhum")
                    novoRegistro.TermosImagem = LblTermosAtual.Text;
                else
                    novoRegistro.TermosImagem = "";
            }

            if (ChkSurtos.Checked == true)
            {
                if (codigoRegistro > 0)
                {
                    if (VerificarSurtosOcupados(codigoRegistro) == true)
                    {
                        ImunneVacinas.Alerta.showMensagemAviso(this.Page, "Aviso", "Esta campanha já possui horários de surtos selecionados e não pode sofrer alterações.");
                        return;
                    }
                }
            }

            int resultado = 0;
            if (VerificarNome(comando, codigoRegistro, TxtIdCampanha.Text) == false)
            {
                textoMensagem = String.Format("Verifique seu cadastro pois já existe uma campanha com a ID {0}.", TxtIdCampanha.Text);
                ImunneVacinas.Alerta.showMensagemErro(this.Page, null, textoMensagem);
                return;
            }

            switch (comando)
            {
                case "insert":
                    {
                        resultado = AtualizarArquivo(novoRegistro);

                        if (resultado > 0)
                        {
                            novoRegistro.DataCriacao = DateTime.Now;
                            resultado = novoRegistro.Incluir();
                            codigoRegistro = resultado;
                        }
                    }
                    break;
                case "edit":
                    {
                        resultado = AtualizarArquivo(novoRegistro);

                        if (resultado > 0)
                        {
                            novoRegistro.Codigo = Convert.ToInt32(codigoRegistro);
                            novoRegistro.DataAlteracao = DateTime.Now;
                            resultado = novoRegistro.Alterar();
                        }
                    }
                    break;
            }

            #region RETORNO DE ERROS

            if (resultado <= 0)
            {
                textoMensagem = "Lamentamos mas ocorreu um erro e não foi possível concluir a operação. Tente novamente ou informe o suporte.";
                ImunneVacinas.Alerta.showMensagemErro(this.Page, null, textoMensagem);
                return;
            }

            #endregion

            #region GERAÇÃO DE LOGS

            // Log descritivo
            ImunneVacinas.Log logAcao = new ImunneVacinas.Log();
            logAcao.Tela = Path.GetFileName(Request.PhysicalPath);
            logAcao.TabelaAcao = "Campanhas";

            switch (comando)
            {
                case "insert":
                    {
                        logAcao.TipoAcao = ImunneVacinas.Utils.AcaoLog.Insercao;
                        logAcao.Registro = resultado;
                        logAcao.Resumo = String.Format("INSERÇÃO DE NOVO REGISTRO {0}.", logAcao.Registro);
                        logAcao.DataAcao = DateTime.Now;
                        logAcao.Campo = String.Empty;
                        logAcao.ValorOriginal = String.Empty;
                        logAcao.ValorNovo = String.Empty;
                        logAcao.Incluir();
                    }
                    break;
                case "edit":
                    {
                        logAcao.TipoAcao = ImunneVacinas.Utils.AcaoLog.Edicao;
                        //ImunneVacinas.RegistraLog.CompararClinica(logAcao, registroOriginal, novoRegistro);
                    }
                    break;
            }

            #endregion

            #region GERAÇÃO DE HORÁRIOS (SURTOS)

            if (ChkSurtos.Checked == true)
                CriarAgendamento(codigoRegistro, novoRegistro.DataAbertura, novoRegistro.DataLimite, novoRegistro.HoraInicio, novoRegistro.HoraTermino, novoRegistro.TotalAtendimentos);

            #endregion

            Response.Redirect(LnkPai.NavigateUrl, false);
            Context.ApplicationInstance.CompleteRequest();
        }
        catch (Exception ex)
        {
            textoMensagem = "Lamentamos mas ocorreu um erro e não foi possível concluir a operação. Tente novamente ou informe o suporte.";
            ImunneVacinas.Alerta.showMensagemErro(this.Page, null, textoMensagem);
            this.SalvarException(ex);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="novaEmpresa"></param>
    /// <returns></returns>
    private int AtualizarArquivo(ImunneVacinas.Campanha novoRegistro)
    {
        try
        {
            if (FupTermos.HasFile)
            {
                string pathTemp = Server.MapPath("~/img/campanhas");

                if (!String.IsNullOrEmpty(LblTermosAtual.Text))
                    File.Delete(pathTemp + "//" + LblTermosAtual.Text);

                string nomeArquivo = pathTemp + "//" + novoRegistro.TermosImagem;
                FupTermos.SaveAs(nomeArquivo);
            }
            return 1;
        }
        catch (Exception ex)
        {
            ImunneVacinas.Erro erro = new ImunneVacinas.Erro();
            erro.addArquivo(ex);
            return 0;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void LnkGerarCampanha_Click(object sender, EventArgs e)
    {
        TxtIdCampanha.Text = "IMN" + ImunneVacinas.Utils.GerarIdRandomica(5);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ChkSurtos_CheckedChanged(object sender, EventArgs e)
    {
        PnlSurtos.Visible = ChkSurtos.Checked;
    }

    /// <summary>
    /// Rotina que verifica se já existe um tipo de registro com os dados a serem cadastrados
    /// </summary>
    /// <param name="comando">VALUE: Comando que está sendo realizado pelo formulário</param>
    /// <param name="codigoRegistro">VALUE: Código do registro que está sendo inserido</param>
    /// <param name="nomeRegistro">VALUE: Nome do registro a ser inserido</param>
    /// <returns></returns>
    private bool VerificarNome(string comando, int codigoRegistro, string nomeRegistro)
    {
        bool retorno = false;

        ImunneVacinas.Campanha registroVerificado = ImunneVacinas.Campanha.ConsultarUnico(nomeRegistro);

        switch (comando)
        {
            case "insert":
                {
                    if (registroVerificado == null)
                        retorno = true;
                    else
                        retorno = false;
                }
                break;
            case "edit":
                {
                    if (registroVerificado == null)
                        retorno = true;
                    else
                    {
                        if (registroVerificado.Codigo == codigoRegistro)
                            retorno = true;
                        else
                            retorno = false;
                    }
                }
                break;
        }

        return retorno;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="codigoCampanha"></param>
    /// <returns></returns>
    private bool VerificarSurtosOcupados(int codigoCampanha)
    {
        bool retorno = false;

        #region HORÁRIOS DE SURTO

        ImunneVacinas.PesquisaHorarioSurto pesquisa = new ImunneVacinas.PesquisaHorarioSurto()
        {
            Campanha = codigoCampanha,
        };
        pesquisa.ListaSituacoes.Add(1);
        pesquisa.ListaSituacoes.Add(2);

        List<ImunneVacinas.HorarioSurto> listaOcupados = ImunneVacinas.HorarioSurto.ConsultarHorarios(pesquisa);
        if (listaOcupados != null && listaOcupados.Count > 0)
            retorno = true;

        #endregion

        return retorno;
    }

    /// <summary>
    /// Rotina que gera horários de agendamento em campanhas de surto
    /// </summary>
    /// <param name="codigoCampanha">VALUE: código de identificação da campanha</param>
    /// <param name="dataInicio">VALUE: Data de início de validade da campanha</param>
    /// <param name="dataFim">VALUE: Data de término da validade da campanha</param>
    /// <param name="horaInicio">VALUE: Horário inicial de atendimento nos dias de campanha</param>
    /// <param name="horaFim">VALUE: Horário final de atendimentos nos dias de campanha</param>
    /// <param name="qtdeAtendimentos"></param>
    private void CriarAgendamento(int codigoCampanha, DateTime dataInicio, DateTime dataFim, int horaInicio, int horaFim, int qtdeAtendimentos)
    {
        #region CRIAÇÃO DE NOVOS HORÁRIOS

        ImunneVacinas.HorarioSurto.ExcluirCampanha(codigoCampanha);

        int minutosAtendimento = (60 / qtdeAtendimentos);

        for (DateTime dataLoop = dataInicio; dataLoop <= dataFim; dataLoop.AddDays(1))
        {
            DateTime dataControle = dataLoop.AddDays(1);

            for (int hora = horaInicio; hora < horaFim; hora++)
            {
                for (int minutos = 0; minutos < 60; minutos += minutosAtendimento)
                {
                    DateTime dataSurto = new DateTime(dataLoop.Year, dataLoop.Month, dataLoop.Day, hora, minutos, 0);

                    ImunneVacinas.HorarioSurto novoRegistro = new ImunneVacinas.HorarioSurto()
                    {
                        Campanha = codigoCampanha,
                        Compra = -1,
                        Situacao = ImunneVacinas.Utils.SituacaoHorario.Livre,
                        InfoHoraro = dataSurto,

                    };

                    novoRegistro.Incluir();
                }
            }

            dataLoop = dataControle;
        }

        #endregion
    }
}