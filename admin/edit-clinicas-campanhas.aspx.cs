﻿using System;
using System.Collections.Specialized;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class edit_clinicas_campanhas : System.Web.UI.Page
{
    private StringBuilder url = new StringBuilder();
    private string textoMensagem = String.Empty;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            IniciarPagina();
    }

    /// <summary>
    /// 
    /// </summary>
    private void IniciarPagina()
    {
        NameValueCollection campanha = new NameValueCollection();
        NameValueCollection clinica = new NameValueCollection();

        int codigoRegistro = -1;
        int codigoCampanha = -1;
        string comando = String.Empty;

        if (Request.QueryString["clinica"] != null)
        {
            clinica = ImunneVacinas.Utils.RecuperarQueryString(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["clinica"]));
            campanha = ImunneVacinas.Utils.RecuperarQueryString(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["campanha"]));

            codigoCampanha = Convert.ToInt32(campanha["codigo"]);

            url = new StringBuilder();
            url.AppendFormat("~/admin/clinicas-campanhas.aspx?campanha={0}&clinica={1}", Request.QueryString["campanha"], Request.QueryString["clinica"]);
            LnkClinicas.NavigateUrl = url.ToString();
            BtnCancelar.OnClientClick = "return CancelarFormulario(this, event,'" + url.Replace("~/admin/", "") + "');";

            url = new StringBuilder();
            url.AppendFormat("~/admin/campanhas.aspx?campanha={0}", Request.QueryString["campanha"]);
            LnkCampanhas.NavigateUrl = url.ToString();

            DdlCampanhas.DataSource = ImunneVacinas.Campanha.ConsultarComboBox(true, "SELECIONE");
            DdlCampanhas.DataBind();
            DdlCampanhas.Enabled = false;
            DdlCampanhas.SelectedValue = codigoCampanha.ToString();

            DdlUfs.DataSource = ImunneVacinas.Uf.ConsultarComboBox(true, "UF");
            DdlUfs.DataBind();

            DdlCidades.Items.Add(new ListItem("CIDADES", "-1"));
            DdlCidades.Enabled = false;

            codigoRegistro = Convert.ToInt32(clinica["codigo"]);
            comando = clinica["acao"].ToString();

            LblForms.Text = ImunneVacinas.Utils.MontarTituloFormulario(comando);

            Page.Title = "S&A Imunizações | " + LblForms.Text;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BtnPesquisar_Click(object sender, EventArgs e)
    {
        ConsultarClinicas();
    }

    /// <summary>
    /// 
    /// </summary>
    private void ConsultarClinicas()
    {
        ImunneVacinas.PesquisaClinica pesquia = new ImunneVacinas.PesquisaClinica()
        {
            Uf = DdlUfs.SelectedValue,
            Cidade = Convert.ToInt32(DdlCidades.SelectedValue),
            CampoLivre = TxtPesquisa.Text,
            Ordenacao = ImunneVacinas.Utils.TipoOrdenacao.porNome
        };

        RptPrincipal.DataSource = ImunneVacinas.Clinica.ConsultarClinicas(pesquia);
        RptPrincipal.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void RptPrincipal_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        StringBuilder enderecoClinica = new StringBuilder();
        StringBuilder cidadeClinica= new StringBuilder();

        ImunneVacinas.Clinica clinicaSelecionada = (ImunneVacinas.Clinica)e.Item.DataItem;

        CheckBox ChkSelecionar = (CheckBox)e.Item.FindControl("ChkSelecionar");
        Label LblTotalCampanhas = (Label)e.Item.FindControl("LblTotalCampanhas");
        Label LblNome = (Label)e.Item.FindControl("LblNome");
        Label LblEndereco = (Label)e.Item.FindControl("LblEndereco");
        Label LblTelefone = (Label)e.Item.FindControl("LblTelefone");
        Label LblCidade = (Label)e.Item.FindControl("LblCidade");
        HiddenField HdfCodigo = (HiddenField)e.Item.FindControl("HdfCodigo");

        #region ENDEREÇAMENTO

        if (!String.IsNullOrEmpty(clinicaSelecionada.IdCidade.Nome))
            cidadeClinica.AppendFormat("{0}", clinicaSelecionada.IdCidade.Nome.ToUpper());

        if (!String.IsNullOrEmpty(clinicaSelecionada.IdUf.Sigla))
        {
            if (cidadeClinica.Length > 0)
                cidadeClinica.Append("/");
            cidadeClinica.AppendFormat("{0}", clinicaSelecionada.IdUf.Sigla.ToUpper());
        }

        if (!String.IsNullOrEmpty(clinicaSelecionada.Endereco))
            enderecoClinica.AppendFormat("{0}", clinicaSelecionada.Endereco);

        if (!String.IsNullOrEmpty(clinicaSelecionada.Numero))
        {
            if (enderecoClinica.Length > 0)
                enderecoClinica.Append(", ");
            enderecoClinica.AppendFormat("{0}", clinicaSelecionada.Numero.ToUpper());
        }

        #endregion

        LblNome.Text = clinicaSelecionada.NomeFantasia;
        LblTelefone.Text = clinicaSelecionada.Telefone;
        LblEndereco.Text = enderecoClinica.ToString();
        LblCidade.Text = cidadeClinica.ToString();
        HdfCodigo.Value = clinicaSelecionada.Codigo.ToString();

        ImunneVacinas.CampanhaClinica campanhaClinica = ImunneVacinas.CampanhaClinica.ConsultarUnico(Convert.ToInt32(DdlCampanhas.SelectedValue), clinicaSelecionada.Codigo);
        if (campanhaClinica != null)
        {
            ChkSelecionar.Checked = true;
            ChkSelecionar.Enabled = false;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BtnSalvar_Click(object sender, EventArgs e)
    {
        try
        {
            int codigoCampanha = Convert.ToInt32(DdlCampanhas.SelectedValue);

            foreach (RepeaterItem item in RptPrincipal.Items)
            {
                CheckBox ChkSelecionar = (CheckBox)item.FindControl("ChkSelecionar");
                HiddenField HdfCodigo = (HiddenField)item.FindControl("HdfCodigo");

                if (ChkSelecionar.Checked)
                {
                    if (ChkSelecionar.Enabled == true)
                    {
                        ImunneVacinas.CampanhaClinica novoRegistro = new ImunneVacinas.CampanhaClinica();
                        novoRegistro.IdCampanha.Codigo = codigoCampanha;
                        novoRegistro.IdClinica.Codigo = Convert.ToInt32(HdfCodigo.Value);
                        novoRegistro.DataCriacao = DateTime.Now;
                        novoRegistro.Incluir();
                    }
                }
            }

            Response.Redirect(LnkClinicas.NavigateUrl, false);
            Context.ApplicationInstance.CompleteRequest();
        }
        catch (Exception ex)
        {
            textoMensagem = "Lamentamos mas ocorreu um erro e não foi possível concluir a operação. Tente novamente ou informe o suporte.";
            ImunneVacinas.Alerta.showMensagemErro(this.Page, null, textoMensagem);
            this.SalvarException(ex);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void DdlUfs_SelectedIndexChanged(object sender, EventArgs e)
    {
        string sigalUf = DdlUfs.SelectedValue;

        DdlCidades.Items.Clear();
        DdlCidades.Items.Add(new ListItem("CIDADES", "-1"));
        DdlCidades.Enabled = false;

        if (!String.IsNullOrEmpty(sigalUf) && sigalUf != "-1")
        {
            DdlCidades.DataSource = ImunneVacinas.Cidade.ConsultarComboBox(sigalUf, true, "CIDADES");
            DdlCidades.DataBind();
            DdlCidades.Enabled = true;
        }
    }
}