﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class usuarios : System.Web.UI.Page
{
    private StringBuilder url = new StringBuilder();
    private StringBuilder parametros = new StringBuilder();

    #region Private Proprieties

    private int PaginaAtual
    {
        get
        {
            object objPage = ViewState["_PaginaAtual"];
            int _PaginaAtual = 0;
            if (objPage == null)
            {
                _PaginaAtual = 0;
            }
            else
            {
                _PaginaAtual = (int)objPage;
            }
            return _PaginaAtual;
        }
        set
        {
            ViewState["_PaginaAtual"] = value;
        }
    }
    private int PrimeiroIndice
    {
        get
        {

            int _PrimeiroIndice = 0;
            if (ViewState["_PrimeiroIndice"] == null)
            {
                _PrimeiroIndice = 0;
            }
            else
            {
                _PrimeiroIndice = Convert.ToInt32(ViewState["_PrimeiroIndice"]);
            }
            return _PrimeiroIndice;
        }
        set
        {
            ViewState["_PrimeiroIndice"] = value;
        }
    }
    private int UltimoIndice
    {
        get
        {

            int _UltimoIndice = 0;
            if (ViewState["_UltimoIndice"] == null)
            {
                _UltimoIndice = 0;
            }
            else
            {
                _UltimoIndice = Convert.ToInt32(ViewState["_UltimoIndice"]);
            }
            return _UltimoIndice;
        }
        set
        {
            ViewState["_UltimoIndice"] = value;
        }
    }

    #endregion

    #region PagedDataSource

    PagedDataSource _PagedDataSource = new PagedDataSource();

    #endregion

    #region Private Methods

    /// <summary>
    /// 
    /// </summary>
    private void VincularItens()
    {
        try
        {
            List<ImunneVacinas.Usuario> retorno = ConsultarRegistros();

            _PagedDataSource.DataSource = retorno;
            _PagedDataSource.AllowPaging = true;
            _PagedDataSource.PageSize = 10;
            _PagedDataSource.CurrentPageIndex = PaginaAtual;
            ViewState["TotalPages"] = _PagedDataSource.PageCount;

            DdlPaginas.Items.Clear();
            for (int x = 0; x < _PagedDataSource.PageCount; x++)
            {
                DdlPaginas.Items.Add(new ListItem((x + 1).ToString(), x.ToString()));
            }

            DdlPaginas.SelectedValue = PaginaAtual.ToString();

            RptPrincipal.DataSource = _PagedDataSource;
            RptPrincipal.DataBind();
            CriarPaginacao();

            StringBuilder totalRegistros = new StringBuilder();
            if (retorno.Count <= 0)
                totalRegistros.Append("Nenhum registro encontrado.");
            else if (retorno.Count == 1)
                totalRegistros.Append("Foi encontrado <b>1</b> registro.");
            else
                totalRegistros.AppendFormat("Foram encontrados <b>{0}</b> registros.", retorno.Count);

            LblRegistros.Text = totalRegistros.ToString();
            LblInfo.Text = "Exibindo página " + (PaginaAtual + 1) + " de " + _PagedDataSource.PageCount + ".";
        }
        catch (Exception Ex)
        {
            this.SalvarException(Ex);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void CriarPaginacao()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("PageIndex");
        dt.Columns.Add("PageText");

        PrimeiroIndice = PaginaAtual - 5;


        if (PaginaAtual > 5)
        {
            UltimoIndice = PaginaAtual + 5;
        }
        else
        {
            UltimoIndice = 10;
        }
        if (UltimoIndice > Convert.ToInt32(ViewState["TotalPages"]))
        {
            UltimoIndice = Convert.ToInt32(ViewState["TotalPages"]);
            PrimeiroIndice = UltimoIndice - 10;
        }

        if (PrimeiroIndice < 0)
        {
            PrimeiroIndice = 0;
        }

        for (int i = PrimeiroIndice; i < UltimoIndice; i++)
        {
            DataRow dr = dt.NewRow();
            dr[0] = i;
            dr[1] = i + 1;
            dt.Rows.Add(dr);
        }

        DdlPaginas.SelectedValue = PaginaAtual.ToString();
    }

    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            IniciarPagina();
    }

    /// <summary>
    /// 
    /// </summary>
    private void IniciarPagina()
    {
        Page.Title = "S&A Imunizações | Usuários";

        try
        {
            NameValueCollection usuario = new NameValueCollection();

            int codigoTipo = -1;
            string situacao = "1";
            string pesquisa = String.Empty;
            int current_page = -1;

            if (!String.IsNullOrEmpty(Request.QueryString["usuario"]))
            {
                usuario = ImunneVacinas.Utils.RecuperarQueryString(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["usuario"]));
                situacao = usuario["situacao"];
                codigoTipo = Convert.ToInt32(usuario["tipo"]);
                pesquisa = usuario["pesquisa"];
                current_page = Convert.ToInt32(usuario["current_page"]);
            }

            DdlSituacoes.DataSource = ImunneVacinas.Utils.ConsultaComboBoxSituacaoComboBox();
            DdlSituacoes.DataBind();
            DdlSituacoes.SelectedValue = situacao;

            ImunneVacinas.PesquisaTipoUsuario pesquisaTipos = new ImunneVacinas.PesquisaTipoUsuario
            {
                Ordenacao = ImunneVacinas.Utils.TipoOrdenacao.porNome
            };

            DdlTipos.DataSource = ImunneVacinas.TipoUsuario.ConsultarComboBox(pesquisaTipos, true, "TIPOS");
            DdlTipos.DataBind();
            DdlTipos.SelectedValue = codigoTipo.ToString();

            if (!String.IsNullOrEmpty(pesquisa) && pesquisa != "PESQUISAR")
                TxtPesquisa.Text = pesquisa;

            if (current_page > 0)
                PaginaAtual = current_page;

            VincularItens();
        }
        catch (Exception ex)
        {
            this.SalvarException(ex);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    private List<ImunneVacinas.Usuario> ConsultarRegistros()
    {
        ImunneVacinas.PesquisaUsuario pesquisa = new ImunneVacinas.PesquisaUsuario
        {
            Tipo = Convert.ToInt32(DdlTipos.SelectedValue),
            Situacao = (ImunneVacinas.Utils.SituacaoRegistro)Enum.Parse(typeof(ImunneVacinas.Utils.SituacaoRegistro), DdlSituacoes.SelectedValue),
            CampoLivre = TxtPesquisa.Text,
            Ordenacao = ImunneVacinas.Utils.TipoOrdenacao.porNome
        };

        return ImunneVacinas.Usuario.ConsultarUsuarios(pesquisa);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void RptPrincipal_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        int codigoRegistro = Convert.ToInt32(e.CommandArgument);
        string comando = e.CommandName;

        ImunneVacinas.Usuario registroSelecionado = ImunneVacinas.Usuario.ConsultarUnico(codigoRegistro);

        switch (comando)
        {
            case "edit":
                {
                    url = new StringBuilder();
                    parametros = new StringBuilder();

                    parametros.AppendFormat("acao:{0}|", "edit");
                    parametros.AppendFormat("codigo:{0}|", codigoRegistro);
                    parametros.AppendFormat("tipo:{0}|", DdlTipos.SelectedValue);
                    parametros.AppendFormat("situacao:{0}|", DdlSituacoes.SelectedValue);
                    parametros.AppendFormat("pesquisa:{0}|", TxtPesquisa.Text);
                    parametros.AppendFormat("current_page:{0}", PaginaAtual);

                    url.AppendFormat("~/admin/edit-usuarios.aspx?usuario={0}", ImunneVacinas.Criptografia.Criptografar(parametros.ToString()));

                    Response.Redirect(url.ToString(), false);
                    Context.ApplicationInstance.CompleteRequest();
                }
                break;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void RptPrincipal_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        ImunneVacinas.Usuario item = (ImunneVacinas.Usuario)e.Item.DataItem;
        int roleUsuario = Page.ConsultarSessionTipoUsuario();

        LinkButton LnkEditList = (LinkButton)e.Item.FindControl("LnkEditList");
        LinkButton LnkDeleteList = (LinkButton)e.Item.FindControl("LnkDeleteList");
        LinkButton LnkStatus = (LinkButton)e.Item.FindControl("lnKStatus");
        Label lblLogin = (Label)e.Item.FindControl("lblLogin");
        Label lblNome = (Label)e.Item.FindControl("lblNome");
        Label LblStatus = (Label)e.Item.FindControl("LblStatus");
        Label lblTipo = (Label)e.Item.FindControl("lblTipo");

        LnkDeleteList.CommandArgument = item.Codigo.ToString();
        LnkEditList.CommandArgument = item.Codigo.ToString();
        LnkStatus.CommandArgument = item.Codigo.ToString();
        LnkDeleteList.OnClientClick = "return ConfirmacaoExclucao(this, event,'" + item.Codigo + "');";
        LnkStatus.OnClientClick = "return ConfirmacaoSituacao(this, event,'" + item.Codigo + "');";
        lblLogin.Text = item.Login;
        lblNome.Text = item.Nome;
        lblTipo.Text = item.Tipo.Descricao;

        if (item.Situacao == ImunneVacinas.Utils.SituacaoRegistro.Inativo)
        {
            LblStatus.Text = "INATIVO";
            LnkStatus.CssClass = "btn btn-sm btn-danger";
        }

        if (roleUsuario != 1)
        {
            if (item.Tipo.Codigo <= 2)
                LnkDeleteList.Visible = false;
        }
    }

    #region Paginação

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void DdlPaginas_SelectedIndexChanged(object sender, EventArgs e)
    {
        PaginaAtual = Convert.ToInt32(DdlPaginas.SelectedValue);
        VincularItens();
    }

    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BtnPesquisar_Click(object sender, EventArgs e)
    {
        PaginaAtual = 0;
        VincularItens();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BtnNovo_Click(object sender, EventArgs e)
    {
        url = new StringBuilder();
        parametros = new StringBuilder();

        parametros.AppendFormat("acao:{0}|", "insert");
        parametros.AppendFormat("codigo:{0}|", "-1");
        parametros.AppendFormat("tipo:{0}|", DdlTipos.SelectedValue);
        parametros.AppendFormat("situacao:{0}|", DdlSituacoes.SelectedValue);
        parametros.AppendFormat("pesquisa:{0}|", TxtPesquisa.Text);
        parametros.AppendFormat("current_page:{0}", PaginaAtual);

        url.AppendFormat("~/admin/edit-usuarios.aspx?usuario={0}", ImunneVacinas.Criptografia.Criptografar(parametros.ToString()));

        Response.Redirect(url.ToString(), false);
        Context.ApplicationInstance.CompleteRequest();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BtnExcluirItens_Click(object sender, EventArgs e)
    {
        ExcluirRegistro();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BtnAlterarSituacao_Click(object sender, EventArgs e)
    {
        AlterarSituacaoRegistro();
    }

    /// <summary>
    /// 
    /// </summary>
    private void ExcluirRegistro()
    {
        int roleUsuario = this.ConsultarSessionTipoUsuario();
        int codigoRegistro = Convert.ToInt32(HdfCodigo.Value);
        ImunneVacinas.Usuario registroSelecionado = ImunneVacinas.Usuario.ConsultarUnico(codigoRegistro);

        if (registroSelecionado == null)
            ImunneVacinas.Alerta.showMensagemErro(this.Page, "Oops!", "Nenhum registro foi localizado com o código informado!");
        else if (this.Page.ConsultarSessionCodigoUsuario() == registroSelecionado.Codigo)
            ImunneVacinas.Alerta.showMensagemErro(this.Page, "Oops!", "Você não pode realizar a exclusão do seu próprio registro!");
        else if (roleUsuario != 5 && registroSelecionado.Tipo.Codigo == 5)
            ImunneVacinas.Alerta.showMensagemErro(this.Page, "Oops!", "Não é permitido a exclusão de usuário de sistema, você não possui as permissões suficientes!");
        else
        {
            try
            {
                int resultado = registroSelecionado.Excluir();
                if (resultado > 0)
                {
                    // Log descritivo
                    ImunneVacinas.Log logAcao = new ImunneVacinas.Log()
                    {
                        Tela = Path.GetFileName(Request.PhysicalPath),
                        TabelaAcao = "Usuarios",
                        TipoAcao = ImunneVacinas.Utils.AcaoLog.Exclusao,
                        Registro = registroSelecionado.Codigo,
                        Resumo = String.Format("EXCLUSÃO DE REGISTRO {0}.", registroSelecionado.Codigo),
                        DataAcao = DateTime.Now,
                        Campo = String.Empty,
                        ValorOriginal = String.Empty,
                        ValorNovo = String.Empty,
                    };

                    logAcao.IdUsuario.Codigo = this.Page.ConsultarSessionCodigoUsuario();
                    logAcao.Incluir();

                    ImunneVacinas.Alerta.showMensagemSucesso(this.Page, "Sucesso!", "Registro excluído com sucesso!");
                    HdfCodigo.Value = "";

                    VincularItens();
                }
                else ImunneVacinas.Alerta.showMensagemErro(this.Page, "Oops!", "Infelizmente não foi possível concluir a solicitação, tente novamente ou informe o suporte.");
            }
            catch (Exception ex)
            {
                this.SalvarException(ex);
                ImunneVacinas.Alerta.showMensagemErro(this.Page, "Oops!", "Infelizmente não foi possível concluir a solicitação, tente novamente ou informe o suporte.");
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void AlterarSituacaoRegistro()
    {
        int roleUsuario = Convert.ToInt32(this.Page.ConsultarSessionTipoUsuario());
        int codigoRegistro = Convert.ToInt32(HdfCodigo.Value);
        ImunneVacinas.Usuario registroSelecionado = ImunneVacinas.Usuario.ConsultarUnico(codigoRegistro);

        if (registroSelecionado == null)
            ImunneVacinas.Alerta.showMensagemErro(this.Page, "Oops!", "Nenhum registro foi localizado com o código informado!");
        else if (roleUsuario != 5 && registroSelecionado.Tipo.Codigo == 5)
            ImunneVacinas.Alerta.showMensagemErro(this.Page, "Oops!", "Não é permitido a alteração de dados de usuários do sistema, você não possui as permissões suficientes!");
        else
        {
            try
            {
                int situacaoNova = (1 - (int)registroSelecionado.Situacao);

                registroSelecionado.Situacao = (ImunneVacinas.Utils.SituacaoRegistro)Enum.Parse(typeof(ImunneVacinas.Utils.SituacaoRegistro), situacaoNova.ToString());
                registroSelecionado.DataAlteracao = DateTime.Now;

                int resultado = registroSelecionado.Alterar();
                if (resultado > 0)
                {
                    // Log descritivo
                    ImunneVacinas.Log logAcao = new ImunneVacinas.Log()
                    {
                        Tela = Path.GetFileName(Request.PhysicalPath),
                        TabelaAcao = "Usuarios",
                        TipoAcao = ImunneVacinas.Utils.AcaoLog.Edicao,
                        Registro = registroSelecionado.Codigo,
                        Resumo = String.Format("EDIÇÃO DE SITUAÇÃO DE USUÁRIO {0}.", registroSelecionado.Codigo),
                        DataAcao = DateTime.Now,
                        Campo = ImunneVacinas.Usuario.GetInfoTipo().Column_name,
                        ValorOriginal = ((ImunneVacinas.Utils.SituacaoRegistro)Enum.Parse(typeof(ImunneVacinas.Utils.SituacaoRegistro), (1 - situacaoNova).ToString())).ToString(),
                        ValorNovo = ((ImunneVacinas.Utils.SituacaoRegistro)Enum.Parse(typeof(ImunneVacinas.Utils.SituacaoRegistro), situacaoNova.ToString())).ToString(),
                    };

                    logAcao.IdUsuario.Codigo = this.Page.ConsultarSessionCodigoUsuario();
                    logAcao.Incluir();

                    ImunneVacinas.Alerta.showMensagemSucesso(this.Page, "Sucesso!", "Situação do registro alterada com sucesso!");
                    HdfCodigo.Value = "";

                    VincularItens();
                }
                else ImunneVacinas.Alerta.showMensagemErro(this.Page, "Oops!", "Infelizmente não foi possível concluir a solicitação, tente novamente ou informe o suporte.");
            }
            catch (Exception ex)
            {
                this.SalvarException(ex);
                ImunneVacinas.Alerta.showMensagemErro(this.Page, "Oops!", "Infelizmente não foi possível concluir a solicitação, tente novamente ou informe o suporte.");
            }
        }
    }
}