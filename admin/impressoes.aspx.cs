﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_impressoes : System.Web.UI.Page
{
    private StringBuilder url = new StringBuilder();
    private StringBuilder parametros = new StringBuilder();

    #region Private Proprieties

    private int PaginaAtual
    {
        get
        {
            object objPage = ViewState["_PaginaAtual"];
            int _PaginaAtual = 0;
            if (objPage == null)
            {
                _PaginaAtual = 0;
            }
            else
            {
                _PaginaAtual = (int)objPage;
            }
            return _PaginaAtual;
        }
        set
        {
            ViewState["_PaginaAtual"] = value;
        }
    }
    private int PrimeiroIndice
    {
        get
        {

            int _PrimeiroIndice = 0;
            if (ViewState["_PrimeiroIndice"] == null)
            {
                _PrimeiroIndice = 0;
            }
            else
            {
                _PrimeiroIndice = Convert.ToInt32(ViewState["_PrimeiroIndice"]);
            }
            return _PrimeiroIndice;
        }
        set
        {
            ViewState["_PrimeiroIndice"] = value;
        }
    }
    private int UltimoIndice
    {
        get
        {

            int _UltimoIndice = 0;
            if (ViewState["_UltimoIndice"] == null)
            {
                _UltimoIndice = 0;
            }
            else
            {
                _UltimoIndice = Convert.ToInt32(ViewState["_UltimoIndice"]);
            }
            return _UltimoIndice;
        }
        set
        {
            ViewState["_UltimoIndice"] = value;
        }
    }

    #endregion

    #region PagedDataSource

    PagedDataSource _PagedDataSource = new PagedDataSource();

    #endregion

    #region Private Methods

    /// <summary>
    /// 
    /// </summary>
    private void VincularItens()
    {
        try
        {
            List<ImunneVacinas.Voucher> retorno = ConsultarRegistros();

            _PagedDataSource.DataSource = retorno;
            _PagedDataSource.AllowPaging = true;
            _PagedDataSource.PageSize = 10;
            _PagedDataSource.CurrentPageIndex = PaginaAtual;
            ViewState["TotalPages"] = _PagedDataSource.PageCount;

            DdlPaginas.Items.Clear();
            for (int x = 0; x < _PagedDataSource.PageCount; x++)
            {
                DdlPaginas.Items.Add(new ListItem((x + 1).ToString(), x.ToString()));
            }

            DdlPaginas.SelectedValue = PaginaAtual.ToString();

            RptPrincipal.DataSource = _PagedDataSource;
            RptPrincipal.DataBind();
            CriarPaginacao();

            LblRegistros.Text = ImunneVacinas.Utils.MontarLinhaTotalizadora(retorno.Count);
            LblInfo.Text = "Exibindo página " + (PaginaAtual + 1) + " de " + _PagedDataSource.PageCount + ".";
        }
        catch (Exception Ex)
        {
            this.SalvarException(Ex);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void CriarPaginacao()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("PageIndex");
        dt.Columns.Add("PageText");

        PrimeiroIndice = PaginaAtual - 5;


        if (PaginaAtual > 5)
        {
            UltimoIndice = PaginaAtual + 5;
        }
        else
        {
            UltimoIndice = 10;
        }
        if (UltimoIndice > Convert.ToInt32(ViewState["TotalPages"]))
        {
            UltimoIndice = Convert.ToInt32(ViewState["TotalPages"]);
            PrimeiroIndice = UltimoIndice - 10;
        }

        if (PrimeiroIndice < 0)
        {
            PrimeiroIndice = 0;
        }

        for (int i = PrimeiroIndice; i < UltimoIndice; i++)
        {
            DataRow dr = dt.NewRow();
            dr[0] = i;
            dr[1] = i + 1;
            dt.Rows.Add(dr);
        }

        DdlPaginas.SelectedValue = PaginaAtual.ToString();
    }

    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = "S&A Imunizações | Impressões de Vales";
        if (!IsPostBack)
            IniciarPagina();
    }

    /// <summary>
    /// 
    /// </summary>
    private void IniciarPagina()
    {
        try
        {
            NameValueCollection compra = new NameValueCollection();

            int codigoCompra = -1;

            if (!String.IsNullOrEmpty(Request.QueryString["compra"]))
            {
                compra = ImunneVacinas.Utils.RecuperarQueryString(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["compra"]));
                codigoCompra = Convert.ToInt32(compra["codigo"]);

                ImunneVacinas.Compra compraSelecionada = ImunneVacinas.Compra.ConsultarUnico(codigoCompra);
                ImunneVacinas.Campanha campanhaSelecionada = ImunneVacinas.Campanha.ConsultarUnico(compraSelecionada.IdCampanha.Codigo);
                ImunneVacinas.Clinica clinicaSelecionada = ImunneVacinas.Clinica.ConsultarUnico(compraSelecionada.IdClinica.Codigo);
                ImunneVacinas.Empresa empresaSelecionada = ImunneVacinas.Empresa.ConsultarUnico(campanhaSelecionada.IdEmpresa.Codigo);

                TxtPedido.Text = compraSelecionada.Codigo.ToString().PadLeft(5, '0');
                TxtData.Text = compraSelecionada.DataCompra.ToString("dd/MM/yyyy");
                TxtNome.Text = compraSelecionada.IdParticipante.Nome.ToUpper();
                TxtCampanha.Text = campanhaSelecionada.Identificacao;
                TxtEmpresa.Text = empresaSelecionada.NomeFantasia.ToUpper();
                TxtClinica.Text = clinicaSelecionada.NomeFantasia.ToUpper() + " | "
                                + clinicaSelecionada.Endereco.ToUpper() + ", "
                                + clinicaSelecionada.Numero + " - "
                                + clinicaSelecionada.IdCidade.Nome + "/"
                                + clinicaSelecionada.IdUf.Sigla;

                LnkPai.NavigateUrl = String.Format("~/admin/compras.aspx?compra={0}", Request.QueryString["compra"]);

                VincularItens();
            }
            else
            {
                Response.Redirect("~/admin/compras.aspx", false);
                Context.ApplicationInstance.CompleteRequest();
            }
        }
        catch (Exception ex)
        {

        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    private List<ImunneVacinas.Voucher> ConsultarRegistros()
    {
        ImunneVacinas.PesquisaVoucher pesquisa = new ImunneVacinas.PesquisaVoucher()
        {
            Compra = Convert.ToInt32(TxtPedido.Text),
            Ordenacao = ImunneVacinas.Utils.TipoOrdenacao.porCodigo
        };

        return ImunneVacinas.Voucher.ConsultarVouchers(pesquisa);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void RptPrincipal_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        try
        {
            int codigoRegistro = Convert.ToInt32(e.CommandArgument);
            ImunneVacinas.Voucher item = ImunneVacinas.Voucher.ConsultarUnico(codigoRegistro);
            ImunneVacinas.Compra compraSelecionada = ImunneVacinas.Compra.ConsultarUnico(Convert.ToInt32(TxtPedido.Text));
            ImunneVacinas.Participante participanteSelecionado = ImunneVacinas.Participante.ConsultarUnico(compraSelecionada.IdParticipante.Codigo);

            string comando = e.CommandName;

            switch (comando)
            {
                case "send":
                    {
                        // Enviar via e-mail ao adquirente
                        StringBuilder conteudoEmail = new StringBuilder();
                        ImunneVacinas.Participante beneficiario = ImunneVacinas.Participante.ConsultarUnico(item.IdParticipante.Codigo);

                        conteudoEmail.AppendFormat("<a href='{0}' target='_blank'>IMPRIMIR", String.Format("https://vacinas.imunne.com.br/prints/voucher.aspx?codigo={0}", ImunneVacinas.Criptografia.Criptografar(item.Codigo.ToString())));
                        conteudoEmail.AppendFormat("</a> | {0} | ", item.IdParticipante.Nome.ToUpper());
                        conteudoEmail.AppendFormat("{0}<br />", beneficiario.Parentesco.Nome.ToUpper());

                        if (!String.IsNullOrEmpty(participanteSelecionado.Email) && ImunneVacinas.Utils.ValidarEmail(participanteSelecionado.Email))
                        {
                            bool envioEmail = ImunneVacinas.Email.EnviarEmail(participanteSelecionado.Email, "S&A Imunizações | Relação de Vales", ImunneVacinas.Email.ValeIndividual(compraSelecionada, conteudoEmail.ToString()));
                            if (envioEmail)
                                ImunneVacinas.Alerta.showMensagemSucesso(this.Page, "Sucesso", "E-mail com os valores enviado com sucesso!");
                            else
                            {
                                ImunneVacinas.Alerta.showMensagemAviso(this.Page, "Aviso", "Infelizmente não foi possível enviar a mensagem!");
                                return;
                            }
                        }
                        else
                        {
                            ImunneVacinas.Alerta.showMensagemAviso(this.Page, "Aviso", "O adquirente não possui um e-mail válido cadastrado!");
                            return;
                        }
                    }
                    break;
            }
        }
        catch (Exception ex)
        {
            ImunneVacinas.Alerta.showMensagemAviso(this.Page, "Aviso", "Infelizmente não foi possível enviar a mensagem!");
            ImunneVacinas.Erro erro = new ImunneVacinas.Erro();
            erro.addArquivo(ex);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void RptPrincipal_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        try
        {
            ImunneVacinas.Voucher registroSelecionado = (ImunneVacinas.Voucher)e.Item.DataItem;
            ImunneVacinas.Compra compraSelecionada = ImunneVacinas.Compra.ConsultarUnico(registroSelecionado.IdCompra.Codigo);
            ImunneVacinas.Campanha campanhaSelecionada = ImunneVacinas.Campanha.ConsultarUnico(registroSelecionado.IdCampanha.Codigo);
            ImunneVacinas.Participante participanteSelecionado = ImunneVacinas.Participante.ConsultarUnico(compraSelecionada.IdParticipante.Codigo);
            ImunneVacinas.Participante beneficiarioSelecionado = ImunneVacinas.Participante.ConsultarUnico(registroSelecionado.IdParticipante.Codigo);

            Label LblUtilizado = (Label)e.Item.FindControl("LblUtilizado");
            Label LblBeneficiario = (Label)e.Item.FindControl("LblBeneficiario");
            Label LblCampanha = (Label)e.Item.FindControl("LblCampanha");
            Label LblNome = (Label)e.Item.FindControl("LblNome");
            Label LblParentesco = (Label)e.Item.FindControl("LblParentesco");
            HiddenField HdfCodigo = (HiddenField)e.Item.FindControl("HdfCodigo");
            LinkButton LnkEnviarList = (LinkButton)e.Item.FindControl("LnkEnviarList");
            HyperLink LnkImprimirList = (HyperLink)e.Item.FindControl("LnkImprimirList");

            LblCampanha.Text = campanhaSelecionada.Identificacao;
            LblNome.Text = compraSelecionada.IdParticipante.Nome.ToUpper();
            LblBeneficiario.Text = registroSelecionado.IdParticipante.Nome.ToUpper();
            LblParentesco.Text = beneficiarioSelecionado.Parentesco.Nome.ToUpper();

            HdfCodigo.Value = registroSelecionado.Codigo.ToString();
            LnkEnviarList.CommandArgument = registroSelecionado.Codigo.ToString();
            LnkImprimirList.NavigateUrl = String.Format("~/prints/voucher.aspx?codigo={0}", ImunneVacinas.Criptografia.Criptografar(registroSelecionado.Codigo.ToString()));

            if (registroSelecionado.DataRetirada == ImunneVacinas.Utils.dataPadrao)
                LblUtilizado.Text = "NÃO UTILIZADO";
            else
                LblUtilizado.Text = registroSelecionado.DataRetirada.ToString("dd/MM/yyyy");
        }
        catch (Exception ex)
        {
            ImunneVacinas.Erro erro = new ImunneVacinas.Erro();
            erro.addArquivo(ex);
        }
    }

    #region Paginação

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void DdlPaginas_SelectedIndexChanged(object sender, EventArgs e)
    {
        PaginaAtual = Convert.ToInt32(DdlPaginas.SelectedValue);
        VincularItens();
    }

    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BtnEnviarTodos_Click(object sender, EventArgs e)
    {
        try
        {
            ImunneVacinas.Compra compraSelecionada = ImunneVacinas.Compra.ConsultarUnico(Convert.ToInt32(TxtPedido.Text));
            ImunneVacinas.Participante participanteSelecionado = ImunneVacinas.Participante.ConsultarUnico(compraSelecionada.IdParticipante.Codigo);

            ImunneVacinas.PesquisaVoucher pesquisa = new ImunneVacinas.PesquisaVoucher()
            {
                Compra = compraSelecionada.Codigo,
                Ordenacao = ImunneVacinas.Utils.TipoOrdenacao.porCodigo
            };

            StringBuilder conteudoEmail = new StringBuilder();
            List<ImunneVacinas.Voucher> listaVales = ImunneVacinas.Voucher.ConsultarVouchers(pesquisa);

            foreach (ImunneVacinas.Voucher item in listaVales)
            {
                ImunneVacinas.Participante beneficiario = ImunneVacinas.Participante.ConsultarUnico(item.IdParticipante.Codigo);

                conteudoEmail.AppendFormat("<a href='{0}' target='_blank'>IMPRIMIR", String.Format("https://vacinas.imunne.com.br/prints/voucher.aspx?codigo={0}", ImunneVacinas.Criptografia.Criptografar(item.Codigo.ToString())));
                conteudoEmail.AppendFormat("</a> | {0} | ", item.IdParticipante.Nome.ToUpper());
                conteudoEmail.AppendFormat("{0}<br />", beneficiario.Parentesco.Nome.ToUpper());
            }

            if (!String.IsNullOrEmpty(participanteSelecionado.Email) && ImunneVacinas.Utils.ValidarEmail(participanteSelecionado.Email))
            {
                bool envioEmail = ImunneVacinas.Email.EnviarEmail(participanteSelecionado.Email, "S&A Imunizações | Relação de Vales", ImunneVacinas.Email.ValesGerados(compraSelecionada, conteudoEmail.ToString()));
                if (envioEmail)
                    ImunneVacinas.Alerta.showMensagemSucesso(this.Page, "Sucesso", "E-mail com os valores enviado com sucesso!");
                else
                {
                    ImunneVacinas.Alerta.showMensagemAviso(this.Page, "Aviso", "Infelizmente não foi possível enviar a mensagem!");
                    return;
                }
            }
            else
            {
                ImunneVacinas.Alerta.showMensagemAviso(this.Page, "Aviso", "O adquirente não possui um e-mail válido cadastrado!");
                return;
            }
        }
        catch (Exception ex)
        {
            ImunneVacinas.Alerta.showMensagemAviso(this.Page, "Aviso", "Infelizmente não foi possível enviar a mensagem!");
            ImunneVacinas.Erro erro = new ImunneVacinas.Erro();
            erro.addArquivo(ex);
        }
    }
}