﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="edit-links.aspx.cs" Inherits="admin_edit_links" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        function validar() {
            // Data
            if (document.getElementById("<%=TxtData.ClientID%>").value == "") {
                MensagemGenerica("Aviso!", "Por favor, informe a data em que se encerram as vendas da campanha.", "warning");
                return false;
            }
        };
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js" type="text/javascript"></script>
    <link href="../vendors/datepicker/css/datepicker3.css" rel="stylesheet" type="text/css" />
    <link href="../vendors/daterangepicker/css/daterangepicker-bs3.css" rel="stylesheet"
        type="text/css" />
    <link href="../vendors/timeticker/css/bootstrap-timepicker.min.css" rel="stylesheet"
        type="text/css" />
    <script src="../vendors/daterangepicker/js/daterangepicker.js" type="text/javascript"></script>
    <script src="../vendors/datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
    <script src="../vendors/timeticker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
    <script src="../vendors/datepicker/locales/bootstrap-datepicker.pt-BR.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <ul class="breadcrumb">
        <li><a href="dashboard.aspx"><i class="fa fa-home"></i>&nbsp;Home</a></li>
        <li>
            <asp:HyperLink ID="LnkClinicas" runat="server" Text="Clínicas"></asp:HyperLink>
        </li>
        <li>
            <asp:HyperLink ID="LnkCampanhas" runat="server" Text="Campanhas da Clínica"></asp:HyperLink>
        </li>
        <li>
            <asp:HyperLink ID="LnkPais" runat="server" Text="Links"></asp:HyperLink>
        </li>
        <li class="active">Inserção/Edição</li>
    </ul>
    <h3><i class="fa fa-ambulance"></i>&nbsp;<asp:Label ID="LblForms" runat="server"></asp:Label></h3>
    <asp:UpdatePanel ID="UpdMain" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="box box-success">
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                            <label>Clínica</label>
                            <asp:DropDownList ID="DdlClinicas" runat="server" CssClass="form-control text-uppercase" Enabled="false" DataValueField="ValueMember" DataTextField="DisplayMember"></asp:DropDownList>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 form-group">
                            <label>Campanha</label>
                            <asp:DropDownList ID="DdlCampanhas" runat="server" CssClass="form-control text-uppercase" Enabled="false" DataValueField="ValueMember" DataTextField="DisplayMember"></asp:DropDownList>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 form-group">
                            <label>Data</label>
                            <asp:TextBox ID="TxtData" runat="server" CssClass="form-control text-uppercase text-center datepicker" data-date-format="dd/mm/yyyy" OnKeyPress="mascara(this, mdata)" MaxLength="10"></asp:TextBox>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 form-group">
                            <label>Hora</label>
                            <asp:DropDownList ID="DdlHoras" runat="server" CssClass="form-control text-center select2"></asp:DropDownList>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 form-group">
                            <label>Minutos</label>
                            <asp:DropDownList ID="DdlMinutos" runat="server" CssClass="form-control text-center select2"></asp:DropDownList>
                        </div>
                    </div>
                </div>
            </div>
            <div class="fixed-button-box">
                <div class="text-right">
                    <asp:LinkButton ID="BtnSalvar" runat="server" CssClass="btn btn-success btn-lg"
                        data-toggle="tooltip" data-container="body" ToolTip="Salvar informações"
                        ValidationGroup="edicao" OnClientClick="return validar();" OnClick="BtnSalvar_Click">
                    <i class="fa fa-save"></i>
                    </asp:LinkButton>
                    &nbsp;
                <asp:LinkButton ID="BtnCancelar" runat="server"
                    CssClass="btn btn-danger btn-lg pull-right" data-toggle="tooltip" data-container="body" ToolTip="Cancelar operação"
                    Text="Cancelar" CausesValidation="false">
                    <i class="fa fa-times"></i>
                </asp:LinkButton>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
