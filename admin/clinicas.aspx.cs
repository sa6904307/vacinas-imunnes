﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.IO;
using System.Text;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class clinicas : System.Web.UI.Page
{
    private StringBuilder url = new StringBuilder();
    private StringBuilder parametros = new StringBuilder();

    #region Private Proprieties

    private int PaginaAtual
    {
        get
        {
            object objPage = ViewState["_PaginaAtual"];
            int _PaginaAtual = 0;
            if (objPage == null)
            {
                _PaginaAtual = 0;
            }
            else
            {
                _PaginaAtual = (int)objPage;
            }
            return _PaginaAtual;
        }
        set
        {
            ViewState["_PaginaAtual"] = value;
        }
    }
    private int PrimeiroIndice
    {
        get
        {

            int _PrimeiroIndice = 0;
            if (ViewState["_PrimeiroIndice"] == null)
            {
                _PrimeiroIndice = 0;
            }
            else
            {
                _PrimeiroIndice = Convert.ToInt32(ViewState["_PrimeiroIndice"]);
            }
            return _PrimeiroIndice;
        }
        set
        {
            ViewState["_PrimeiroIndice"] = value;
        }
    }
    private int UltimoIndice
    {
        get
        {

            int _UltimoIndice = 0;
            if (ViewState["_UltimoIndice"] == null)
            {
                _UltimoIndice = 0;
            }
            else
            {
                _UltimoIndice = Convert.ToInt32(ViewState["_UltimoIndice"]);
            }
            return _UltimoIndice;
        }
        set
        {
            ViewState["_UltimoIndice"] = value;
        }
    }

    #endregion

    #region PagedDataSource

    PagedDataSource _PagedDataSource = new PagedDataSource();

    #endregion

    #region Private Methods

    /// <summary>
    /// 
    /// </summary>
    private void VincularItens()
    {
        try
        {
            List<ImunneVacinas.Clinica> retorno = ConsultarRegistros();

            _PagedDataSource.DataSource = retorno;
            _PagedDataSource.AllowPaging = true;
            _PagedDataSource.PageSize = 10;
            _PagedDataSource.CurrentPageIndex = PaginaAtual;
            ViewState["TotalPages"] = _PagedDataSource.PageCount;

            DdlPaginas.Items.Clear();
            for (int x = 0; x < _PagedDataSource.PageCount; x++)
            {
                DdlPaginas.Items.Add(new ListItem((x + 1).ToString(), x.ToString()));
            }

            DdlPaginas.SelectedValue = PaginaAtual.ToString();

            RptPrincipal.DataSource = _PagedDataSource;
            RptPrincipal.DataBind();
            CriarPaginacao();

            LblRegistros.Text = ImunneVacinas.Utils.MontarLinhaTotalizadora(retorno.Count);
            LblInfo.Text = "Exibindo página " + (PaginaAtual + 1) + " de " + _PagedDataSource.PageCount + ".";

            if (retorno != null && retorno.Count > 0)
                BtnExportar.Visible = true;
            else BtnExportar.Visible = false;
        }
        catch (Exception Ex)
        {
            this.SalvarException(Ex);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void CriarPaginacao()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("PageIndex");
        dt.Columns.Add("PageText");

        PrimeiroIndice = PaginaAtual - 5;


        if (PaginaAtual > 5)
        {
            UltimoIndice = PaginaAtual + 5;
        }
        else
        {
            UltimoIndice = 10;
        }
        if (UltimoIndice > Convert.ToInt32(ViewState["TotalPages"]))
        {
            UltimoIndice = Convert.ToInt32(ViewState["TotalPages"]);
            PrimeiroIndice = UltimoIndice - 10;
        }

        if (PrimeiroIndice < 0)
        {
            PrimeiroIndice = 0;
        }

        for (int i = PrimeiroIndice; i < UltimoIndice; i++)
        {
            DataRow dr = dt.NewRow();
            dr[0] = i;
            dr[1] = i + 1;
            dt.Rows.Add(dr);
        }

        DdlPaginas.SelectedValue = PaginaAtual.ToString();
    }

    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = "S&A Imunizações | Clínicas";
        if (!IsPostBack)
            IniciarPagina();
    }

    /// <summary>
    /// 
    /// </summary>
    private void IniciarPagina()
    {
        try
        {
            NameValueCollection clinica = new NameValueCollection();

            int codigoCidade = -1;
            string situacao = "1";
            string siglaUf = String.Empty;
            string pesquisa = String.Empty;
            int current_page = -1;

            if (!String.IsNullOrEmpty(Request.QueryString["clinica"]))
            {
                clinica = ImunneVacinas.Utils.RecuperarQueryString(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["clinica"]));
                situacao = clinica["situacao"];
                pesquisa = clinica["pesquisa"];
                siglaUf = clinica["uf"];
                codigoCidade = Convert.ToInt32(clinica["cidade"]);
                current_page = Convert.ToInt32(clinica["current_page"]);
            }

            DdlUfs.DataSource = ImunneVacinas.Uf.ConsultarComboBox(true, "UF");
            DdlUfs.DataBind();
            DdlUfs.SelectedValue = siglaUf;

            DdlCidades.Items.Add(new ListItem("CIDADES", "-1"));
            DdlCidades.Enabled = false;

            if (!String.IsNullOrEmpty(siglaUf) && siglaUf != "-1")
            {
                DdlCidades.DataSource = ImunneVacinas.Cidade.ConsultarComboBox(siglaUf, true, "CIDADES");
                DdlCidades.DataBind();
                DdlCidades.SelectedValue = codigoCidade.ToString();
                DdlCidades.Enabled = true;
            }

            if (!String.IsNullOrEmpty(pesquisa) && pesquisa != "PESQUISAR")
                TxtPesquisa.Text = pesquisa;

            if (current_page > 0)
                PaginaAtual = current_page;

            VincularItens();
        }
        catch (Exception ex)
        {
            this.SalvarException(ex);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    private List<ImunneVacinas.Clinica> ConsultarRegistros()
    {
        ImunneVacinas.PesquisaClinica pesquisa = new ImunneVacinas.PesquisaClinica
        {
            Uf = DdlUfs.SelectedValue,
            Cidade = Convert.ToInt32(DdlCidades.SelectedValue),
            CampoLivre = TxtPesquisa.Text,
            Ordenacao = ImunneVacinas.Utils.TipoOrdenacao.porNome
        };

        return ImunneVacinas.Clinica.ConsultarClinicas(pesquisa);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void RptPrincipal_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        int codigoRegistro = Convert.ToInt32(e.CommandArgument);
        string comando = e.CommandName;

        ImunneVacinas.Clinica registroSelecionado = ImunneVacinas.Clinica.ConsultarUnico(codigoRegistro);

        switch (comando)
        {
            case "edit":
                {
                    url = new StringBuilder();
                    parametros = new StringBuilder();

                    parametros.AppendFormat("acao:{0}|", "edit");
                    parametros.AppendFormat("codigo:{0}|", codigoRegistro);
                    parametros.AppendFormat("cidade:{0}|", DdlCidades.SelectedValue);
                    parametros.AppendFormat("uf:{0}|", DdlUfs.SelectedValue);
                    parametros.AppendFormat("pesquisa:{0}|", TxtPesquisa.Text);
                    parametros.AppendFormat("current_page:{0}", PaginaAtual);

                    url.AppendFormat("~/admin/edit-clinicas.aspx?clinica={0}", ImunneVacinas.Criptografia.Criptografar(parametros.ToString()));

                    Response.Redirect(url.ToString(), false);
                    Context.ApplicationInstance.CompleteRequest();
                }
                break;
            case "movimentacoes":
                {
                    url = new StringBuilder();
                    parametros = new StringBuilder();

                    parametros.AppendFormat("acao:{0}|", "edit");
                    parametros.AppendFormat("codigo:{0}|", codigoRegistro);
                    parametros.AppendFormat("cidade:{0}|", DdlCidades.SelectedValue);
                    parametros.AppendFormat("uf:{0}|", DdlUfs.SelectedValue);
                    parametros.AppendFormat("pesquisa:{0}|", TxtPesquisa.Text);
                    parametros.AppendFormat("current_page:{0}", PaginaAtual);

                    url.AppendFormat("~/admin/estoque.aspx?clinica={0}", ImunneVacinas.Criptografia.Criptografar(parametros.ToString()));

                    Response.Redirect(url.ToString(), false);
                    Context.ApplicationInstance.CompleteRequest();
                }
                break;
            case "send":
                {
                    url = new StringBuilder();
                    parametros = new StringBuilder();

                    parametros.AppendFormat("acao:{0}|", "edit");
                    parametros.AppendFormat("codigo:{0}|", codigoRegistro);
                    parametros.AppendFormat("cidade:{0}|", DdlCidades.SelectedValue);
                    parametros.AppendFormat("uf:{0}|", DdlUfs.SelectedValue);
                    parametros.AppendFormat("pesquisa:{0}|", TxtPesquisa.Text);
                    parametros.AppendFormat("current_page:{0}", PaginaAtual);

                    url.AppendFormat("~/admin/boas-vindas.aspx?clinica={0}", ImunneVacinas.Criptografia.Criptografar(parametros.ToString()));

                    Response.Redirect(url.ToString(), false);
                    Context.ApplicationInstance.CompleteRequest();
                }
                break;
            case "campanhas":
                {
                    url = new StringBuilder();
                    parametros = new StringBuilder();

                    parametros.AppendFormat("acao:{0}|", "edit");
                    parametros.AppendFormat("codigo:{0}|", codigoRegistro);
                    parametros.AppendFormat("cidade:{0}|", DdlCidades.SelectedValue);
                    parametros.AppendFormat("uf:{0}|", DdlUfs.SelectedValue);
                    parametros.AppendFormat("pesquisa:{0}|", TxtPesquisa.Text);
                    parametros.AppendFormat("current_page:{0}", PaginaAtual);

                    url.AppendFormat("~/admin/campanhas-clinicas.aspx?clinica={0}", ImunneVacinas.Criptografia.Criptografar(parametros.ToString()));

                    Response.Redirect(url.ToString(), false);
                    Context.ApplicationInstance.CompleteRequest();
                } break;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void RptPrincipal_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        try
        {
            ImunneVacinas.Clinica registroSelecionado = (ImunneVacinas.Clinica)e.Item.DataItem;

            LinkButton LnkEditList = (LinkButton)e.Item.FindControl("LnkEditList");
            LinkButton LnkDeleteList = (LinkButton)e.Item.FindControl("LnkDeleteList");
            LinkButton LnkStatus = (LinkButton)e.Item.FindControl("lnKStatus");
            LinkButton LnkBoasVindasList = (LinkButton)e.Item.FindControl("LnkBoasVindasList");
            LinkButton LnkCampanhasList = (LinkButton)e.Item.FindControl("LnkCampanhasList");
            LinkButton LnkEventosList = (LinkButton)e.Item.FindControl("LnkEventosList");

            Label LblTotalCampanhas = (Label)e.Item.FindControl("LblTotalCampanhas");
            Label LblTotalCompras = (Label)e.Item.FindControl("LblTotalCompras");
            Label LblNome = (Label)e.Item.FindControl("LblNome");
            Label LblCNPJ = (Label)e.Item.FindControl("LblCNPJ");
            Label LblEndereco = (Label)e.Item.FindControl("LblEndereco");
            Label LblTelefone = (Label)e.Item.FindControl("LblTelefone");
            Label LblSaldo = (Label)e.Item.FindControl("LblSaldo");
            Label LblLogin = (Label)e.Item.FindControl("LblLogin");

            LnkDeleteList.CommandArgument = registroSelecionado.Codigo.ToString();
            LnkEditList.CommandArgument = registroSelecionado.Codigo.ToString();
            LnkBoasVindasList.CommandArgument = registroSelecionado.Codigo.ToString();
            LnkCampanhasList.CommandArgument = registroSelecionado.Codigo.ToString();
            LnkEventosList.CommandArgument = registroSelecionado.Codigo.ToString();
            LnkDeleteList.OnClientClick = "return ConfirmacaoExclucao(this, event,'" + registroSelecionado.Codigo + "');";

            HtmlGenericControl IcnBoasVindasList = (HtmlGenericControl)e.Item.FindControl("IcnBoasVindasList");

            if (String.IsNullOrEmpty(registroSelecionado.CpfCnpj))
                LblCNPJ.Text = "--";
            else
                LblCNPJ.Text = ImunneVacinas.Utils.FormatarCpfCnpj(registroSelecionado.CpfCnpj);

            LblNome.Text = registroSelecionado.NomeFantasia;
            LblTelefone.Text = registroSelecionado.Telefone;
            LblEndereco.Text = registroSelecionado.Endereco + ", "
                             + registroSelecionado.Numero + " - "
                             + registroSelecionado.IdCidade.Nome + "/"
                             + registroSelecionado.IdUf.Sigla;

            IcnBoasVindasList.Attributes.Clear();
            if (registroSelecionado.EnvioBoasVindas == 1)
            {
                IcnBoasVindasList.Attributes.Add("class", "fa fa-envelope text-regular");
                LnkBoasVindasList.ToolTip = String.Format("Boas-Vindas enviadas em {0}.", registroSelecionado.DataEnvio.ToString("dd/MM/yyyy"));
            }
            else IcnBoasVindasList.Attributes.Add("class", "fa fa-envelope-o text-regular");

            ImunneVacinas.PesquisaCampanhaClinica pesquisa = new ImunneVacinas.PesquisaCampanhaClinica()
            {
                Clinica = registroSelecionado.Codigo
            };

            int totalCampanhas = ImunneVacinas.CampanhaClinica.ConsultarCampanhasClinicas(pesquisa).Count;
            LblTotalCampanhas.Text = totalCampanhas.ToString();

            ImunneVacinas.PesquisaCompra pesquisaCompra = new ImunneVacinas.PesquisaCompra()
            {
                Clinica = registroSelecionado.Codigo
            };

            int totalCompras = ImunneVacinas.Compra.ConsultarTotalCompras(pesquisaCompra);
            LblTotalCompras.Text = totalCompras.ToString();

            if (totalCampanhas > 0 || totalCompras > 0)
                LnkDeleteList.Visible = false;

            if (totalCampanhas <= 0)
                LnkCampanhasList.Visible = false;

            #region SALDO DE ESTOQUE

            decimal totalEntradas = 0;
            decimal totalSaidas = 0;
            decimal saldoFinal = 0;

            ImunneVacinas.PesquisaMovimentacaoEstoque pesquisaEstoque = new ImunneVacinas.PesquisaMovimentacaoEstoque();
            pesquisaEstoque.Clinica = registroSelecionado.Codigo;
            pesquisaEstoque.Tipo = ImunneVacinas.Utils.TipoMovimentacao.Entrada;
            totalEntradas = ImunneVacinas.MovimentacaoEstoque.ConsultarTotaisMovimentacao(pesquisaEstoque);

            pesquisaEstoque.Tipo = ImunneVacinas.Utils.TipoMovimentacao.Saida;
            totalSaidas = (ImunneVacinas.MovimentacaoEstoque.ConsultarTotaisMovimentacao(pesquisaEstoque) * -1);

            saldoFinal = totalEntradas + totalSaidas;
            LblSaldo.Text = saldoFinal.ToString();

            if (saldoFinal < 0)
                LblSaldo.CssClass = "text-red text-bold";
            else LblSaldo.CssClass = "text-blue text-bold";

            #endregion

            #region USUÁRIO CLÍNICA

            ImunneVacinas.PesquisaUsuario pesquisaUsuario = new ImunneVacinas.PesquisaUsuario()
            {
                Quantidade = 1,
                Clinica = registroSelecionado.Codigo,
                Situacao = ImunneVacinas.Utils.SituacaoRegistro.Ativo,
                Tipo = 7
            };

            List<ImunneVacinas.Usuario> usuariosClinica = ImunneVacinas.Usuario.ConsultarUsuarios(pesquisaUsuario);
            if (usuariosClinica != null && usuariosClinica.Count > 0)
                LblLogin.Text = usuariosClinica[0].Login.ToLower();
            else LblLogin.Text = "--";

            #endregion

            LnkCampanhasList.Visible = false;
        }
        catch (Exception ex)
        {
        }
    }

    #region Paginação

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void DdlPaginas_SelectedIndexChanged(object sender, EventArgs e)
    {
        PaginaAtual = Convert.ToInt32(DdlPaginas.SelectedValue);
        VincularItens();
    }

    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BtnPesquisar_Click(object sender, EventArgs e)
    {
        PaginaAtual = 0;
        VincularItens();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BtnNovo_Click(object sender, EventArgs e)
    {
        url = new StringBuilder();
        parametros = new StringBuilder();

        parametros.AppendFormat("acao:{0}|", "insert");
        parametros.AppendFormat("codigo:{0}|", "-1");
        parametros.AppendFormat("cidade:{0}|", DdlCidades.SelectedValue);
        parametros.AppendFormat("uf:{0}|", DdlUfs.SelectedValue);
        parametros.AppendFormat("pesquisa:{0}|", TxtPesquisa.Text);
        parametros.AppendFormat("current_page:{0}", PaginaAtual);

        url.AppendFormat("~/admin/edit-clinicas.aspx?clinica={0}", ImunneVacinas.Criptografia.Criptografar(parametros.ToString()));

        Response.Redirect(url.ToString(), false);
        Context.ApplicationInstance.CompleteRequest();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BtnExcluirItens_Click(object sender, EventArgs e)
    {
        ExcluirRegistro();
    }

    /// <summary>
    /// 
    /// </summary>
    private void ExcluirRegistro()
    {
        int codigoRegistro = Convert.ToInt32(HdfCodigo.Value);
        ImunneVacinas.Clinica registroSelecionado = ImunneVacinas.Clinica.ConsultarUnico(codigoRegistro);

        if (registroSelecionado == null)
            ImunneVacinas.Alerta.showMensagemErro(this.Page, "Oops!", "Nenhum registro foi localizado com o código informado!");
        else
        {
            try
            {
                int resultado = registroSelecionado.Excluir();
                if (resultado > 0)
                {
                    // Log descritivo
                    ImunneVacinas.Log logAcao = new ImunneVacinas.Log()
                    {
                        Tela = Path.GetFileName(Request.PhysicalPath),
                        TabelaAcao = "Clinicas",
                        TipoAcao = ImunneVacinas.Utils.AcaoLog.Exclusao,
                        Registro = registroSelecionado.Codigo,
                        Resumo = String.Format("EXCLUSÃO DE REGISTRO {0}.", registroSelecionado.Codigo),
                        DataAcao = DateTime.Now,
                        Campo = String.Empty,
                        ValorOriginal = String.Empty,
                        ValorNovo = String.Empty,
                    };

                    logAcao.Incluir();

                    ImunneVacinas.Alerta.showMensagemSucesso(this.Page, "Sucesso!", "Registro excluído com sucesso!");
                    HdfCodigo.Value = "";

                    VincularItens();
                }
                else ImunneVacinas.Alerta.showMensagemErro(this.Page, "Oops!", "Infelizmente não foi possível concluir a solicitação, tente novamente ou informe o suporte.");
            }
            catch (Exception ex)
            {
                this.SalvarException(ex);
                ImunneVacinas.Alerta.showMensagemErro(this.Page, "Oops!", "Infelizmente não foi possível concluir a solicitação, tente novamente ou informe o suporte.");
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void DdlUfs_SelectedIndexChanged(object sender, EventArgs e)
    {
        string siglaUf = DdlUfs.SelectedValue;
        DdlCidades.Items.Clear();

        if (siglaUf != "-1")
        {
            DdlCidades.DataSource = ImunneVacinas.Cidade.ConsultarComboBox(siglaUf, true, "CIDADES");
            DdlCidades.DataBind();
            DdlCidades.Enabled = true;
        }
        else
        {
            DdlCidades.Items.Add(new ListItem("CIDADES", "-1"));
            DdlCidades.Enabled = false;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BtnExportar_Click(object sender, EventArgs e)
    {
        if (RptPrincipal.Items.Count <= 0)
        {
            ImunneVacinas.Alerta.showMensagemAviso(this.Page, "Aviso!", "Sua pesquisa não possui nenhum resultado para exportação da listagem.");
            return;
        }
        else ImunneVacinas.ExportacoesExcel.NovoExportarClinicas(ConsultarRegistros());
    }
}