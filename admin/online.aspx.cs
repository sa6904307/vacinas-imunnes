﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class restrict_online : System.Web.UI.Page
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            carregaUsuariosSession();
    }

    /// <summary>
    /// 
    /// </summary>
    private void carregaUsuariosSession()
    {
        HttpBrowserCapabilities browser = Request.Browser;

        try
        {
            // DataTable que vai receber os dados os usuários conectados
            DataTable dtSessions = new System.Data.DataTable();
            dtSessions.Columns.Add("session");              // 0
            dtSessions.Columns.Add("usuario");              // 1
            dtSessions.Columns.Add("role");                 // 2
            dtSessions.Columns.Add("imobuser");             // 3
            dtSessions.Columns.Add("nomeuser");             // 4
            dtSessions.Columns.Add("login");                // 5
            dtSessions.Columns.Add("imobiliaria");          // 6
            dtSessions.Columns.Add("ultimaatualizacao");    // 7
            dtSessions.Columns.Add("timeout");              // 8
            dtSessions.Columns.Add("ultimapagina");         // 9
            dtSessions.Columns.Add("ip");                   // 10
            dtSessions.Columns.Add("minutosrestantes");     // 11
            dtSessions.Columns.Add("browser");              // 12
            dtSessions.Columns.Add("avatar");               // 13

            foreach (string applicationKey in Application.AllKeys)
            {
                if (applicationKey != null && Application.Get(applicationKey).GetType().FullName == "System.Web.SessionState.HttpSessionState")
                {
                    System.Web.SessionState.HttpSessionState ses = ((System.Web.SessionState.HttpSessionState)Application.Get(applicationKey));

                    // Criando uma nova linha
                    DataRow dr = dtSessions.NewRow();

                    // Criando um elemento do tipo Cliente
                    ImunneVacinas.Clinica clinicaSelecionada = new ImunneVacinas.Clinica();

                    // Criando um elemento do tipo Usuario
                    ImunneVacinas.Usuario usuarioSelecionado = new ImunneVacinas.Usuario();

                    // Verificando se a sessão é anonima ou não
                    if (!String.IsNullOrEmpty(ses["codigoUsuario"].ToString()) && ses["codigoUsuario"].ToString() != "anon")
                    {
                        string nomeClinica = String.Empty;

                        if (Convert.ToInt32(ses["clinica"]) > 0)
                            clinicaSelecionada = ImunneVacinas.Clinica.ConsultarUnico(Convert.ToInt32(ses["clinica"]));


                        if (clinicaSelecionada != null)
                            nomeClinica = clinicaSelecionada.NomeFantasia.ToUpper();

                        try
                        {
                            int codigoUsuario = Convert.ToInt32(ses["codigoUsuario"]);

                            // Consultado a base para obter os dados do cliente
                            usuarioSelecionado = ImunneVacinas.Usuario.ConsultarUnico(Convert.ToInt32(ses["codigoUsuario"]));
                            if (usuarioSelecionado != null)
                            {
                                dr[1] = usuarioSelecionado.Codigo;
                                dr[4] = ImunneVacinas.Utils.RetornarTextoCapitalizado(usuarioSelecionado.Nome.ToLower());
                                dr[5] = usuarioSelecionado.Login;
                                dr[6] = nomeClinica;
                                dr[2] = usuarioSelecionado.Tipo.Descricao.ToUpper();

                                //if (String.IsNullOrEmpty(usuarioSelecionado.FotoUsuario) || usuarioSelecionado.FotoUsuario.ToLower() == "NENHUM")
                                //    dr[13] = "../img/corretores/generic.png";
                                //else
                                //    dr[13] = "../img/corretores/" + usuarioSelecionado.FotoUsuario;

                                // Dados sobre página, acesso e session
                                dr[7] = ses["ultimaatualizacao"];
                                dr[8] = ses.Timeout;
                                dr[9] = ses["ultimapagevisitada"];

                                dr[10] = ses["ip"];

                                try
                                {
                                    TimeSpan t = new TimeSpan();
                                    t = Convert.ToDateTime(ses["ultimaatualizacao"]).AddMinutes(ses.Timeout) - DateTime.Now;
                                    dr[11] = t.Hours.ToString().PadLeft(2, '0') + ":" + t.Minutes.ToString().PadLeft(2, '0') + ":" + t.Seconds.ToString().PadLeft(2, '0');
                                }
                                catch
                                {
                                }

                                dr[12] = ses["browser"];
                                dtSessions.Rows.Add(dr);
                            }
                        }
                        catch (Exception ex)
                        {
                            this.SalvarException(ex);
                        }
                    }
                }
            }

            dtlUsuariosOnline.DataSource = dtSessions;
            dtlUsuariosOnline.DataBind();
        }
        catch (Exception ex)
        {
            this.SalvarException(ex);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void tmrUsuariosOnline_Tick(object sender, EventArgs e)
    {
        carregaUsuariosSession();
    }
}