﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/login.master" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="_default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        label {
            display: inline-block;
            max-width: 100%;
            margin: 0px 0px 5px 5px;
            font-weight: bold;
        }
    </style>
    <script type="text/javascript">
        function ShowHidePassword() {
            if (document.getElementById("<%=chkMostraSenha.ClientID%>").checked) {
                document.getElementById("<%=TxtPassword.ClientID%>").type = "text";
            }
            else {
                document.getElementById("<%=TxtPassword.ClientID%>").type = "password";
            }
        };
    </script>
    <script type="text/javascript">
        function validarLogin() {
            // Login
            if (document.getElementById("<%=TxtLogin.ClientID %>").value == "") {
                MensagemGenerica("Aviso!", "Por favor, informe seu login para acessar a plataforma.", "warning");
                return false;
            }

            // Password
            if (document.getElementById("<%=TxtPassword.ClientID%>").value == "") {
                MensagemGenerica("Aviso!", "Por favor, informe sua senha para acessar a plataforma.", "warning");
                return false;
            }
        };

        function validarRecuperacao() {
            // Login
            if (document.getElementById("<%=TxtLogin.ClientID %>").value == "") {
                MensagemGenerica("Aviso!", "Por favor, informe seu login para solicitar sua recuperação de senha.", "warning");
                return false;
            }
        };
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="UpdLogin" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="login-box animated">
                <div class="login-box-body">
                    <asp:Image ID="imgLogo" runat="server" CssClass="img-responsive" ImageUrl="~/img/logos/logo-main.png" /><br />
                    <asp:Panel ID="pnlLogin" runat="server" DefaultButton="BtnLogin">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group has-feedback">
                            <asp:TextBox ID="TxtLogin" runat="server" CssClass="form-control text-lowercase" Placeholder="Login"></asp:TextBox>
                            <span class="glyphicon glyphicon-user form-control-feedback" style="margin-right: 18px;"></span>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group has-feedback">
                            <asp:TextBox ID="TxtPassword" runat="server" CssClass="form-control" Placeholder="Senha" TextMode="Password"></asp:TextBox>
                            <span class="glyphicon glyphicon-lock form-control-feedback" style="margin-right: 18px;"></span>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left">
                            <asp:CheckBox ID="chkMostraSenha" runat="server" Text="Mostrar senha" onclick="ShowHidePassword();" />
                        </div>
                        <div class="row">
                            <div class="dv-spc-10"></div>
                            <div class="col-xs-12 text-right">
                                <asp:LinkButton ID="BtnLogin" runat="server" CssClass="btn btn-success btn-lg btn-block btn-flat" OnClick="BtnLogin_Click" OnClientClick="return validarLogin();">
                            <i class="glyphicon glyphicon-log-in"></i>&nbsp;Acessar
                                </asp:LinkButton>
                            </div>
                            <div class="dv-spc-10"></div>
                            <div class="col-xs-12 text-right hidden">
                                <asp:LinkButton ID="LnkRecover" runat="server" CssClass="btn btn-success btn-lg btn-block btn-flat" OnClick="LnkRecover_Click" OnClientClick="return validarRecuperacao()">
                            <i class="glyphicon glyphicon-lock"></i>&nbsp;Recuperar senha
                                </asp:LinkButton>
                            </div>
                        </div>
                    </asp:Panel>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

