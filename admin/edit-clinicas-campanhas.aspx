﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="edit-clinicas-campanhas.aspx.cs" Inherits="edit_clinicas_campanhas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row">
        <div class="col-md-12">
            <ul class="breadcrumb">
                <li><a href="dashboard.aspx"><i class="fa fa-home"></i>&nbsp;Home</a></li>
                <li>
                    <asp:HyperLink ID="LnkCampanhas" runat="server" Text="Campanhas"></asp:HyperLink>
                </li>
                <li>
                    <asp:HyperLink ID="LnkClinicas" runat="server" Text="Clínicas - Campanhas"></asp:HyperLink>
                </li>
                <li class="active">Inserção/Edição</li>
            </ul>
        </div>
    </div>
    <h3><i class="fa fa-hospital-o"></i>&nbsp;<asp:Label ID="LblForms" runat="server"></asp:Label></h3>
    <asp:UpdatePanel ID="UpdMain" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PnlPesquisar" runat="server" DefaultButton="BtnPesquisar">
                <div class="box box-success">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                <asp:DropDownList ID="DdlCampanhas" runat="server" CssClass="form-control text-uppercase" DataValueField="ValueMember" DataTextField="DisplayMember" Enabled="false"></asp:DropDownList>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 form-group">
                                <asp:DropDownList ID="DdlUfs" runat="server" CssClass="form-control text-uppercase" DataValueField="ValueMember" DataTextField="DisplayMember" AutoPostBack="true" OnSelectedIndexChanged="DdlUfs_SelectedIndexChanged"></asp:DropDownList>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10 form-group">
                                <asp:DropDownList ID="DdlCidades" runat="server" CssClass="form-control text-uppercase" DataValueField="ValueMember" DataTextField="DisplayMember"></asp:DropDownList>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                <asp:TextBox ID="TxtPesquisa" runat="server" CssClass="form-control" placeHolder="PESQUISAR..."></asp:TextBox>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 pull-right">
                                <asp:LinkButton ID="BtnPesquisar" runat="server" CssClass="btn btn-block btn-success" data-toggle="tooltip" data-container="body" ToolTip="Pesquisar registros" CausesValidation="false" OnClick="BtnPesquisar_Click">
                                        <i class="fa fa-search"></i>&nbsp;Pesquisar
                                </asp:LinkButton>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 text-left bloco-legenda">
                                    <a href="#" class="dropdown-toggle btn btn-block btn-primary" data-toggle="dropdown">
                                        <i class="fa fa-info-circle"></i>&nbsp;Legenda
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <div style="padding: 8px;">
                                                <i class="fa fa-trash text-regular"></i>&nbsp;Excluir registro
                                            </div>
                                        </li>
                                    </ul>
                                    <div class="dv-spc-10"></div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10 box-resultados text-right conteudo-centro">
                                    <asp:Label ID="LblRegistros" runat="server"></asp:Label>
                                </div>
                            </div>
                            <div class="dv-spc-10"></div>
                            <div class="box-grids">
                                <div class="dv-grid-f-header">
                                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="display: inline-block; float: none;">
                                        Ações
                                    </div>
                                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="display: inline-block; float: none;">
                                        Clínica
                                    </div>
                                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="display: inline-block; float: none;">
                                        Endereço
                                    </div>
                                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" style="display: inline-block; float: none;">
                                        Cidade
                                    </div>
                                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 text-center" style="display: inline-block; float: none;">
                                        Telefone
                                    </div>
                                </div>
                                <div class="dv-grid-f">
                                    <asp:Repeater ID="RptPrincipal" runat="server" OnItemDataBound="RptPrincipal_ItemDataBound">
                                        <ItemTemplate>
                                            <div id="dvItem" runat="server">
                                                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-left" style="display: inline-block; float: none;">
                                                    <div style="margin-top: -42px; position: absolute;">
                                                        <asp:CheckBox ID="ChkSelecionar" runat="server" CssClass="checkbox" Text="SELECIONAR" />
                                                    </div>
                                                </div>
                                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="display: inline-block; float: none;">
                                                    <div class="truncate">
                                                        <asp:HiddenField ID="HdfCodigo" runat="server" />
                                                        <asp:Label ID="LblNome" runat="server" CssClass="text-uppercase"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="display: inline-block; float: none;">
                                                    <div class="truncate">
                                                        <asp:Label ID="LblEndereco" runat="server" CssClass="text-uppercase"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" style="display: inline-block; float: none;">
                                                    <div class="truncate">
                                                        <asp:Label ID="LblCidade" runat="server" CssClass="text-uppercase"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 text-center" style="display: inline-block; float: none;">
                                                    <div class="truncate">
                                                        <asp:Label ID="LblTelefone" runat="server" CssClass="text-uppercase"></asp:Label>
                                                    </div>
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="fixed-button-box">
                <div class="text-right">
                    <asp:LinkButton ID="BtnSalvar" runat="server" CssClass="btn btn-success btn-lg"
                        data-toggle="tooltip" data-container="body" ToolTip="Salvar informações"
                        ValidationGroup="edicao" OnClientClick="return validar();" OnClick="BtnSalvar_Click">
                    <i class="fa fa-save"></i>
                    </asp:LinkButton>
                    &nbsp;
                <asp:LinkButton ID="BtnCancelar" runat="server"
                    CssClass="btn btn-danger btn-lg pull-right" data-toggle="tooltip" data-container="body" ToolTip="Cancelar operação"
                    Text="Cancelar" CausesValidation="false">
                    <i class="fa fa-times"></i>
                </asp:LinkButton>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
