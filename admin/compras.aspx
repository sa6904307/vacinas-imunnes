﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="compras.aspx.cs" Inherits="admin_compras" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js" type="text/javascript"></script>
    <link href="../vendors/datepicker/css/datepicker3.css" rel="stylesheet" type="text/css" />
    <link href="../vendors/daterangepicker/css/daterangepicker-bs3.css" rel="stylesheet"
        type="text/css" />
    <link href="../vendors/timeticker/css/bootstrap-timepicker.min.css" rel="stylesheet"
        type="text/css" />
    <script src="../vendors/daterangepicker/js/daterangepicker.js" type="text/javascript"></script>
    <script src="../vendors/datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
    <script src="../vendors/timeticker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
    <script src="../vendors/datepicker/locales/bootstrap-datepicker.pt-BR.js" type="text/javascript"></script>
    <script type="text/javascript">
        var main = document.getElementById('treinamentos');
        document.addEventListener('DOMContentLoaded', function () {
            var inicio = document.getElementById("<%=HdfInicio.ClientID%>");
            var final = document.getElementById("<%=HdfFinal.ClientID%>");

            if (inicio.value == null || inicio.value == '')
                $('#daterange-btn span').html("<i class='fa fa-calendar'></i>&nbsp;Datas");
            else
                $('#daterange-btn span').html(inicio.value + ' - ' + final.value);
        });
    </script>
    <link href="../vendors/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <script src="../vendors/global/jquery.min.js"></script>
    <script type="text/javascript" src="../vendors/select2/js/select2.full.min.js"></script>
    <script type="text/javascript">
        var select$ = jQuery.noConflict();
        select$(function () {
            //Initialize Select2 Elements
            select$('.select2').select2()
        })
    </script>
    <script type="text/javascript">
        // Chamada de alerta para confirmação de exclusão
        function ConfirmacaoExclucao(ctl, event, id) {
            var defaultAction = $(ctl).prop("href");
            event.preventDefault();
            swal({
                title: "Deseja excluir?",
                text: 'Uma vez excluídos, os registros não poderão ser recuperados.',
                type: 'question',
                showCancelButton: true,
                confirmButtonText: "<i class='fa fa-check'></i>&nbsp;Sim!",
                cancelButtonText: "<i class='fa fa-times'></i>&nbsp;Não!",
                confirmButtonColor: '#00a65a',
                cancelButtonColor: '#dd4b39',
                showLoaderOnConfirm: true,
                animation: 'slide-from-top',
                allowEscapeKey: false,
                allowOutsideClick: false
            }).then(result => {
                if (result.value) {
                    document.getElementById('<%=HdfCodigo.ClientID%>').value = id;
                    var btn = document.getElementById('<%=BtnExcluirItens.ClientID%>');
                    btn.click();
                    return true;
                }
            })
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row">
        <div class="col-md-12">
            <ul class="breadcrumb">
                <li><a href="dashboard.aspx"><i class="fa fa-home"></i>&nbsp;Home</a></li>
                <li class="active">Compras</li>
            </ul>
            <h1 class="conteudo-centro"><i class="fa fa-tags"></i>&nbsp;Compras</h1>
        </div>
    </div>
    <div class="row">
        <asp:Panel ID="PnlPesquisa" runat="server" DefaultButton="BtnPesquisar">
            <div class="col-md-12">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-sliders"></i>&nbsp;Filtros</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" data-container="body" title="Ocultar/exibir filtros">
                                <i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="box-body" style="display: block;">
                        <div class="row">
                            <asp:UpdatePanel ID="UpdClinicas" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <script type="text/javascript">
                                        Sys.Application.add_load(SetSelectBox);
                                        var select$ = jQuery.noConflict();
                                        function SetSelectBox() {
                                            select$('.select2').select2();
                                        }
                                    </script>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                        <asp:DropDownList ID="DdlClinicas" runat="server" CssClass="form-control text-uppercase select2" DataValueField="ValueMember" DataTextField="DisplayMember" AutoPostBack="true" OnSelectedIndexChanged="DdlClinicas_SelectedIndexChanged"></asp:DropDownList>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                        <asp:DropDownList ID="DdlCampanhas" runat="server" CssClass="form-control text-uppercase select2" DataValueField="ValueMember" DataTextField="DisplayMember" AutoPostBack="true" OnSelectedIndexChanged="DdlCampanhas_SelectedIndexChanged"></asp:DropDownList>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-group">
                                        <asp:DropDownList ID="DdlSituacoesCompras" runat="server" CssClass="form-control text-uppercase select2" DataValueField="ValueMember" DataTextField="DisplayMember"></asp:DropDownList>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-group">
                                        <asp:DropDownList ID="DdlCategorias" runat="server" CssClass="form-control text-uppercase select2" AutoPostBack="true" DataValueField="ValueMember" DataTextField="DisplayMember" OnSelectedIndexChanged="DdlCategorias_SelectedIndexChanged"></asp:DropDownList>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-group">
                                        <asp:DropDownList ID="DdlUtilizacoes" runat="server" CssClass="form-control text-uppercase select2" DataValueField="ValueMember" DataTextField="DisplayMember"></asp:DropDownList>
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="DdlClinicas" />
                                    <asp:AsyncPostBackTrigger ControlID="DdlCategorias" />
                                    <asp:AsyncPostBackTrigger ControlID="DdlLiberacoes" />
                                </Triggers>
                            </asp:UpdatePanel>
                            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-group">
                                <div id="DvDatas" runat="server">
                                    <button type="button" class="btn btn-block btn-primary pull-left" id="daterange-btn">
                                        <span>
                                            <i class="fa fa-calendar"></i>&nbsp;Datas
                                        </span>
                                        <i class="fa fa-chevron-down"></i>
                                    </button>
                                </div>
                                <script type="text/javascript">
                                    //Date range picker
                                    $('#reservation').daterangepicker()
                                    //Date range as a button
                                    $('#daterange-btn').daterangepicker(
                                        {
                                            "locale": {
                                                "format": "MM/DD/YYYY",
                                                "separator": " - ",
                                                "applyLabel": "Aplicar",
                                                "cancelLabel": "Cancelar",
                                                "fromLabel": "Início",
                                                "toLabel": "Final",
                                                "customRangeLabel": "Personalizado",
                                                "daysOfWeek": [
                                                    "Dom",
                                                    "Seg",
                                                    "Ter",
                                                    "Qua",
                                                    "Qui",
                                                    "Sex",
                                                    "Sab"
                                                ],
                                                "monthNames": [
                                                    "Janeiro",
                                                    "Fevereiro",
                                                    "Março",
                                                    "Abril",
                                                    "Maio",
                                                    "Junho",
                                                    "Julho",
                                                    "Agosto",
                                                    "Setembro",
                                                    "Outubro",
                                                    "Novembro",
                                                    "Dezembro"
                                                ],
                                                "firstDay": 1
                                            },
                                            ranges: {
                                                'Limpar': [new Date(1900, 01, 01), new Date(1900, 01, 01)],
                                                'Hoje': [moment(), moment()],
                                                'Ontem': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                                                'Última semana': [moment().subtract(6, 'days'), moment()],
                                                'Últimos 30 dias': [moment().subtract(29, 'days'), moment()],
                                                'Este mês': [moment().startOf('month'), moment().endOf('month')]
                                                //'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                                            },
                                            startDate: moment().subtract(29, 'days'),
                                            endDate: moment()
                                        },
                                        function (start, end) {
                                            if (start.format('DD/MM/YYYY') == "01/02/1900") {
                                                $('#daterange-btn span').html("<i class='fa fa-calendar'></i>&nbsp;Datas");
                                                document.getElementById("<%=HdfInicio.ClientID%>").value = "";
                                                document.getElementById("<%=HdfFinal.ClientID%>").value = "";
                                            }
                                            else {
                                                $('#daterange-btn span').html(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
                                                document.getElementById("<%=HdfInicio.ClientID%>").value = start.format('DD/MM/YYYY');
                                                document.getElementById("<%=HdfFinal.ClientID%>").value = end.format('DD/MM/YYYY');
                                            }
                                        }
                                    )
                                </script>
                            </div>
                            <asp:UpdatePanel ID="UpdLiberacoes" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 form-group">
                                        <asp:DropDownList ID="DdlLiberacoes" runat="server" AutoPostBack="true" CssClass="form-control text-uppercase select2" OnSelectedIndexChanged="DdlLiberacoes_SelectedIndexChanged" DataValueField="ValueMember" DataTextField="DisplayMember"></asp:DropDownList>
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="DdlCategorias" />
                                    <asp:AsyncPostBackTrigger ControlID="DdlLiberacoes" />
                                </Triggers>
                            </asp:UpdatePanel>
                            <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7 form-group">
                                <asp:HiddenField ID="HdfInicio" runat="server" />
                                <asp:HiddenField ID="HdfFinal" runat="server" />
                                <asp:TextBox ID="TxtPesquisa" runat="server" CssClass="form-control" placeHolder="PESQUISAR..."></asp:TextBox>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-group">
                                <asp:DropDownList ID="DdlItens" runat="server" CssClass="form-control text-uppercase select2" DataValueField="ValueMember" DataTextField="DisplayMember"></asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer" style="display: block;">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 pull-right">
                                <asp:LinkButton ID="BtnExportar" runat="server" CssClass="btn btn-block btn-success" data-toggle="tooltip" data-container="body" ToolTip="Exportar registros" CausesValidation="false" OnClick="BtnExportar_Click">
                                        <i class="fa fa-download"></i>&nbsp;Exportar - Geral
                                </asp:LinkButton>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 pull-right">
                                <asp:LinkButton ID="BtnExportarItens" runat="server" CssClass="btn btn-block btn-success" data-toggle="tooltip" data-container="body" ToolTip="Exportar registros" CausesValidation="false" OnClick="BtnExportarItens_Click">
                                        <i class="fa fa-download"></i>&nbsp;Exportar - Itens
                                </asp:LinkButton>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 pull-right">
                                <asp:LinkButton ID="BtnPesquisar" runat="server" CssClass="btn btn-block btn-success" data-toggle="tooltip" data-container="body" ToolTip="Pesquisar registros" CausesValidation="false" OnClick="BtnPesquisar_Click">
                                        <i class="fa fa-search"></i>&nbsp;Pesquisar
                                </asp:LinkButton>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>
    </div>
    <asp:UpdatePanel ID="UpdMain" runat="server">
        <ContentTemplate>
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 text-left bloco-legenda">
                                    <a href="#" class="dropdown-toggle btn btn-block btn-primary" data-toggle="dropdown">
                                        <i class="fa fa-info-circle"></i>&nbsp;Legenda
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <div style="padding: 8px;">
                                                <i class="fa fa-search text-regular" aria-hidden="true"></i>&nbsp;Visualizar itens
                                            </div>
                                        </li>
                                    </ul>
                                    <div class="dv-spc-10"></div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10 box-resultados text-right conteudo-centro">
                                    <asp:Label ID="LblRegistros" runat="server"></asp:Label>
                                </div>
                            </div>
                            <div class="dv-spc-10"></div>
                            <div class="box-grids">
                                <div class="dv-grid-f-header">
                                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="width: 80px !important; display: inline-block; float: none;">
                                        &nbsp;
                                    </div>
                                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="width: 120px !important; display: inline-block; float: none;">
                                        <i class="fa fa-trash"></i>
                                    </div>
                                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="width: 190px !important; display: inline-block; float: none;">
                                        envio
                                    </div>
                                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="width: 120px !important; display: inline-block; float: none;">
                                        campanha
                                    </div>
                                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="width: 240px !important; display: inline-block; float: none;">
                                        aplicação
                                    </div>
                                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="width: 200px !important; display: inline-block; float: none;">
                                        comprovante
                                    </div>
                                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1" style="width: 260px !important; display: inline-block; float: none;">
                                        titular
                                    </div>
                                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1" style="width: 180px !important; display: inline-block; float: none;">
                                        clinica
                                    </div>
                                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1" style="width: 160px !important; display: inline-block; float: none;">
                                        cidade
                                    </div>
                                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="width: 100px !important; display: inline-block; float: none;">
                                        uf
                                    </div>
                                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="width: 150px !important; display: inline-block; float: none;">
                                        data
                                    </div>
                                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="width: 80px !important; display: inline-block; float: none;">
                                        itens
                                    </div>
                                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-left" style="width: 120px !important; display: inline-block; float: none;">
                                        valor
                                    </div>
                                </div>
                                <div class="dv-grid-f" style="height: auto; min-height: 330px;">
                                    <asp:Repeater ID="RptPrincipal" runat="server" OnItemDataBound="RptPrincipal_ItemDataBound" OnItemCommand="RptPrincipal_ItemCommand">
                                        <ItemTemplate>
                                            <div id="dvItem" runat="server">
                                                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="width: 80px !important; display: inline-block; float: none;">
                                                    <div class="truncate">
                                                        <a id="LnkDetalhes" runat="server" data-toggle="modal" title="Visualizar itens" class="openZoomItens" href="#openZoomItens">
                                                            <i class="fa fa-search text-regular" aria-hidden="true"></i>
                                                        </a>
                                                        <asp:HyperLink ID="LnkPrintList" runat="server" Target="_blank">
                                                            <i class="fa fa-print text-regular" aria-hidden="true"></i>
                                                        </asp:HyperLink>
                                                        <asp:LinkButton ID="LnkImpressoesList" CssClass="hidden" runat="server" CommandName="print">
                                                            <i class="fa fa-print text-regular" aria-hidden="true"></i>
                                                        </asp:LinkButton>
                                                        <%--                                                        <asp:LinkButton ID="LnkDeleteList" runat="server" data-toggle="tooltip" data-container="body" ToolTip="Excluir registro" CommandName="delete">
                                                        <i class="fa fa-trash text-regular" aria-hidden="true"></i>
                                                        </asp:LinkButton>--%>
                                                    </div>
                                                </div>
                                                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="width: 120px !important; display: inline-block; float: none;">
                                                    <div class="truncate">
                                                        <asp:CheckBox ID="ChkExclusao" runat="server" CssClass="check" Text="EXCLUIR" Width="90px" AutoPostBack="true" OnCheckedChanged="ChkUtilizacao_CheckedChanged" />
                                                    </div>
                                                </div>
                                                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1" style="width: 190px !important; display: inline-block; float: none;">
                                                    <div class="truncate">
                                                        <asp:HiddenField ID="HdfCodigoCompra" runat="server" />
                                                        <asp:CheckBox ID="ChkUtilizacao" runat="server" CssClass="check" Text=" " Width="90px" AutoPostBack="true" OnCheckedChanged="ChkUtilizacao_CheckedChanged" />
                                                    </div>
                                                </div>
                                                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="width: 120px !important; display: inline-block; float: none;">
                                                    <div class="truncate">
                                                        <asp:Label ID="LblCampanha" runat="server" CssClass="text-uppercase"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="width: 240px !important; display: inline-block; float: none;">
                                                    <div class="truncate">
                                                        <asp:Label ID="LblAplicacao" runat="server" CssClass="text-uppercase"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="width: 200px !important; display: inline-block; float: none;">
                                                    <div class="truncate">
                                                        <asp:Label ID="LblComprovante" runat="server" Text="0" CssClass="text-uppercase"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1" style="width: 260px !important; display: inline-block; float: none;">
                                                    <div class="truncate">
                                                        <asp:Label ID="LblParticipante" runat="server" CssClass="text-uppercase"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1" style="width: 180px !important; display: inline-block; float: none;">
                                                    <div class="truncate">
                                                        <asp:Label ID="LblClinica" runat="server" CssClass="text-uppercase"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1" style="width: 160px !important; display: inline-block; float: none;">
                                                    <div class="truncate">
                                                        <asp:Label ID="LblCidade" runat="server" CssClass="text-uppercase"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="width: 100px !important; display: inline-block; float: none;">
                                                    <div class="truncate">
                                                        <asp:Label ID="LblUf" runat="server" CssClass="text-uppercase"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="width: 150px !important; display: inline-block; float: none;">
                                                    <div class="truncate">
                                                        <asp:Label ID="LblData" runat="server" Text="0" CssClass="text-uppercase"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="width: 80px !important; display: inline-block; float: none;">
                                                    <div class="truncate">
                                                        <asp:Label ID="LblTotalItens" runat="server" Text="0" CssClass="text-uppercase"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-left" style="width: 120px !important; display: inline-block; float: none;">
                                                    <div class="truncate">
                                                        <asp:Label ID="LblValor" runat="server" Text="0" CssClass="text-uppercase"></asp:Label>
                                                    </div>
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                        </div>
                        <div class="dv-spc-10">
                        </div>
                        <div class="box-footer">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                                    <asp:Label ID="LblInfo" runat="server"></asp:Label>
                                    <br />
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                                    <label>Página selecionada</label>&nbsp;
                                    <asp:DropDownList ID="DdlPaginas" runat="server" CssClass="form-control max-drop select2"
                                        AutoPostBack="true" OnSelectedIndexChanged="DdlPaginas_SelectedIndexChanged" data-toggle="tooltip" data-container="body" ToolTip="Escolher página para exibição">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="fixed-button-box">
                            <asp:CheckBox ID="ChkTodos" runat="server" CssClass="hidden" />
                            <asp:LinkButton ID="LnkMarcarTodos" runat="server" data-toggle="tooltip" data-container="body" CssClass="btn btn-success" CausesValidation="false" OnClick="LnkMarcarTodos_Click">
                            </asp:LinkButton>
                            <asp:LinkButton ID="LnkAplicacoes" runat="server" data-toggle="tooltip" data-container="body" ToolTip="Enviar compras autorizadas" CssClass="btn btn-success" CausesValidation="false" OnClick="LnkAplicacoes_Click">
                                    <i class="fa fa-envelope"></i>&nbsp;Enviar
                            </asp:LinkButton>
                            <br />
                            <br />
                            <asp:CheckBox ID="ChkTodosLixo" runat="server" CssClass="hidden" />
                            <asp:LinkButton ID="LnkMarcarTodosLixo" runat="server" data-toggle="tooltip" data-container="body" CssClass="btn btn-danger" CausesValidation="false" OnClick="LnkMarcarTodosLixo_Click">
                            </asp:LinkButton>
                            <asp:LinkButton ID="LnkExclusoes" runat="server" data-toggle="tooltip" data-container="body" ToolTip="Excluir compras" CssClass="btn btn-danger" CausesValidation="false">
                                    <i class="fa fa-trash"></i>&nbsp;Excluir
                            </asp:LinkButton>
                        </div>
                        <div class="modal fade" id="openZoomItens">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header bg-aqua">
                                        <button class="close" data-dismiss="modal">×</button>
                                        <h4>Informações da Compra</h4>
                                    </div>
                                    <div class="modal-body" style="height: auto;">
                                        <iframe id="frmItem" src="" style="border: none; width: 100%; min-height: 350px;"></iframe>
                                    </div>
                                    <div class="modal-footer bg-aqua">
                                        <button type="button" class="btn btn-outline" data-dismiss="modal">
                                            <i class="fa fa-close"></i>&nbsp;FECHAR
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <script type="text/javascript">
                            $(document).on("click", ".openZoomItens", function () {
                                var importValue = $(this).data('id');
                                $(".modal-body #frmItem").attr('src', importValue);
                                $('#openZoomItens').on('hidden.bs.modal', function () {
                                })
                            });
                        </script>
                    </div>
                </div>
            </div>
            <asp:LinkButton ID="BtnExcluirItens" runat="server" CssClass="hidden" OnClick="BtnExcluirItens_Click">
            </asp:LinkButton>
            <asp:HiddenField ID="HdfCodigo" runat="server" />
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="BtnPesquisar" />
            <asp:AsyncPostBackTrigger ControlID="LnkMarcarTodos" />
            <asp:AsyncPostBackTrigger ControlID="LnkMarcarTodosLixo" />
            <asp:AsyncPostBackTrigger ControlID="DdlPaginas" />
            <asp:AsyncPostBackTrigger ControlID="RptPrincipal" />
            <asp:AsyncPostBackTrigger ControlID="LnkAplicacoes" />
            <asp:AsyncPostBackTrigger ControlID="LnkExclusoes" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
