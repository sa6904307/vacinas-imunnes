﻿using System;
using System.Web.UI;

public partial class dashboard : System.Web.UI.Page
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        CarregarDashboards();
        if (!IsPostBack)
            IniciarPagina();
    }

    /// <summary>
    /// 
    /// </summary>
    private void IniciarPagina()
    {
        Page.Title = "S&A Imunizações | Dashboard";
    }

    /// <summary>
    /// 
    /// </summary>
    private void CarregarDashboards()
    {
        UserControl controleHeader = new UserControl();

        int roleUsuario = this.Page.ConsultarSessionTipoUsuario();

        switch (roleUsuario)
        {
            case 5:
                {
                    controleHeader = (UserControl)Page.LoadControl("~/controls/dashboard-admin.ascx");
                }
                break;
            case 6:
                {
                    controleHeader = (UserControl)Page.LoadControl("~/controls/dashboard-admin.ascx");
                }
                break;
            case 7:
                {
                    controleHeader = (UserControl)Page.LoadControl("~/controls/dashboard-clinica.ascx");
                }
                break;
        }

        PnlDashboard.Controls.Add(controleHeader);
    }
}