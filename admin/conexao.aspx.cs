﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

public partial class conexao : System.Web.UI.Page
{
    protected string usuarioAPI = String.Format("{0}", System.Configuration.ConfigurationManager.AppSettings.Get("USUARIO"));
    protected string senhaAPI = String.Format("{0}", System.Configuration.ConfigurationManager.AppSettings.Get("SENHA"));
    protected string urlAPI = System.Configuration.ConfigurationManager.AppSettings.Get("URLBASE");
    protected string endPoint = System.Configuration.ConfigurationManager.AppSettings.Get("ENDPOINT");
    protected string storeCode = System.Configuration.ConfigurationManager.AppSettings.Get("STORECODE");

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            IniciarPagina();
    }

    /// <summary>
    /// 
    /// </summary>
    private void IniciarPagina()
    {
        //try
        //{
        //    string usuarioAPI = String.Format("{0}", System.Configuration.ConfigurationManager.AppSettings.Get("USUARIO"));
        //    string senhaAPI = String.Format("{0}", System.Configuration.ConfigurationManager.AppSettings.Get("SENHA"));
        //    string urlAPI = System.Configuration.ConfigurationManager.AppSettings.Get("URLBASE");
        //    string endPoint = System.Configuration.ConfigurationManager.AppSettings.Get("ENDPOINT");

        //    using (var client = new HttpClient())
        //    {
        //        client.BaseAddress = new Uri(urlAPI);
        //        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        //        client.DefaultRequestHeaders.ConnectionClose = true;


        //        var base64String = Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(string.Format("{0}:{1}", usuarioAPI, senhaAPI)));
        //        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);

        //        ImunneVacinas.Integracao integracao = new ImunneVacinas.Integracao();
        //        integracao.codigoFormaPagamento = "170";

        //        var result = client.PostAsJsonAsync(endPoint, integracao);
        //        Response.Write(result);
        //    }
        //}
        //catch (Exception ex)
        //{
        //    Response.Write(ex.StackTrace);
        //}
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public void DoPost()
    {
        // try
        // {
        //     #region Criando a transação

        //     ImunneVacinas.Integracao novaIntegracao = new ImunneVacinas.Integracao()
        //     {
        //         codigoEstabelecimento = storeCode,
        //         codigoFormaPagamento = "170",
        //         transacao = new ImunneVacinas.Transacao()
        //         {
        //             numeroTransacao = 208,
        //             valor = 888,
        //             parcelas = 1,
        //             idioma = 1
        //         },

        //         dadosCartao = new ImunneVacinas.DadosCartao()
        //         {
        //             nomePortador = "Teste Teste",
        //             numeroCartao = "0000000000000002",
        //             codigoSeguranca = "170",
        //             dataValidade = "12/2020"
        //         }

        //     };

        //     List<ImunneVacinas.ItensDoPedido> itensDoPedido = new List<ImunneVacinas.ItensDoPedido>();
        //     ImunneVacinas.ItensDoPedido item = new ImunneVacinas.ItensDoPedido()
        //     {
        //         categoryName = "Vacinas",
        //         productAmount = 1,
        //         productCategory = "1",
        //         productCode = "26",
        //         productName = "Vacina X",
        //         productUnitaryValue = 39
        //     };
        //     itensDoPedido.Add(item);

        //     novaIntegracao.itensDoPedido = itensDoPedido;
        //     novaIntegracao.dadosCobranca = new ImunneVacinas.DadosCobranca();
        //     novaIntegracao.dadosEntrega = new ImunneVacinas.DadosEntrega();

        //     // Conteudo em Json
        //     var conteudoRequisicao = new StringContent(JsonConvert.SerializeObject(novaIntegracao, Formatting.None, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }), Encoding.UTF8, "application/json");

        //     #endregion

        //     var httpWebRequest = (HttpWebRequest)WebRequest.Create(urlAPI + endPoint);
        //     httpWebRequest.ContentType = "application/json";
        //     httpWebRequest.Method = "POST";
        //     httpWebRequest.Headers.Add(HttpRequestHeader.Authorization, "Basic" + Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(string.Format("{0}:{1}", usuarioAPI, senhaAPI))));
        //     ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

        //     var jsonContent = JsonConvert.SerializeObject(novaIntegracao, Formatting.Indented);
        //     using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
        //     {
        //         string json = JsonConvert.SerializeObject(novaIntegracao);
        //         streamWriter.Write(jsonContent);
        //         streamWriter.Flush();
        //         streamWriter.Close();
        //     }

        //     cdJson.InnerHtml = jsonContent;

        //     var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
        //     using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
        //     {
        //         var result = streamReader.ReadToEnd();

        //         ImunneVacinas.TransacaoRealizada transacaoEfetivada = JsonConvert.DeserializeObject<ImunneVacinas.TransacaoRealizada>(result);
        //         cdJsonRetorno.InnerHtml = JsonConvert.DeserializeObject(result).ToString();
        //     }
        // }
        // catch (Exception ex)
        // {
        //     Response.Write(ex.StackTrace);
        //     cdJsonRetorno.InnerHtml = ex.Message;
        // }
    }

    /// <summary>
    /// 
    /// </summary>
    public void EnviaRequisicaoPOST()
    {
        //string usuarioAPI = System.Configuration.ConfigurationManager.AppSettings.Get("USUARIO");
        //string senhaAPI = System.Configuration.ConfigurationManager.AppSettings.Get("SENHA");
        //string urlAPI = System.Configuration.ConfigurationManager.AppSettings.Get("URLBASE");
        //string endPoint = System.Configuration.ConfigurationManager.AppSettings.Get("ENDPOINT");
        //string storeCode = System.Configuration.ConfigurationManager.AppSettings.Get("STORECODE");

        //try
        //{
        //    Credential credential = new Credential();
        //    credential.user = usuarioAPI;
        //    credential.password = senhaAPI;

        //    Transaction novaTransacao = TransactionBuilder.build();

        //    TransactionBuilder.NewTransaction(storeCode, 170, 1236, 150);
        //    TransactionBuilder.WithSingleCard("Teste Teste", "0000000000000002", "123", "12/2020");
        //    //TransactionBuilder.WithCharging();

        //    RestV3 restCommunication = new RestV3(new BaseURL().sandbox);
        //    restCommunication.TransactionAuthorize(credential, novaTransacao);
        //}
        //catch (Exception ex)
        //{
        //    Response.Write(ex.InnerException.ToString());
        //}
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BtnCode_Click(object sender, EventArgs e)
    {
        // DoPost();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BtnYapay_Click(object sender, EventArgs e)
    {
        // ImunneVacinas.Integracao novaIntegracao = new ImunneVacinas.Integracao()
        // {
        //     codigoEstabelecimento = storeCode,
        //     codigoFormaPagamento = "170",
        //     transacao = new ImunneVacinas.Transacao()
        //     {
        //         numeroTransacao = 1234,
        //         valor = 200,
        //         parcelas = 1,
        //         idioma = 1
        //     },

        //     dadosCartao = new ImunneVacinas.DadosCartao()
        //     {
        //         nomePortador = "Teste Teste",
        //         numeroCartao = "0000000000000001",
        //         codigoSeguranca = "170",
        //         dataValidade = "12/2020"
        //     }

        // };

        // List<ImunneVacinas.ItensDoPedido> itensDoPedido = new List<ImunneVacinas.ItensDoPedido>();
        // ImunneVacinas.ItensDoPedido item = new ImunneVacinas.ItensDoPedido()
        // {
        //     categoryName = "Vacinas",
        //     productAmount = 1,
        //     productCategory = "1",
        //     productCode = "26",
        //     productName = "Vacina X",
        //     productUnitaryValue = 39
        // };
        // itensDoPedido.Add(item);

        // novaIntegracao.itensDoPedido = itensDoPedido;

        // //var tarefa = sendAsync(novaIntegracao);

        // //Response.Write(tarefa.Result);

        // ConexaoDLL();
    }

    /// <summary>
    /// 
    /// </summary>
    public void ConexaoDLL()
    {
        ////try
        ////{
        //Credential credential = new Credential()
        //{
        //    user = usuarioAPI,
        //    storeCode = storeCode,
        //    password = senhaAPI
        //};

        //TransactionBuilder.NewTransaction(storeCode, 170, 2526, 200);
        //TransactionBuilder.WithInstallments(2);
        //TransactionBuilder.WithSingleCard("Teste Teste", "0000000000000001", "170", "12/2020");
        //Transaction novaTransacao = TransactionBuilder.build();

        //RestV3 restCommunication = new RestV3(new BaseURL().sandbox);
        //cdJson.InnerText = JsonConvert.SerializeObject(novaTransacao, Formatting.Indented, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
        //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
        ////Response.Write(restCommunication.TransactionAuthorize(credential, novaTransacao));
        ////string localFile = Server.MapPath("~");
        ////File.WriteAllText(localFile + "howsmyssl-httpclient.html", new HttpClient().GetStringAsync("https://www.howsmyssl.com").Result);
        ////}
        ////catch(Exception ex)
        ////{
        ////    LblResultado.Text = ex.InnerException.InnerException.ToString();
        ////}
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="integracao"></param>
    /// <returns></returns>
    // public static async Task<HttpResponseMessage> sendAsync(ImunneVacinas.Integracao integracao)
    // {
        // ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
        // string usuarioAPI = String.Format("{0}", System.Configuration.ConfigurationManager.AppSettings.Get("USUARIO"));
        // string senhaAPI = String.Format("{0}", System.Configuration.ConfigurationManager.AppSettings.Get("SENHA"));
        // string urlAPI = System.Configuration.ConfigurationManager.AppSettings.Get("URLBASE");
        // string endPoint = System.Configuration.ConfigurationManager.AppSettings.Get("ENDPOINT");
        // string storeCode = System.Configuration.ConfigurationManager.AppSettings.Get("STORECODE");

        // // Conteudo em Json
        // var conteudoRequisicao = new StringContent(JsonConvert.SerializeObject(integracao, Formatting.None, new JsonSerializerSettings
        // { NullValueHandling = NullValueHandling.Ignore }), Encoding.UTF8, "application/json");

        // HttpResponseMessage resposta;
        // using (var cliente = new HttpClient())
        // {
        //     resposta = await cliente.PostAsync(urlAPI + endPoint, conteudoRequisicao);
        // }

        // return resposta;
    // }
}