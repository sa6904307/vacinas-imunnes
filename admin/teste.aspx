﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="teste.aspx.cs" Inherits="teste" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title></title>
    <!-- INFORMANDO SOBRE SER UM LAYOUT RESPONSIVO -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport" />
    <!-- BOOTSTRAP -->
    <link href="../vendors/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js" type="text/javascript" defer="defer"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js" type="text/javascript" defer="defer"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.26.11/dist/sweetalert2.all.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        // Função de mensagem genérica
        function MensagemGenerica(titulo, chamada, tipo) {
            swal({
                title: titulo,
                text: chamada,
                type: tipo,
                animation: 'slide-from-top',
                allowEscapeKey: false,
                allowOutsideClick: false,
                confirmButtonText: "<i class='fa fa-check'></i>&nbsp;OK",
                confirmButtonColor: '#00a65a',
            });
        }

        // Função de confirmações de Mensagens
        function MensagemContinuar(titulo, chamada, tipo) {
            swal({
                title: titulo,
                text: chamada,
                type: tipo,
                confirmButtonText: 'Ok',
                showCancelButton: false
            });
        }

        // Chamada de alerta para confirmação de cancelamento
        function CancelarFormulario(ctl, event, url) {
            var defaultAction = $(ctl).prop("href");
            event.preventDefault();
            swal({
                title: "Aviso!",
                text: 'Deseja realmente cancelar a operação?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: "<i class='fa fa-check'></i>&nbsp;Sim!",
                cancelButtonText: "<i class='fa fa-times'></i>&nbsp;Não!",
                confirmButtonColor: '#00a65a',
                cancelButtonColor: '#dd4b39',
                animation: 'slide-from-top',
                allowEscapeKey: false,
                allowOutsideClick: false
            }).then(result => {
                if (result.value) {
                    window.location.replace(url);
                    return true;
                }
            })
        }

        // Chamada de alerta para confirmação de logoff
        function AltertaLogOff(ctl, event, url) {
            var defaultAction = $(ctl).prop("href");
            event.preventDefault();
            swal({
                title: "Aviso!",
                text: 'Deseja realmente realizar o logoff?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: "<i class='fa fa-check'></i>&nbsp;Sim!",
                cancelButtonText: "<i class='fa fa-times'></i>&nbsp;Não!",
                confirmButtonColor: '#00a65a',
                cancelButtonColor: '#dd4b39',
                animation: 'slide-from-top',
                allowEscapeKey: false,
                allowOutsideClick: false
            }).then(result => {
                if (result.value) {
                    window.location.replace(url);
                    return true;
                }
            })
        }
    </script>
    <!-- BOOTSTRAP -->
    <script src="../vendors/bootstrap/js/bootstrap.min.js" type="text/javascript" defer="defer"></script>
    <script src="../vendors/global/format.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scmMain" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdTeste" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                        <label>HOST</label>
                        <asp:TextBox ID="TxtHost" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                        <label>Porta</label>
                        <asp:TextBox ID="TxtPorta" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                        <label>Usuário</label>
                        <asp:TextBox ID="TxtUser" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                        <label>Senha</label>
                        <asp:TextBox ID="TxtSenha" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <br />
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                        <asp:Button ID="btnSend" runat="server" Text="Enviar" CssClass="btn btn-success" OnClick="btnSend_Click" />
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
