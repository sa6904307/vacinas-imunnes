﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="campanhas.aspx.cs" Inherits="campanhas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js" type="text/javascript"></script>
    <link href="../vendors/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <script src="../vendors/global/jquery.min.js"></script>
    <script type="text/javascript" src="../vendors/select2/js/select2.full.min.js"></script>
    <script type="text/javascript">
        var select$ = jQuery.noConflict();
        select$(function () {
            //Initialize Select2 Elements
            select$('.select2').select2()
        })
    </script>
    <script type="text/javascript">
        // Chamada de alerta para confirmação de exclusão
        function ConfirmacaoExclucao(ctl, event, id) {
            var defaultAction = $(ctl).prop("href");
            event.preventDefault();
            swal({
                title: "Deseja excluir?",
                text: 'Uma vez excluído, o registro não poderá ser recuperado.',
                type: 'question',
                showCancelButton: true,
                confirmButtonText: "<i class='fa fa-check'></i>&nbsp;Sim!",
                cancelButtonText: "<i class='fa fa-times'></i>&nbsp;Não!",
                confirmButtonColor: '#00a65a',
                cancelButtonColor: '#dd4b39',
                showLoaderOnConfirm: true,
                animation: 'slide-from-top',
                allowEscapeKey: false,
                allowOutsideClick: false
            }).then(result => {
                if (result.value) {
                    document.getElementById('<%=HdfCodigo.ClientID%>').value = id;
                    var btn = document.getElementById('<%=BtnExcluirItens.ClientID%>');
                    btn.click();
                    return true;
                }
            })
        }

        // Chamada de alerta para confirmação de troca de Situação
        function ConfirmacaoSituacao(ctl, event, id) {
            var defaultAction = $(ctl).prop("href");
            event.preventDefault();
            swal({
                title: "Deseja alterar a situação?",
                text: 'A alteração pode fazer com que algumas funcionalidades deixem de funcionar corretamente.',
                type: 'question',
                showCancelButton: true,
                confirmButtonText: "<i class='fa fa-check'></i>&nbsp;Sim!",
                cancelButtonText: "<i class='fa fa-times'></i>&nbsp;Não!",
                confirmButtonColor: '#00a65a',
                cancelButtonColor: '#dd4b39',
                showLoaderOnConfirm: true,
                animation: 'slide-from-top',
                allowEscapeKey: false,
                allowOutsideClick: false
            }).then(result => {
                if (result.value) {
                    document.getElementById('<%=HdfCodigo.ClientID%>').value = id;
                    var btn = document.getElementById('<%=BtnAlterarSituacao.ClientID%>');
                    btn.click();
                    return true;
                }
            })
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row">
        <div class="col-md-12">
            <ul class="breadcrumb">
                <li><a href="dashboard.aspx"><i class="fa fa-home"></i>&nbsp;Home</a></li>
                <li class="active">Campanhas</li>
            </ul>
            <h1 class="conteudo-centro"><i class="fa fa-ambulance"></i>&nbsp;Campanhas</h1>
        </div>
    </div>
    <asp:UpdatePanel ID="UpdMain" runat="server">
        <ContentTemplate>
            <script type="text/javascript">
                Sys.Application.add_load(SetSelectBox);
                var select$ = jQuery.noConflict();
                function SetSelectBox() {
                    select$('.select2').select2();
                }
            </script>
            <div class="row">
                <div class="col-md-12">
                    <asp:Panel ID="PnlPesquisa" runat="server" DefaultButton="BtnPesquisar">
                        <div class="box box-success">
                            <div class="box-header with-border">
                                <h3 class="box-title"><i class="fa fa-sliders"></i>&nbsp;Filtros</h3>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" data-container="body" title="Ocultar/exibir filtros">
                                        <i class="fa fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="box-body" style="display: block;">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                        <asp:DropDownList ID="DdlEmpresas" runat="server" CssClass="form-control select2" DataValueField="ValueMember" DataTextField="DisplayMember"></asp:DropDownList>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 form-group">
                                        <asp:DropDownList ID="DdlSurtos" runat="server" CssClass="form-control select2" DataValueField="ValueMember" DataTextField="DisplayMember"></asp:DropDownList>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10 form-group">
                                        <asp:TextBox ID="TxtPesquisa" runat="server" CssClass="form-control" placeHolder="PESQUISAR..."></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer" style="display: block;">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 pull-right">
                                        <asp:LinkButton ID="BtnPesquisar" runat="server" CssClass="btn btn-block btn-success" data-toggle="tooltip" data-container="body" ToolTip="Pesquisar registros" CausesValidation="false" OnClick="BtnPesquisar_Click">
                                        <i class="fa fa-search"></i>&nbsp;Pesquisar
                                        </asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 text-left bloco-legenda">
                                    <a href="#" class="dropdown-toggle btn btn-block btn-primary" data-toggle="dropdown">
                                        <i class="fa fa-info-circle"></i>&nbsp;Legenda
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <div style="padding: 8px;">
                                                <i class="fa fa-pencil text-regular" aria-hidden="true"></i>&nbsp;Editar registro
                                            </div>
                                        </li>
                                        <li>
                                            <div style="padding: 8px;">
                                                <i class="fa fa-trash text-regular" aria-hidden="true"></i>&nbsp;Excluir registro
                                            </div>
                                        </li>
                                        <li>
                                            <div style="padding: 8px;">
                                                <i class="fa fa-hospital-o text-regular" aria-hidden="true"></i>&nbsp;Visualizar clínicas
                                            </div>
                                        </li>
                                        <li>
                                            <div style="padding: 8px;">
                                                <i class="fa fa-medkit text-regular" aria-hidden="true"></i>&nbsp;Visualizar produtos
                                            </div>
                                        </li>
                                        <li>
                                            <div style="padding: 8px;">
                                                <i class="fa fa-external-link text-regular" aria-hidden="true"></i>&nbsp;Link de adesões
                                            </div>
                                        </li>
                                        <li>
                                            <div style="padding: 8px;">
                                                <i class="fa fa-map-marker text-regular" aria-hidden="true"></i>&nbsp;Locais p/ adesões
                                            </div>
                                        </li>
                                        <li>
                                            <div style="padding: 8px;">
                                                <i class="fa fa-users text-regular" aria-hidden="true"></i>&nbsp;Adesões
                                            </div>
                                        </li>
                                    </ul>
                                    <div class="dv-spc-10"></div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10 box-resultados text-right conteudo-centro">
                                    <asp:Label ID="LblRegistros" runat="server"></asp:Label>
                                </div>
                            </div>
                            <div class="dv-spc-10"></div>
                            <div class="box-grids">
                                <div class="dv-grid-f-header">
                                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="width: 180px !important; display: inline-block; float: none;">
                                        Ações
                                    </div>
                                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="width: 100px !important; display: inline-block; float: none;">
                                        Clínicas
                                    </div>
                                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="width: 110px !important; display: inline-block; float: none;">
                                        Produtos
                                    </div>
                                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="width: 100px !important; display: inline-block; float: none;">
                                        Compras
                                    </div>
                                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="width: 100px !important; display: inline-block; float: none;">
                                        Parcelas
                                    </div>
                                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" style="width: 590px !important; display: inline-block; float: none;">
                                        Campanha
                                    </div>
                                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="width: 130px !important; display: inline-block; float: none;">
                                        Início
                                    </div>
                                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="width: 130px !important; display: inline-block; float: none;">
                                        Termíno
                                    </div>
                                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="width: 130px !important; display: inline-block; float: none;">
                                        APL. INÍCIO
                                    </div>
                                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="width: 130px !important; display: inline-block; float: none;">
                                        APL. FIM
                                    </div>
                                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" style="width: 300px !important; display: inline-block; float: none;">
                                        Contato
                                    </div>
                                </div>
                                <div class="dv-grid-f">
                                    <asp:Repeater ID="RptPrincipal" runat="server" OnItemDataBound="RptPrincipal_ItemDataBound" OnItemCommand="RptPrincipal_ItemCommand">
                                        <ItemTemplate>
                                            <div id="dvItem" runat="server">
                                                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="width: 180px !important; display: inline-block; float: none;">
                                                    <div class="truncate">
                                                        <asp:LinkButton ID="LnkEditList" runat="server" CommandName="edit" data-toggle="tooltip" data-container="body" ToolTip="Editar registro">
                                                        <i class="fa fa-pencil text-regular" aria-hidden="true"></i>
                                                        </asp:LinkButton>
                                                        <asp:LinkButton ID="LnkDeleteList" runat="server" data-toggle="tooltip" data-container="body" ToolTip="Excluir registro" CommandName="delete">
                                                        <i class="fa fa-trash text-regular" aria-hidden="true"></i>
                                                        </asp:LinkButton>
                                                        <asp:LinkButton ID="LnkClinicasList" runat="server" data-toggle="tooltip" data-container="body" ToolTip="Visualizar clínicas" CommandName="clinicas">
                                                        <i class="fa fa-hospital-o text-regular" aria-hidden="true"></i>
                                                        </asp:LinkButton>
                                                        <asp:LinkButton ID="LnkProdutosList" runat="server" data-toggle="tooltip" data-container="body" ToolTip="Visualizar produtos" CommandName="produtos">
                                                        <i class="fa fa-medkit text-regular" aria-hidden="true"></i>
                                                        </asp:LinkButton>
                                                        <asp:LinkButton ID="LnkGerarLinkList" runat="server" data-toggle="tooltip" data-container="body" ToolTip="Gerar lista de adesões" CommandName="gerar">
                                                        <i class="fa fa-users text-regular" aria-hidden="true"></i>
                                                        </asp:LinkButton>
                                                        <asp:HyperLink ID="LnkAcessarLinkList" runat="server" data-toggle="tooltip" data-container="body" ToolTip="Acessar link" CommandName="acessar">
                                                        <i class="fa fa-external-link text-regular" aria-hidden="true"></i>
                                                        </asp:HyperLink>
                                                        <asp:LinkButton ID="LnkImportarList" runat="server" data-toggle="tooltip" data-container="body" ToolTip="Importar adesões" CommandName="importar">
                                                        <i class="fa fa-upload text-regular" aria-hidden="true"></i>
                                                        </asp:LinkButton>
                                                        <asp:LinkButton ID="LnkLocaisList" runat="server" data-toggle="tooltip" data-container="body" ToolTip="Locais p/ adesões" CommandName="locais">
                                                        <i class="fa fa-map-marker text-regular" aria-hidden="true"></i>
                                                        </asp:LinkButton>
                                                        <asp:LinkButton ID="LnkAdesoesList" runat="server" data-toggle="tooltip" data-container="body" ToolTip="Visualizar adesões" CommandName="adesoes">
                                                        <i class="fa fa-users text-regular" aria-hidden="true"></i>
                                                        </asp:LinkButton>
                                                    </div>
                                                </div>
                                                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="width: 100px !important; display: inline-block; float: none;">
                                                    <div class="truncate">
                                                        <asp:Label ID="LblTotalClinicas" runat="server" Text="0"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="width: 110px !important; display: inline-block; float: none;">
                                                    <div class="truncate">
                                                        <asp:Label ID="LblTotalProdutos" runat="server" Text="0"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="width: 100px !important; display: inline-block; float: none;">
                                                    <div class="truncate">
                                                        <asp:Label ID="LblTotalCompras" runat="server" Text="0"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="width: 100px !important; display: inline-block; float: none;">
                                                    <div class="truncate">
                                                        <asp:Label ID="LblParcelas" runat="server" CssClass="text-uppercase"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" style="width: 590px !important; display: inline-block; float: none;">
                                                    <div class="truncate">
                                                        <b>
                                                            <asp:Label ID="LblIdentificacao" runat="server" CssClass="text-uppercase"></asp:Label></b>
                                                        &nbsp;-&nbsp;
                                                        <asp:Label ID="LblNome" runat="server" CssClass="text-uppercase"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="width: 130px !important; display: inline-block; float: none;">
                                                    <div class="truncate">
                                                        <asp:Label ID="LblInicio" runat="server" CssClass="text-uppercase"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="width: 130px !important; width: 130px !important; display: inline-block; float: none;">
                                                    <div class="truncate">
                                                        <asp:Label ID="LblTermino" runat="server" CssClass="text-uppercase"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="width: 130px !important; display: inline-block; float: none;">
                                                    <div class="truncate">
                                                        <asp:Label ID="LblAplicacoes" runat="server" CssClass="text-uppercase"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="width: 130px !important; display: inline-block; float: none;">
                                                    <div class="truncate">
                                                        <asp:Label ID="LblValidade" runat="server" CssClass="text-uppercase"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" style="width: 300px !important; display: inline-block; float: none;">
                                                    <div class="truncate">
                                                        <asp:Label ID="LblContato" runat="server" CssClass="text-uppercase"></asp:Label>
                                                    </div>
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                        </div>
                        <div class="dv-spc-10">
                        </div>
                        <div class="box-footer">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                                    <asp:Label ID="LblInfo" runat="server"></asp:Label>
                                    <br />
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                                    <label>Página selecionada</label>&nbsp;
                                    <asp:DropDownList ID="DdlPaginas" runat="server" CssClass="form-control max-drop select2"
                                        AutoPostBack="true" OnSelectedIndexChanged="DdlPaginas_SelectedIndexChanged" data-toggle="tooltip" data-container="body" ToolTip="Escolher página para exibição">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="fixed-button-box">
                <asp:LinkButton ID="BtnNovo" runat="server" CssClass="btn btn-success btn-lg" CausesValidation="False" OnClick="BtnNovo_Click" data-toggle="tooltip">
                    <i class="fa fa-plus"></i>
                </asp:LinkButton>
            </div>
            <asp:LinkButton ID="BtnExcluirItens" runat="server" CssClass="hidden" OnClick="BtnExcluirItens_Click">
            </asp:LinkButton>
            <asp:LinkButton ID="BtnAlterarSituacao" runat="server" CssClass="hidden">
            </asp:LinkButton>
            <asp:HiddenField ID="HdfCodigo" runat="server" />
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="BtnPesquisar" />
            <asp:AsyncPostBackTrigger ControlID="RptPrincipal" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
