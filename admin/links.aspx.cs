﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class campanhas_clinicas : System.Web.UI.Page
{
    private StringBuilder url = new StringBuilder();
    private StringBuilder parametros = new StringBuilder();

    #region Private Proprieties

    private int PaginaAtual
    {
        get
        {
            object objPage = ViewState["_PaginaAtual"];
            int _PaginaAtual = 0;
            if (objPage == null)
            {
                _PaginaAtual = 0;
            }
            else
            {
                _PaginaAtual = (int)objPage;
            }
            return _PaginaAtual;
        }
        set
        {
            ViewState["_PaginaAtual"] = value;
        }
    }
    private int PrimeiroIndice
    {
        get
        {

            int _PrimeiroIndice = 0;
            if (ViewState["_PrimeiroIndice"] == null)
            {
                _PrimeiroIndice = 0;
            }
            else
            {
                _PrimeiroIndice = Convert.ToInt32(ViewState["_PrimeiroIndice"]);
            }
            return _PrimeiroIndice;
        }
        set
        {
            ViewState["_PrimeiroIndice"] = value;
        }
    }
    private int UltimoIndice
    {
        get
        {

            int _UltimoIndice = 0;
            if (ViewState["_UltimoIndice"] == null)
            {
                _UltimoIndice = 0;
            }
            else
            {
                _UltimoIndice = Convert.ToInt32(ViewState["_UltimoIndice"]);
            }
            return _UltimoIndice;
        }
        set
        {
            ViewState["_UltimoIndice"] = value;
        }
    }

    #endregion

    #region PagedDataSource

    PagedDataSource _PagedDataSource = new PagedDataSource();

    #endregion

    #region Private Methods

    /// <summary>
    /// 
    /// </summary>
    private void VincularItens()
    {
        try
        {
            List<ImunneVacinas.LinkCampanha> retorno = ConsultarRegistros();

            _PagedDataSource.DataSource = retorno;
            _PagedDataSource.AllowPaging = true;
            _PagedDataSource.PageSize = 10;
            _PagedDataSource.CurrentPageIndex = PaginaAtual;
            ViewState["TotalPages"] = _PagedDataSource.PageCount;

            DdlPaginas.Items.Clear();
            for (int x = 0; x < _PagedDataSource.PageCount; x++)
            {
                DdlPaginas.Items.Add(new ListItem((x + 1).ToString(), x.ToString()));
            }

            DdlPaginas.SelectedValue = PaginaAtual.ToString();

            RptPrincipal.DataSource = _PagedDataSource;
            RptPrincipal.DataBind();
            CriarPaginacao();

            LblRegistros.Text = ImunneVacinas.Utils.MontarLinhaTotalizadora(retorno.Count);
            LblInfo.Text = "Exibindo página " + (PaginaAtual + 1) + " de " + _PagedDataSource.PageCount + ".";
        }
        catch (Exception Ex)
        {
            this.SalvarException(Ex);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void CriarPaginacao()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("PageIndex");
        dt.Columns.Add("PageText");

        PrimeiroIndice = PaginaAtual - 5;


        if (PaginaAtual > 5)
        {
            UltimoIndice = PaginaAtual + 5;
        }
        else
        {
            UltimoIndice = 10;
        }
        if (UltimoIndice > Convert.ToInt32(ViewState["TotalPages"]))
        {
            UltimoIndice = Convert.ToInt32(ViewState["TotalPages"]);
            PrimeiroIndice = UltimoIndice - 10;
        }

        if (PrimeiroIndice < 0)
        {
            PrimeiroIndice = 0;
        }

        for (int i = PrimeiroIndice; i < UltimoIndice; i++)
        {
            DataRow dr = dt.NewRow();
            dr[0] = i;
            dr[1] = i + 1;
            dt.Rows.Add(dr);
        }

        DdlPaginas.SelectedValue = PaginaAtual.ToString();
    }

    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = "S&A Imunizações | Links da Campanha";

        if (!IsPostBack)
            IniciarPagina();
    }

    /// <summary>
    /// 
    /// </summary>
    private void IniciarPagina()
    {
        try
        {
            NameValueCollection clinica = new NameValueCollection();
            NameValueCollection campanha = new NameValueCollection();

            int codigoClinica = -1;
            int codigoCampanha = -1;
            int current_page = -1;

            if (!String.IsNullOrEmpty(Request.QueryString["clinica"]))
            {
                clinica = ImunneVacinas.Utils.RecuperarQueryString(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["clinica"]));
                codigoClinica = Convert.ToInt32(clinica["codigo"]);
            }

            if (!String.IsNullOrEmpty(Request.QueryString["campanha"]))
            {
                campanha = ImunneVacinas.Utils.RecuperarQueryString(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["campanha"]));
                codigoCampanha = Convert.ToInt32(clinica["campanha"]);
                current_page = Convert.ToInt32(clinica["current_page"]);
            }

            LnkClinicas.NavigateUrl = String.Format("~/admin/clinicas.aspx?clinica={0}", Request.QueryString["campanha"]);
            LnkCampanhas.NavigateUrl = String.Format("~/admin/campanhas-clinicas.aspx?campanha={0}&clinica={1}", Request.QueryString["campanha"], Request.QueryString["clinica"]);

            DdlClinicas.DataSource = ImunneVacinas.Clinica.ConsultarComboBox(true, "CLINICAS");
            DdlClinicas.DataBind();
            DdlClinicas.SelectedValue = codigoClinica.ToString();
            DdlClinicas.Enabled = false;

            DdlCampanhas.DataSource = ImunneVacinas.Campanha.ConsultarComboBox(false, "");
            DdlCampanhas.DataBind();
            DdlCampanhas.SelectedValue = codigoCampanha.ToString();
            DdlCampanhas.Enabled = false;

            if (current_page > 0)
                PaginaAtual = current_page;

            VincularItens();
        }
        catch (Exception ex)
        {
            this.SalvarException(ex);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    private List<ImunneVacinas.LinkCampanha> ConsultarRegistros()
    {
        ImunneVacinas.PesquisaLinkCampanha pesquisa = new ImunneVacinas.PesquisaLinkCampanha
        {
            Clinica = Convert.ToInt32(DdlClinicas.SelectedValue),
            Ordenacao = ImunneVacinas.Utils.TipoOrdenacao.porNome
        };

        return ImunneVacinas.LinkCampanha.ConsultarLinks(pesquisa);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void RptPrincipal_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void RptPrincipal_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        ImunneVacinas.LinkCampanha registroSelecionado = (ImunneVacinas.LinkCampanha)e.Item.DataItem;
        ImunneVacinas.Campanha campanhaSelecionada = ImunneVacinas.Campanha.ConsultarUnico(registroSelecionado.Campanha.Codigo);

        LinkButton LnkDeleteList = (LinkButton)e.Item.FindControl("LnkDeleteList");
        LinkButton LnkLinksList = (LinkButton)e.Item.FindControl("LnkLinksList");
        LinkButton LnkStatus = (LinkButton)e.Item.FindControl("LnkStatus");

        Label LblCampanha = (Label)e.Item.FindControl("LblCampanha");
        Label LblID = (Label)e.Item.FindControl("LblID");
        Label LblValidade = (Label)e.Item.FindControl("LblValidade");
        Label LblStatus = (Label)e.Item.FindControl("LblStatus");

        LnkDeleteList.CommandArgument = registroSelecionado.Codigo.ToString();
        LnkLinksList.CommandArgument = registroSelecionado.Codigo.ToString();
        LnkDeleteList.OnClientClick = "return ConfirmacaoExclucao(this, event,'" + registroSelecionado.Codigo + "');";

        LblCampanha.Text = campanhaSelecionada.Nome;
        LblID.Text = campanhaSelecionada.Identificacao;
        LblValidade.Text = registroSelecionado.DataLimite.ToString("dd/MM/yyyy");

        if (registroSelecionado.DataLimite < DateTime.Now)
        {
            LblStatus.Text = "EXPIRADO";
            LnkStatus.CssClass = "btn btn-danger";
        }
    }

    #region Paginação

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void DdlPaginas_SelectedIndexChanged(object sender, EventArgs e)
    {
        PaginaAtual = Convert.ToInt32(DdlPaginas.SelectedValue);
        VincularItens();
    }

    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BtnPesquisar_Click(object sender, EventArgs e)
    {
        PaginaAtual = 0;
        VincularItens();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BtnNovo_Click(object sender, EventArgs e)
    {
        url = new StringBuilder();
        parametros = new StringBuilder();

        parametros.AppendFormat("acao:{0}|", "insert");
        parametros.AppendFormat("current_page:{0}", PaginaAtual);

        url.AppendFormat("~/admin/edit-linksaspx?campanha={0}&clinica={1}&link={2}", Request.QueryString["campanha"], Request.QueryString["clinica"], ImunneVacinas.Criptografia.Criptografar(parametros.ToString()));

        Response.Redirect(url.ToString(), false);
        Context.ApplicationInstance.CompleteRequest();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BtnExcluirItens_Click(object sender, EventArgs e)
    {
        ExcluirRegistro();
    }

    /// <summary>
    /// 
    /// </summary>
    private void ExcluirRegistro()
    {
        int codigoRegistro = Convert.ToInt32(HdfCodigo.Value);
        ImunneVacinas.LinkCampanha registroSelecionado = ImunneVacinas.LinkCampanha.ConsultarUnico(codigoRegistro);

        if (registroSelecionado == null)
            ImunneVacinas.Alerta.showMensagemErro(this.Page, "Oops!", "Nenhum registro foi localizado com o código informado!");
        else
        {
            try
            {
                int resultado = registroSelecionado.Excluir();
                if (resultado > 0)
                {
                    // Log descritivo
                    ImunneVacinas.Log logAcao = new ImunneVacinas.Log()
                    {
                        Tela = Path.GetFileName(Request.PhysicalPath),
                        TabelaAcao = "LinksCampanhas",
                        TipoAcao = ImunneVacinas.Utils.AcaoLog.Exclusao,
                        Registro = registroSelecionado.Codigo,
                        Resumo = String.Format("EXCLUSÃO DE REGISTRO {0}.", registroSelecionado.Codigo),
                        DataAcao = DateTime.Now,
                        Campo = String.Empty,
                        ValorOriginal = String.Empty,
                        ValorNovo = String.Empty,
                    };

                    logAcao.Incluir();

                    ImunneVacinas.Alerta.showMensagemSucesso(this.Page, "Sucesso!", "Registro excluído com sucesso!");
                    HdfCodigo.Value = "";

                    VincularItens();
                }
                else ImunneVacinas.Alerta.showMensagemErro(this.Page, "Oops!", "Infelizmente não foi possível concluir a solicitação, tente novamente ou informe o suporte.");
            }
            catch (Exception ex)
            {
                this.SalvarException(ex);
                ImunneVacinas.Alerta.showMensagemErro(this.Page, "Oops!", "Infelizmente não foi possível concluir a solicitação, tente novamente ou informe o suporte.");
            }
        }
    }
}