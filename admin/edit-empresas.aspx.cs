﻿using System;
using System.Collections.Specialized;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class edit_empresas : System.Web.UI.Page
{
    private StringBuilder url = new StringBuilder();
    private string textoMensagem = String.Empty;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        ScriptManager.GetCurrent(Page).RegisterPostBackControl(BtnSalvar);

        if (!IsPostBack)
            IniciarPagina();
    }

    /// <summary>
    /// 
    /// </summary>
    private void IniciarPagina()
    {
        NameValueCollection empresa = new NameValueCollection();
        int codigoRegistro = -1;
        string comando = String.Empty;

        if (Request.QueryString["empresa"] != null)
        {
            empresa = ImunneVacinas.Utils.RecuperarQueryString(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["empresa"]));

            url = new StringBuilder();
            url.AppendFormat("~/admin/empresas.aspx?empresa={0}", Request.QueryString["empresa"]);
            LnkPai.NavigateUrl = url.ToString();
            BtnCancelar.OnClientClick = "return CancelarFormulario(this, event,'" + url.Replace("~/admin/", "") + "');";

            codigoRegistro = Convert.ToInt32(empresa["codigo"]);
            comando = empresa["acao"].ToString();

            DdlUfs.DataSource = ImunneVacinas.Uf.ConsultarComboBox(true, "UF");
            DdlUfs.DataBind();

            DdlCidades.Items.Add(new ListItem("CIDADES", "-1"));
            DdlCidades.Enabled = false;

            TxtCodigo.Text = codigoRegistro.ToString();
            HdfAcao.Value = comando;

            PopularCampos();

            LblForms.Text = ImunneVacinas.Utils.MontarTituloFormulario(comando);

            switch (comando)
            {
                case "edit":
                    {
                        ConsultarDetalhes(codigoRegistro);
                    }
                    break;
            }

            Page.Title = "S&A Imunizações | " + LblForms.Text;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void PopularCampos()
    {
        TxtBairro.MaxLength = (int)ImunneVacinas.Empresa.GetInfoBairro().Max_length;
        TxtComplemento.MaxLength = (int)ImunneVacinas.Empresa.GetInfoComplemento().Max_length;
        TxtContato.MaxLength = (int)ImunneVacinas.Empresa.GetInfoContato().Max_length;
        TxtEmail.MaxLength = (int)ImunneVacinas.Empresa.GetInfoEmail().Max_length;
        TxtEndereco.MaxLength = (int)ImunneVacinas.Empresa.GetInfoEndereco().Max_length;
        TxtNome.MaxLength = (int)ImunneVacinas.Empresa.GetInfoNomeFantasia().Max_length;
        TxtNumero.MaxLength = (int)ImunneVacinas.Empresa.GetInfoNumero().Max_length;
        TxtRazaoSocial.MaxLength = (int)ImunneVacinas.Empresa.GetInfoRazaoSocial().Max_length;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="codigo"></param>
    private void ConsultarDetalhes(int codigo)
    {
        ImunneVacinas.Empresa registroSelecionado = ImunneVacinas.Empresa.ConsultarUnico(codigo);

        TxtBairro.Text = registroSelecionado.Bairro;
        TxtCEP.Text = registroSelecionado.Cep;
        TxtCodigo.Text = registroSelecionado.Codigo.ToString();
        TxtComplemento.Text = registroSelecionado.Complemento;
        TxtContato.Text = registroSelecionado.Contato;
        TxtCpfCnpj.Text = ImunneVacinas.Utils.FormatarCpfCnpj(registroSelecionado.Cnpj);
        TxtEmail.Text = registroSelecionado.Email;
        TxtEndereco.Text = registroSelecionado.Endereco;
        TxtNome.Text = registroSelecionado.NomeFantasia;
        TxtNumero.Text = registroSelecionado.Numero;
        TxtRazaoSocial.Text = registroSelecionado.RazaoSocial;
        TxtTelefone.Text = registroSelecionado.Telefone;

        DdlUfs.SelectedValue = registroSelecionado.IdUf.Sigla;

        HdfLatitude.Value = registroSelecionado.Latitude;
        HdfLongitude.Value = registroSelecionado.Longitude;

        if (!String.IsNullOrEmpty(registroSelecionado.IdUf.Sigla) && registroSelecionado.IdUf.Sigla != "-1")
        {
            DdlCidades.DataSource = ImunneVacinas.Cidade.ConsultarComboBox(registroSelecionado.IdUf.Sigla, true, "SELECIONE...");
            DdlCidades.DataBind();
            DdlCidades.Enabled = true;
            DdlCidades.SelectedValue = registroSelecionado.IdCidade.Codigo.ToString();
        }

        if (!String.IsNullOrEmpty(registroSelecionado.Logotipo))
            LblLogoTipoAtual.Text = registroSelecionado.Logotipo;
        else
            LblLogoTipoAtual.Text = "Nenhum";
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void TxtCEP_TextChanged(object sender, EventArgs e)
    {
        try
        {
            ImunneVacinas.Cep cepRetorno = ImunneVacinas.Cep.ConsultarCep(TxtCEP.Text.Replace("-", ""));

            if (cepRetorno != null)
            {
                string[] arrayLogradouro = cepRetorno.logradouro.Split(',');

                if (!String.IsNullOrEmpty(cepRetorno.estado.sigla))
                {
                    DdlUfs.SelectedValue = cepRetorno.estado.sigla;
                    DdlUfs.Enabled = false;

                    if (!String.IsNullOrEmpty(cepRetorno.cidade.nome))
                    {
                        DdlCidades.Items.Clear();
                        DdlCidades.DataSource = ImunneVacinas.Cidade.ConsultarComboBox(cepRetorno.estado.sigla, false, "");
                        DdlCidades.DataBind();
                        string nomeCidade = ImunneVacinas.Utils.RemoverAcentos(cepRetorno.cidade.nome).ToUpper();

                        DdlCidades.SelectedValue = DdlCidades.Items.FindByText(nomeCidade).Value;
                        DdlCidades.Enabled = false;
                    }
                }

                // Bairro
                if (!String.IsNullOrEmpty(cepRetorno.bairro))
                {
                    TxtBairro.Text = ImunneVacinas.Utils.RemoverAcentos(cepRetorno.bairro.ToUpper());
                    TxtBairro.Enabled = false;

                    // Logradouro
                    if (!String.IsNullOrEmpty(arrayLogradouro[0]))
                        TxtEndereco.Text = ImunneVacinas.Utils.RemoverAcentos(arrayLogradouro[0].ToUpper());
                }
                else
                {
                    TxtBairro.Text = "";
                    TxtEndereco.Text = "";
                    TxtComplemento.Text = "";
                }

                // Coordenadas
                HdfLatitude.Value = cepRetorno.latitude;
                HdfLongitude.Value = cepRetorno.longitude;

                TxtNumero.Focus();
            }
            else return;
        }
        catch (Exception ex)
        {
            ImunneVacinas.Erro erro = new ImunneVacinas.Erro();
            erro.addArquivo(ex);
            //this.salvaException(ex);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void DdlUfs_SelectedIndexChanged(object sender, EventArgs e)
    {
        string sigalUf = DdlUfs.SelectedValue;

        DdlCidades.Items.Clear();
        DdlCidades.Items.Add(new ListItem("SELECIONE...", "-1"));
        DdlCidades.Enabled = false;

        if (!String.IsNullOrEmpty(sigalUf) && sigalUf != "-1")
        {
            DdlCidades.DataSource = ImunneVacinas.Cidade.ConsultarComboBox(sigalUf, true, "SELECIONE...");
            DdlCidades.DataBind();
            DdlCidades.Enabled = true;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BtnSalvar_Click(object sender, EventArgs e)
    {
        try
        {
            NameValueCollection empresa = new NameValueCollection();

            empresa = ImunneVacinas.Utils.RecuperarQueryString(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["empresa"]));

            int codigoRegistro = ImunneVacinas.Parser.parseInteiro(empresa["codigo"]);
            string comando = empresa["acao"].ToString();

            ImunneVacinas.Empresa novoRegistro;
            ImunneVacinas.Empresa registroOriginal = new ImunneVacinas.Empresa();
            if (codigoRegistro > 0)
            {
                novoRegistro = ImunneVacinas.Empresa.ConsultarUnico(codigoRegistro);
                registroOriginal = ImunneVacinas.Empresa.ConsultarUnico(codigoRegistro);
            }
            else
                novoRegistro = new ImunneVacinas.Empresa();

            novoRegistro.NomeFantasia = TxtNome.Text;
            novoRegistro.Cnpj = ImunneVacinas.Utils.LimparCpfCnpj(TxtCpfCnpj.Text);
            novoRegistro.Cep = TxtCEP.Text;
            novoRegistro.Complemento = TxtComplemento.Text;
            novoRegistro.Contato = TxtContato.Text;
            novoRegistro.Email = TxtEmail.Text;
            novoRegistro.Endereco = TxtEndereco.Text;
            novoRegistro.Bairro = TxtBairro.Text;
            novoRegistro.IdCidade.Codigo = Convert.ToInt32(DdlCidades.SelectedValue);
            novoRegistro.IdUf.Sigla = DdlUfs.SelectedValue;
            novoRegistro.Latitude = HdfLatitude.Value;
            novoRegistro.Longitude = HdfLongitude.Value;
            novoRegistro.Numero = TxtNumero.Text;
            novoRegistro.Telefone = TxtTelefone.Text;
            novoRegistro.RazaoSocial = TxtRazaoSocial.Text;

            if (FupLogotipo.HasFile)
            {
                novoRegistro.Logotipo = DateTime.Now.ToString("yyyyMMddHHmmssfff") + System.IO.Path.GetFileNameWithoutExtension(FupLogotipo.FileName);
                int tam = (int)ImunneVacinas.Empresa.GetInfoLogotipo().Max_length - 4;
                novoRegistro.Logotipo = ImunneVacinas.Utils.TruncarString(novoRegistro.Logotipo, tam) + System.IO.Path.GetExtension(FupLogotipo.FileName);
            }
            else
            {
                if (LblLogoTipoAtual.Text != "Nenhum")
                    novoRegistro.Logotipo = LblLogoTipoAtual.Text;
                else
                    novoRegistro.Logotipo = "";
            }

            int resultado = 0;

            switch (comando)
            {
                case "insert":
                    {
                        resultado = AtualizarArquivo(novoRegistro);

                        if (resultado > 0)
                        {
                            novoRegistro.DataCriacao = DateTime.Now;
                            resultado = novoRegistro.Incluir();
                        }
                    }
                    break;
                case "edit":
                    {
                        resultado = AtualizarArquivo(novoRegistro);

                        if (resultado > 0)
                        {
                            novoRegistro.Codigo = Convert.ToInt32(codigoRegistro);
                            novoRegistro.DataAlteracao = DateTime.Now;
                            resultado = novoRegistro.Alterar();
                        }
                    }
                    break;
            }

            if (resultado <= 0)
            {
                textoMensagem = "Lamentamos mas ocorreu um erro e não foi possível concluir a operação. Tente novamente ou informe o suporte.";
                ImunneVacinas.Alerta.showMensagemErro(this.Page, null, textoMensagem);
                return;
            }

            #region Geração de Logs

            // Log descritivo
            ImunneVacinas.Log logAcao = new ImunneVacinas.Log();
            logAcao.Tela = Path.GetFileName(Request.PhysicalPath);
            logAcao.TabelaAcao = "Empresas";

            switch (comando)
            {
                case "insert":
                    {
                        logAcao.TipoAcao = ImunneVacinas.Utils.AcaoLog.Insercao;
                        logAcao.Registro = resultado;
                        logAcao.Resumo = String.Format("INSERÇÃO DE NOVO REGISTRO {0}.", logAcao.Registro);
                        logAcao.DataAcao = DateTime.Now;
                        logAcao.Campo = String.Empty;
                        logAcao.ValorOriginal = String.Empty;
                        logAcao.ValorNovo = String.Empty;
                        logAcao.Incluir();
                    }
                    break;
                case "edit":
                    {
                        logAcao.TipoAcao = ImunneVacinas.Utils.AcaoLog.Edicao;
                        //ImunneVacinas.RegistraLog.CompararEmpresa(logAcao, registroOriginal, novoRegistro);
                    }
                    break;
            }

            #endregion

            url = new StringBuilder();
            url.AppendFormat("~/admin/empresas.aspx?empresa={0}", Request.QueryString["empresa"]);
            Response.Redirect(url.ToString(), false);
            Context.ApplicationInstance.CompleteRequest();
        }
        catch (Exception ex)
        {
            ImunneVacinas.Erro erro = new ImunneVacinas.Erro();
            erro.addArquivo(ex);
            textoMensagem = "Lamentamos mas ocorreu um erro e não foi possível concluir a operação. Tente novamente ou informe o suporte.";
            ImunneVacinas.Alerta.showMensagemErro(this.Page, null, textoMensagem);
            //this.SalvarException(ex);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="novaEmpresa"></param>
    /// <returns></returns>
    private int AtualizarArquivo(ImunneVacinas.Empresa novaEmpresa)
    {
        try
        {
            if (FupLogotipo.HasFile)
            {
                string pathTemp = Server.MapPath("~/img/empresas");

                //Excluindo arquivo no FTP
                if (!String.IsNullOrEmpty(LblLogoTipoAtual.Text))
                    File.Delete(pathTemp + "//" + LblLogoTipoAtual.Text);

                // Salvando na pasta final
                string nomeArquivo = pathTemp + "//" + novaEmpresa.Logotipo;
                FupLogotipo.SaveAs(nomeArquivo);
            }
            return 1;
        }
        catch (Exception ex)
        {
            ImunneVacinas.Erro erro = new ImunneVacinas.Erro();
            erro.addArquivo(ex);
            return 0;
        }
    }
}