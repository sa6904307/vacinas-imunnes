﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_estoque : System.Web.UI.Page
{
    private StringBuilder url = new StringBuilder();
    private StringBuilder parametros = new StringBuilder();

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = "S&A Imunizações | Movimentações";

        if (!IsPostBack)
            IniciarPagina();
    }

    /// <summary>
    /// 
    /// </summary>
    private void IniciarPagina()
    {
        try
        {
            NameValueCollection clinica = new NameValueCollection();

            int codigoClinica = -1;
            string tipoEventos = "-1";

            if (!String.IsNullOrEmpty(Request.QueryString["clinica"]))
            {
                clinica = ImunneVacinas.Utils.RecuperarQueryString(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["clinica"]));
                codigoClinica = Convert.ToInt32(clinica["codigo"]);
            }

            LnkClinicias.NavigateUrl = String.Format("~/admin/clinicas.aspx?clinica={0}", Request.QueryString["clinica"]);

            DdlClinicas.DataSource = ImunneVacinas.Clinica.ConsultarComboBox(true, "CLINICAS");
            DdlClinicas.DataBind();
            DdlClinicas.SelectedValue = codigoClinica.ToString();
            DdlClinicas.Enabled = false;

            #region PRODUTOS CAMPANHA

            DdlProdutos.DataSource = ImunneVacinas.ProdutoCampanha.ConsultarProdutosClinicaComboBox(codigoClinica, -1, true, "PRODUTOS");
            DdlProdutos.DataBind();

            DdlProdutosForm.DataSource = ImunneVacinas.ProdutoCampanha.ConsultarProdutosClinicaComboBox(codigoClinica, -1, true, "PRODUTOS");
            DdlProdutosForm.DataBind();

            #endregion

            DdlTipos.DataSource = ImunneVacinas.Utils.ConsultarListaEnum<ImunneVacinas.Utils.TipoMovimentacao>();
            DdlTipos.DataBind();
            DdlTipos.Items.Remove(DdlTipos.Items.FindByValue("-1"));
            DdlTipos.Items.Add(new ListItem("TIPOS", "-1"));
            DdlTipos.SelectedValue = tipoEventos;

            DdlTiposForm.DataSource = ImunneVacinas.Utils.ConsultarListaEnum<ImunneVacinas.Utils.TipoMovimentacao>();
            DdlTiposForm.DataBind();
            DdlTiposForm.Items.Remove(DdlTiposForm.Items.FindByValue("-1"));

            PopularRegistros();
        }
        catch (Exception ex)
        {
            this.SalvarException404(ex);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void PopularRegistros()
    {
        List<ImunneVacinas.MovimentacaoEstoque> listaRetornos = ConsultarRegistros();
        if (listaRetornos != null && listaRetornos.Count > 0)
            LblRegistros.Text = ImunneVacinas.Utils.MontarLinhaTotalizadora(listaRetornos.Count);
        else LblRegistros.Text = "Nenhum registro encontrado.";

        RptPrincipal.DataSource = listaRetornos;
        RptPrincipal.DataBind();

        TotalizarEventos();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    private List<ImunneVacinas.MovimentacaoEstoque> ConsultarRegistros()
    {
        ImunneVacinas.PesquisaMovimentacaoEstoque pesquisa = new ImunneVacinas.PesquisaMovimentacaoEstoque()
        {
            Clinica = Convert.ToInt32(DdlClinicas.SelectedValue),
            Produto = Convert.ToInt32(DdlProdutos.SelectedValue),
            Tipo = (ImunneVacinas.Utils.TipoMovimentacao)Enum.Parse(typeof(ImunneVacinas.Utils.TipoMovimentacao), DdlTipos.SelectedValue),
            Ordenacao = ImunneVacinas.Utils.TipoOrdenacao.porDataDesc
        };

        return ImunneVacinas.MovimentacaoEstoque.ConsultarMovimentacoes(pesquisa);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void RptPrincipal_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        int codigoRegistro = Convert.ToInt32(e.CommandArgument);
        string comando = e.CommandName;

        ImunneVacinas.MovimentacaoEstoque registroSelecionado = ImunneVacinas.MovimentacaoEstoque.ConsultarUnico(codigoRegistro);
        ScriptManager.RegisterClientScriptBlock(UpdFormulario, this.GetType(), "Pop", "openFormulario();", true);

        switch (comando)
        {
            case "edit":
                {
                    PopularCampos(codigoRegistro);
                }
                break;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void RptPrincipal_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        try
        {
            ImunneVacinas.MovimentacaoEstoque registroSelecionado = (ImunneVacinas.MovimentacaoEstoque)e.Item.DataItem;
            ImunneVacinas.Usuario usuarioSelecionado = ImunneVacinas.Usuario.ConsultarUnico(registroSelecionado.Usuario.Codigo);

            LinkButton LnkEditList = (LinkButton)e.Item.FindControl("LnkEditList");

            Label LblQtde = (Label)e.Item.FindControl("LblQtde");
            Label LblData = (Label)e.Item.FindControl("LblData");
            Label LblUsuario = (Label)e.Item.FindControl("LblUsuario");
            Label LblHistorico = (Label)e.Item.FindControl("LblHistorico");
            Label LblProduto = (Label)e.Item.FindControl("LblProduto");
            Label LblTipo = (Label)e.Item.FindControl("LblTipo");

            LblData.Text = registroSelecionado.DataCriacao.ToString("dd/MM/yyyy HH:mm");
            LblHistorico.Text = registroSelecionado.Historico;
            LblTipo.Text = registroSelecionado.Tipo.ToString();

            if (usuarioSelecionado != null)
                LblUsuario.Text = usuarioSelecionado.Nome;
            else LblUsuario.Text = "PLATAFORMA";

            LnkEditList.CommandArgument = registroSelecionado.Codigo.ToString();

            switch (registroSelecionado.Tipo)
            {
                case ImunneVacinas.Utils.TipoMovimentacao.Saida:
                    {
                        LblQtde.Text = String.Format("(-) {0}", registroSelecionado.Quantidade.ToString());
                        LblQtde.CssClass = "text-red text-bold";
                    }
                    break;
                case ImunneVacinas.Utils.TipoMovimentacao.Entrada:
                    {
                        LblQtde.Text = String.Format("(+) {0}", registroSelecionado.Quantidade.ToString());
                        LblQtde.CssClass = "text-blue text-bold";
                    }
                    break;
            }

            #region PRODUTO

            if (registroSelecionado.Produto.Codigo > 0)
            {
                ImunneVacinas.Vacina produtoEstoque = ImunneVacinas.Vacina.ConsultarUnico(registroSelecionado.Produto.Codigo);
                if (produtoEstoque != null)
                    LblProduto.Text = produtoEstoque.Nome.ToUpper();
            }
            else LblProduto.Text = "--";

            #endregion
        }
        catch (Exception ex)
        {
            this.SalvarException(ex);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BtnNovo_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterClientScriptBlock(UpdFormulario, this.GetType(), "Pop", "openFormulario();", true);
        LblTituloFormulario.Text = "Novo evento";
        LimparCamposEvento();
        UpdFormulario.Update();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void LnkPesquisar_Click(object sender, EventArgs e)
    {
        PopularRegistros();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="seder"></param>
    /// <param name="e"></param>
    protected void LnkSalvar_Click(object seder, EventArgs e)
    {
        try
        {
            ImunneVacinas.MovimentacaoEstoque novoRegistro = new ImunneVacinas.MovimentacaoEstoque();
            novoRegistro.Clinica.Codigo = Convert.ToInt32(DdlClinicas.SelectedValue);
            novoRegistro.Tipo = (ImunneVacinas.Utils.TipoMovimentacao)Enum.Parse(typeof(ImunneVacinas.Utils.TipoMovimentacao), DdlTiposForm.SelectedValue);
            novoRegistro.Quantidade = Convert.ToInt32(txtQtdeForm.Text);
            novoRegistro.Historico = TxtHistoricoForm.Text;
            novoRegistro.Produto.Codigo = Convert.ToInt32(DdlProdutosForm.SelectedValue);
            novoRegistro.DataCriacao = Convert.ToDateTime(TxtDataForm.Text); //DateTime.Now;
            novoRegistro.Usuario.Codigo = this.Page.ConsultarSessionCodigoUsuario();

            int resultado = novoRegistro.Incluir();
            if (resultado <= 0)
                ImunneVacinas.Alerta.showMensagemAviso(this.Page, "Aviso!", "Infelizmente não foi possível concluir a operação, tente novamente ou informe ao suporte.");
            else
            {
                LimparCamposEvento();
                UpdFormulario.Update();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "HidePopup", "$('#MdlFormulario').modal('hide')", true);

                PopularRegistros();
                TotalizarEventos();
                UpdGrid.Update();
            }
        }
        catch (Exception ex)
        {
            this.SalvarException(ex);
            ImunneVacinas.Alerta.showMensagemAviso(this.Page, "Aviso!", "Infelizmente não foi possível concluir a operação, tente novamente ou informe ao suporte.");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void LimparCamposEvento()
    {
        TxtDataForm.Text = DateTime.Now.ToString("dd/MM/yyyy");

        DvSalvar.Visible = true;

        DdlProdutosForm.SelectedIndex = 0;
        DdlProdutosForm.Enabled = true;

        DdlTiposForm.SelectedIndex = 0;
        DdlTiposForm.Enabled = true;

        TxtHistoricoForm.Text = "";
        txtQtdeForm.Text = "";

        txtQtdeForm.Enabled = true;
        TxtHistoricoForm.Enabled = true;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="codigo"></param>
    private void PopularCampos(int codigo)
    {
        UpdMain.Update();

        TxtHistoricoForm.Enabled = false;
        txtQtdeForm.Enabled = false;
        DdlTiposForm.Enabled = false;

        DvSalvar.Visible = false;

        ImunneVacinas.MovimentacaoEstoque registroSelecionado = ImunneVacinas.MovimentacaoEstoque.ConsultarUnico(codigo);
        if (registroSelecionado != null)
        {
            TxtDataForm.Text = registroSelecionado.DataCriacao.ToString("dd/MM/yyyy");
            txtQtdeForm.Text = registroSelecionado.Quantidade.ToString();
            TxtHistoricoForm.Text = registroSelecionado.Historico;
            DdlTiposForm.SelectedValue = ((int)registroSelecionado.Tipo).ToString();
            DdlProdutosForm.SelectedValue = registroSelecionado.Produto.Codigo.ToString();

            ScriptManager.RegisterClientScriptBlock(UpdFormulario, this.GetType(), "Pop", "openFormulario();", true);
            LblTituloFormulario.Text = "Detalhes do evento";
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void TotalizarEventos()
    {
        decimal totalEntradas = 0;
        decimal totalSaidas = 0;
        decimal saldoFinal = 0;

        ImunneVacinas.PesquisaMovimentacaoEstoque pesquisa = new ImunneVacinas.PesquisaMovimentacaoEstoque()
        {
            Clinica = Convert.ToInt32(DdlClinicas.SelectedValue),
            Tipo = ImunneVacinas.Utils.TipoMovimentacao.Entrada,
            Produto = Convert.ToInt32(DdlProdutos.SelectedValue),
            Ordenacao = ImunneVacinas.Utils.TipoOrdenacao.porDataDesc
        };
        totalEntradas = ImunneVacinas.MovimentacaoEstoque.ConsultarTotaisMovimentacao(pesquisa);

        pesquisa.Tipo = ImunneVacinas.Utils.TipoMovimentacao.Saida;
        totalSaidas = (ImunneVacinas.MovimentacaoEstoque.ConsultarTotaisMovimentacao(pesquisa) * -1);

        saldoFinal = totalEntradas + totalSaidas;

        TxtTotalEntradas.Text = totalEntradas.ToString();
        TxtTotalSaidas.Text = totalSaidas.ToString();
        TxtSaldoFinal.Text = saldoFinal.ToString();

        if (saldoFinal < 0)
            TxtSaldoFinal.CssClass = "form-control text-right text-red text-bold";
        else TxtSaldoFinal.CssClass = "form-control text-right text-blue text-bold";
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BtnExportar_Click(object sender, EventArgs e)
    {
        int codigoCliente = Convert.ToInt32(DdlClinicas.SelectedValue);

        ImunneVacinas.ExportacoesExcel.ExportarMovimentos(codigoCliente, ConsultarRegistros());
    }
}