﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="trocar-senha.aspx.cs" Inherits="troca_senha" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js" type="text/javascript"></script>
    <link href="../vendors/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <script src="../vendors/global/jquery.min.js"></script>
    <script type="text/javascript" src="../vendors/select2/js/select2.full.min.js"></script>
    <script type="text/javascript">
        var select$ = jQuery.noConflict();
        select$(function () {
            //Initialize Select2 Elements
            select$('.select2').select2()
        })
    </script>
    <script type="text/javascript">
        function validar() {
            // Nome
            if (document.getElementById("<%=TxtNome.ClientID%>").value == "") {
                MensagemGenerica("Aviso!", "Por favor, informe o nome do usuário.", "warning");
                return false;
            }

            // Login
            if (document.getElementById("<%=TxtLogin.ClientID%>").value == "") {
                MensagemGenerica("Aviso!", "Por favor, informe o login do usuário.", "warning");
                return false;
            }

            // Senha
            if (document.getElementById("<%=TxtSenha.ClientID%>").value == "") {
                MensagemGenerica("Aviso!", "Por favor, informe a nova senha.", "warning");
                return false;
            }

            // Senha e confirmação diferentes
            if (document.getElementById("<%=TxtSenha.ClientID%>").value != document.getElementById("<%=TxtConfirmar.ClientID%>").value) {
                MensagemGenerica("Aviso!", "As senhas informadas não conferem.", "warning");
                return false;
            }
        };
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <ul class="breadcrumb">
        <li><a href="home.aspx"><i class="fa fa-home"></i>&nbsp;Home</a> </li>
        <li class="active">Troca de Senha</li>
    </ul>
    <h1 class="conteudo-centro"><i class="fa fa-lock"></i>&nbsp;Troca de Senha</h1>
    <asp:UpdatePanel ID="UpdMain" runat="server">
        <ContentTemplate>
            <script type="text/javascript">
                Sys.Application.add_load(SetSelectBox);
                var select$ = jQuery.noConflict();
                function SetSelectBox() {
                    select$('.select2').select2();
                }
            </script>
            <div class="box box-success">
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                            <label>Nome</label>
                            <asp:TextBox ID="TxtNome" runat="server" CssClass="form-control text-uppercase" ReadOnly="true"></asp:TextBox>
                            <asp:HiddenField ID="hdfSenhaAntiga" runat="server" />
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 form-group">
                            <label>Login</label>
                            <asp:TextBox ID="TxtLogin" runat="server" CssClass="form-control text-lowercase" ReadOnly="true"></asp:TextBox>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 form-group">
                            <label>Senha *</label>
                            <asp:TextBox ID="TxtSenha" runat="server" CssClass="form-control text-lowercase" TextMode="Password"></asp:TextBox>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 form-group">
                            <label>Confirmar *</label>
                            <asp:TextBox ID="TxtConfirmar" runat="server" CssClass="form-control text-lowercase" TextMode="Password"></asp:TextBox>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 form-group">
                            <label>Dica</label>
                            <asp:TextBox ID="TxtDica" runat="server" CssClass="form-control text-lowercase"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
            <div class="fixed-button-box">
                <div class="text-right">
                    <asp:LinkButton ID="BtnSalvar" runat="server" CssClass="btn btn-success btn-lg"
                        data-toggle="tooltip" data-container="body" ToolTip="Salvar informações"
                        ValidationGroup="edicao" OnClientClick="return validar();" OnClick="BtnSalvar_Click">
                    <i class="fa fa-save"></i>
                    </asp:LinkButton>
                    &nbsp;
                <asp:LinkButton ID="BtnCancelar" runat="server"
                    CssClass="btn btn-danger btn-lg pull-right" data-toggle="tooltip" data-container="body" ToolTip="Cancelar operação"
                    Text="Cancelar" CausesValidation="false">
                    <i class="fa fa-times"></i>
                </asp:LinkButton>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

