﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class vacinas : System.Web.UI.Page
{
    private StringBuilder url = new StringBuilder();
    private StringBuilder parametros = new StringBuilder();

    #region Private Proprieties

    private int PaginaAtual
    {
        get
        {
            object objPage = ViewState["_PaginaAtual"];
            int _PaginaAtual = 0;
            if (objPage == null)
            {
                _PaginaAtual = 0;
            }
            else
            {
                _PaginaAtual = (int)objPage;
            }
            return _PaginaAtual;
        }
        set
        {
            ViewState["_PaginaAtual"] = value;
        }
    }
    private int PrimeiroIndice
    {
        get
        {

            int _PrimeiroIndice = 0;
            if (ViewState["_PrimeiroIndice"] == null)
            {
                _PrimeiroIndice = 0;
            }
            else
            {
                _PrimeiroIndice = Convert.ToInt32(ViewState["_PrimeiroIndice"]);
            }
            return _PrimeiroIndice;
        }
        set
        {
            ViewState["_PrimeiroIndice"] = value;
        }
    }
    private int UltimoIndice
    {
        get
        {

            int _UltimoIndice = 0;
            if (ViewState["_UltimoIndice"] == null)
            {
                _UltimoIndice = 0;
            }
            else
            {
                _UltimoIndice = Convert.ToInt32(ViewState["_UltimoIndice"]);
            }
            return _UltimoIndice;
        }
        set
        {
            ViewState["_UltimoIndice"] = value;
        }
    }

    #endregion

    #region PagedDataSource

    PagedDataSource _PagedDataSource = new PagedDataSource();

    #endregion

    #region Private Methods

    /// <summary>
    /// 
    /// </summary>
    private void VincularItens()
    {
        try
        {
            List<ImunneVacinas.Vacina> retorno = ConsultarRegistros();

            _PagedDataSource.DataSource = retorno;
            _PagedDataSource.AllowPaging = true;
            _PagedDataSource.PageSize = 10;
            _PagedDataSource.CurrentPageIndex = PaginaAtual;
            ViewState["TotalPages"] = _PagedDataSource.PageCount;

            DdlPaginas.Items.Clear();
            for (int x = 0; x < _PagedDataSource.PageCount; x++)
            {
                DdlPaginas.Items.Add(new ListItem((x + 1).ToString(), x.ToString()));
            }

            DdlPaginas.SelectedValue = PaginaAtual.ToString();

            RptPrincipal.DataSource = _PagedDataSource;
            RptPrincipal.DataBind();
            CriarPaginacao();

            LblRegistros.Text = ImunneVacinas.Utils.MontarLinhaTotalizadora(retorno.Count);
            LblInfo.Text = "Exibindo página " + (PaginaAtual + 1) + " de " + _PagedDataSource.PageCount + ".";
        }
        catch (Exception Ex)
        {
            this.SalvarException(Ex);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void CriarPaginacao()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("PageIndex");
        dt.Columns.Add("PageText");

        PrimeiroIndice = PaginaAtual - 5;


        if (PaginaAtual > 5)
        {
            UltimoIndice = PaginaAtual + 5;
        }
        else
        {
            UltimoIndice = 10;
        }
        if (UltimoIndice > Convert.ToInt32(ViewState["TotalPages"]))
        {
            UltimoIndice = Convert.ToInt32(ViewState["TotalPages"]);
            PrimeiroIndice = UltimoIndice - 10;
        }

        if (PrimeiroIndice < 0)
        {
            PrimeiroIndice = 0;
        }

        for (int i = PrimeiroIndice; i < UltimoIndice; i++)
        {
            DataRow dr = dt.NewRow();
            dr[0] = i;
            dr[1] = i + 1;
            dt.Rows.Add(dr);
        }

        DdlPaginas.SelectedValue = PaginaAtual.ToString();
    }

    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            IniciarPagina();
    }

    /// <summary>
    /// 
    /// </summary>
    private void IniciarPagina()
    {
        Page.Title = "S&A Imunizações | Vacinas";

        try
        {
            NameValueCollection vacina = new NameValueCollection();

            string pesquisa = String.Empty;
            int current_page = -1;

            if (!String.IsNullOrEmpty(Request.QueryString["vacina"]))
            {
                vacina = ImunneVacinas.Utils.RecuperarQueryString(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["vacina"]));
                pesquisa = vacina["pesquisa"];
                current_page = Convert.ToInt32(vacina["current_page"]);
            }

            if (!String.IsNullOrEmpty(pesquisa) && pesquisa != "PESQUISAR")
                TxtPesquisa.Text = pesquisa;

            if (current_page > 0)
                PaginaAtual = current_page;

            VincularItens();
        }
        catch (Exception ex)
        {
            this.SalvarException(ex);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    private List<ImunneVacinas.Vacina> ConsultarRegistros()
    {
        ImunneVacinas.PesquisaVacina pesquisa = new ImunneVacinas.PesquisaVacina
        {
            CampoLivre = TxtPesquisa.Text,
            Ordenacao = ImunneVacinas.Utils.TipoOrdenacao.porNome
        };

        return ImunneVacinas.Vacina.ConsultarVacinas(pesquisa);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void RptPrincipal_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        int codigoRegistro = Convert.ToInt32(e.CommandArgument);
        string comando = e.CommandName;

        ImunneVacinas.Vacina registroSelecionado = ImunneVacinas.Vacina.ConsultarUnico(codigoRegistro);

        switch (comando)
        {
            case "edit":
                {
                    url = new StringBuilder();
                    parametros = new StringBuilder();

                    parametros.AppendFormat("acao:{0}|", "edit");
                    parametros.AppendFormat("codigo:{0}|", codigoRegistro);
                    parametros.AppendFormat("pesquisa:{0}|", TxtPesquisa.Text);
                    parametros.AppendFormat("current_page:{0}", PaginaAtual);

                    url.AppendFormat("~/admin/edit-vacinas.aspx?vacina={0}", ImunneVacinas.Criptografia.Criptografar(parametros.ToString()));

                    Response.Redirect(url.ToString(), false);
                    Context.ApplicationInstance.CompleteRequest();
                }
                break;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void RptPrincipal_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        ImunneVacinas.Vacina registroSelecionado = (ImunneVacinas.Vacina)e.Item.DataItem;

        LinkButton LnkEditList = (LinkButton)e.Item.FindControl("LnkEditList");
        LinkButton LnkDeleteList = (LinkButton)e.Item.FindControl("LnkDeleteList");
        Label LblTotalCampanhas = (Label)e.Item.FindControl("LblTotalCampanhas");
        Label LblNome = (Label)e.Item.FindControl("LblNome");

        LnkDeleteList.CommandArgument = registroSelecionado.Codigo.ToString();
        LnkEditList.CommandArgument = registroSelecionado.Codigo.ToString();
        LnkDeleteList.OnClientClick = "return ConfirmacaoExclucao(this, event,'" + registroSelecionado.Codigo + "');";

        ImunneVacinas.PesquisaProdutoCampanha pesquisa = new ImunneVacinas.PesquisaProdutoCampanha()
        {
            Vacina = registroSelecionado.Codigo
        };

        LblNome.Text = registroSelecionado.Nome;

        int totalCampanhas = ImunneVacinas.ProdutoCampanha.ConsultarProdutoCampanhas(pesquisa).Count;
        LblTotalCampanhas.Text = totalCampanhas.ToString();
        if (totalCampanhas > 0)
            LnkDeleteList.Visible = false;
    }

    #region Paginação

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void DdlPaginas_SelectedIndexChanged(object sender, EventArgs e)
    {
        PaginaAtual = Convert.ToInt32(DdlPaginas.SelectedValue);
        VincularItens();
    }

    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BtnPesquisar_Click(object sender, EventArgs e)
    {
        PaginaAtual = 0;
        VincularItens();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BtnNovo_Click(object sender, EventArgs e)
    {
        url = new StringBuilder();
        parametros = new StringBuilder();

        parametros.AppendFormat("acao:{0}|", "insert");
        parametros.AppendFormat("codigo:{0}|", "-1");
        parametros.AppendFormat("pesquisa:{0}|", TxtPesquisa.Text);
        parametros.AppendFormat("current_page:{0}", PaginaAtual);

        url.AppendFormat("~/admin/edit-vacinas.aspx?vacina={0}", ImunneVacinas.Criptografia.Criptografar(parametros.ToString()));

        Response.Redirect(url.ToString(), false);
        Context.ApplicationInstance.CompleteRequest();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BtnExcluirItens_Click(object sender, EventArgs e)
    {
        ExcluirRegistro();
    }

    /// <summary>
    /// 
    /// </summary>
    private void ExcluirRegistro()
    {
        int codigoRegistro = Convert.ToInt32(HdfCodigo.Value);
        ImunneVacinas.Vacina registroSelecionado = ImunneVacinas.Vacina.ConsultarUnico(codigoRegistro);

        if (registroSelecionado == null)
            ImunneVacinas.Alerta.showMensagemErro(this.Page, "Oops!", "Nenhum registro foi localizado com o código informado!");
        else
        {
            try
            {
                int resultado = registroSelecionado.Excluir();
                if (resultado > 0)
                {
                    // Log descritivo
                    ImunneVacinas.Log logAcao = new ImunneVacinas.Log()
                    {
                        Tela = Path.GetFileName(Request.PhysicalPath),
                        TabelaAcao = "Vacinas",
                        TipoAcao = ImunneVacinas.Utils.AcaoLog.Exclusao,
                        Registro = registroSelecionado.Codigo,
                        Resumo = String.Format("EXCLUSÃO DE REGISTRO {0}.", registroSelecionado.Codigo),
                        DataAcao = DateTime.Now,
                        Campo = String.Empty,
                        ValorOriginal = String.Empty,
                        ValorNovo = String.Empty,
                    };

                    logAcao.IdUsuario.Codigo = this.Page.ConsultarSessionCodigoUsuario();
                    logAcao.Incluir();

                    ImunneVacinas.Alerta.showMensagemSucesso(this.Page, "Sucesso!", "Registro excluído com sucesso!");
                    HdfCodigo.Value = "";

                    VincularItens();
                }
                else ImunneVacinas.Alerta.showMensagemErro(this.Page, "Oops!", "Infelizmente não foi possível concluir a solicitação, tente novamente ou informe o suporte.");
            }
            catch (Exception ex)
            {
                this.SalvarException(ex);
                ImunneVacinas.Alerta.showMensagemErro(this.Page, "Oops!", "Infelizmente não foi possível concluir a solicitação, tente novamente ou informe o suporte.");
            }
        }
    }
}