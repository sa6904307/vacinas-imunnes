﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="boas-vindas.aspx.cs" Inherits="admin_boas_vindas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
        <script type="text/javascript">
        function validar() {

            // Destinatários
            if (document.getElementById("<%=TxtDestinatarios.ClientID%>").value == "") {
                MensagemGenerica("Aviso!", "Por favor, informe os e-mails que irão receber a mensagem de boas-vindas.", "warning");
                return false;
            }

            // Mensagem
            if (document.getElementById("<%=TxtConteudo.ClientID%>").value == "") {
                MensagemGenerica("Aviso!", "Por favor, informe o conteúdo da mensagem de boas vindas.", "warning");
                return false;
            }
        };
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <ul class="breadcrumb">
        <li><a href="dashboard.aspx"><i class="fa fa-briefcase"></i>&nbsp;Home</a> </li>
        <li>
            <asp:HyperLink ID="LnkPai" runat="server" Text="Clínicas"></asp:HyperLink>
        </li>
        <li class="active">Boas-Vindas</li>
    </ul>
    <h3><i class="fa fa-envelope"></i>&nbsp;E-mail de Boas-Vindas</h3>
    <asp:UpdatePanel ID="UpdMain" runat="server">
        <ContentTemplate>
            <div class="box box-success">
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                            <label>Destinatários (para inserir mais que um endereço, separe-os por ;) *</label>
                            <asp:TextBox ID="TxtDestinatarios" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                            <label>Textual *</label>
                            <asp:TextBox ID="TxtConteudo" runat="server" CssClass="form-control" TextMode="MultiLine" Height="250px"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
            <div class="fixed-button-box">
                <div class="text-right">
                    <asp:LinkButton ID="BtnEnviar" runat="server" CssClass="btn btn-success btn-lg"
                        data-toggle="tooltip" data-container="body" ToolTip="Enviar mensagem" OnClientClick="return validar();" OnClick="BtnEnviar_Click">
                    <i class="fa fa-envelope"></i>
                    </asp:LinkButton>
                    &nbsp;
                <asp:HyperLink ID="LnkVoltar" runat="server"
                    CssClass="btn btn-warning btn-lg pull-right" data-toggle="tooltip" data-container="body" ToolTip="Voltar à tela anterior" Visible="false"
                    Text="Cancelar" CausesValidation="false">
                    <i class="fa fa-chevron-left"></i>
                </asp:HyperLink>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="BtnEnviar" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
