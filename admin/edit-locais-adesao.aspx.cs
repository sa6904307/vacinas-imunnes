﻿using System;
using System.Collections.Specialized;
using System.IO;
using System.Text;

public partial class admin_edit_locais_adesao : System.Web.UI.Page
{
    private StringBuilder url = new StringBuilder();
    private string textoMensagem = String.Empty;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            IniciarPagina();
    }

    /// <summary>
    /// 
    /// </summary>
    private void IniciarPagina()
    {
        NameValueCollection campanha = new NameValueCollection();
        NameValueCollection local = new NameValueCollection();
        int codigoRegistro = -1;
        int codigoCampanha = -1;
        int paginaAtual = 0;

        string pesquisa = String.Empty;
        string comando = String.Empty;

        if (Request.QueryString["local"] != null)
        {
            campanha = ImunneVacinas.Utils.RecuperarQueryString(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["campanha"]));
            local = ImunneVacinas.Utils.RecuperarQueryString(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["local"]));
            pesquisa = local["pesquisa"];
            paginaAtual = Convert.ToInt32(local["current_page"]);

            url = new StringBuilder();
            url.AppendFormat("~/admin/campanhas.aspx?campanha={0}=", Request.QueryString["campanha"]);
            LnkCampanhas.NavigateUrl = url.ToString();

            url = new StringBuilder();
            url.AppendFormat("~/admin/locais-adesao.aspx?campanha={0}&local={1}", Request.QueryString["campanha"], Request.QueryString["local"]);
            LnkLocais.NavigateUrl = url.ToString();
            BtnCancelar.OnClientClick = "return CancelarFormulario(this, event,'" + url.Replace("~/admin/", "") + "');";

            codigoRegistro = Convert.ToInt32(local["codigo"]);
            codigoCampanha = Convert.ToInt32(local["campanha"]);
            comando = local["acao"].ToString();

            ImunneVacinas.Campanha campanhaSelecionada = ImunneVacinas.Campanha.ConsultarUnico(codigoCampanha);

            PopularCampos();

            LblForms.Text = ImunneVacinas.Utils.MontarTituloFormulario(comando);

            DdlEmpresas.DataSource = ImunneVacinas.Empresa.ConsultarComboBox(true, "SELECIONE");
            DdlEmpresas.DataBind();
            DdlEmpresas.SelectedValue = campanhaSelecionada.IdEmpresa.Codigo.ToString();

            DdlCampanhas.DataSource = ImunneVacinas.Campanha.ConsultarComboBox(false, "");
            DdlCampanhas.DataBind();
            DdlCampanhas.SelectedValue = codigoCampanha.ToString();

            switch (comando)
            {
                case "edit":
                    {
                        ConsultarDetalhes(codigoRegistro);
                    }
                    break;
            }

            Page.Title = "S&A Imunizações | " + LblForms.Text;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void PopularCampos()
    {
        TxtNome.MaxLength = (int)ImunneVacinas.LocalAdesao.GetInfoNome().Max_length;
        TxtEndereco.MaxLength = (int)ImunneVacinas.LocalAdesao.GetInfoEndereco().Max_length;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="codigo"></param>
    private void ConsultarDetalhes(int codigo)
    {
        try
        {
            ImunneVacinas.LocalAdesao registroSelecionado = ImunneVacinas.LocalAdesao.ConsultarUnico(codigo);

            DdlCampanhas.SelectedValue = registroSelecionado.Campanha.Codigo.ToString();

            TxtNome.Text = registroSelecionado.Nome;
            TxtEndereco.Text = registroSelecionado.Endereco;
        }
        catch (Exception ex)
        {
            this.SalvarException404(ex);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BtnSalvar_Click(object sender, EventArgs e)
    {
        try
        {
            NameValueCollection campanha = new NameValueCollection();
            NameValueCollection local = new NameValueCollection();

            campanha = ImunneVacinas.Utils.RecuperarQueryString(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["campanha"]));
            local = ImunneVacinas.Utils.RecuperarQueryString(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["local"]));

            int codigoRegistro = ImunneVacinas.Parser.parseInteiro(local["codigo"]);
            string comando = local["acao"].ToString();

            ImunneVacinas.LocalAdesao novoRegistro;
            ImunneVacinas.LocalAdesao registroOriginal = new ImunneVacinas.LocalAdesao();
            if (codigoRegistro > 0)
            {
                novoRegistro = ImunneVacinas.LocalAdesao.ConsultarUnico(codigoRegistro);
                registroOriginal = ImunneVacinas.LocalAdesao.ConsultarUnico(codigoRegistro);
            }
            else
                novoRegistro = new ImunneVacinas.LocalAdesao();

            novoRegistro.Campanha.Codigo = Convert.ToInt32(DdlCampanhas.SelectedValue);
            novoRegistro.Nome = TxtNome.Text;
            novoRegistro.Endereco = TxtEndereco.Text;

            int resultado = 0;

            switch (comando)
            {
                case "insert":
                    {
                        novoRegistro.DataCriacao = DateTime.Now;
                        resultado = novoRegistro.Incluir();
                    }
                    break;
                case "edit":
                    {
                        novoRegistro.Codigo = Convert.ToInt32(codigoRegistro);
                        novoRegistro.DataCriacao = DateTime.Now;
                        resultado = novoRegistro.Alterar();
                    }
                    break;
            }

            if (resultado <= 0)
            {
                textoMensagem = "Lamentamos mas ocorreu um erro e não foi possível concluir a operação. Tente novamente ou informe o suporte.";
                ImunneVacinas.Alerta.showMensagemErro(this.Page, null, textoMensagem);
                return;
            }

            #region Geração de Logs

            // Log descritivo
            ImunneVacinas.Log logAcao = new ImunneVacinas.Log();
            logAcao.Tela = Path.GetFileName(Request.PhysicalPath);
            logAcao.TabelaAcao = "Campanhas";

            switch (comando)
            {
                case "insert":
                    {
                        logAcao.TipoAcao = ImunneVacinas.Utils.AcaoLog.Insercao;
                        logAcao.Registro = resultado;
                        logAcao.Resumo = String.Format("INSERÇÃO DE NOVO REGISTRO {0}.", logAcao.Registro);
                        logAcao.DataAcao = DateTime.Now;
                        logAcao.Campo = String.Empty;
                        logAcao.ValorOriginal = String.Empty;
                        logAcao.ValorNovo = String.Empty;
                        logAcao.Incluir();
                    }
                    break;
                case "edit":
                    {
                        logAcao.TipoAcao = ImunneVacinas.Utils.AcaoLog.Edicao;
                    }
                    break;
            }

            #endregion

            Response.Redirect(LnkLocais.NavigateUrl, false);
            Context.ApplicationInstance.CompleteRequest();
        }
        catch (Exception ex)
        {
            textoMensagem = "Lamentamos mas ocorreu um erro e não foi possível concluir a operação. Tente novamente ou informe o suporte.";
            ImunneVacinas.Alerta.showMensagemErro(this.Page, null, textoMensagem);
            this.SalvarException(ex);
        }
    }
}