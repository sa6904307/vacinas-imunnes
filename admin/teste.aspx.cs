﻿using System;
using System.Net.Mail;
using System.Web;

public partial class teste : System.Web.UI.Page
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            IniciarPagina();
    }

    /// <summary>
    /// 
    /// </summary>
    private void IniciarPagina()
    {
        string hostMail = System.Configuration.ConfigurationManager.AppSettings.Get("HOSTEMAIL");
        string portaEmail = System.Configuration.ConfigurationManager.AppSettings.Get("PORTEMAIL");
        string userMail = System.Configuration.ConfigurationManager.AppSettings.Get("USEREMAIL");
        string passEmail = System.Configuration.ConfigurationManager.AppSettings.Get("PASSEMAIL");
        string replyEmail = System.Configuration.ConfigurationManager.AppSettings.Get("REPLYEMAIL");

        TxtHost.Text = hostMail;
        TxtPorta.Text = portaEmail;
        TxtSenha.Text = passEmail;
        TxtUser.Text = userMail;
    }

    private AspNetHostingPermissionLevel GetCurrentTrustLevel()
    {
        foreach (AspNetHostingPermissionLevel trustLevel in
                new AspNetHostingPermissionLevel[] {
                AspNetHostingPermissionLevel.Unrestricted,
                AspNetHostingPermissionLevel.High,
                AspNetHostingPermissionLevel.Medium,
                AspNetHostingPermissionLevel.Low,
                AspNetHostingPermissionLevel.Minimal
                })
        {
            try
            {
                new AspNetHostingPermission(trustLevel).Demand();
            }
            catch (System.Security.SecurityException)
            {
                continue;
            }

            return trustLevel;
        }

        return AspNetHostingPermissionLevel.None;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSend_Click(object sender, EventArgs e)
    {
        bool confirmacaoEnvio = false;

        // ImunneVacinas.Email.TesteEnviarEmail("gpetraca@gmail.com", "TESTE", "ENVIO");

        //string hostMail = System.Configuration.ConfigurationManager.AppSettings.Get("HOSTEMAIL");
        //string portaEmail = System.Configuration.ConfigurationManager.AppSettings.Get("PORTEMAIL");
        //string userMail = System.Configuration.ConfigurationManager.AppSettings.Get("USEREMAIL");
        //string passEmail = System.Configuration.ConfigurationManager.AppSettings.Get("PASSEMAIL");
        //string replyEmail = System.Configuration.ConfigurationManager.AppSettings.Get("REPLYEMAIL");

        ////ImunneVacinas.Email.TesteEnviarEmail("gpetraca@gmail.com", "TESTE ENVIO", "TESTE ENVIO");

        //using (MailMessage mail = new MailMessage())
        //{
        //    mail.From = new MailAddress(userMail, "TESTE");
        //    mail.To.Add("gpetraca@gmail.com");
        //    mail.Subject = "TESTE";
        //    mail.Body = "TESTE ENVIO";
        //    mail.IsBodyHtml = true;

        //    SmtpClient smtp = new SmtpClient(hostMail);

        //    smtp.Host = TxtHost.Text;
        //    smtp.Port = Convert.ToInt32(TxtPorta.Text);
        //    smtp.EnableSsl = false;

        //    smtp.Credentials = new System.Net.NetworkCredential(TxtUser.Text, TxtSenha.Text);

        //    ////smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
        //    //smtp.Credentials = new System.Net.NetworkCredential(userMail, passEmail);
        //    ////smtp.EnableSsl = true;

        //    smtp.Send(mail);
        //    ImunneVacinas.Alerta.showMensagemAviso(this.Page, "Ok", "Teste");
        //    //try
        //    //{

        //    //    Response.Write("TESTE OK");
        //    //else
        //    //    Response.Write("TESTE ERRADO");
        //    //}
        //    //catch (Exception ex)
        //    //{
        //    //    ImunneVacinas.Erro err = new ImunneVacinas.Erro();
        //    //    err.AdicionarLog(ex);
        //    //    Response.Write(ex.ToString().Replace("\r\n", "<br />"));
        //    //}
        //}
    }
}