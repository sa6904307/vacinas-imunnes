﻿using System;
using System.Text;

public partial class troca_senha : System.Web.UI.Page
{
    private StringBuilder url = new StringBuilder();
    private string textoMensagem = String.Empty;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            IniciarPagina();
    }

    /// <summary>
    /// 
    /// </summary>
    private void IniciarPagina()
    {
        Page.Title = "S&A Imunizações | Troca de Senha";

        PopularCampos();

        int codigoUsuario = Convert.ToInt32(this.Page.ConsultarSessionCodigoUsuario());

        ImunneVacinas.Usuario usuarioSelecionado = ImunneVacinas.Usuario.ConsultarUnico(codigoUsuario);
        url = new StringBuilder();
        url.Append("~/admin/dashboard.aspx");
        BtnCancelar.OnClientClick = "return CancelarFormulario(this, event,'" + url.Replace("~/", "") + "');";

        TxtLogin.Text = usuarioSelecionado.Login;
        TxtNome.Text = usuarioSelecionado.Nome;
        hdfSenhaAntiga.Value = usuarioSelecionado.Senha;
    }

    /// <summary>
    /// 
    /// </summary>
    private void PopularCampos()
    {
        int maxSenha = 15;

        TxtSenha.MaxLength = maxSenha;
        TxtConfirmar.MaxLength = maxSenha;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BtnSalvar_Click(object sender, EventArgs e)
    {
        try
        {
            int codigoUsuario = Convert.ToInt32(this.Page.ConsultarSessionCodigoUsuario());

            ImunneVacinas.Usuario novoUsuario = novoUsuario = ImunneVacinas.Usuario.ConsultarUnico(codigoUsuario);
            novoUsuario.Senha = ImunneVacinas.Criptografia.Criptografar(TxtSenha.Text);
            novoUsuario.Dica = TxtDica.Text;

            if (hdfSenhaAntiga.Value.ToUpper() == novoUsuario.Senha.ToUpper())
            {
                ImunneVacinas.Alerta.showMensagemAlerta(this.Page, "Aviso", "Sua nova senha deve ser diferente da senha antiga.");
                TxtSenha.Focus();
                return;
            }

            int resultado = 0;
            ImunneVacinas.Usuario dadosAtuais = ImunneVacinas.Usuario.ConsultarUnico(codigoUsuario);
            novoUsuario.Codigo = Convert.ToInt32(codigoUsuario);
            novoUsuario.DataAlteracao = DateTime.Now;
            resultado = novoUsuario.Alterar();

            Response.Redirect("~/admin/dashboard.aspx", false);
            Context.ApplicationInstance.CompleteRequest();
        }
        catch (Exception ex)
        {
            this.SalvarException(ex);
            textoMensagem = "Lamentamos mas não foi possível alterar sua senha. Tente novamente ou informe nosso suporte.";
            ImunneVacinas.Alerta.showMensagemErro(this.Page, null, textoMensagem);
        }
    }
}