﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="impressoes.aspx.cs" Inherits="admin_impressoes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        // Chamada de alerta para confirmação de exclusão
        function ConfirmarImpressao(ctl, event, id) {
            var defaultAction = $(ctl).prop("href");
            event.preventDefault();
            swal({
                title: "Deseja encaminhar os vales via e-mail?",
                text: 'Os vales que já foram utilizados terão a marcação de já utilizados. A conferência de tais informações pela clínica é sempre necessária.',
                type: 'question',
                showCancelButton: true,
                confirmButtonText: "<i class='fa fa-check'></i>&nbsp;Sim!",
                cancelButtonText: "<i class='fa fa-times'></i>&nbsp;Não!",
                confirmButtonColor: '#00a65a',
                cancelButtonColor: '#dd4b39',
                showLoaderOnConfirm: true,
                animation: 'slide-from-top',
                allowEscapeKey: false,
                allowOutsideClick: false
            }).then(result => {
                if (result.value) {
                    document.getElementById("<%=BtnEnviarTodos.ClientID%>").click();
                    return true;
                }
            })
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row">
        <div class="col-md-12">
            <ul class="breadcrumb">
                <li><a href="dashboard.aspx"><i class="fa fa-home"></i>&nbsp;Home</a></li>
                <li>
                    <asp:HyperLink ID="LnkPai" runat="server" Text="Compras"></asp:HyperLink>
                </li>
                <li class="active">Impressões de Vales</li>
            </ul>
            <h1 class="conteudo-centro"><i class="fa fa-print"></i>&nbsp;Impressões de Vales</h1>
        </div>
    </div>
    <div class="row">
        <asp:Panel ID="PnlPesquisa" runat="server">
            <div class="col-md-12">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-sliders"></i>&nbsp;Dados da compra</h3>
                    </div>
                    <div class="box-body">
                        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 form-group">
                            <label>Pedido</label>
                            <asp:TextBox ID="TxtPedido" runat="server" CssClass="form-control text-center" Enabled="false"></asp:TextBox>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 form-group">
                            <label>Data</label>
                            <asp:TextBox ID="TxtData" runat="server" CssClass="form-control text-center" Enabled="false"></asp:TextBox>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 form-group">
                            <label>Adquirente</label>
                            <asp:TextBox ID="TxtNome" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 form-group">
                            <label>Campanha</label>
                            <asp:TextBox ID="TxtCampanha" runat="server" CssClass="form-control text-center" Enabled="false"></asp:TextBox>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10 form-group">
                            <label>Empresa</label>
                            <asp:TextBox ID="TxtEmpresa" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                            <label>Clínica</label>
                            <asp:TextBox ID="TxtClinica" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                        </div>
                    </div>
                    <div class="box-footer" style="display: block;">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 pull-right">
                                <asp:LinkButton ID="BtnImprimirTodos" runat="server" CssClass="btn btn-block btn-success" data-toggle="tooltip" data-container="body" ToolTip="Enviar todos os vales ao adquirente" CausesValidation="false" OnClientClick="return ConfirmarImpressao(this, event)">
                                        <i class="fa fa-print"></i>&nbsp;Enviar Todos
                                </asp:LinkButton>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-success">
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 text-left bloco-legenda">
                            <a href="#" class="dropdown-toggle btn btn-block btn-primary" data-toggle="dropdown">
                                <i class="fa fa-info-circle"></i>&nbsp;Legenda
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <div style="padding: 8px;">
                                        <i class="fa fa-envelope text-regular"></i>&nbsp;Enviar vale
                                    </div>
                                </li>
                                <li>
                                    <div style="padding: 8px;">
                                        <i class="fa fa-print text-regular"></i>&nbsp;Imprimir vale
                                    </div>
                                </li>
                            </ul>
                            <div class="dv-spc-10"></div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10 box-resultados text-right conteudo-centro">
                            <asp:Label ID="LblRegistros" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="dv-spc-10"></div>
                    <div class="box-grids">
                        <div class="dv-grid-f-header">
                            <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="display: inline-block; float: none;">
                                AÇÕES
                            </div>
                            <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="display: inline-block; float: none;">
                                UTILIZADO
                            </div>
                            <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="display: inline-block; float: none;">
                                CAMPANHA
                            </div>
                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="display: inline-block; float: none;">
                                BENEFICIÁRIO
                            </div>
                            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" style="display: inline-block; float: none;">
                                PARENTESCO
                            </div>
                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="display: inline-block; float: none;">
                                ADQUIRENTE
                            </div>
                        </div>
                        <div class="dv-grid-f">
                            <asp:Repeater ID="RptPrincipal" runat="server" OnItemDataBound="RptPrincipal_ItemDataBound" OnItemCommand="RptPrincipal_ItemCommand">
                                <ItemTemplate>
                                    <div id="dvItem" runat="server">
                                        <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="display: inline-block; float: none;">
                                            <div class="truncate">
                                                <asp:LinkButton ID="LnkEnviarList" runat="server" CommandName="send" data-toggle="tooltip" data-container="body" ToolTip="Enviar vale via e-mail">
                                                        <i class="fa fa-envelope text-regular"></i>
                                                </asp:LinkButton>
                                                <asp:HyperLink ID="LnkImprimirList" runat="server" Target="_blank">
                                                        <i class="fa fa-print text-regular"></i>
                                                </asp:HyperLink>
                                            </div>
                                        </div>
                                        <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="display: inline-block; float: none;">
                                            <div class="truncate">
                                                <asp:HiddenField ID="HdfCodigo" runat="server" />
                                                <asp:Label ID="LblUtilizado" runat="server" CssClass="text-uppercase"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="display: inline-block; float: none;">
                                            <div class="truncate">
                                                <asp:Label ID="LblCampanha" runat="server" CssClass="text-uppercase"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="display: inline-block; float: none;">
                                            <div class="truncate">
                                                <asp:Label ID="LblBeneficiario" runat="server" CssClass="text-uppercase"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" style="display: inline-block; float: none;">
                                            <div class="truncate">
                                                <asp:Label ID="LblParentesco" runat="server" CssClass="text-uppercase"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="display: inline-block; float: none;">
                                            <div class="truncate">
                                                <asp:Label ID="LblNome" runat="server" CssClass="text-uppercase"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>
                <div class="dv-spc-10">
                </div>
                <div class="box-footer">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                            <asp:Label ID="LblInfo" runat="server"></asp:Label>
                            <br />
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                            <label>Página selecionada</label>&nbsp;
                                    <asp:DropDownList ID="DdlPaginas" runat="server" CssClass="form-control max-drop"
                                        AutoPostBack="true" OnSelectedIndexChanged="DdlPaginas_SelectedIndexChanged" data-toggle="tooltip" data-container="body" ToolTip="Escolher página para exibição">
                                    </asp:DropDownList>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:LinkButton ID="BtnEnviarTodos" runat="server" CssClass="hiddden" OnClick="BtnEnviarTodos_Click"></asp:LinkButton>
</asp:Content>
