﻿using System;
using System.Collections.Specialized;
using System.IO;
using System.Text;
using System.Web.UI;

public partial class edit_usuarios : System.Web.UI.Page
{
    private StringBuilder url = new StringBuilder();
    private string textoMensagem = String.Empty;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            IniciarPagina();
    }

    /// <summary>
    /// 
    /// </summary>
    private void IniciarPagina()
    {
        NameValueCollection usuario = new NameValueCollection();
        int roleUsuario = Page.ConsultarSessionTipoUsuario();
        int codigoRegistro = -1;
        int codigoEmpresa = -1;
        string comando = String.Empty;

        if (Request.QueryString["usuario"] != null)
        {
            usuario = ImunneVacinas.Utils.RecuperarQueryString(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["usuario"]));

            url = new StringBuilder();
            url.AppendFormat("~/admin/usuarios.aspx?usuario={0}", Request.QueryString["usuario"]);
            LnkPai.NavigateUrl = url.ToString();
            BtnCancelar.OnClientClick = "return CancelarFormulario(this, event,'" + url.Replace("~/admin/", "") + "');";

            codigoEmpresa = Convert.ToInt32(usuario["cliente"]);
            codigoRegistro = Convert.ToInt32(usuario["codigo"]);
            comando = usuario["acao"].ToString();

            ImunneVacinas.PesquisaTipoUsuario pesquisaTipo = new ImunneVacinas.PesquisaTipoUsuario
            {
                Ordenacao = ImunneVacinas.Utils.TipoOrdenacao.porNome
            };

            DdlTipos.DataSource = ImunneVacinas.TipoUsuario.ConsultarComboBox(pesquisaTipo, true, "SELECIONE");
            DdlTipos.DataBind();

            DdlClinicas.DataSource = ImunneVacinas.Clinica.ConsultarComboBox(true, "SELECIONE");
            DdlClinicas.DataBind();

            PopularCampos();

            if (roleUsuario > 5)
                DdlTipos.Items.Remove(DdlTipos.Items.FindByText("ADMINISTRADOR"));

            switch (comando)
            {
                case "insert":
                    {
                        LblForms.Text = "Inserindo novo usuário";
                        HdfAcao.Value = comando;
                    }
                    break;
                case "edit":
                    {
                        LblForms.Text = "Alterando dados do usuário";
                        ConsultarDetalhes(codigoRegistro);
                    }
                    break;
            }

            Page.Title = "S&A Imunizações | " + LblForms.Text;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="codigo"></param>
    private void ConsultarDetalhes(int codigo)
    {
        try
        {
            ImunneVacinas.Usuario registroSelecionado = ImunneVacinas.Usuario.ConsultarUnico(codigo);

            TxtNome.Text = registroSelecionado.Nome;
            DdlTipos.SelectedValue = registroSelecionado.Tipo.Codigo.ToString();
            TxtEmail.Text = registroSelecionado.Email;
            TxtLogin.Text = registroSelecionado.Login;
            TxtConfirmar.Text = registroSelecionado.Senha;
            TxtDica.Text = registroSelecionado.Dica;
            TxtSenha.Text = registroSelecionado.Senha;
            DdlClinicas.SelectedValue = registroSelecionado.Clinica.ToString();

            if (registroSelecionado.Clinica > 0)
                PnlGrupoClinicas.Visible = true;
        }
        catch (Exception ex)
        {
            this.SalvarException404(ex);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void PopularCampos()
    {
        int maxSenha = 15;

        TxtNome.MaxLength = (int)ImunneVacinas.Usuario.GetInfoNome().Max_length;
        TxtEmail.MaxLength = (int)ImunneVacinas.Usuario.GetInfoEmail().Max_length;
        TxtLogin.MaxLength = (int)ImunneVacinas.Usuario.GetInfoLogin().Max_length;
        TxtDica.MaxLength = (int)ImunneVacinas.Usuario.GetInfoDica().Max_length;
        TxtSenha.MaxLength = maxSenha;
        TxtConfirmar.MaxLength = maxSenha;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BtnSalvar_Click(object sender, EventArgs e)
    {
        TxtLogin.Attributes["value"] = TxtLogin.Text;
        TxtConfirmar.Attributes["value"] = TxtConfirmar.Text;

        try
        {
            NameValueCollection usuario = new NameValueCollection();

            usuario = ImunneVacinas.Utils.RecuperarQueryString(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["usuario"]));

            int codigoRegistro = ImunneVacinas.Parser.parseInteiro(usuario["codigo"]);
            string comando = usuario["acao"].ToString();

            ImunneVacinas.Usuario novoRegistro;
            ImunneVacinas.Usuario registroOriginal = new ImunneVacinas.Usuario();
            if (codigoRegistro > 0)
            {
                novoRegistro = ImunneVacinas.Usuario.ConsultarUnico(codigoRegistro);
                registroOriginal = ImunneVacinas.Usuario.ConsultarUnico(codigoRegistro);
            }
            else
                novoRegistro = new ImunneVacinas.Usuario();

            novoRegistro.Nome = TxtNome.Text;
            novoRegistro.Tipo.Codigo = Convert.ToInt32(DdlTipos.SelectedValue);
            novoRegistro.Email = TxtEmail.Text.ToLower();
            novoRegistro.Login = TxtLogin.Text.ToLower();
            novoRegistro.Senha = ImunneVacinas.Criptografia.Criptografar(TxtSenha.Text);
            novoRegistro.Dica = TxtDica.Text;
            novoRegistro.Clinica = Convert.ToInt32(DdlClinicas.SelectedValue);

            if (VerificarLogin(comando, codigoRegistro, TxtLogin.Text) == false)
            {
                textoMensagem = String.Format("Verifique seu cadastro pois já existe um usuário com este login {0}. Sugerimos o uso de um e-mail como login.", TxtLogin.Text);
                ImunneVacinas.Alerta.showMensagemErro(this.Page, null, textoMensagem);
                return;
            }

            int resultado = 0;

            switch (comando)
            {
                case "insert":
                    {
                        novoRegistro.Situacao = ImunneVacinas.Utils.SituacaoRegistro.Ativo;
                        novoRegistro.DataCriacao = DateTime.Now;
                        resultado = novoRegistro.Incluir();
                    }
                    break;
                case "edit":
                    {
                        ImunneVacinas.Usuario dadosAtuais = ImunneVacinas.Usuario.ConsultarUnico(codigoRegistro);
                        novoRegistro.Codigo = Convert.ToInt32(codigoRegistro);
                        novoRegistro.DataAlteracao = DateTime.Now;

                        //Por questão de segurança, o asp.net não exibe o conteúdo de campos password. 
                        //Então, ao popular o nosso formulário, os campos TxtSenha e TxtConfirma
                        //ficam vazios. Caso isso ocorra, buscamos no BD a senha do usuário, 
                        //já que, no Update, esse dado SEMPRE é atualizado.
                        if (TxtSenha.Text.Trim() == "" && TxtConfirmar.Text.Trim() == "")
                        {
                            novoRegistro.Senha = dadosAtuais.Senha;
                        }
                        else
                            novoRegistro.Senha = ImunneVacinas.Criptografia.Criptografar(TxtSenha.Text);

                        resultado = novoRegistro.Alterar();
                    }
                    break;
            }

            if (resultado <= 0)
            {
                textoMensagem = "Lamentamos mas ocorreu um erro e não foi possível concluir a operação. Tente novamente ou informe o suporte.";
                ImunneVacinas.Alerta.showMensagemErro(this.Page, null, textoMensagem);
                return;
            }

            #region Geração de Logs

            // Log descritivo
            ImunneVacinas.Log logAcao = new ImunneVacinas.Log();
            logAcao.Tela = Path.GetFileName(Request.PhysicalPath);
            logAcao.IdUsuario.Codigo = this.Page.ConsultarSessionCodigoUsuario();
            logAcao.TabelaAcao = "Usuarios";

            switch (comando)
            {
                case "insert":
                    {
                        logAcao.TipoAcao = ImunneVacinas.Utils.AcaoLog.Insercao;
                        logAcao.Registro = resultado;
                        logAcao.Resumo = String.Format("INSERÇÃO DE NOVO REGISTRO {0}.", logAcao.Registro);
                        logAcao.DataAcao = DateTime.Now;
                        logAcao.Campo = String.Empty;
                        logAcao.ValorOriginal = String.Empty;
                        logAcao.ValorNovo = String.Empty;
                        logAcao.Incluir();
                    }
                    break;
                case "edit":
                    {
                        logAcao.TipoAcao = ImunneVacinas.Utils.AcaoLog.Edicao;
                        //ImunneVacinas.RegistraLog.CompararUsuario(logAcao, registroOriginal, novoRegistro);
                    }
                    break;
            }

            #endregion

            url = new StringBuilder();
            url.AppendFormat("~/admin/usuarios.aspx?usuario={0}", Request.QueryString["usuario"]);
            Response.Redirect(url.ToString(), false);
            Context.ApplicationInstance.CompleteRequest();
        }
        catch (Exception ex)
        {
            textoMensagem = "Lamentamos mas ocorreu um erro e não foi possível concluir a operação. Tente novamente ou informe o suporte.";
            ImunneVacinas.Alerta.showMensagemErro(this.Page, null, textoMensagem);
            this.SalvarException(ex);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void DdlTipos_SelectedIndexChanged(object sender, EventArgs e)
    {
        int codigoTipo = Convert.ToInt32(DdlTipos.SelectedValue);

        if (codigoTipo == 7)
            PnlGrupoClinicas.Visible = true;
        else
            PnlGrupoClinicas.Visible = false;
    }

    /// <summary>
    /// Rotina que verifica se já existe um tipo de registro com os dados a serem cadastrados
    /// </summary>
    /// <param name="comando">VALUE: Comando que está sendo realizado pelo formulário</param>
    /// <param name="codigoRegistro">VALUE: Código do registro que está sendo inserido</param>
    /// <param name="nomeRegistro">VALUE: Nome do registro a ser inserido</param>
    /// <returns></returns>
    private bool VerificarLogin(string comando, int codigoRegistro, string loginRegistro)
    {
        bool retorno = false;

        ImunneVacinas.Usuario registroVerificado = ImunneVacinas.Usuario.ConsultarUnicoLogin(loginRegistro);

        switch (comando)
        {
            case "insert":
                {
                    if (registroVerificado == null)
                        retorno = true;
                    else
                        retorno = false;
                }
                break;
            case "edit":
                {
                    if (registroVerificado == null)
                        retorno = true;
                    else
                    {
                        if (registroVerificado.Codigo == codigoRegistro)
                            retorno = true;
                        else
                            retorno = false;
                    }
                }
                break;
        }

        return retorno;
    }
}