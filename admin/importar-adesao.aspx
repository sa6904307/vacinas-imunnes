﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="importar-adesao.aspx.cs" Inherits="admin_importar_adesao" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js" type="text/javascript"></script>
    <link href="../vendors/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <script src="../vendors/global/jquery.min.js"></script>
    <script type="text/javascript" src="../vendors/select2/js/select2.full.min.js"></script>
    <script type="text/javascript">
        var select$ = jQuery.noConflict();
        select$(function () {
            //Initialize Select2 Elements
            select$('.select2').select2()
        })
    </script>
    <script type="text/javascript">
        function validar() {
            if (document.getElementById('<%=UplArquivo.ClientID%>').value == "") {
                MensagemGenerica("Aviso!", "Por favor, informe o arquivo a ser enviado.", "warning");
                return false;
            }
        };
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row">
        <div class="col-md-12">
            <ul class="breadcrumb">
                <li><a href="dashboard.aspx"><i class="fa fa-home"></i>&nbsp;Home</a></li>
                <li>
                    <asp:HyperLink ID="LnkCampanhas" runat="server" Text="Campanhas"></asp:HyperLink>
                </li>
                <li class="active">Processar Adesões</li>
            </ul>
            <h1 class="conteudo-centro"><i class="fa fa-upload"></i>&nbsp;Processar Adesões</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                            <label>Modelo para preenchimento</label><br />
                            <a href="../modelos/modelo_planilha.xlsx" class="btn btn-success" target="_blank" download><i class="fa fa-download"></i>&nbsp;XLS</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Filtros</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" data-container="body" title="Ocultar/exibir filtros">
                            <i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body" style="display: block;">
                    <div class="row">
                        <asp:UpdatePanel ID="UpdFiltros" runat="server">
                            <ContentTemplate>
                                <script type="text/javascript">
                                    Sys.Application.add_load(SetSelectBox);
                                    var select$ = jQuery.noConflict();
                                    function SetSelectBox() {
                                        select$('.select2').select2();
                                    }
                                </script>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                    <asp:DropDownList ID="DdlCampanhas" runat="server" CssClass="form-control text-uppercase select2" Enabled="false" DataValueField="ValueMember" DataTextField="DisplayMember"></asp:DropDownList>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="BtnExportarResumo" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 form-group">
                            <label>
                                Arquivo:
                            </label>
                            <asp:FileUpload ID="UplArquivo" runat="server" CssClass="btn btn-block btn-success" accept=".xls, .xlsx" />
                        </div>
                    </div>
                </div>
                <div class="box-footer" style="display: block;">
                    <div class="row">
                        <div class="col-lg-12 text-right">
                            <asp:LinkButton ID="LnkUpload" runat="server" CssClass="btn btn-success" data-toggle="tooltip" data-container="body" ToolTip="Processar arquivo" CausesValidation="false" OnClientClick="return validar()" OnClick="LnkUpload_Click">
                                        <i class="fa fa-upload"></i>&nbsp;Processar arquivo
                            </asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:Panel ID="PnlResumoImportacoes" runat="server" Visible="false">
        <asp:UpdatePanel ID="UpdResumoInformacoes" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">Resultados</h3>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" data-container="body" title="Ocultar/exibir filtros">
                                        <i class="fa fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="box-body" style="display: block;">
                                <div class="box-grids">
                                    <div class="dv-grid-f-header">
                                        <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="display: inline-block; float: none;">
                                            LINHA
                                        </div>
                                        <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center hidden" style="display: inline-block; float: none;">
                                            TIPO
                                        </div>
                                        <div class="col-xs-11 col-sm-11 col-md-11 col-lg-11" style="display: inline-block; float: none;">
                                            RESUMO
                                        </div>
                                    </div>
                                    <div class="dv-grid-f" style="overflow-x: hidden; overflow-y: auto;">
                                        <asp:Repeater ID="RptResumoImportacoes" runat="server" OnItemDataBound="RptResumoImportacoes_ItemDataBound">
                                            <ItemTemplate>
                                                <div id="dvItem" runat="server">
                                                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="display: inline-block; float: none;">
                                                        <div class="truncate">
                                                            <asp:Label ID="LblLinha" runat="server" CssClass="text-uppercase" Font-Bold="true"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center hidden" style="display: inline-block; float: none;">
                                                        <div class="truncate">
                                                            <asp:Label ID="LblClassificacao" runat="server" CssClass="text-uppercase"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-11 col-sm-11 col-md-11 col-lg-11" style="display: inline-block; float: none;">
                                                        <div class="truncate">
                                                            <asp:Label ID="LblResumo" runat="server" CssClass="text-uppercase"></asp:Label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="BtnExportarResumo" />
            </Triggers>
        </asp:UpdatePanel>
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-footer">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 pull-right">
                                <asp:LinkButton ID="BtnExportarResumo" runat="server" CssClass="btn btn-block btn-success" data-toggle="tooltip" ToolTip="Exportar resultado da carga" CausesValidation="false" OnClick="BtnExportarResumo_Click">
                                        <i class="fa fa-download"></i>&nbsp;Exportar
                                </asp:LinkButton>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
</asp:Content>
