﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class localidades : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = "S&A Imunizações | Localidades";
    }

    #region Botões e Ações

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BtnPesquisar_Click(object sender, EventArgs e)
    {
        if (TxtPesquisa.Text.ToUpper() != "MAT123456")
        {
            ImunneVacinas.Alerta.showMensagemAviso(this.Page, "Aviso", "Não foi encontrada nenhum localidade para esta matrícula!");

            LblRegistros.Text = "";
            LblInfo.Text = "";
            RptPrincipal.DataSource = null;
            RptPrincipal.DataBind();

            return;
        }

        DataTable dtLocalidades = new DataTable();
        dtLocalidades.Columns.Add("Nome");
        dtLocalidades.Columns.Add("Localidade");

        DataRow linha;

        for (int i = 1; i <= 8; i++)
        {
            linha = dtLocalidades.NewRow();
            linha[0] = "LOCALIDADE " + i.ToString().PadLeft(2, '0');
            linha[1] = "RUA PERIMETRAL " + i.ToString().PadLeft(2, '0');

            dtLocalidades.Rows.Add(linha);
        }

        RptPrincipal.DataSource = dtLocalidades;
        RptPrincipal.DataBind();

        DdlPaginas.Items.Add(new ListItem("1", "1"));

        LblRegistros.Text = "Foram encontrados " + dtLocalidades.Rows.Count + " registros";
        LblInfo.Text = "Exibindo página 1 de 1";

        //PaginaAtual = 0;
        //VincularItens();
    }

    #endregion
}