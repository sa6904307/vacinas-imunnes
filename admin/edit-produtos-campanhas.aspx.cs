﻿using System;
using System.Collections.Specialized;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class edit_produtos_campanhas : System.Web.UI.Page
{
    private StringBuilder url = new StringBuilder();
    private string textoMensagem = String.Empty;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            IniciarPagina();
    }

    /// <summary>
    /// 
    /// </summary>
    private void IniciarPagina()
    {
        NameValueCollection campanha = new NameValueCollection();
        NameValueCollection produto = new NameValueCollection();

        int codigoRegistro = -1;
        int codigoCampanha = -1;
        string comando = String.Empty;

        if (Request.QueryString["produto"] != null)
        {
            produto = ImunneVacinas.Utils.RecuperarQueryString(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["produto"]));
            campanha = ImunneVacinas.Utils.RecuperarQueryString(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["campanha"]));

            codigoCampanha = Convert.ToInt32(campanha["codigo"]);

            url = new StringBuilder();
            url.AppendFormat("~/admin/produtos-campanhas.aspx?campanha={0}&produto={1}", Request.QueryString["campanha"], Request.QueryString["produto"]);
            LnkProdutos.NavigateUrl = url.ToString();
            BtnCancelar.OnClientClick = "return CancelarFormulario(this, event,'" + url.Replace("~/admin/", "") + "');";

            url = new StringBuilder();
            url.AppendFormat("~/admin/campanhas.aspx?campanha={0}", Request.QueryString["campanha"]);
            LnkCampanhas.NavigateUrl = url.ToString();

            DdlCampanhas.DataSource = ImunneVacinas.Campanha.ConsultarComboBox(true, "SELECIONE");
            DdlCampanhas.DataBind();
            DdlCampanhas.Enabled = false;
            DdlCampanhas.SelectedValue = codigoCampanha.ToString();

            codigoRegistro = Convert.ToInt32(produto["codigo"]);
            comando = produto["acao"].ToString();

            LblForms.Text = ImunneVacinas.Utils.MontarTituloFormulario(comando);

            Page.Title = "S&A Imunizações | " + LblForms.Text;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BtnPesquisar_Click(object sender, EventArgs e)
    {
        ConsultarProdutos();
    }

    /// <summary>
    /// 
    /// </summary>
    private void ConsultarProdutos()
    {
        ImunneVacinas.PesquisaVacina pesquia = new ImunneVacinas.PesquisaVacina()
        {
            CampoLivre = TxtPesquisa.Text,
            Ordenacao = ImunneVacinas.Utils.TipoOrdenacao.porNome
        };

        RptPrincipal.DataSource = ImunneVacinas.Vacina.ConsultarVacinas(pesquia);
        RptPrincipal.DataBind();

        LblRegistros.Text = ImunneVacinas.Utils.MontarLinhaTotalizadora(RptPrincipal.Items.Count);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void RptPrincipal_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        ImunneVacinas.Vacina vacinaSelecionada = (ImunneVacinas.Vacina)e.Item.DataItem;

        CheckBox ChkSelecionar = (CheckBox)e.Item.FindControl("ChkSelecionar");
        TextBox TxtPreco = (TextBox)e.Item.FindControl("TxtPreco");
        Label LblNome = (Label)e.Item.FindControl("LblNome");
        HiddenField HdfCodigo = (HiddenField)e.Item.FindControl("HdfCodigo");

        LblNome.Text = vacinaSelecionada.Nome;
        HdfCodigo.Value = vacinaSelecionada.Codigo.ToString();

        ImunneVacinas.ProdutoCampanha campanhaProduto = ImunneVacinas.ProdutoCampanha.ConsultarUnico(Convert.ToInt32(DdlCampanhas.SelectedValue), vacinaSelecionada.Codigo);
        if (campanhaProduto != null)
        {
            TxtPreco.Text = campanhaProduto.Preco.ToString("N2");
            ChkSelecionar.Checked = true;
            ChkSelecionar.Enabled = false;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BtnSalvar_Click(object sender, EventArgs e)
    {
        try
        {
            int codigoCampanha = Convert.ToInt32(DdlCampanhas.SelectedValue);
            int contadorZeros = 0;

            foreach (RepeaterItem item in RptPrincipal.Items)
            {
                CheckBox ChkSelecionar = (CheckBox)item.FindControl("ChkSelecionar");
                TextBox TxtPreco = (TextBox)item.FindControl("TxtPreco");

                if (Convert.ToDecimal(TxtPreco.Text) <= 0 && ChkSelecionar.Checked)
                    contadorZeros += 1;
            }

            if (contadorZeros > 0)
            {
                ImunneVacinas.Alerta.showMensagemAlerta(this.Page, "Aviso", "Nenhum produto que integra a campanha pode ter valor menor ou igual à zero.");
                return;
            }

            foreach (RepeaterItem item in RptPrincipal.Items)
            {
                CheckBox ChkSelecionar = (CheckBox)item.FindControl("ChkSelecionar");
                HiddenField HdfCodigo = (HiddenField)item.FindControl("HdfCodigo");
                TextBox TxtPreco = (TextBox)item.FindControl("TxtPreco");

                if (ChkSelecionar.Checked)
                {
                    if (ChkSelecionar.Enabled == true && !String.IsNullOrEmpty(TxtPreco.Text))
                    {
                        ImunneVacinas.ProdutoCampanha novoRegistro = new ImunneVacinas.ProdutoCampanha();
                        novoRegistro.IdCampanha.Codigo = codigoCampanha;
                        novoRegistro.Preco = Convert.ToDecimal(TxtPreco.Text);
                        novoRegistro.IdVacina.Codigo = Convert.ToInt32(HdfCodigo.Value);
                        novoRegistro.DataCriacao = DateTime.Now;
                        novoRegistro.Incluir();
                    }
                }
            }

            Response.Redirect(LnkProdutos.NavigateUrl, false);
            Context.ApplicationInstance.CompleteRequest();
        }
        catch (Exception ex)
        {
            textoMensagem = "Lamentamos mas ocorreu um erro e não foi possível concluir a operação. Tente novamente ou informe o suporte.";
            ImunneVacinas.Alerta.showMensagemErro(this.Page, null, textoMensagem);
            this.SalvarException(ex);
        }
    }
}