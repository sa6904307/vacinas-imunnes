﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;

public partial class admin_gerar_adesao : System.Web.UI.Page
{
    private StringBuilder url = new StringBuilder();
    private string textoMensagem = String.Empty;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = "S&A Imunizações | Gerar lista de adesões";

        if (!IsPostBack)
            IniciarPagina();
    }

    /// <summary>
    /// 
    /// </summary>
    private void IniciarPagina()
    {
        NameValueCollection campanha = new NameValueCollection();
        campanha = ImunneVacinas.Utils.RecuperarQueryString(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["campanha"]));
        int codigoRegistro = -1;
        int codigoEmpresa = -1;

        try
        {
            if (!String.IsNullOrEmpty(Request.QueryString["campanha"]))
            {
                url = new StringBuilder();
                url.AppendFormat("~/admin/campanhas.aspx?campanha={0}", Request.QueryString["campanha"]);
                LnkPai.NavigateUrl = url.ToString();
                BtnCancelar.OnClientClick = "return CancelarFormulario(this, event,'" + url.Replace("~/admin/", "") + "');";

                codigoRegistro = Convert.ToInt32(campanha["codigo"]);
                codigoEmpresa = Convert.ToInt32(campanha["empresa"]);

                DdlCampanhas.DataSource = ImunneVacinas.Campanha.ConsultarComboBox(false, "");
                DdlCampanhas.DataBind();
                DdlCampanhas.SelectedValue = codigoRegistro.ToString();

                DdlEmpresas.DataSource = ImunneVacinas.Empresa.ConsultarComboBox(false, "");
                DdlEmpresas.DataBind();
                DdlEmpresas.SelectedValue = codigoEmpresa.ToString();
            }
        }
        catch (Exception ex)
        {
            this.SalvarException404(ex);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BtnSalvar_Click(object sender, EventArgs e)
    {
        try
        {
            NameValueCollection campanha = new NameValueCollection();
            campanha = ImunneVacinas.Utils.RecuperarQueryString(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["campanha"]));

            int codigoRegistro = ImunneVacinas.Parser.parseInteiro(campanha["codigo"]);
            string comando = campanha["acao"].ToString();
            //string urlAdesao = ImunneVacinas.Utils.EncurtadorUrl(String.Format("http://localhost:63119/adesoes/termos.aspx?campanha={0}", ImunneVacinas.Criptografia.Criptografar(codigoRegistro.ToString())));
            string urlAdesao = ImunneVacinas.Utils.EncurtadorUrl(String.Format("https://vacinas.imunne.com.br/adesoes/termos.aspx?campanha={0}", ImunneVacinas.Criptografia.Criptografar(codigoRegistro.ToString())));

            // GERAÇÃO DO LINK DE ADESÕES
            ImunneVacinas.Campanha campanhaSelecionada = ImunneVacinas.Campanha.ConsultarUnico(codigoRegistro);
            campanhaSelecionada.DataAlteracao = DateTime.Now;
            campanhaSelecionada.LinkAdesao = urlAdesao;
            campanhaSelecionada.Alterar();

            // GERANDO OS LOCAIS DE APLICAÇÕES
            // AS OPÇÕES SÃO DE UTILIZAR UM CAMPO LIVRE PARA INFORMAÇÃO DOS MESMOS OU,
            // UTILIZAR OS ENDEREÇOS DAS CLÍNICAS ASSOCIADAS A CAMPANHA
            if (ChkListagem.Checked)
            {
                string[] arrayLocais = TxtEnderecos.Text.Split(';');

                foreach (string item in arrayLocais)
                {
                    string[] arrayItem = item.Split('|');
                    if (arrayItem.Length == 2)
                    {
                        ImunneVacinas.LocalAdesao novoLocal = new ImunneVacinas.LocalAdesao();
                        novoLocal.Campanha.Codigo = codigoRegistro;
                        novoLocal.Nome = arrayItem[0].ToUpper();
                        novoLocal.Endereco = arrayItem[1].ToUpper();
                        novoLocal.DataCriacao = DateTime.Now;
                        novoLocal.Incluir();
                    }
                }
            }
            else
            {
                ImunneVacinas.PesquisaCampanhaClinica pesquisa = new ImunneVacinas.PesquisaCampanhaClinica()
                {
                    Campanha = codigoRegistro,
                    Ordenacao = ImunneVacinas.Utils.TipoOrdenacao.porNome
                };

                List<ImunneVacinas.CampanhaClinica> listaClinicas = ImunneVacinas.CampanhaClinica.ConsultarCampanhasClinicas(pesquisa);
                if (listaClinicas == null || listaClinicas.Count <= 0)
                {
                    ImunneVacinas.Alerta.showMensagemAviso(this.Page, "Aviso!", "Esta campanha não possui nenhuma clínica associada. Utilize o campo livre para informar os locais.");
                    return;
                }

                foreach (ImunneVacinas.CampanhaClinica item in listaClinicas)
                {
                    ImunneVacinas.LocalAdesao novoLocal = new ImunneVacinas.LocalAdesao();
                    novoLocal.Campanha.Codigo = codigoRegistro;
                    novoLocal.Nome = item.IdClinica.NomeFantasia;
                    novoLocal.Endereco = String.Format("{0}, {1} - {2}/{3}", item.IdClinica.Endereco, item.IdClinica.Numero, item.IdClinica.IdCidade.Nome, item.IdClinica.IdUf.Sigla);
                    novoLocal.DataCriacao = DateTime.Now;
                    novoLocal.Incluir();
                }
            }

            Response.Redirect(LnkPai.NavigateUrl, false);
            Context.ApplicationInstance.CompleteRequest();
        }
        catch (Exception ex)
        {
            textoMensagem = "Lamentamos mas ocorreu um erro e não foi possível concluir a operação. Tente novamente ou informe o suporte.";
            ImunneVacinas.Alerta.showMensagemErro(this.Page, null, textoMensagem);
            this.SalvarException(ex);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ChkListagem_CheckedChanged(object sender, EventArgs e)
    {
        int flagOpcoes = Convert.ToInt32(ChkListagem.Checked);

        TxtEnderecos.Enabled = Convert.ToBoolean(flagOpcoes);
        TxtEnderecos.Focus();
    }
}