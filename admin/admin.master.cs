﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class admin : System.Web.UI.MasterPage
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            IniciarPagina();
    }

    /// <summary>
    /// 
    /// </summary>
    private void IniciarPagina()
    {
        // Com sessão não autenticada, faz logoff direto
        if (!Request.IsAuthenticated)
            RealizarLogOff();

        // Verifica se é uma chamada de logoff
        string mnu = Request.QueryString["mnu"];
        if (mnu == "off")
            RealizarLogOff();

        // Verifica se sessão está vazia ou anônima
        if (HttpContext.Current.Session == null)
            RealizarLogOff();

        if (Session == null || Session.Keys.Count == 0 || Session["codigoUsuario"] == null 
            || String.IsNullOrEmpty(Session["codigoUsuario"].ToString()) 
            || Session["codigoUsuario"].ToString() == "anon")
            RealizarLogOff();
        else
        {
            try
            {
                Page.AlterarSessionUltimaAtualizacai();
                Page.AlterarSessionUltimaPaginaVisitada();
                Page.AlterarSessionBrowser();
                Session["pagina"] = Request.Url.AbsoluteUri;
                Application[Session.SessionID] = Session;

                int codigoUsuario = this.Page.ConsultarSessionCodigoUsuario();
                int tipoUsuario = this.Page.ConsultarSessionTipoUsuario();
                string nomeUsuario = this.Page.ConsultarSessionNomeUsuario();
                string paginaAtual = Path.GetFileNameWithoutExtension(Page.Request.AppRelativeCurrentExecutionFilePath);

                ImunneVacinas.Usuario usuarioConectado = ImunneVacinas.Usuario.ConsultarUnico(codigoUsuario);
                if (usuarioConectado == null)
                    RealizarLogOff();
                else
                {
                    lblNomeUsuarioDrop.Text = ImunneVacinas.Utils.RetornarTextoCapitalizado(nomeUsuario.ToLower());
                    lblNomeUsuarioSideBar.Text = ImunneVacinas.Utils.RetornarTextoCapitalizado(nomeUsuario.ToLower());
                    lblNomePainel.Text = ImunneVacinas.Utils.RetornarTextoCapitalizado(nomeUsuario.ToLower());
                    lblRole.Text = usuarioConectado.Tipo.Descricao.ToUpper();

                    PopularPermissoes(tipoUsuario);

                    string urlLogOff = "dashboard.aspx?mnu=off";
                    LnkLogOff.Attributes.Add("onclick", "return AltertaLogOff(this, event,'" + urlLogOff + "');");
                    LnkSairPop.Attributes.Add("onclick", "return AltertaLogOff(this, event,'" + urlLogOff + "');");

                    // negando acesso, caso o usuário logado digite o endereço de um recurso ao qual ele não tem permissao
                    bool permitir = true;

                    if (paginaAtual != "dashboard")
                    {
                        //Se o usuário não tiver acesso ao menu principal... redireciona
                        if (!permitir)
                        {
                            Response.Redirect("~/403.aspx", true);
                        }
                    }

                    lblCopyright.Text = "&copy; " + DateTime.Now.Year + " | S&A Imunizações";
                }
            }
            catch (Exception ex)
            {
                ImunneVacinas.Erro erro = new ImunneVacinas.Erro();
                erro.addArquivo(ex);
            }
        }
    }

    /// <summary>
    /// Rotina que popula recursos na tela inicial
    /// </summary>
    /// <param name="role">VALUE: Código do role para consulta de permissões de recursos</param>
    protected void PopularPermissoes(int role)
    {
        try
        {
            RptPermissoesMenu.DataSource = ImunneVacinas.GrupoMenu.ConsultarGrupos(role);
            RptPermissoesMenu.DataBind();

            SelecionarMenu();
        }
        catch (Exception ex)
        {
            ImunneVacinas.Erro erro = new ImunneVacinas.Erro();
            erro.addArquivo(ex);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void RptPermissoesMenu_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        ImunneVacinas.GrupoMenu item = (ImunneVacinas.GrupoMenu)e.Item.DataItem;
        int codigoRole = this.Page.ConsultarSessionTipoUsuario();

        //((HiddenField)e.Item.FindControl("HdfGrupoMenu*/")).Value = item.Codigo.ToString();
        ((HtmlGenericControl)e.Item.FindControl("i_icon")).Attributes.Add("class", item.Imagem);
        ((Label)e.Item.FindControl("lblNomeMenu")).Text = item.Nome;

        ImunneVacinas.PesquisaPermissao pesquisa = new ImunneVacinas.PesquisaPermissao()
        {
            Grupo = item.Codigo,
            Menu = -1,
            TipoUsuario = codigoRole
        };
        List<ImunneVacinas.Permissao> listaPermissoes = ImunneVacinas.Permissao.ConsultarPermissoes(pesquisa);

        if (listaPermissoes != null && listaPermissoes.Count > 0)
        {
            ((Repeater)e.Item.FindControl("RptSubMenus")).DataSource = listaPermissoes;
            ((Repeater)e.Item.FindControl("RptSubMenus")).DataBind();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void RptSubMenus_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        ImunneVacinas.Permissao item = (ImunneVacinas.Permissao)e.Item.DataItem;
        ImunneVacinas.Menu menuSelecionado = ImunneVacinas.Menu.ConsultarUnico(item.Menu.Codigo);

        ((HiddenField)e.Item.FindControl("HdfCodigoMenu")).Value = menuSelecionado.Codigo.ToString();
        ((HtmlAnchor)e.Item.FindControl("lnk_sub_menu")).HRef = menuSelecionado.Caminho;
        ((Label)e.Item.FindControl("lblSubMenu")).Text = menuSelecionado.Nome;
    }

    /// <summary>
    /// 
    /// </summary>
    private void SelecionarMenu()
    {
        Repeater rptMenus = (Repeater)Page.Master.FindControl("RptPermissoesMenu");
        string nomePagina = Path.GetFileName(Request.Path).Replace(".aspx", "").Replace("edit-", "").Replace("edit", "").Replace("-", " ");
        int contador = 0;

        ImunneVacinas.Menu menuSelecionado = ImunneVacinas.Menu.ConsultarUnico(nomePagina);
        int codigoMenu = 0;
        if (menuSelecionado != null)
            codigoMenu = menuSelecionado.Codigo;

        li_home.Attributes.Add("class", "treeview");

        foreach (RepeaterItem item in rptMenus.Items)
        {
            Label lblNomeMenu = (Label)item.FindControl("lblNomeMenu");
            HtmlGenericControl li_treeview = (HtmlGenericControl)item.FindControl("li_treeview");

            if (menuSelecionado != null)
            {
                if (lblNomeMenu.Text == menuSelecionado.IdGrupo.Nome)
                {
                    Repeater rptSubmenu = (Repeater)item.FindControl("RptSubMenus");

                    foreach (RepeaterItem submenu in rptSubmenu.Items)
                    {
                        Label lblNomeSubMenu = (Label)submenu.FindControl("lblSubMenu");
                        HtmlGenericControl li_submenu = (HtmlGenericControl)submenu.FindControl("li_submenu");

                        int codigoMenuPai = Convert.ToInt32(((HiddenField)submenu.FindControl("HdfCodigoMenu")).Value);
                        if (menuSelecionado.MenuPai > 0)
                        {
                            if (menuSelecionado.MenuPai == codigoMenuPai)
                            {
                                li_submenu.Attributes.Add("class", "active");
                                break;
                            }
                        }
                        else if (lblNomeSubMenu.Text == menuSelecionado.Nome)
                        {
                            li_submenu.Attributes.Add("class", "active");
                            break;
                        }
                    }

                    li_treeview.Attributes.Add("class", "treeview active");
                    contador = 1;
                    break;
                }
            }
        }

        if (contador == 0)
            li_home.Attributes.Add("class", "treeview active");
    }

    /// <summary>
    /// 
    /// </summary>
    private void RealizarLogOff()
    {
        // Log descritivo
        ImunneVacinas.Log logAcao = new ImunneVacinas.Log()
        {
            Tela = Path.GetFileName(Request.PhysicalPath),
            TabelaAcao = "Usuarios",
            TipoAcao = ImunneVacinas.Utils.AcaoLog.Acesso,
            Registro = this.Page.ConsultarSessionCodigoUsuario(),
            Resumo = String.Format("LOGOFF NO SISTEMA - USUÁRIO {0}.", this.Page.ConsultarSessionCodigoUsuario()),
            DataAcao = DateTime.Now,
            Campo = "LOGOFF",
            ValorOriginal = String.Empty,
            ValorNovo = String.Empty,
        };

        logAcao.IdUsuario.Codigo = this.Page.ConsultarSessionCodigoUsuario();
        logAcao.Incluir();

        Application.Remove(Session.SessionID);
        Session.Timeout = 1;
        Session.Abandon();
        Response.Cookies.Remove(FormsAuthentication.FormsCookieName);
        FormsAuthentication.SignOut();
        Server.ClearError();
        Context.ApplicationInstance.CompleteRequest();

        string sPath = System.Web.HttpContext.Current.Request.Url.AbsoluteUri;
        Response.Redirect("~/admin", false);
        Context.ApplicationInstance.CompleteRequest();
    }
}
