﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.IO;
using System.Text;
using System.Web.UI.WebControls;

public partial class campanhas : System.Web.UI.Page
{
    private StringBuilder url = new StringBuilder();
    private StringBuilder parametros = new StringBuilder();

    #region Private Proprieties

    private int PaginaAtual
    {
        get
        {
            object objPage = ViewState["_PaginaAtual"];
            int _PaginaAtual = 0;
            if (objPage == null)
            {
                _PaginaAtual = 0;
            }
            else
            {
                _PaginaAtual = (int)objPage;
            }
            return _PaginaAtual;
        }
        set
        {
            ViewState["_PaginaAtual"] = value;
        }
    }
    private int PrimeiroIndice
    {
        get
        {

            int _PrimeiroIndice = 0;
            if (ViewState["_PrimeiroIndice"] == null)
            {
                _PrimeiroIndice = 0;
            }
            else
            {
                _PrimeiroIndice = Convert.ToInt32(ViewState["_PrimeiroIndice"]);
            }
            return _PrimeiroIndice;
        }
        set
        {
            ViewState["_PrimeiroIndice"] = value;
        }
    }
    private int UltimoIndice
    {
        get
        {

            int _UltimoIndice = 0;
            if (ViewState["_UltimoIndice"] == null)
            {
                _UltimoIndice = 0;
            }
            else
            {
                _UltimoIndice = Convert.ToInt32(ViewState["_UltimoIndice"]);
            }
            return _UltimoIndice;
        }
        set
        {
            ViewState["_UltimoIndice"] = value;
        }
    }

    #endregion

    #region PagedDataSource

    PagedDataSource _PagedDataSource = new PagedDataSource();

    #endregion

    #region Private Methods

    /// <summary>
    /// 
    /// </summary>
    private void VincularItens()
    {
        try
        {
            List<ImunneVacinas.Campanha> retorno = ConsultarRegistros();

            _PagedDataSource.DataSource = retorno;
            _PagedDataSource.AllowPaging = true;
            _PagedDataSource.PageSize = 10;
            _PagedDataSource.CurrentPageIndex = PaginaAtual;
            ViewState["TotalPages"] = _PagedDataSource.PageCount;

            DdlPaginas.Items.Clear();
            for (int x = 0; x < _PagedDataSource.PageCount; x++)
            {
                DdlPaginas.Items.Add(new ListItem((x + 1).ToString(), x.ToString()));
            }

            DdlPaginas.SelectedValue = PaginaAtual.ToString();

            RptPrincipal.DataSource = _PagedDataSource;
            RptPrincipal.DataBind();
            CriarPaginacao();

            LblRegistros.Text = ImunneVacinas.Utils.MontarLinhaTotalizadora(retorno.Count);
            LblInfo.Text = "Exibindo página " + (PaginaAtual + 1) + " de " + _PagedDataSource.PageCount + ".";
        }
        catch (Exception Ex)
        {
            this.SalvarException(Ex);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void CriarPaginacao()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("PageIndex");
        dt.Columns.Add("PageText");

        PrimeiroIndice = PaginaAtual - 5;


        if (PaginaAtual > 5)
        {
            UltimoIndice = PaginaAtual + 5;
        }
        else
        {
            UltimoIndice = 10;
        }
        if (UltimoIndice > Convert.ToInt32(ViewState["TotalPages"]))
        {
            UltimoIndice = Convert.ToInt32(ViewState["TotalPages"]);
            PrimeiroIndice = UltimoIndice - 10;
        }

        if (PrimeiroIndice < 0)
        {
            PrimeiroIndice = 0;
        }

        for (int i = PrimeiroIndice; i < UltimoIndice; i++)
        {
            DataRow dr = dt.NewRow();
            dr[0] = i;
            dr[1] = i + 1;
            dt.Rows.Add(dr);
        }

        DdlPaginas.SelectedValue = PaginaAtual.ToString();
    }

    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            IniciarPagina();
    }

    /// <summary>
    /// 
    /// </summary>
    private void IniciarPagina()
    {
        Page.Title = "S&A Imunizações | Campanhas";

        try
        {
            NameValueCollection campanha = new NameValueCollection();

            string pesquisa = String.Empty;
            int current_page = -1;
            int codigoEmpresa = -1;
            int tipo = -1;

            if (!String.IsNullOrEmpty(Request.QueryString["campanha"]))
            {
                campanha = ImunneVacinas.Utils.RecuperarQueryString(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["campanha"]));
                pesquisa = campanha["pesquisa"];
                codigoEmpresa = Convert.ToInt32(campanha["empresa"]);
                tipo = Convert.ToInt32(campanha["tipo"]);
                current_page = Convert.ToInt32(campanha["current_page"]);
            }

            DdlSurtos.Items.Add(new ListItem("TIPOS", "-1"));
            DdlSurtos.Items.Add(new ListItem("SURTOS", "1"));
            DdlSurtos.Items.Add(new ListItem("REGULARES", "0"));
            DdlSurtos.SelectedValue = tipo.ToString();

            DdlEmpresas.DataSource = ImunneVacinas.Empresa.ConsultarComboBox(true, "EMPRESAS");
            DdlEmpresas.DataBind();
            DdlEmpresas.SelectedValue = codigoEmpresa.ToString();

            if (!String.IsNullOrEmpty(pesquisa) && pesquisa != "PESQUISAR")
                TxtPesquisa.Text = pesquisa;

            if (current_page > 0)
                PaginaAtual = current_page;

            VincularItens();
        }
        catch (Exception ex)
        {
            this.SalvarException(ex);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    private List<ImunneVacinas.Campanha> ConsultarRegistros()
    {
        ImunneVacinas.PesquisaCampanha pesquisa = new ImunneVacinas.PesquisaCampanha
        {
            Empresa = Convert.ToInt32(DdlEmpresas.SelectedValue),
            CampoLivre = TxtPesquisa.Text,
            Surto = Convert.ToInt32(DdlSurtos.SelectedValue),
            Ordenacao = ImunneVacinas.Utils.TipoOrdenacao.porDataDesc
        };

        return ImunneVacinas.Campanha.ConsultarCampanhas(pesquisa);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void RptPrincipal_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        int codigoRegistro = Convert.ToInt32(e.CommandArgument);
        string comando = e.CommandName;

        ImunneVacinas.Campanha registroSelecionado = ImunneVacinas.Campanha.ConsultarUnico(codigoRegistro);

        switch (comando)
        {
            case "importar":
                {
                    url = new StringBuilder();
                    parametros = new StringBuilder();

                    parametros.AppendFormat("acao:{0}|", "importar");
                    parametros.AppendFormat("codigo:{0}|", codigoRegistro);
                    parametros.AppendFormat("empresa:{0}|", DdlEmpresas.SelectedValue);
                    parametros.AppendFormat("tipo:{0}|", DdlSurtos.SelectedValue);
                    parametros.AppendFormat("pesquisa:{0}|", TxtPesquisa.Text);
                    parametros.AppendFormat("current_page:{0}", PaginaAtual);

                    url.AppendFormat("~/admin/importar-adesao.aspx?campanha={0}", ImunneVacinas.Criptografia.Criptografar(parametros.ToString()));

                    Response.Redirect(url.ToString(), false);
                    Context.ApplicationInstance.CompleteRequest();
                } break;
            case "gerar":
                {
                    url = new StringBuilder();
                    parametros = new StringBuilder();

                    parametros.AppendFormat("acao:{0}|", "gerar");
                    parametros.AppendFormat("codigo:{0}|", codigoRegistro);
                    parametros.AppendFormat("empresa:{0}|", DdlEmpresas.SelectedValue);
                    parametros.AppendFormat("tipo:{0}|", DdlSurtos.SelectedValue);
                    parametros.AppendFormat("pesquisa:{0}|", TxtPesquisa.Text);
                    parametros.AppendFormat("current_page:{0}", PaginaAtual);

                    url.AppendFormat("~/admin/gerar-adesao.aspx?campanha={0}", ImunneVacinas.Criptografia.Criptografar(parametros.ToString()));

                    Response.Redirect(url.ToString(), false);
                    Context.ApplicationInstance.CompleteRequest();
                } break;
            case "adesoes":
                {
                    url = new StringBuilder();
                    parametros = new StringBuilder();

                    parametros.AppendFormat("acao:{0}|", "edit");
                    parametros.AppendFormat("codigo:{0}|", codigoRegistro);
                    parametros.AppendFormat("empresa:{0}|", DdlEmpresas.SelectedValue);
                    parametros.AppendFormat("tipo:{0}|", DdlSurtos.SelectedValue);
                    parametros.AppendFormat("pesquisa:{0}|", TxtPesquisa.Text);
                    parametros.AppendFormat("current_page:{0}", PaginaAtual);

                    url.AppendFormat("~/admin/adesoes.aspx?campanha={0}", ImunneVacinas.Criptografia.Criptografar(parametros.ToString()));

                    Response.Redirect(url.ToString(), false);
                    Context.ApplicationInstance.CompleteRequest();
                } break;
            case "edit":
                {
                    url = new StringBuilder();
                    parametros = new StringBuilder();

                    parametros.AppendFormat("acao:{0}|", "edit");
                    parametros.AppendFormat("codigo:{0}|", codigoRegistro);
                    parametros.AppendFormat("empresa:{0}|", DdlEmpresas.SelectedValue);
                    parametros.AppendFormat("tipo:{0}|", DdlSurtos.SelectedValue);
                    parametros.AppendFormat("pesquisa:{0}|", TxtPesquisa.Text);
                    parametros.AppendFormat("current_page:{0}", PaginaAtual);

                    url.AppendFormat("~/admin/edit-campanhas.aspx?campanha={0}", ImunneVacinas.Criptografia.Criptografar(parametros.ToString()));

                    Response.Redirect(url.ToString(), false);
                    Context.ApplicationInstance.CompleteRequest();
                }
                break;
            case "locais":
                {
                    url = new StringBuilder();
                    parametros = new StringBuilder();

                    parametros.AppendFormat("acao:{0}|", "edit");
                    parametros.AppendFormat("codigo:{0}|", codigoRegistro);
                    parametros.AppendFormat("empresa:{0}|", DdlEmpresas.SelectedValue);
                    parametros.AppendFormat("tipo:{0}|", DdlSurtos.SelectedValue);
                    parametros.AppendFormat("pesquisa:{0}|", TxtPesquisa.Text);
                    parametros.AppendFormat("current_page:{0}", PaginaAtual);

                    url.AppendFormat("~/admin/locais-adesao.aspx?campanha={0}", ImunneVacinas.Criptografia.Criptografar(parametros.ToString()));

                    Response.Redirect(url.ToString(), false);
                    Context.ApplicationInstance.CompleteRequest();
                }
                break;
            case "clinicas":
                {
                    url = new StringBuilder();
                    parametros = new StringBuilder();

                    parametros.AppendFormat("acao:{0}|", "edit");
                    parametros.AppendFormat("codigo:{0}|", codigoRegistro);
                    parametros.AppendFormat("empresa:{0}|", DdlEmpresas.SelectedValue);
                    parametros.AppendFormat("tipo:{0}|", DdlSurtos.SelectedValue);
                    parametros.AppendFormat("pesquisa:{0}|", TxtPesquisa.Text);
                    parametros.AppendFormat("current_page:{0}", PaginaAtual);

                    url.AppendFormat("~/admin/clinicas-campanhas.aspx?campanha={0}", ImunneVacinas.Criptografia.Criptografar(parametros.ToString()));

                    Response.Redirect(url.ToString(), false);
                    Context.ApplicationInstance.CompleteRequest();
                }
                break;
            case "produtos":
                {
                    url = new StringBuilder();
                    parametros = new StringBuilder();

                    parametros.AppendFormat("acao:{0}|", "edit");
                    parametros.AppendFormat("codigo:{0}|", codigoRegistro);
                    parametros.AppendFormat("empresa:{0}|", DdlEmpresas.SelectedValue);
                    parametros.AppendFormat("tipo:{0}|", DdlSurtos.SelectedValue);
                    parametros.AppendFormat("pesquisa:{0}|", TxtPesquisa.Text);
                    parametros.AppendFormat("current_page:{0}", PaginaAtual);

                    url.AppendFormat("~/admin/produtos-campanhas.aspx?campanha={0}", ImunneVacinas.Criptografia.Criptografar(parametros.ToString()));

                    Response.Redirect(url.ToString(), false);
                    Context.ApplicationInstance.CompleteRequest();
                }
                break;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void RptPrincipal_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        try
        {
            ImunneVacinas.Campanha campanhaSelecionada = (ImunneVacinas.Campanha)e.Item.DataItem;

            LinkButton LnkEditList = (LinkButton)e.Item.FindControl("LnkEditList");
            LinkButton LnkDeleteList = (LinkButton)e.Item.FindControl("LnkDeleteList");
            LinkButton LnkClinicasList = (LinkButton)e.Item.FindControl("LnkClinicasList");
            LinkButton LnkProdutosList = (LinkButton)e.Item.FindControl("LnkProdutosList");
            LinkButton LnkGerarLinkList = (LinkButton)e.Item.FindControl("LnkGerarLinkList");
            LinkButton LnkAdesoesList = (LinkButton)e.Item.FindControl("LnkAdesoesList");
            LinkButton LnkImportarList = (LinkButton)e.Item.FindControl("LnkImportarList");
            LinkButton LnkLocaisList = (LinkButton)e.Item.FindControl("LnkLocaisList");

            HyperLink LnkAcessarLinkList = (HyperLink)e.Item.FindControl("LnkAcessarLinkList");

            Label LblTotalClinicas = (Label)e.Item.FindControl("LblTotalClinicas");
            Label LblTotalCompras = (Label)e.Item.FindControl("LblTotalCompras");
            Label LblTotalProdutos = (Label)e.Item.FindControl("LblTotalProdutos");
            Label LblNome = (Label)e.Item.FindControl("LblNome");
            Label LblContato = (Label)e.Item.FindControl("LblContato");
            Label LblInicio = (Label)e.Item.FindControl("LblInicio");
            Label LblTermino = (Label)e.Item.FindControl("LblTermino");
            Label LblValidade = (Label)e.Item.FindControl("LblValidade");
            Label LblParcelas = (Label)e.Item.FindControl("LblParcelas");
            Label LblIdentificacao = (Label)e.Item.FindControl("LblIdentificacao");
            Label LblAplicacoes = (Label)e.Item.FindControl("LblAplicacoes");

            LnkLocaisList.CommandArgument = campanhaSelecionada.Codigo.ToString();
            LnkDeleteList.CommandArgument = campanhaSelecionada.Codigo.ToString();
            LnkEditList.CommandArgument = campanhaSelecionada.Codigo.ToString();
            LnkClinicasList.CommandArgument = campanhaSelecionada.Codigo.ToString();
            LnkProdutosList.CommandArgument = campanhaSelecionada.Codigo.ToString();
            LnkGerarLinkList.CommandArgument = campanhaSelecionada.Codigo.ToString();
            LnkProdutosList.CommandArgument = campanhaSelecionada.Codigo.ToString();
            LnkAdesoesList.CommandArgument = campanhaSelecionada.Codigo.ToString();
            LnkImportarList.CommandArgument = campanhaSelecionada.Codigo.ToString();

            LnkDeleteList.OnClientClick = "return ConfirmacaoExclucao(this, event,'" + campanhaSelecionada.Codigo + "');";

            LblNome.Text = campanhaSelecionada.Nome;
            LblContato.Text = campanhaSelecionada.NomeContato;
            LblInicio.Text = campanhaSelecionada.DataInicio.ToString("dd/MM/yyyy");
            LblTermino.Text = campanhaSelecionada.DataTermino.ToString("dd/MM/yyyy");
            LblAplicacoes.Text = campanhaSelecionada.DataInicio.ToString("dd/MM/yyyy");
            LblValidade.Text = campanhaSelecionada.DataLimite.ToString("dd/MM/yyyy");
            LblParcelas.Text = campanhaSelecionada.Parcelas.ToString().PadLeft(2, '0');
            LblIdentificacao.Text = campanhaSelecionada.Identificacao;

            int totalClinicas = 0;
            int totalCompras = 0;
            int totalProdutos = 0;
            int totalAdesoes = 0;

            #region CLINICAS

            ImunneVacinas.PesquisaCampanhaClinica pesquisa = new ImunneVacinas.PesquisaCampanhaClinica()
            {
                Campanha = campanhaSelecionada.Codigo
            };
            totalClinicas = ImunneVacinas.CampanhaClinica.ConsultarCampanhasClinicas(pesquisa).Count;
            LblTotalClinicas.Text = totalClinicas.ToString();

            #endregion

            #region PRODUTOS

            ImunneVacinas.PesquisaProdutoCampanha pesquisaProduto = new ImunneVacinas.PesquisaProdutoCampanha()
            {
                Campanha = campanhaSelecionada.Codigo
            };
            totalProdutos = ImunneVacinas.ProdutoCampanha.ConsultarProdutoCampanhas(pesquisaProduto).Count;
            LblTotalProdutos.Text = totalProdutos.ToString();

            #endregion

            #region COMPRAS

            ImunneVacinas.PesquisaCompra pesquisaCompra = new ImunneVacinas.PesquisaCompra()
            {
                Campanha = campanhaSelecionada.Codigo
            };

            totalCompras = ImunneVacinas.Compra.ConsultarTotalCompras(pesquisaCompra);
            LblTotalCompras.Text = totalCompras.ToString();

            #endregion

            if (totalClinicas > 0 || totalProdutos > 0 || totalCompras > 0)
                LnkDeleteList.Visible = false;

            #region ADESÕES

            ImunneVacinas.PesquisaAdesao pesquisaAdesao = new ImunneVacinas.PesquisaAdesao()
            {
                Campanha = campanhaSelecionada.Codigo,
                Ordenacao = ImunneVacinas.Utils.TipoOrdenacao.porCodigo
            };

            totalAdesoes = ImunneVacinas.Adesao.ConsultarAdesoes(pesquisaAdesao).Count;

            if (String.IsNullOrEmpty(campanhaSelecionada.LinkAdesao))
            {
                LnkAcessarLinkList.Visible = false;
                LnkAdesoesList.Visible = false;
                LnkGerarLinkList.Visible = true;
                LnkLocaisList.Visible = false;

                if (totalAdesoes > 0)
                    LnkAdesoesList.Visible = true;
                else LnkAdesoesList.Visible = false;
            }
            else
            {
                LnkAcessarLinkList.NavigateUrl = campanhaSelecionada.LinkAdesao;
                LnkAcessarLinkList.Target = "_blank";
                LnkAcessarLinkList.Visible = true;
                LnkAdesoesList.Visible = true;
                LnkGerarLinkList.Visible = false;
                LnkLocaisList.Visible = true;

                if (totalAdesoes > 0)
                    LnkAdesoesList.Visible = true;
                else LnkAdesoesList.Visible = false;
            }

            #endregion
        }
        catch (Exception ex)
        {
            this.SalvarException(ex);
        }
    }

    #region Paginação

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void DdlPaginas_SelectedIndexChanged(object sender, EventArgs e)
    {
        PaginaAtual = Convert.ToInt32(DdlPaginas.SelectedValue);
        VincularItens();
    }

    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BtnPesquisar_Click(object sender, EventArgs e)
    {
        PaginaAtual = 0;
        VincularItens();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BtnNovo_Click(object sender, EventArgs e)
    {
        url = new StringBuilder();
        parametros = new StringBuilder();

        parametros.AppendFormat("acao:{0}|", "insert");
        parametros.AppendFormat("codigo:{0}|", "-1");
        parametros.AppendFormat("empresa:{0}|", DdlEmpresas.SelectedValue);
        parametros.AppendFormat("tipo:{0}|", DdlSurtos.SelectedValue);
        parametros.AppendFormat("pesquisa:{0}|", TxtPesquisa.Text);
        parametros.AppendFormat("current_page:{0}", PaginaAtual);

        url.AppendFormat("~/admin/edit-campanhas.aspx?campanha={0}", ImunneVacinas.Criptografia.Criptografar(parametros.ToString()));

        Response.Redirect(url.ToString(), false);
        Context.ApplicationInstance.CompleteRequest();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BtnExcluirItens_Click(object sender, EventArgs e)
    {
        ExcluirRegistro();
    }

    /// <summary>
    /// 
    /// </summary>
    private void ExcluirRegistro()
    {
        int codigoRegistro = Convert.ToInt32(HdfCodigo.Value);
        ImunneVacinas.Campanha registroSelecionado = ImunneVacinas.Campanha.ConsultarUnico(codigoRegistro);

        if (registroSelecionado == null)
            ImunneVacinas.Alerta.showMensagemErro(this.Page, "Oops!", "Nenhum registro foi localizado com o código informado!");
        else
        {
            try
            {
                int resultado = registroSelecionado.Excluir();
                if (resultado > 0)
                {
                    // Log descritivo
                    ImunneVacinas.Log logAcao = new ImunneVacinas.Log()
                    {
                        Tela = Path.GetFileName(Request.PhysicalPath),
                        TabelaAcao = "Campanhas",
                        TipoAcao = ImunneVacinas.Utils.AcaoLog.Exclusao,
                        Registro = registroSelecionado.Codigo,
                        Resumo = String.Format("EXCLUSÃO DE REGISTRO {0}.", registroSelecionado.Codigo),
                        DataAcao = DateTime.Now,
                        Campo = String.Empty,
                        ValorOriginal = String.Empty,
                        ValorNovo = String.Empty,
                    };

                    logAcao.Incluir();

                    ImunneVacinas.Alerta.showMensagemSucesso(this.Page, "Sucesso!", "Registro excluído com sucesso!");
                    HdfCodigo.Value = "";

                    VincularItens();
                }
                else ImunneVacinas.Alerta.showMensagemErro(this.Page, "Oops!", "Infelizmente não foi possível concluir a solicitação, tente novamente ou informe o suporte.");
            }
            catch (Exception ex)
            {
                this.SalvarException(ex);
                ImunneVacinas.Alerta.showMensagemErro(this.Page, "Oops!", "Infelizmente não foi possível concluir a solicitação, tente novamente ou informe o suporte.");
            }
        }
    }
}