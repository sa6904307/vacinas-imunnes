﻿using System;
using System.Web;

public partial class trust : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //Response.Write(GetCurrentTrustLevel().ToString());
        if (!IsPostBack)
            IniciarPagina();
    }

    /// <summary>
    /// 
    /// </summary>
    private void IniciarPagina()
    {
        ////string urlFinal = ImunneVacinas.Utils.ShortenUrl("http://www.google.com", 5);
        //UrlShortener.Settings;
        string urlFinal = ImunneVacinas.Utils.EncurtadorUrl("http://www.google.com");
        Response.Write(urlFinal.ToString());
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    private AspNetHostingPermissionLevel GetCurrentTrustLevel()
    {
        foreach (AspNetHostingPermissionLevel trustLevel in
                new AspNetHostingPermissionLevel[] {
                AspNetHostingPermissionLevel.Unrestricted,
                AspNetHostingPermissionLevel.High,
                AspNetHostingPermissionLevel.Medium,
                AspNetHostingPermissionLevel.Low,
                AspNetHostingPermissionLevel.Minimal
                })
        {
            try
            {
                new AspNetHostingPermission(trustLevel).Demand();
            }
            catch (System.Security.SecurityException)
            {
                continue;
            }

            return trustLevel;
        }

        return AspNetHostingPermissionLevel.None;
    }
}