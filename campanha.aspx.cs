﻿using System;
using System.Text;

public partial class imunne_campanha : System.Web.UI.Page
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void LnkConsultar_Click(object sender, EventArgs e)
    {
        StringBuilder textoMensagem = new StringBuilder();

        ImunneVacinas.Campanha campanhaSelecionada = ImunneVacinas.Campanha.ConsultarUnico(TxtCampanha.Text.Trim());
        if (campanhaSelecionada != null)
        {
            if (campanhaSelecionada.DataTermino < DateTime.Now.Date)
            {
                textoMensagem.AppendFormat("Infelizmente a campanha {0} terminou sua validade na data de {1}.", campanhaSelecionada.Identificacao.ToUpper() + " | " + campanhaSelecionada.Nome.ToUpper(), campanhaSelecionada.DataTermino.ToString("dd/MM/yyyy"));
                ImunneVacinas.Alerta.showMensagemAviso(this.Page, "Aviso", textoMensagem.ToString());
                return;
            }
            else if (campanhaSelecionada.DataInicio > DateTime.Now.Date)
            {
                textoMensagem.AppendFormat("Informamos que a campanha {0} ainda não iniciou sua validade, retorne em {1}.", campanhaSelecionada.Identificacao.ToUpper() + " | " + campanhaSelecionada.Nome.ToUpper(), campanhaSelecionada.DataInicio.ToString("dd/MM/yyyy"));
                ImunneVacinas.Alerta.showMensagemAviso(this.Page, "Aviso", textoMensagem.ToString());
                return;
            }
        }
        else
        {
            textoMensagem.AppendFormat("Infelizmente nenhuma campanha foi encontra como {0}.", TxtCampanha.Text.ToUpper());
            ImunneVacinas.Alerta.showMensagemAviso(this.Page, "Aviso", textoMensagem.ToString());
            return;
        }

        // Redirecionamento da campanha
        StringBuilder urlPagina = new StringBuilder();
        urlPagina.AppendFormat("~/termos.aspx?campanha={0}", ImunneVacinas.Criptografia.Criptografar(campanhaSelecionada.Codigo.ToString()));
        Response.Redirect(urlPagina.ToString(), false);
        Context.ApplicationInstance.CompleteRequest();
    }
}