﻿using System;
using System.Text;
using System.Web.UI.WebControls;

public partial class imunne_clinicas : System.Web.UI.Page
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            IniciarPagina();
    }

    /// <summary>
    /// 
    /// </summary>
    private void IniciarPagina()
    {
        StringBuilder textoMensagem = new StringBuilder();
        int codigoCampanha = -1;

        try
        {
            if (!String.IsNullOrEmpty(Request.QueryString["campanha"]))
            {
                codigoCampanha = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["campanha"]));

                ImunneVacinas.Campanha campanhaSelecionada = ImunneVacinas.Campanha.ConsultarUnico(codigoCampanha);
                LblCampanha.Text = campanhaSelecionada.Identificacao.ToUpper() + " | " + campanhaSelecionada.Nome.ToUpper();
                LblValidade.Text = campanhaSelecionada.DataLimite.ToString("dd/MM/yyyy");
                LblEmpresa.Text = campanhaSelecionada.IdEmpresa.NomeFantasia.ToUpper();

                Page.Title = "S&A Imunizações | " + campanhaSelecionada.Nome.ToUpper();

                DdlUfs.DataSource = ImunneVacinas.Uf.ConsultarComboBox(true, "UF");
                DdlUfs.DataBind();

                DdlCidades.Items.Add(new ListItem("CIDADES", "-1"));
                DdlCidades.Enabled = false;

                ConsultarClinicas(codigoCampanha);
            }
            else
            {
                Response.Redirect("~/", false);
                Context.ApplicationInstance.CompleteRequest();
            }
        }
        catch (Exception ex)
        {
            Response.Redirect("~/", false);
            Context.ApplicationInstance.CompleteRequest();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="codigoCampanha"></param>
    private void ConsultarClinicas(int codigoCampanha)
    {
        ImunneVacinas.PesquisaCampanhaClinica pesquisa = new ImunneVacinas.PesquisaCampanhaClinica()
        {
            Uf = DdlUfs.SelectedValue,
            CampoLivre = TxtCampoLivre.Text,
            Cidade = Convert.ToInt32(DdlCidades.SelectedValue),
            Campanha = codigoCampanha,
            Ordenacao = ImunneVacinas.Utils.TipoOrdenacao.porNome
        };

        RptPrincipal.DataSource = ImunneVacinas.CampanhaClinica.ConsultarCampanhasClinicas(pesquisa);
        RptPrincipal.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void RptPrincipal_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        StringBuilder urlPagina = new StringBuilder();
        StringBuilder parametros = new StringBuilder();

        int codigoRegistro = Convert.ToInt32(e.CommandArgument);
        string comando = e.CommandName;

        switch (comando)
        {
            case "selecionar":
                {
                    urlPagina.AppendFormat("~/identificacao.aspx?campanha={0}", Request.QueryString["campanha"]);
                    urlPagina.AppendFormat("&clinica={0}", ImunneVacinas.Criptografia.Criptografar(codigoRegistro.ToString()));
                    Response.Redirect(urlPagina.ToString(), false);
                    Context.ApplicationInstance.CompleteRequest();
                } break;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void RptPrincipal_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        ImunneVacinas.CampanhaClinica item = (ImunneVacinas.CampanhaClinica)e.Item.DataItem;
        ImunneVacinas.Clinica clinicaSelecionada = ImunneVacinas.Clinica.ConsultarUnico(item.IdClinica.Codigo);

        Label LblNomeClinica = (Label)e.Item.FindControl("LblNomeClinica");
        Label LblEndereco = (Label)e.Item.FindControl("LblEndereco");
        Label LblTelefone = (Label)e.Item.FindControl("LblTelefone");
        Label LblCidade = (Label)e.Item.FindControl("LblCidade");
        LinkButton LnkSelecionar = (LinkButton)e.Item.FindControl("LnkSelecionar");

        LblNomeClinica.Text = clinicaSelecionada.NomeFantasia;
        LblTelefone.Text = clinicaSelecionada.Telefone;
        LblEndereco.Text = clinicaSelecionada.Endereco + ", "
                         + clinicaSelecionada.Numero;

        LblCidade.Text = clinicaSelecionada.IdCidade.Nome + "/"
                         + clinicaSelecionada.IdUf.Sigla;

        LnkSelecionar.CommandArgument = clinicaSelecionada.Codigo.ToString();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void DdlUfs_SelectedIndexChanged(object sender, EventArgs e)
    {
        string siglaUf = DdlUfs.SelectedValue;
        DdlCidades.Items.Clear();

        if (siglaUf != "-1")
        {
            DdlCidades.DataSource = ImunneVacinas.Cidade.ConsultarComboBox(siglaUf, true, "CIDADES");
            DdlCidades.DataBind();
            DdlCidades.Enabled = true;
            DdlCidades.Focus();
        }
        else
        {
            DdlCidades.Items.Add(new ListItem("CIDADES", "-1"));
            DdlCidades.Enabled = false;
            DdlUfs.Focus();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void LnkPesquisar_Click(object sender, EventArgs e)
    {
        int codigoCampanha = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["campanha"]));
        ConsultarClinicas(codigoCampanha);
    }
}