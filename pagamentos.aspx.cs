﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Net;
using System.Net.Http;
using Newtonsoft.Json;

public partial class imunne_pagamentos : System.Web.UI.Page
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = "S&A Imunizações | Informações de Pagamentos";

        if (!IsPostBack)
            IniciarPagina();
    }

    /// <summary>
    /// Método acionado no carregamento inicial da página. São lidas as informações dos parametros da URL
    /// e populados todos os dados para o usuário seguir com sua aquisição
    /// </summary>
    private void IniciarPagina()
    {
        try
        {
            int codigoCampanha = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["campanha"]));
            int codigoParticipante = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["participante"]));
            int codigoCompra = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["compra"]));
            int codigoClinica = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["clinica"]));

            // Verificação de parametros via URL
            if (codigoCampanha <= 0 || codigoParticipante <= 0 || codigoCompra <= 0)
            {
                Response.Redirect("~/", false);
                Context.ApplicationInstance.CompleteRequest();
            }

            ImunneVacinas.Participante participanteSelecionado = ImunneVacinas.Participante.ConsultarUnico(codigoParticipante);
            ImunneVacinas.Campanha campanhaSelecionada = ImunneVacinas.Campanha.ConsultarUnico(codigoCampanha);
            ImunneVacinas.Compra compraSelecionada = ImunneVacinas.Compra.ConsultarUnico(codigoCompra);
            ImunneVacinas.Clinica clinicaSelecionada = ImunneVacinas.Clinica.ConsultarUnico(codigoClinica);

            LblNascimento.Text = participanteSelecionado.DataNascimento.ToString("dd/MM/yyyy");
            LblNome.Text = participanteSelecionado.Nome.ToUpper();
            LblCPF.Text = ImunneVacinas.Utils.FormatarCpfCnpj(participanteSelecionado.Cpf);
            LblValor.Text = ImunneVacinas.Utils.ToMoeda(compraSelecionada.Valor);

            LblCampanha.Text = campanhaSelecionada.Identificacao.ToUpper() + " | " + campanhaSelecionada.Nome.ToUpper();
            LblValidade.Text = campanhaSelecionada.DataLimite.ToString("dd/MM/yyyy");
            LblClinica.Text = clinicaSelecionada.NomeFantasia.ToUpper() + "<br />"
                            + clinicaSelecionada.Endereco.ToUpper() + ", "
                            + clinicaSelecionada.Numero + " - "
                            + clinicaSelecionada.IdCidade.Nome.ToUpper() + "/"
                            + clinicaSelecionada.IdUf.Sigla;
            LblEmpresa.Text = campanhaSelecionada.IdEmpresa.NomeFantasia.ToUpper();

            LblSituacao.Text = compraSelecionada.Situacao.ToString().ToUpper();
            LblDataCompra.Text = compraSelecionada.DataCompra.ToString("dd/MM/yyyy");

            // Criando combo de Parcelas, com os valores de cada uma
            for (int i = 1; i <= campanhaSelecionada.Parcelas; i++)
            {
                decimal valorParcela = compraSelecionada.Valor / i;
                string descricaoParcela = String.Format("{0}x - {1}", i, ImunneVacinas.Utils.ToMoeda(valorParcela));

                DdlParcelas.Items.Add(new ListItem(descricaoParcela, i.ToString()));
            }

            DdlMesValidade.Items.Add(new ListItem("MÊS", "-1"));
            DdlAnoValidade.Items.Add(new ListItem("ANO", "-1"));

            for (int ano = 2019; ano <= 2040; ano++)
            {
                DdlAnoValidade.Items.Add(new ListItem(ano.ToString(), ano.ToString()));
            }

            for (int mes = 1; mes <= 12; mes++)
            {
                DdlMesValidade.Items.Add(new ListItem(mes.ToString().PadLeft(2, '0'), mes.ToString().PadLeft(2, '0')));
            }

            // Verifica tipo de pagamento
            bool metodoPagamentoCartao = VerificaTransacaoCartao(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["tipoPagamento"]));
            PnlInformacoesPgto.Visible = metodoPagamentoCartao;
            PnlInformacoesPix.Visible = !metodoPagamentoCartao;

            /// Travando campos de cartão
            if (compraSelecionada.Situacao != ImunneVacinas.Compra.SituacaoCompra.Concluida)
            {
                if(metodoPagamentoCartao)
                {
                    OcultaPagamentoPix();
                    PnlInformacoesPgto.Visible = true;
                } 
                else
                {
                    OcultaPagamentoCartao();
                    PnlInformacoesPix.Visible = true;
                    
                    #region Gera cobrança pix
                    PagamentoPix();
                    #endregion
                }

                PnlInformacoesDirecionamento.Visible = false;

                if (campanhaSelecionada.Surto == 1)
                    ImunneVacinas.Alerta.showMensagemAviso(this.Page, "S&A Imunizações", "Após a aprovação de sua compra, você será direcionado para a escolha do horário de aplicação!");
            }
            else
            {
                PnlInformacoesPgto.Visible = false;
                PnlInformacoesPix.Visible = false;
                PnlInformacoesDirecionamento.Visible = true;
            }

        }
        catch (Exception ex)
        {
            ImunneVacinas.Erro erro = new ImunneVacinas.Erro();
            erro.addArquivo(ex);
            Response.Redirect("~/", false);
            Context.ApplicationInstance.CompleteRequest();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void LnkContinuar_Click(object sender, EventArgs e)
    {
        string tipoPagamento = ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["tipoPagamento"]);
        if(tipoPagamento == "cartao"){
            PagamentoCartao();
        } else {
            VerificaPagamentoPix("");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void LnkContinuar2_Click(object sender, EventArgs e)
    {
        // AQUI OCORRE O DIRECIONAMENTO DO USUÁRIO, CASO O MESMO OPTE POR SIMPLESMENTE SEGUIR
        // SEM ESCOLHER O AGENDAMENTO.

        #region DADOS DA URL 

        StringBuilder urlPagina = new StringBuilder();

        urlPagina.AppendFormat("~/vouchers.aspx?campanha={0}", Request.QueryString["campanha"]);
        urlPagina.AppendFormat("&clinica={0}", Request.QueryString["clinica"]);
        urlPagina.AppendFormat("&participante={0}", Request.QueryString["participante"]);
        urlPagina.AppendFormat("&itens={0}", Request.QueryString["itens"]);
        urlPagina.AppendFormat("&qtde={0}", Request.QueryString["qtde"]);
        urlPagina.AppendFormat("&compra={0}", Request.QueryString["compra"]);
        urlPagina.AppendFormat("&membros={0}", Request.QueryString["membros"]);

        #endregion

        Response.Redirect(urlPagina.ToString(), false);
        Context.ApplicationInstance.CompleteRequest();
    }

    /// <summary>
    /// Método LOCAL que simula uma compra de testes, sem validar dados de cartão na plataforma,
    /// afim de testar as informações que são enviadas e recebidas, de acordo com a compra
    /// que foi realizada na campanha
    /// </summary>
    /// <param name="compraSelecionada">VALUE: Elemento do tipo COMPRA a ser validado</param>
    /// <returns>Retorna um elemento do tipo TRANSACAOREALIZADA, com os dados para seguir o fluxo de operações</returns>
    private ImunneVacinas.TransacaoRealizada GerarTransacaoTestes(ImunneVacinas.Compra compraSelecionada)
    {
        int contadorInclusoes = 0;
        int codigoClinica = compraSelecionada.IdClinica.Codigo;
        int codigoCampanha = compraSelecionada.IdCampanha.Codigo;
        int codigoTitular = compraSelecionada.IdParticipante.Codigo;

        string[] membros = new string[1];
        string[] itens = new string[1];

        if (!String.IsNullOrEmpty(Request.QueryString["membros"]))
            membros = ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["membros"]).Split('|');

        if (!String.IsNullOrEmpty(Request.QueryString["itens"]))
            itens = ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["itens"]).Split('|');

        if (!String.IsNullOrEmpty(Request.QueryString["membros"]))
            membros = ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["membros"]).Split('|');

        #region PRODUTOS DA CAMPANHA

        decimal totalValorCompra = 0;

        ImunneVacinas.PesquisaProdutoCampanha pesquisaProdutos = new ImunneVacinas.PesquisaProdutoCampanha()
        {
            Campanha = codigoCampanha,
            Ordenacao = ImunneVacinas.Utils.TipoOrdenacao.porNome
        };

        List<ImunneVacinas.ProdutoCampanha> listaProdutos = ImunneVacinas.ProdutoCampanha.ConsultarProdutoCampanhas(pesquisaProdutos);
        if (listaProdutos == null || listaProdutos.Count <= 0)
        {
            return null;
        }

        #endregion

        if (compraSelecionada.Codigo > 0)
        {
            int totalErros = 0;
            int totalItens = 0;

            #region GERANDO OS VOUCHERS DE CADA UM

            foreach (string membro in membros)
            {
                ImunneVacinas.Participante membroSelecionado = ImunneVacinas.Participante.ConsultarUnico(Convert.ToInt32(membro));

                StringBuilder descricaoVoucher = new StringBuilder();
                descricaoVoucher.AppendFormat("{0} ADQUIRIU PRODUTOS DA CAMPANHA {1}", membroSelecionado.Nome.ToUpper(), compraSelecionada.IdCampanha.Identificacao.ToUpper().ToString());
                descricaoVoucher.AppendFormat(" QUE SERÃO APLICADOS NA CLÍNICA {0}.\r\nABAIXO OS PRODUTOS:\r\n\r\n", compraSelecionada.IdClinica.NomeFantasia.ToUpper());

                foreach (string item in itens)
                {
                    ImunneVacinas.ProdutoCampanha produtoSelecionado = ImunneVacinas.ProdutoCampanha.ConsultarUnico(Convert.ToInt32(item));
                    descricaoVoucher.AppendFormat("1 UNIDADE(S) DE {0}.\r\n", produtoSelecionado.IdVacina.Nome.ToUpper());
                }

                descricaoVoucher.AppendFormat("\r\nCOMPRAS REALIZADAS POR {0}.", compraSelecionada.IdParticipante.Nome.ToUpper());

                ImunneVacinas.Voucher novoVoucher = new ImunneVacinas.Voucher();
                novoVoucher.IdCampanha.Codigo = codigoCampanha;
                novoVoucher.IdClinica.Codigo = codigoClinica;
                novoVoucher.IdCompra.Codigo = compraSelecionada.Codigo;
                novoVoucher.IdParticipante.Codigo = membroSelecionado.Codigo;
                novoVoucher.DataCriacao = DateTime.Now;
                novoVoucher.Descricao = descricaoVoucher.ToString();
                novoVoucher.Situacao = ImunneVacinas.Voucher.SituacaoVoucher.Gerado;

                try
                {
                    contadorInclusoes = 1;
                    novoVoucher.Incluir();
                }
                catch (Exception ex)
                {
                    ImunneVacinas.Erro erro = new ImunneVacinas.Erro();
                    erro.AdicionarLog(ex);
                }
            }

            #endregion

            #region GERANDO ITENS DA COMPRA

            ImunneVacinas.PesquisaItemCompra pesquisaItens = new ImunneVacinas.PesquisaItemCompra()
            {
                Compra = compraSelecionada.Codigo
            };

            decimal valoresItens = 0;
            List<ImunneVacinas.ItemCompra> listaItens = ImunneVacinas.ItemCompra.ConsultarItens(pesquisaItens);
            foreach (ImunneVacinas.ItemCompra item in listaItens)
            {
                valoresItens += item.Valor;
            }

            totalValorCompra = valoresItens;

            #endregion

            #region TRANSAÇÃO

            string valorTransacao = ImunneVacinas.Utils.RetornaValorJson(totalValorCompra.ToString());
            int valorOficial = Convert.ToInt32(valorTransacao);

            ImunneVacinas.TransacaoRealizada novaTransacao = ImunneVacinas.TransacaoRealizada.ConsultarUnicoCompra(compraSelecionada.Codigo);
            if (novaTransacao != null)
                novaTransacao.Excluir();

            Random generator = new Random();
            String r = generator.Next(0, 999999).ToString("D6");

            novaTransacao = new ImunneVacinas.TransacaoRealizada();
            novaTransacao.autorizacao = "IMUNNEVACINAS";
            novaTransacao.codigoEstabelecimento = "1550561256295";
            novaTransacao.compraTransacao.Codigo = compraSelecionada.Codigo;
            novaTransacao.valor = valorOficial;
            novaTransacao.statusTransacao = 1;
            novaTransacao.numeroComprovanteVenda = "TESTES" + ImunneVacinas.Utils.GerarIdRandomica(10);
            novaTransacao.numeroTransacao = Convert.ToInt32(r);
            novaTransacao.mensagemVenda = "COMPRA REALIZADA EM TESTES";
            int codigoTransacao = novaTransacao.Incluir();

            #endregion

            #region ATUALIZANDO A COMPRA COM O VALOR FINAL

            //compraSelecionada.Valor = totalValorCompra;
            compraSelecionada.Alterar();

            #endregion

            return novaTransacao;
        }
        else return null;
    }

    private bool VerificaTransacaoCartao(string tipoPagamento){
        return (tipoPagamento == "cartao");
    }

    private void PagamentoCartao (){
        #region LEITURA DE DADOS DE TELA E PARAMETROS

        string storeCode = System.Configuration.ConfigurationManager.AppSettings.Get("STORECODESANDBOX");
        int codigoCampanha = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["campanha"]));
        int codigoCompra = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["compra"]));
        int codigoParticipante = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["participante"]));
        string tipoPagamento = ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["tipoPagamento"]);

        ImunneVacinas.Compra compraSelecionada = ImunneVacinas.Compra.ConsultarUnico(codigoCompra);

        StringBuilder urlPagina;

        ImunneVacinas.Participante participanteSelecionado = ImunneVacinas.Participante.ConsultarUnico(codigoParticipante);
        ImunneVacinas.Campanha campanhaSelecionada = ImunneVacinas.Campanha.ConsultarUnico(codigoCampanha);

        #endregion
        try
        {
            #region COMPRA CONCLUIDA

            if (LblSituacao.Text == "CONCLUIDA")
            {
                urlPagina = new StringBuilder();

                urlPagina.AppendFormat("~/vouchers.aspx?campanha={0}", Request.QueryString["campanha"]);
                urlPagina.AppendFormat("&clinica={0}", Request.QueryString["clinica"]);
                urlPagina.AppendFormat("&participante={0}", Request.QueryString["participante"]);
                urlPagina.AppendFormat("&itens={0}", Request.QueryString["itens"]);
                urlPagina.AppendFormat("&qtde={0}", Request.QueryString["qtde"]);
                urlPagina.AppendFormat("&compra={0}", Request.QueryString["compra"]);
                urlPagina.AppendFormat("&membros={0}", Request.QueryString["membros"]);

                Response.Redirect(urlPagina.ToString(), false);
                Context.ApplicationInstance.CompleteRequest();
            }

            #endregion

            #region VALIDAÇÃO GETNET

            // VALIDANDO AS INFORMAÇÕES DO CARTÃO DE CRÉDITO DO USUÁRIO QUE REALIZOU A COMPRA
            // A MESMA ESTÁ 'CONCLUÍDA', O QUE SIGNIFICA QUE AS ETAPAS DE ESCOLHA DE PRODUTOS,
            // CLÍNICA, BENEFICIÁRIOS E ETC. FOI REALIZADA COM TOTAL SUCESSO.

            // CASO O CARTÃO NÃO ESTEJA HABILIDADE NA PLATAFORMA YAPAY, A MENSAGEM DE TELA IRÁ INFORMAR AO USUÁRIO

            int formaPagamento = Convert.ToInt32(ImunneVacinas.Cartao.RetornarOperadoraGetNet(TxtNumeroCartao.Text));
            if (formaPagamento <= 0)
            {
                ImunneVacinas.Alerta.showMensagemAlerta(this.Page, "Aviso", "A bandeira de seu cartão ainda não está habilitada em nossa plataforma. No momento trabalhamos com as bandeiras contidas na imagem abaixo.");
                return;
            }

            #endregion

            #region INTEGRAÇÃO COM O GATWEAY

            // TRATAMENTO DOS DADOS DA COMPRA PARA CRIAÇÃO DA INTEGRAÇÃO COM A PLATAFORMA DE PAGAMENTOS

            string valorTransacao = ImunneVacinas.Utils.RetornaValorJson(LblValor.Text);
            int valorOficial = Convert.ToInt32(valorTransacao);

            // CRIANDO NOVA INTEGRAÇÃO
            List<ImunneVacinas.Contact> contacts = new List<ImunneVacinas.Contact>();
            List<ImunneVacinas.Address> addresses = new List<ImunneVacinas.Address>();
            contacts.Add(new ImunneVacinas.Contact()
            {
                number_contact = participanteSelecionado.Telefone
            });
            addresses.Add(new ImunneVacinas.Address()
            {
                type_address = "B",
                postal_code = "17000-000",
                street = "Av Esmeralda",
                number = "1001",
                completion = "A",
                neighborhood = "Jd Esmeralda",
                city = "Marilia",
                state = "SP"
            });
            ImunneVacinas.Integracao novaIntegracao = new ImunneVacinas.Integracao()
            {
                token_account = storeCode,
                customer = new ImunneVacinas.Customer()
                {
                    name = participanteSelecionado.Nome,
                    birth_date = participanteSelecionado.DataNascimento.ToString().Split(' ')[0],
                    cpf = participanteSelecionado.Cpf,
                    email = participanteSelecionado.Email,
                    contacts = contacts,
                    addresses = addresses
                },
                payment = new ImunneVacinas.Payment()
                {
                    payment_method_id = "3",
                    card_name = TxtNomeTitular.Text.ToUpper(),
                    card_number = TxtNumeroCartao.Text,
                    card_cvv = TxtCVV.Text,
                    card_expdate_month = DdlMesValidade.SelectedValue,
                    card_expdate_year = DdlAnoValidade.SelectedValue,
                    split = !String.IsNullOrEmpty(DdlParcelas.Text) ? DdlParcelas.Text : "1"
                }
            };

            // PESQUISANDO ITENS DA COMPRA
            ImunneVacinas.PesquisaItemCompra pesquisa = new ImunneVacinas.PesquisaItemCompra()
            {
                Compra = codigoCompra
            };

            List<ImunneVacinas.ItemCompra> listaItensCompra = ImunneVacinas.ItemCompra.ConsultarItens(pesquisa);
            List<ImunneVacinas.TransactionProduct> itensDoPedido = new List<ImunneVacinas.TransactionProduct>();

            // ADICIOANDO ITENS DA COMPRA
            foreach (ImunneVacinas.ItemCompra compra in listaItensCompra)
            {
                ImunneVacinas.ProdutoCampanha produto = ImunneVacinas.ProdutoCampanha.ConsultarUnico(codigoCampanha, compra.IdVacina.Codigo);
                ImunneVacinas.TransactionProduct item = new ImunneVacinas.TransactionProduct()
                {
                    extra = "Vacinas",
                    quantity = compra.Quantidade.ToString(),
                    code = produto.Codigo.ToString(),
                    description = compra.IdVacina.Nome,
                    price_unit = produto.Preco.ToString().Replace(',', '.')
                };
                itensDoPedido.Add(item);
            }
            novaIntegracao.transaction_product = itensDoPedido;

            /*
            novaIntegracao.dadosCobranca = new ImunneVacinas.DadosCobranca()
            {
                nome = TxtNomeTitular.Text.ToUpper(),
                documento = ImunneVacinas.Utils.LimparCpfCnpj(participanteSelecionado.Cpf)
            };
            */

            //novaIntegracao.dadosEntrega = new ImunneVacinas.DadosEntrega();

            #endregion

            #region REALIZANDO A TRANSAÇÃO

            // ROTINA DE COMPRAS OFICIAL - DEIXAR COMENTADO SEMPRE QUE ESTIVER EM ESTES - VALIDAÇÃO JÁ CONFIRMADA
            // VALIDANDO DADOS DA TRANSAÇÃO NA PLATAFORMA YAPAY
            //ImunneVacinas.TransacaoRealizada transacaoRealizada = ImunneVacinas.Pagamento.ValidarPagamento(novaIntegracao);
            ImunneVacinas.TransacaoRealizada transacaoRealizada = new ImunneVacinas.TransacaoRealizada();

            // GERAÇÃO DE COMPRA 'FAKE' PARA TESTES DO PROCESSO, SEM A NECESSIDADE GERAÇÃO DE VALORES
            // DEIXAR COMENTADO SEMPRE QUE FOR SUBIR AO OFICIAL
            //transacaoRealizada = GerarTransacaoTestes(compraSelecionada);
            transacaoRealizada = ImunneVacinas.Pagamento.ValidarPagamento(novaIntegracao, codigoCompra);

            // CONFIRMANDO A REALIZAÇÃO DA TRANSAÇÃO
            if (transacaoRealizada != null)
            {
                transacaoRealizada.compraTransacao.Codigo = codigoCompra;
                transacaoRealizada.numeroCartao = TxtNumeroCartao.Text;

                bool resultado = false;

                try
                {
                    resultado = transacaoRealizada.ExisteBD();
                }
                catch (Exception ex)
                {
                    resultado = false;
                }

                if (resultado == true)
                    transacaoRealizada.Alterar();
                else transacaoRealizada.Incluir();

                // TRANSAÇÕES JÁ APROVADAS/PAGAS
                // SE SEGUE O FLUXO DE DIRECIONAMENTO DAS TELAS DE AGENDAMENTO OU FINALIZAÇÃO,
                // DE ACORDO COM TIPO DA CAMPANHA (SURTO OU REGULAR)
                if (transacaoRealizada.codigoTransacaoOperadora == "00" || transacaoRealizada.codigoTransacaoOperadora == "12321")
                {
                    compraSelecionada.Situacao = ImunneVacinas.Compra.SituacaoCompra.Concluida;
                    compraSelecionada.Parcelas = Convert.ToInt32(DdlParcelas.SelectedValue);
                    compraSelecionada.Alterar();

                    if (campanhaSelecionada.Surto == 0)
                    {
                        // CAMPANHAS DO TIPO 'REGULAR' SÃO DIRECIONADAS PARA A FINALIZAÇÃO,
                        // ONDE SÃO VISUALIZADOS OS BENEFICIÁRIOS. AS APLICAÇÕES DESTAS COMPRAS
                        // SÃO INFORMADAS, POSTERIORMENTE, PELA CLÍNICA, PODEMOS O USUÁRIO IMPRIMIR
                        // UM COMPROVANTE DAS AQUISIÇÕES.

                        urlPagina = new StringBuilder();

                        urlPagina.AppendFormat("~/vouchers.aspx?campanha={0}", Request.QueryString["campanha"]);
                        urlPagina.AppendFormat("&clinica={0}", Request.QueryString["clinica"]);
                        urlPagina.AppendFormat("&participante={0}", Request.QueryString["participante"]);
                        urlPagina.AppendFormat("&itens={0}", Request.QueryString["itens"]);
                        urlPagina.AppendFormat("&qtde={0}", Request.QueryString["qtde"]);
                        urlPagina.AppendFormat("&compra={0}", Request.QueryString["compra"]);
                        urlPagina.AppendFormat("&membros={0}", Request.QueryString["membros"]);
                    }
                    else
                    {
                        // CAMPANHAS DO TIPO 'SURTO' SÃO DIRECIONADAS PARA A TELA DE AGENDAMENTO,
                        // ONDE O USUÁRIO PODE ESCOLHER OS HORÁRIOS PARA APLICAÇÃO DOS ITENS ADQUIRIDOS.
                        // ELE TAMBÉM PODE SAIR SEM AGENDAMENTO, FICANDO A CRITÉRIO DA CLÍNICA TAL ESCOLHA.

                        urlPagina = new StringBuilder();

                        urlPagina.AppendFormat("~/horarios.aspx?campanha={0}", Request.QueryString["campanha"]);
                        urlPagina.AppendFormat("&clinica={0}", Request.QueryString["clinica"]);
                        urlPagina.AppendFormat("&participante={0}", Request.QueryString["participante"]);
                        urlPagina.AppendFormat("&itens={0}", Request.QueryString["itens"]);
                        urlPagina.AppendFormat("&qtde={0}", Request.QueryString["qtde"]);
                        urlPagina.AppendFormat("&compra={0}", Request.QueryString["compra"]);
                        urlPagina.AppendFormat("&membros={0}", Request.QueryString["membros"]);
                    }

                    Response.Redirect(urlPagina.ToString(), false);
                    Context.ApplicationInstance.CompleteRequest();
                }
                else
                {
                    // AVISO E ATUALIZAÇÃO DE DADOS DE COMPRAS NÃO AUTORIZADAS PELA PLATAFORMA DE PGTOS

                    compraSelecionada.Situacao = ImunneVacinas.Compra.SituacaoCompra.Cancelada;
                    compraSelecionada.Parcelas = Convert.ToInt32(DdlParcelas.SelectedValue);
                    compraSelecionada.Alterar();
                    ImunneVacinas.Alerta.showMensagemAlerta(this.Page, "Aviso", $"A compra não foi autorizada. {transacaoRealizada.mensagemVenda}");
                }
            }
            else
            {
                // AVISO E ATUALIZAÇÃO DE DADOS DE COMPRAS NÃO AUTORIZADAS PELA PLATAFORMA DE PGTOS

                ImunneVacinas.Alerta.showMensagemAlerta(this.Page, "Aviso", "A compra não foi autorizada. Verifique os dados de seu cartão e tente novamente!");
            }

            #endregion
        }
        catch (Exception ex)
        {
            // AVISO E TRATATIVAS DE ERROS GERAIS DA TELA

            ImunneVacinas.Alerta.showMensagemAlerta(this.Page, "Aviso", "Não foi possível estabelecer a conexão no momento, tente novamente!");
            ImunneVacinas.Erro erro = new ImunneVacinas.Erro();
            erro.addArquivo(ex);
        }
    }

    // 
    private void PagamentoPix (){
        #region LEITURA DE DADOS DE TELA E PARAMETROS

        string storeCode = System.Configuration.ConfigurationManager.AppSettings.Get("STORECODE");
        int codigoCampanha = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["campanha"]));
        int codigoCompra = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["compra"]));
        int codigoParticipante = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["participante"]));
        string tipoPagamento = ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["tipoPagamento"]);

        ImunneVacinas.Compra compraSelecionada = ImunneVacinas.Compra.ConsultarUnico(codigoCompra);

        StringBuilder urlPagina;

        ImunneVacinas.Participante participanteSelecionado = ImunneVacinas.Participante.ConsultarUnico(codigoParticipante);
        ImunneVacinas.Campanha campanhaSelecionada = ImunneVacinas.Campanha.ConsultarUnico(codigoCampanha);

        #endregion

        try
        {
            #region COMPRA CONCLUIDA

            if (LblSituacao.Text == "CONCLUIDA")
            {
                urlPagina = new StringBuilder();

                urlPagina.AppendFormat("~/vouchers.aspx?campanha={0}", Request.QueryString["campanha"]);
                urlPagina.AppendFormat("&clinica={0}", Request.QueryString["clinica"]);
                urlPagina.AppendFormat("&participante={0}", Request.QueryString["participante"]);
                urlPagina.AppendFormat("&itens={0}", Request.QueryString["itens"]);
                urlPagina.AppendFormat("&qtde={0}", Request.QueryString["qtde"]);
                urlPagina.AppendFormat("&compra={0}", Request.QueryString["compra"]);
                urlPagina.AppendFormat("&membros={0}", Request.QueryString["membros"]);

                Response.Redirect(urlPagina.ToString(), false);
                Context.ApplicationInstance.CompleteRequest();
            }

            #endregion

            #region INTEGRAÇÃO COM O GATWEAY

            // TRATAMENTO DOS DADOS DA COMPRA PARA CRIAÇÃO DA INTEGRAÇÃO COM A PLATAFORMA DE PAGAMENTOS

            string valorTransacao = ImunneVacinas.Utils.RetornaValorJson(LblValor.Text);
            int valorOficial = Convert.ToInt32(valorTransacao);

            // CRIANDO NOVA INTEGRAÇÃO
            List<ImunneVacinas.Contact> contacts = new List<ImunneVacinas.Contact>();
            List<ImunneVacinas.Address> addresses = new List<ImunneVacinas.Address>();
            contacts.Add(new ImunneVacinas.Contact()
            {
                number_contact = participanteSelecionado.Telefone
            });
            addresses.Add(new ImunneVacinas.Address()
            {
                type_address = "B",
                postal_code = "17000-000",
                street = "Av Esmeralda",
                number = "1001",
                completion = "A",
                neighborhood = "Jd Esmeralda",
                city = "Marilia",
                state = "SP"
            });
            ImunneVacinas.Integracao novaIntegracao = new ImunneVacinas.Integracao()
            {
                token_account = storeCode,
                customer = new ImunneVacinas.Customer()
                {
                    name = participanteSelecionado.Nome,
                    birth_date = participanteSelecionado.DataNascimento.ToString().Split(' ')[0],
                    cpf = participanteSelecionado.Cpf,
                    email = participanteSelecionado.Email,
                    contacts = contacts,
                    addresses = addresses
                },
                payment = new ImunneVacinas.Payment()
                {
                    payment_method_id = "27",
                    card_name = "",
                    card_number = "",
                    card_cvv = "",
                    card_expdate_month = "",
                    card_expdate_year = "",
                    split = "1"
                }
            };

            // PESQUISANDO ITENS DA COMPRA
            ImunneVacinas.PesquisaItemCompra pesquisa = new ImunneVacinas.PesquisaItemCompra()
            {
                Compra = codigoCompra
            };

            List<ImunneVacinas.ItemCompra> listaItensCompra = ImunneVacinas.ItemCompra.ConsultarItens(pesquisa);
            List<ImunneVacinas.TransactionProduct> itensDoPedido = new List<ImunneVacinas.TransactionProduct>();

            // ADICIOANDO ITENS DA COMPRA
            foreach (ImunneVacinas.ItemCompra compra in listaItensCompra)
            {
                ImunneVacinas.ProdutoCampanha produto = ImunneVacinas.ProdutoCampanha.ConsultarUnico(codigoCampanha, compra.IdVacina.Codigo);
                ImunneVacinas.TransactionProduct item = new ImunneVacinas.TransactionProduct()
                {
                    extra = "Vacinas",
                    quantity = compra.Quantidade.ToString(),
                    code = produto.Codigo.ToString(),
                    description = compra.IdVacina.Nome,
                    price_unit = produto.Preco.ToString().Replace(',', '.')
                };
                itensDoPedido.Add(item);
            }
            novaIntegracao.transaction_product = itensDoPedido;

            #endregion

            #region FAZENDO REQUISIÇÃO AO GATEWAY

            string usuarioAPI = String.Format("{0}", System.Configuration.ConfigurationManager.AppSettings.Get("USUARIO"));
            string senhaAPI = String.Format("{0}", System.Configuration.ConfigurationManager.AppSettings.Get("SENHA"));
            string urlAPI = System.Configuration.ConfigurationManager.AppSettings.Get("URLBASE");
            string endPoint = System.Configuration.ConfigurationManager.AppSettings.Get("ENDPOINT");

            ServicePointManager.ServerCertificateValidationCallback = (sender, cert, chain, sslPolicyErrors) => true;
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(urlAPI + endPoint);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";
            httpWebRequest.Headers.Add(HttpRequestHeader.Authorization, "Basic" + Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(string.Format("{0}:{1}", usuarioAPI, senhaAPI))));

            var jsonContent = JsonConvert.SerializeObject(novaIntegracao, Formatting.Indented);

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = JsonConvert.SerializeObject(novaIntegracao);
                streamWriter.Write(jsonContent);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            var err = httpResponse;
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                PaymentReturn paymentReturn = JsonConvert.DeserializeObject<PaymentReturn>(result);
                // Adiciona o id de transacao para verificação.
                HdnTokenTransaction.Value = $"{paymentReturn.data_response.transaction.token_transaction}";
                HdnMaxTimeTransaction.Value = $"{paymentReturn.data_response.transaction.max_days_to_keep_waiting_payment}";
                // "Gerar" o Qrcode
                ImgQrcode.Src = $"{paymentReturn.data_response.transaction.payment.qrcode_path}";
                // Adicionar local copia e cola
                LblCopiaECola.Text = $"{paymentReturn.data_response.transaction.payment.qrcode_original_path}";
            }
            #endregion
        }
    catch (Exception ex)
        {
            // AVISO E TRATATIVAS DE ERROS GERAIS DA TELA

            ImunneVacinas.Alerta.showMensagemAlerta(this.Page, "Aviso", "Não foi possível estabelecer a conexão no momento, tente novamente!");
            ImunneVacinas.Erro erro = new ImunneVacinas.Erro();
            erro.addArquivo(ex);
        }
        // gerar o pagamento e o qrcode
    }

    private bool VerificaPagamentoPix(string txid){
        return true;
    }
    
    protected void OcultaPagamentoCartao(){
        foreach (Control controle in PnlInformacoesPgto.Controls)
        {
            controle.Visible = false;
        }
    }

    protected void OcultaPagamentoPix(){
        foreach (Control controle in PnlInformacoesPix.Controls)
        {
            controle.Visible = false;
        }
    }
}