﻿/*QUALQUER ALTERAÇÃO DE CÓDIGO FEITA AQUI, DEVE SER ATUALIZADA NO ARQUIVO GUESS.MIN.JS*/

/*----------------------------------------------------------------------------
Formata um campo, chamando o método adequado (ver abaixo)
OnKeyPress="mascara(this,nome_do_metodo)"
//http://brunobrum.wordpress.com/2010/05/20/mascara-javascript-de-cnpj-e-cpf-no-mesmo-campo-do-formulario/
//http: //wbruno.com.br/expressao-regular/diversas-mascaras-com-er/
//http://elcio.com.br/ajax/mascara/
-----------------------------------------------------------------------------*/
function mascara(o, f) {
    v_obj = o
    v_fun = f
    setTimeout("execmascara()", 1)
}

function execmascara() {
    v_obj.value = v_fun(v_obj.value)
}

/*----------------------------------------------------------------------------
Formata um campo para cpf ou cnpj, de acordo com a quantidade de dígitos.
Mascara aplicada: 
adicionar este evento ao textBox:
CPF: OnKeyPress="mascara(this,cpfCnpj)" MaxLength="14"
CNPJ: OnKeyPress="mascara(this,cpfCnpj)" MaxLength="18"
-----------------------------------------------------------------------------*/
function cpfCnpj(v) {

    //Remove tudo o que não é dígito
    v = v.replace(/\D/g, "")

    if (v.length <= 11) { //CPF (Corrigido MP - 2014/03/26

        //Coloca um ponto entre o terceiro e o quarto dígitos
        v = v.replace(/(\d{3})(\d)/, "$1.$2")

        //Coloca um ponto entre o terceiro e o quarto dígitos
        //de novo (para o segundo bloco de números)
        v = v.replace(/(\d{3})(\d)/, "$1.$2")

        //Coloca um hífen entre o terceiro e o quarto dígitos
        v = v.replace(/(\d{3})(\d{1,2})$/, "$1-$2")

    } else { //CNPJ

        //Coloca ponto entre o segundo e o terceiro dígitos
        v = v.replace(/^(\d{2})(\d)/, "$1.$2")

        //Coloca ponto entre o quinto e o sexto dígitos
        v = v.replace(/^(\d{2})\.(\d{3})(\d)/, "$1.$2.$3")

        //Coloca uma barra entre o oitavo e o nono dígitos
        v = v.replace(/\.(\d{3})(\d)/, ".$1/$2")

        //Coloca um hífen depois do bloco de quatro dígitos
        v = v.replace(/(\d{4})(\d)/, "$1-$2")

    }

    return v

}

/*----------------------------------------------------------------------------
Formata um campo para cpf
Mascara aplicada: 
adicionar este evento ao textBox:
CPF: OnKeyPress="mascara(this,cpfCnpj)" MaxLength="14"
-----------------------------------------------------------------------------*/
function cpf(v) {

    //Remove tudo o que não é dígito
    v = v.replace(/\D/g, "")

    
    //Coloca um ponto entre o terceiro e o quarto dígitos
    v = v.replace(/(\d{3})(\d)/, "$1.$2")

    //Coloca um ponto entre o terceiro e o quarto dígitos
    //de novo (para o segundo bloco de números)
    v = v.replace(/(\d{3})(\d)/, "$1.$2")

    //Coloca um hífen entre o terceiro e o quarto dígitos
    v = v.replace(/(\d{3})(\d{1,2})$/, "$1-$2")


    return v

}

/*----------------------------------------------------------------------------
Formata um campo para cnpj
Mascara aplicada: 
adicionar este evento ao textBox:
CNPJ: OnKeyPress="mascara(this,cpfCnpj)" MaxLength="18"
-----------------------------------------------------------------------------*/
function cnpj(v) {

    //Remove tudo o que não é dígito
    v = v.replace(/\D/g, "")


    //Coloca ponto entre o segundo e o terceiro dígitos
    v = v.replace(/^(\d{2})(\d)/, "$1.$2")

    //Coloca ponto entre o quinto e o sexto dígitos
    v = v.replace(/^(\d{2})\.(\d{3})(\d)/, "$1.$2.$3")

    //Coloca uma barra entre o oitavo e o nono dígitos
    v = v.replace(/\.(\d{3})(\d)/, ".$1/$2")

    //Coloca um hífen depois do bloco de quatro dígitos
    v = v.replace(/(\d{4})(\d)/, "$1-$2")    

    return v

}


/*----------------------------------------------------------------------------
Formata um campo de telefone, de acordo com a quantidade de dígitos. 
Mascara aplicada: (##) #####-####
adicionar este evento ao textBox:
OnKeyPress="mascara(this, mtel)"  MaxLength="15"
-----------------------------------------------------------------------------*/

function mtel(v) {
    v = v.replace(/\D/g, "");             //Remove tudo o que não é dígito
    v = v.replace(/^(\d{2})(\d)/g, "($1) $2"); //Coloca parênteses em volta dos dois primeiros dígitos
    v = v.replace(/(\d)(\d{4})$/, "$1-$2");    //Coloca hífen entre o quarto e o quinto dígitos
    return v;
}

/*----------------------------------------------------------------------------
Formata um campo de telefone, de acordo com a quantidade de dígitos. 
Mascara aplicada: #####-####
adicionar este evento ao textBox:
OnKeyPress="mascara(this, mtel2)"  MaxLength="10"
-----------------------------------------------------------------------------*/
function mtel2(v) {
    v = v.replace(/\D/g, "");             //Remove tudo o que não é dígito
    v = v.replace(/(\d)(\d{4})$/, "$1-$2");    //Coloca hífen entre o quarto e o quinto dígitos
    return v;
}

/*----------------------------------------------------------------------------
Formata um campo de telefone, de acordo com a quantidade de dígitos
adicionar este evento ao textBox:
Mascara aplicada: #####-###
OnKeyPress="mascara(this, mcep)" MaxLength="9"
-----------------------------------------------------------------------------*/
function mcep(v) {
    v = v.replace(/\D/g, "")                    //Remove tudo o que não é dígito
    v = v.replace(/^(\d{5})(\d)/, "$1-$2")         //Esse é tão fácil que não merece explicações
    return v
}

/*----------------------------------------------------------------------------
Formata um campo de data
adicionar este evento ao textBox:
Mascara aplicada: ##/##/####
OnKeyPress="mascara(this, mdata)" MaxLength="10"
-----------------------------------------------------------------------------*/
function mdata(v) {
    v = v.replace(/\D/g, "");                    //Remove tudo o que não é dígito
    v = v.replace(/(\d{2})(\d)/, "$1/$2");
    v = v.replace(/(\d{2})(\d)/, "$1/$2");

    v = v.replace(/(\d{2})(\d{2})$/, "$1$2");
    return v;
}

/*----------------------------------------------------------------------------
Formata um campo numerico, de acordo com a quantidade de dígitos
adicionar este evento ao textBox:
OnKeyPress="mascara(this, mnumeros)"
-----------------------------------------------------------------------------*/
function mnumeros(v) {
    v = v.replace(/\D/g, "");       //Remove tudo o que não é dígito
    return v;
}

/*----------------------------------------------------------------------------
Formata um campo decimal, de acordo com a quantidade de dígitos
adicionar este evento ao textBox:
OnKeyPress="mascara(this, mvalor)"
-----------------------------------------------------------------------------*/
function mvalor(v) {
    v = v.replace(/\D/g, ""); //Remove tudo o que não é dígito
    v = v.replace(/(\d)(\d{35})$/, "$1.$2");//colocando os demais pontos
    v = v.replace(/(\d)(\d{32})$/, "$1.$2");
    v = v.replace(/(\d)(\d{29})$/, "$1.$2");
    v = v.replace(/(\d)(\d{26})$/, "$1.$2");
    v = v.replace(/(\d)(\d{23})$/, "$1.$2"); 
    v = v.replace(/(\d)(\d{20})$/, "$1.$2"); 
    v = v.replace(/(\d)(\d{17})$/, "$1.$2"); 
    v = v.replace(/(\d)(\d{14})$/, "$1.$2"); 
    v = v.replace(/(\d)(\d{11})$/, "$1.$2"); 

    v = v.replace(/(\d)(\d{8})$/, "$1.$2"); //coloca o ponto dos milhões
    v = v.replace(/(\d)(\d{5})$/, "$1.$2"); //coloca o ponto dos milhares

    v = v.replace(/(\d)(\d{2})$/, "$1,$2"); //coloca a virgula antes dos 2 últimos dígitos
    return v;
}

/******** NÃO USAR ************/
/*----------------------------------------------------------------------------
Formatação para qualquer mascara
ex.: OnKeyPress="formatar(event, '## ####-####')"
-----------------------------------------------------------------------------*/
function formatar(ev, mask) {
    var elemento;
    if (this.navigator.appName == "Microsoft Internet Explorer") {
        elemento = ev.srcElement;
    }
    else {
        elemento = ev.currentTarget;
    }
    var i = elemento.value.length;
    var saida = mask.substring(0, 1);
    var texto = mask.substring(i);
    if (texto.substring(0, 1) != saida) {
        elemento.value += texto.substring(0, 1);
    }
}
/*----------------------------------------------------------------------------
Formatação para qualquer mascara, permitindo apenas números
ex.: OnKeyPress="return formatarSomenteNumero(event,'## ####-####')"
-----------------------------------------------------------------------------*/
function formatarSomenteNumero(ev, mask) {//function formatarSomenteNumero(src, mask) {
    if (SomenteNumero(ev)) {//if (SomenteNumero(src.event)) {
        formatar(ev, mask); // formatar(src, mask);
        return true;
    }
    else
        return false;
}

/*----------------------------------------------------------------------------
Bloqueia letras em inputs numéricos
adicionar este evento ao textBox:
OnKeyPress="return SomenteNumero(event)"
-----------------------------------------------------------------------------*/
function SomenteNumero(ev) {
    var tecla = (ev.which) ? ev.which : ev.keyCode; // (window.event) ? event.keyCode : e.which;
    if ((tecla > 47 && tecla < 58)) return true;
    else {
        if (tecla == 8 || tecla == 0) return true;
        else return false;
    }
}

//Formata valor
//ex.: onKeyDown='FormataValor("valor", 13, event)
//Obs.: "valor" é o nome do campo, 13 o tamanho máximo permitido de carac. do campo e event é a tecla pressionada'
function FormataValor(obj, tammax, teclapres) {
    var tecla = teclapres.keyCode;
    vr = obj.value;
    vr = vr.replace("/", "");
    vr = vr.replace("/", "");
    vr = vr.replace(",", "");
    vr = vr.replace(",", "");
    vr = vr.replace(".", "");
    vr = vr.replace(".", "");
    vr = vr.replace(".", "");
    vr = vr.replace(".", "");
    //Replaces adicionais
    //vr = vr.replace( "-", "" );
    //vr = vr.replace( "+", "" );
    //vr = vr.replace( "*", "" );
    tam = vr.length;

    if (tam < tammax && tecla != 8) { tam = vr.length + 1; }

    if (tecla == 8) { tam = tam - 1; }

    if (tecla == 8 || tecla >= 48 && tecla <= 57 || tecla >= 96 && tecla <= 105) {
        if (tam <= 2) {
            obj.value = vr;
        }
        if ((tam > 2) && (tam <= 5)) {
            obj.value = vr.substr(0, tam - 2) + ',' + vr.substr(tam - 2, tam);
        }
        if ((tam >= 6) && (tam <= 8)) {
            obj.value = vr.substr(0, tam - 5) + '.' + vr.substr(tam - 5, 3) + ',' + vr.substr(tam - 2, tam);
        }
        if ((tam >= 9) && (tam <= 11)) {
            obj.value = vr.substr(0, tam - 8) + '.' + vr.substr(tam - 8, 3) + '.' + vr.substr(tam - 5, 3) + ',' + vr.substr(tam - 2, tam);
        }
        if ((tam >= 12) && (tam <= 14)) {
            obj.value = vr.substr(0, tam - 11) + '.' + vr.substr(tam - 11, 3) + '.' + vr.substr(tam - 8, 3) + '.' + vr.substr(tam - 5, 3) + ',' + vr.substr(tam - 2, tam);
        }
        if ((tam >= 15) && (tam <= 17)) {
            obj.value = vr.substr(0, tam - 14) + '.' + vr.substr(tam - 14, 3) + '.' + vr.substr(tam - 11, 3) + '.' + vr.substr(tam - 8, 3) + '.' + vr.substr(tam - 5, 3) + ',' + vr.substr(tam - 2, tam);
        }
    }
}

/*----------------------------------------------------------------------------
Formata um campo de telefone, de acordo com a quantidade de dígitos. 
Mascara aplicada: (##) #####-####
adicionar este evento ao textBox:
OnKeyPress="mascara(this, mhora)"  MaxLength="6"
-----------------------------------------------------------------------------*/

function mhora(v) {
    v = v.replace(/\D/g, "");             //Remove tudo o que não é dígito
    v = v.replace(/(\d)(\d{2})$/, "$1:$2");    //Coloca hífen entre o quarto e o quinto dígitos
    return v;
}