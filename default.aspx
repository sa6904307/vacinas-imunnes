﻿<%@ Page Title="S&A Imunizações - Campanhas" Language="C#" MasterPageFile="~/imunne.master" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="imunne_default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="dv-spc-20"></div>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 caixa-central col-centered">
                <div class="caixa-branca">
                    <div class="step-header">
                        BOAS VINDAS!
                    </div>
                    <div class="box-inside">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                            <p>
                                Esta é a nossa plataforma para aquisição de
                                doses de vacinas por campanhas. Tenha sempre
                                em mãos o número da campanha a ser consultada.
                            </p>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                            <asp:HyperLink ID="LnkInicio" runat="server" CssClass="btn btn-block btn-primary" ToolTip="Iniciar a aquisição de vacinas" NavigateUrl="~/campanha.aspx">
                            <i class="fa fa-play"></i>&nbsp;Iniciar
                            </asp:HyperLink>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
