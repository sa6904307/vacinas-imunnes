﻿<%@ Page Title="" Language="C#" MasterPageFile="~/imunne.master" AutoEventWireup="true" CodeFile="produtos.aspx.cs" Inherits="imunne_produtos" MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        function validar() {
            // Qtde
            if (document.getElementById("<%=DdlQuantidades.ClientID%>").value == "-1") {
                MensagemGenerica("Aviso!", "Por favor, informe a quantidade de compras a realizar.", "warning");
                return false;
            }
        };
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section class="subheader">
        <div class="container">
            <div class="row">
                <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                    <i class="fa fa-list"></i>&nbsp;Produtos da Campanha
                </div>
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 text-right">
                    <a href="javascript:history.back()" title="Voltar à página anterior">
                        <i class="fa fa-chevron-left"></i>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <div class="container">
        <div class="row">
            <asp:UpdatePanel ID="UpdProdutos" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
                        <div class="caixa-branca">
                            <div class="step-header">
                                DADOS DO ADQUIRENTE
                            </div>
                            <div class="box-inside">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                    <label>Nome</label>
                                    <asp:Label ID="LblNome" runat="server" CssClass="label-identificacao"></asp:Label>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 form-group">
                                    <label>CPF</label>
                                    <asp:Label ID="LblCPF" runat="server" CssClass="label-identificacao"></asp:Label>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 form-group">
                                    <label>Nascimento</label>
                                    <asp:Label ID="LblNascimento" runat="server" CssClass="label-identificacao"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="caixa-branca">
                            <div class="step-header">
                                INFORMAÇÕES DA CAMPANHA
                            </div>
                            <div class="box-inside">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                    <label>CAMPANHA</label>
                                    <asp:Label ID="LblCampanha" runat="server" CssClass="label-identificacao"></asp:Label>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                    <label>CLÍNICA</label>
                                    <asp:Label ID="LblClinica" runat="server" CssClass="label-identificacao"></asp:Label>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                    <label>EMPRESA</label>
                                    <asp:Label ID="LblEmpresa" runat="server" CssClass="label-identificacao"></asp:Label>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                    <label>VALIDADE</label>
                                    <asp:Label ID="LblValidade" runat="server" CssClass="label-identificacao"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                        <div class="caixa-branca">
                            <div class="step-header">
                                PRODUTOS DA CAMPANHA
                            </div>
                            <div class="box-inside">
                                <div class="box-grids" style="overflow-x: visible;">
                                    <div class="dv-grid-f-header">
                                        <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="display: inline-block; float: none;">
                                            AÇÕES
                                        </div>
                                        <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-right" style="display: inline-block; float: none;">
                                            Preço
                                        </div>
                                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="display: inline-block; float: none;">
                                            Produto
                                        </div>
                                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="display: inline-block; float: none;">
                                            Campanha
                                        </div>
                                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="display: inline-block; float: none;">
                                            Clínica
                                        </div>
                                    </div>
                                    <div class="dv-grid-f">
                                        <asp:Repeater ID="RptPrincipal" runat="server" OnItemDataBound="RptPrincipal_ItemDataBound">
                                            <ItemTemplate>
                                                <div id="dvItem" runat="server">
                                                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-left" style="display: inline-block; float: none;">
                                                        <div style="margin-top: -42px; position: absolute;">
                                                            <asp:CheckBox ID="ChkSelecionar" runat="server" CssClass="checkbox" Text="Selecionar" Width="200px" />
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-right" style="display: inline-block; float: none;">
                                                        <div class="truncate">
                                                            <asp:HiddenField ID="HdfCodigo" runat="server" />
                                                            <asp:Label ID="LblPreco" runat="server" CssClass="text-uppercase"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="display: inline-block; float: none;">
                                                        <div class="truncate">
                                                            <asp:Label ID="LblNomeProduto" runat="server" CssClass="text-uppercase"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="display: inline-block; float: none;">
                                                        <div class="truncate">
                                                            <asp:Label ID="LblNomeCampanha" runat="server" CssClass="text-uppercase"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="display: inline-block; float: none;">
                                                        <div class="truncate">
                                                            <asp:Label ID="LblNomeClinica" runat="server" CssClass="text-uppercase"></asp:Label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>
                            </div>
                            <div class="dv-spc-10"></div>
                            <div class="box-inside">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="float: none; margin: 0 auto !important;">
                                    <label>QUANTIDADE DE DEPENDENTES</label>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 text-center" style="float: none; margin: 0 auto !important;">
                                    <asp:DropDownList ID="DdlQuantidades" runat="server" CssClass="form-control text-center"></asp:DropDownList><br />
                                    <div class="dv-spc-10"></div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="float: none; margin: 0 auto !important;">
                                    <asp:LinkButton ID="LnkContinuar" runat="server" CssClass="btn btn-block btn-primary" OnClick="LnkContinuar_Click" OnClientClick="return validar();" CausesValidation="false">
                            <i class="fa fa-chevron-right"></i>&nbsp;
                            Continuar
                                    </asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="LnkContinuar" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>