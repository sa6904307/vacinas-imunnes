﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class horarios : System.Web.UI.Page
{
    #region VARIAVEIS

    int totalIndividual = 0;
    decimal larguraItem = 0;

    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = "S&A Imunizações | Escolha de Horário";

        if (!IsPostBack)
            IniciarPagina();
    }

    /// <summary>
    /// Método acionado no carregamento inicial da página. São lidas as informações dos parametros da URL
    /// e populados todos os dados para o usuário seguir com sua aquisição
    /// </summary>
    private void IniciarPagina()
    {
        // AVISO PARA A FORMA DE SELEÇÃO DE AGENDAMENTOS
        ImunneVacinas.Alerta.showMensagemAviso(this.Page, "Aviso!", "Você pode utilizar duas formas de seleção de horários. AUTOMATICAMENTE, onde o sistema seleciona as datas sequencialmente após a seleção do primeiro horário e disponibilidade ou INDIVIDUALMENTE, onde o usuário escolhe, beneficiário por beneficiário, clicando sobre o horário desejado.");

        try
        {
            if (String.IsNullOrEmpty(Request.QueryString["campanha"]))
            {
                Response.Redirect("~/", false);
                Context.ApplicationInstance.CompleteRequest();
            }

            #region LENDO INFORMAÇÕES DA URL E POPULANDO DADOS DE TELA

            int codigoParticipante = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["participante"]));
            int codigoCompra = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["compra"]));
            int codigoCampanha = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["campanha"]));
            int codigoClinica = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["clinica"]));
            int qtde = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["qtde"]));
            string[] membros = new string[1];
            string[] itens = new string[1];

            ImunneVacinas.Participante participanteSelecionado = ImunneVacinas.Participante.ConsultarUnico(codigoParticipante);
            ImunneVacinas.Campanha campanhaSelecionada = ImunneVacinas.Campanha.ConsultarUnico(codigoCampanha);
            ImunneVacinas.Compra compraSelecionada = ImunneVacinas.Compra.ConsultarUnico(codigoCompra);
            ImunneVacinas.Clinica clinicaSelecionada = ImunneVacinas.Clinica.ConsultarUnico(codigoClinica);

            LblNascimento.Text = participanteSelecionado.DataNascimento.ToString("dd/MM/yyyy");
            LblNome.Text = participanteSelecionado.Nome.ToUpper();
            LblCPF.Text = ImunneVacinas.Utils.FormatarCpfCnpj(participanteSelecionado.Cpf);

            LblValor.Text = ImunneVacinas.Utils.ToMoeda(compraSelecionada.Valor);

            LblCampanha.Text = campanhaSelecionada.Identificacao.ToUpper() + " | " + campanhaSelecionada.Nome.ToUpper();
            LblValidade.Text = campanhaSelecionada.DataLimite.ToString("dd/MM/yyyy");
            LblClinica.Text = clinicaSelecionada.NomeFantasia.ToUpper() + "<br />"
                            + clinicaSelecionada.Endereco.ToUpper() + ", "
                            + clinicaSelecionada.Numero + " - "
                            + clinicaSelecionada.IdCidade.Nome.ToUpper() + "/"
                            + clinicaSelecionada.IdUf.Sigla;
            LblEmpresa.Text = campanhaSelecionada.IdEmpresa.NomeFantasia.ToUpper();

            LblSituacao.Text = compraSelecionada.Situacao.ToString().ToUpper();
            LblDataCompra.Text = compraSelecionada.DataCompra.ToString("dd/MM/yyyy");

            if (!String.IsNullOrEmpty(Request.QueryString["membros"]))
                membros = ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["membros"]).Split('|');

            if (!String.IsNullOrEmpty(Request.QueryString["itens"]))
                itens = ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["itens"]).Split('|');

            ImunneVacinas.Campanha campanha = ImunneVacinas.Campanha.ConsultarUnico(codigoCampanha);

            #endregion

            #region DIAS DE VIGÊNCIA DA CAMPANHA - PARA CRIAÇÃO DO CALENDÁRIO

            for (DateTime diaInicio = campanha.DataAbertura; diaInicio <= campanha.DataLimite; diaInicio.AddDays(1))
            {
                DdlDias.Items.Add(new ListItem(diaInicio.ToString("dd/MM/yyyy"), diaInicio.ToString("dd/MM/yyyy")));
                diaInicio = diaInicio.AddDays(1);
            }

            #endregion

            #region OPÇÕES DE HORÁRIOS

            DdlOpcoesHorarios.Items.Add(new ListItem("SELECIONE", "-1"));
            DdlOpcoesHorarios.Items.Add(new ListItem("ESCOLHER AUTOMATICAMENTE", "1"));
            DdlOpcoesHorarios.Items.Add(new ListItem("ESCOLHER INDIVIDUALMENTE", "2"));

            ImunneVacinas.PesquisaHorarioSurto pesquisaTotaisSurtos = new ImunneVacinas.PesquisaHorarioSurto()
            {
                Compra = codigoCompra,
                Situacao = ImunneVacinas.Utils.SituacaoHorario.Confirmado
            };
            int totalCompras = ImunneVacinas.HorarioSurto.ContabilizarHorarios(pesquisaTotaisSurtos);
            if (totalCompras > 0)
            {
                LnkSemAgenda.Enabled = false;
                LnkSemAgenda.Style.Add("opacity", ".5");

                if (totalCompras >= membros.Length)
                {
                    ImunneVacinas.Alerta.showMensagemAviso(this.Page, "Aviso!", "Esta compra já realizou todos seus agendamentos. Verifique seu e-mail ou entre em contato com a clínica para verificação.");
                    DdlOpcoesHorarios.Enabled = false;
                }
                else if (totalCompras > 0 && totalCompras < membros.Length)
                {
                    DdlOpcoesHorarios.SelectedValue = "2";
                    HdfOpcoesHorarios.Value = "2";

                    PnlBeneficiarios.Visible = true;
                    PnlHorarios.Visible = true;
                    PnlDatasPesquisa.Visible = true;
                }
            }

            LblTotalEscolhidos.Text = totalCompras.ToString().PadLeft(2, '0');

            #endregion

            ConsultarRegistros();
        }
        catch (Exception ex)
        {
            Response.Redirect("~/", false);
            Context.ApplicationInstance.CompleteRequest();
        }
    }

    /// <summary>
    /// Método que realiza a consulta de registros a serem populados em tela para escolha dos agendamentos
    /// </summary>
    private void ConsultarRegistros()
    {
        #region DADOS DA URL

        int codigoParticipante = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["participante"]));
        int codigoCompra = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["compra"]));
        int codigoCampanha = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["campanha"]));
        int codigoClinica = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["clinica"]));
        int qtde = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["qtde"]));

        string[] membros = new string[1];
        string[] itens = new string[1];

        if (!String.IsNullOrEmpty(Request.QueryString["membros"]))
            membros = ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["membros"]).Split('|');

        if (!String.IsNullOrEmpty(Request.QueryString["itens"]))
            itens = ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["itens"]).Split('|');

        LblTotalBeneficiarios.Text = membros.Length.ToString().PadLeft(2, '0');

        ImunneVacinas.Participante participanteSelecionado = ImunneVacinas.Participante.ConsultarUnico(codigoParticipante);
        ImunneVacinas.Campanha campanhaSelecionada = ImunneVacinas.Campanha.ConsultarUnico(codigoCampanha);
        ImunneVacinas.Clinica clinicaSelecionada = ImunneVacinas.Clinica.ConsultarUnico(codigoClinica);

        #endregion

        #region HORÁRIOS DOS SURTOS

        ImunneVacinas.PesquisaHorarioSurto pesquisa = new ImunneVacinas.PesquisaHorarioSurto()
        {
            Dia = Convert.ToDateTime(DdlDias.SelectedValue),
            Campanha = campanhaSelecionada.Codigo,
            Ordenacao = ImunneVacinas.Utils.TipoOrdenacao.porData
        };

        larguraItem = 0;

        DateTime dataAcao = Convert.ToDateTime(DdlDias.SelectedValue);
        int totalHora = campanhaSelecionada.TotalAtendimentos;

        List<ImunneVacinas.HorarioSurto> horariosCampanha = ImunneVacinas.HorarioSurto.ConsultarHorarios(pesquisa);
        larguraItem = Convert.ToDecimal(100 / Convert.ToDecimal(totalHora));

        DtlHorarios.DataSource = horariosCampanha;
        DtlHorarios.DataBind();

        #endregion

        #region CRIANDO VOUCHERS

        foreach (string membro in membros)
        {
            ImunneVacinas.Participante membroSelecionado = ImunneVacinas.Participante.ConsultarUnico(Convert.ToInt32(membro));

            StringBuilder descricaoVoucher = new StringBuilder();
            descricaoVoucher.AppendFormat("{0} ADQUIRIU PRODUTOS DA CAMPANHA {1}", membroSelecionado.Nome.ToUpper(), campanhaSelecionada.Identificacao.ToUpper().ToString());
            descricaoVoucher.AppendFormat(" QUE SERÃO APLICADOS NA CLÍNICA {0}.\r\nABAIXO OS PRODUTOS:\r\n\r\n", clinicaSelecionada.NomeFantasia.ToUpper());

            foreach (string item in itens)
            {
                ImunneVacinas.ProdutoCampanha produtoSelecionado = ImunneVacinas.ProdutoCampanha.ConsultarUnico(Convert.ToInt32(item));
                descricaoVoucher.AppendFormat("1 UNIDADE(S) DE {0}.\r\n", produtoSelecionado.IdVacina.Nome.ToUpper());
            }

            descricaoVoucher.AppendFormat("\r\nCOMPRAS REALIZADAS POR {0}.", participanteSelecionado.Nome.ToUpper());

            ImunneVacinas.Voucher novoVoucher = new ImunneVacinas.Voucher();
            novoVoucher.IdCampanha.Codigo = codigoCampanha;
            novoVoucher.IdClinica.Codigo = codigoClinica;
            novoVoucher.IdCompra.Codigo = codigoCompra;
            novoVoucher.IdParticipante.Codigo = membroSelecionado.Codigo;
            novoVoucher.DataCriacao = DateTime.Now;
            novoVoucher.Descricao = descricaoVoucher.ToString();
            novoVoucher.Situacao = ImunneVacinas.Voucher.SituacaoVoucher.Gerado;

            try
            {
                // TESTAR JÁ EXISTÊNCIA
                ImunneVacinas.Voucher voucherExiste = ImunneVacinas.Voucher.ConsultarUnico(membroSelecionado.Codigo, codigoClinica, codigoCampanha, codigoCompra);
                if (voucherExiste == null)
                    novoVoucher.Incluir();
            }
            catch (Exception ex)
            {
                ImunneVacinas.Erro erro = new ImunneVacinas.Erro();
                erro.AdicionarLog(ex);
            }
        }

        #endregion

        #region DADOS DE BENEFICIÁRIOS

        ImunneVacinas.PesquisaVoucher pesquisaVouchers = new ImunneVacinas.PesquisaVoucher()
        {
            Compra = codigoCompra
        };

        DdlBeneficiarios.Items.Clear();
        List<ImunneVacinas.Voucher> listaVouchers = ImunneVacinas.Voucher.ConsultarVouchers(pesquisaVouchers);
        if (listaVouchers != null)
        {
            if (listaVouchers.Count > 1)
            {
                DdlBeneficiarios.Items.Add(new ListItem("SELECIONE", "-1"));
                foreach (ImunneVacinas.Voucher item in listaVouchers)
                {
                    DdlBeneficiarios.Items.Add(new ListItem(item.IdParticipante.Nome.ToUpper(), item.IdParticipante.Codigo.ToString())); ;
                }
            }
            else DdlBeneficiarios.Items.Add(new ListItem(listaVouchers[0].IdParticipante.Nome.ToUpper(), listaVouchers[0].IdParticipante.Codigo.ToString())); ;
        }

        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void DtlHorarios_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        try
        {
            ImunneVacinas.HorarioSurto item = (ImunneVacinas.HorarioSurto)e.Item.DataItem;

            HtmlGenericControl DvItemHorario = (HtmlGenericControl)e.Item.FindControl("DvItemHorario");

            HiddenField HdfCodigo = (HiddenField)e.Item.FindControl("HdfCodigo");

            LinkButton LnkHorario = (LinkButton)e.Item.FindControl("LnkHorario");

            Label LblHorario = (Label)e.Item.FindControl("LblHorario");
            Label LblData = (Label)e.Item.FindControl("LblData");
            Label LblSituacao = (Label)e.Item.FindControl("LblSituacao");

            DvItemHorario.Style.Add("width", larguraItem.ToString().Replace(',', '.') + "%");
            LblHorario.Text = item.InfoHoraro.ToString("HH:mm");
            LblData.Text = item.InfoHoraro.ToString("dd/MM/yyyy");

            HdfCodigo.Value = item.Codigo.ToString();

            LnkHorario.CommandArgument = item.Codigo.ToString();

            #region SITUAÇÃO DO HORÁRIO

            // É REALIZADA UMA MUDANÇA VISUAL PARA HORÁRIOS JÁ SELECIONADOS

            if (item.Situacao == ImunneVacinas.Utils.SituacaoHorario.Confirmado)
            {
                LnkHorario.Enabled = false;
                DvItemHorario.Attributes.Add("class", "item-horario-ocupado");
                LblSituacao.Text = "RESERVADO";
            }
            else LblSituacao.Text = "DISPONÍVEL";

            #endregion

            LnkHorario.OnClientClick = "return ConfirmacaoExclucao(this, event,'" + item.Codigo + "');";
        }
        catch (Exception ex)
        {

        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void DtlHorarios_ItemCommand(object source, DataListCommandEventArgs e)
    {
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BtnConfirmar_Click(object sender, EventArgs e)
    {
        int opcaoHorarios = Convert.ToInt32(HdfOpcoesHorarios.Value);
        DateTime dataAcao = Convert.ToDateTime(DdlDias.SelectedValue);

        int codigoParticipante = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["participante"]));
        int codigoCompra = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["compra"]));
        int codigoCampanha = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["campanha"]));
        int codigoClinica = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["clinica"]));
        int qtde = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["qtde"]));
        string[] membros = new string[1];
        string[] itens = new string[1];

        if (!String.IsNullOrEmpty(Request.QueryString["membros"]))
            membros = ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["membros"]).Split('|');

        if (!String.IsNullOrEmpty(Request.QueryString["itens"]))
            itens = ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["itens"]).Split('|');

        int codigoRegistro = Convert.ToInt32(HdfCodigo.Value);

        ImunneVacinas.HorarioSurto item = ImunneVacinas.HorarioSurto.ConsultarUnico(codigoRegistro);
        if (item.Situacao == ImunneVacinas.Utils.SituacaoHorario.Confirmado)
        {
            ConsultarRegistros();

            ImunneVacinas.Alerta.showMensagemAviso(this.Page, "S&A Imunizações", "O horário escolhido acabou de ser selecionado por um outro usuário, escolha um novo!");
            return;
        }
        else
        {
            // SELEÇÃO DE ACORDO COM A FORMA DE ESCOLHA

            // SELEÇÃO AUTOMÁTICA
            if (opcaoHorarios == 1)
            {

                ImunneVacinas.PesquisaHorarioSurto pesquisaDisponiveis = new ImunneVacinas.PesquisaHorarioSurto()
                {
                    Campanha = codigoCampanha,
                    CodigoInicial = codigoRegistro,
                    Situacao = ImunneVacinas.Utils.SituacaoHorario.Livre,
                    Dia = dataAcao
                };

                List<ImunneVacinas.HorarioSurto> listaHorariosLivres = ImunneVacinas.HorarioSurto.ConsultarHorarios(pesquisaDisponiveis);
                int totaisLivres = ImunneVacinas.HorarioSurto.ContabilizarHorarios(pesquisaDisponiveis);
                if (totaisLivres < membros.Length)
                {
                    ImunneVacinas.Alerta.showMensagemAviso(this.Page, "S&A Imunizações",
                                 String.Format("Informamos que você necessidade de {0} disponíveis a partir "
                                 + " do horário {1} que você selecionou."
                                 + " Selecione um horário anterior no dia {2} ou consulte outro dia."
                                 , membros.Length.ToString().PadLeft(2, '0'), item.InfoHoraro.ToString("HH:mm"), DdlDias.SelectedItem.Text));
                    codigoRegistro = 0;
                    HdfCodigo.Value = String.Empty;
                }
                else
                {
                    int contador = 0;
                    foreach (ImunneVacinas.HorarioSurto horario in listaHorariosLivres)
                    {
                        horario.Compra = codigoCompra;
                        horario.Participante = Convert.ToInt32(membros[contador]);
                        horario.Situacao = ImunneVacinas.Utils.SituacaoHorario.Confirmado;
                        horario.Alterar();

                        if ((contador + 1) >= membros.Length)
                            break;

                        contador += 1;
                    }

                    StringBuilder urlPagina = new StringBuilder();

                    urlPagina.AppendFormat("~/vouchers.aspx?campanha={0}", Request.QueryString["campanha"]);
                    urlPagina.AppendFormat("&clinica={0}", Request.QueryString["clinica"]);
                    urlPagina.AppendFormat("&participante={0}", Request.QueryString["participante"]);
                    urlPagina.AppendFormat("&itens={0}", Request.QueryString["itens"]);
                    urlPagina.AppendFormat("&qtde={0}", Request.QueryString["qtde"]);
                    urlPagina.AppendFormat("&compra={0}", Request.QueryString["compra"]);
                    urlPagina.AppendFormat("&membros={0}", Request.QueryString["membros"]);

                    Response.Redirect(urlPagina.ToString(), false);
                    Context.ApplicationInstance.CompleteRequest();
                }
            }
            // SELEÇÃO MANUAL
            else
            {
                // VERIFICANDO SE FOI INFORMADO O BENEFICIÁRIO
                if (DdlBeneficiarios.SelectedValue == "-1")
                {
                    ImunneVacinas.Alerta.showMensagemAviso(this.Page, "Aviso!", "Para seguir com a confirmação é necessário escolher um beneficiário antes.");
                    return;
                }

                int codigoBeneficiario = Convert.ToInt32(DdlBeneficiarios.SelectedValue);

                // VERIFICANDO SE O MESMO JÁ TEM HORÁRIO MARCADO
                item = ImunneVacinas.HorarioSurto.ConsultarUnico(codigoCompra, codigoBeneficiario);
                if (item != null)
                {
                    ImunneVacinas.Alerta.showMensagemAviso(this.Page, "Aviso!", String.Format("O beneficiário {0} já possui um agendamento para {1} às {2}. Informe outro beneficiário.", DdlBeneficiarios.SelectedItem.Text, item.InfoHoraro.ToString("dd/MM/yyyy"), item.InfoHoraro.ToString("HH:mm")));
                    return;
                }
                else
                {
                    item = ImunneVacinas.HorarioSurto.ConsultarUnico(codigoRegistro);
                    item.Participante = codigoBeneficiario;
                    item.Compra = codigoCompra;
                    item.Situacao = ImunneVacinas.Utils.SituacaoHorario.Confirmado;
                    item.Alterar();

                    ImunneVacinas.PesquisaHorarioSurto pesquisaTotais = new ImunneVacinas.PesquisaHorarioSurto()
                    {
                        Compra = codigoCompra,
                        Situacao = ImunneVacinas.Utils.SituacaoHorario.Confirmado
                    };

                    totalIndividual = ImunneVacinas.HorarioSurto.ContabilizarHorarios(pesquisaTotais);
                    LblTotalEscolhidos.Text = totalIndividual.ToString().PadLeft(2, '0');

                    // ENVIO DE E-MAIL AUTOMÁTICO DE TOTAL DE CONFIRMAÇÕES IGUAL TOTAL DE BENEFICÁRIOS
                    if (totalIndividual >= membros.Length)
                    {
                        StringBuilder urlPagina = new StringBuilder();

                        urlPagina.AppendFormat("~/vouchers.aspx?campanha={0}", Request.QueryString["campanha"]);
                        urlPagina.AppendFormat("&clinica={0}", Request.QueryString["clinica"]);
                        urlPagina.AppendFormat("&participante={0}", Request.QueryString["participante"]);
                        urlPagina.AppendFormat("&itens={0}", Request.QueryString["itens"]);
                        urlPagina.AppendFormat("&qtde={0}", Request.QueryString["qtde"]);
                        urlPagina.AppendFormat("&compra={0}", Request.QueryString["compra"]);
                        urlPagina.AppendFormat("&membros={0}", Request.QueryString["membros"]);

                        Response.Redirect(urlPagina.ToString(), false);
                        Context.ApplicationInstance.CompleteRequest();
                    }

                    ConsultarRegistros();

                    LnkSemAgenda.Enabled = false;
                    LnkSemAgenda.Style.Add("opacity", ".5");
                }
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void LnkPesquisar_Click(object sender, EventArgs e)
    {
        ConsultarRegistros();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void LnkSemAgenda_Click(object sender, EventArgs e)
    {
        // AQUI OCORRE O DIRECIONAMENTO DO USUÁRIO, CASO O MESMO OPTE POR SIMPLESMENTE SEGUIR
        // SEM ESCOLHER O AGENDAMENTO.

        #region DADOS DA URL 

        StringBuilder urlPagina = new StringBuilder();

        urlPagina.AppendFormat("~/vouchers.aspx?campanha={0}", Request.QueryString["campanha"]);
        urlPagina.AppendFormat("&clinica={0}", Request.QueryString["clinica"]);
        urlPagina.AppendFormat("&participante={0}", Request.QueryString["participante"]);
        urlPagina.AppendFormat("&itens={0}", Request.QueryString["itens"]);
        urlPagina.AppendFormat("&qtde={0}", Request.QueryString["qtde"]);
        urlPagina.AppendFormat("&compra={0}", Request.QueryString["compra"]);
        urlPagina.AppendFormat("&membros={0}", Request.QueryString["membros"]);

        #endregion

        Response.Redirect(urlPagina.ToString(), false);
        Context.ApplicationInstance.CompleteRequest();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void DdlOpcoesHorarios_SelectedIndexChanged(object sender, EventArgs e)
    {
        int opcaoSelecionada = Convert.ToInt32(DdlOpcoesHorarios.SelectedValue);
        PnlOpcoes.Visible = false;

        // MONTA O PAINEL DE ESCOLHA DE AGENDAMENTO DOS HORÁRIOS DE APLICAÇÕES DOS ITENS DA COMPRA

        switch (opcaoSelecionada)
        {
            // ESCOLHA AUTOMÁTICA, SEGUINDO A SEQUENCIA DE HORÁRIOS LIVRES
            case 1:
                {
                    PnlBeneficiarios.Visible = false;
                    PnlHorarios.Visible = true;
                    PnlDatasPesquisa.Visible = true;

                    HdfOpcoesHorarios.Value = DdlOpcoesHorarios.SelectedValue;
                }
                break;
            // ESCOLHA MANUAL, SEGUINDO A ESCOLHA DO USUÁRIO
            case 2:
                {
                    PnlBeneficiarios.Visible = true;
                    PnlHorarios.Visible = true;
                    PnlDatasPesquisa.Visible = true;

                    HdfOpcoesHorarios.Value = DdlOpcoesHorarios.SelectedValue;
                }
                break;
            default:
                {
                    ImunneVacinas.Alerta.showMensagemAviso(this.Page, "Aviso!", "Para escolher os horários, é preciso definir a forma de escolha. Caso contrário, poderá seguir sem agendamente prévio, clicando em 'SEGUIR SEM AGENDAMENTO'");
                }
                break;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void DdlDias_SelectedIndexChanged(object sender, EventArgs e)
    {
        ConsultarRegistros();
    }
}