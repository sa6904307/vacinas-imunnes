﻿<%@ Page Title="S&A Imunizações | Identificação da Campanha" Language="C#" MasterPageFile="~/imunne.master" AutoEventWireup="true" CodeFile="campanha.aspx.cs" Inherits="imunne_campanha" MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        function validar() {
            // Nome
            if (document.getElementById("<%=TxtCampanha.ClientID%>").value == "") {
                MensagemGenerica("Aviso!", "Por favor, informe o nome ou a identificação da campanha.", "warning");
                return false;
            }
        };
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section class="subheader">
        <div class="container">
            <div class="row">
                <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                    <i class="fa fa-ambulance"></i>&nbsp;Seleção da Campanha
                </div>
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 text-right">
                    <a href="javascript:history.back()" title="Voltar à página anterior">
                        <i class="fa fa-chevron-left"></i>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 caixa-central col-centered">
                <asp:UpdatePanel ID="UpdCampanha" runat="server">
                    <ContentTemplate>
                        <asp:Panel ID="PnlCampanha" runat="server" DefaultButton="LnkConsultar">
                            <div class="caixa-branca">
                                <div class="step-header">
                                    INFORME A CAMPANHA
                                </div>
                                <div class="box-inside">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group text-justify">
                                        <p>Para iniciar a aquisição das vacinas, por favor informe o nome ou a identificação da campanha no campo abaixo e clique em <b>CONSULTAR</b>.</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group text-center">
                                        <asp:TextBox ID="TxtCampanha" runat="server" CssClass="form-control text-uppercase" placeHolder="NOME OU IDENTIFICAÇÃO"></asp:TextBox>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group text-center">
                                        <asp:LinkButton ID="LnkConsultar" runat="server" CssClass="btn btn-block btn-primary" ToolTip="Consultar a campanha desejada" OnClick="LnkConsultar_Click" OnClientClick="return validar();">
                            <i class="fa fa-search"></i>&nbsp;Consultar
                                        </asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="LnkConsultar" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
</asp:Content>
