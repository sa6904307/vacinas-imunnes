﻿<%@ Page Title="" Language="C#" MasterPageFile="~/imunne.master" AutoEventWireup="true" CodeFile="pagamentos.aspx.cs" Inherits="imunne_pagamentos" MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        function validar() {
            // Titulo Cartão
            if (document.getElementById("<%=TxtNomeTitular.ClientID%>").value == "") {
                MensagemGenerica("Aviso!", "Por favor, informe o nome do titular do cartão.", "warning");
                return false;
            }

            // Número Cartão
            if (document.getElementById("<%=TxtNumeroCartao.ClientID%>").value == "") {
                MensagemGenerica("Aviso!", "Por favor, informe o número do cartão.", "warning");
                return false;
            }

            // CVV
            if (document.getElementById("<%=TxtCVV.ClientID%>").value == "") {
                MensagemGenerica("Aviso!", "Por favor, informe o CVV do cartão.", "warning");
                return false;
            }

            // Mês Validade
            if (document.getElementById("<%=DdlMesValidade.ClientID%>").value == "-1") {
                MensagemGenerica("Aviso!", "Por favor, informe o mês de validade do cartão.", "warning");
                return false;
            }

            // Ano Validade
            if (document.getElementById("<%=DdlAnoValidade.ClientID%>").value == "-1") {
                MensagemGenerica("Aviso!", "Por favor, informe o ano de validade do cartão.", "warning");
                return false;
            }
        };
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section class="subheader">
        <div class="container">
            <div class="row">
                <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                    <i class="fa fa-credit-card"></i>&nbsp;Pagamento das vacinas
                </div>
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 text-right">
                    <a href="javascript:history.back()" title="Voltar à página anterior">
                        <i class="fa fa-chevron-left"></i>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <div class="container">
        <div class="row">
            <asp:UpdatePanel ID="UpdCampanha" runat="server">
                <ContentTemplate>
                    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
                        <div class="caixa-branca">
                            <div class="step-header">
                                DADOS DO ADQUIRENTE
                            </div>
                            <div class="box-inside">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                    <label>Nome</label>
                                    <asp:Label ID="LblNome" runat="server" CssClass="label-identificacao"></asp:Label>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 form-group">
                                    <label>CPF</label>
                                    <asp:Label ID="LblCPF" runat="server" CssClass="label-identificacao"></asp:Label>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 form-group">
                                    <label>Nascimento</label>
                                    <asp:Label ID="LblNascimento" runat="server" CssClass="label-identificacao"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="caixa-branca">
                            <div class="step-header">
                                INFORMAÇÕES DA CAMPANHA
                            </div>
                            <div class="box-inside">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                    <label>CAMPANHA</label>
                                    <asp:Label ID="LblCampanha" runat="server" CssClass="label-identificacao"></asp:Label>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                    <label>CLÍNICA</label>
                                    <asp:Label ID="LblClinica" runat="server" CssClass="label-identificacao"></asp:Label>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                    <label>EMPRESA</label>
                                    <asp:Label ID="LblEmpresa" runat="server" CssClass="label-identificacao"></asp:Label>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                    <label>VALIDADE</label>
                                    <asp:Label ID="LblValidade" runat="server" CssClass="label-identificacao"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                        <div class="caixa-branca">
                            <div class="step-header">
                                DADOS DA COMPRA
                            </div>
                            <div class="box-inside">
                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 form-group">
                                    <label>Situação</label>
                                    <asp:Label ID="LblSituacao" runat="server" CssClass="label-identificacao"></asp:Label>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 form-group">
                                    <label>Data</label>
                                    <asp:Label ID="LblDataCompra" runat="server" CssClass="label-identificacao"></asp:Label>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 form-group">
                                    <label>Valor</label>
                                    <asp:Label ID="LblValor" runat="server" CssClass="label-identificacao"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="caixa-branca">
                            <div class="step-header">
                                DADOS DE PAGAMENTO
                            </div>
                            <div class="box-inside">
                                <asp:Panel ID="PnlInformacoesPgto" runat="server">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                        <label>Nome Titular</label>
                                        <asp:TextBox ID="TxtNomeTitular" runat="server" CssClass="form-control text-uppercase"></asp:TextBox>
                                    </div>
                                    <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9 form-group">
                                        <label>Número do Cartão</label>
                                        <asp:TextBox ID="TxtNumeroCartao" runat="server" CssClass="form-control" OnKeyPress="mascara(this, mnumeros)" MaxLength="19"></asp:TextBox>
                                    </div>
                                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 form-group">
                                        <label>CVV</label>
                                        <asp:TextBox ID="TxtCVV" runat="server" CssClass="form-control text-center" OnKeyPress="mascara(this, mnumeros)" MaxLength="4"></asp:TextBox>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 form-group">
                                        <label>Validade Cartão</label>
                                        <asp:DropDownList ID="DdlMesValidade" runat="server" CssClass="form-control text-center"></asp:DropDownList>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 form-group">
                                        <label>&nbsp</label>
                                        <asp:DropDownList ID="DdlAnoValidade" runat="server" CssClass="form-control text-center"></asp:DropDownList>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 form-group">
                                        <label>Parcelas</label>
                                        <asp:DropDownList ID="DdlParcelas" runat="server" CssClass="form-control"></asp:DropDownList>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right">
                                        <img src="../img/logos/cartoes.png" class="img-responsive" />
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right">
                                        <asp:LinkButton ID="LnkContinuar" runat="server" CssClass="btn btn-block btn-primary" OnClick="LnkContinuar_Click" OnClientClick="return validar();" CausesValidation="false">
                            <i class="fa fa-chevron-right"></i>&nbsp;
                            Continuar
                                        </asp:LinkButton>
                                    </div>
                                </asp:Panel>
                                <asp:Panel ID="PnlInformacoesPix" runat="server" Visible="false">
                                    <!-- Seção de informações de pagamento para PIX -->
                                    <!-- ... (código para informações de pagamento via PIX) ... -->
                                    <asp:HiddenField id="HdnTokenTransaction" runat="server" value=""/>
                                    <asp:HiddenField id="HdnMaxTimeTransaction" runat="server" value=""/>
                                    <embed type="image/svg+xml" style="display: block; margin: auto;" src="" width="200" height="200" runat="server" id="ImgQrcode" alt="Qrcode pagamento Pix"></embed><br>
                                    <asp:Label ID="LblCopiaECola" runat="server" CssClass=""></asp:Label>
                                    <h1 id="timer" style="display: block; margin: auto;" >00:00</h1>
                                    <script defer>
                                        window.onload = function() {
                                            var tempoRestante;
                                            var timerId;
                                            var timerMestreId;

                                            iniciarTimer();

                                            function iniciarTimer() {
                                                // Obtenha o valor do input para definir o tempo máximo
                                                //var tempoMaximoString = document.getElementById("ContentPlaceHolder1_HdnMaxTimeTransaction").value;

                                                // Converta o formato da string para uma data JavaScript
                                                // var dataTempoMaximo = new Date(tempoMaximoString);
        
                                                var date = new Date();
                                                date.setMinutes(date.getMinutes() + 10);

                                                console.log(`dataTempoMaximo: ${date}`)

                                                tempoRestante = Math.floor((date - new Date()) / 1000);

                                                console.log(`tempoRestante: ${tempoRestante}`)

                                                console.log(`timer: ${document.getElementById("timer") }`)

                                                // Atualize o elemento HTML com o tempo inicial formatado
                                                atualizarTimer();

                                                console.log(`tempoRestante: ${document.getElementById("timer")}`)

                                                // Inicie o intervalo do timer regressivo a cada segundo
                                                timerId = setInterval(function() {
                                                    tempoRestante--;

                                                    if (tempoRestante >= 0) {
                                                        // Atualize o elemento HTML com o tempo regressivo formatado
                                                        atualizarTimer();
                                                        verificarRequisicao();
                                                    } else {
                                                        // O tempo chegou a zero, pare o intervalo
                                                        clearInterval(timerId);
                                                        alert("Tempo esgotado! Por favor, retorne para o passo anterior e refaça a operação.");
                                                    }
                                                }, 1000);
                                                timerMestreId = setInterval(verificarRequisicao,5000);
                                            }

                                            function atualizarTimer() {
                                                var horas = Math.floor(tempoRestante / 3600);
                                                var minutos = Math.floor((tempoRestante % 3600) / 60);
                                                var segundos = tempoRestante % 60;

                                                var horasFormatadas = horas < 10 ? "0" + horas : horas;
                                                var minutosFormatados = minutos < 10 ? "0" + minutos : minutos;
                                                var segundosFormatados = segundos < 10 ? "0" + segundos : segundos;

                                                document.getElementById("timer").innerText = horasFormatadas + ":" + minutosFormatados + ":" + segundosFormatados;
                                            }

                                            function resolveUrl(relativeUrl) {
                                                if (relativeUrl.charAt(0) === '~') {
                                                    // Obtém o caminho base da aplicação
                                                    var appPath = window.location.pathname.split('/')[1];

                                                    // Constrói a URL completa
                                                    return window.location.origin + '/' + appPath + relativeUrl.slice(1);
                                                }

                                                // Se não for relativa, retorna a própria URL
                                                return relativeUrl;
                                            }

                                            function verificarRequisicao() {
                                                // Resgatar parâmetros da URL
                                                const url = new URL(window.location.href);
                                                const token = document.getElementById("ContentPlaceHolder1_HdnTokenTransaction").value;
                                                const campanha = url.searchParams.get("campanha");
                                                const clinica = url.searchParams.get("clinica");
                                                const participante = url.searchParams.get("participante");
                                                const itens = url.searchParams.get("itens");
                                                const qtde = url.searchParams.get("qtde");
                                                const compra = url.searchParams.get("compra");
                                                const membros = url.searchParams.get("membros");

                                                fetch('VerificaPix.ashx', {
                                                    method: 'POST',
                                                    headers: {
                                                        'Content-Type': 'application/json',
                                                    },
                                                    body: JSON.stringify({
                                                        data: {
                                                            token: token,
                                                            campanha: campanha,
                                                            clinica: clinica,
                                                            participante: participante,
                                                            itens: itens,
                                                            qtde: qtde,
                                                            compra: compra,
                                                            membros: membros,
                                                        },
                                                    }),
                                                })
                                                .then(response => {
                                                    if (response.ok) {
                                                        console.log(response);
                                                        return response.json(); // Converte a resposta para JSON
                                                    } else {
                                                        console.log("erro: "+response.text());
                                                        throw new Error(`Erro na requisição: ${response.status}`);
                                                    }
                                                })
                                                .then(data => {
                                                    console.log(`responseData: ${JSON.stringify(data)}`)
                                                    // Verifica se o status é "Aprovado" e se existe um URL de redirecionamento
                                                    if (data.status === "Aprovado" && data.data && data.data.urlRedirect) {
                                                        console.log("Status Aprovado. Redirecionar para:", data.data.urlRedirect);
                                                        window.location.href = resolveUrl(data.data.urlRedirect);
                                                    } else if (data.status === "Cancelado") {
                                                        clearInterval(timerMestreId);
                                                    }
                                                })
                                                .catch(error => {
                                                    console.error('Erro na requisição:', error);
                                                    clearInterval(timerMestreId);
                                                });
                                            }
                                        
                                            document.getElementById("ContentPlaceHolder1_LblCopiaECola").addEventListener("click", function() {
                                                copiarConteudoDoSpan("ContentPlaceHolder1_LblCopiaECola");
                                            });

                                            function copiarConteudoDoSpan(idDoSpan) {
                                            var spanElement = document.getElementById(idDoSpan);

                                            if (spanElement) {
                                                var conteudoDoSpan = spanElement.textContent || spanElement.innerText;

                                                var tempInput = document.createElement("textarea");
                                                tempInput.value = conteudoDoSpan;
                                                document.body.appendChild(tempInput);

                                                tempInput.select();
                                                document.execCommand("copy");

                                                document.body.removeChild(tempInput);

                                                console.log("Conteúdo do span copiado para a área de transferência:", conteudoDoSpan);
                                            } else {
                                                console.error("Elemento span não encontrado com o ID:", idDoSpan);
                                            }
                                            }
                                        };
                                    </script>
                                </asp:Panel>
                                <asp:Panel ID="PnlInformacoesDirecionamento" runat="server" Visible="false">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right">
                                        <asp:LinkButton ID="LnkContinuar2" runat="server" CssClass="btn btn-block btn-primary" OnClick="LnkContinuar2_Click" CausesValidation="false">
                            <i class="fa fa-chevron-right"></i>&nbsp;
                            Continuar
                                        </asp:LinkButton>
                                    </div>
                                </asp:Panel>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
