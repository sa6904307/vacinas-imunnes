﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="email.aspx.cs" Inherits="email" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">
        body {
            font-family: Arial, sans-serif;
        }

        input[type=text] {
            width: 300px;
        }

        .btn {
            color: #fff;
            width: 300px;
            background-color: #149216;
            border: none;
            height: 40px;
            line-height: 40px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server" autocomplete="off" aria-autocomplete="none">
        <div>
            HOST<br />
            <asp:TextBox ID="TxtHost" runat="server"></asp:TextBox><br /><br />
            PORTA<br />
            <asp:TextBox ID="TxtPorta" runat="server"></asp:TextBox><br /><br />
            USUÁRIO<br />
            <asp:TextBox ID="TxtUser" runat="server"></asp:TextBox><br /><br />
            SENHA<br />
            <asp:TextBox ID="TxtPass" runat="server" AutoCompleteType="None"></asp:TextBox><br /><br />
            DESTINO<br />
            <asp:TextBox ID="TxtDestino" runat="server"></asp:TextBox><br /><br />
            ASSUNTO<br />
            <asp:TextBox ID="TxtAssunto" runat="server" Text="TESTE"></asp:TextBox><br /><br />
            CONTEUDO<br />
            <asp:TextBox ID="TxtConteudo" runat="server" Text="TESTE"></asp:TextBox><br /><br />
            <asp:Button ID="BtnEnviar" runat="server" Text="ENVIAR MODO 1" CssClass="btn" OnClick="BtnEnviar_Click" /><br /><br />
            <asp:Button ID="BtnEnvio2" runat="server" Text="ENVIAR MODO 2" CssClass="btn" OnClick="BtnEnviar2_Click" />
            <br /><br />
            RESPOSTA<br />
            <asp:TextBox ID="TxtResultado" runat="server" TextMode="MultiLine" Height="200" Width="300"></asp:TextBox>
        </div>
    </form>
</body>
</html>
