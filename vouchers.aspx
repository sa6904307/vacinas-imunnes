﻿<%@ Page Title="" Language="C#" MasterPageFile="~/imunne.master" AutoEventWireup="true" CodeFile="vouchers.aspx.cs" Inherits="imunne_vouchers" MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section class="subheader">
        <div class="container">
            <div class="row">
                <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                    <i class="fa fa-print"></i>&nbsp;Impressão dos Vales-Vacinas
                </div>
                <asp:Panel ID="PnlBack" runat="server">
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 text-right">
                        <a href="javascript:history.back()" title="Voltar à página anterior">
                            <i class="fa fa-chevron-left"></i>
                        </a>
                    </div>
                </asp:Panel>
            </div>
        </div>
    </section>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
                <div class="caixa-branca">
                    <div class="step-header">
                        DADOS DO ADQUIRENTE
                    </div>
                    <div class="box-inside">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                            <label>Nome</label>
                            <asp:Label ID="LblNome" runat="server" CssClass="label-identificacao"></asp:Label>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 form-group">
                            <label>CPF</label>
                            <asp:Label ID="LblCPF" runat="server" CssClass="label-identificacao"></asp:Label>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 form-group">
                            <label>Nascimento</label>
                            <asp:Label ID="LblNascimento" runat="server" CssClass="label-identificacao"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="caixa-branca">
                    <div class="step-header">
                        INFORMAÇÕES DA CAMPANHA
                    </div>
                    <div class="box-inside">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                            <label>CAMPANHA</label>
                            <asp:Label ID="LblCampanha" runat="server" CssClass="label-identificacao"></asp:Label>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                            <label>CLÍNICA</label>
                            <asp:Label ID="LblClinica" runat="server" CssClass="label-identificacao"></asp:Label>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                            <label>EMPRESA</label>
                            <asp:Label ID="LblEmpresa" runat="server" CssClass="label-identificacao"></asp:Label>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                            <label>VALIDADE</label>
                            <asp:Label ID="LblValidade" runat="server" CssClass="label-identificacao"></asp:Label>
                        </div>
                        <asp:UpdatePanel ID="UpdImpressao" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                    <asp:LinkButton ID="LnkImpressao" runat="server" CssClass="btn btn-block btn-primary" CausesValidation="false" OnClick="LnkImpressao_Click">
                                <i class="fa fa-envelope"></i>&nbsp;ENVIAR E-MAIL
                                    </asp:LinkButton>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="LnkImpressao" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                <div class="caixa-branca">
                    <div class="step-header">
                        <%--VALES-VACINAS--%>
                        BENEFICIÁRIOS
                    </div>
                    <div class="box-inside">
                        <div class="box-grids">
                            <div class="dv-grid-f-header">
                                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 hidden" style="display: inline-block; float: none;">
                                    AÇÕES
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="display: inline-block; float: none;">
                                    Participante
                                </div>
                                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" style="display: inline-block; float: none;">
                                    Campanha
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="display: inline-block; float: none;">
                                    Clínica
                                </div>
                            </div>
                            <div class="dv-grid-f">
                                <asp:Repeater ID="RptPrincipal" runat="server" OnItemDataBound="RptPrincipal_ItemDataBound" OnItemCommand="RptPrincipal_ItemCommand">
                                    <ItemTemplate>
                                        <div id="dvItem" runat="server">
                                            <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 hidden" style="display: inline-block; float: none;">
                                                <div style="margin-top: -38px; position: absolute;">
                                                    <asp:HyperLink ID="LnkImprimir" runat="server" Target="_blank">
                                                        <i class="fa fa-print text-regular" aria-hidden="true"></i>&nbsp;IMPRIMIR
                                                    </asp:HyperLink>
                                                </div>
                                            </div>
                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="display: inline-block; float: none;">
                                                <div class="truncate">
                                                    <asp:Label ID="LblNomeParticipante" runat="server" CssClass="text-uppercase"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" style="display: inline-block; float: none;">
                                                <div class="truncate">
                                                    <asp:Label ID="LblNomeCampanha" runat="server" CssClass="text-uppercase"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="display: inline-block; float: none;">
                                                <div class="truncate">
                                                    <asp:Label ID="LblNomeClinica" runat="server" CssClass="text-uppercase"></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
