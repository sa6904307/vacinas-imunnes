﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;

public partial class prints_voucher : System.Web.UI.Page
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            IniciarPagina();
    }

    /// <summary>
    /// 
    /// </summary>
    private void IniciarPagina()
    {
        try
        {
            int codigo = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["codigo"]));

            ImunneVacinas.Voucher voucherSelecionado = ImunneVacinas.Voucher.ConsultarUnico(codigo);
            ImunneVacinas.Compra compraSelecionada = ImunneVacinas.Compra.ConsultarUnico(voucherSelecionado.IdCompra.Codigo);
            ImunneVacinas.Campanha campanhaSelecionada = ImunneVacinas.Campanha.ConsultarUnico(voucherSelecionado.IdCampanha.Codigo);
            ImunneVacinas.Clinica clinicaSelecionada = ImunneVacinas.Clinica.ConsultarUnico(voucherSelecionado.IdClinica.Codigo);
            ImunneVacinas.Participante beneficiarioSelecionado = ImunneVacinas.Participante.ConsultarUnico(voucherSelecionado.IdParticipante.Codigo);
            ImunneVacinas.Participante participanteSelecionado;

            if (beneficiarioSelecionado.ParticipantePai > 0)
                participanteSelecionado = ImunneVacinas.Participante.ConsultarUnico(beneficiarioSelecionado.ParticipantePai);
            else
                participanteSelecionado = ImunneVacinas.Participante.ConsultarUnico(compraSelecionada.IdParticipante.Codigo);

            ImunneVacinas.Empresa empresaSelecionada = ImunneVacinas.Empresa.ConsultarUnico(campanhaSelecionada.IdEmpresa.Codigo);
            ImunneVacinas.TransacaoRealizada transacaoSelecionada = ImunneVacinas.TransacaoRealizada.ConsultarUnicoCompra(compraSelecionada.Codigo);

            Page.Title = "VALE-VACINA - " + beneficiarioSelecionado.Nome.ToUpper();

            if (!String.IsNullOrEmpty(empresaSelecionada.Logotipo))
                ImgLogoEmpresa.ImageUrl = "~/img/empresas/" + empresaSelecionada.Logotipo;
            else
                ImgLogoEmpresa.Visible = false;

            if (voucherSelecionado.Situacao == ImunneVacinas.Voucher.SituacaoVoucher.Gerado)
            {
                voucherSelecionado.DataImpressao = DateTime.Now;
                voucherSelecionado.Alterar();
            }

            // BLOCO IDENTIFICAÇÕES
            StringBuilder dadosClinica = new StringBuilder();
            StringBuilder dadosEmpresa = new StringBuilder();

            #region Dados da Clinica

            dadosClinica.AppendFormat(clinicaSelecionada.NomeFantasia.ToUpper());
            if (!String.IsNullOrEmpty(clinicaSelecionada.Endereco))
            {
                dadosClinica.AppendFormat(" - {0}", clinicaSelecionada.Endereco.ToUpper());
                dadosClinica.AppendFormat(", {0}", clinicaSelecionada.Numero);
            }

            if (clinicaSelecionada.IdCidade.Codigo > 0)
            {
                dadosClinica.AppendFormat(" - {0}", clinicaSelecionada.IdCidade.Nome.ToUpper());
                dadosClinica.AppendFormat("/{0}", clinicaSelecionada.IdUf.Sigla);
            }

            if (!String.IsNullOrEmpty(clinicaSelecionada.Telefone))
                dadosClinica.AppendFormat(" - TEL.: {0}", clinicaSelecionada.Telefone);

            #endregion

            #region Dados da Empresa

            dadosEmpresa.AppendFormat(empresaSelecionada.NomeFantasia.ToUpper());
            if (!String.IsNullOrEmpty(empresaSelecionada.Endereco))
            {
                dadosEmpresa.AppendFormat(" - {0}", empresaSelecionada.Endereco);
                dadosEmpresa.AppendFormat(", {0}", empresaSelecionada.Numero);
            }

            if (empresaSelecionada.IdCidade.Codigo > 0)
            {
                dadosEmpresa.AppendFormat(" - {0}", empresaSelecionada.IdCidade.Nome);
                dadosEmpresa.AppendFormat("/{0}", empresaSelecionada.IdUf.Sigla);
            }

            if (!String.IsNullOrEmpty(empresaSelecionada.Telefone))
                dadosEmpresa.AppendFormat(" - TEL.: {0}", empresaSelecionada.Telefone);

            #endregion

            LblDadosCampanha.Text = campanhaSelecionada.Identificacao + " | " + campanhaSelecionada.Nome.ToUpper();
            LblDadosEmpresa.Text = dadosEmpresa.ToString();
            LblDadosClinica.Text = dadosClinica.ToString();

            #region Dados Adquirente

            StringBuilder dadosPrincipal = new StringBuilder();
            dadosPrincipal.Append("<div class='col2 ftl-left'><b>COMPRA:</b></div>");
            dadosPrincipal.AppendFormat("<div class='col10 ftl-right'>{0} | {1}</div>", compraSelecionada.Codigo.ToString().PadLeft(5, '0') + " | " + transacaoSelecionada.numeroComprovanteVenda, compraSelecionada.DataCompra.ToString("dd/MM/yyyy"));

            dadosPrincipal.Append("<div class='col2 ftl-left'><b>NOME:</b></div>");
            dadosPrincipal.AppendFormat("<div class='col10 ftl-right'>{0}</div>", participanteSelecionado.Nome.ToUpper());

            if (!String.IsNullOrEmpty(participanteSelecionado.Cpf))
            {
                dadosPrincipal.Append("<div class='col2 ftl-left'><b>CPF/ID:</b></div>");
                if (ImunneVacinas.Utils.ValidarCpfCnpj(ImunneVacinas.Utils.LimparCpfCnpj(participanteSelecionado.Cpf)))
                    dadosPrincipal.AppendFormat("<div class='col10 ftl-right'>{0}</div>", ImunneVacinas.Utils.FormatarCpfCnpj(participanteSelecionado.Cpf));
                else
                    dadosPrincipal.AppendFormat("<div class='col10 ftl-right'>{0}</div>", participanteSelecionado.Cpf);
            }

            if (participanteSelecionado.DataNascimento != ImunneVacinas.Utils.dataPadrao)
            {
                dadosPrincipal.Append("<div class='col2 ftl-left'><b>NASCIMENTO:</b></div>");
                dadosPrincipal.AppendFormat("<div class='col10 ftl-right'>{0}</div>", participanteSelecionado.DataNascimento.ToString("dd/MM/yyyy"));
            }

            if (!String.IsNullOrEmpty(participanteSelecionado.Telefone))
            {
                dadosPrincipal.Append("<div class='col2 ftl-left'><b>TEL.:</b></div>");
                dadosPrincipal.AppendFormat("<div class='col10 ftl-right'>{0}</div>", participanteSelecionado.Telefone);
            }

            if (!String.IsNullOrEmpty(participanteSelecionado.Email))
            {
                dadosPrincipal.Append("<div class='col2 ftl-left'><b>E-MAIL:</b></div>");
                dadosPrincipal.AppendFormat("<div class='col10 ftl-right'>{0}</div>", participanteSelecionado.Email.ToLower());
            }

            #endregion

            #region Dados Beneficiário

            StringBuilder dadosBeneficiario = new StringBuilder();
            dadosBeneficiario.Append("<div class='col2 ftl-left'><b>NOME:</b></div>");
            dadosBeneficiario.AppendFormat("<div class='col10 ftl-right'>{0}</div>", beneficiarioSelecionado.Nome.ToUpper());

            if (!String.IsNullOrEmpty(beneficiarioSelecionado.Cpf))
            {
                dadosBeneficiario.Append("<div class='col2 ftl-left'><b>CPF/ID:</b></div>");
                if (ImunneVacinas.Utils.ValidarCpfCnpj(ImunneVacinas.Utils.LimparCpfCnpj(beneficiarioSelecionado.Cpf)))
                    dadosBeneficiario.AppendFormat("<div class='col10 ftl-right'>{0}</div>", ImunneVacinas.Utils.FormatarCpfCnpj(beneficiarioSelecionado.Cpf));
                else
                    dadosBeneficiario.AppendFormat("<div class='col10 ftl-right'>{0}</div>", beneficiarioSelecionado.Cpf);
            }

            if (beneficiarioSelecionado.DataNascimento != ImunneVacinas.Utils.dataPadrao)
            {
                dadosBeneficiario.Append("<div class='col2 ftl-left'><b>NASCIMENTO:</b></div>");
                dadosBeneficiario.AppendFormat("<div class='col10 ftl-right'>{0}</div>", beneficiarioSelecionado.DataNascimento.ToString("dd/MM/yyyy"));
            }

            if (!String.IsNullOrEmpty(beneficiarioSelecionado.Telefone))
            {
                dadosBeneficiario.Append("<div class='col2 ftl-left'><b>TEL.:</b></div>");
                dadosBeneficiario.AppendFormat("<div class='col10 ftl-right'>{0}</div>", beneficiarioSelecionado.Telefone);
            }

            if (!String.IsNullOrEmpty(beneficiarioSelecionado.Email))
            {
                dadosBeneficiario.Append("<div class='col2 ftl-left'><b>E-MAIL:</b></div>");
                dadosBeneficiario.AppendFormat("<div class='col10 ftl-right'>{0}</div>", beneficiarioSelecionado.Email.ToLower());
            }

            if (beneficiarioSelecionado.Parentesco.Codigo > 0)
            {
                dadosBeneficiario.Append("<div class='col2 ftl-left'><b>PARENTESCO:</b></div>");
                dadosBeneficiario.AppendFormat("<div class='col10 ftl-right'>{0}</div>", beneficiarioSelecionado.Parentesco.Nome);
            }

            #endregion

            #region Dados Utilização

            if (voucherSelecionado.DataRetirada != ImunneVacinas.Utils.dataPadrao)
            {
                StringBuilder dadosUtilizacao = new StringBuilder();
                dadosUtilizacao.Append("<div class='col2 ftl-left'><b>INFORMAÇÕES:</b></div>");
                dadosUtilizacao.AppendFormat("<div class='col10 ftl-right'>OS ITENS CONTIDOS NESTE VALE FORAM UTILIZADOS EM {0}</div>", voucherSelecionado.DataRetirada.ToString("dd/MM/yyyy"));

                DvUtilizacao.InnerHtml = dadosUtilizacao.ToString();
                PnlUtilizacao.Visible = true;
            }
            else PnlUtilizacao.Visible = false;

            #endregion

            DvDadosPrincipal.InnerHtml = dadosPrincipal.ToString();
            DvDadosBeneficiario.InnerHtml = dadosBeneficiario.ToString();

            #region Itens

            StringBuilder dadosItens = new StringBuilder();

            ImunneVacinas.PesquisaItemCompra pesquisa = new ImunneVacinas.PesquisaItemCompra()
            {
                Compra = compraSelecionada.Codigo,
                Ordenacao = ImunneVacinas.Utils.TipoOrdenacao.porNome
            };

            List<ImunneVacinas.ItemCompra> listaItens = ImunneVacinas.ItemCompra.ConsultarItens(pesquisa);
            foreach (ImunneVacinas.ItemCompra item in listaItens)
            {
                dadosItens.Append("<div class='col2 ftl-left'><b>ITEM:</b></div>");
                dadosItens.AppendFormat("<div class='col10 ftl-right'>1 DOSE | {0}</div>", item.IdVacina.Nome.ToUpper());
            }

            DvDadosItens.InnerHtml = dadosItens.ToString();

            #endregion

            StringBuilder dadosObservacoes = new StringBuilder();
            dadosObservacoes.AppendFormat("As aplicações desta campanha se iniciam em <b>{0}</b>.<br />", campanhaSelecionada.DataAbertura.ToString("dd/MM/yyyy"));
            dadosObservacoes.AppendFormat("Os itens adquiridos estarão disponíveis até a data de <b>{0}</b>.", campanhaSelecionada.DataLimite.ToString("dd/MM/yyyy"));
            DvDadosObservacoes.InnerHtml = dadosObservacoes.ToString();
        }
        catch (Exception ex)
        {
            ImunneVacinas.Erro erro = new ImunneVacinas.Erro();
            erro.addArquivo(ex);
        }
    }
}