﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="compra.aspx.cs" Inherits="prints_compra" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="../app_themes/print.css" rel="stylesheet" />
    <!-- FONT AWESOME -->
    <script src="https://use.fontawesome.com/fbb802a78b.js"></script>
</head>
<%--<body>--%>
<body style="" onload="javascript:print();">
    <form id="form1" runat="server">
        <div class="dv-box-print">
            <div class="onerow">
                <!-- HEADER DA IMPRESSÃO -->
                <div class="header">
                    <div class="col3 text-left">
                        <img src="../img/logos/logo-main.png" />
                    </div>
                    <div class="col6 text-center">
                        <h2>VALE-VACINA</h2>
                    </div>
                    <div class="col3 text-right">
                        <asp:Image ID="ImgLogoEmpresa" runat="server" />
                    </div>
                </div>
                <!-- DADOS DA CAMPANHA -->
                <div class="bloco-conteudo">
                    <div class="col2 ftl-left">
                        <b>CAMPANHA:</b>
                    </div>
                    <div class="col10 ftl-right">
                        <asp:Label ID="LblDadosCampanha" runat="server"></asp:Label>
                    </div>
                    <div class="col2 ftl-left">
                        <b>EMPRESA:</b>
                    </div>
                    <div class="col10 ftl-right">
                        <asp:Label ID="LblDadosEmpresa" runat="server"></asp:Label>
                    </div>
                    <div class="col2 ftl-left">
                        <b>CLÍNICA:</b>
                    </div>
                    <div class="col10 ftl-right">
                        <asp:Label ID="LblDadosClinica" runat="server"></asp:Label>
                    </div>
                    <%--                    <div class="col2 ftl-left">
                        <b>VALE:</b>
                    </div>
                    <div class="col10 ftl-right">
                        <asp:Label ID="LblDadosVale" runat="server"></asp:Label>
                    </div>--%>
                    <div class="dv-spc-5-bottom">
                    </div>
                    <div class="dv-spc-10">
                    </div>
                </div>
                <!-- DADOS DO PARTICIPANTE PRINCIPAL -->
                <div class="bloco-conteudo">
                    <div class="col12 ftl-left">
                        <b>DADOS DO TITULAR</b>
                        <div id="DvDadosPrincipal" runat="server"></div>
                    </div>
                    <div class="dv-spc-5-bottom">
                    </div>
                    <div class="dv-spc-10">
                    </div>
                </div>
                <!-- DADOS DO BENEFICIÁRIO -->
                <div class="bloco-conteudo">
                    <div class="col12 ftl-left">
                        <b>DADOS DOS BENEFICIÁRIOS</b>
                        <div id="DvDadosBeneficiario" runat="server"></div>
                    </div>
                    <div class="dv-spc-5-bottom">
                    </div>
                    <div class="dv-spc-10">
                    </div>
                </div>
                <!-- ITENS ADQUIRIDOS -->
                <div class="bloco-conteudo">
                    <div class="col12 ftl-left">
                        <b>ITENS ADQUIRIDOS</b>
                        <div id="DvDadosItens" runat="server"></div>
                    </div>
                    <div class="dv-spc-5-bottom">
                    </div>
                    <div class="dv-spc-10">
                    </div>
                </div>
                <!-- OBSERVAÇÕES -->
                <div class="bloco-conteudo">
                    <div class="col12 ftl-left">
                        <b>OBSERVAÇÕES IMPORTANTES</b><br />
                        <div id="DvDadosObservacoes" runat="server"></div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
