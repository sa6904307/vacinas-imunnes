﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;

public partial class prints_compra : System.Web.UI.Page
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            IniciarPagina();
    }

    /// <summary>
    /// 
    /// </summary>
    private void IniciarPagina()
    {
        try
        {
            int codigo = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["compra"]));

            ImunneVacinas.Compra compraSelecionada = ImunneVacinas.Compra.ConsultarUnico(codigo);
            ImunneVacinas.Campanha campanhaSelecionada = ImunneVacinas.Campanha.ConsultarUnico(compraSelecionada.IdCampanha.Codigo);
            ImunneVacinas.Clinica clinicaSelecionada = ImunneVacinas.Clinica.ConsultarUnico(compraSelecionada.IdClinica.Codigo);
            ImunneVacinas.Participante participanteSelecionado = ImunneVacinas.Participante.ConsultarUnico(compraSelecionada.IdParticipante.Codigo);
            ImunneVacinas.TransacaoRealizada transacaoSelecionada = ImunneVacinas.TransacaoRealizada.ConsultarUnicoCompra(compraSelecionada.Codigo);
            ImunneVacinas.Empresa empresaSelecionada = ImunneVacinas.Empresa.ConsultarUnico(campanhaSelecionada.IdEmpresa.Codigo);

            // BLOCO IDENTIFICAÇÕES

            StringBuilder dadosClinica = new StringBuilder();
            StringBuilder dadosEmpresa = new StringBuilder();

            #region DADOS DA CLINICA

            dadosClinica.AppendFormat(clinicaSelecionada.NomeFantasia.ToUpper());
            if (!String.IsNullOrEmpty(clinicaSelecionada.Endereco))
            {
                dadosClinica.AppendFormat(" - {0}", clinicaSelecionada.Endereco.ToUpper());
                dadosClinica.AppendFormat(", {0}", clinicaSelecionada.Numero);
            }

            if (clinicaSelecionada.IdCidade.Codigo > 0)
            {
                dadosClinica.AppendFormat(" - {0}", clinicaSelecionada.IdCidade.Nome.ToUpper());
                dadosClinica.AppendFormat("/{0}", clinicaSelecionada.IdUf.Sigla);
            }

            if (!String.IsNullOrEmpty(clinicaSelecionada.Telefone))
                dadosClinica.AppendFormat(" - TEL.: {0}", clinicaSelecionada.Telefone);

            #endregion

            #region DADOS DA EMPRESA

            dadosEmpresa.AppendFormat(empresaSelecionada.NomeFantasia.ToUpper());
            if (!String.IsNullOrEmpty(empresaSelecionada.Endereco))
            {
                dadosEmpresa.AppendFormat(" - {0}", empresaSelecionada.Endereco.ToUpper());
                dadosEmpresa.AppendFormat(", {0}", empresaSelecionada.Numero);
            }

            if (empresaSelecionada.IdCidade.Codigo > 0)
            {
                dadosEmpresa.AppendFormat(" - {0}", empresaSelecionada.IdCidade.Nome.ToUpper());
                dadosEmpresa.AppendFormat("/{0}", empresaSelecionada.IdUf.Sigla);
            }

            if (!String.IsNullOrEmpty(empresaSelecionada.Telefone))
                dadosEmpresa.AppendFormat(" - TEL.: {0}", empresaSelecionada.Telefone);

            #endregion

            LblDadosCampanha.Text = campanhaSelecionada.Identificacao.ToUpper() + " | " + campanhaSelecionada.Nome.ToUpper();
            LblDadosEmpresa.Text = dadosEmpresa.ToString();
            LblDadosClinica.Text = dadosClinica.ToString();

            #region DADOS HORÁRIOS

            ImunneVacinas.PesquisaHorarioSurto pesquisaHorarios = new ImunneVacinas.PesquisaHorarioSurto()
            {
                Compra = compraSelecionada.Codigo,
                Situacao = ImunneVacinas.Utils.SituacaoHorario.Confirmado
            };

            List<ImunneVacinas.HorarioSurto> listaHorarios = ImunneVacinas.HorarioSurto.ConsultarHorarios(pesquisaHorarios);

            #endregion

            #region DADOS TITULAR

            Page.Title = String.Format("{0} | {1}", compraSelecionada.Codigo.ToString().PadLeft(5, '0') + " | " + transacaoSelecionada.numeroComprovanteVenda, compraSelecionada.DataCompra.ToString("dd/MM/yyyy"));

            StringBuilder dadosPrincipal = new StringBuilder();
            dadosPrincipal.Append("<div class='col2 ftl-left'><b>COMPRA:</b></div>");
            dadosPrincipal.AppendFormat("<div class='col10 ftl-right'>{0} | {1}</div>", compraSelecionada.Codigo.ToString().PadLeft(5, '0') + " | " + transacaoSelecionada.numeroComprovanteVenda, compraSelecionada.DataCompra.ToString("dd/MM/yyyy"));

            dadosPrincipal.Append("<div class='col2 ftl-left'><b>NOME:</b></div>");
            dadosPrincipal.AppendFormat("<div class='col10 ftl-right'>{0}</div>", participanteSelecionado.Nome.ToUpper());

            if (!String.IsNullOrEmpty(participanteSelecionado.Cpf))
            {
                dadosPrincipal.Append("<div class='col2 ftl-left'><b>CPF/ID:</b></div>");

                if (ImunneVacinas.Utils.ValidarCpfCnpj(ImunneVacinas.Utils.LimparCpfCnpj(participanteSelecionado.Cpf)))
                    dadosPrincipal.AppendFormat("<div class='col10 ftl-right'>{0}</div>", ImunneVacinas.Utils.FormatarCpfCnpj(participanteSelecionado.Cpf));
                else
                    dadosPrincipal.AppendFormat("<div class='col10 ftl-right'>{0}</div>", participanteSelecionado.Cpf);
            }

            if (participanteSelecionado.DataNascimento != ImunneVacinas.Utils.dataPadrao)
            {
                dadosPrincipal.Append("<div class='col2 ftl-left'><b>NASCIMENTO:</b></div>");
                dadosPrincipal.AppendFormat("<div class='col10 ftl-right'>{0}</div>", participanteSelecionado.DataNascimento.ToString("dd/MM/yyyy"));
            }

            if (!String.IsNullOrEmpty(participanteSelecionado.Telefone))
            {
                dadosPrincipal.Append("<div class='col2 ftl-left'><b>TEL.:</b></div>");
                dadosPrincipal.AppendFormat("<div class='col10 ftl-right'>{0}</div>", participanteSelecionado.Telefone);
            }

            if (!String.IsNullOrEmpty(participanteSelecionado.Email))
            {
                dadosPrincipal.Append("<div class='col2 ftl-left'><b>E-MAIL:</b></div>");
                dadosPrincipal.AppendFormat("<div class='col10 ftl-right'>{0}</div>", participanteSelecionado.Email.ToLower());
            }

            DvDadosPrincipal.InnerHtml = dadosPrincipal.ToString();

            #endregion

            #region DADOS BENEFICIÁRIOS

            StringBuilder dadosBeneficiario = new StringBuilder();

            ImunneVacinas.PesquisaVoucher pesquisaVouchers = new ImunneVacinas.PesquisaVoucher()
            {
                Compra = codigo,
                Ordenacao = ImunneVacinas.Utils.TipoOrdenacao.porNome
            };
            List<ImunneVacinas.Voucher> listaVouchers = ImunneVacinas.Voucher.ConsultarVouchers(pesquisaVouchers);

            foreach (ImunneVacinas.Voucher item in listaVouchers)
            {
                ImunneVacinas.Participante beneficiarioSelecionado = ImunneVacinas.Participante.ConsultarUnico(item.IdParticipante.Codigo);

                dadosBeneficiario.Append("<div class='col2 ftl-left'><b>NOME:</b></div>");
                dadosBeneficiario.AppendFormat("<div class='col10 ftl-right'>{0}</div>", beneficiarioSelecionado.Nome.ToUpper());

                if (!String.IsNullOrEmpty(beneficiarioSelecionado.Cpf))
                {
                    dadosBeneficiario.Append("<div class='col2 ftl-left'><b>CPF/ID:</b></div>");
                    if (ImunneVacinas.Utils.ValidarCpfCnpj(ImunneVacinas.Utils.LimparCpfCnpj(beneficiarioSelecionado.Cpf)))
                        dadosBeneficiario.AppendFormat("<div class='col10 ftl-right'>{0}</div>", ImunneVacinas.Utils.FormatarCpfCnpj(beneficiarioSelecionado.Cpf));
                    else
                        dadosBeneficiario.AppendFormat("<div class='col10 ftl-right'>{0}</div>", beneficiarioSelecionado.Cpf);
                }

                if (beneficiarioSelecionado.DataNascimento != ImunneVacinas.Utils.dataPadrao)
                {
                    dadosBeneficiario.Append("<div class='col2 ftl-left'><b>NASCIMENTO:</b></div>");
                    dadosBeneficiario.AppendFormat("<div class='col10 ftl-right'>{0}</div>", beneficiarioSelecionado.DataNascimento.ToString("dd/MM/yyyy"));
                }

                if (!String.IsNullOrEmpty(beneficiarioSelecionado.Telefone))
                {
                    dadosBeneficiario.Append("<div class='col2 ftl-left'><b>TEL.:</b></div>");
                    dadosBeneficiario.AppendFormat("<div class='col10 ftl-right'>{0}</div>", beneficiarioSelecionado.Telefone);
                }

                if (!String.IsNullOrEmpty(beneficiarioSelecionado.Email))
                {
                    dadosBeneficiario.Append("<div class='col2 ftl-left'><b>E-MAIL:</b></div>");
                    dadosBeneficiario.AppendFormat("<div class='col10 ftl-right'>{0}</div>", beneficiarioSelecionado.Email.ToLower());
                }

                if (beneficiarioSelecionado.Parentesco.Codigo > 0)
                {
                    dadosBeneficiario.Append("<div class='col2 ftl-left'><b>PARENTESCO:</b></div>");
                    dadosBeneficiario.AppendFormat("<div class='col10 ftl-right'>{0}</div>", beneficiarioSelecionado.Parentesco.Nome);
                }

                foreach (ImunneVacinas.HorarioSurto horario in listaHorarios)
                {
                    if (horario.Participante == beneficiarioSelecionado.Codigo)
                    {
                        dadosBeneficiario.Append("<div class='col2 ftl-left'><b>AGENDADO PARA:</b></div>");
                        dadosBeneficiario.AppendFormat("<div class='col10 ftl-right'>{0}</div>", horario.InfoHoraro.ToString("dd/MM/yyyy HH:mm"));
                        break;
                    }
                }

                if (item.DataRetirada != ImunneVacinas.Utils.dataPadrao)
                {
                    dadosBeneficiario.Append("<div class='col2 ftl-left'><b>APLICADO:</b></div>");
                    dadosBeneficiario.AppendFormat("<div class='col10 ftl-right'>{0}</div>", item.DataRetirada.ToString("dd/MM/yyyy HH:mm:ss"));
                }

                dadosBeneficiario.Append("<div class='col12 ftl-left'><b>&nbsp;</b></div>");
            }

            DvDadosBeneficiario.InnerHtml = dadosBeneficiario.ToString();

            #endregion

            #region ITENS COMPRA

            StringBuilder dadosItens = new StringBuilder();

            ImunneVacinas.PesquisaItemCompra pesquisa = new ImunneVacinas.PesquisaItemCompra()
            {
                Compra = compraSelecionada.Codigo,
                Ordenacao = ImunneVacinas.Utils.TipoOrdenacao.porNome
            };

            List<ImunneVacinas.ItemCompra> listaItens = ImunneVacinas.ItemCompra.ConsultarItens(pesquisa);
            foreach (ImunneVacinas.ItemCompra item in listaItens)
            {
                string valorProduto = String.Format("{0} ({1}x {2})", ImunneVacinas.Utils.ToMoeda(item.Valor), item.Quantidade, ImunneVacinas.Utils.ToMoeda(Convert.ToInt32(item.Valor / item.Quantidade)));

                dadosItens.Append("<div class='col2 ftl-left'><b>ITEM:</b></div>");
                dadosItens.AppendFormat("<div class='col10 ftl-right'>{0} | {1}</div>", item.IdVacina.Nome.ToUpper().ToUpper(), valorProduto);
            }

            DvDadosItens.InnerHtml = dadosItens.ToString();

            #endregion

            StringBuilder dadosObservacoes = new StringBuilder();

            if (listaHorarios != null && listaHorarios.Count > 0)
                dadosObservacoes.Append("Os horários de aplicações escolhidos pelo adquirinte estão especificados em cada beneficiário.<br/>");
            else dadosObservacoes.Append("Adquirinte não específicou um horário para aplicação.<br/>");

            dadosObservacoes.AppendFormat("As aplicações desta campanha se iniciam em <b>{0}</b>.<br />", campanhaSelecionada.DataAbertura.ToString("dd/MM/yyyy"));
            dadosObservacoes.AppendFormat("Os itens adquiridos estarão disponíveis até a data de <b>{0}</b>.", campanhaSelecionada.DataLimite.ToString("dd/MM/yyyy"));
            DvDadosObservacoes.InnerHtml = dadosObservacoes.ToString();
        }
        catch (Exception ex)
        {
            ImunneVacinas.Erro erro = new ImunneVacinas.Erro();
            erro.addArquivo(ex);
            //ImunneVacinas.Utils.MostrarAviso(this.Page, ex.Message);
        }
    }
}