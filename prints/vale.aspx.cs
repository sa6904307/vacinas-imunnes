﻿using System;
using System.Collections.Generic;
using System.Text;

public partial class prints_vale : System.Web.UI.Page
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            IniciarPagina();
    }

    /// <summary>
    /// 
    /// </summary>
    private void IniciarPagina()
    {
        Response.Redirect("~/", false);
        Context.ApplicationInstance.CompleteRequest();

        //try
        //{
        //    int codigoCompra = 0;
        //    int codigoParticipante = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["participante"]));
        //    int codigoCampanha = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["campanha"]));
        //    int codigoClinica = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["clinica"]));
        //    int totalCompras = 0;

        //    ImunneVacinas.PesquisaCompra pesquisaCompra = new ImunneVacinas.PesquisaCompra()
        //    {
        //        Campanha = codigoCampanha,
        //        Participante = codigoParticipante,
        //        Categoria = ImunneVacinas.Compra.CategoriaCompra.Gratis
        //    };

        //    List<ImunneVacinas.Compra> listaCompras = ImunneVacinas.Compra.ConsultarComprar(pesquisaCompra);

        //    if (listaCompras == null || listaCompras.Count <= 0)
        //        codigoCompra = GerarCompraFree(codigoParticipante, codigoClinica, codigoCampanha);
        //    else codigoCompra = listaCompras[0].Codigo;

        //    int codigoVoucher = GerarVoucher(codigoParticipante, codigoClinica, codigoCampanha, codigoCompra);

        //    ImunneVacinas.Voucher voucherSelecionado = ImunneVacinas.Voucher.ConsultarUnico(codigoVoucher);
        //    ImunneVacinas.Compra compraSelecionada = ImunneVacinas.Compra.ConsultarUnico(codigoCompra);
        //    ImunneVacinas.Campanha campanhaSelecionada = ImunneVacinas.Campanha.ConsultarUnico(codigoCampanha);
        //    ImunneVacinas.Clinica clinicaSelecionada = ImunneVacinas.Clinica.ConsultarUnico(codigoClinica);
        //    ImunneVacinas.Participante participanteSelecionado = ImunneVacinas.Participante.ConsultarUnico(codigoParticipante);
        //    ImunneVacinas.Empresa empresaSelecionada = ImunneVacinas.Empresa.ConsultarUnico(campanhaSelecionada.IdEmpresa.Codigo);
        //    ImunneVacinas.TransacaoRealizada transacaoSelecionada = ImunneVacinas.TransacaoRealizada.ConsultarUnicoCompra(compraSelecionada.Codigo);

        //    Page.Title = "VALE-VACINA - " + participanteSelecionado.Nome.ToUpper();

        //    if (!String.IsNullOrEmpty(empresaSelecionada.Logotipo))
        //        ImgLogoEmpresa.ImageUrl = "~/img/empresas/" + empresaSelecionada.Logotipo;
        //    else
        //        ImgLogoEmpresa.Visible = false;

        //    if (voucherSelecionado.Situacao == ImunneVacinas.Voucher.SituacaoVoucher.Gerado)
        //    {
        //        voucherSelecionado.DataImpressao = DateTime.Now;
        //        voucherSelecionado.Alterar();
        //    }

        //    //switch (voucherSelecionado.Situacao)
        //    //{
        //    //    case ImunneVacinas.Voucher.SituacaoVoucher.Gerado:
        //    //        {
        //    //            LblDadosVale.Text = "DISPONÍVEL PARA UTILIZAÇÃO";
        //    //        }
        //    //        break;
        //    //    case ImunneVacinas.Voucher.SituacaoVoucher.Impresso:
        //    //        {
        //    //            LblDadosVale.Text = "DISPONÍVEL PARA UTILIZAÇÃO, JÁ IMPRESSO EM " + voucherSelecionado.DataImpressao.ToString("dd/MM/yyyy");
        //    //        }
        //    //        break;
        //    //    case ImunneVacinas.Voucher.SituacaoVoucher.Utilizado:
        //    //        {
        //    //            LblDadosVale.Text = "VALE JÁ UTILIZADO EM " + voucherSelecionado.DataRetirada.ToString("dd/MM/yyyy");
        //    //        }
        //    //        break;
        //    //}

        //    // BLOCO IDENTIFICAÇÕES
        //    StringBuilder dadosClinica = new StringBuilder();
        //    StringBuilder dadosEmpresa = new StringBuilder();

        //    #region Dados da Clinica

        //    dadosClinica.AppendFormat(clinicaSelecionada.NomeFantasia.ToUpper());
        //    if (!String.IsNullOrEmpty(clinicaSelecionada.Endereco))
        //    {
        //        dadosClinica.AppendFormat(" - {0}", clinicaSelecionada.Endereco);
        //        dadosClinica.AppendFormat(", {0}", clinicaSelecionada.Numero);
        //    }

        //    if (clinicaSelecionada.IdCidade.Codigo > 0)
        //    {
        //        dadosClinica.AppendFormat(" - {0}", clinicaSelecionada.IdCidade.Nome);
        //        dadosClinica.AppendFormat("/{0}", clinicaSelecionada.IdUf.Sigla);
        //    }

        //    if (!String.IsNullOrEmpty(clinicaSelecionada.Telefone))
        //        dadosClinica.AppendFormat(" - TEL.: {0}", clinicaSelecionada.Telefone);

        //    #endregion

        //    #region Dados da Empresa

        //    dadosEmpresa.AppendFormat(empresaSelecionada.NomeFantasia.ToUpper());
        //    if (!String.IsNullOrEmpty(empresaSelecionada.Endereco))
        //    {
        //        dadosEmpresa.AppendFormat(" - {0}", empresaSelecionada.Endereco);
        //        dadosEmpresa.AppendFormat(", {0}", empresaSelecionada.Numero);
        //    }

        //    if (empresaSelecionada.IdCidade.Codigo > 0)
        //    {
        //        dadosEmpresa.AppendFormat(" - {0}", empresaSelecionada.IdCidade.Nome);
        //        dadosEmpresa.AppendFormat("/{0}", empresaSelecionada.IdUf.Sigla);
        //    }

        //    if (!String.IsNullOrEmpty(empresaSelecionada.Telefone))
        //        dadosEmpresa.AppendFormat(" - TEL.: {0}", empresaSelecionada.Telefone);

        //    #endregion

        //    LblDadosCampanha.Text = campanhaSelecionada.Identificacao + " | " + campanhaSelecionada.Nome.ToUpper();
        //    LblDadosEmpresa.Text = dadosEmpresa.ToString();
        //    LblDadosClinica.Text = dadosClinica.ToString();

        //    #region Dados Adquirente

        //    StringBuilder dadosPrincipal = new StringBuilder();
        //    dadosPrincipal.Append("<div class='col2 ftl-left'><b>COMPRA:</b></div>");
        //    dadosPrincipal.AppendFormat("<div class='col10 ftl-right'>{0} | {1}</div>", transacaoSelecionada.numeroComprovanteVenda, compraSelecionada.DataCompra.ToString("dd/MM/yyyy"));

        //    dadosPrincipal.Append("<div class='col2 ftl-left'><b>NOME:</b></div>");
        //    dadosPrincipal.AppendFormat("<div class='col10 ftl-right'>{0}</div>", participanteSelecionado.Nome.ToUpper());

        //    if (!String.IsNullOrEmpty(participanteSelecionado.Cpf))
        //    {
        //        dadosPrincipal.Append("<div class='col2 ftl-left'><b>CPF:</b></div>");
        //        dadosPrincipal.AppendFormat("<div class='col10 ftl-right'>{0}</div>", ImunneVacinas.Utils.FormatarCpfCnpj(participanteSelecionado.Cpf));
        //    }

        //    if (participanteSelecionado.DataNascimento != ImunneVacinas.Utils.dataPadrao)
        //    {
        //        dadosPrincipal.Append("<div class='col2 ftl-left'><b>NASCIMENTO:</b></div>");
        //        dadosPrincipal.AppendFormat("<div class='col10 ftl-right'>{0}</div>", participanteSelecionado.DataNascimento.ToString("dd/MM/yyyy"));
        //    }

        //    if (!String.IsNullOrEmpty(participanteSelecionado.Telefone))
        //    {
        //        dadosPrincipal.Append("<div class='col2 ftl-left'><b>TEL.:</b></div>");
        //        dadosPrincipal.AppendFormat("<div class='col10 ftl-right'>{0}</div>", participanteSelecionado.Telefone);
        //    }

        //    if (!String.IsNullOrEmpty(participanteSelecionado.Email))
        //    {
        //        dadosPrincipal.Append("<div class='col2 ftl-left'><b>E-MAIL:</b></div>");
        //        dadosPrincipal.AppendFormat("<div class='col10 ftl-right'>{0}</div>", participanteSelecionado.Email.ToLower());
        //    }

        //    #endregion

        //    DvDadosPrincipal.InnerHtml = dadosPrincipal.ToString();

        //    #region Itens

        //    StringBuilder dadosItens = new StringBuilder();

        //    ImunneVacinas.PesquisaItemCompra pesquisa = new ImunneVacinas.PesquisaItemCompra()
        //    {
        //        Compra = compraSelecionada.Codigo,
        //        Ordenacao = ImunneVacinas.Utils.TipoOrdenacao.porNome
        //    };

        //    List<ImunneVacinas.ItemCompra> listaItens = ImunneVacinas.ItemCompra.ConsultarItens(pesquisa);
        //    foreach (ImunneVacinas.ItemCompra item in listaItens)
        //    {
        //        dadosItens.Append("<div class='col2 ftl-left'><b>ITEM:</b></div>");
        //        dadosItens.AppendFormat("<div class='col10 ftl-right'>1 DOSE | {0}</div>", item.IdVacina.Nome.ToUpper());
        //    }

        //    DvDadosItens.InnerHtml = dadosItens.ToString();

        //    #endregion

        //    //DvDadosObservacoes.InnerText = "Os itens adquiridos estarão disponíveis até a data de " + campanhaSelecionada.DataLimite.ToString("dd/MM/yyyy") + ".";
        //    StringBuilder dadosObservacoes = new StringBuilder();
        //    dadosObservacoes.AppendFormat("As aplicações desta campanha se iniciam em <b>{0}</b>.<br />", campanhaSelecionada.DataAbertura.ToString("dd/MM/yyyy"));
        //    dadosObservacoes.AppendFormat("Os itens adquiridos estarão disponíveis até a data de <b>{0}</b>.", campanhaSelecionada.DataLimite.ToString("dd/MM/yyyy"));
        //    DvDadosObservacoes.InnerHtml = dadosObservacoes.ToString();
        //}
        //catch (Exception ex)
        //{

        //}
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="codigoParticipante"></param>
    /// <param name="codigoClinica"></param>
    /// <param name="codigoCampanha"></param>
    /// <returns></returns>
    private int GerarCompraFree(int codigoParticipante, int codigoClinica, int codigoCampanha)
    {
        int codigoRetorno = 0;

        try
        {
            ImunneVacinas.Compra novaCompra = new ImunneVacinas.Compra()
            {
                Categoria = ImunneVacinas.Compra.CategoriaCompra.Gratis,
                DataCompra = DateTime.Now,
                Parcelas = 1,
                Situacao = ImunneVacinas.Compra.SituacaoCompra.Concluida,
                Valor = 0,
            };
            novaCompra.IdCampanha.Codigo = codigoCampanha;
            novaCompra.IdClinica.Codigo = codigoClinica;
            novaCompra.IdParticipante.Codigo = codigoParticipante;

            codigoRetorno = novaCompra.Incluir();

            ImunneVacinas.PesquisaProdutoCampanha pesquisaProduto = new ImunneVacinas.PesquisaProdutoCampanha()
            {
                Campanha = codigoCampanha
            };
            List<ImunneVacinas.ProdutoCampanha> listaProdutos = ImunneVacinas.ProdutoCampanha.ConsultarProdutoCampanhas(pesquisaProduto);

            decimal totalValorCompra = 0;

            foreach(ImunneVacinas.ProdutoCampanha produto in listaProdutos)
            {
                ImunneVacinas.ItemCompra novoItem = new ImunneVacinas.ItemCompra();
                novoItem.IdCompra.Codigo = codigoRetorno;
                novoItem.IdVacina.Codigo = produto.IdVacina.Codigo;
                novoItem.DataCriacao = novaCompra.DataCompra;
                novoItem.Quantidade = 1;
                novoItem.Valor = 1 * produto.Preco;
                novoItem.Observacoes = String.Format("{0} UNIDADE(S) DE {1}.", 1, produto.IdVacina.Nome.ToUpper());
                novoItem.Incluir();

                totalValorCompra += novoItem.Valor;
            }

            novaCompra = ImunneVacinas.Compra.ConsultarUnico(codigoRetorno);
            novaCompra.Valor = totalValorCompra;
            novaCompra.Alterar();

            string valorTransacao = ImunneVacinas.Utils.RetornaValorJson(totalValorCompra.ToString());
            int valorOficial = Convert.ToInt32(valorTransacao);

            ImunneVacinas.TransacaoRealizada novaTransacao = new ImunneVacinas.TransacaoRealizada();
            novaTransacao.autorizacao = "IMUNNEVACINAS";
            novaTransacao.codigoEstabelecimento = "1550561256295";
            novaTransacao.compraTransacao.Codigo = codigoRetorno;
            //novaTransacao.valor = totalValorCompra;
            novaTransacao.valor = valorOficial;
            novaTransacao.numeroComprovanteVenda = "FREE" + ImunneVacinas.Utils.GerarIdRandomica(10);
            novaTransacao.mensagemVenda = "COMPRADA PELA EMPRESA DO PARTICIPANTE";
            novaTransacao.Incluir();

            return codigoRetorno;
        }
        catch (Exception ex)
        {
            return codigoRetorno;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="codigoParticipante"></param>
    /// <param name="codigoClinica"></param>
    /// <param name="codigoCampanha"></param>
    /// <param name="codigoCompra"></param>
    /// <returns></returns>
    private int GerarVoucher(int codigoParticipante, int codigoClinica, int codigoCampanha, int codigoCompra)
    {
        int codigoRetorno = 0;
        ImunneVacinas.Participante participanteSelecionado = ImunneVacinas.Participante.ConsultarUnico(codigoParticipante);
        ImunneVacinas.Compra compraSelecionada = ImunneVacinas.Compra.ConsultarUnico(codigoCompra);
        ImunneVacinas.Campanha campanhaSelecionada = ImunneVacinas.Campanha.ConsultarUnico(codigoCampanha);
        ImunneVacinas.Clinica clinicaSelecionada = ImunneVacinas.Clinica.ConsultarUnico(codigoClinica);

        ImunneVacinas.Voucher voucherSelecionado = ImunneVacinas.Voucher.ConsultarUnico(codigoParticipante, codigoClinica, codigoCampanha, codigoCompra);
        if (voucherSelecionado == null)
        {
            StringBuilder descricaoVoucher = new StringBuilder();
            descricaoVoucher.AppendFormat("{0} ADQUIRIU PRODUTOS DA CAMPANHA {1}", participanteSelecionado.Nome.ToUpper(), campanhaSelecionada.Identificacao.ToString());
            descricaoVoucher.AppendFormat(" QUE SERÃO APLICADOS NA CLÍNICA {0}.\r\nABAIXO OS PRODUTOS:\r\n\r\n", clinicaSelecionada.NomeFantasia.ToUpper());

            ImunneVacinas.PesquisaProdutoCampanha pesquisaProduto = new ImunneVacinas.PesquisaProdutoCampanha()
            {
                Campanha = codigoCampanha
            };
            List<ImunneVacinas.ProdutoCampanha> listaProdutos = ImunneVacinas.ProdutoCampanha.ConsultarProdutoCampanhas(pesquisaProduto);

            foreach (ImunneVacinas.ProdutoCampanha produto in listaProdutos)
            {
                descricaoVoucher.AppendFormat("1 UNIDADE(S) DE {0}.\r\n", produto.IdVacina.Nome.ToUpper());
            }

            descricaoVoucher.AppendFormat("\r\nCOMPRAS REALIZADAS POR {0}.", participanteSelecionado.Nome.ToUpper());

            ImunneVacinas.Voucher novoVoucher = new ImunneVacinas.Voucher();
            novoVoucher.IdCampanha.Codigo = codigoCampanha;
            novoVoucher.IdClinica.Codigo = codigoClinica;
            novoVoucher.IdCompra.Codigo = codigoCompra;
            novoVoucher.IdParticipante.Codigo = codigoParticipante;
            novoVoucher.DataCriacao = DateTime.Now;
            novoVoucher.Descricao = descricaoVoucher.ToString();
            novoVoucher.Situacao = ImunneVacinas.Voucher.SituacaoVoucher.Gerado;

            codigoRetorno = novoVoucher.Incluir();
        }
        else codigoRetorno = voucherSelecionado.Codigo;

        return codigoRetorno;
    }
}