﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class horarios : System.Web.UI.Page
{
    decimal larguraItem = 0;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        //LnkSemAgenda.OnClientClick = "return ConfirmarSemAgenda(this, event, -1');";
        Page.Title = "S&A Imunizações | Escolha de Horário";

        if (!IsPostBack)
            IniciarPagina();
    }

    /// <summary>
    /// 
    /// </summary>
    private void IniciarPagina()
    {
        try
        {
            int codigoParticipante = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["participante"]));
            int codigoCompra = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["compra"]));
            int codigoCampanha = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["campanha"]));
            int codigoClinica = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["clinica"]));
            int qtde = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["qtde"]));
            string[] membros = new string[1];
            string[] itens = new string[1];

            ImunneVacinas.Participante participanteSelecionado = ImunneVacinas.Participante.ConsultarUnico(codigoParticipante);
            ImunneVacinas.Campanha campanhaSelecionada = ImunneVacinas.Campanha.ConsultarUnico(codigoCampanha);
            ImunneVacinas.Compra compraSelecionada = ImunneVacinas.Compra.ConsultarUnico(codigoCompra);
            ImunneVacinas.Clinica clinicaSelecionada = ImunneVacinas.Clinica.ConsultarUnico(codigoClinica);

            LblNascimento.Text = participanteSelecionado.DataNascimento.ToString("dd/MM/yyyy");
            LblNome.Text = participanteSelecionado.Nome.ToUpper();
            LblCPF.Text = ImunneVacinas.Utils.FormatarCpfCnpj(participanteSelecionado.Cpf);

            LblValor.Text = ImunneVacinas.Utils.ToMoeda(compraSelecionada.Valor);

            LblCampanha.Text = campanhaSelecionada.Identificacao.ToUpper() + " | " + campanhaSelecionada.Nome.ToUpper();
            LblValidade.Text = campanhaSelecionada.DataLimite.ToString("dd/MM/yyyy");
            LblClinica.Text = clinicaSelecionada.NomeFantasia.ToUpper() + "<br />"
                            + clinicaSelecionada.Endereco + ", "
                            + clinicaSelecionada.Numero + " - "
                            + clinicaSelecionada.IdCidade.Nome + "/"
                            + clinicaSelecionada.IdUf.Sigla;
            LblEmpresa.Text = campanhaSelecionada.IdEmpresa.NomeFantasia.ToUpper();

            LblSituacao.Text = compraSelecionada.Situacao.ToString().ToUpper();
            LblDataCompra.Text = compraSelecionada.DataCompra.ToString("dd/MM/yyyy");

            if (!String.IsNullOrEmpty(Request.QueryString["membros"]))
                membros = ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["membros"]).Split('|');

            if (!String.IsNullOrEmpty(Request.QueryString["itens"]))
                itens = ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["itens"]).Split('|');

            ImunneVacinas.Campanha campanha = ImunneVacinas.Campanha.ConsultarUnico(codigoCampanha);

            for (DateTime diaInicio = campanha.DataAbertura; diaInicio <= campanha.DataLimite; diaInicio.AddDays(1))
            {
                DdlDias.Items.Add(new ListItem(diaInicio.ToString("dd/MM/yyyy"), diaInicio.ToString("dd/MM/yyyy")));
                diaInicio = diaInicio.AddDays(1);
            }

            ConsultarRegistros();
        }
        catch (Exception ex)
        {

        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="codigoCampanha"></param>
    private void ConsultarRegistros()
    {
        int codigoParticipante = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["participante"]));
        int codigoCompra = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["compra"]));
        int codigoCampanha = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["campanha"]));
        int codigoClinica = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["clinica"]));
        int qtde = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["qtde"]));
        string[] membros = new string[1];
        string[] itens = new string[1];

        if (!String.IsNullOrEmpty(Request.QueryString["membros"]))
            membros = ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["membros"]).Split('|');

        if (!String.IsNullOrEmpty(Request.QueryString["itens"]))
            itens = ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["itens"]).Split('|');

        ImunneVacinas.Campanha campanhaSelecionada = ImunneVacinas.Campanha.ConsultarUnico(codigoCampanha);

        ImunneVacinas.PesquisaHorarioSurto pesquisa = new ImunneVacinas.PesquisaHorarioSurto()
        {
            Dia = Convert.ToDateTime(DdlDias.SelectedValue),
            Campanha = campanhaSelecionada.Codigo,
            Ordenacao = ImunneVacinas.Utils.TipoOrdenacao.porData
        };

        larguraItem = 0;

        DateTime dataAcao = Convert.ToDateTime(DdlDias.SelectedValue);
        int totalHora = campanhaSelecionada.TotalAtendimentos;

        List<ImunneVacinas.HorarioSurto> horariosCampanha = ImunneVacinas.HorarioSurto.ConsultarHorarios(pesquisa);
        larguraItem = Convert.ToDecimal(100 / Convert.ToDecimal(totalHora));

        DtlHorarios.DataSource = horariosCampanha;
        DtlHorarios.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void DtlHorarios_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        ImunneVacinas.HorarioSurto item = (ImunneVacinas.HorarioSurto)e.Item.DataItem;

        HtmlGenericControl DvItemHorario = (HtmlGenericControl)e.Item.FindControl("DvItemHorario");

        HiddenField HdfCodigo = (HiddenField)e.Item.FindControl("HdfCodigo");

        LinkButton LnkHorario = (LinkButton)e.Item.FindControl("LnkHorario");

        Label LblHorario = (Label)e.Item.FindControl("LblHorario");
        Label LblData = (Label)e.Item.FindControl("LblData");
        Label LblSituacao = (Label)e.Item.FindControl("LblSituacao");

        DvItemHorario.Style.Add("width", larguraItem.ToString().Replace(',', '.') + "%");
        LblHorario.Text = item.InfoHoraro.ToString("HH:mm");
        LblData.Text = item.InfoHoraro.ToString("dd/MM/yyyy");

        HdfCodigo.Value = item.Codigo.ToString();

        LnkHorario.CommandArgument = item.Codigo.ToString();

        //DvItemHorario.Attributes.Add("class", "item-horario-ocupado");

        if (item.Situacao == ImunneVacinas.Utils.SituacaoHorario.Confirmado)
        {
            LnkHorario.Enabled = false;
            DvItemHorario.Attributes.Add("class", "item-horario-ocupado");
            LblSituacao.Text = "RESERVADO";
        }
        else LblSituacao.Text = "DISPONÍVEL";

        LnkHorario.OnClientClick = "return ConfirmacaoExclucao(this, event,'" + item.Codigo + "');";
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void DtlHorarios_ItemCommand(object source, DataListCommandEventArgs e)
    {
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BtnConfirmar_Click(object sender, EventArgs e)
    {
        DateTime dataAcao = Convert.ToDateTime(DdlDias.SelectedValue);

        int codigoParticipante = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["participante"]));
        int codigoCompra = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["compra"]));
        int codigoCampanha = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["campanha"]));
        int codigoClinica = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["clinica"]));
        int qtde = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["qtde"]));
        string[] membros = new string[1];
        string[] itens = new string[1];

        if (!String.IsNullOrEmpty(Request.QueryString["membros"]))
            membros = ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["membros"]).Split('|');

        if (!String.IsNullOrEmpty(Request.QueryString["itens"]))
            itens = ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["itens"]).Split('|');

        int codigoRegistro = Convert.ToInt32(HdfCodigo.Value);

        ImunneVacinas.HorarioSurto item = ImunneVacinas.HorarioSurto.ConsultarUnico(codigoRegistro);
        if (item.Situacao == ImunneVacinas.Utils.SituacaoHorario.Confirmado)
        {
            ConsultarRegistros();

            ImunneVacinas.Alerta.showMensagemAviso(this.Page, "S&A Imunizações", "O horário escolhido acabou de ser selecionado por um outro usuário, escolha um novo!");
            return;
        }
        else
        {
            ImunneVacinas.PesquisaHorarioSurto pesquisaDisponiveis = new ImunneVacinas.PesquisaHorarioSurto()
            {
                Campanha = codigoCampanha,
                CodigoInicial = codigoRegistro,
                Situacao = ImunneVacinas.Utils.SituacaoHorario.Livre,
                Dia = dataAcao
            };

            List<ImunneVacinas.HorarioSurto> listaHorariosLivres = ImunneVacinas.HorarioSurto.ConsultarHorarios(pesquisaDisponiveis);
            int totaisLivres = ImunneVacinas.HorarioSurto.ContabilizarHorarios(pesquisaDisponiveis);
            if (totaisLivres < membros.Length)
            {
                ImunneVacinas.Alerta.showMensagemAviso(this.Page, "S&A Imunizações",
                             String.Format("Informamos que você necessidade de {0} disponíveis a partir "
                             + " do horário {1} que você selecionou."
                             + " Selecione um horário anterior no dia {2} ou consulte outro dia."
                             , membros.Length.ToString().PadLeft(2, '0'), item.InfoHoraro.ToString("HH:mm"), DdlDias.SelectedItem.Text));
                codigoRegistro = 0;
                HdfCodigo.Value = String.Empty;
            }
            else
            {
                int contador = 0;
                foreach (ImunneVacinas.HorarioSurto horario in listaHorariosLivres)
                {
                    horario.Compra = codigoCompra;
                    horario.Participante = Convert.ToInt32(membros[contador]);
                    horario.Situacao = ImunneVacinas.Utils.SituacaoHorario.Confirmado;
                    horario.Alterar();

                    if ((contador + 1) >= membros.Length)
                        break;

                    contador += 1;
                }

                StringBuilder urlPagina = new StringBuilder();

                urlPagina.AppendFormat("~/vouchers.aspx?campanha={0}", Request.QueryString["campanha"]);
                urlPagina.AppendFormat("&clinica={0}", Request.QueryString["clinica"]);
                urlPagina.AppendFormat("&participante={0}", Request.QueryString["participante"]);
                urlPagina.AppendFormat("&itens={0}", Request.QueryString["itens"]);
                urlPagina.AppendFormat("&qtde={0}", Request.QueryString["qtde"]);
                urlPagina.AppendFormat("&compra={0}", Request.QueryString["compra"]);
                urlPagina.AppendFormat("&membros={0}", Request.QueryString["membros"]);

                Response.Redirect(urlPagina.ToString(), false);
                Context.ApplicationInstance.CompleteRequest();
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void LnkPesquisar_Click(object sender, EventArgs e)
    {
        ConsultarRegistros();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void LnkSemAgenda_Click(object sender, EventArgs e)
    {
        StringBuilder urlPagina = new StringBuilder();

        urlPagina.AppendFormat("~/vouchers.aspx?campanha={0}", Request.QueryString["campanha"]);
        urlPagina.AppendFormat("&clinica={0}", Request.QueryString["clinica"]);
        urlPagina.AppendFormat("&participante={0}", Request.QueryString["participante"]);
        urlPagina.AppendFormat("&itens={0}", Request.QueryString["itens"]);
        urlPagina.AppendFormat("&qtde={0}", Request.QueryString["qtde"]);
        urlPagina.AppendFormat("&compra={0}", Request.QueryString["compra"]);
        urlPagina.AppendFormat("&membros={0}", Request.QueryString["membros"]);

        Response.Redirect(urlPagina.ToString(), false);
        Context.ApplicationInstance.CompleteRequest();
    }
}