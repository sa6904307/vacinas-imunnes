﻿using System;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class adesoes_locais : System.Web.UI.Page
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            IniciarPagina();
    }

    /// <summary>
    /// 
    /// </summary>
    private void IniciarPagina()
    {
        int codigoCampanha = -1;
        StringBuilder dataValidade = new StringBuilder();
        StringBuilder dataAplicacoes = new StringBuilder();

        try
        {
            if (!String.IsNullOrEmpty(Request.QueryString["campanha"]))
            {
                codigoCampanha = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["campanha"]));
                ImunneVacinas.Campanha campanhaSelecionada = ImunneVacinas.Campanha.ConsultarUnico(codigoCampanha);
                LblCampanha.Text = campanhaSelecionada.Identificacao + " | " + campanhaSelecionada.Nome.ToUpper();

                if (campanhaSelecionada.DataInicio == campanhaSelecionada.DataTermino)
                    dataValidade.AppendFormat("VÁLIDA A PARTIR DE {0}.", campanhaSelecionada.DataInicio.ToString("dd/MM/yyyy"));
                else dataValidade.AppendFormat("VÁLIDA DE {0} ATÉ {1}.", campanhaSelecionada.DataInicio.ToString("dd/MM/yyyy"), campanhaSelecionada.DataTermino.ToString("dd/MM/yyyy"));

                if (campanhaSelecionada.DataAbertura == campanhaSelecionada.DataLimite)
                    dataAplicacoes.AppendFormat("APLICAÇÕES A PARTIR DE {0}.", campanhaSelecionada.DataAbertura.ToString("dd/MM/yyyy"));
                else dataAplicacoes.AppendFormat("APLICAÇÕES DE {0} ATÉ {1}.", campanhaSelecionada.DataAbertura.ToString("dd/MM/yyyy"), campanhaSelecionada.DataLimite.ToString("dd/MM/yyyy"));

                LblValidade.Text = dataValidade.ToString();
                LblAplicacoes.Text = dataAplicacoes.ToString();

                Page.Title = "S&A Imunizações | " + campanhaSelecionada.Nome.ToUpper();

                ConsultarLocais(codigoCampanha);
            }
            else
            {
                Response.Redirect("~/", false);
                Context.ApplicationInstance.CompleteRequest();
            }
        }
        catch (Exception ex)
        {
            this.SalvarException(ex);
            Response.Redirect("~/", false);
            Context.ApplicationInstance.CompleteRequest();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void RptPrincipal_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        StringBuilder urlPagina = new StringBuilder();
        StringBuilder parametros = new StringBuilder();

        int codigoRegistro = Convert.ToInt32(e.CommandArgument);
        string comando = e.CommandName;

        switch (comando)
        {
            case "selecionar":
                {
                    urlPagina.AppendFormat("~/adesoes/conclusao.aspx?campanha={0}", Request.QueryString["campanha"]);
                    urlPagina.AppendFormat("&participante={0}", Request.QueryString["participante"]);
                    urlPagina.AppendFormat("&local={0}", ImunneVacinas.Criptografia.Criptografar(codigoRegistro.ToString()));
                    Response.Redirect(urlPagina.ToString(), false);
                    Context.ApplicationInstance.CompleteRequest();
                }
                break;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void RptPrincipal_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        ImunneVacinas.LocalAdesao item = (ImunneVacinas.LocalAdesao)e.Item.DataItem;

        Label LblNomeClinica = (Label)e.Item.FindControl("LblNomeClinica");

        LinkButton LnkSelecionar = (LinkButton)e.Item.FindControl("LnkSelecionar");

        LblNomeClinica.Text = String.Format("{0} | {1}", item.Nome.ToUpper(), item.Endereco);

        LnkSelecionar.CommandArgument = item.Codigo.ToString();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void LnkPesquisar_Click(object sender, EventArgs e)
    {
        int codigoCampanha = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["campanha"]));
        ConsultarLocais(codigoCampanha);
    }

    /// <summary>
    /// 
    /// </summary>
    private void ConsultarLocais(int codigoCampanha)
    {
        ImunneVacinas.PesquisaLocalAdesao pesquisa = new ImunneVacinas.PesquisaLocalAdesao()
        {
            CampoLivre = TxtCampoLivre.Text,
            Campanha = codigoCampanha,
            Ordenacao = ImunneVacinas.Utils.TipoOrdenacao.porNome
        };

        RptPrincipal.DataSource = ImunneVacinas.LocalAdesao.ConsultarLocaisAdesoes(pesquisa);
        RptPrincipal.DataBind();
    }
}