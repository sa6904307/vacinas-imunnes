﻿<%@ Page Title="" Language="C#" MasterPageFile="~/adesoes/imunne.master" AutoEventWireup="true" CodeFile="termos.aspx.cs" Inherits="imunne_termos" MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section class="subheader">
        <div class="container">
            <div class="row">
                <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                    <i class="fa fa-file-text-o"></i>&nbsp;Termos e Condições - Adesões
                </div>
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 text-right">
                    <a href="javascript:history.back()" title="Voltar à página anterior">
                        <i class="fa fa-chevron-left"></i>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
                <div class="caixa-branca">
                    <div class="step-header">
                        INFORMAÇÕES DA CAMPANHA
                    </div>
                    <div class="box-inside">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                            <label><b>CAMPANHA</b></label>
                            <asp:Label ID="LblCampanha" runat="server" CssClass="label-identificacao"></asp:Label>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                            <label><b>EMPRESA</b></label>
                            <asp:Label ID="LblEmpresa" runat="server" CssClass="label-identificacao"></asp:Label>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                            <label><b>VALIDADE</b></label>
                            <asp:Label ID="LblValidade" runat="server" CssClass="label-identificacao"></asp:Label>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                            <label><b>APLICAÇÕES</b></label>
                            <asp:Label ID="LblAplicacoes" runat="server" CssClass="label-identificacao"></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                <asp:UpdatePanel ID="UpdCampanha" runat="server">
                    <ContentTemplate>
                        <asp:Panel ID="PnlTermosGerais" runat="server">
                            <div class="caixa-branca">
                                <div class="step-header">
                                    TERMOS E CONDIÇÕES
                                </div>
                                <div class="box-inside">
                                    <asp:Panel ID="PnlTermosTextuais" runat="server" Visible="false">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group text-center">
                                            <div class="caixa-leitura" id="DvLeitura" runat="server">
                                            </div>
                                        </div>
                                    </asp:Panel>
                                    <asp:Panel ID="PnlTermosImagem" runat="server" Visible="false">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group text-center">
                                            <asp:Image ID="ImgTermos" runat="server" CssClass="img-responsive" />
                                        </div>
                                    </asp:Panel>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left bloco-opcao form-group">
                                        <asp:CheckBox ID="ChkConcordo" runat="server" Text="Li e concordo com os termos e condições informados" OnCheckedChanged="ChkConcordo_CheckedChanged" AutoPostBack="true" /><br />
                                    </div>
                                    <asp:Panel ID="PnlBotaoContinuar" runat="server" Visible="false">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group text-center">
                                            <asp:LinkButton ID="LnkContinuar" runat="server" CssClass="btn btn-block btn-primary" OnClick="LnkContinuar_Click">
                                    <i class="fa fa-chevron-right"></i>&nbsp;Continuar
                                            </asp:LinkButton>
                                    </asp:Panel>
                                </div>
                            </div>
                        </asp:Panel>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="LnkContinuar" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
</asp:Content>
