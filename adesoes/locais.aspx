﻿<%@ Page Title="" Language="C#" MasterPageFile="~/adesoes/imunne.master" AutoEventWireup="true" CodeFile="locais.aspx.cs" Inherits="adesoes_locais" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section class="subheader">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <i class="fa fa-thumbs-up"></i>&nbsp;Locais - Adesões
                </div>
            </div>
        </div>
    </section>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
                <div class="caixa-branca">
                    <div class="step-header">
                        INFORMAÇÕES DA CAMPANHA
                    </div>
                    <div class="box-inside">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                            <label><b>CAMPANHA</b></label>
                            <asp:Label ID="LblCampanha" runat="server" CssClass="label-identificacao"></asp:Label>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                            <label><b>EMPRESA</b></label>
                            <asp:Label ID="LblEmpresa" runat="server" CssClass="label-identificacao"></asp:Label>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                            <label><b>VALIDADE</b></label>
                            <asp:Label ID="LblValidade" runat="server" CssClass="label-identificacao"></asp:Label>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                            <label><b>APLICAÇÕES</b></label>
                            <asp:Label ID="LblAplicacoes" runat="server" CssClass="label-identificacao"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="caixa-branca">
                    <div class="col-lg-12">
                        <div class="step-header">
                            PESQUISE SEU LOCAL
                        </div>
                        <div class="box-inside">
                            <asp:UpdatePanel ID="UpdPesquisa" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                        <label>Pesquisar</label>
                                        <asp:TextBox ID="TxtCampoLivre" runat="server" CssClass="form-control" placeHolder="Informe o que procura"></asp:TextBox>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 pull-right form-group">
                                        <asp:LinkButton ID="LnkPesquisar" runat="server" CssClass="btn btn-block btn-success" OnClick="LnkPesquisar_Click">
                                    <i class="fa fa-search"></i>&nbsp;Pesquisar
                                        </asp:LinkButton>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                <div class="caixa-branca">
                    <div class="step-header">
                        LOCAIS
                    </div>
                    <div class="box-inside">
                        <asp:UpdatePanel ID="UpdResultadoPesquisa" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="box-grids" style="overflow-x: auto;">
                                    <div class="dv-grid-f-header">
                                        <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-left" style="width: 6% !important; display: inline-block; float: none;">
                                            AÇÕES
                                        </div>
                                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="width: 94% !important; display: inline-block; float: none;">
                                            Local
                                        </div>
                                    </div>
                                    <div class="dv-grid-f">
                                        <asp:Repeater ID="RptPrincipal" runat="server" OnItemDataBound="RptPrincipal_ItemDataBound" OnItemCommand="RptPrincipal_ItemCommand">
                                            <ItemTemplate>
                                                <div id="dvItem" runat="server">
                                                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="width: 6% !important; display: inline-block; float: none;">
                                                        <div style="margin: 0 auto; margin-top: -38px; position: absolute;">
                                                            <asp:LinkButton ID="LnkSelecionar" runat="server" CssClass="btn btn-selecao" Text="SELECIONAR" CommandName="selecionar"></asp:LinkButton>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="width: 94% !important; display: inline-block; float: none;">
                                                        <div class="truncate">
                                                            <asp:Label ID="LblNomeClinica" runat="server" CssClass="text-uppercase"></asp:Label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="LnkPesquisar" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
