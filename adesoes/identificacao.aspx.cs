﻿using System;
using System.Collections.Specialized;
using System.Text;
using System.Web.UI;

public partial class imunne_identificacao : System.Web.UI.Page
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            IniciarPagina();
    }

    /// <summary>
    /// 
    /// </summary>
    private void IniciarPagina()
    {
        StringBuilder textoMensagem = new StringBuilder();
        int codigoCampanha = -1;
        StringBuilder dataValidade = new StringBuilder();
        StringBuilder dataAplicacoes = new StringBuilder();

        try
        {
            if (!String.IsNullOrEmpty(Request.QueryString["campanha"]))
            {
                codigoCampanha = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["campanha"]));
                ImunneVacinas.Campanha campanhaSelecionada = ImunneVacinas.Campanha.ConsultarUnico(codigoCampanha);
                LblCampanha.Text = campanhaSelecionada.Identificacao + " | " + campanhaSelecionada.Nome.ToUpper();

                if (campanhaSelecionada.DataInicio == campanhaSelecionada.DataTermino)
                    dataValidade.AppendFormat("VÁLIDA A PARTIR DE {0}.", campanhaSelecionada.DataInicio.ToString("dd/MM/yyyy"));
                else dataValidade.AppendFormat("VÁLIDA DE {0} ATÉ {1}.", campanhaSelecionada.DataInicio.ToString("dd/MM/yyyy"), campanhaSelecionada.DataTermino.ToString("dd/MM/yyyy"));

                if (campanhaSelecionada.DataAbertura == campanhaSelecionada.DataLimite)
                    dataAplicacoes.AppendFormat("APLICAÇÕES A PARTIR DE {0}.", campanhaSelecionada.DataAbertura.ToString("dd/MM/yyyy"));
                else dataAplicacoes.AppendFormat("APLICAÇÕES DE {0} ATÉ {1}.", campanhaSelecionada.DataAbertura.ToString("dd/MM/yyyy"), campanhaSelecionada.DataLimite.ToString("dd/MM/yyyy"));

                LblValidade.Text = dataValidade.ToString();
                LblAplicacoes.Text = dataAplicacoes.ToString();

                Page.Title = "S&A Imunizações | " + campanhaSelecionada.Nome.ToUpper();
            }
            else
            {
                Response.Redirect("~/", false);
                Context.ApplicationInstance.CompleteRequest();
            }
        }
        catch (Exception ex)
        {
            this.SalvarException(ex);
            Response.Redirect("~/", false);
            Context.ApplicationInstance.CompleteRequest();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void LnkContinuar_Click(object sender, EventArgs e)
    {
        StringBuilder urlPagina = new StringBuilder();

        int codigoCampanha = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["campanha"]));

        if (PnlIdentificacao.Visible == true)
        {
            // Validação do CPF/CNPJ
            if (!ImunneVacinas.Utils.ValidarCpfCnpj(ImunneVacinas.Utils.LimparCpfCnpj(TxtCpfCnpjIdentificacao.Text)))
            {
                ImunneVacinas.Alerta.showMensagemErro(this.Page, "Aviso", "CPF informado é inválido");
                return;
            }

            ImunneVacinas.Participante participante = ImunneVacinas.Participante.ConsultarUnico(ImunneVacinas.Utils.LimparCpfCnpj(TxtCpfCnpjIdentificacao.Text));
            if (participante == null)
            {
                TxtCpfCadastro.Text = ImunneVacinas.Utils.FormatarCpfCnpj(TxtCpfCnpjIdentificacao.Text);
                PnlCadastro.Visible = true;
                PnlIdentificacao.Visible = false;
                return;
            }

            urlPagina.AppendFormat("~/adesoes/locais.aspx?campanha={0}", Request.QueryString["campanha"]);
            urlPagina.AppendFormat("&participante={0}", ImunneVacinas.Criptografia.Criptografar(participante.Codigo.ToString()));
            Response.Redirect(urlPagina.ToString(), false);
            Context.ApplicationInstance.CompleteRequest();
        }
        
        if (PnlCadastro.Visible == true)
        {
            // Validação do CPF/CNPJ
            if (!ImunneVacinas.Utils.ValidarCpfCnpj(ImunneVacinas.Utils.LimparCpfCnpj(TxtCpfCadastro.Text)))
            {
                ImunneVacinas.Alerta.showMensagemErro(this.Page, "Aviso", "CPF informado é inválido.");
                return;
            }

            // Validação de data de nascimento
            if (String.IsNullOrEmpty(TxtDataNascimento.Text) || ImunneVacinas.Utils.VerificarDataValida(TxtDataNascimento.Text) == false)
            {
                ImunneVacinas.Alerta.showMensagemErro(this.Page, "Aviso", "Informe uma data de nascimento válida.");
                return;
            }

            // Validação de nome
            if (String.IsNullOrEmpty(TxtNome.Text))
            {
                ImunneVacinas.Alerta.showMensagemErro(this.Page, "Aviso", "Informe seu nome completo.");
                return;
            }

            // Cadastro de novo participante

            ImunneVacinas.Participante participante = new ImunneVacinas.Participante()
            {
                Nome = TxtNome.Text,
                Cpf = ImunneVacinas.Utils.LimparCpfCnpj(TxtCpfCnpjIdentificacao.Text),
                DataNascimento = Convert.ToDateTime(TxtDataNascimento.Text),
                Email = txtEmail.Text,
                DataCriacao = DateTime.Now,
                Telefone = TxtTelefone.Text
            };

            int resultado = participante.Incluir();
            if (resultado <= 0)
            {
                ImunneVacinas.Alerta.showMensagemErro(this.Page, "Aviso", "Infelizmente não foi possível concluir seu cadastro, tente novamente ou informe nosso suporte.");
                return;
            }

            // Redirecionamento
            urlPagina.AppendFormat("~/adesoes/locais.aspx?campanha={0}", Request.QueryString["campanha"]);
            urlPagina.AppendFormat("&participante={0}", ImunneVacinas.Criptografia.Criptografar(resultado.ToString()));
            Response.Redirect(urlPagina.ToString(), false);
            Context.ApplicationInstance.CompleteRequest();
        }
    }
}