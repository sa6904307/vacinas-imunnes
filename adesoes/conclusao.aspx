﻿<%@ Page Title="" Language="C#" MasterPageFile="~/adesoes/imunne.master" AutoEventWireup="true" CodeFile="conclusao.aspx.cs" Inherits="adesoes_conclusao" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section class="subheader">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <i class="fa fa-thumbs-up"></i>&nbsp;Conclusão - Adesões
                </div>
            </div>
        </div>
    </section>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
                <div class="caixa-branca">
                    <div class="step-header">
                        INFORMAÇÕES DA CAMPANHA
                    </div>
                    <div class="box-inside">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                            <label><b>CAMPANHA</b></label>
                            <asp:Label ID="LblCampanha" runat="server" CssClass="label-identificacao"></asp:Label>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                            <label><b>EMPRESA</b></label>
                            <asp:Label ID="LblEmpresa" runat="server" CssClass="label-identificacao"></asp:Label>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                            <label><b>VALIDADE</b></label>
                            <asp:Label ID="LblValidade" runat="server" CssClass="label-identificacao"></asp:Label>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                            <label><b>APLICAÇÕES</b></label>
                            <asp:Label ID="LblAplicacoes" runat="server" CssClass="label-identificacao"></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                <div class="caixa-branca">
                    <div class="step-header">
                        CONCLUSÃO
                    </div>
                    <div class="box-inside">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group text-center">
                            <p id="PrfConclusao" runat="server">
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
