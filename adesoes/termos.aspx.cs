﻿using System;
using System.Text;
using System.Web.UI;

public partial class imunne_termos : System.Web.UI.Page
{
    private StringBuilder textoMensagem = new StringBuilder();

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            IniciarPagina();
    }

    /// <summary>
    /// 
    /// </summary>
    private void IniciarPagina()
    {
        int codigoCampanha = -1;
        StringBuilder dataValidade = new StringBuilder();
        StringBuilder dataAplicacoes = new StringBuilder();

        try
        {
            if (!String.IsNullOrEmpty(Request.QueryString["campanha"]))
            {
                codigoCampanha = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["campanha"]));

                ImunneVacinas.Campanha campanhaSelecionada = ImunneVacinas.Campanha.ConsultarUnico(codigoCampanha);
                LblCampanha.Text = campanhaSelecionada.Identificacao + " | " + campanhaSelecionada.Nome.ToUpper();

                if (campanhaSelecionada.DataInicio == campanhaSelecionada.DataTermino)
                    dataValidade.AppendFormat("VÁLIDA A PARTIR DE {0}.", campanhaSelecionada.DataInicio.ToString("dd/MM/yyyy"));
                else dataValidade.AppendFormat("VÁLIDA DE {0} ATÉ {1}.", campanhaSelecionada.DataInicio.ToString("dd/MM/yyyy"), campanhaSelecionada.DataTermino.ToString("dd/MM/yyyy"));

                if (campanhaSelecionada.DataAbertura == campanhaSelecionada.DataLimite)
                    dataAplicacoes.AppendFormat("APLICAÇÕES A PARTIR DE {0}.", campanhaSelecionada.DataAbertura.ToString("dd/MM/yyyy"));
                else dataAplicacoes.AppendFormat("APLICAÇÕES DE {0} ATÉ {1}.", campanhaSelecionada.DataAbertura.ToString("dd/MM/yyyy"), campanhaSelecionada.DataLimite.ToString("dd/MM/yyyy"));

                LblValidade.Text = dataValidade.ToString();
                LblAplicacoes.Text = dataAplicacoes.ToString();

                LblEmpresa.Text = campanhaSelecionada.IdEmpresa.NomeFantasia.ToUpper();

                if (campanhaSelecionada != null)
                {
                    if (campanhaSelecionada.DataTermino < DateTime.Now.Date)
                    {
                        textoMensagem.AppendFormat("Infelizmente a campanha {0} terminou sua validade na data de {1}.", campanhaSelecionada.Nome, campanhaSelecionada.DataTermino.ToString("dd/MM/yyyy"));
                        ImunneVacinas.Alerta.showMensagemAviso(this.Page, "Aviso", textoMensagem.ToString());

                        PnlTermosGerais.Visible = false;

                        return;
                    }
                }
                else
                {
                    textoMensagem.AppendFormat("Infelizmente nenhuma campanha foi encontra.");
                    ImunneVacinas.Alerta.showMensagemAviso(this.Page, "Aviso", textoMensagem.ToString());
                    return;
                }

                if (!String.IsNullOrEmpty(campanhaSelecionada.TermosImagem))
                {
                    PnlTermosImagem.Visible = true;
                    ImgTermos.ImageUrl = "~/img/campanhas/" + campanhaSelecionada.TermosImagem;
                }
                else
                {
                    PnlTermosTextuais.Visible = true;
                    DvLeitura.InnerHtml = campanhaSelecionada.TermosTexto.Replace("\r\n", "<br />").Replace("\n", "<br />");
                }
                Page.Title = "S&A Imunizações | " + campanhaSelecionada.Nome.ToUpper();
            }
            else
            {
                Response.Redirect("~/", false);
                Context.ApplicationInstance.CompleteRequest();
            }
        }
        catch (Exception ex)
        {
            ImunneVacinas.Erro erro = new ImunneVacinas.Erro();
            erro.AdicionarLog(ex);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ChkConcordo_CheckedChanged(object sender, EventArgs e)
    {
        PnlBotaoContinuar.Visible = ChkConcordo.Checked;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void LnkContinuar_Click(object sender, EventArgs e)
    {
        Response.Redirect(String.Format("~/adesoes/identificacao.aspx?campanha={0}", Request.QueryString["campanha"]), false);
        Context.ApplicationInstance.CompleteRequest();
    }
}