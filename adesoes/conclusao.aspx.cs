﻿using System;
using System.Text;
using System.Web.UI;

public partial class adesoes_conclusao : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            IniciarPagina();
    }

    /// <summary>
    /// 
    /// </summary>
    private void IniciarPagina()
    {
        int codigoCampanha = -1;
        int codigoParticipante = -1;
        int codigoLocal = -1;
        StringBuilder dataValidade = new StringBuilder();
        StringBuilder dataAplicacoes = new StringBuilder();

        try
        {
            if (!String.IsNullOrEmpty(Request.QueryString["campanha"]))
            {
                codigoCampanha = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["campanha"]));
                codigoParticipante = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["participante"]));
                codigoLocal = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["local"]));

                ImunneVacinas.Campanha campanhaSelecionada = ImunneVacinas.Campanha.ConsultarUnico(codigoCampanha);
                LblCampanha.Text = campanhaSelecionada.Identificacao + " | " + campanhaSelecionada.Nome.ToUpper();

                if (campanhaSelecionada.DataInicio == campanhaSelecionada.DataTermino)
                    dataValidade.AppendFormat("VÁLIDA A PARTIR DE {0}.", campanhaSelecionada.DataInicio.ToString("dd/MM/yyyy"));
                else dataValidade.AppendFormat("VÁLIDA DE {0} ATÉ {1}.", campanhaSelecionada.DataInicio.ToString("dd/MM/yyyy"), campanhaSelecionada.DataTermino.ToString("dd/MM/yyyy"));

                if (campanhaSelecionada.DataAbertura == campanhaSelecionada.DataLimite)
                    dataAplicacoes.AppendFormat("APLICAÇÕES A PARTIR DE {0}.", campanhaSelecionada.DataAbertura.ToString("dd/MM/yyyy"));
                else dataAplicacoes.AppendFormat("APLICAÇÕES DE {0} ATÉ {1}.", campanhaSelecionada.DataAbertura.ToString("dd/MM/yyyy"), campanhaSelecionada.DataLimite.ToString("dd/MM/yyyy"));

                LblValidade.Text = dataValidade.ToString();
                LblAplicacoes.Text = dataAplicacoes.ToString();

                Page.Title = "S&A Imunizações | " + campanhaSelecionada.Nome.ToUpper();

                GerarAdesao(codigoCampanha, codigoLocal, codigoParticipante);
            }
            else
            {
                Response.Redirect("~/", false);
                Context.ApplicationInstance.CompleteRequest();
            }
        }
        catch (Exception ex)
        {
            this.SalvarException(ex);
            Response.Redirect("~/", false);
            Context.ApplicationInstance.CompleteRequest();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="codigoCampanha"></param>
    /// <param name="codigoLocal"></param>
    /// <param name="codigoParticipante"></param>
    private void GerarAdesao(int codigoCampanha, int codigoLocal, int codigoParticipante)
    {
        ImunneVacinas.Participante participanteSelecionado = ImunneVacinas.Participante.ConsultarUnico(codigoParticipante);
        ImunneVacinas.LocalAdesao localSelecionado = ImunneVacinas.LocalAdesao.ConsultarUnico(codigoLocal);

        ImunneVacinas.PesquisaAdesao pesquisa = new ImunneVacinas.PesquisaAdesao()
        {
            Participante = codigoParticipante,
            Campanha = codigoCampanha
        };

        ImunneVacinas.Adesao item = ImunneVacinas.Adesao.ConsultarUnico(codigoCampanha, codigoParticipante);
        if (item != null)
            PrfConclusao.InnerHtml = String.Format("<b>{0}</b> você já fez a opção de adesão para "
                                   + "esta campanha em {1}. O local de aplicação é <b>{2}</b>.", participanteSelecionado.Nome, item.DataAdesao.ToString("dd/MM/yyyy"), localSelecionado.Endereco.ToUpper());
        else
        {
            item = new ImunneVacinas.Adesao();
            item.Campanha.Codigo = codigoCampanha;
            item.Participante.Codigo = codigoParticipante;
            item.Local.Codigo = codigoLocal;
            item.DataAdesao = DateTime.Now;
            int resultado = item.Incluir();

            if (resultado > 0)
                PrfConclusao.InnerHtml = String.Format("Parabéns <b>{0}</b>, sua adesão parea esta campanha foi realizada com "
                                       + "sucesso hoje {1} e você escolheu o local <b>{2}</b>.", participanteSelecionado.Nome.ToUpper(), item.DataAdesao.ToString("dd/MM/yyyy"), localSelecionado.Endereco.ToUpper());
            else
                PrfConclusao.InnerHtml = "Infelizmente não foi possível a conclusão, tente novamente mais tarde ou informe ao suporte.";
        }
    }
}