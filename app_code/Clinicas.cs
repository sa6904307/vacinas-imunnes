﻿using MySql.Data.MySqlClient;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace ImunneVacinas
{
    /// <summary>
    /// Descrição resumida de Clinicas
    /// </summary>
    [Serializable]
    public class Clinica
    {
        private static string _tabela = "Clinicas";
        private static List<BD.Columns> columns;
        private static DateTime columnsUpdated;

        #region Propriedades do elemento

        private int _codigo;
        private string _cpfCnpj;
        private string _nomeFantasia;
        private string _cep;
        private string _complemento;
        private string _endereco;
        private string _numero;
        private string _bairro;
        private string _contatoComercial;
        private string _contatoOperacional;
        private string _emailFinanceiro;
        private string _emailGeral;
        private string _telefone;
        private Uf _idUf;
        private Cidade _idCidade;
        private string _longitude;
        private string _latitude;
        private DateTime _dataCriacao;
        private DateTime _dataAlteracao;
        private string _observacoes;
        private int _envioBoasVindas;
        private DateTime _dataEnvio;

        public int Codigo
        {
            get
            {
                return _codigo;
            }

            set
            {
                _codigo = value;
            }
        }

        public string CpfCnpj
        {
            get
            {
                return _cpfCnpj;
            }

            set
            {
                _cpfCnpj = value;
            }
        }

        public string NomeFantasia
        {
            get
            {
                return _nomeFantasia;
            }

            set
            {
                _nomeFantasia = value;
            }
        }

        public string Cep
        {
            get
            {
                return _cep;
            }

            set
            {
                _cep = value;
            }
        }

        public string Complemento
        {
            get
            {
                return _complemento;
            }

            set
            {
                _complemento = value;
            }
        }

        public string Endereco
        {
            get
            {
                return _endereco;
            }

            set
            {
                _endereco = value;
            }
        }

        public string Numero
        {
            get
            {
                return _numero;
            }

            set
            {
                _numero = value;
            }
        }

        public string Bairro
        {
            get
            {
                return _bairro;
            }

            set
            {
                _bairro = value;
            }
        }

        public string ContatoComercial
        {
            get
            {
                return _contatoComercial;
            }

            set
            {
                _contatoComercial = value;
            }
        }

        public string ContatoOperacional
        {
            get
            {
                return _contatoOperacional;
            }

            set
            {
                _contatoOperacional = value;
            }
        }

        public string EmailFinanceiro
        {
            get
            {
                return _emailFinanceiro;
            }

            set
            {
                _emailFinanceiro = value;
            }
        }

        public string EmailGeral
        {
            get
            {
                return _emailGeral;
            }

            set
            {
                _emailGeral = value;
            }
        }

        public string Telefone
        {
            get
            {
                return _telefone;
            }

            set
            {
                _telefone = value;
            }
        }

        public Uf IdUf
        {
            get
            {
                return _idUf;
            }

            set
            {
                _idUf = value;
            }
        }

        public Cidade IdCidade
        {
            get
            {
                return _idCidade;
            }

            set
            {
                _idCidade = value;
            }
        }

        public string Longitude
        {
            get
            {
                return _longitude;
            }

            set
            {
                _longitude = value;
            }
        }

        public string Latitude
        {
            get
            {
                return _latitude;
            }

            set
            {
                _latitude = value;
            }
        }

        public DateTime DataCriacao
        {
            get
            {
                return _dataCriacao;
            }

            set
            {
                _dataCriacao = value;
            }
        }

        public DateTime DataAlteracao
        {
            get
            {
                return _dataAlteracao;
            }

            set
            {
                _dataAlteracao = value;
            }
        }

        public string Observacoes
        {
            get { return _observacoes; }
            set { _observacoes = value; }
        }

        public int EnvioBoasVindas
        {
            get
            {
                return _envioBoasVindas;
            }

            set
            {
                _envioBoasVindas = value;
            }
        }

        public DateTime DataEnvio
        {
            get
            {
                return _dataEnvio;
            }

            set
            {
                _dataEnvio = value;
            }
        }

        #endregion

        #region GetInfos

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos os campos da tabela.
        /// </summary>
        private static void GetInfoTabela()
        {
            if (columns == null || (DateTime.Now - columnsUpdated).Minutes > 0)
            {
                columns = BD.Columns.getColumnsInfo(_tabela);
                columnsUpdated = DateTime.Now;
            }
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo codigo.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoCodigo()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "codigo");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo cpfcnpj.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoCpfCnpj()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "cpfcnpj");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo nomefantasia.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoNomeFantasia()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "nomefantasia");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo cep.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoCep()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "cep");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo endereco.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoEndereco()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "endereco");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo numero.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoNumero()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "numero");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo complemento.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoComplemento()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "complemento");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo bairro.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoBairro()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "bairro");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo estado.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoEstado()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "estado");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo cidade.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoCidade()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "cidade");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo contatocomercial.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoContatoComercial()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "contatocomercial");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo contatooperacional.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoContatoOperacional()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "contatooperacional");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo emailfinanceiro.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoEmailFinanceiro()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "emailfinanceiro");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo emailgeral.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoEmailGeral()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "emailgeral");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo telefone.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoTelefone()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "telefone");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo datacriacao.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoDataCriacao()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "datacriacao");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo dataalteracao.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoDataAlteracao()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "dataalteracao");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo longitude.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoLongitude()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "longitude");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo latitude.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoLatitude()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "latitude");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo observacoes.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoObservacoes()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "observacoes");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo envioboasvindas.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoEnvioBoasVindas()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "envioboasvindas");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo envioboasvindas.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoDataEnvio()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "dataenvio");
        }

        #endregion

        /// <summary> 
        /// Rotina que cria um Array com os parâmetros utilizados nas ações de INSERT, UPDATE e DELETE
        /// <returns>Array com vários MySqlParameters</returns>
        /// </summary>
        private Array GetParametrosSQL()
        {
            MySqlParameter p;
            ArrayList ap = new ArrayList();

            p = new MySqlParameter("@codigo", Parser.parseInteiro(_codigo));
            p.MySqlDbType = GetInfoCodigo().Type;
            ap.Add(p);

            p = new MySqlParameter("@cidade", Parser.parseInteiro(_idCidade.Codigo));
            p.MySqlDbType = GetInfoCidade().Type;
            ap.Add(p);

            p = new MySqlParameter("@nomefantasia", Parser.parseString(_nomeFantasia));
            p.MySqlDbType = GetInfoNomeFantasia().Type;
            ap.Add(p);

            p = new MySqlParameter("@cpfcnpj", Parser.parseString(_cpfCnpj));
            p.MySqlDbType = GetInfoCpfCnpj().Type;
            ap.Add(p);

            p = new MySqlParameter("@cep", Parser.parseString(_cep));
            p.MySqlDbType = GetInfoCep().Type;
            ap.Add(p);

            p = new MySqlParameter("@endereco", Parser.parseString(_endereco));
            p.MySqlDbType = GetInfoEndereco().Type;
            ap.Add(p);

            p = new MySqlParameter("@numero", Parser.parseString(_numero));
            p.MySqlDbType = GetInfoNumero().Type;
            ap.Add(p);

            p = new MySqlParameter("@complemento", Parser.parseString(_complemento));
            p.MySqlDbType = GetInfoComplemento().Type;
            ap.Add(p);

            p = new MySqlParameter("@bairro", Parser.parseString(_bairro));
            p.MySqlDbType = GetInfoBairro().Type;
            ap.Add(p);

            p = new MySqlParameter("@estado", Parser.parseString(_idUf.Sigla));
            p.MySqlDbType = GetInfoEstado().Type;
            ap.Add(p);

            p = new MySqlParameter("@telefone", Parser.parseString(_telefone));
            p.MySqlDbType = GetInfoTelefone().Type;
            ap.Add(p);

            p = new MySqlParameter("@emailgeral", Parser.parseString(_emailGeral));
            p.MySqlDbType = GetInfoEmailGeral().Type;
            ap.Add(p);

            p = new MySqlParameter("@emailfinanceiro", Parser.parseString(_emailFinanceiro));
            p.MySqlDbType = GetInfoEmailFinanceiro().Type;
            ap.Add(p);

            p = new MySqlParameter("@contatocomercial", Parser.parseString(_contatoComercial));
            p.MySqlDbType = GetInfoContatoComercial().Type;
            ap.Add(p);

            p = new MySqlParameter("@contatooperacional", Parser.parseString(_contatoOperacional));
            p.MySqlDbType = GetInfoContatoOperacional().Type;
            ap.Add(p);

            p = new MySqlParameter("@datacriacao", Parser.parseDateTimeBD(_dataCriacao));
            p.MySqlDbType = GetInfoDataCriacao().Type;
            ap.Add(p);

            p = new MySqlParameter("@dataalteracao", Parser.parseDateTimeBD(_dataAlteracao));
            p.MySqlDbType = GetInfoDataAlteracao().Type;
            ap.Add(p);

            p = new MySqlParameter("@latitude", Parser.parseString(_latitude));
            p.MySqlDbType = GetInfoLatitude().Type;
            ap.Add(p);

            p = new MySqlParameter("@longitude", Parser.parseString(_longitude));
            p.MySqlDbType = GetInfoLongitude().Type;
            ap.Add(p);

            p = new MySqlParameter("@observacoes", Parser.parseString(_observacoes));
            p.MySqlDbType = GetInfoObservacoes().Type;
            ap.Add(p);

            p = new MySqlParameter("@envioboasvindas", (int)_envioBoasVindas);
            p.MySqlDbType = GetInfoEnvioBoasVindas().Type;
            ap.Add(p);

            p = new MySqlParameter("@dataenvio", Parser.parseDateTimeBD(_dataEnvio));
            p.MySqlDbType = GetInfoDataEnvio().Type;
            ap.Add(p);

            return ap.ToArray();
        }

        /// <summary>
        /// Inicialização das propriedades do elemento com seus valores padrão
        /// </summary>
        private void IniciarElemento()
        {
            GetInfoTabela();
            _idCidade = new Cidade();
            _idUf = new Uf();

            _idCidade.Codigo = -1;
            _idUf.Sigla = String.Empty;

            _nomeFantasia = String.Empty;
            _bairro = String.Empty;
            _cep = String.Empty;
            _complemento = String.Empty;
            _contatoComercial = String.Empty;
            _contatoOperacional = String.Empty;
            _cpfCnpj = String.Empty;
            _emailFinanceiro = String.Empty;
            _emailGeral = String.Empty;
            _numero = String.Empty;
            _dataAlteracao = Utils.dataPadrao;
            _dataCriacao = Utils.dataPadrao;
            _latitude = String.Empty;
            _longitude = String.Empty;
            _observacoes = String.Empty;
            _envioBoasVindas = 0;
            _dataEnvio = Utils.dataPadrao;
        }

        /// <summary>
        /// Inicialização do elemento
        /// </summary>
        public Clinica()
        {
            IniciarElemento();
        }

        /// <summary>
        /// Rotina que insere o registro no BD
        /// </summary>
        /// <returns>Código do registro inserido no BD</returns>
        public int Incluir()
        {
            if (String.IsNullOrEmpty(_nomeFantasia))
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    sql.Parameters.AddRange(GetParametrosSQL());
                    sql.CommandText = "INSERT INTO Clinicas "
                                    + "(cpfcnpj, nomefantasia, cep, endereco, "
                                    + "numero, complemento, bairro, estado, "
                                    + "cidade, contatocomercial, contatooperacional, "
                                    + "emailfinanceiro, emailgeral, telefone, longitude, "
                                    + "latitude, datacriacao, dataalteracao, observacoes, "
                                    + "envioboasvindas, dataenvio) "
                                    + "VALUE (@cpfcnpj, @nomefantasia, @cep, @endereco, "
                                    + "@numero, @complemento, @bairro, @estado, "
                                    + "@cidade, @contatocomercial, @contatooperacional, "
                                    + "@emailfinanceiro, @emailgeral, @telefone, @longitude, "
                                    + "@latitude, @datacriacao, @dataalteracao, @observacoes, "
                                    + "@envioboasvindas, @dataenvio); "
                                    + "SELECT LAST_INSERT_ID();";
                    int resultado = Parser.parseInteiro(new Data().executeEscalar(sql));
                    return resultado;
                }
            }
            catch (ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Rotina que altera os dados do registro no BD
        /// </summary>
        /// <returns>Quantidade de linhas alteradas na ação junto ao BD</returns>
        public int Alterar()
        {
            if (_codigo < 0)
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    sql.Parameters.AddRange(GetParametrosSQL());
                    sql.CommandText = "UPDATE Clinicas "
                                    + "SET "
                                    + "cpfcnpj = @cpfcnpj, nomefantasia = @nomefantasia, cep = @cep, "
                                    + "endereco = @endereco, numero = @numero, complemento = @complemento, "
                                    + "envioboasvindas = @envioboasvindas, dataenvio = @dataenvio, "
                                    + "bairro = @bairro, estado = @estado, cidade = @cidade, contatocomercial = @contatocomercial, "
                                    + "contatooperacional = @contatooperacional, emailfinanceiro = @emailfinanceiro, emailgeral = @emailgeral, "
                                    + "telefone = @telefone, longitude = @longitude, latitude = @latitude, dataalteracao = @dataalteracao, observacoes = @observacoes "
                                    + "WHERE codigo = @codigo";
                    int resultado = Parser.parseInteiro(new Data().execute(sql));
                    return resultado;
                }
            }
            catch (ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Rotina que exclui o registro do BD
        /// </summary>
        /// <returns>Quantidade de linhas alteradas na ação junto ao BD</returns>
        public int Excluir()
        {
            if (_codigo < 0)
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    sql.Parameters.AddRange(GetParametrosSQL());
                    sql.CommandText = "DELETE FROM Clinicas "
                                    + "WHERE codigo = @codigo";
                    int resultado = Parser.parseInteiro(new Data().execute(sql));
                    return resultado;
                }
            }
            catch (ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Rotina que localiza e retorna um único registro, mediante o código informado
        /// </summary>
        /// <param name="codigo">VALUE: código de identificação do registro (obrigatório)</param>
        /// <returns>Elemento Usuario com os dados retornados da consulta</returns>
        public static Clinica ConsultarUnico(int codigo)
        {
            if (codigo < 0)
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            ImunneVacinas.PesquisaClinica pesquisa = new PesquisaClinica()
            {
                Codigo = codigo
            };

            List<Clinica> retorno = ConsultaGenerica(pesquisa);
            if (retorno == null || retorno.Count.Equals(0))
                return null;
            else
                return retorno[0];
        }

        /// <summary>
        /// Rotina que localiza e retorna um único registro, mediante o código informado
        /// </summary>
        /// <param name="nome">VALUE: campo textual para pesquisa por nome (obrigatório)</param>
        /// <returns>Elemento Usuario com os dados retornados da consulta</returns>
        public static Clinica ConsultarUnico(string cpfcnpj)
        {
            if (String.IsNullOrEmpty(cpfcnpj))
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            ImunneVacinas.PesquisaClinica pesquisa = new PesquisaClinica()
            {
                CpfCnpj = cpfcnpj
            };

            List<ImunneVacinas.Clinica> retorno = ConsultaGenerica(pesquisa);
            if (retorno == null || retorno.Count.Equals(0))
                return null;
            else
                return retorno[0];
        }

        /// <summary>
        /// Rotina que realiza a pesquisa de elementos mediante os parametros de pesquisa informados
        /// </summary>
        /// <param name="pesquisa">VALUE: elemento de pesquisa com os parametros para consulta</param>
        /// <returns></returns>
        public static List<Clinica> ConsultarClinicas(PesquisaClinica pesquisa)
        {
            return ConsultaGenerica(pesquisa);
        }

        /// <summary>
        /// Rotina que retorna um elemento RetornoCBX para utilização em DropLists
        /// </summary>
        /// <param name="InserirLinhaPadrao">VALUE: Informa se será adiconada a linha padrão</param>
        /// <param name="textoPadrao">VALUE: Informa o texto utilizado para linha padão (se utilizado NULL, será apresentado SELECIONE)</param>
        /// <returns>Lista do elemento RetornoCBX com os dados desejados</returns>
        public static List<RetornoComboBox> ConsultarComboBox(bool InserirLinhaPadrao, string textoPadrao)
        {
            DataTable dt = new DataTable();
            try
            {
                dt.Columns.Add("DisplayMember");
                dt.Columns.Add("ValueMember");

                ImunneVacinas.PesquisaClinica pesquisa = new PesquisaClinica()
                {
                    Ordenacao = Utils.TipoOrdenacao.porNome
                };

                List<Clinica> retorno = ConsultaGenerica(pesquisa);
                if (retorno != null)
                {
                    foreach (Clinica c in retorno)
                    {
                        StringBuilder displayCombo = new StringBuilder();
                        displayCombo.AppendFormat("{0} - ", c.NomeFantasia.ToUpper());
                        displayCombo.AppendFormat("({0}/{1})", c.IdCidade.Nome.ToUpper(), c.IdUf.Sigla.ToUpper());
                        dt.Rows.Add(new object[] { displayCombo.ToString().ToUpper(), c.Codigo });
                    }
                }
                return Utils.MontarRetornoComboBox(dt, InserirLinhaPadrao, textoPadrao);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// Rotina interna utilizada para todas as pesquisas do elemento
        /// </summary>
        /// <param name="pesquisa">VALUE: Elemento com os parâmetros com os quais será realizada a pesquisa</param>
        /// <returns>Lista do elemento com os dados retornados da consulta</returns>
        internal static List<Clinica> ConsultaGenerica(PesquisaClinica pesquisa)
        {
            StringBuilder ordenacao = new StringBuilder();
            StringBuilder filtro = new StringBuilder();
            StringBuilder top = new StringBuilder();
            MySqlParameter p;

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    if (pesquisa.Quantidade > 0)
                        top.AppendFormat(" LIMIT {0} ", pesquisa.Quantidade);

                    #region Campo Código

                    if (pesquisa.Codigo >= 0)
                    {
                        filtro.Append(" AND Clinicas.codigo = @codigo");
                        p = new MySqlParameter("@codigo", pesquisa.Codigo);
                        p.MySqlDbType = GetInfoCodigo().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo CPFCNPJ

                    if (!String.IsNullOrEmpty(pesquisa.CpfCnpj))
                    {
                        string cpfCnpj = String.Format("{0}", pesquisa.CpfCnpj);
                        filtro.Append(" AND Clinicas.cpfcnpj = @cpfcnpj ");
                        p = new MySqlParameter("@cpfcnpj", cpfCnpj);
                        p.MySqlDbType = MySqlDbType.VarChar;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campos UF e Cidade

                    if (!String.IsNullOrEmpty(pesquisa.Uf) && pesquisa.Uf != "-1")
                    {
                        string siglaEstado = String.Format("{0}", pesquisa.Uf);
                        filtro.Append(" AND Clinicas.estado = @estado ");
                        p = new MySqlParameter("@estado", siglaEstado);
                        p.MySqlDbType = MySqlDbType.VarChar;
                        sql.Parameters.Add(p);

                        if (pesquisa.Cidade >= 0)
                        {
                            filtro.Append(" AND Clinicas.cidade = @cidade");
                            p = new MySqlParameter("@cidade", pesquisa.Cidade);
                            p.MySqlDbType = GetInfoCidade().Type;
                            sql.Parameters.Add(p);
                        }
                    }

                    #endregion

                    #region Campo Livre

                    if (!String.IsNullOrEmpty(pesquisa.CampoLivre.Trim()))
                    {
                        string campoLivre = String.Format("%{0}%", pesquisa.CampoLivre.Trim());
                        filtro.Append(" AND (Clinicas.nomefantasia LIKE @campoLivre ");
                        filtro.Append(" OR Clinicas.endereco LIKE @campoLivre ");
                        filtro.Append(" OR Clinicas.bairro LIKE @campoLivre ");
                        filtro.Append(" OR Clinicas.contatocomercial LIKE @campoLivre ");
                        filtro.Append(" OR Clinicas.contatooperacional LIKE @campoLivre ");
                        filtro.Append(" OR Cidades.nome_cidade LIKE @campoLivre ");
                        filtro.Append(")");
                        p = new MySqlParameter("@campoLivre", campoLivre);
                        p.MySqlDbType = MySqlDbType.VarChar;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    if (filtro.Length > 0)
                        filtro = filtro.Replace(" AND ", " WHERE ", 0, 5);

                    #region Ordenação

                    switch (pesquisa.Ordenacao)
                    {
                        case Utils.TipoOrdenacao.porCodigo:
                            {
                                ordenacao.Append("ORDER BY Clinicas.codigo");
                            }
                            break;
                        case Utils.TipoOrdenacao.porCodigoDesc:
                            {
                                ordenacao.Append("ORDER BY Clinicas.codigo DESC");
                            }
                            break;
                        case Utils.TipoOrdenacao.porNome:
                            {
                                ordenacao.Append("ORDER BY Clinicas.nomefantasia");
                            }
                            break;
                        case Utils.TipoOrdenacao.porNomeDesc:
                            {
                                ordenacao.Append("ORDER BY Clinicas.nomefantasia DESC");
                            }
                            break;
                        case Utils.TipoOrdenacao.porData:
                            {
                                ordenacao.Append("ORDER BY Clinicas.datacriacao");
                            }
                            break;
                        case Utils.TipoOrdenacao.porDataDesc:
                            {
                                ordenacao.Append("ORDER BY Clinicas.datacriacao DESC");
                            }
                            break;
                        case Utils.TipoOrdenacao.randomico:
                            {
                                ordenacao.Append("ORDER BY RAND()");
                            }
                            break;
                        default:
                            {
                                ordenacao.Append("ORDER BY Clinicas.codigo");
                            }
                            break;
                    }

                    #endregion

                    sql.CommandText = String.Format("SELECT "
                        + "Cidades.codigo_cidade AS codigoCidade, Cidades.nome_cidade AS nomeCidade, "
                        + "Estados.sigla AS siglaEstado, "
                        + "Clinicas.* "
                        + "FROM Clinicas "
                        + "INNER JOIN Cidades ON Cidades.codigo_cidade = Clinicas.cidade "
                        + "INNER JOIN Estados ON Estados.sigla = Clinicas.estado "
                        + "{0} {1} {2}", filtro, ordenacao, top);
                    DataTable dt = new Data().getData(sql);
                    return MontarRetorno(dt);
                }
            }
            catch (ImunneVacinas.ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Rotina que retorna uma lista genérica do elemento, de acordo com o retorno da Consultar ao BD
        /// </summary>
        /// <param name="dt">VALUE: DataTable com o resultado da Consultar ao BD</param>
        /// <returns>Lista do elemento com os dados retornados da consulta</returns>
        internal static List<Clinica> MontarRetorno(DataTable dt)
        {
            int i = -1;
            List<Clinica> list = new List<Clinica>();

            if (dt == null && dt.Rows.Count == 0)
                return list;
            else
            {
                Clinica retorno;
                try
                {
                    for (i = 0; i < dt.Rows.Count; i++)
                    {
                        retorno = new Clinica()
                        {
                            _codigo = Parser.parseInteiro(dt.Rows[i]["codigo"]),
                            _bairro = Parser.parseString(dt.Rows[i]["bairro"]),
                            _cep = Parser.parseString(dt.Rows[i]["cep"]),
                            _complemento = Parser.parseString(dt.Rows[i]["complemento"]),
                            _contatoComercial = Parser.parseString(dt.Rows[i]["contatocomercial"]),
                            _contatoOperacional = Parser.parseString(dt.Rows[i]["contatooperacional"]),
                            _cpfCnpj = Parser.parseString(dt.Rows[i]["cpfCnpj"]),
                            _emailFinanceiro = Parser.parseString(dt.Rows[i]["emailfinanceiro"]),
                            _emailGeral = Parser.parseString(dt.Rows[i]["emailgeral"]),
                            _endereco = Parser.parseString(dt.Rows[i]["endereco"]),
                            _nomeFantasia = Parser.parseString(dt.Rows[i]["nomefantasia"]),
                            _numero = Parser.parseString(dt.Rows[i]["numero"]),
                            _telefone = Parser.parseString(dt.Rows[i]["telefone"]),
                            _latitude = Parser.parseString(dt.Rows[i]["latitude"]),
                            _longitude = Parser.parseString(dt.Rows[i]["longitude"]),
                            _dataCriacao = Convert.ToDateTime(dt.Rows[i]["datacriacao"]),
                            _observacoes = Parser.parseString(dt.Rows[i]["observacoes"]),
                            _dataAlteracao = Convert.ToDateTime(dt.Rows[i]["dataalteracao"]),
                            _dataEnvio = Convert.ToDateTime(dt.Rows[i]["dataenvio"]),
                            _envioBoasVindas = Parser.parseInteiro(dt.Rows[i]["envioboasvindas"])
                        };

                        retorno._idCidade.Codigo = Parser.parseInteiro(dt.Rows[i]["codigoCidade"]);
                        retorno._idCidade.Nome = Parser.parseString(dt.Rows[i]["nomecidade"]);
                        retorno.IdUf.Sigla = Parser.parseString(dt.Rows[i]["siglaEstado"]);

                        list.Add(retorno);
                    }
                    return list;
                }
                catch (Exception ex)
                {
                    ex.Data.Add("último retorno lido", i);
                    throw;
                }
            }
        }
    }
}