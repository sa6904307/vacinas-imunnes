﻿using MySql.Data.MySqlClient;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web;

namespace ImunneVacinas
{

    /// <summary>
    /// Informações extras para gerar o log
    /// </summary>
    public class Erro
    {
        /// <summary>
        /// tipos de produtos Guess que utilizarão esta dll
        /// </summary>
        public enum produtos
        {
            ImunneVacinas
        }

        #region Propriedades dos Campos da Tabela

        private int empresa;
        private int usuario;
        private string descricao;
        private string msgExtra;
        private produtos programaOrigem;


        private string tipoErro;

        public string TipoErro
        {
            get
            {
                return tipoErro;
            }
            set
            {
                tipoErro = value;
            }
        }
        private string msgErro;

        public string MsgErro
        {
            get
            {
                return msgErro;
            }
            set
            {
                msgErro = value;
            }
        }
        private string msgTrace;

        public string MsgTrace
        {
            get
            {
                return msgTrace;
            }
            set
            {
                msgTrace = value;
            }
        }


        /// <summary>
        /// Código da imobiliaria
        /// <para>VALUE: um inteiro maior ou igual a zero.</para>
        /// <para>REMARKS: Se não for indicado um código, será gravado -1.</para>
        /// </summary>
        public int Empresa
        {
            get
            {
                return empresa;
            }
            set
            {
                empresa = value;
            }
        }

        /// <summary>
        /// Código do usuário logado
        /// <para>VALUE: um inteiro maior ou igual a zero.</para>
        /// <para>REMARKS: Se não for indicado um código, será gravado -1.</para>
        /// </summary>
        public int Usuario
        {
            get
            {
                return usuario;
            }
            set
            {
                usuario = value;
            }
        }

        /// <summary>
        /// Descrição adicional do erro. 
        /// <para>VALUE: ate 100 caracteres.</para>
        /// </summary>
        [Obsolete("Não usar")]
        public string Descricao
        {
            get
            {
                return descricao;
            }
            set
            {
                descricao = value;
            }
        }

        /// <summary>
        /// Informação extra. Querys, conteúdo de variáveis... etc
        /// <para>VALUE: sem limite de caracteres.</para>
        /// </summary>
        public string MsgExtra
        {
            get
            {
                return msgExtra;
            }
            set
            {
                msgExtra = value;
            }
        }

        /// <summary>
        /// Qual produto Guess gerou o erro
        /// </summary>
        public produtos ProgramaOrigem
        {
            get
            {
                return programaOrigem;
            }
            set
            {
                programaOrigem = value;
            }
        }

        #endregion

        /// <summary>
        /// Construtor
        /// </summary>
        /// 
        public Erro()
        {
            empresa = -1;
            usuario = -1;
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="programaOrigem"></param>
        public Erro(produtos programaOrigem)
        {
            empresa = -1;
            usuario = -1;
            ProgramaOrigem = programaOrigem;
        }

        /// <summary>
        /// Registra erro no arquivo
        /// <para>REMARKS: Por padrão, é criada uma pasta 'logs' na raiz do site onde a Dll está incorporada.</para>
        /// <para>É criado um arquivo texto cujo nome é a data corrente no formato ano+mês+dia (por ex.: 20120426.Txt). Dessa forma, a cada dia cria-se um arquivo novo para facilitar a localização de erros.</para>
        /// </summary>
        /// <param name="erro">Exceção capturada por um catch</param>
        public void addArquivo(System.Exception erro)
        {
            //System.Diagnostics.StackTrace stack = new System.Diagnostics.StackTrace(erro, true);
            //System.Diagnostics.StackFrame sf = stack.GetFrame(stack.FrameCount - 1);

            //System.Text.StringBuilder trace = new System.Text.StringBuilder();
            ////string file = System.IO.Path.GetFileName(sf.GetFileName());
            //trace.AppendFormat("linha {2} em {0} : {1}", sf.GetFileName(), sf.GetMethod(), sf.GetFileLineNumber());
            System.Text.StringBuilder trace = new System.Text.StringBuilder();

            if (erro != null)
            {
                //Capturando na pilha de erro apenas a ocorrencia que indica o módulo, método e linha onde o erro ocorreu
                // System.Diagnostics.StackTrace stack = new System.Diagnostics.StackTrace(erro, true);
                //logando do ultimo ao primeiro frame. Ate que finalize os frames ou que sejam logados os 2 ultimos
                //for (int i = stack.FrameCount - 1; i == 0 || (i >= stack.FrameCount - 3); i--)
                //{
                //    System.Diagnostics.StackFrame sf = stack.GetFrame(i);
                //    string file = System.IO.Path.GetFileName(sf.GetFileName());
                //    trace.AppendFormat(" linha {2} em {0} : {1}", file, sf.GetMethod(), sf.GetFileLineNumber());
                //}
                trace.Append(erro.StackTrace);
            }


            HttpContext contexto = HttpContext.Current;
            if (!System.IO.Directory.Exists(contexto.Server.MapPath("~") + "/logs"))
                System.IO.Directory.CreateDirectory(contexto.Server.MapPath("~") + "/logs");
            string caminho = contexto.Server.MapPath("~");
            System.IO.FileStream arquivoLog = new System.IO.FileStream(String.Format("{0}/logs/{1}.Txt", caminho, DateTime.Now.ToString("yyyyMMdd")), System.IO.FileMode.Append);
            System.IO.StreamWriter sw = new System.IO.StreamWriter(arquivoLog);
            System.Text.StringBuilder x = new System.Text.StringBuilder();
            x.AppendLine(System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            x.AppendFormat("|ORIGEM: {0}", ProgramaOrigem.ToString());
            x.AppendLine();
            x.AppendFormat("|EMPRESA: {0}", empresa);
            x.AppendLine();
            x.AppendFormat("|USER: {0}", Usuario);
            x.AppendLine();
            x.AppendFormat("|ERRO: {0}", erro == null ? "" : erro.Message);
            x.AppendLine();
            //x.AppendFormat("|MSG: {0}", Descricao);
            //x.AppendLine();
            x.AppendFormat("|TIPO: {0}", erro == null ? "" : erro.GetType().ToString());
            x.AppendLine();
            x.AppendFormat("|TRACE: {0}", erro == null ? "" : trace.ToString());
            x.AppendLine();
            x.AppendFormat("|EXTRA: {0}", MsgExtra);
            x.AppendLine();
            x.AppendLine("".PadRight(80, '-'));
            sw.WriteLine(x.ToString());
            sw.Close();
            arquivoLog.Close();

        }

        /// <summary>
        /// Registra erro no arquivo
        /// <para>REMARKS: Por padrão, é criada uma pasta 'logs' na raiz do site onde a Dll está incorporada.</para>
        /// <para>É criado um arquivo texto cujo nome é a data corrente no formato ano+mês+dia (por ex.: 20120426.Txt). Dessa forma, a cada dia cria-se um arquivo novo para facilitar a localização de erros.</para>
        /// </summary>
        /// <param name="texto">Um texto qualquer.</param>
        public void addArquivo(String texto)
        {
            HttpContext contexto = HttpContext.Current;
            if (!System.IO.Directory.Exists(contexto.Server.MapPath("~") + "/logs"))
                System.IO.Directory.CreateDirectory(contexto.Server.MapPath("~") + "/logs");
            string caminho = contexto.Server.MapPath("~");
            System.IO.FileStream arquivoLog = new System.IO.FileStream(String.Format("{0}/logs/{1}.Txt", caminho, DateTime.Now.ToString("yyyyMMdd")), System.IO.FileMode.Append);
            System.IO.StreamWriter sw = new System.IO.StreamWriter(arquivoLog);
            System.Text.StringBuilder x = new System.Text.StringBuilder();
            x.AppendLine(System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            x.AppendFormat("|MSG: {0}", texto);
            x.AppendLine();
            x.AppendLine("".PadRight(80, '-'));
            sw.WriteLine(x.ToString());
            sw.Close();
            arquivoLog.Close();
            //if(exibirPagErro)
            //    page.Response.Redirect("~/404.htm");
        }

        /// <summary>
        /// Registra erro no banco. Se não for possível conectar ou ocorrer algum problema, será gerado no arquivo. (Ver remarks de <see cref="addArquivo(System.Exception)"/>
        /// </summary>
        /// <param name="erro">Exceção capturada por um catch. (Use null para ignorar)</param>
        /// <param name="vars">Array com os valores das variáveis/parâmetros que se quer logar. Valores de arrays, structs e propriedades de classe são extraídas automaticamente.
        /// <para>Remarks: Loga uma string com os valores lidos (ver abaixo) separados por pipe ("|")</para>
        /// <para>Arrays: [a,b,c]</para>
        /// <para>Classes: (tipoDaClasse){tipoDaPropriedade:valor}</para>
        /// <para>Enum: (tipoDaEnum){valor}</para> 
        /// <para>Outros: valor</para> 
        /// </param>
        public void AdicionarLog(System.Exception erro, Object[] vars)
        {
            MySqlCommand sql = new MySqlCommand();
            MySqlParameter p;
            try
            {
                if (erro != null)
                {
                    #region MyRegion
                    //    //Capturando na pilha de erro apenas a ocorrencia que indica o módulo, método e linha onde o erro ocorreu
                    //    System.Diagnostics.StackTrace stack = new System.Diagnostics.StackTrace(erro, true);
                    //    //logando do ultimo ao primeiro frame. Ate que finalize os frames ou que sejam logados os 2 ultimos
                    //    //for (int i = stack.FrameCount - 1; i == 0 || (i >= stack.FrameCount - 3); i--)
                    //    //{
                    //    //    System.Diagnostics.StackFrame sf = stack.GetFrame(i);
                    //    //    string file = System.IO.Path.GetFileName(sf.GetFileName());
                    //    //    trace.AppendFormat(" linha {2} em {0} : {1}", file, sf.GetMethod(), sf.GetFileLineNumber());
                    //    //}
                    //    System.Diagnostics.StackFrame sf = stack.GetFrame(stack.FrameCount - 1); //logando apenas o ultimo frame
                    //    string file = System.IO.Path.GetFileName(sf.GetFileName());
                    //    trace.AppendFormat(" em {1} na {0}: linha {2}", file, sf.GetMethod(), sf.GetFileLineNumber());
                    //trace.Append(erro.StackTrace); 
                    #endregion
                    this.tipoErro += erro.GetType().ToString();
                    this.MsgTrace += erro.StackTrace;
                    this.MsgErro += erro.Message;
                }


                p = new MySqlParameter("@EMPRESA", MySqlDbType.Int32);
                p.Value = empresa;
                sql.Parameters.Add(p);

                p = new MySqlParameter("@TIPO", MySqlDbType.VarChar);
                p.Value = Utils.AlterarStringQuery(Utils.TruncarStringNoInicio(tipoErro, 50));
                sql.Parameters.Add(p);

                p = new MySqlParameter("@ERRO", MySqlDbType.VarChar);
                p.Value = Utils.AlterarStringQuery(Utils.TruncarString(msgErro, 200));
                sql.Parameters.Add(p);

                p = new MySqlParameter("@DESCRICAO", MySqlDbType.VarChar);
                p.Value = Utils.AlterarStringQuery(Descricao);
                sql.Parameters.Add(p);

                p = new MySqlParameter("@DATA", MySqlDbType.DateTime);
                p.Value = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                sql.Parameters.Add(p);

                p = new MySqlParameter("@ORIGEM", MySqlDbType.VarChar);
                p.Value = ProgramaOrigem.ToString();
                sql.Parameters.Add(p);

                p = new MySqlParameter("@EXTRA", MySqlDbType.Text);
                p.Value = Utils.AlterarStringQuery(MsgExtra + getValues(vars));
                sql.Parameters.Add(p);

                p = new MySqlParameter("@TRACE", MySqlDbType.Text);
                p.Value = Utils.AlterarStringQuery(Utils.TruncarStringNoInicio(MsgTrace, 8000));//8000 é o tamanho máximo de um varchar. Um tamanho razoável para truncar a pilha de erro.
                sql.Parameters.Add(p);

                p = new MySqlParameter("@CODIGO", MySqlDbType.Int32);
                p.Value = Usuario;
                sql.Parameters.Add(p);

                sql.CommandText = String.Format("insert into erros(empresa, tipo, descricao, erro, descricao, data, origem, extra, trace, user) values(@IMOBILIARIA, @TIPO, @ERRO, @DESCRICAO, @DATA, @ORIGEM, @EXTRA, @TRACE, @CODIGO)");

                int linhasAlteradas = new Data().execute(sql);
                if (linhasAlteradas.Equals(0))
                    addArquivo(erro);
            }
            catch
            {
                addArquivo(erro);
            }
            finally
            {
                sql.Dispose();
            }
        }

        /// <summary>
        /// Registra erro no banco. Se não for possível conectar ou ocorrer algum problema, será gerado no arquivo. (Ver remarks de <see cref="addArquivo(System.Exception)"/>
        /// </summary>
        /// <param name="erro">Exceção capturada por um catch.(Use null para ignorar)</param>        
        public void AdicionarLog(System.Exception erro)
        {
            AdicionarLog(erro, null);
        }

        /// <summary>
        /// 
        /// </summary>
        public void AdicionarLog()
        {
            AdicionarLog(null, null);
        }

        /// <summary>
        /// Lê o valor dos elementos do array.
        /// <para>RETURNS: Uma string com os valores lidos(ver abaixo) separados por pipe ("|")</para>
        /// <para>Formato:Arrays: [a,b,c]</para>
        /// <para>Classes: (tipoDaClasse){tipoDaPropriedade:valor}</para>
        /// <para>Enum: (tipoDaEnum){valor}</para> 
        /// <para>Outros: valor</para> 
        /// </summary>
        /// <param name="vars">Array com o valor das variáveis. Valores em  arrays, structs e propriedades de classe são extraídas automaticamente. </param>
        public string getValues(Object[] vars)
        {
            StringBuilder retorno = new StringBuilder();
            try
            {
                if (vars == null || vars.Length == 0)
                    return "";


                foreach (object obj in vars)
                {
                    retorno.AppendFormat("{0}|", getValue(obj));
                }

                return retorno.ToString();
            }
            catch (Exception ex)
            {
                Erro erro = new Erro(Erro.produtos.ImunneVacinas);
                erro.AdicionarLog(ex);
                return retorno.ToString();
            }
        }

        /// <summary>
        /// Lê o valor de um objeto.
        /// <para>RETURNS: Uma string com o valors lido. Formato:
        /// <para>Arrays: [a,b,c]</para>
        /// <para>Classes: (tipoDaClasse){tipoDaPropriedade:valor}</para>
        /// <para>Enum: (tipoDaEnum){valor}</para>
        /// </summary>
        /// <param name="vars">Array com o valor das variáveis. Valores em  arrays, structs e propriedades de classe são extraídas automaticamente. </param>       
        private string getValue(Object obj)
        {
            StringBuilder retorno = new StringBuilder();

            if (obj == null)
                retorno.AppendFormat(" {0}", "null");

            else if (obj.GetType().IsPrimitive || obj.GetType().Name.Equals("String"))
                retorno.AppendFormat(" {0}", obj);

            else if (obj.GetType().IsArray)
            {
                Array ar = (Array)obj;
                retorno.Append("[");
                if (obj != null && ar.Length > 0)
                {
                    for (int i = 0; i < ar.Length; i++) // || i == 5 limitando resultados em 5
                    {
                        retorno.AppendFormat("{0},", getValue(ar.GetValue(i)));
                    }
                    retorno = retorno.Remove(retorno.Length - 1, 1); //removendo a ultima vírgulas
                }
                retorno.Append("]");
            }

            else if (obj.GetType().FullName.Equals("System.Data.MySqlClient.MySqlParameterCollection"))
            {
                retorno.AppendFormat(" ({0})", obj.GetType());
                retorno.Append("{");
                MySql.Data.MySqlClient.MySqlParameterCollection col = (MySql.Data.MySqlClient.MySqlParameterCollection)obj;
                for (int i = 0; i < col.Count; i++)
                {

                    String nome = col[i].ParameterName;
                    object valor = col[i].Value;
                    if (valor == null)
                        valor = "null";
                    retorno.AppendFormat(" {0}:{1}", nome, valor);
                }
                retorno.Append("}");
            }
            else if (obj.GetType().IsClass)
            {
                retorno.AppendFormat(" ({0})", obj.GetType());
                retorno.Append("{");
                foreach (System.Reflection.PropertyInfo p in obj.GetType().GetProperties())
                {
                    String nome = p.Name;
                    object valor = obj.GetType().GetProperty(nome).GetValue(obj, null);

                    //String tipo = p.PropertyType.Name;
                    if (valor == null)
                        valor = "null";
                    else if (p.PropertyType.IsArray)
                        valor = getValue(valor);

                    retorno.AppendFormat(" {0}:{1}", nome, valor);
                }
                retorno.Append("}");
            }
            else if (obj.GetType().IsEnum)
                retorno.AppendFormat(" ({0}){1}", obj.GetType(), obj);
            else
                retorno.AppendFormat(" {0}", obj);


            return retorno.ToString();
        }
    }

    /// <summary>
    /// Informações, rotinas e propriedades do elemento Log
    /// </summary>
    [Serializable]
    public class Log
    {
        private const string _tabela = "Logs";
        private static List<BD.Columns> columns;
        private static DateTime columnsUpdated;

        #region Declaração das propriedades do elemento

        private Usuario _idUsuario;
        private int _codigo;
        private DateTime _dataAcao;
        private Utils.AcaoLog _tipoAcao;
        private string _tela;
        private int _registro;
        private string _tabelaAcao;
        private string _campo;
        private string _valorOriginal;
        private string _valorNovo;
        private string _resumo;

        /// <summary>
        /// Campo: usuario
        /// </summary>
        public Usuario IdUsuario
        {
            get { return _idUsuario; }
            set { _idUsuario = value; }
        }

        /// <summary>
        /// Campo: codigo
        /// </summary>
        public int Codigo
        {
            get { return _codigo; }
            set { _codigo = value; }
        }

        /// <summary>
        /// Campo: dataAcao
        /// </summary>
        public DateTime DataAcao
        {
            get { return _dataAcao; }
            set { _dataAcao = value; }
        }

        /// <summary>
        /// Campo: tipOAcao
        /// </summary>
        public Utils.AcaoLog TipoAcao
        {
            get { return _tipoAcao; }
            set { _tipoAcao = value; }
        }

        /// <summary>
        /// Campo: tela
        /// </summary>
        public string Tela
        {
            get { return _tela; }
            set { _tela = value; }
        }

        /// <summary>
        /// Campo: registro
        /// </summary>
        public int Registro
        {
            get { return _registro; }
            set { _registro = value; }
        }

        /// <summary>
        /// Campo: tabela
        /// </summary>
        public string TabelaAcao
        {
            get { return _tabelaAcao; }
            set { _tabelaAcao = value; }
        }

        /// <summary>
        /// Campo: campo
        /// </summary>
        public string Campo
        {
            get { return _campo; }
            set { _campo = value; }
        }

        /// <summary>
        /// Campo: valorOriginal
        /// </summary>
        public string ValorOriginal
        {
            get { return _valorOriginal; }
            set { _valorOriginal = value; }
        }

        /// <summary>
        /// Campo: valorNovo
        /// </summary>
        public string ValorNovo
        {
            get { return _valorNovo; }
            set { _valorNovo = value; }
        }

        /// <summary>
        /// Campo: resumo
        /// </summary>
        public string Resumo
        {
            get { return _resumo; }
            set { _resumo = value; }
        }

        #endregion

        #region GetInfo de todas as propriedades

        /// <summary>
        /// Busca informações, no banco de dados, sobre os campos da tabela.
        /// </summary>
        private static void GetInfoTabela()
        {
            if (columns == null || (DateTime.Now - columnsUpdated).Days > 0) //Atualiza com mais de 24h de diferença
            {
                columns = BD.Columns.getColumnsInfo(_tabela);
                columnsUpdated = DateTime.Now;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoCodigo()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "codigo");

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoDataAcao()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "dataAcao");

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoUsuario()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "usuario");

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoResumo()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "resumo");

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoTabelaBase()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "tabela");

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoRegistro()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "registro");

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoTela()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "tela");

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoCampo()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "campo");

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoValorOriginal()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "valorOriginal");

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoValorNovo()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "valorNovo");

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoTipoAcao()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "tipoAcao");

        }

        #endregion

        /// <summary>
        /// Inicialização das propriedades do elemento Log com seus valores padrão
        /// </summary>
        private void PopularCampos()
        {
            GetInfoTabela();

            _idUsuario = new Usuario();

            _idUsuario.Codigo = -1;
            _campo = String.Empty;
            _dataAcao = DateTime.Now;
            _registro = -1;
            _resumo = String.Empty;
            _tabelaAcao = String.Empty;
            _tela = String.Empty;
            _tipoAcao = Utils.AcaoLog.Acesso;
            _valorNovo = String.Empty;
            _valorOriginal = String.Empty;
        }

        /// <summary>
        /// Inicialização do elemento Cliente
        /// </summary>
        public Log()
        {
            PopularCampos();
        }

        /// <summary> 
        /// Rotina que cria um Array com os parâmetros utilizados nas ações de INSERT, UPDATE e DELETE
        /// <returns>Array com vários MySqlParameters</returns>
        /// </summary>
        private Array GetParametrosSQL()
        {
            MySqlParameter p;
            ArrayList ap = new ArrayList();

            p = new MySqlParameter("@codigo", Parser.parseInteiro(_codigo));
            p.MySqlDbType = GetInfoCodigo().Type;
            ap.Add(p);

            p = new MySqlParameter("@dataAcao", Parser.parseDateTime(_dataAcao));
            p.MySqlDbType = GetInfoDataAcao().Type;
            ap.Add(p);

            p = new MySqlParameter("@usuario", Parser.parseString(_idUsuario.Codigo));
            p.MySqlDbType = GetInfoUsuario().Type;
            ap.Add(p);

            p = new MySqlParameter("@resumo", Parser.parseString(_resumo));
            p.MySqlDbType = GetInfoResumo().Type;
            ap.Add(p);

            p = new MySqlParameter("@tabelaBase", Parser.parseString(_tabelaAcao));
            p.MySqlDbType = GetInfoTabelaBase().Type;
            ap.Add(p);

            p = new MySqlParameter("@registro", Parser.parseInteiro(_registro));
            p.MySqlDbType = GetInfoRegistro().Type;
            ap.Add(p);

            p = new MySqlParameter("@tela", Parser.parseString(_tela));
            p.MySqlDbType = GetInfoTela().Type;
            ap.Add(p);

            p = new MySqlParameter("@campo", Parser.parseString(_campo));
            p.MySqlDbType = GetInfoCampo().Type;
            ap.Add(p);

            p = new MySqlParameter("@valorOriginal", Parser.parseString(_valorOriginal));
            p.MySqlDbType = GetInfoValorOriginal().Type;
            ap.Add(p);

            p = new MySqlParameter("@valorNovo", Parser.parseString(_valorNovo));
            p.MySqlDbType = GetInfoValorNovo().Type;
            ap.Add(p);

            p = new MySqlParameter("@tipoAcao", (int)_tipoAcao);
            p.MySqlDbType = GetInfoTipoAcao().Type;
            ap.Add(p);

            return ap.ToArray();
        }

        /// <summary>
        /// Rotina que adiciona os dados de um novo referência
        /// </summary>
        /// <returns></returns>
        public int Incluir()
        {
            //if (_idCliente.Codigo <= 0 || _idUsuario.Codigo <= 0)
            //    throw new Exception("WebGuess - parâmetros com valores inválidos");

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    sql.Parameters.AddRange(GetParametrosSQL());
                    sql.CommandText = "INSERT INTO Logs "
                                    + "(usuario, dataAcao, tipoAcao, "
                                    + "tela, registro, tabela, campo, valorOriginal, "
                                    + "valorNovo, resumo) "
                                    + "VALUE(@usuario, @dataAcao, @tipoAcao, "
                                    + "@tela, @registro, @tabelaBase, @campo, @valorOriginal, "
                                    + "@valorNovo, @resumo); "
                                    + "SELECT LAST_INSERT_ID();";
                    int resultado = Parser.parseInteiro(new Data().executeEscalar(sql));
                    return resultado;
                }
            }
            catch (ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="codigo"></param>
        /// <returns></returns>
        public static Log ConsultarUnico(int codigo)
        {
            if (codigo < 0)
                return null;

            PesquisaLog pesquisa = new PesquisaLog();
            pesquisa.Codigo = codigo;

            List<Log> retornos = ConsultaGenerica(pesquisa);
            if (retornos == null || retornos.Count == 0)
                return null;
            else return retornos[0];
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pesquisa"></param>
        /// <returns></returns>
        public static List<Log> ConsultarLogs(PesquisaLog pesquisa)
        {
            return ConsultaGenerica(pesquisa);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pesquisa"></param>
        /// <returns></returns>
        internal static List<Log> ConsultaGenerica(PesquisaLog pesquisa)
        {
            StringBuilder top = new StringBuilder();
            StringBuilder order = new StringBuilder();
            StringBuilder filtro = new StringBuilder();
            MySqlParameter p;

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    #region Campo Top

                    if (pesquisa.Quantidade > 0)
                    {
                        filtro.Append(" LIMIT @quantidade");
                        p = new MySqlParameter("@quantidade", pesquisa.Quantidade);
                        p.MySqlDbType = MySqlDbType.Int32;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Código

                    if (pesquisa.Codigo > 0)
                    {
                        filtro.Append(" AND Logs.codigo = @codigo");
                        p = new MySqlParameter("@codigo", pesquisa.Codigo);
                        p.MySqlDbType = GetInfoCodigo().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Usuário

                    if (pesquisa.IdUsuario > 0)
                    {
                        filtro.Append(" AND Logs.usuario = @usuario");
                        p = new MySqlParameter("@usuario", pesquisa.IdUsuario);
                        p.MySqlDbType = GetInfoUsuario().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Ação

                    if (pesquisa.AcaoRealizada != Utils.AcaoLog.Indiferente)
                    {
                        filtro.Append(" AND Logs.tipoAcao = @acaoRealizada");
                        p = new MySqlParameter("@acaoRealizada", (int)pesquisa.AcaoRealizada);
                        p.MySqlDbType = GetInfoTipoAcao().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    order.Append("ORDER BY Logs.dataAcao DESC");

                    if (filtro.Length > 0)
                        filtro.Replace(" AND ", " WHERE ", 0, 5);

                    sql.CommandText = String.Format("SELECT * FROM Logs "
                                    + "{0} {1} {2}", filtro, order, top);
                    DataTable dt = new Data().getData(sql);
                    return MontarRetorno(dt);
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        internal static List<Log> MontarRetorno(DataTable dt)
        {
            List<Log> retornos = new List<Log>();

            try
            {
                if (dt != null && dt.Rows.Count > 0)
                {
                    Log item;
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        item = new Log();

                        item._codigo = Parser.parseInteiro(dt.Rows[i]["codigo"]);
                        item._dataAcao = Parser.parseDateTime(dt.Rows[i]["dataAcao"]);
                        item._idUsuario.Codigo = Parser.parseInteiro(dt.Rows[i]["usuario"]);
                        item._tipoAcao = (Utils.AcaoLog)Enum.Parse(typeof(Utils.AcaoLog), dt.Rows[i]["tipoAcao"].ToString());
                        item._tela = Parser.parseString(dt.Rows[i]["tela"]);
                        item._registro = Parser.parseInteiro(dt.Rows[i]["registro"]);
                        item._tabelaAcao = Parser.parseString(dt.Rows[i]["tabela"]);
                        item._campo = Parser.parseString(dt.Rows[i]["campo"]);
                        item._valorOriginal = Parser.parseString(dt.Rows[i]["valorOriginal"]);
                        item._valorNovo = Parser.parseString(dt.Rows[i]["valorNovo"]);
                        item._resumo = Parser.parseString(dt.Rows[i]["resumo"]);

                        retornos.Add(item);
                    }
                }

                return retornos;
            }
            catch
            {
                throw;
            }
        }
    }
}
