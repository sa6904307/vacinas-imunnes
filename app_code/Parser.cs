﻿using System;
using System.Text;

namespace ImunneVacinas
{
    public class Parser
    {
        /// <summary>
        /// Data mínima do sqlServer, que usaremos como default (1900-01-01 00:00:00.000)
        /// </summary>
        public readonly static DateTime dataPadrao = new DateTime(1900, 01, 01);

        /// <summary>
        /// Formata o número no padrão americano, usado no Banco de Dados.
        /// Padrão: #####.##
        /// </summary>
        /// <param name="valor"></param>
        public static string parseNumeroBD(object valor)
        {
            try
            {
                if (valor == null || valor.ToString().Trim().Equals(""))
                    return "0";
                Double temp = 0;
                if (Double.TryParse(valor.ToString(), out temp)) //Serve para double, decimal ou inteiro
                {
                    string[] milhares = null;
                    StringBuilder valorfinal = null;
                    milhares = valor.ToString().Split(new char[] { ',', '.' });
                    valorfinal = new StringBuilder();
                    for (int i = 0; i < milhares.Length; i++)
                    {
                        string p = "";
                        if (milhares.Length - 2 == i)//Se penultimo, acrescenta sinal decimal
                            p = ".";
                        valorfinal.AppendFormat("{0}{1}", milhares[i], p);
                    }
                    return valorfinal.ToString();
                }
                else
                    return "0";
            }
            catch (Exception ex)
            {
                return valor.ToString();
            }
        }

        public static string parseString(object valor, string valorDefault)
        {
            if (valor == null)
                return valorDefault;
            if (!String.IsNullOrEmpty(valor.ToString()))
                return valor.ToString().Trim();
            else
                return valorDefault;
        }
        /// <summary>
        /// remove espaços em branco de uma string. Retorna uma string vazia se Null
        /// </summary>
        /// <param name="valor"></param>
        public static string parseString(object valor)
        {
            if (valor == null)
                return "";
            if (!String.IsNullOrEmpty(valor.ToString()))
                return valor.ToString().Trim();
            else
                return "";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="valor"></param>
        /// <returns></returns>
        public static bool parseBool(object valor)
        {
            if (valor == null)
                return false;

            if (parseString(valor).ToLower().Equals(bool.FalseString.ToLower()) || parseString(valor).ToLower().Equals(bool.TrueString.ToLower()))
                return bool.Parse(valor.ToString());
            else
                if (parseInteiro(valor).Equals(0))
                    return false;
                else
                    return true;
        }
        /// <summary>
        /// Converte uma string em um inteiro. Se a string não contiver um inteiro, retorna zero.
        /// </summary>
        /// <param name="valor"></param>
        public static int parseInteiro(object valor)
        {
            return parseInteiro(valor, 0);
        }

        /// <summary>
        /// Converte uma string em um inteiro. Se a string não contiver um inteiro, retorna o número indicado no parâmetro 'retorno'.
        /// </summary>
        /// <param name="valor"></param>
        /// <param name="retorno"></param>
        /// <returns></returns>
        public static int parseInteiro(object valor, int retorno)
        {
            if (valor == null)
                return retorno;
            if (String.IsNullOrEmpty(valor.ToString()) || valor.ToString().Trim().Equals(""))
                return retorno;
            int temp = retorno;
            if (int.TryParse(valor.ToString(), out temp))
                temp = Convert.ToInt32(valor);
            return temp;
        }

        /// <summary>
        /// Converte uma string em um inteiro. Se a string não contiver um inteiro, retorna zero.
        /// </summary>
        /// <param name="valor"></param>
        public static long parseLong(object valor)
        {
            return parseLong(valor, 0);
        }
        /// <summary>
        /// Converte uma string em um inteiro. Se a string não contiver um inteiro, retorna o número indicado no parâmetro 'retorno'.
        /// </summary>
        /// <param name="valor"></param>
        /// <param name="retorno"></param>
        /// <returns></returns>
        public static long parseLong(object valor, int retorno)
        {
            if (valor == null)
                return retorno;
            if (String.IsNullOrEmpty(valor.ToString()) || valor.ToString().Trim().Equals(""))
                return retorno;
            long temp = retorno;
            if (long.TryParse(valor.ToString(), out temp))
                temp = Convert.ToInt64(valor);
            return temp;
        }

        /// <summary>
        /// Converte uma string em um decimal. Se a string não contiver um decimal, retorna zero.
        /// </summary>
        /// <param name="valor"></param>
        public static decimal parseDecimal(object valor)
        {
            return parseDecimal(valor, 0);
        }
        /// <summary>
        /// Converte uma string em um decimal. Se a string não contiver um decimal, retorna número indicado no parâmetro 'retorno'.
        /// </summary>
        /// <param name="valor"></param>
        public static decimal parseDecimal(object valor, int retorno)
        {
            if (valor == null)
                return 0;
            //else
            //    return (parseDecimal(valor.ToString()));
            if (String.IsNullOrEmpty(valor.ToString()) || valor.ToString().Trim().Equals(""))
                return retorno;
            decimal temp = retorno;
            valor = parseNumeroBD(valor);
            if (decimal.TryParse(valor.ToString(), out temp))
                temp = Convert.ToDecimal(valor, System.Globalization.CultureInfo.GetCultureInfo("en-US"));
            return temp;
        }

        /// <summary>
        /// Converte uma string em um double. Se a string não contiver um double, retorna zero.
        /// </summary>
        /// <param name="valor"></param>
        public static double parseDouble(object valor)
        {
            if (valor == null || valor.ToString().Trim().Equals(""))
                return 0;
            double temp = 0;
            valor = parseNumeroBD(valor);
            if (double.TryParse(valor.ToString(), out temp))
                temp = Convert.ToDouble(valor, System.Globalization.CultureInfo.GetCultureInfo("en-US"));
            return temp;
        }

        /// <summary>
        /// Converte uma string em DateTime. Se a string for vazia ou inválida, retorna a data padrão (1900-01-01)
        /// </summary>
        /// <param name="data">String contendo a data</param>
        public static DateTime parseDateTime(object data)
        {
            try
            {
                if (data == null)
                    return dataPadrao;

                DateTime date = new DateTime();
                if (DateTime.TryParse(data.ToString(), out date))
                {
                    if (date < dataPadrao) //A data mínima aceita pelo Sql Server é maior do que a permitida pelo C#, então retornamos a data padrão para não dar erro nas Consultas ao BD
                        return dataPadrao;
                    return date;
                }
                else
                    return dataPadrao;

            }
            catch
            {
                return dataPadrao;
            }
        }

        /// <summary>
        /// Converte uma string em TimeSpan. Se a string for vazia ou inválida, retorna a hora padrão (00:00:00)
        /// </summary>
        /// <param name="hora">String contendo a hora</param>
        public static TimeSpan toTime(object hora)
        {
            TimeSpan horaPadrao = new TimeSpan(0, 0, 0);
            try
            {
                if (hora == null)
                    return horaPadrao;

                TimeSpan date = new TimeSpan();
                if (TimeSpan.TryParse(hora.ToString(), out date))
                {
                    return date;
                }
                else
                    return horaPadrao;

            }
            catch
            {
                return horaPadrao;
            }
        }

        /// <summary>
        /// Formata uma string que contém uma data no formato "yyyy-MM-dd HH:mm:ss" para ser armazenada no banco.
        /// </summary>
        /// <param name="valor"></param>
        /// <param name="permitirDataEmBranco"></param>
        /// <returns></returns>
        public static string parseDateTimeBD(string valor, bool permitirDataEmBranco)
        {
            if (String.IsNullOrEmpty(valor) || valor.Trim().Equals(""))
            {
                if (permitirDataEmBranco)
                    return "";
                else
                    return dataPadrao.ToString();
            }
            else
                return parseDateTimeBD(Convert.ToDateTime(valor));
        }

        /// <summary>
        /// Formata uma string que contém uma data no formato "yyyy-MM-dd HH:mm:ss" para ser armazenada no banco.
        /// </summary>
        public static string parseDateTimeBD(DateTime valor)
        {
            try
            {
                DateTime date = new DateTime();
                TimeSpan time = new TimeSpan();

                if (TimeSpan.TryParse(valor.ToString(), out time)) //testando formatos de hora: 00:00:00
                    return valor.ToString();
                else if (DateTime.TryParse(valor.ToString(), out date)) //testando data
                {
                    if (valor < dataPadrao) //A data mínima aceita pelo Sql Server é maior do que a permitida pelo C#, então retornamos a data padrão para não dar erro nas Consultas ao BD
                        return dataPadrao.ToString("yyyy-MM-dd HH:mm:ss");

                    else if (date.ToLongTimeString().Equals("00:00:00")) //se o campo for apenas uma data
                        return Convert.ToDateTime(date).ToString("yyyy-MM-dd");
                    else
                        return Convert.ToDateTime(date).ToString("yyyy-MM-dd HH:mm:ss"); //se for uma data completa
                }
                else
                    return dataPadrao.ToString("yyyy-MM-dd HH:mm:ss");

            }
            catch (Exception ex)
            {
                return valor.ToString();
            }
        }

        /// <summary>
        /// Formata uma data para exibição em tela (dd/MM/yyyy)
        /// </summary>
        /// <param name="valor">Objeto DateTime a ser formatado</param>
        /// <param name="omitirHora">Exibir hora na string de saída?</param>
        public static string dateToString(object valor, bool omitirHora)
        {
            return dateToString(valor, omitirHora, false);
        }
        /// <summary>
        /// Formata uma data para exibição em tela (dd/MM/yyyy)
        /// </summary>
        /// <param name="valor">Objeto DateTime a ser formatado </param>
        /// <param name="omitirHora">Exibir hora na string de saída?</param>
        /// <param name="permitirDataEmBranco">Retonar string em branco caso a data seja a default(1900-01-01)? </param>        
        public static string dateToString(object valor, bool omitirHora, bool permitirDataEmBranco)
        {
            try
            {
                DateTime date = new DateTime();

                if (DateTime.TryParse(valor.ToString(), out date)) //testando data
                {
                    if (date.Equals(dataPadrao) && permitirDataEmBranco)
                        return "";

                    if (omitirHora)
                        return Convert.ToDateTime(date).ToString("dd/MM/yyyy");
                    else
                        return Convert.ToDateTime(date).ToString("dd/MM/yyyy HH:mm:ss"); //se for uma data completa
                }
                else
                    return "";

            }
            catch (Exception ex)
            {
                return valor.ToString();
            }
        }

    }
}