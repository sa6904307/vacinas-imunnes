﻿using MySql.Data.MySqlClient;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace ImunneVacinas
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class Cidade
    {
        private static string _tabela = "Cidades";
        private static List<BD.Columns> columns;
        private static DateTime columnsUpdated;

        #region Propriedades: uma para cada campo da tabela

        private int _codigo;
        /// <summary>
        /// 
        /// </summary>
        public int Codigo
        {
            get
            {
                return _codigo;
            }
            set
            {
                _codigo = value;
            }
        }

        private string _estado;
        /// <summary>
        /// 
        /// </summary>
        public string Estado
        {
            get
            {
                return _estado;
            }
            set
            {
                _estado = value;
            }
        }

        private string _nome;
        /// <summary>
        /// 
        /// </summary>
        public string Nome
        {
            get
            {
                return _nome;
            }
            set
            {
                _nome = value;
            }
        }

        private string _cepInicial;
        /// <summary>
        /// 
        /// </summary>
        public string CepInicial
        {
            get
            {
                return _cepInicial;
            }
            set
            {
                _cepInicial = value;
            }
        }

        private string _cepFinal;
        /// <summary>
        /// 
        /// </summary>
        public string CepFinal
        {
            get
            {
                return _cepFinal;
            }
            set
            {
                _cepFinal = value;
            }
        }

        private string _latitude;
        /// <summary>
        /// 
        /// </summary>
        public string Latitude
        {
            get
            {
                return _latitude;
            }
            set
            {
                _latitude = value;
            }
        }

        private string _longitude;
        /// <summary>
        /// 
        /// </summary>
        public string Longitude
        {
            get
            {
                return _longitude;
            }
            set
            {
                _longitude = value;
            }
        }

        #endregion

        #region GetInfos

        /// <summary>
        /// 
        /// </summary>
        private static void GetInfoTabela()
        {
            if (columns == null || (DateTime.Now - columnsUpdated).Minutes > 0)
            {
                columns = ImunneVacinas.BD.Columns.getColumnsInfo(_tabela);
                columnsUpdated = DateTime.Now;
            }
        }

        /// <summary>
        /// Retorna as propriedade da coluna: codigo_cidade
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoCodigo()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "codigo_cidade");
        }

        /// <summary>
        /// Retorna as propriedade da coluna: nome_cidade
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoNome()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "nome_cidade");
        }

        /// <summary>
        /// Retorna as propriedade da coluna: sigla_estado
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoEstado()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "sigla_estado");
        }

        /// <summary>
        /// Retorna as propriedade da coluna: cep_inicial
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoCepInicial()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "cep_inicial");
        }

        /// <summary>
        /// Retorna as propriedade da coluna: cep final
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoCepFinal()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "cep_final");
        }

        /// <summary>
        /// Retorna as propriedade da coluna: cidade_latitude
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoLatitude()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "cidade_latitude");
        }

        /// <summary>
        /// Retorna as propriedade da coluna: cidade_longitude
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoLongitude()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "cidade_longitude");
        }

        #endregion

        /// <summary> 
        /// Rotina que cria um Array com os parâmetros utilizados nas ações de INSERT, UPDATE e DELETE
        /// <returns>Array com vários MySqlParameters</returns>
        /// </summary>
        private Array GetParametrosSQL()
        {
            MySqlParameter p;
            ArrayList ap = new ArrayList();

            p = new MySqlParameter("@codigo_cidade", Parser.parseInteiro(_codigo));
            p.MySqlDbType = GetInfoCodigo().Type;
            ap.Add(p);

            p = new MySqlParameter("@nome_cidade", Parser.parseString(_nome));
            p.MySqlDbType = GetInfoNome().Type;
            ap.Add(p);

            p = new MySqlParameter("@sigla_estado", Parser.parseString(_estado));
            p.MySqlDbType = GetInfoEstado().Type;
            ap.Add(p);

            p = new MySqlParameter("@cep_inicial", Parser.parseString(_cepInicial));
            p.MySqlDbType = GetInfoCepInicial().Type;
            ap.Add(p);

            p = new MySqlParameter("@cep_final", Parser.parseString(_cepFinal));
            p.MySqlDbType = GetInfoCepFinal().Type;
            ap.Add(p);

            p = new MySqlParameter("@cidade_latitude", Parser.parseString(_latitude));
            p.MySqlDbType = GetInfoLatitude().Type;
            ap.Add(p);

            p = new MySqlParameter("@cidade_longitude", Parser.parseString(_longitude));
            p.MySqlDbType = GetInfoLongitude().Type;
            ap.Add(p);

            return ap.ToArray();
        }

        /// <summary>
        /// Rotina que insere o registro no BD
        /// </summary>
        /// <returns>Código do registro inserido no BD</returns>
        public int Incluir()
        {
            if (String.IsNullOrEmpty(_nome))
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    sql.Parameters.AddRange(GetParametrosSQL());
                    sql.CommandText = "INSERT INTO Cidades "
                                    + "(sigla_estado, nome_cidade, cep_inicial, cep_final, cidade_latitude, cidade_longitude) "
                                    + "VALUE(@sigla_estado, @nome_cidade, @cep_inicial, @cep_final, @cidade_latitude, @cidade_longitude); "
                                    + "SELECT LAST_INSERT_ID();";
                    int resultado = Parser.parseInteiro(new Data().executeEscalar(sql));
                    return resultado;
                }
            }
            catch (ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Rotina que altera os dados do registro no BD
        /// </summary>
        /// <returns>Quantidade de linhas alteradas na ação junto ao BD</returns>
        public int Alterar()
        {
            if (_codigo < 0)
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    sql.Parameters.AddRange(GetParametrosSQL());
                    sql.CommandText = "UPDATE Cidades "
                                    + "SET sigla_estado = @sigla_estado, nome_cidade = @nome_cidade, "
                                    + "cep_inicial = @cep_inicial, cep_final = @cep_final, cidade_latitude = @cidade_latitude, "
                                    + "cidade_longitude = @cidade_longitude "
                                    + "WHERE codigo_cidade = @codigo_cidade; ";
                    int resultado = Parser.parseInteiro(new Data().execute(sql));
                    return resultado;
                }
            }
            catch (ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Rotina que localiza e retorna um único registro, mediante o código informado
        /// </summary>
        /// <param name="codigo">VALUE: código de identificação do registro (obrigatório)</param>
        /// <returns>Elemento Usuario com os dados retornados da consulta</returns>
        public static Cidade ConsultarUnico(int codigo)
        {
            if (codigo <= 0)
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            PesquisaCidade pesquisa = new PesquisaCidade()
            {
                Codigo = codigo,
                Ordenacao = Utils.TipoOrdenacao.porCodigo
            };

            List<Cidade> retornos = ConsultaGenerica(pesquisa);
            if (retornos != null && retornos.Count > 0)
                return retornos[0];
            else
                return null;
        }

        /// <summary>
        /// Rotina que realiza a pesquisa de elementos mediante os parametros de pesquisa informados
        /// </summary>
        /// <param name="pesquisa">VALUE: elemento de pesquisa com os parametros para consulta</param>
        /// <returns></returns>
        public static List<Cidade> ConsultarCidades(PesquisaCidade pesquisa)
        {
            return ConsultaGenerica(pesquisa);
        }

        /// <summary>
        /// Rotina que retorna um elemento RetornoCBX para utilização em DropLists
        /// </summary>
        /// <param name="uf">VALUE: Elemento com dados de pesquisa</param>
        /// <param name="InserirLinhaPadrao">VALUE: Informa se será adiconada a linha padrão</param>
        /// <param name="textoPadrao">VALUE: Informa o texto utilizado para linha padão (se utilizado NULL, será apresentado SELECIONE)</param>
        /// <returns>Lista do elemento RetornoCBX com os dados desejados</returns>
        public static List<RetornoComboBox> ConsultarComboBox(string uf, bool InserirLinhaPadrao, string textoPadrao)
        {
            DataTable dt = new DataTable();
            try
            {
                dt.Columns.Add("DisplayMember");
                dt.Columns.Add("ValueMember");

                ImunneVacinas.PesquisaCidade pesquisa = new PesquisaCidade()
                {
                    Uf = uf,
                    Ordenacao = Utils.TipoOrdenacao.porNome
                };

                List<Cidade> retorno = ConsultaGenerica(pesquisa);
                if (retorno != null)
                {
                    foreach (Cidade c in retorno)
                    {
                        dt.Rows.Add(new object[] { c.Nome.ToUpper(), c.Codigo });
                    }
                }
                return Utils.MontarRetornoComboBox(dt, InserirLinhaPadrao, textoPadrao);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// Rotina interna utilizada para todas as pesquisas do elemento
        /// </summary>
        /// <param name="pesquisa">VALUE: Elemento com os parâmetros com os quais será realizada a pesquisa</param>
        /// <returns>Lista do elemento com os dados retornados da consulta</returns>
        internal static List<Cidade> ConsultaGenerica(PesquisaCidade pesquisa)
        {
            StringBuilder ordenacao = new StringBuilder();
            StringBuilder filtro = new StringBuilder();
            StringBuilder top = new StringBuilder();
            MySqlParameter p;

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    if (pesquisa.Quantidade > 0)
                        top.AppendFormat(" LIMIT {0} ", pesquisa.Quantidade);

                    #region Campo Código

                    if (pesquisa.Codigo >= 0)
                    {
                        filtro.Append(" AND Cidades.codigo_cidade = @codigo_cidade");
                        p = new MySqlParameter("@codigo_cidade", pesquisa.Codigo);
                        p.MySqlDbType = (MySqlDbType)MySqlDbType.Int32;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Estado

                    if (!String.IsNullOrEmpty(pesquisa.Uf) && pesquisa.Uf != "-1")
                    {
                        filtro.Append(" AND Cidades.sigla_estado = @sigla_estado");
                        p = new MySqlParameter("@sigla_estado", pesquisa.Uf);
                        p.MySqlDbType = (MySqlDbType)MySqlDbType.VarChar;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Pesquisa

                    if (!String.IsNullOrEmpty(pesquisa.CampoLivre))
                    {
                        string campoLivre = String.Format("%{0}%", pesquisa.CampoLivre);
                        filtro.Append(" AND (Cidades.sigla_estado like @pesquisa");
                        filtro.Append(" or Cidades.nome_cidade like @pesquisa");
                        filtro.Append(" or Cidades.cep_inicial like @pesquisa");
                        filtro.Append(" or Cidades.cep_final like @pesquisa");
                        filtro.Append(" ) ");
                        p = new MySqlParameter("@pesquisa", campoLivre);
                        p.MySqlDbType = (MySqlDbType)MySqlDbType.VarChar;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    if (filtro.Length > 0)
                        filtro.Replace(" AND ", " WHERE ", 0, 5);

                    #region Ordenações

                    switch (pesquisa.Ordenacao)
                    {
                        case Utils.TipoOrdenacao.porCodigo:
                            {
                                ordenacao.Append("ORDER BY Cidades.codigo_cidade");
                            }
                            break;
                        case Utils.TipoOrdenacao.porNome:
                            {
                                ordenacao.Append("ORDER BY Cidades.nome_cidade");
                            }
                            break;
                        case Utils.TipoOrdenacao.porNomeDesc:
                            {
                                ordenacao.Append("ORDER BY Cidades.nome_cidade DESC");
                            }
                            break;
                        case Utils.TipoOrdenacao.randomico:
                            {
                                ordenacao.Append("ORDER BY RAND()");
                            }
                            break;
                        default:
                            {
                                ordenacao.Append("ORDER BY Cidades.codigo_cidade");
                            }
                            break;
                    }

                    #endregion

                    sql.CommandText = String.Format("SELECT "
                        + "Cidades.* FROM Cidades "
                        + "{0} {1} {2}", filtro, ordenacao, top);
                    DataTable dt = new Data().getData(sql);
                    return MontarRetorno(dt);
                }
            }
            catch (ImunneVacinas.ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Rotina que retorna uma lista genérica do elemento, de acordo com o retorno da Consultar ao BD
        /// </summary>
        /// <param name="dt">VALUE: DataTable com o resultado da Consultar ao BD</param>
        /// <returns>Lista do elemento com os dados retornados da consulta</returns>
        internal static List<Cidade> MontarRetorno(DataTable dt)
        {
            int i = -1;
            List<Cidade> list = new List<Cidade>();

            if (dt == null && dt.Rows.Count == 0)
                return list;
            else
            {
                try
                {
                    Cidade item;
                    for (i = 0; i < dt.Rows.Count; i++)
                    {
                        item = new Cidade()
                        {

                            _codigo = Convert.ToInt32(dt.Rows[i]["codigo_cidade"]),
                            _estado = Parser.parseString(dt.Rows[i]["sigla_estado"].ToString()),
                            _nome = Parser.parseString(dt.Rows[i]["nome_cidade"].ToString()),
                            _cepInicial = Parser.parseString(dt.Rows[i]["cep_inicial"].ToString()),
                            _cepFinal = Parser.parseString(dt.Rows[i]["cep_final"].ToString()),
                            _latitude = Parser.parseString(dt.Rows[i]["cidade_latitude"].ToString()),
                            _longitude = Parser.parseString(dt.Rows[i]["cidade_longitude"].ToString())
                        };
                        list.Add(item);
                    }

                    return list;
                }
                catch (Exception ex)
                {
                    ex.Data.Add("último retorno lido", i);
                    throw;
                }
            }
        }
    }
}