﻿using System;
using System.Web.UI;

namespace ImunneVacinas
{
    /// <summary>
    /// 
    /// </summary>
    public class Alerta
    {
        public Alerta()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public static void showMensagemAviso(Page page, string tituloMensagem, string textMensagem)
        {
            if (String.IsNullOrEmpty(tituloMensagem))
                tituloMensagem = "S&A Imunizações";

            ScriptManager.RegisterStartupScript(page, page.GetType(), "Popup", "MensagemGenerica('" + tituloMensagem + "', '" + textMensagem + "', 'info');", true);
        }

        /// <summary>
        /// 
        /// </summary>
        public static void showMensagemSucesso(Page page, string tituloMensagem, string textMensagem)
        {
            if (String.IsNullOrEmpty(tituloMensagem))
                tituloMensagem = "S&A Imunizações";

            ScriptManager.RegisterStartupScript(page, page.GetType(), "Popup", "MensagemGenerica('" + tituloMensagem + "', '" + textMensagem + "', 'success');", true);
        }

        /// <summary>
        /// 
        /// </summary>
        public static void showMensagemErro(Page page, string tituloMensagem, string textMensagem)
        {
            if (String.IsNullOrEmpty(tituloMensagem))
                tituloMensagem = "Oops...";

            ScriptManager.RegisterStartupScript(page, page.GetType(), "Popup", "MensagemGenerica('" + tituloMensagem + "', '" + textMensagem + "', 'error');", true);
        }

        /// <summary>
        /// 
        /// </summary>
        public static void showMensagemAlerta(Page page, string tituloMensagem, string textMensagem)
        {
            if (String.IsNullOrEmpty(tituloMensagem))
                tituloMensagem = "Aviso!";

            ScriptManager.RegisterStartupScript(page, page.GetType(), "Popup", "MensagemGenerica('" + tituloMensagem + "', '" + textMensagem + "', 'warning');", true);
        }

        /// <summary>
        /// 
        /// </summary>
        public static void showMensagemContinuar(Page page, string tituloMensagem, string textMensagem, bool botaoCancelar)
        {
            if (String.IsNullOrEmpty(tituloMensagem))
                tituloMensagem = "Sucesso!";

            ScriptManager.RegisterStartupScript(page, page.GetType(), "Popup", "MensagemContinuar('" + tituloMensagem + "', '" + textMensagem + "', 'success');", true);
        }
    }
}