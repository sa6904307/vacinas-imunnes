﻿using MySql.Data.MySqlClient;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace ImunneVacinas
{
    /// <summary>
    /// Descrição resumida de ItensCompras
    /// </summary>
    public class ItemCompra
    {
        private static string _tabela = "ItensCompras";
        private static List<BD.Columns> columns;
        private static DateTime columnsUpdated;

        #region Propriedades do elemento

        private Compra _idCompra;
        private Vacina _idVacina;
        private int _codigo;
        private decimal _valor;
        private int _quantidade;
        private DateTime _dataCriacao;
        private string _observacoes;

        /// <summary>
        /// 
        /// </summary>
        public Compra IdCompra
        {
            get
            {
                return _idCompra;
            }

            set
            {
                _idCompra = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Vacina IdVacina
        {
            get
            {
                return _idVacina;
            }

            set
            {
                _idVacina = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public int Codigo
        {
            get
            {
                return _codigo;
            }

            set
            {
                _codigo = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public decimal Valor
        {
            get
            {
                return _valor;
            }

            set
            {
                _valor = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public int Quantidade
        {
            get
            {
                return _quantidade;
            }

            set
            {
                _quantidade = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public DateTime DataCriacao
        {
            get
            {
                return _dataCriacao;
            }

            set
            {
                _dataCriacao = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Observacoes
        {
            get
            {
                return _observacoes;
            }

            set
            {
                _observacoes = value;
            }
        }

        #endregion

        #region GetInfos

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos os campos da tabela.
        /// </summary>
        private static void GetInfoTabela()
        {
            if (columns == null || (DateTime.Now - columnsUpdated).Minutes > 0)
            {
                columns = BD.Columns.getColumnsInfo(_tabela);
                columnsUpdated = DateTime.Now;
            }
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo compra.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoCompra()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "compra");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo vacina.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoVacina()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "vacina");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo codigo.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoCodigo()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "codigo");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo valor.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoValor()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "valor");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo quantidade.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoQuantidade()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "quantidade");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo datacriacao.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoDataCriacao()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "datacriacao");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo observacoes.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoObservacoes()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "observacoes");
        }

        #endregion

        /// <summary> 
        /// Rotina que cria um Array com os parâmetros utilizados nas ações de INSERT, UPDATE e DELETE
        /// <returns>Array com vários MySqlParameters</returns>
        /// </summary>
        private Array GetParametrosSQL()
        {
            MySqlParameter p;
            ArrayList ap = new ArrayList();

            p = new MySqlParameter("@compra", Parser.parseInteiro(_idCompra.Codigo));
            p.MySqlDbType = GetInfoCompra().Type;
            ap.Add(p);

            p = new MySqlParameter("@vacina", Parser.parseInteiro(_idVacina.Codigo));
            p.MySqlDbType = GetInfoVacina().Type;
            ap.Add(p);

            p = new MySqlParameter("@codigo", Parser.parseInteiro(_codigo));
            p.MySqlDbType = GetInfoCodigo().Type;
            ap.Add(p);

            p = new MySqlParameter("@valor", Parser.parseDecimal(_valor));
            p.MySqlDbType = GetInfoValor().Type;
            ap.Add(p);

            p = new MySqlParameter("@quantidade", Parser.parseInteiro(_quantidade));
            p.MySqlDbType = GetInfoQuantidade().Type;
            ap.Add(p);

            p = new MySqlParameter("@datacriacao", Parser.parseDateTimeBD(_dataCriacao));
            p.MySqlDbType = GetInfoDataCriacao().Type;
            ap.Add(p);

            p = new MySqlParameter("@observacoes", Parser.parseString(_observacoes));
            p.MySqlDbType = GetInfoObservacoes().Type;
            ap.Add(p);

            return ap.ToArray();
        }

        /// <summary>
        /// Inicialização das propriedades do elemento com seus valores padrão
        /// </summary>
        private void IniciarElemento()
        {
            GetInfoTabela();

            _idCompra = new Compra();
            _idVacina = new Vacina();

            _idCompra.Codigo = -1;
            _idVacina.Codigo = -1;
            _valor = 0;
            _quantidade = 1;
            _observacoes = String.Empty;
            _dataCriacao = Utils.dataPadrao;
        }

        /// <summary>
        /// 
        /// </summary>
        public ItemCompra()
        {
            IniciarElemento();
        }

        /// <summary>
        /// Rotina que insere o registro no BD
        /// </summary>
        /// <returns>Código do registro inserido no BD</returns>
        public int Incluir()
        {
            if (_idCompra.Codigo <= 0 || _idVacina.Codigo <= 0 || _valor <= 0)
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    sql.Parameters.AddRange(GetParametrosSQL());
                    sql.CommandText = "INSERT INTO ItensCompras "
                                    + "(compra, vacina, valor, "
                                    + "quantidade, datacriacao, observacoes) "
                                    + "VALUE( "
                                    + "@compra, @vacina, @valor, "
                                    + "@quantidade, @datacriacao, @observacoes); "
                                    + "SELECT LAST_INSERT_ID();";
                    int resultado = Parser.parseInteiro(new Data().executeEscalar(sql));
                    return resultado;
                }
            }
            catch (ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Rotina que exclui o registro do BD
        /// </summary>
        /// <returns>Quantidade de linhas alteradas na ação junto ao BD</returns>
        public int Excluir()
        {
            if (_codigo < 0)
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    sql.Parameters.AddRange(GetParametrosSQL());
                    sql.CommandText = "DELETE FROM ItensCompras "
                                    + "WHERE codigo = @codigo";
                    int resultado = Parser.parseInteiro(new Data().execute(sql));
                    return resultado;
                }
            }
            catch (ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Rotina que localiza e retorna um único registro, mediante o código informado
        /// </summary>
        /// <param name="codigo">VALUE: código de identificação do registro (obrigatório)</param>
        /// <returns>Elemento Usuario com os dados retornados da consulta</returns>
        public static ItemCompra ConsultarUnico(int codigo)
        {
            if (codigo <= 0)
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            ImunneVacinas.PesquisaItemCompra pesquisa = new PesquisaItemCompra()
            {
                Codigo = codigo
            };

            List<ItemCompra> retorno = ConsultaGenerica(pesquisa);
            if (retorno == null || retorno.Count.Equals(0))
                return null;
            else
                return retorno[0];
        }

        /// <summary>
        /// Rotina que realiza a pesquisa de elementos mediante os parametros de pesquisa informados
        /// </summary>
        /// <param name="pesquisa">VALUE: elemento de pesquisa com os parametros para consulta</param>
        /// <returns></returns>
        public static List<ItemCompra> ConsultarItens(PesquisaItemCompra pesquisa)
        {
            return ConsultaGenerica(pesquisa);
        }

        /// <summary>
        /// Rotina interna utilizada para todas as pesquisas do elemento
        /// </summary>
        /// <param name="pesquisa">VALUE: Elemento com os parâmetros com os quais será realizada a pesquisa</param>
        /// <returns>Lista do elemento com os dados retornados da consulta</returns>
        internal static List<ItemCompra> ConsultaGenerica(PesquisaItemCompra pesquisa)
        {
            StringBuilder ordenacao = new StringBuilder();
            StringBuilder filtro = new StringBuilder();
            StringBuilder top = new StringBuilder();
            MySqlParameter p;

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    if (pesquisa.Quantidade > 0)
                        top.AppendFormat(" LIMIT {0} ", pesquisa.Quantidade);

                    #region Campo Empresa

                    if (pesquisa.Empresa > 0)
                    {
                        filtro.Append(" AND Campanhas.empresa = @empresa");
                        p = new MySqlParameter("@empresa", pesquisa.Empresa);
                        p.MySqlDbType = MySqlDbType.Int32;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Vacina

                    if (pesquisa.Vacina >= 0)
                    {
                        filtro.Append(" AND ItensCompras.vacina = @vacina");
                        p = new MySqlParameter("@vacina", pesquisa.Vacina);
                        p.MySqlDbType = GetInfoVacina().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Compra

                    if (pesquisa.Compra >= 0)
                    {
                        filtro.Append(" AND ItensCompras.compra = @compra");
                        p = new MySqlParameter("@compra", pesquisa.Compra);
                        p.MySqlDbType = GetInfoCompra().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Código

                    if (pesquisa.Codigo >= 0)
                    {
                        filtro.Append(" AND ItensCompras.codigo = @codigo");
                        p = new MySqlParameter("@codigo", pesquisa.Codigo);
                        p.MySqlDbType = GetInfoCodigo().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campos UF e Cidade

                    if (!String.IsNullOrEmpty(pesquisa.Uf) && pesquisa.Uf != "-1")
                    {
                        string siglaEstado = String.Format("{0}", pesquisa.Uf);
                        filtro.Append(" AND Clinicas.estado = @estado ");
                        p = new MySqlParameter("@estado", siglaEstado);
                        p.MySqlDbType = MySqlDbType.VarChar;
                        sql.Parameters.Add(p);

                        if (pesquisa.Cidade >= 0)
                        {
                            filtro.Append(" AND Clinicas.cidade = @cidade");
                            p = new MySqlParameter("@cidade", pesquisa.Cidade);
                            p.MySqlDbType = MySqlDbType.Int32;
                            sql.Parameters.Add(p);
                        }
                    }

                    #endregion

                    #region Campo Clinica

                    if (pesquisa.Clinica >= 0)
                    {
                        filtro.Append(" AND Compras.clinica = @clinica");
                        p = new MySqlParameter("@clinica", pesquisa.Clinica);
                        p.MySqlDbType = MySqlDbType.Int32;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Campanha

                    if (pesquisa.Campanha >= 0)
                    {
                        filtro.Append(" AND Compras.campanha = @campanha");
                        p = new MySqlParameter("@campanha", pesquisa.Campanha);
                        p.MySqlDbType = MySqlDbType.Int32;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Situação

                    if (pesquisa.Situacao != Compra.SituacaoCompra.Indiferente)
                    {
                        filtro.Append(" AND Compras.situacao = @situacao");
                        p = new MySqlParameter("@situacao", (int)pesquisa.Situacao);
                        p.MySqlDbType = MySqlDbType.Int32;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Categoria

                    if (pesquisa.Categoria != Compra.CategoriaCompra.Indiferente)
                    {
                        filtro.Append(" AND Compras.categoria = @categoria");
                        p = new MySqlParameter("@categoria", (int)pesquisa.Categoria);
                        p.MySqlDbType = MySqlDbType.Int32;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    if (filtro.Length > 0)
                        filtro = filtro.Replace(" AND ", " WHERE ", 0, 5);

                    #region Ordenação

                    switch (pesquisa.Ordenacao)
                    {
                        case Utils.TipoOrdenacao.porCodigo:
                            {
                                ordenacao.Append("ORDER BY ItensCompras.codigo");
                            }
                            break;
                        case Utils.TipoOrdenacao.porCodigoDesc:
                            {
                                ordenacao.Append("ORDER BY ItensCompras.codigo DESC");
                            }
                            break;
                        case Utils.TipoOrdenacao.porNome:
                            {
                                ordenacao.Append("ORDER BY Vacinas.nome");
                            }
                            break;
                        case Utils.TipoOrdenacao.porNomeDesc:
                            {
                                ordenacao.Append("ORDER BY Vacinas.nome DESC");
                            }
                            break;
                        case Utils.TipoOrdenacao.porData:
                            {
                                ordenacao.Append("ORDER BY Compras.datacompra");
                            }
                            break;
                        case Utils.TipoOrdenacao.porDataDesc:
                            {
                                ordenacao.Append("ORDER BY Compras.datacompra DESC");
                            }
                            break;
                        case Utils.TipoOrdenacao.randomico:
                            {
                                ordenacao.Append("ORDER BY RAND()");
                            }
                            break;
                        default:
                            {
                                ordenacao.Append("ORDER BY ItensCompras.codigo");
                            }
                            break;
                    }

                    #endregion

                    sql.CommandText = String.Format("SELECT "
                                    + "Compras.codigo AS codigoCompra, Compras.datacompra AS dataCompra, "
                                    + "Vacinas.codigo AS codigoVacina, Vacinas.nome AS nomeVacina, "
                                    + "ItensCompras.* "
                                    + "FROM ItensCompras "
                                    + "LEFT JOIN Vacinas ON Vacinas.codigo = ItensCompras.vacina "
                                    + "LEFT JOIN Compras ON Compras.codigo = ItensCompras.compra "
                                    + "LEFT JOIN Campanhas ON Campanhas.codigo = Compras.campanha "
                                    + "LEFT JOIN Clinicas ON Clinicas.codigo = Compras.clinica "
                                    + "{0} {1} {2}", filtro, ordenacao, top);
                    DataTable dt = new Data().getData(sql);
                    return MontarRetorno(dt);
                }
            }
            catch (ImunneVacinas.ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Rotina que retorna uma lista genérica do elemento, de acordo com o retorno da Consultar ao BD
        /// </summary>
        /// <param name="dt">VALUE: DataTable com o resultado da Consultar ao BD</param>
        /// <returns>Lista do elemento com os dados retornados da consulta</returns>
        internal static List<ItemCompra> MontarRetorno(DataTable dt)
        {
            int i = -1;
            List<ItemCompra> list = new List<ItemCompra>();

            if (dt == null && dt.Rows.Count == 0)
                return list;
            else
            {
                ItemCompra retorno;
                try
                {
                    for (i = 0; i < dt.Rows.Count; i++)
                    {
                        retorno = new ItemCompra()
                        {
                            _codigo = Parser.parseInteiro(dt.Rows[i]["codigo"]),
                            _observacoes = Parser.parseString(dt.Rows[i]["observacoes"]),
                            _quantidade = Parser.parseInteiro(dt.Rows[i]["quantidade"]),
                            _valor = Parser.parseDecimal(dt.Rows[i]["valor"]),
                            _dataCriacao = Convert.ToDateTime(dt.Rows[i]["datacriacao"])
                        };

                        retorno._idCompra.Codigo = Parser.parseInteiro(dt.Rows[i]["codigoCompra"]);
                        retorno._idCompra.DataCompra = Parser.parseDateTime(dt.Rows[i]["dataCompra"]);

                        retorno._idVacina.Codigo = Convert.ToInt32(dt.Rows[i]["codigoVacina"]);
                        retorno._idVacina.Nome = Parser.parseString(dt.Rows[i]["nomeVacina"]);

                        list.Add(retorno);
                    }
                    return list;
                }
                catch (Exception ex)
                {
                    ex.Data.Add("último retorno lido", i);
                    throw;
                }
            }
        }

        #region EXPORTAÇÃO EXCEL

        /// <summary>
        /// Rotina interna utilizada para todas as pesquisas do elemento
        /// </summary>
        /// <param name="pesquisa">VALUE: Elemento com os parâmetros com os quais será realizada a pesquisa</param>
        /// <returns>Lista do elemento com os dados retornados da consulta</returns>
        public static List<ListagemItemCompra> ConsultaItensComprasListagem(PesquisaCompra pesquisa)
        {
            StringBuilder ordenacao = new StringBuilder();
            StringBuilder filtro = new StringBuilder();
            StringBuilder top = new StringBuilder();
            MySqlParameter p;

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    if (pesquisa.Quantidade > 0)
                        top.AppendFormat(" LIMIT {0} ", pesquisa.Quantidade);

                    #region Campo Código

                    if (pesquisa.Codigo >= 0)
                    {
                        filtro.Append(" AND Compras.codigo = @codigo");
                        p = new MySqlParameter("@codigo", pesquisa.Codigo);
                        p.MySqlDbType = GetInfoCodigo().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Liberados

                    if (pesquisa.Liberados > 0)
                    {
                        switch (pesquisa.Liberados)
                        {
                            // COMPRAS AINDA À LIBERAR
                            case 1:
                                {
                                    filtro.Append(" AND Compras.dataliberacao = @LIBERADOS");
                                    p = new MySqlParameter("@LIBERADOS", Parser.parseDateTimeBD(ImunneVacinas.Utils.dataPadrao));
                                    p.MySqlDbType = MySqlDbType.Int32;
                                    sql.Parameters.Add(p);
                                }
                                break;
                            // DEMAIS COMPRAS
                            default:
                                {
                                    filtro.Append(" AND Compras.dataliberacao > @LIBERADOS");
                                    p = new MySqlParameter("@LIBERADOS", Parser.parseDateTimeBD(ImunneVacinas.Utils.dataPadrao));
                                    p.MySqlDbType = MySqlDbType.Int32;
                                    sql.Parameters.Add(p);
                                }
                                break;
                        }
                    }

                    #endregion

                    #region Campo Participante

                    if (pesquisa.Participante >= 0)
                    {
                        filtro.Append(" AND Compras.participante = @participante");
                        p = new MySqlParameter("@participante", pesquisa.Participante);
                        p.MySqlDbType = MySqlDbType.Int32;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Empresa

                    if (pesquisa.Empresa > 0)
                    {
                        filtro.Append(" AND Campanhas.empresa = @empresa");
                        p = new MySqlParameter("@empresa", pesquisa.Empresa);
                        p.MySqlDbType = MySqlDbType.Int32;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Clinica

                    if (pesquisa.Clinica >= 0)
                    {
                        filtro.Append(" AND Compras.clinica = @clinica");
                        p = new MySqlParameter("@clinica", pesquisa.Clinica);
                        p.MySqlDbType = MySqlDbType.Int32;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Campanha

                    if (pesquisa.Campanha >= 0)
                    {
                        filtro.Append(" AND Compras.campanha = @campanha");
                        p = new MySqlParameter("@campanha", pesquisa.Campanha);
                        p.MySqlDbType = MySqlDbType.Int32;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Situação

                    if (pesquisa.Situacao != Compra.SituacaoCompra.Indiferente)
                    {
                        filtro.Append(" AND Compras.situacao = @situacao");
                        p = new MySqlParameter("@situacao", (int)pesquisa.Situacao);
                        p.MySqlDbType = MySqlDbType.Int32;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Aplicação

                    if (pesquisa.Aplicacao != Compra.Aplicacao.Indiferente)
                    {
                        filtro.Append(" AND Compras.aplicacao = @aplicacao");
                        p = new MySqlParameter("@aplicacao", (int)pesquisa.Aplicacao);
                        p.MySqlDbType = MySqlDbType.Int32;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Categoria

                    if (pesquisa.Categoria != Compra.CategoriaCompra.Indiferente)
                    {
                        filtro.Append(" AND Compras.categoria = @categoria");
                        p = new MySqlParameter("@categoria", (int)pesquisa.Categoria);
                        p.MySqlDbType = MySqlDbType.Int32;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campos UF e Cidade

                    if (!String.IsNullOrEmpty(pesquisa.Uf) && pesquisa.Uf != "-1")
                    {
                        string siglaEstado = String.Format("{0}", pesquisa.Uf);
                        filtro.Append(" AND Clinicas.estado = @estado ");
                        p = new MySqlParameter("@estado", siglaEstado);
                        p.MySqlDbType = MySqlDbType.VarChar;
                        sql.Parameters.Add(p);

                        if (pesquisa.Cidade >= 0)
                        {
                            filtro.Append(" AND Clinicas.cidade = @cidade");
                            p = new MySqlParameter("@cidade", pesquisa.Cidade);
                            p.MySqlDbType = MySqlDbType.Int32;
                            sql.Parameters.Add(p);
                        }
                    }

                    #endregion

                    #region Campos Data Compra

                    if (pesquisa.DataInicio != Utils.dataPadrao && pesquisa.DataFim != Utils.dataPadrao)
                    {
                        filtro.Append(" AND Compras.datacompra >= @inicio");
                        p = new MySqlParameter("@inicio", Parser.parseDateTimeBD(pesquisa.DataInicio))
                        {
                            MySqlDbType = MySqlDbType.DateTime
                        };
                        sql.Parameters.Add(p);

                        filtro.Append(" AND Compras.datacompra <= @termino");
                        p = new MySqlParameter("@termino", Parser.parseDateTimeBD(pesquisa.DataFim))
                        {
                            MySqlDbType = MySqlDbType.DateTime
                        };
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Livre

                    if (!String.IsNullOrEmpty(pesquisa.CampoLivre.Trim()))
                    {
                        string campoLivre = String.Format("%{0}%", pesquisa.CampoLivre.Trim());
                        filtro.Append(" AND (Participantes.nome LIKE @campoLivre ");
                        filtro.Append(" OR TransacoesRealizadas.numeroComprovanteVenda LIKE @campoLivre");
                        filtro.Append(" OR TransacoesRealizadas.numeroCartao LIKE @campoLivre");
                        filtro.Append(" OR Participantes.cpf LIKE @campoLivre");
                        filtro.Append(")");
                        p = new MySqlParameter("@campoLivre", campoLivre)
                        {
                            MySqlDbType = MySqlDbType.VarChar
                        };
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Valor

                    if (pesquisa.Valor == true)
                        filtro.Append(" AND Compras.valor > 0");

                    #endregion

                    if (filtro.Length > 0)
                        filtro = filtro.Replace(" AND ", " WHERE ", 0, 5);

                    #region Ordenação

                    switch (pesquisa.Ordenacao)
                    {
                        case Utils.TipoOrdenacao.porCodigo:
                            {
                                ordenacao.Append("ORDER BY Compras.codigo");
                            }
                            break;
                        case Utils.TipoOrdenacao.porCodigoDesc:
                            {
                                ordenacao.Append("ORDER BY Compras.codigo DESC");
                            }
                            break;
                        case Utils.TipoOrdenacao.porData:
                            {
                                ordenacao.Append("ORDER BY Compras.datacompra, Campanhas.identificacao, Clinicas.nomefantasia");
                            }
                            break;
                        case Utils.TipoOrdenacao.porDataDesc:
                            {
                                ordenacao.Append("ORDER BY Compras.datacompra DESC, Campanhas.identificacao, Clinicas.nomefantasia");
                            }
                            break;
                        case Utils.TipoOrdenacao.randomico:
                            {
                                ordenacao.Append("ORDER BY RAND()");
                            }
                            break;
                        default:
                            {
                                ordenacao.Append("ORDER BY Compras.codigo");
                            }
                            break;
                    }

                    #endregion

                    sql.CommandText = String.Format("SELECT "
                        + "Campanhas.identificacao AS Campanha, Empresas.razaosocial AS Empresa, Compras.codigo AS Pedido, "
                        + "TransacoesRealizadas.numeroComprovanteVenda AS Comprovante, Participantes.nome AS Titular, "
                        + "Participantes.email AS Email, Participantes.telefone AS Telefone, t.nome AS Beneficiario, "
                        + "Clinicas.nomefantasia AS Clinica, Cidades.nome_cidade AS Cidade, Clinicas.estado AS UF, "
                        + "ItensCompras.quantidade AS Quantidade, Vacinas.nome AS Produto, ProdutosCampanhas.preco AS Preco, "
                        //+ "Compras.datacompra AS DataCompra, Compras.dataliberacao AS DataLiberacao, Compras.dataaplicacao AS DataAplicacao "
                        + "Compras.datacompra AS DataCompra, Campanhas.dataabertura AS DataLiberacao, Compras.dataaplicacao AS DataAplicacao "
                        + "FROM ItensCompras "
                        + "INNER JOIN Vacinas ON Vacinas.codigo = ItensCompras.vacina "
                        + "INNER JOIN Compras ON Compras.codigo = ItensCompras.compra "
                        + "INNER JOIN Clinicas ON Clinicas.codigo = Compras.clinica "
                        + "INNER JOIN Cidades ON Cidades.codigo_cidade = Clinicas.cidade "
                        + "INNER JOIN TransacoesRealizadas ON TransacoesRealizadas.compraTransacao = Compras.codigo "
                        + "INNER JOIN Vouchers ON Vouchers.compra = Compras.codigo "
                        + "INNER JOIN Participantes ON Participantes.codigo = Compras.participante "
                        + "INNER JOIN Participantes AS t ON t.codigo = Vouchers.participante "
                        + "INNER JOIN Campanhas ON Campanhas.codigo = Compras.campanha "
                        + "INNER JOIN ProdutosCampanhas ON ProdutosCampanhas.campanha = Campanhas.codigo "
                        + "AND ProdutosCampanhas.vacina = Vacinas.codigo "
                        + "INNER JOIN Empresas ON Empresas.codigo = Campanhas.empresa"
                        + "{0} {1} {2}", filtro, ordenacao, top);
                    DataTable dt = new Data().getData(sql);
                    return MontarRetornoListagem(dt);
                }
            }
            catch (ImunneVacinas.ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        internal static List<ListagemItemCompra> MontarRetornoListagem(DataTable dt)
        {
            int i = -1;
            List<ListagemItemCompra> list = new List<ListagemItemCompra>();

            if (dt == null && dt.Rows.Count == 0)
                return list;
            else
            {
                ListagemItemCompra retorno;
                try
                {
                    for (i = 0; i < dt.Rows.Count; i++)
                    {
                        retorno = new ListagemItemCompra();
                        retorno.Beneficiario = ImunneVacinas.Parser.parseString(dt.Rows[i]["beneficiario"]);
                        retorno.Campanha = ImunneVacinas.Parser.parseString(dt.Rows[i]["campanha"]);
                        retorno.Cidade = ImunneVacinas.Parser.parseString(dt.Rows[i]["cidade"]);
                        retorno.Clinica = ImunneVacinas.Parser.parseString(dt.Rows[i]["clinica"]);
                        retorno.Comprovante = ImunneVacinas.Parser.parseString(dt.Rows[i]["comprovante"]);
                        retorno.Email = ImunneVacinas.Parser.parseString(dt.Rows[i]["email"]);
                        retorno.Empresa = ImunneVacinas.Parser.parseString(dt.Rows[i]["empresa"]);
                        retorno.Pedido = ImunneVacinas.Parser.parseInteiro(dt.Rows[i]["pedido"]);
                        retorno.Preco = ImunneVacinas.Parser.parseDecimal(dt.Rows[i]["preco"]);
                        retorno.Produto = ImunneVacinas.Parser.parseString(dt.Rows[i]["produto"]);
                        retorno.Quantidade = ImunneVacinas.Parser.parseInteiro(dt.Rows[i]["quantidade"]);
                        retorno.Telefone = ImunneVacinas.Parser.parseString(dt.Rows[i]["telefone"]);
                        retorno.Titular = ImunneVacinas.Parser.parseString(dt.Rows[i]["titular"]);
                        retorno.UF = ImunneVacinas.Parser.parseString(dt.Rows[i]["uf"]);

                        // TRATAMENTO DAS DATAS
                        if (!String.IsNullOrEmpty(dt.Rows[i]["datacompra"].ToString()) && Convert.ToDateTime(dt.Rows[i]["datacompra"].ToString()) != ImunneVacinas.Utils.dataPadrao)
                            retorno.DataCompra = Convert.ToDateTime(dt.Rows[i]["datacompra"]).ToString("dd/MM/yyyy HH:mm");
                        else retorno.DataCompra = "--";

                        if (!String.IsNullOrEmpty(dt.Rows[i]["dataliberacao"].ToString()) && Convert.ToDateTime(dt.Rows[i]["dataliberacao"].ToString()) != ImunneVacinas.Utils.dataPadrao)
                            //retorno.DataLiberacao = Convert.ToDateTime(dt.Rows[i]["dataliberacao"]).ToString("dd/MM/yyyy HH:mm");
                            retorno.DataLiberacao = Convert.ToDateTime(dt.Rows[i]["dataliberacao"]).ToString("dd/MM/yyyy");
                        else retorno.DataLiberacao = "--";

                        if (!String.IsNullOrEmpty(dt.Rows[i]["dataaplicacao"].ToString()) && Convert.ToDateTime(dt.Rows[i]["dataaplicacao"].ToString()) != ImunneVacinas.Utils.dataPadrao)
                            retorno.DataAplicacao = Convert.ToDateTime(dt.Rows[i]["dataaplicacao"]).ToString("dd/MM/yyyy HH:mm");
                        else retorno.DataAplicacao = "NÃO APLICADO";

                        list.Add(retorno);
                    }
                    return list;
                }
                catch (Exception ex)
                {
                    ex.Data.Add("último retorno lido", i);
                    throw;
                }
            }
        }

        #endregion
    }
}