﻿using MySql.Data.MySqlClient;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace ImunneVacinas
{
    /// <summary>
    /// Descrição resumida de GrausParentescos
    /// </summary>
    [Serializable]
    public class GrauParentesco
    {
        private static string _tabela = "GrausParentescos";
        private static List<BD.Columns> columns;
        private static DateTime columnsUpdated;

        #region Propriedades do elemento

        private int _codigo;
        private string _nome;
        private string _observacoes;
        private DateTime _dataCriacao;
        private DateTime _dataAlteracao;

        /// <summary>
        /// Código de identificação do registro
        /// </summary>
        public int Codigo
        {
            get
            {
                return _codigo;
            }

            set
            {
                _codigo = value;
            }
        }

        /// <summary>
        /// Nome da vacina
        /// </summary>
        public string Nome
        {
            get
            {
                return _nome;
            }

            set
            {
                _nome = value;
            }
        }

        /// <summary>
        /// Data de criação do registro
        /// </summary>
        public DateTime DataCriacao
        {
            get
            {
                return _dataCriacao;
            }

            set
            {
                _dataCriacao = value;
            }
        }

        /// <summary>
        /// Data da última alteração do registro
        /// </summary>
        public DateTime DataAlteracao
        {
            get
            {
                return _dataAlteracao;
            }

            set
            {
                _dataAlteracao = value;
            }
        }

        #endregion

        #region GetInfos

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos os campos da tabela.
        /// </summary>
        private static void GetInfoTabela()
        {
            if (columns == null || (DateTime.Now - columnsUpdated).Minutes > 0)
            {
                columns = BD.Columns.getColumnsInfo(_tabela);
                columnsUpdated = DateTime.Now;
            }
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo codigo.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoCodigo()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "codigo");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo nome.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoNome()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "nome");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo datacriacao.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoDataCriacao()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "datacriacao");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo dataalteracao.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoDataAlteracao()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "dataalteracao");
        }

        #endregion

        /// <summary> 
        /// Rotina que cria um Array com os parâmetros utilizados nas ações de INSERT, UPDATE e DELETE
        /// <returns>Array com vários MySqlParameters</returns>
        /// </summary>
        private Array GetParametrosSQL()
        {
            MySqlParameter p;
            ArrayList ap = new ArrayList();

            p = new MySqlParameter("@codigo", Parser.parseInteiro(_codigo));
            p.MySqlDbType = GetInfoCodigo().Type;
            ap.Add(p);

            p = new MySqlParameter("@nome", Parser.parseString(_nome));
            p.MySqlDbType = GetInfoNome().Type;
            ap.Add(p);

            p = new MySqlParameter("@datacriacao", Parser.parseDateTimeBD(_dataCriacao));
            p.MySqlDbType = GetInfoDataCriacao().Type;
            ap.Add(p);

            p = new MySqlParameter("@dataalteracao", Parser.parseDateTimeBD(_dataAlteracao));
            p.MySqlDbType = GetInfoDataAlteracao().Type;
            ap.Add(p);

            return ap.ToArray();
        }

        /// <summary>
        /// Inicialização das propriedades do elemento com seus valores padrão
        /// </summary>
        private void IniciarElemento()
        {
            GetInfoTabela();

            _nome = String.Empty;
            _dataAlteracao = Utils.dataPadrao;
            _dataCriacao = Utils.dataPadrao;
        }

        /// <summary>
        /// Inicialização do elemento
        /// </summary>
        public GrauParentesco()
        {
            IniciarElemento();
        }

        /// <summary>
        /// Rotina que insere o registro no BD
        /// </summary>
        /// <returns>Código do registro inserido no BD</returns>
        public int Incluir()
        {
            if (String.IsNullOrEmpty(_nome))
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    sql.Parameters.AddRange(GetParametrosSQL());
                    sql.CommandText = "INSERT INTO GrausParentescos "
                                    + "(nome, datacriacao, dataalteracao)  "
                                    + "VALUE (@nome, @datacriacao, @dataalteracao); "
                                    + "SELECT LAST_INSERT_ID();";
                    int resultado = Parser.parseInteiro(new Data().executeEscalar(sql));
                    return resultado;
                }
            }
            catch (ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Rotina que altera os dados do registro no BD
        /// </summary>
        /// <returns>Quantidade de linhas alteradas na ação junto ao BD</returns>
        public int Alterar()
        {
            if (_codigo < 0)
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    sql.Parameters.AddRange(GetParametrosSQL());
                    sql.CommandText = "UPDATE GrausParentescos "
                                    + "SET "
                                    + "nome = @nome, dataalteracao = @dataalteracao "
                                    + "WHERE codigo = @codigo";
                    int resultado = Parser.parseInteiro(new Data().execute(sql));
                    return resultado;
                }
            }
            catch (ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Rotina que exclui o registro do BD
        /// </summary>
        /// <returns>Quantidade de linhas alteradas na ação junto ao BD</returns>
        public int Excluir()
        {
            if (_codigo < 0)
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    sql.Parameters.AddRange(GetParametrosSQL());
                    sql.CommandText = "DELETE FROM GrausParentescos "
                                    + "WHERE codigo = @codigo";
                    int resultado = Parser.parseInteiro(new Data().execute(sql));
                    return resultado;
                }
            }
            catch (ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Rotina que localiza e retorna um único registro, mediante o código informado
        /// </summary>
        /// <param name="codigo">VALUE: código de identificação do registro (obrigatório)</param>
        /// <returns>Elemento Usuario com os dados retornados da consulta</returns>
        public static GrauParentesco ConsultarUnico(int codigo)
        {
            if (codigo <= 0)
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            ImunneVacinas.PesquisaGrauParentesco pesquisa = new PesquisaGrauParentesco()
            {
                Codigo = codigo
            };

            List<GrauParentesco> retorno = ConsultaGenerica(pesquisa);
            if (retorno == null || retorno.Count.Equals(0))
                return null;
            else
                return retorno[0];
        }

        /// <summary>
        /// Rotina que localiza e retorna um único registro, mediante o código informado
        /// </summary>
        /// <param name="nome">VALUE: campo textual para pesquisa por nome (obrigatório)</param>
        /// <returns>Elemento Usuario com os dados retornados da consulta</returns>
        public static GrauParentesco ConsultarUnico(string nome)
        {
            if (String.IsNullOrEmpty(nome))
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            ImunneVacinas.PesquisaGrauParentesco pesquisa = new PesquisaGrauParentesco()
            {
                Nome = nome
            };

            List<ImunneVacinas.GrauParentesco> retorno = ConsultaGenerica(pesquisa);
            if (retorno == null || retorno.Count.Equals(0))
                return null;
            else
                return retorno[0];
        }

        /// <summary>
        /// Rotina que retorna um elemento RetornoCBX para utilização em DropLists
        /// </summary>
        /// <param name="InserirLinhaPadrao">VALUE: Informa se será adiconada a linha padrão</param>
        /// <param name="textoPadrao">VALUE: Informa o texto utilizado para linha padão (se utilizado NULL, será apresentado SELECIONE)</param>
        /// <returns>Lista do elemento RetornoCBX com os dados desejados</returns>
        public static List<RetornoComboBox> ConsultarComboBox(bool InserirLinhaPadrao, string textoPadrao)
        {
            DataTable dt = new DataTable();
            try
            {
                dt.Columns.Add("DisplayMember");
                dt.Columns.Add("ValueMember");

                ImunneVacinas.PesquisaGrauParentesco pesquisa = new PesquisaGrauParentesco()
                {
                    Ordenacao = Utils.TipoOrdenacao.porNome
                };

                List<GrauParentesco> retorno = ConsultaGenerica(pesquisa);
                if (retorno != null)
                {
                    foreach (GrauParentesco c in retorno)
                    {
                        dt.Rows.Add(new object[] { c.Nome.ToUpper(), c.Codigo });
                    }
                }
                return Utils.MontarRetornoComboBox(dt, InserirLinhaPadrao, textoPadrao);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// Rotina que realiza a pesquisa de elementos mediante os parametros de pesquisa informados
        /// </summary>
        /// <param name="pesquisa">VALUE: elemento de pesquisa com os parametros para consulta</param>
        /// <returns></returns>
        public static List<GrauParentesco> ConsultarVacinas(PesquisaGrauParentesco pesquisa)
        {
            return ConsultaGenerica(pesquisa);
        }

        /// <summary>
        /// Rotina interna utilizada para todas as pesquisas do elemento
        /// </summary>
        /// <param name="pesquisa">VALUE: Elemento com os parâmetros com os quais será realizada a pesquisa</param>
        /// <returns>Lista do elemento com os dados retornados da consulta</returns>
        internal static List<GrauParentesco> ConsultaGenerica(PesquisaGrauParentesco pesquisa)
        {
            StringBuilder ordenacao = new StringBuilder();
            StringBuilder filtro = new StringBuilder();
            StringBuilder top = new StringBuilder();
            MySqlParameter p;

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    if (pesquisa.Quantidade > 0)
                        top.AppendFormat(" LIMIT {0} ", pesquisa.Quantidade);

                    #region Campo Código

                    if (pesquisa.Codigo >= 0)
                    {
                        filtro.Append(" AND GrausParentescos.codigo = @codigo");
                        p = new MySqlParameter("@codigo", pesquisa.Codigo);
                        p.MySqlDbType = GetInfoCodigo().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Nome

                    if (!String.IsNullOrEmpty(pesquisa.Nome))
                    {
                        string nome = String.Format("{0}", pesquisa.Nome);
                        filtro.Append(" AND GrausParentescos.nome = @nome ");
                        p = new MySqlParameter("@nome", nome);
                        p.MySqlDbType = MySqlDbType.VarChar;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Livre

                    if (!String.IsNullOrEmpty(pesquisa.CampoLivre.Trim()))
                    {
                        string campoLivre = String.Format("%{0}%", pesquisa.CampoLivre.Trim());
                        filtro.Append(" AND (GrausParentescos.nome LIKE @campoLivre ");
                        filtro.Append(")");
                        p = new MySqlParameter("@campoLivre", campoLivre);
                        p.MySqlDbType = MySqlDbType.VarChar;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    if (filtro.Length > 0)
                        filtro = filtro.Replace(" AND ", " WHERE ", 0, 5);

                    #region Ordenação

                    switch (pesquisa.Ordenacao)
                    {
                        case Utils.TipoOrdenacao.porCodigo:
                            {
                                ordenacao.Append("ORDER BY GrausParentescos.codigo");
                            }
                            break;
                        case Utils.TipoOrdenacao.porCodigoDesc:
                            {
                                ordenacao.Append("ORDER BY GrausParentescos.codigo DESC");
                            }
                            break;
                        case Utils.TipoOrdenacao.porNome:
                            {
                                ordenacao.Append("ORDER BY GrausParentescos.nome");
                            }
                            break;
                        case Utils.TipoOrdenacao.porNomeDesc:
                            {
                                ordenacao.Append("ORDER BY GrausParentescos.nome DESC");
                            }
                            break;
                        case Utils.TipoOrdenacao.porData:
                            {
                                ordenacao.Append("ORDER BY GrausParentescos.datacriacao");
                            }
                            break;
                        case Utils.TipoOrdenacao.porDataDesc:
                            {
                                ordenacao.Append("ORDER BY Vacinas.datacriacao DESC");
                            }
                            break;
                        case Utils.TipoOrdenacao.randomico:
                            {
                                ordenacao.Append("ORDER BY RAND()");
                            }
                            break;
                        default:
                            {
                                ordenacao.Append("ORDER BY GrausParentescos.codigo");
                            }
                            break;
                    }

                    #endregion

                    sql.CommandText = String.Format("SELECT "
                        + "GrausParentescos.* "
                        + "FROM GrausParentescos "
                        + "{0} {1} {2}", filtro, ordenacao, top);
                    DataTable dt = new Data().getData(sql);
                    return MontarRetorno(dt);
                }
            }
            catch (ImunneVacinas.ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Rotina que retorna uma lista genérica do elemento, de acordo com o retorno da Consultar ao BD
        /// </summary>
        /// <param name="dt">VALUE: DataTable com o resultado da Consultar ao BD</param>
        /// <returns>Lista do elemento com os dados retornados da consulta</returns>
        internal static List<GrauParentesco> MontarRetorno(DataTable dt)
        {
            int i = -1;
            List<GrauParentesco> list = new List<GrauParentesco>();

            if (dt == null && dt.Rows.Count == 0)
                return list;
            else
            {
                GrauParentesco retorno;
                try
                {
                    for (i = 0; i < dt.Rows.Count; i++)
                    {
                        retorno = new GrauParentesco()
                        {
                            _codigo = Parser.parseInteiro(dt.Rows[i]["codigo"]),
                            _nome = Parser.parseString(dt.Rows[i]["nome"]),
                            _dataCriacao = Convert.ToDateTime(dt.Rows[i]["datacriacao"])
                        };

                        if (!String.IsNullOrEmpty(dt.Rows[i]["dataalteracao"].ToString()))
                            retorno._dataAlteracao = Convert.ToDateTime(dt.Rows[i]["dataalteracao"]);

                        list.Add(retorno);
                    }
                    return list;
                }
                catch (Exception ex)
                {
                    ex.Data.Add("último retorno lido", i);
                    throw;
                }
            }
        }
    }
}