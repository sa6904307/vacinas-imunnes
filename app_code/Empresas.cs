﻿using MySql.Data.MySqlClient;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace ImunneVacinas
{
    /// <summary>
    /// Descrição resumida de Empresa
    /// </summary>
    public class Empresa
    {
        private static string _tabela = "Empresas";
        private static List<BD.Columns> columns;
        private static DateTime columnsUpdated;

        #region Propriedades do elemento

        private int _codigo;
        private string _cnpj;
        private string _razaoSocial;
        private string _nomeFantasia;
        private string _cep;
        private string _complemento;
        private string _endereco;
        private string _numero;
        private string _bairro;
        private string _contato;
        private string _telefone;
        private string _email;
        private string _longitude;
        private string _latitude;
        private Uf _idUf;
        private Cidade _idCidade;
        private DateTime _dataCriacao;
        private DateTime _dataAlteracao;
        private string _logotipo;

        public int Codigo
        {
            get
            {
                return _codigo;
            }

            set
            {
                _codigo = value;
            }
        }

        public string Cnpj
        {
            get
            {
                return _cnpj;
            }

            set
            {
                _cnpj = value;
            }
        }

        public string RazaoSocial
        {
            get
            {
                return _razaoSocial;
            }

            set
            {
                _razaoSocial = value;
            }
        }

        public string NomeFantasia
        {
            get
            {
                return _nomeFantasia;
            }

            set
            {
                _nomeFantasia = value;
            }
        }

        public string Cep
        {
            get
            {
                return _cep;
            }

            set
            {
                _cep = value;
            }
        }

        public string Complemento
        {
            get
            {
                return _complemento;
            }

            set
            {
                _complemento = value;
            }
        }

        public string Endereco
        {
            get
            {
                return _endereco;
            }

            set
            {
                _endereco = value;
            }
        }

        public string Numero
        {
            get
            {
                return _numero;
            }

            set
            {
                _numero = value;
            }
        }

        public string Bairro
        {
            get
            {
                return _bairro;
            }

            set
            {
                _bairro = value;
            }
        }

        public string Contato
        {
            get
            {
                return _contato;
            }

            set
            {
                _contato = value;
            }
        }

        public string Telefone
        {
            get
            {
                return _telefone;
            }

            set
            {
                _telefone = value;
            }
        }

        public string Email
        {
            get
            {
                return _email;
            }

            set
            {
                _email = value;
            }
        }

        public Uf IdUf
        {
            get
            {
                return _idUf;
            }

            set
            {
                _idUf = value;
            }
        }

        public Cidade IdCidade
        {
            get
            {
                return _idCidade;
            }

            set
            {
                _idCidade = value;
            }
        }

        public string Longitude
        {
            get
            {
                return _longitude;
            }

            set
            {
                _longitude = value;
            }
        }

        public string Latitude
        {
            get
            {
                return _latitude;
            }

            set
            {
                _latitude = value;
            }
        }

        public DateTime DataCriacao
        {
            get
            {
                return _dataCriacao;
            }

            set
            {
                _dataCriacao = value;
            }
        }

        public DateTime DataAlteracao
        {
            get
            {
                return _dataAlteracao;
            }

            set
            {
                _dataAlteracao = value;
            }
        }

        public string Logotipo
        {
            get
            {
                return _logotipo;
            }

            set
            {
                _logotipo = value;
            }
        }

        #endregion

        #region GetInfos

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos os campos da tabela.
        /// </summary>
        private static void GetInfoTabela()
        {
            if (columns == null || (DateTime.Now - columnsUpdated).Minutes > 0)
            {
                columns = BD.Columns.getColumnsInfo(_tabela);
                columnsUpdated = DateTime.Now;
            }
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo codigo.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoCodigo()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "codigo");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo cpfcnpj.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoCnpj()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "cnpj");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo razaosocial.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoRazaoSocial()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "razaosocial");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo nomefantasia.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoNomeFantasia()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "nomefantasia");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo cep.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoCep()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "cep");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo endereco.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoEndereco()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "endereco");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo numero.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoNumero()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "numero");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo complemento.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoComplemento()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "complemento");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo bairro.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoBairro()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "bairro");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo estado.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoEstado()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "estado");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo cidade.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoCidade()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "cidade");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo contato.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoContato()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "contato");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo telefone.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoTelefone()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "telefone");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo email.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoEmail()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "email");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo datacriacao.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoDataCriacao()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "datacriacao");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo dataalteracao.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoDataAlteracao()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "dataalteracao");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo longitude.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoLongitude()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "longitude");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo latitude.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoLatitude()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "latitude");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo logotipo.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoLogotipo()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "logotipo");
        }

        #endregion

        /// <summary> 
        /// Rotina que cria um Array com os parâmetros utilizados nas ações de INSERT, UPDATE e DELETE
        /// <returns>Array com vários MySqlParameters</returns>
        /// </summary>
        private Array GetParametrosSQL()
        {
            MySqlParameter p;
            ArrayList ap = new ArrayList();

            p = new MySqlParameter("@codigo", Parser.parseInteiro(_codigo));
            p.MySqlDbType = GetInfoCodigo().Type;
            ap.Add(p);

            p = new MySqlParameter("@cidade", Parser.parseInteiro(_idCidade.Codigo));
            p.MySqlDbType = GetInfoCidade().Type;
            ap.Add(p);

            p = new MySqlParameter("@nomefantasia", Parser.parseString(_nomeFantasia));
            p.MySqlDbType = GetInfoNomeFantasia().Type;
            ap.Add(p);

            p = new MySqlParameter("@razaosocial", Parser.parseString(_razaoSocial));
            p.MySqlDbType = GetInfoRazaoSocial().Type;
            ap.Add(p);

            p = new MySqlParameter("@cnpj", Parser.parseString(_cnpj));
            p.MySqlDbType = GetInfoCnpj().Type;
            ap.Add(p);

            p = new MySqlParameter("@cep", Parser.parseString(_cep));
            p.MySqlDbType = GetInfoCep().Type;
            ap.Add(p);

            p = new MySqlParameter("@endereco", Parser.parseString(_endereco));
            p.MySqlDbType = GetInfoEndereco().Type;
            ap.Add(p);

            p = new MySqlParameter("@numero", Parser.parseString(_numero));
            p.MySqlDbType = GetInfoNumero().Type;
            ap.Add(p);

            p = new MySqlParameter("@complemento", Parser.parseString(_complemento));
            p.MySqlDbType = GetInfoComplemento().Type;
            ap.Add(p);

            p = new MySqlParameter("@bairro", Parser.parseString(_bairro));
            p.MySqlDbType = GetInfoBairro().Type;
            ap.Add(p);

            p = new MySqlParameter("@estado", Parser.parseString(_idUf.Sigla));
            p.MySqlDbType = GetInfoEstado().Type;
            ap.Add(p);

            p = new MySqlParameter("@telefone", Parser.parseString(_telefone));
            p.MySqlDbType = GetInfoTelefone().Type;
            ap.Add(p);

            p = new MySqlParameter("@email", Parser.parseString(_email));
            p.MySqlDbType = GetInfoEmail().Type;
            ap.Add(p);

            p = new MySqlParameter("@contato", Parser.parseString(_contato));
            p.MySqlDbType = GetInfoContato().Type;
            ap.Add(p);

            p = new MySqlParameter("@logotipo", Parser.parseString(_logotipo));
            p.MySqlDbType = GetInfoLogotipo().Type;
            ap.Add(p);

            p = new MySqlParameter("@datacriacao", Parser.parseDateTimeBD(_dataCriacao));
            p.MySqlDbType = GetInfoDataCriacao().Type;
            ap.Add(p);

            p = new MySqlParameter("@dataalteracao", Parser.parseDateTimeBD(_dataAlteracao));
            p.MySqlDbType = GetInfoDataAlteracao().Type;
            ap.Add(p);

            p = new MySqlParameter("@latitude", Parser.parseString(_latitude));
            p.MySqlDbType = GetInfoLatitude().Type;
            ap.Add(p);

            p = new MySqlParameter("@longitude", Parser.parseString(_longitude));
            p.MySqlDbType = GetInfoLongitude().Type;
            ap.Add(p);

            return ap.ToArray();
        }

        /// <summary>
        /// Inicialização das propriedades do elemento com seus valores padrão
        /// </summary>
        private void IniciarElemento()
        {
            GetInfoTabela();
            _idCidade = new Cidade();
            _idUf = new Uf();

            _idCidade.Codigo = -1;
            _idUf.Sigla = String.Empty;

            _razaoSocial = String.Empty;
            _nomeFantasia = String.Empty;
            _bairro = String.Empty;
            _cep = String.Empty;
            _complemento = String.Empty;
            _endereco = String.Empty;
            _contato = String.Empty;
            _logotipo = String.Empty;
            _telefone = String.Empty;
            _email = String.Empty;
            _cnpj = String.Empty;
            _numero = String.Empty;
            _dataAlteracao = Utils.dataPadrao;
            _dataCriacao = Utils.dataPadrao;
            _latitude = String.Empty;
            _longitude = String.Empty;
        }

        /// <summary>
        /// Inicialização do elemento
        /// </summary>
        public Empresa()
        {
            IniciarElemento();
        }

        /// <summary>
        /// Rotina que insere o registro no BD
        /// </summary>
        /// <returns>Código do registro inserido no BD</returns>
        public int Incluir()
        {
            if (String.IsNullOrEmpty(_nomeFantasia))
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    sql.Parameters.AddRange(GetParametrosSQL());
                    sql.CommandText = "INSERT INTO Empresas "
                                    + "(cnpj, razaosocial, nomefantasia, cep, complemento, "
                                    + "endereco, numero, bairro, contato, telefone, longitude, "
                                    + "latitude, estado, cidade, datacriacao, dataalteracao, logotipo) "
                                    + "VALUE(@cnpj, @razaosocial, @nomefantasia, @cep, @complemento, "
                                    + "@endereco, @numero, @bairro, @contato, @telefone, @longitude, "
                                    + "@latitude, @estado, @cidade, @datacriacao, @dataalteracao, @logotipo); "
                                    + "SELECT LAST_INSERT_ID();";
                    int resultado = Parser.parseInteiro(new Data().executeEscalar(sql));
                    return resultado;
                }
            }
            catch (ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Rotina que altera os dados do registro no BD
        /// </summary>
        /// <returns>Quantidade de linhas alteradas na ação junto ao BD</returns>
        public int Alterar()
        {
            if (_codigo < 0)
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    sql.Parameters.AddRange(GetParametrosSQL());
                    sql.CommandText = "UPDATE Empresas SET "
                                    + "cnpj = @cnpj, razaosocial = @razaosocial, nomefantasia = @nomefantasia, cep = @cep, "
                                    + "complemento = @complemento, endereco = @endereco, numero = @numero, bairro = @bairro, "
                                    + "contato = @contato, telefone = @telefone, longitude = @longitude, latitude = @latitude, "
                                    + "estado = @estado, cidade = @cidade, datacriacao = @datacriacao, dataalteracao = @dataalteracao, logotipo = @logotipo "
                                    + "WHERE codigo = @codigo";
                    int resultado = Parser.parseInteiro(new Data().execute(sql));
                    return resultado;
                }
            }
            catch (ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Rotina que exclui o registro do BD
        /// </summary>
        /// <returns>Quantidade de linhas alteradas na ação junto ao BD</returns>
        public int Excluir()
        {
            if (_codigo < 0)
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    sql.Parameters.AddRange(GetParametrosSQL());
                    sql.CommandText = "DELETE FROM Empresas "
                                    + "WHERE codigo = @codigo";
                    int resultado = Parser.parseInteiro(new Data().execute(sql));
                    return resultado;
                }
            }
            catch (ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Rotina que localiza e retorna um único registro, mediante o código informado
        /// </summary>
        /// <param name="codigo">VALUE: código de identificação do registro (obrigatório)</param>
        /// <returns>Elemento Usuario com os dados retornados da consulta</returns>
        public static Empresa ConsultarUnico(int codigo)
        {
            if (codigo <= 0)
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            ImunneVacinas.PesquisaEmpresa pesquisa = new PesquisaEmpresa()
            {
                Codigo = codigo
            };

            List<Empresa> retorno = ConsultaGenerica(pesquisa);
            if (retorno == null || retorno.Count.Equals(0))
                return null;
            else
                return retorno[0];
        }

        /// <summary>
        /// Rotina que localiza e retorna um único registro, mediante o código informado
        /// </summary>
        /// <param name="nome">VALUE: campo textual para pesquisa por nome (obrigatório)</param>
        /// <returns>Elemento Usuario com os dados retornados da consulta</returns>
        public static Empresa ConsultarUnico(string cpfcnpj)
        {
            if (String.IsNullOrEmpty(cpfcnpj))
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            ImunneVacinas.PesquisaEmpresa pesquisa = new PesquisaEmpresa()
            {
                CpfCnpj = cpfcnpj
            };

            List<ImunneVacinas.Empresa> retorno = ConsultaGenerica(pesquisa);
            if (retorno == null || retorno.Count.Equals(0))
                return null;
            else
                return retorno[0];
        }

        /// <summary>
        /// Rotina que realiza a pesquisa de elementos mediante os parametros de pesquisa informados
        /// </summary>
        /// <param name="pesquisa">VALUE: elemento de pesquisa com os parametros para consulta</param>
        /// <returns></returns>
        public static List<Empresa> ConsultarEmpresas(PesquisaEmpresa pesquisa)
        {
            return ConsultaGenerica(pesquisa);
        }

        /// <summary>
        /// Rotina que retorna um elemento RetornoCBX para utilização em DropLists
        /// </summary>
        /// <param name="InserirLinhaPadrao">VALUE: Informa se será adiconada a linha padrão</param>
        /// <param name="textoPadrao">VALUE: Informa o texto utilizado para linha padão (se utilizado NULL, será apresentado SELECIONE)</param>
        /// <returns>Lista do elemento RetornoCBX com os dados desejados</returns>
        public static List<RetornoComboBox> ConsultarComboBox(bool InserirLinhaPadrao, string textoPadrao)
        {
            DataTable dt = new DataTable();
            try
            {
                dt.Columns.Add("DisplayMember");
                dt.Columns.Add("ValueMember");

                ImunneVacinas.PesquisaEmpresa pesquisa = new PesquisaEmpresa()
                {
                    Ordenacao = Utils.TipoOrdenacao.porNome
                };

                List<Empresa> retorno = ConsultaGenerica(pesquisa);
                if (retorno != null)
                {
                    foreach (Empresa c in retorno)
                    {
                        dt.Rows.Add(new object[] { c.NomeFantasia.ToUpper(), c.Codigo });
                    }
                }
                return Utils.MontarRetornoComboBox(dt, InserirLinhaPadrao, textoPadrao);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// Rotina interna utilizada para todas as pesquisas do elemento
        /// </summary>
        /// <param name="pesquisa">VALUE: Elemento com os parâmetros com os quais será realizada a pesquisa</param>
        /// <returns>Lista do elemento com os dados retornados da consulta</returns>
        internal static List<Empresa> ConsultaGenerica(PesquisaEmpresa pesquisa)
        {
            StringBuilder ordenacao = new StringBuilder();
            StringBuilder filtro = new StringBuilder();
            StringBuilder top = new StringBuilder();
            MySqlParameter p;

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    if (pesquisa.Quantidade > 0)
                        top.AppendFormat(" LIMIT {0} ", pesquisa.Quantidade);

                    #region Campo Código

                    if (pesquisa.Codigo >= 0)
                    {
                        filtro.Append(" AND Empresas.codigo = @codigo");
                        p = new MySqlParameter("@codigo", pesquisa.Codigo);
                        p.MySqlDbType = GetInfoCodigo().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo CPFCNPJ

                    if (!String.IsNullOrEmpty(pesquisa.CpfCnpj))
                    {
                        string cpfCnpj = String.Format("{0}", pesquisa.CpfCnpj);
                        filtro.Append(" AND Empresas.cnpj = @cnpj ");
                        p = new MySqlParameter("@cnpj", cpfCnpj);
                        p.MySqlDbType = MySqlDbType.VarChar;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campos UF e Cidade

                    if (!String.IsNullOrEmpty(pesquisa.UF) && pesquisa.UF != "-1")
                    {
                        string siglaEstado = String.Format("{0}", pesquisa.CpfCnpj);
                        filtro.Append(" AND Empresas.estado = @estado ");
                        p = new MySqlParameter("@estado", siglaEstado);
                        p.MySqlDbType = MySqlDbType.VarChar;
                        sql.Parameters.Add(p);

                        if (pesquisa.Cidade >= 0)
                        {
                            filtro.Append(" AND Empresas.cidade = @cidade");
                            p = new MySqlParameter("@cidade", pesquisa.Cidade);
                            p.MySqlDbType = GetInfoCidade().Type;
                            sql.Parameters.Add(p);
                        }
                    }

                    #endregion

                    #region Campo Livre

                    if (!String.IsNullOrEmpty(pesquisa.CampoLivre.Trim()))
                    {
                        string campoLivre = String.Format("%{0}%", pesquisa.CampoLivre.Trim());
                        filtro.Append(" AND (Empresas.nomefantasia LIKE @campoLivre ");
                        filtro.Append(" OR Empresas.endereco LIKE @campoLivre ");
                        filtro.Append(" OR Empresas.bairro LIKE @campoLivre ");
                        filtro.Append(" OR Empresas.contato LIKE @campoLivre ");
                        filtro.Append(" OR Empresas.razaosocial LIKE @campoLivre ");
                        filtro.Append(" OR Cidades.nome_cidade LIKE @campoLivre ");
                        filtro.Append(")");
                        p = new MySqlParameter("@campoLivre", campoLivre);
                        p.MySqlDbType = MySqlDbType.VarChar;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    if (filtro.Length > 0)
                        filtro = filtro.Replace(" AND ", " WHERE ", 0, 5);

                    #region Ordenação

                    switch (pesquisa.Ordenacao)
                    {
                        case Utils.TipoOrdenacao.porCodigo:
                            {
                                ordenacao.Append("ORDER BY Empresas.codigo");
                            }
                            break;
                        case Utils.TipoOrdenacao.porCodigoDesc:
                            {
                                ordenacao.Append("ORDER BY Empresas.codigo DESC");
                            }
                            break;
                        case Utils.TipoOrdenacao.porNome:
                            {
                                ordenacao.Append("ORDER BY Empresas.nomefantasia");
                            }
                            break;
                        case Utils.TipoOrdenacao.porNomeDesc:
                            {
                                ordenacao.Append("ORDER BY Empresas.nomefantasia DESC");
                            }
                            break;
                        case Utils.TipoOrdenacao.porData:
                            {
                                ordenacao.Append("ORDER BY Empresas.datacriacao");
                            }
                            break;
                        case Utils.TipoOrdenacao.porDataDesc:
                            {
                                ordenacao.Append("ORDER BY Empresas.datacriacao DESC");
                            }
                            break;
                        case Utils.TipoOrdenacao.randomico:
                            {
                                ordenacao.Append("ORDER BY RAND()");
                            }
                            break;
                        default:
                            {
                                ordenacao.Append("ORDER BY Empresas.codigo");
                            }
                            break;
                    }

                    #endregion

                    sql.CommandText = String.Format("SELECT "
                        + "Cidades.codigo_cidade AS codigoCidade, Cidades.nome_cidade AS nomeCidade, "
                        + "Estados.sigla AS siglaEstado, "
                        + "Empresas.* "
                        + "FROM Empresas "
                        + "INNER JOIN Cidades ON Cidades.codigo_cidade = Empresas.cidade "
                        + "INNER JOIN Estados ON Estados.sigla = Empresas.estado "
                        + "{0} {1} {2}", filtro, ordenacao, top);
                    DataTable dt = new Data().getData(sql);
                    return MontarRetorno(dt);
                }
            }
            catch (ImunneVacinas.ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Rotina que retorna uma lista genérica do elemento, de acordo com o retorno da Consultar ao BD
        /// </summary>
        /// <param name="dt">VALUE: DataTable com o resultado da Consultar ao BD</param>
        /// <returns>Lista do elemento com os dados retornados da consulta</returns>
        internal static List<Empresa> MontarRetorno(DataTable dt)
        {
            int i = -1;
            List<Empresa> list = new List<Empresa>();

            if (dt == null && dt.Rows.Count == 0)
                return list;
            else
            {
                Empresa retorno;
                try
                {
                    for (i = 0; i < dt.Rows.Count; i++)
                    {
                        retorno = new Empresa()
                        {
                            _codigo = Parser.parseInteiro(dt.Rows[i]["codigo"]),
                            _bairro = Parser.parseString(dt.Rows[i]["bairro"]),
                            _cep = Parser.parseString(dt.Rows[i]["cep"]),
                            _complemento = Parser.parseString(dt.Rows[i]["complemento"]),
                            _contato = Parser.parseString(dt.Rows[i]["contato"]),
                            _cnpj = Parser.parseString(dt.Rows[i]["cnpj"]),
                            _email = Parser.parseString(dt.Rows[i]["email"]),
                            _endereco = Parser.parseString(dt.Rows[i]["endereco"]),
                            _nomeFantasia = Parser.parseString(dt.Rows[i]["nomefantasia"]),
                            _numero = Parser.parseString(dt.Rows[i]["numero"]),
                            _telefone = Parser.parseString(dt.Rows[i]["telefone"]),
                            _latitude = Parser.parseString(dt.Rows[i]["latitude"]),
                            _longitude = Parser.parseString(dt.Rows[i]["longitude"]),
                            _razaoSocial = Parser.parseString(dt.Rows[i]["razaosocial"]),
                            _logotipo = Parser.parseString(dt.Rows[i]["logotipo"]),
                            _dataCriacao = Convert.ToDateTime(dt.Rows[i]["datacriacao"])
                        };

                        retorno._idCidade.Codigo = Parser.parseInteiro(dt.Rows[i]["codigoCidade"]);
                        retorno._idCidade.Nome = Parser.parseString(dt.Rows[i]["nomecidade"]);
                        retorno.IdUf.Sigla = Parser.parseString(dt.Rows[i]["siglaEstado"]);

                        if (!String.IsNullOrEmpty(dt.Rows[i]["dataalteracao"].ToString()))
                            retorno._dataAlteracao = Convert.ToDateTime(dt.Rows[i]["dataalteracao"]);

                        list.Add(retorno);
                    }
                    return list;
                }
                catch (Exception ex)
                {
                    ex.Data.Add("último retorno lido", i);
                    throw;
                }
            }
        }
    }
}