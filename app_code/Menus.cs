﻿using MySql.Data.MySqlClient;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace ImunneVacinas
{
    /// <summary>
    /// Descrição resumida de Menus
    /// </summary>
    [Serializable]
    public class Menu
    {
        private const string _tabela = "Menus";
        private static List<BD.Columns> columns;
        private static DateTime columnsUpdated;

        #region Propriedades de todos os campos da tabela

        private int _codigo;
        private string _nome;
        private string _caminho;
        private string _imagem;
        private int _sequencia;
        private Utils.SituacaoRegistro _situacao;
        private DateTime _dataCriacao;
        private DateTime _dataAlteracao;
        private int _menuPai;
        private GrupoMenu _idGrupo;

        /// <summary>
        /// Campo: codigo
        /// </summary>
        public int Codigo
        {
            get
            {
                return _codigo;
            }

            set
            {
                _codigo = value;
            }
        }

        /// <summary>
        /// Campo: ordenacao
        /// </summary>
        public int Sequencia
        {
            get
            {
                return _sequencia;
            }

            set
            {
                _sequencia = value;
            }
        }

        /// <summary>
        /// Campo: descricao
        /// </summary>
        public string Nome
        {
            get
            {
                return _nome;
            }

            set
            {
                _nome = value;
            }
        }

        /// <summary>
        /// Campo: imagem
        /// </summary>
        public string Imagem
        {
            get
            {
                return _imagem;
            }

            set
            {
                _imagem = value;
            }
        }

        /// <summary>
        /// Campo: caminho
        /// </summary>
        public string Caminho
        {
            get
            {
                return _caminho;
            }

            set
            {
                _caminho = value;
            }
        }

        /// <summary>
        /// Campo: situacao
        /// </summary>
        public Utils.SituacaoRegistro Situacao
        {
            get
            {
                return _situacao;
            }

            set
            {
                _situacao = value;
            }
        }

        /// <summary>
        /// Campo: dataCriacao
        /// </summary>
        public DateTime DataCriacao
        {
            get
            {
                return _dataCriacao;
            }

            set
            {
                _dataCriacao = value;
            }
        }

        /// <summary>
        /// Campo: dataAlteracao
        /// </summary>
        public DateTime DataAlteracao
        {
            get
            {
                return _dataAlteracao;
            }

            set
            {
                _dataAlteracao = value;
            }
        }

        /// <summary>
        /// Campo: menuPai
        /// </summary>
        public int MenuPai
        {
            get
            {
                return _menuPai;
            }

            set
            {
                _menuPai = value;
            }
        }

        /// <summary>
        /// Grupo do menu
        /// </summary>
        public GrupoMenu IdGrupo
        {
            get
            {
                return _idGrupo;
            }

            set
            {
                _idGrupo = value;
            }
        }

        #endregion

        #region GetInfo para cada um campo da tabela

        /// <summary>
        /// Busca informações, no banco de dados, sobre os campos da tabela.
        /// </summary>
        private static void GetInfoTabela()
        {
            if (columns == null || (DateTime.Now - columnsUpdated).Minutes > 0) //Atualiza com mais de 24h de diferença
            {
                columns = ImunneVacinas.BD.Columns.getColumnsInfo(_tabela);
                columnsUpdated = DateTime.Now;
            }
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre o campo codigo
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoCodigo()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "codigo");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre o campo descricao
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoNome()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "descricao");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre o campo ordenacao
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoSequencia()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "ordenacao");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre o campo imagem
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoImagem()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "imagem");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre o campo caminho
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoCaminho()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "caminho");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre o campo situacao
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoSituacao()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "situacao");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre o campo dataCriacao
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoDataCriacao()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "datacriacao");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre o campo dataAlteracao
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoDataAlteracao()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "dataalteracao");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre o campo grupo
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoGrupo()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "grupo");
        }

        #endregion

        /// <summary> 
        /// Monta o array com os parâmetros usados pelo objeto MySqlCommand em inserts, deletes e Updates
        /// <returns>Um array com vários SqlParameter</returns>
        /// </summary>
        private Array GetParametrosSQL()
        {
            MySqlParameter p;
            ArrayList ap = new ArrayList();

            p = new MySqlParameter("@codigo", Parser.parseInteiro(_codigo))
            {
                MySqlDbType = GetInfoCodigo().Type
            };
            ap.Add(p);

            p = new MySqlParameter("@ordenacao", Parser.parseInteiro(_sequencia))
            {
                MySqlDbType = GetInfoSequencia().Type
            };
            ap.Add(p);

            p = new MySqlParameter("@descricao", Parser.parseString(_nome));
            p.MySqlDbType = GetInfoNome().Type;
            ap.Add(p);

            p = new MySqlParameter("@imagem", Parser.parseString(_imagem));
            p.MySqlDbType = GetInfoImagem().Type;
            ap.Add(p);

            p = new MySqlParameter("@caminho", Parser.parseString(_caminho))
            {
                MySqlDbType = GetInfoCaminho().Type
            };
            ap.Add(p);

            p = new MySqlParameter("@situacao", (int)_situacao);
            p.MySqlDbType = GetInfoSituacao().Type;
            ap.Add(p);

            p = new MySqlParameter("@dataCriacao", Parser.parseDateTime(_dataCriacao));
            p.MySqlDbType = GetInfoDataCriacao().Type;
            ap.Add(p);

            p = new MySqlParameter("@dataAlteracao", Parser.parseDateTime(_dataAlteracao));
            p.MySqlDbType = GetInfoDataAlteracao().Type;
            ap.Add(p);

            p = new MySqlParameter("@grupo", Parser.parseInteiro(_idGrupo.Codigo))
            {
                MySqlDbType = GetInfoGrupo().Type
            };
            ap.Add(p);

            return ap.ToArray();
        }

        /// <summary>
        /// Inicializa as propriedades com os valores padrão
        /// </summary>
        private void PopularCampos()
        {
            GetInfoTabela();

            _idGrupo = new GrupoMenu();

            _idGrupo.Codigo = -1;
            _nome = String.Empty;
            _sequencia = -1;
            _situacao = Utils.SituacaoRegistro.Ativo;
            _imagem = String.Empty;
            _caminho = String.Empty;
            _dataAlteracao = Utils.dataPadrao;
            _dataCriacao = Utils.dataPadrao;
        }

        /// <summary>
        /// Iniciliza elemento
        /// </summary>
        public Menu()
        {
            PopularCampos();
        }

        /// <summary>
        /// Localiza um registro único, por base do seu código
        /// </summary>
        /// <param name="codigo">VALUE: código de identificação do registro (obrigatório)</param>
        /// <returns></returns>
        public static Menu ConsultarUnico(int codigo)
        {
            if (codigo < 0)
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            PesquisaMenu pesquisa = new PesquisaMenu();
            pesquisa.Codigo = codigo;

            List<Menu> retornos = ConsultaGenerica(pesquisa);
            if (retornos != null && retornos.Count > 0)
                return retornos[0];
            else return null;
        }

        /// <summary>
        /// Localiza um registro único, por base do seu nome
        /// </summary>
        /// <param name="nome">VALUE: código de identificação do registro (obrigatório)</param>
        /// <returns></returns>
        public static Menu ConsultarUnico(string nome)
        {
            if (String.IsNullOrEmpty(nome))
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            PesquisaMenu pesquisa = new PesquisaMenu()
            {
                Nome = nome
            };

            List<Menu> retornos = ConsultaGenerica(pesquisa);
            if (retornos != null && retornos.Count > 0)
                return retornos[0];
            else return null;
        }

        /// <summary>
        /// Localiza 1 ou N registro de acordo com os parâmetros de pesquisa
        /// </summary>
        /// <param name="pesquisa">VALUE: Parâmetros de pesquisa</param>
        /// <returns></returns>
        public static List<Menu> ConsultarMenus(PesquisaMenu pesquisa)
        {
            return ConsultaGenerica(pesquisa);
        }

        /// <summary>
        /// Pesquisa que realiza todas as Consultas no banco de dados
        /// </summary>
        /// <param name="pesquisa"></param>
        /// <returns></returns>
        internal static List<Menu> ConsultaGenerica(PesquisaMenu pesquisa)
        {
            StringBuilder ordenacao = new StringBuilder();
            StringBuilder filtro = new StringBuilder();
            StringBuilder top = new StringBuilder();
            MySqlParameter p;

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    if (pesquisa.Quantidade > 0)
                        top.AppendFormat(" LIMIT {0} ", pesquisa.Quantidade);

                    #region Campo Código

                    if (pesquisa.Codigo >= 0)
                    {
                        filtro.Append(" AND Menus.codigo = @codigo");
                        p = new MySqlParameter("@codigo", pesquisa.Codigo);
                        p.MySqlDbType = GetInfoCodigo().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Grupo

                    if (pesquisa.Grupo >= 0)
                    {
                        filtro.Append(" AND Menus.grupo = @grupo");
                        p = new MySqlParameter("@grupo", pesquisa.Grupo);
                        p.MySqlDbType = GetInfoGrupo().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Nome

                    if (!String.IsNullOrEmpty(pesquisa.Nome))
                    {
                        string nome = String.Format("{0}", pesquisa.Nome);
                        filtro.Append(" AND (Menus.nome = @nome)");
                        p = new MySqlParameter("@nome", nome);
                        p.MySqlDbType = MySqlDbType.VarChar;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Livre

                    if (!String.IsNullOrEmpty(pesquisa.CampoLivre.Trim()))
                    {
                        string campoLivre = String.Format("%{0}%", pesquisa.CampoLivre.Trim());
                        filtro.Append(" AND (Menus.nome LIKE @campoLivre)");
                        p = new MySqlParameter("@campoLivre", campoLivre);
                        p.MySqlDbType = MySqlDbType.VarChar;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    if (filtro.Length > 0)
                        filtro = filtro.Replace(" AND ", " WHERE ", 0, 5);

                    #region Ordenação

                    switch (pesquisa.Sequencia)
                    {
                        case Utils.TipoOrdenacao.porCodigo:
                            {
                                ordenacao.Append("ORDER BY Menus.codigo");
                            }
                            break;
                        case Utils.TipoOrdenacao.porCodigoDesc:
                            {
                                ordenacao.Append("ORDER BY Menus.codigo DESC");
                            }
                            break;
                        case Utils.TipoOrdenacao.porSequencia:
                            {
                                ordenacao.Append("ORDER BY Menus.ordenacao");
                            }
                            break;
                        case Utils.TipoOrdenacao.porNome:
                            {
                                ordenacao.Append("ORDER BY Menus.nome");
                            }
                            break;
                        case Utils.TipoOrdenacao.porNomeDesc:
                            {
                                ordenacao.Append("ORDER BY Menus.menu DESC");
                            }
                            break;
                        case Utils.TipoOrdenacao.porData:
                            {
                                ordenacao.Append("ORDER BY Menus.dataCriacao");
                            }
                            break;
                        case Utils.TipoOrdenacao.porDataDesc:
                            {
                                ordenacao.Append("ORDER BY Menus.dataCriacao DESC");
                            }
                            break;
                        case Utils.TipoOrdenacao.randomico:
                            {
                                ordenacao.Append("ORDER BY RAND()");
                            }
                            break;
                        default:
                            {
                                ordenacao.Append("ORDER BY Menus.codigo");
                            }
                            break;
                    }

                    #endregion

                    sql.CommandText = String.Format("SELECT "
                        + "GruposMenus.descricao AS nomeGrupo, "
                        + "Menus.* "
                        + "FROM Menus "
                        + "LEFT JOIN GruposMenus ON GruposMenus.codigo = Menus.grupo "
                        + "{0} {1} {2}", filtro, ordenacao, top);
                    DataTable dt = new Data().getData(sql);
                    return MontarRetorno(dt);
                }
            }
            catch (ImunneVacinas.ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Monta a lista do elemento para retornar
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        internal static List<Menu> MontarRetorno(DataTable dt)
        {
            int i = -1;
            List<Menu> list = new List<Menu>();
            if (dt == null && dt.Rows.Count == 0)
                return list;
            else
            {
                Menu retorno;
                try
                {
                    for (i = 0; i < dt.Rows.Count; i++)
                    {
                        retorno = new Menu();
                        retorno._codigo = Parser.parseInteiro(dt.Rows[i]["codigo"]);
                        retorno._nome = Parser.parseString(dt.Rows[i]["nome"]);
                        retorno._caminho = Parser.parseString(dt.Rows[i]["caminho"]);
                        retorno._imagem = Parser.parseString(dt.Rows[i]["imagem"]);
                        retorno._dataCriacao = Convert.ToDateTime(dt.Rows[i]["datacriacao"]);
                        retorno._sequencia = Parser.parseInteiro(dt.Rows[i]["sequencia"]);
                        retorno._situacao = (ImunneVacinas.Utils.SituacaoRegistro)Enum.Parse(typeof(ImunneVacinas.Utils.SituacaoRegistro), dt.Rows[i]["situacao"].ToString());
                        retorno._menuPai = Parser.parseInteiro(dt.Rows[i]["menupai"]);

                        retorno._idGrupo.Codigo = Parser.parseInteiro(dt.Rows[i]["grupo"]);
                        retorno._idGrupo.Nome = Parser.parseString(dt.Rows[i]["nomeGrupo"]);

                        if (!String.IsNullOrEmpty(dt.Rows[i]["dataalteracao"].ToString()))
                            retorno._dataAlteracao = Convert.ToDateTime(dt.Rows[i]["dataalteracao"]);

                        list.Add(retorno);
                    }
                    return list;
                }
                catch (Exception ex)
                {
                    ex.Data.Add("último retorno lido", i);
                    throw;
                }
            }
        }
    }

    /// <summary>
    /// Descrição resumida de Permissões
    /// </summary>
    [Serializable]
    public class Permissao
    {
        private const string _tabela = "Permissoes";
        private static List<BD.Columns> columns;
        private static DateTime columnsUpdated;

        #region Propriedades de todos os campos da tabela

        private Menu _menu;
        private TipoUsuario _tipoUsuario;
        private int _codigo;
        private DateTime _dataCriacao;

        /// <summary>
        /// Campo: menu
        /// </summary>
        public Menu Menu
        {
            get
            {
                return _menu;
            }

            set
            {
                _menu = value;
            }
        }

        /// <summary>
        /// Campo: tipoUsuario
        /// </summary>
        public TipoUsuario TipoUsuario
        {
            get
            {
                return _tipoUsuario;
            }

            set
            {
                TipoUsuario = value;
            }
        }

        /// <summary>
        /// Campo: codigo
        /// </summary>
        public int Codigo
        {
            get
            {
                return _codigo;
            }

            set
            {
                _codigo = value;
            }
        }

        /// <summary>
        /// Campo: dataCriacao
        /// </summary>
        public DateTime DataCriacao
        {
            get
            {
                return _dataCriacao;
            }

            set
            {
                _dataCriacao = value;
            }
        }

        #endregion

        #region GetInfo para cada um campo da tabela

        /// <summary>
        /// Busca informações, no banco de dados, sobre os campos da tabela.
        /// </summary>
        private static void GetInfoTabela()
        {
            if (columns == null || (DateTime.Now - columnsUpdated).Minutes > 0) //Atualiza com mais de 24h de diferença
            {
                columns = ImunneVacinas.BD.Columns.getColumnsInfo(_tabela);
                columnsUpdated = DateTime.Now;
            }
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre o campo menu
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoMenu()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "menu");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre o campo tipoUsuario
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoTipoUsuario()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "tipo");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre o campo codigo
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoCodigo()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "codigo");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre o campo dataCriacao
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoDataCriacao()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "datacriacao");
        }

        #endregion

        /// <summary> 
        /// Monta o array com os parâmetros usados pelo objeto MySqlCommand em inserts, deletes e Updates
        /// <returns>Um array com vários SqlParameter</returns>
        /// </summary>
        private Array GetParametrosSQL()
        {
            MySqlParameter p;
            ArrayList ap = new ArrayList();

            p = new MySqlParameter("@menu", Parser.parseInteiro(_menu.Codigo))
            {
                MySqlDbType = GetInfoMenu().Type
            };
            ap.Add(p);

            p = new MySqlParameter("@tipo", Parser.parseInteiro(_tipoUsuario.Codigo))
            {
                MySqlDbType = GetInfoTipoUsuario().Type
            };
            ap.Add(p);

            p = new MySqlParameter("@codigo", Parser.parseInteiro(_codigo))
            {
                MySqlDbType = GetInfoCodigo().Type
            };
            ap.Add(p);

            p = new MySqlParameter("@dataCriacao", Parser.parseDateTime(_dataCriacao))
            {
                MySqlDbType = GetInfoDataCriacao().Type
            };
            ap.Add(p);

            return ap.ToArray();
        }

        /// <summary>
        /// Inicializa as propriedades com os valores padrão
        /// </summary>
        private void PopularCampos()
        {
            GetInfoTabela();

            _menu = new Menu();
            _tipoUsuario = new TipoUsuario();

            _menu.Codigo = -1;
            _tipoUsuario.Codigo = -1;
            _dataCriacao = Utils.dataPadrao;
        }

        /// <summary>
        /// Iniciliza elemento
        /// </summary>
        public Permissao()
        {
            PopularCampos();
        }

        /// <summary>
        /// Localiza um registro único, por base do seu código
        /// </summary>
        /// <param name="codigo">VALUE: código de identificação do registro (obrigatório)</param>
        /// <returns></returns>
        public static Permissao ConsultarUnico(int codigo)
        {
            if (codigo < 0)
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            PesquisaPermissao pesquisa = new PesquisaPermissao();
            pesquisa.Codigo = codigo;

            List<Permissao> retornos = ConsultaGenerica(pesquisa);
            if (retornos != null && retornos.Count > 0)
                return retornos[0];
            else return null;
        }

        /// <summary>
        /// Localiza 1 ou N registro de acordo com os parâmetros de pesquisa
        /// </summary>
        /// <param name="pesquisa">VALUE: Parâmetros de pesquisa</param>
        /// <returns></returns>
        public static List<Permissao> ConsultarPermissoes(PesquisaPermissao pesquisa)
        {
            return ConsultaGenerica(pesquisa);
        }

        /// <summary>
        /// Pesquisa que realiza todas as Consultas no banco de dados
        /// </summary>
        /// <param name="pesquisa"></param>
        /// <returns></returns>
        internal static List<Permissao> ConsultaGenerica(PesquisaPermissao pesquisa)
        {
            StringBuilder ordenacao = new StringBuilder();
            StringBuilder filtro = new StringBuilder();
            StringBuilder top = new StringBuilder();
            MySqlParameter p;

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    if (pesquisa.Quantidade > 0)
                        top.AppendFormat(" LIMIT {0} ", pesquisa.Quantidade);

                    #region Campo Menu

                    if (pesquisa.Menu >= 0)
                    {
                        filtro.Append(" AND Permissoes.menu = @menu");
                        p = new MySqlParameter("@menu", pesquisa.Menu);
                        p.MySqlDbType = GetInfoMenu().Type;
                        sql.Parameters.Add(p);
                    }
                    else filtro.Append(" AND Menus.menupai = -1");

                    #endregion

                    #region Campo TipoUsuario

                    if (pesquisa.TipoUsuario >= 0)
                    {
                        filtro.Append(" AND Permissoes.tipo = @tipo");
                        p = new MySqlParameter("@tipo", pesquisa.TipoUsuario);
                        p.MySqlDbType = GetInfoTipoUsuario().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Código

                    if (pesquisa.Codigo >= 0)
                    {
                        filtro.Append(" AND Permissoes.codigo = @codigo");
                        p = new MySqlParameter("@codigo", pesquisa.Codigo);
                        p.MySqlDbType = GetInfoCodigo().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Grupo

                    if (pesquisa.Grupo >= 0)
                    {
                        filtro.Append(" AND Menus.grupo = @grupo");
                        p = new MySqlParameter("@grupo", pesquisa.Grupo);
                        p.MySqlDbType = GetInfoCodigo().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    if (filtro.Length > 0)
                        filtro = filtro.Replace(" AND ", " WHERE ", 0, 5);

                    #region Ordenação

                    ordenacao.Append("ORDER BY Menus.sequencia");

                    #endregion

                    sql.CommandText = String.Format("SELECT "
                                    + "Permissoes.* "
                                    + "FROM Permissoes "
                                    + "INNER JOIN Menus ON Menus.codigo = Permissoes.menu "
                                    + "{0} {1} {2}", filtro, ordenacao, top);
                    DataTable dt = new Data().getData(sql);
                    return MontarRetorno(dt);
                }
            }
            catch (ImunneVacinas.ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Monta a lista do elemento para retornar
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        internal static List<Permissao> MontarRetorno(DataTable dt)
        {
            int i = -1;
            List<Permissao> list = new List<Permissao>();
            if (dt == null && dt.Rows.Count == 0)
                return list;
            else
            {
                Permissao retorno;
                try
                {
                    for (i = 0; i < dt.Rows.Count; i++)
                    {
                        retorno = new Permissao();
                        retorno._codigo = Parser.parseInteiro(dt.Rows[i]["codigo"]);
                        retorno._menu.Codigo = Parser.parseInteiro(dt.Rows[i]["menu"]);
                        retorno._tipoUsuario.Codigo = Parser.parseInteiro(dt.Rows[i]["tipo"]);
                        retorno._dataCriacao = Convert.ToDateTime(dt.Rows[i]["datacriacao"]);

                        list.Add(retorno);
                    }
                    return list;
                }
                catch (Exception ex)
                {
                    ex.Data.Add("último retorno lido", i);
                    throw;
                }
            }
        }
    }

    /// <summary>
    ///  Descrição resumida de GrupoMenu
    /// </summary>
    [Serializable]
    public class GrupoMenu
    {
        private const string _tabela = "GruposMenus";
        private static List<BD.Columns> columns;
        private static DateTime columnsUpdated;

        #region Propriedades de todos os campos da tabela

        public int Codigo { get; set; }
        public string Nome { get; set; }
        public string Imagem { get; set; }

        #endregion

        /// <summary>
        /// Localiza 1 ou N registro de acordo com os parâmetros de pesquisa
        /// </summary>
        /// <returns></returns>
        public static List<GrupoMenu> ConsultarGrupos(int role)
        {
            return ConsultaGenerica(role);
        }

        /// <summary>
        /// Pesquisa que realiza todas as Consultas no banco de dados
        /// </summary>
        /// <param name="pesquisa"></param>
        /// <returns></returns>
        internal static List<GrupoMenu> ConsultaGenerica(int role)
        {
            StringBuilder ordenacao = new StringBuilder();
            StringBuilder filtro = new StringBuilder();
            StringBuilder top = new StringBuilder();
            MySqlParameter p;

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    #region Campo Role

                    if (role >= 0)
                    {
                        filtro.Append(" AND Permissoes.tipo = @role");
                        p = new MySqlParameter("@role", role);
                        p.MySqlDbType = MySqlDbType.Int32;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    if (filtro.Length > 0)
                        filtro = filtro.Replace(" AND ", " WHERE ", 0, 5);

                    #region Ordenação

                    ordenacao.Append("ORDER BY GruposMenus.sequencia");

                    #endregion

                    sql.CommandText = String.Format("SELECT "
                                    + "DISTINCT(GruposMenus.codigo), GruposMenus.descricao, GruposMenus.imagem, GruposMenus.sequencia "
                                    + "FROM Permissoes "
                                    + "INNER JOIN Menus ON Menus.codigo = Permissoes.menu "
                                    + "INNER JOIN GruposMenus ON GruposMenus.codigo = Menus.grupo "
                                    + "{0} {1}", filtro, ordenacao);
                    DataTable dt = new Data().getData(sql);
                    return MontarRetorno(dt);
                }
            }
            catch (ImunneVacinas.ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        internal static List<GrupoMenu> MontarRetorno(DataTable dt)
        {
            int i = -1;
            List<GrupoMenu> list = new List<GrupoMenu>();
            if (dt == null && dt.Rows.Count == 0)
                return list;
            else
            {
                GrupoMenu retorno;
                try
                {
                    for (i = 0; i < dt.Rows.Count; i++)
                    {
                        retorno = new GrupoMenu();
                        retorno.Codigo = Parser.parseInteiro(dt.Rows[i]["codigo"]);
                        retorno.Nome = Parser.parseString(dt.Rows[i]["descricao"]);
                        retorno.Imagem = Parser.parseString(dt.Rows[i]["imagem"]);

                        list.Add(retorno);
                    }
                    return list;
                }
                catch (Exception ex)
                {
                    ex.Data.Add("último retorno lido", i);
                    throw;
                }
            }
        }
    }
}