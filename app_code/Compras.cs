﻿using MySql.Data.MySqlClient;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace ImunneVacinas
{
    /// <summary>
    /// Descrição resumida de Compras
    /// </summary>
    public class Compra
    {
        /// <summary>
        /// 
        /// </summary>
        public enum SituacaoCompra
        {
            Indiferente = -1,
            Aberta = 1,
            Cancelada = 2,
            Concluida = 3
        }

        /// <summary>
        /// 
        /// </summary>
        public enum CategoriaCompra
        {
            Indiferente = -1,
            Paga = 1,
            Gratis = 2,
            Importada = 3
        }

        /// <summary>
        /// 
        /// </summary>
        public enum Aplicacao
        {
            Indiferente = -1,
            NaoAplicado = 0,
            Aplicado = 1
        }

        private static string _tabela = "Compras";
        private static List<BD.Columns> columns;
        private static DateTime columnsUpdated;

        #region Propriedades do elemento

        private Clinica _idClinica;
        private Campanha _idCampanha;
        private Participante _idParticipante;
        private int _codigo;
        private decimal _valor;
        private DateTime _dataCompra;
        private int _parcelas;
        private SituacaoCompra _situacao;
        private CategoriaCompra _categoria;
        private Aplicacao _aplicacaoCompra;
        private DateTime _dataAplicacao;
        private DateTime _dataLiberacao;
        private string _numeroComprovante;

        /// <summary>
        /// 
        /// </summary>
        public Clinica IdClinica
        {
            get
            {
                return _idClinica;
            }

            set
            {
                _idClinica = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Campanha IdCampanha
        {
            get
            {
                return _idCampanha;
            }

            set
            {
                _idCampanha = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Participante IdParticipante
        {
            get
            {
                return _idParticipante;
            }

            set
            {
                _idParticipante = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public int Codigo
        {
            get
            {
                return _codigo;
            }

            set
            {
                _codigo = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public decimal Valor
        {
            get
            {
                return _valor;
            }

            set
            {
                _valor = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public DateTime DataCompra
        {
            get
            {
                return _dataCompra;
            }

            set
            {
                _dataCompra = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public int Parcelas
        {
            get
            {
                return _parcelas;
            }

            set
            {
                _parcelas = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public SituacaoCompra Situacao
        {
            get
            {
                return _situacao;
            }

            set
            {
                _situacao = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public CategoriaCompra Categoria
        {
            get
            {
                return _categoria;
            }

            set
            {
                _categoria = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Aplicacao AplicacaoCompra
        {
            get
            {
                return _aplicacaoCompra;
            }

            set
            {
                _aplicacaoCompra = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public DateTime DataAplicacao
        {
            get
            {
                return _dataAplicacao;
            }

            set
            {
                _dataAplicacao = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public DateTime DataLiberacao
        {
            get
            {
                return _dataLiberacao;
            }

            set
            {
                _dataLiberacao = value;
            }
        }

        /// <summary>
        /// Número do comprovante de compra, usado para pesquisas e listagens
        /// </summary>
        public string NumeroComprovante
        {
            get
            {
                return _numeroComprovante;
            }

            set
            {
                _numeroComprovante = value;
            }
        }

        #endregion

        #region GetInfos

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos os campos da tabela.
        /// </summary>
        private static void GetInfoTabela()
        {
            if (columns == null || (DateTime.Now - columnsUpdated).Minutes > 0)
            {
                columns = BD.Columns.getColumnsInfo(_tabela);
                columnsUpdated = DateTime.Now;
            }
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo participante.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoParticipante()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "participante");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo clinica.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoClinica()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "clinica");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo campanha.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoCampanha()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "campanha");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo codigo.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoCodigo()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "codigo");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo valor.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoValor()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "valor");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo parcelas.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoParcelas()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "parcelas");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo datacompra.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoDataCompra()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "datacompra");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo situacao.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoSituacao()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "situacao");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo categoria.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoCategoria()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "categoria");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo aplicacao.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoAplicacao()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "aplicacao");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo dataaplicacao.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoDataAplicacao()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "dataaplicacao");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoDataLiberacao()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "dataliberacao");
        }

        #endregion

        /// <summary> 
        /// Rotina que cria um Array com os parâmetros utilizados nas ações de INSERT, UPDATE e DELETE
        /// <returns>Array com vários MySqlParameters</returns>
        /// </summary>
        private Array GetParametrosSQL()
        {
            MySqlParameter p;
            ArrayList ap = new ArrayList();

            p = new MySqlParameter("@participante", Parser.parseInteiro(_idParticipante.Codigo));
            p.MySqlDbType = GetInfoParticipante().Type;
            ap.Add(p);

            p = new MySqlParameter("@campanha", Parser.parseInteiro(_idCampanha.Codigo));
            p.MySqlDbType = GetInfoCampanha().Type;
            ap.Add(p);

            p = new MySqlParameter("@clinica", Parser.parseInteiro(_idClinica.Codigo));
            p.MySqlDbType = GetInfoClinica().Type;
            ap.Add(p);

            p = new MySqlParameter("@codigo", Parser.parseInteiro(_codigo));
            p.MySqlDbType = GetInfoCodigo().Type;
            ap.Add(p);

            p = new MySqlParameter("@valor", Parser.parseDecimal(_valor));
            p.MySqlDbType = GetInfoValor().Type;
            ap.Add(p);

            p = new MySqlParameter("@datacompra", Parser.parseDateTimeBD(_dataCompra));
            p.MySqlDbType = GetInfoDataCompra().Type;
            ap.Add(p);

            p = new MySqlParameter("@parcelas", Parser.parseInteiro(_parcelas));
            p.MySqlDbType = GetInfoParticipante().Type;
            ap.Add(p);

            p = new MySqlParameter("@situacao", (int)_situacao);
            p.MySqlDbType = GetInfoSituacao().Type;
            ap.Add(p);

            p = new MySqlParameter("@categoria", (int)_categoria);
            p.MySqlDbType = GetInfoCategoria().Type;
            ap.Add(p);

            p = new MySqlParameter("@aplicacao", (int)_aplicacaoCompra);
            p.MySqlDbType = GetInfoAplicacao().Type;
            ap.Add(p);

            p = new MySqlParameter("@dataaplicacao", Parser.parseDateTimeBD(_dataAplicacao));
            p.MySqlDbType = GetInfoDataAplicacao().Type;
            ap.Add(p);

            p = new MySqlParameter("@dataliberacao", Parser.parseDateTimeBD(_dataLiberacao));
            p.MySqlDbType = GetInfoDataLiberacao().Type;
            ap.Add(p);

            return ap.ToArray();
        }

        /// <summary>
        /// Inicialização das propriedades do elemento com seus valores padrão
        /// </summary>
        private void IniciarElemento()
        {
            GetInfoTabela();

            _idParticipante = new Participante();
            _idClinica = new Clinica();
            _idCampanha = new Campanha();

            _idParticipante.Codigo = -1;
            _idCampanha.Codigo = -1;
            _idClinica.Codigo = -1;
            _dataCompra = Utils.dataPadrao;
            _parcelas = 1;
            _valor = 0;
            _situacao = SituacaoCompra.Aberta;
            _categoria = CategoriaCompra.Paga;
            _aplicacaoCompra = Aplicacao.NaoAplicado;
            _dataAplicacao = Utils.dataPadrao;
            _dataLiberacao = Utils.dataPadrao;
            _numeroComprovante = String.Empty;
        }

        /// <summary>
        /// Inicialização do elemento
        /// </summary>
        public Compra()
        {
            IniciarElemento();
        }

        /// <summary>
        /// Rotina que insere o registro no BD
        /// </summary>
        /// <returns>Código do registro inserido no BD</returns>
        public int Incluir()
        {
            if (_idParticipante.Codigo <= 0 || _valor < 0)
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    sql.Parameters.AddRange(GetParametrosSQL());
                    sql.CommandText = "INSERT INTO Compras "
                                    + "(participante, clinica, campanha, valor, parcelas, "
                                    + "datacompra, situacao, categoria, aplicacao, dataaplicacao, dataliberacao) "
                                    + "VALUE( "
                                    + "@participante, @clinica, @campanha, @valor, @parcelas, "
                                    + "@datacompra, @situacao, @categoria, @aplicacao, @dataaplicacao, @dataliberacao); "
                                    + "SELECT LAST_INSERT_ID();";
                    int resultado = Parser.parseInteiro(new Data().executeEscalar(sql));
                    return resultado;
                }
            }
            catch (ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Rotina que altera os dados do registro no BD
        /// </summary>
        /// <returns>Quantidade de linhas alteradas na ação junto ao BD</returns>
        public int Alterar()
        {
            if (_codigo < 0)
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    sql.Parameters.AddRange(GetParametrosSQL());
                    sql.CommandText = "UPDATE Compras SET "
                                    + "valor = @valor, parcelas = @parcelas, aplicacao = @aplicacao, dataaplicacao = @dataaplicacao, "
                                    + "clinica = @clinica, campanha = @campanha, categoria = @categoria, dataliberacao = @dataliberacao, "
                                    + "datacompra = @datacompra, situacao = @situacao "
                                    + "WHERE codigo = @codigo; ";
                    int resultado = Parser.parseInteiro(new Data().execute(sql));
                    return resultado;
                }
            }
            catch (ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Rotina que exclui o registro do BD
        /// </summary>
        /// <returns>Quantidade de linhas alteradas na ação junto ao BD</returns>
        public int Excluir()
        {
            if (_codigo < 0)
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    sql.Parameters.AddRange(GetParametrosSQL());
                    sql.CommandText = "DELETE FROM Compras "
                                    + "WHERE codigo = @codigo";
                    int resultado = Parser.parseInteiro(new Data().execute(sql));
                    return resultado;
                }
            }
            catch (ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Rotina que localiza e retorna um único registro, mediante o código informado
        /// </summary>
        /// <param name="codigo">VALUE: código de identificação do registro (obrigatório)</param>
        /// <returns>Elemento Usuario com os dados retornados da consulta</returns>
        public static Compra ConsultarUnico(int codigo)
        {
            if (codigo <= 0)
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            ImunneVacinas.PesquisaCompra pesquisa = new PesquisaCompra()
            {
                Codigo = codigo
            };

            List<Compra> retorno = ConsultaGenerica(pesquisa);
            if (retorno == null || retorno.Count.Equals(0))
                return null;
            else
                return retorno[0];
        }

        /// <summary>
        /// Rotina que realiza a pesquisa de elementos mediante os parametros de pesquisa informados
        /// </summary>
        /// <param name="pesquisa">VALUE: elemento de pesquisa com os parametros para consulta</param>
        /// <returns></returns>
        public static List<Compra> ConsultarComprar(PesquisaCompra pesquisa)
        {
            return ConsultaGenerica(pesquisa);
        }

        /// <summary>
        /// Rotina que realiza a pesquisa de elementos mediante os parametros de pesquisa informados
        /// </summary>
        /// <param name="pesquisa">VALUE: elemento de pesquisa com os parametros para consulta</param>
        /// <returns></returns>
        public static List<Compra> ConsultarCompras(PesquisaCompra pesquisa)
        {
            return ConsultaGenerica(pesquisa);
        }

        /// <summary>
        /// Rotina que realiza a pesquisa de elementos mediante os parametros de pesquisa informados
        /// </summary>
        /// <param name="pesquisa">VALUE: elemento de pesquisa com os parametros para consulta</param>
        /// <returns></returns>
        public static int ConsultarTotalCompras(PesquisaCompra pesquisa)
        {
            int retorno = 0;

            StringBuilder ordenacao = new StringBuilder();
            StringBuilder filtro = new StringBuilder();
            StringBuilder top = new StringBuilder();
            MySqlParameter p;

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    #region Campo Código

                    if (pesquisa.Codigo >= 0)
                    {
                        filtro.Append(" AND Compras.codigo = @codigo");
                        p = new MySqlParameter("@codigo", pesquisa.Codigo);
                        p.MySqlDbType = GetInfoCodigo().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Participante

                    if (pesquisa.Participante >= 0)
                    {
                        filtro.Append(" AND Compras.participante = @participante");
                        p = new MySqlParameter("@participante", pesquisa.Participante);
                        p.MySqlDbType = GetInfoParticipante().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Empresa

                    if (pesquisa.Empresa > 0)
                    {
                        filtro.Append(" AND Campanhas.empresa = @empresa");
                        p = new MySqlParameter("@empresa", pesquisa.Empresa);
                        p.MySqlDbType = MySqlDbType.Int32;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Clinica

                    if (pesquisa.Clinica >= 0)
                    {
                        filtro.Append(" AND Compras.clinica = @clinica");
                        p = new MySqlParameter("@clinica", pesquisa.Clinica);
                        p.MySqlDbType = GetInfoClinica().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Campanha

                    if (pesquisa.Campanha >= 0)
                    {
                        filtro.Append(" AND Compras.campanha = @campanha");
                        p = new MySqlParameter("@campanha", pesquisa.Campanha);
                        p.MySqlDbType = GetInfoCampanha().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Situação

                    if (pesquisa.Situacao != SituacaoCompra.Indiferente)
                    {
                        filtro.Append(" AND Compras.situacao = @situacao");
                        p = new MySqlParameter("@situacao", (int)pesquisa.Situacao);
                        p.MySqlDbType = GetInfoSituacao().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Aplicação

                    if (pesquisa.Aplicacao != Aplicacao.Indiferente)
                    {
                        filtro.Append(" AND Compras.aplicacao = @aplicacao");
                        p = new MySqlParameter("@aplicacao", (int)pesquisa.Aplicacao);
                        p.MySqlDbType = GetInfoAplicacao().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Categoria

                    if (pesquisa.Categoria != CategoriaCompra.Indiferente)
                    {
                        filtro.Append(" AND Compras.categoria = @categoria");
                        p = new MySqlParameter("@categoria", (int)pesquisa.Categoria);
                        p.MySqlDbType = GetInfoCategoria().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campos UF e Cidade

                    if (!String.IsNullOrEmpty(pesquisa.Uf) && pesquisa.Uf != "-1")
                    {
                        string siglaEstado = String.Format("{0}", pesquisa.Uf);
                        filtro.Append(" AND Clinicas.estado = @estado ");
                        p = new MySqlParameter("@estado", siglaEstado);
                        p.MySqlDbType = MySqlDbType.VarChar;
                        sql.Parameters.Add(p);

                        if (pesquisa.Cidade >= 0)
                        {
                            filtro.Append(" AND Clinicas.cidade = @cidade");
                            p = new MySqlParameter("@cidade", pesquisa.Cidade);
                            p.MySqlDbType = MySqlDbType.Int32;
                            sql.Parameters.Add(p);
                        }
                    }

                    #endregion

                    #region Campos Data Compra

                    if (pesquisa.DataInicio != Utils.dataPadrao && pesquisa.DataFim != Utils.dataPadrao)
                    {
                        filtro.Append(" AND Compras.datacompra >= @inicio");
                        p = new MySqlParameter("@inicio", Parser.parseDateTimeBD(pesquisa.DataInicio))
                        {
                            MySqlDbType = GetInfoDataCompra().Type
                        };
                        sql.Parameters.Add(p);

                        filtro.Append(" AND Compras.datacompra <= @termino");
                        p = new MySqlParameter("@termino", Parser.parseDateTimeBD(pesquisa.DataFim))
                        {
                            MySqlDbType = GetInfoDataCompra().Type
                        };
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Livre

                    if (!String.IsNullOrEmpty(pesquisa.CampoLivre.Trim()))
                    {
                        string campoLivre = String.Format("%{0}%", pesquisa.CampoLivre.Trim());
                        filtro.Append(" AND (Participantes.nome LIKE @campoLivre ");
                        filtro.Append(" OR TransacoesRealizadas.numeroComprovanteVenda LIKE @campoLivre");
                        filtro.Append(" OR Participantes.cpf LIKE @campoLivre");
                        filtro.Append(")");
                        p = new MySqlParameter("@campoLivre", campoLivre)
                        {
                            MySqlDbType = MySqlDbType.VarChar
                        };
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Valor

                    if (pesquisa.Valor == true)
                        filtro.Append(" AND Compras.valor > 0");

                    #endregion

                    if (filtro.Length > 0)
                        filtro = filtro.Replace(" AND ", " WHERE ", 0, 5);

                    #region Ordenação

                    switch (pesquisa.Ordenacao)
                    {
                        case Utils.TipoOrdenacao.porCodigo:
                            {
                                ordenacao.Append("ORDER BY Compras.codigo");
                            }
                            break;
                        case Utils.TipoOrdenacao.porCodigoDesc:
                            {
                                ordenacao.Append("ORDER BY Compras.codigo DESC");
                            }
                            break;
                        case Utils.TipoOrdenacao.porData:
                            {
                                ordenacao.Append("ORDER BY Compras.datacompra, Campanhas.identificacao, Clinicas.nomefantasia");
                            }
                            break;
                        case Utils.TipoOrdenacao.porDataDesc:
                            {
                                ordenacao.Append("ORDER BY Compras.datacompra DESC, Campanhas.identificacao, Clinicas.nomefantasia");
                            }
                            break;
                        case Utils.TipoOrdenacao.randomico:
                            {
                                ordenacao.Append("ORDER BY RAND()");
                            }
                            break;
                        default:
                            {
                                ordenacao.Append("ORDER BY Compras.codigo");
                            }
                            break;
                    }

                    #endregion

                    sql.CommandText = String.Format("SELECT COUNT(*) AS Total "
                        + "FROM Compras "
                        + "INNER JOIN Participantes ON Participantes.codigo = Compras.participante "
                        + "INNER JOIN Campanhas ON Campanhas.codigo = Compras.campanha "
                        + "LEFT JOIN Clinicas ON Clinicas.codigo = Compras.clinica "
                        + "LEFT JOIN TransacoesRealizadas On TransacoesRealizadas.compraTransacao = Compras.codigo"
                        + "{0} {1} {2}", filtro, ordenacao, top);
                    DataTable dt = new Data().getData(sql);
                    if (dt != null && dt.Rows.Count > 0)
                        retorno = Convert.ToInt32(dt.Rows[0]["Total"]);
                }
            }
            catch (ImunneVacinas.ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }

            return retorno;
        }

        /// <summary>
        /// Rotina que realiza a pesquisa de elementos mediante os parametros de pesquisa informados
        /// </summary>
        /// <param name="pesquisa">VALUE: elemento de pesquisa com os parametros para consulta</param>
        /// <returns></returns>
        public static decimal ConsultarTotalValorCompras(PesquisaCompra pesquisa)
        {
            decimal retorno = 0;

            StringBuilder ordenacao = new StringBuilder();
            StringBuilder filtro = new StringBuilder();
            StringBuilder top = new StringBuilder();
            MySqlParameter p;

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    #region Campo Código

                    if (pesquisa.Codigo >= 0)
                    {
                        filtro.Append(" AND Compras.codigo = @codigo");
                        p = new MySqlParameter("@codigo", pesquisa.Codigo);
                        p.MySqlDbType = GetInfoCodigo().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Participante

                    if (pesquisa.Participante >= 0)
                    {
                        filtro.Append(" AND Compras.participante = @participante");
                        p = new MySqlParameter("@participante", pesquisa.Participante);
                        p.MySqlDbType = GetInfoParticipante().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Empresa

                    if (pesquisa.Empresa > 0)
                    {
                        filtro.Append(" AND Campanhas.empresa = @empresa");
                        p = new MySqlParameter("@empresa", pesquisa.Empresa);
                        p.MySqlDbType = MySqlDbType.Int32;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Clinica

                    if (pesquisa.Clinica >= 0)
                    {
                        filtro.Append(" AND Compras.clinica = @clinica");
                        p = new MySqlParameter("@clinica", pesquisa.Clinica);
                        p.MySqlDbType = GetInfoClinica().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Campanha

                    if (pesquisa.Campanha >= 0)
                    {
                        filtro.Append(" AND Compras.campanha = @campanha");
                        p = new MySqlParameter("@campanha", pesquisa.Campanha);
                        p.MySqlDbType = GetInfoCampanha().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Situação

                    if (pesquisa.Situacao != SituacaoCompra.Indiferente)
                    {
                        filtro.Append(" AND Compras.situacao = @situacao");
                        p = new MySqlParameter("@situacao", (int)pesquisa.Situacao);
                        p.MySqlDbType = GetInfoSituacao().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Aplicação

                    if (pesquisa.Aplicacao != Aplicacao.Indiferente)
                    {
                        filtro.Append(" AND Compras.aplicacao = @aplicacao");
                        p = new MySqlParameter("@aplicacao", (int)pesquisa.Aplicacao);
                        p.MySqlDbType = GetInfoAplicacao().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Categoria

                    if (pesquisa.Categoria != CategoriaCompra.Indiferente)
                    {
                        filtro.Append(" AND Compras.categoria = @categoria");
                        p = new MySqlParameter("@categoria", (int)pesquisa.Categoria);
                        p.MySqlDbType = GetInfoCategoria().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campos UF e Cidade

                    if (!String.IsNullOrEmpty(pesquisa.Uf) && pesquisa.Uf != "-1")
                    {
                        string siglaEstado = String.Format("{0}", pesquisa.Uf);
                        filtro.Append(" AND Clinicas.estado = @estado ");
                        p = new MySqlParameter("@estado", siglaEstado);
                        p.MySqlDbType = MySqlDbType.VarChar;
                        sql.Parameters.Add(p);

                        if (pesquisa.Cidade >= 0)
                        {
                            filtro.Append(" AND Clinicas.cidade = @cidade");
                            p = new MySqlParameter("@cidade", pesquisa.Cidade);
                            p.MySqlDbType = MySqlDbType.Int32;
                            sql.Parameters.Add(p);
                        }
                    }

                    #endregion

                    #region Campos Data Compra

                    if (pesquisa.DataInicio != Utils.dataPadrao && pesquisa.DataFim != Utils.dataPadrao)
                    {
                        filtro.Append(" AND Compras.datacompra >= @inicio");
                        p = new MySqlParameter("@inicio", Parser.parseDateTimeBD(pesquisa.DataInicio))
                        {
                            MySqlDbType = GetInfoDataCompra().Type
                        };
                        sql.Parameters.Add(p);

                        filtro.Append(" AND Compras.datacompra <= @termino");
                        p = new MySqlParameter("@termino", Parser.parseDateTimeBD(pesquisa.DataFim))
                        {
                            MySqlDbType = GetInfoDataCompra().Type
                        };
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Livre

                    if (!String.IsNullOrEmpty(pesquisa.CampoLivre.Trim()))
                    {
                        string campoLivre = String.Format("%{0}%", pesquisa.CampoLivre.Trim());
                        filtro.Append(" AND (Participantes.nome LIKE @campoLivre ");
                        filtro.Append(" OR TransacoesRealizadas.numeroComprovanteVenda LIKE @campoLivre");
                        filtro.Append(" OR Participantes.cpf LIKE @campoLivre");
                        filtro.Append(")");
                        p = new MySqlParameter("@campoLivre", campoLivre)
                        {
                            MySqlDbType = MySqlDbType.VarChar
                        };
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Valor

                    if (pesquisa.Valor == true)
                        filtro.Append(" AND Compras.valor > 0");

                    #endregion

                    if (filtro.Length > 0)
                        filtro = filtro.Replace(" AND ", " WHERE ", 0, 5);

                    #region Ordenação

                    switch (pesquisa.Ordenacao)
                    {
                        case Utils.TipoOrdenacao.porCodigo:
                            {
                                ordenacao.Append("ORDER BY Compras.codigo");
                            }
                            break;
                        case Utils.TipoOrdenacao.porCodigoDesc:
                            {
                                ordenacao.Append("ORDER BY Compras.codigo DESC");
                            }
                            break;
                        case Utils.TipoOrdenacao.porData:
                            {
                                ordenacao.Append("ORDER BY Compras.datacompra, Campanhas.identificacao, Clinicas.nomefantasia");
                            }
                            break;
                        case Utils.TipoOrdenacao.porDataDesc:
                            {
                                ordenacao.Append("ORDER BY Compras.datacompra DESC, Campanhas.identificacao, Clinicas.nomefantasia");
                            }
                            break;
                        case Utils.TipoOrdenacao.randomico:
                            {
                                ordenacao.Append("ORDER BY RAND()");
                            }
                            break;
                        default:
                            {
                                ordenacao.Append("ORDER BY Compras.codigo");
                            }
                            break;
                    }

                    #endregion

                    sql.CommandText = String.Format("SELECT SUM(Compras.valor) AS Total "
                        + "FROM Compras "
                        + "INNER JOIN Participantes ON Participantes.codigo = Compras.participante "
                        + "INNER JOIN Campanhas ON Campanhas.codigo = Compras.campanha "
                        + "LEFT JOIN Clinicas ON Clinicas.codigo = Compras.clinica "
                        + "LEFT JOIN TransacoesRealizadas On TransacoesRealizadas.compraTransacao = Compras.codigo"
                        + "{0} {1} {2}", filtro, ordenacao, top);
                    DataTable dt = new Data().getData(sql);
                    if (dt != null && dt.Rows.Count > 0)
                        retorno = Convert.ToDecimal(dt.Rows[0]["Total"]);
                }
            }
            catch (ImunneVacinas.ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }

            return retorno;
        }

        /// <summary>
        /// Rotina interna utilizada para todas as pesquisas do elemento
        /// </summary>
        /// <param name="pesquisa">VALUE: Elemento com os parâmetros com os quais será realizada a pesquisa</param>
        /// <returns>Lista do elemento com os dados retornados da consulta</returns>
        public static int ContabilizarCompras(PesquisaCompra pesquisa)
        {
            int retorno = 0;

            StringBuilder filtro = new StringBuilder();
            MySqlParameter p;

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    #region Campo Código

                    if (pesquisa.Codigo >= 0)
                    {
                        filtro.Append(" AND Compras.codigo = @codigo");
                        p = new MySqlParameter("@codigo", pesquisa.Codigo);
                        p.MySqlDbType = GetInfoCodigo().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Liberados

                    if (pesquisa.Liberados > 0)
                    {
                        switch (pesquisa.Liberados)
                        {
                            // COMPRAS AINDA À LIBERAR
                            case 1:
                                {
                                    filtro.Append(" AND Compras.dataliberacao = @LIBERADOS");
                                    p = new MySqlParameter("@LIBERADOS", Parser.parseDateTimeBD(ImunneVacinas.Utils.dataPadrao));
                                    p.MySqlDbType = GetInfoDataLiberacao().Type;
                                    sql.Parameters.Add(p);
                                }
                                break;
                            // DEMAIS COMPRAS
                            default:
                                {
                                    filtro.Append(" AND Compras.dataliberacao > @LIBERADOS");
                                    p = new MySqlParameter("@LIBERADOS", Parser.parseDateTimeBD(ImunneVacinas.Utils.dataPadrao));
                                    p.MySqlDbType = GetInfoDataLiberacao().Type;
                                    sql.Parameters.Add(p);
                                }
                                break;
                        }
                    }

                    #endregion

                    #region Campo Participante

                    if (pesquisa.Participante >= 0)
                    {
                        filtro.Append(" AND Compras.participante = @participante");
                        p = new MySqlParameter("@participante", pesquisa.Participante);
                        p.MySqlDbType = GetInfoParticipante().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Empresa

                    if (pesquisa.Empresa > 0)
                    {
                        filtro.Append(" AND Campanhas.empresa = @empresa");
                        p = new MySqlParameter("@empresa", pesquisa.Empresa);
                        p.MySqlDbType = MySqlDbType.Int32;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Clinica

                    if (pesquisa.Clinica >= 0)
                    {
                        filtro.Append(" AND Compras.clinica = @clinica");
                        p = new MySqlParameter("@clinica", pesquisa.Clinica);
                        p.MySqlDbType = GetInfoClinica().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Campanha

                    if (pesquisa.Campanha >= 0)
                    {
                        filtro.Append(" AND Compras.campanha = @campanha");
                        p = new MySqlParameter("@campanha", pesquisa.Campanha);
                        p.MySqlDbType = GetInfoCampanha().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Situação

                    if (pesquisa.Situacao != SituacaoCompra.Indiferente)
                    {
                        filtro.Append(" AND Compras.situacao = @situacao");
                        p = new MySqlParameter("@situacao", (int)pesquisa.Situacao);
                        p.MySqlDbType = GetInfoSituacao().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Aplicação

                    if (pesquisa.Aplicacao != Aplicacao.Indiferente)
                    {
                        filtro.Append(" AND Compras.aplicacao = @aplicacao");
                        p = new MySqlParameter("@aplicacao", (int)pesquisa.Aplicacao);
                        p.MySqlDbType = GetInfoAplicacao().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Categoria

                    if (pesquisa.Categoria != CategoriaCompra.Indiferente)
                    {
                        filtro.Append(" AND Compras.categoria = @categoria");
                        p = new MySqlParameter("@categoria", (int)pesquisa.Categoria);
                        p.MySqlDbType = GetInfoCategoria().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campos UF e Cidade

                    if (!String.IsNullOrEmpty(pesquisa.Uf) && pesquisa.Uf != "-1")
                    {
                        string siglaEstado = String.Format("{0}", pesquisa.Uf);
                        filtro.Append(" AND Clinicas.estado = @estado ");
                        p = new MySqlParameter("@estado", siglaEstado);
                        p.MySqlDbType = MySqlDbType.VarChar;
                        sql.Parameters.Add(p);

                        if (pesquisa.Cidade >= 0)
                        {
                            filtro.Append(" AND Clinicas.cidade = @cidade");
                            p = new MySqlParameter("@cidade", pesquisa.Cidade);
                            p.MySqlDbType = MySqlDbType.Int32;
                            sql.Parameters.Add(p);
                        }
                    }

                    #endregion

                    #region Campos Data Compra

                    if (pesquisa.DataInicio != Utils.dataPadrao && pesquisa.DataFim != Utils.dataPadrao)
                    {
                        filtro.Append(" AND Compras.datacompra >= @inicio");
                        p = new MySqlParameter("@inicio", Parser.parseDateTimeBD(pesquisa.DataInicio))
                        {
                            MySqlDbType = GetInfoDataCompra().Type
                        };
                        sql.Parameters.Add(p);

                        filtro.Append(" AND Compras.datacompra <= @termino");
                        p = new MySqlParameter("@termino", Parser.parseDateTimeBD(pesquisa.DataFim))
                        {
                            MySqlDbType = GetInfoDataCompra().Type
                        };
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Livre

                    if (!String.IsNullOrEmpty(pesquisa.CampoLivre.Trim()))
                    {
                        string campoLivre = String.Format("%{0}%", pesquisa.CampoLivre.Trim());
                        filtro.Append(" AND (Participantes.nome LIKE @campoLivre ");
                        filtro.Append(" OR TransacoesRealizadas.numeroComprovanteVenda LIKE @campoLivre");
                        filtro.Append(" OR TransacoesRealizadas.numeroCartao LIKE @campoLivre");
                        filtro.Append(" OR Participantes.cpf LIKE @campoLivre");
                        filtro.Append(")");
                        p = new MySqlParameter("@campoLivre", campoLivre)
                        {
                            MySqlDbType = MySqlDbType.VarChar
                        };
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Valor

                    if (pesquisa.Valor == true)
                        filtro.Append(" AND Compras.valor > 0");

                    #endregion

                    if (filtro.Length > 0)
                        filtro = filtro.Replace(" AND ", " WHERE ", 0, 5);

                    sql.CommandText = String.Format("SELECT "
                        + "COUNT (Compras.codigo) AS Total "
                        + "FROM Compras "
                        + "INNER JOIN Participantes ON Participantes.codigo = Compras.participante "
                        + "INNER JOIN Campanhas ON Campanhas.codigo = Compras.campanha "
                        + "LEFT JOIN Clinicas ON Clinicas.codigo = Compras.clinica "
                        + "LEFT JOIN TransacoesRealizadas On TransacoesRealizadas.compraTransacao = Compras.codigo "
                        + "LEFT JOIN Cidades ON Cidades.codigo_cidade = Clinicas.cidade "
                        + "LEFT JOIN Empresas ON Empresas.codigo = Campanhas.empresa"
                        + "{0}", filtro);
                    DataTable dt = new Data().getData(sql);
                    if (dt != null && dt.Rows.Count > 0)
                        retorno = Convert.ToInt32(dt.Rows[0]["Total"]);

                    return retorno;
                }
            }
            catch (ImunneVacinas.ImunneVacinasDataException)
            {
                return 0;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        /// <summary>
        /// Rotina interna utilizada para todas as pesquisas do elemento
        /// </summary>
        /// <param name="pesquisa">VALUE: Elemento com os parâmetros com os quais será realizada a pesquisa</param>
        /// <returns>Lista do elemento com os dados retornados da consulta</returns>
        internal static List<Compra> ConsultaGenerica(PesquisaCompra pesquisa)
        {
            StringBuilder ordenacao = new StringBuilder();
            StringBuilder filtro = new StringBuilder();
            StringBuilder top = new StringBuilder();
            MySqlParameter p;

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    if (pesquisa.Quantidade > 0)
                        top.AppendFormat(" LIMIT {0} ", pesquisa.Quantidade);

                    #region Campo Código

                    if (pesquisa.Codigo >= 0)
                    {
                        filtro.Append(" AND Compras.codigo = @codigo");
                        p = new MySqlParameter("@codigo", pesquisa.Codigo);
                        p.MySqlDbType = GetInfoCodigo().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Liberados

                    if (pesquisa.Liberados > 0)
                    {
                        switch (pesquisa.Liberados)
                        {
                            // COMPRAS AINDA À LIBERAR
                            case 1:
                                {
                                    filtro.Append(" AND Compras.dataliberacao = @LIBERADOS");
                                    p = new MySqlParameter("@LIBERADOS", Parser.parseDateTimeBD(ImunneVacinas.Utils.dataPadrao));
                                    p.MySqlDbType = GetInfoDataLiberacao().Type;
                                    sql.Parameters.Add(p);
                                } break;
                            // DEMAIS COMPRAS
                            default:
                                {
                                    filtro.Append(" AND Compras.dataliberacao > @LIBERADOS");
                                    p = new MySqlParameter("@LIBERADOS", Parser.parseDateTimeBD(ImunneVacinas.Utils.dataPadrao));
                                    p.MySqlDbType = GetInfoDataLiberacao().Type;
                                    sql.Parameters.Add(p);
                                } break;
                        }
                    }

                    #endregion

                    #region Campo Participante

                    if (pesquisa.Participante >= 0)
                    {
                        filtro.Append(" AND Compras.participante = @participante");
                        p = new MySqlParameter("@participante", pesquisa.Participante);
                        p.MySqlDbType = GetInfoParticipante().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Empresa

                    if (pesquisa.Empresa > 0)
                    {
                        filtro.Append(" AND Campanhas.empresa = @empresa");
                        p = new MySqlParameter("@empresa", pesquisa.Empresa);
                        p.MySqlDbType = MySqlDbType.Int32;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Clinica

                    if (pesquisa.Clinica >= 0)
                    {
                        filtro.Append(" AND Compras.clinica = @clinica");
                        p = new MySqlParameter("@clinica", pesquisa.Clinica);
                        p.MySqlDbType = GetInfoClinica().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Campanha

                    if (pesquisa.Campanha >= 0)
                    {
                        filtro.Append(" AND Compras.campanha = @campanha");
                        p = new MySqlParameter("@campanha", pesquisa.Campanha);
                        p.MySqlDbType = GetInfoCampanha().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Situação

                    if (pesquisa.Situacao != SituacaoCompra.Indiferente)
                    {
                        filtro.Append(" AND Compras.situacao = @situacao");
                        p = new MySqlParameter("@situacao", (int)pesquisa.Situacao);
                        p.MySqlDbType = GetInfoSituacao().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Aplicação

                    if (pesquisa.Aplicacao != Aplicacao.Indiferente)
                    {
                        filtro.Append(" AND Compras.aplicacao = @aplicacao");
                        p = new MySqlParameter("@aplicacao", (int)pesquisa.Aplicacao);
                        p.MySqlDbType = GetInfoAplicacao().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Categoria

                    if (pesquisa.Categoria != CategoriaCompra.Indiferente)
                    {
                        filtro.Append(" AND Compras.categoria = @categoria");
                        p = new MySqlParameter("@categoria", (int)pesquisa.Categoria);
                        p.MySqlDbType = GetInfoCategoria().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campos UF e Cidade

                    if (!String.IsNullOrEmpty(pesquisa.Uf) && pesquisa.Uf != "-1")
                    {
                        string siglaEstado = String.Format("{0}", pesquisa.Uf);
                        filtro.Append(" AND Clinicas.estado = @estado ");
                        p = new MySqlParameter("@estado", siglaEstado);
                        p.MySqlDbType = MySqlDbType.VarChar;
                        sql.Parameters.Add(p);

                        if (pesquisa.Cidade >= 0)
                        {
                            filtro.Append(" AND Clinicas.cidade = @cidade");
                            p = new MySqlParameter("@cidade", pesquisa.Cidade);
                            p.MySqlDbType = MySqlDbType.Int32;
                            sql.Parameters.Add(p);
                        }
                    }

                    #endregion

                    #region Campos Data Compra

                    if (pesquisa.DataInicio != Utils.dataPadrao && pesquisa.DataFim != Utils.dataPadrao)
                    {
                        filtro.Append(" AND Compras.datacompra >= @inicio");
                        p = new MySqlParameter("@inicio", Parser.parseDateTimeBD(pesquisa.DataInicio))
                        {
                            MySqlDbType = GetInfoDataCompra().Type
                        };
                        sql.Parameters.Add(p);

                        filtro.Append(" AND Compras.datacompra <= @termino");
                        p = new MySqlParameter("@termino", Parser.parseDateTimeBD(pesquisa.DataFim))
                        {
                            MySqlDbType = GetInfoDataCompra().Type
                        };
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Livre

                    if (!String.IsNullOrEmpty(pesquisa.CampoLivre.Trim()))
                    {
                        string campoLivre = String.Format("%{0}%", pesquisa.CampoLivre.Trim());
                        filtro.Append(" AND (Participantes.nome LIKE @campoLivre ");
                        filtro.Append(" OR TransacoesRealizadas.numeroComprovanteVenda LIKE @campoLivre");
                        filtro.Append(" OR TransacoesRealizadas.numeroCartao LIKE @campoLivre");
                        filtro.Append(" OR Participantes.cpf LIKE @campoLivre");
                        filtro.Append(")");
                        p = new MySqlParameter("@campoLivre", campoLivre)
                        {
                            MySqlDbType = MySqlDbType.VarChar
                        };
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Valor

                    if (pesquisa.Valor == true)
                        filtro.Append(" AND Compras.valor > 0");

                    #endregion

                    if (filtro.Length > 0)
                        filtro = filtro.Replace(" AND ", " WHERE ", 0, 5);

                    #region Ordenação

                    switch (pesquisa.Ordenacao)
                    {
                        case Utils.TipoOrdenacao.porCodigo:
                            {
                                ordenacao.Append("ORDER BY Compras.codigo");
                            }
                            break;
                        case Utils.TipoOrdenacao.porCodigoDesc:
                            {
                                ordenacao.Append("ORDER BY Compras.codigo DESC");
                            }
                            break;
                        case Utils.TipoOrdenacao.porData:
                            {
                                ordenacao.Append("ORDER BY Compras.datacompra, Campanhas.identificacao, Clinicas.nomefantasia");
                            }
                            break;
                        case Utils.TipoOrdenacao.porDataDesc:
                            {
                                ordenacao.Append("ORDER BY Compras.datacompra DESC, Campanhas.identificacao, Clinicas.nomefantasia");
                            }
                            break;
                        case Utils.TipoOrdenacao.randomico:
                            {
                                ordenacao.Append("ORDER BY RAND()");
                            }
                            break;
                        default:
                            {
                                ordenacao.Append("ORDER BY Compras.codigo");
                            }
                            break;
                    }

                    #endregion

                    sql.CommandText = String.Format("SELECT "
                        + "Participantes.codigo AS codigoParticipante, Participantes.nome AS nomeParticipante,"
                        + "Participantes.cpf AS cpfParticipante, "
                        + "Clinicas.codigo AS codigoClinica, Clinicas.nomefantasia AS nomeClinica, "
                        + "Campanhas.codigo AS codigoCampanha, Campanhas.nome AS nomeCampanha, Campanhas.identificacao AS idCampanha, "
                        + "Empresas.nomefantasia AS nomeEmpresa, Participantes.email AS emailParticipante, Participantes.telefone AS telefoneParticipante, "
                        + "Participantes.datanascimento AS nascimentoParticipante, Campanhas.datainicio AS aberturaCampanha, "
                        + "Clinicas.estado AS ufClinica, Cidades.nome_cidade AS cidadeClinica, TransacoesRealizadas.numeroComprovanteVenda AS comprovanteCompra, "
                        + "Compras.* "
                        + "FROM Compras "
                        + "INNER JOIN Participantes ON Participantes.codigo = Compras.participante "
                        + "INNER JOIN Campanhas ON Campanhas.codigo = Compras.campanha "
                        + "LEFT JOIN Clinicas ON Clinicas.codigo = Compras.clinica "
                        + "LEFT JOIN TransacoesRealizadas On TransacoesRealizadas.compraTransacao = Compras.codigo "
                        + "LEFT JOIN Cidades ON Cidades.codigo_cidade = Clinicas.cidade "
                        + "LEFT JOIN Empresas ON Empresas.codigo = Campanhas.empresa"
                        + "{0} {1} {2}", filtro, ordenacao, top);
                    DataTable dt = new Data().getData(sql);
                    return MontarRetorno(dt);
                }
            }
            catch (ImunneVacinas.ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Rotina que retorna uma lista genérica do elemento, de acordo com o retorno da Consultar ao BD
        /// </summary>
        /// <param name="dt">VALUE: DataTable com o resultado da Consultar ao BD</param>
        /// <returns>Lista do elemento com os dados retornados da consulta</returns>
        internal static List<Compra> MontarRetorno(DataTable dt)
        {
            int i = -1;
            List<Compra> list = new List<Compra>();

            if (dt == null && dt.Rows.Count == 0)
                return list;
            else
            {
                Compra retorno;
                try
                {
                    for (i = 0; i < dt.Rows.Count; i++)
                    {
                        retorno = new Compra()
                        {
                            _codigo = Parser.parseInteiro(dt.Rows[i]["codigo"]),
                            _parcelas = Parser.parseInteiro(dt.Rows[i]["parcelas"]),
                            _valor = Parser.parseDecimal(dt.Rows[i]["valor"]),
                            _dataAplicacao = Parser.parseDateTime(dt.Rows[i]["dataaplicacao"]),
                            _dataLiberacao = Parser.parseDateTime(dt.Rows[i]["dataliberacao"]),
                            _numeroComprovante = ImunneVacinas.Parser.parseString(dt.Rows[i]["comprovanteCompra"]),
                            _aplicacaoCompra = (ImunneVacinas.Compra.Aplicacao)Enum.Parse(typeof(ImunneVacinas.Compra.Aplicacao), dt.Rows[i]["aplicacao"].ToString()),
                            _situacao = (ImunneVacinas.Compra.SituacaoCompra)Enum.Parse(typeof(ImunneVacinas.Compra.SituacaoCompra), dt.Rows[i]["situacao"].ToString()),
                            _categoria = (ImunneVacinas.Compra.CategoriaCompra)Enum.Parse(typeof(ImunneVacinas.Compra.CategoriaCompra), dt.Rows[i]["categoria"].ToString()),
                            _dataCompra = Convert.ToDateTime(dt.Rows[i]["datacompra"])
                        };

                        retorno._idParticipante.Codigo = ImunneVacinas.Parser.parseInteiro(dt.Rows[i]["codigoParticipante"]);
                        retorno._idParticipante.Cpf = ImunneVacinas.Parser.parseString(dt.Rows[i]["cpfParticipante"]);
                        retorno._idParticipante.Nome = ImunneVacinas.Parser.parseString(dt.Rows[i]["nomeParticipante"]);
                        retorno._idParticipante.Email = ImunneVacinas.Parser.parseString(dt.Rows[i]["emailParticipante"]);
                        retorno._idParticipante.Telefone = ImunneVacinas.Parser.parseString(dt.Rows[i]["telefoneParticipante"]);
                        retorno._idParticipante.DataNascimento = Convert.ToDateTime(dt.Rows[i]["nascimentoParticipante"]);

                        retorno._idClinica.Codigo = ImunneVacinas.Parser.parseInteiro(dt.Rows[i]["codigoClinica"]);
                        retorno._idClinica.NomeFantasia = ImunneVacinas.Parser.parseString(dt.Rows[i]["nomeClinica"]);
                        retorno._idClinica.IdCidade.Nome = ImunneVacinas.Parser.parseString(dt.Rows[i]["cidadeClinica"]);
                        retorno._idClinica.IdUf.Sigla = ImunneVacinas.Parser.parseString(dt.Rows[i]["ufClinica"]);

                        retorno._idCampanha.Codigo = ImunneVacinas.Parser.parseInteiro(dt.Rows[i]["codigoCampanha"]);
                        retorno._idCampanha.Nome = ImunneVacinas.Parser.parseString(dt.Rows[i]["nomeCampanha"]);
                        retorno._idCampanha.Identificacao = ImunneVacinas.Parser.parseString(dt.Rows[i]["idCampanha"]);
                        retorno._idCampanha.DataAbertura = Convert.ToDateTime(dt.Rows[i]["aberturaCampanha"]);
                        retorno._idCampanha.IdEmpresa.NomeFantasia = ImunneVacinas.Parser.parseString(dt.Rows[i]["nomeEmpresa"]);

                        list.Add(retorno);
                    }
                    return list;
                }
                catch (Exception ex)
                {
                    ex.Data.Add("último retorno lido", i);
                    throw;
                }
            }
        }

        /// <summary>
        /// Verifica as características da COMPRA e retorna se a mesma pode ou não ser excluída
        /// </summary>
        /// <param name="compra">VALUE: Elemento COMPRA a ser tratado</param>
        /// <returns></returns>
        public static bool VerificarExclucao(ImunneVacinas.Compra compra)
        {
            bool retorno = true;

            try
            {
                // COMPRAS APLICADAS NÃO PODEM SER EXCLUÍDAS
                if (compra.AplicacaoCompra == Aplicacao.Aplicado)
                    retorno = false;

                // COMPRAS PAGAS VIA CARTÃO NÃO PODEM SER EXCLUÍDAS
                if (compra.Categoria == CategoriaCompra.Paga)
                    retorno = false;

                return retorno;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        #region EXPORTAÇÃO EXCEL

        /// <summary>
        /// Rotina interna utilizada para todas as pesquisas do elemento
        /// </summary>
        /// <param name="pesquisa">VALUE: Elemento com os parâmetros com os quais será realizada a pesquisa</param>
        /// <returns>Lista do elemento com os dados retornados da consulta</returns>
        public static List<ListagemCompra> ConsultaComprasListagem(PesquisaCompra pesquisa)
        {
            StringBuilder ordenacao = new StringBuilder();
            StringBuilder filtro = new StringBuilder();
            StringBuilder top = new StringBuilder();
            MySqlParameter p;

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    if (pesquisa.Quantidade > 0)
                        top.AppendFormat(" LIMIT {0} ", pesquisa.Quantidade);

                    #region Campo Código

                    if (pesquisa.Codigo >= 0)
                    {
                        filtro.Append(" AND Compras.codigo = @codigo");
                        p = new MySqlParameter("@codigo", pesquisa.Codigo);
                        p.MySqlDbType = GetInfoCodigo().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Liberados

                    if (pesquisa.Liberados > 0)
                    {
                        switch (pesquisa.Liberados)
                        {
                            // COMPRAS AINDA À LIBERAR
                            case 1:
                                {
                                    filtro.Append(" AND Compras.dataliberacao = @LIBERADOS");
                                    p = new MySqlParameter("@LIBERADOS", Parser.parseDateTimeBD(ImunneVacinas.Utils.dataPadrao));
                                    p.MySqlDbType = MySqlDbType.Int32;
                                    sql.Parameters.Add(p);
                                }
                                break;
                            // DEMAIS COMPRAS
                            default:
                                {
                                    filtro.Append(" AND Compras.dataliberacao > @LIBERADOS");
                                    p = new MySqlParameter("@LIBERADOS", Parser.parseDateTimeBD(ImunneVacinas.Utils.dataPadrao));
                                    p.MySqlDbType = MySqlDbType.Int32;
                                    sql.Parameters.Add(p);
                                }
                                break;
                        }
                    }

                    #endregion

                    #region Campo Participante

                    if (pesquisa.Participante >= 0)
                    {
                        filtro.Append(" AND Compras.participante = @participante");
                        p = new MySqlParameter("@participante", pesquisa.Participante);
                        p.MySqlDbType = MySqlDbType.Int32;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Empresa

                    if (pesquisa.Empresa > 0)
                    {
                        filtro.Append(" AND Campanhas.empresa = @empresa");
                        p = new MySqlParameter("@empresa", pesquisa.Empresa);
                        p.MySqlDbType = MySqlDbType.Int32;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Clinica

                    if (pesquisa.Clinica >= 0)
                    {
                        filtro.Append(" AND Compras.clinica = @clinica");
                        p = new MySqlParameter("@clinica", pesquisa.Clinica);
                        p.MySqlDbType = MySqlDbType.Int32;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Campanha

                    if (pesquisa.Campanha >= 0)
                    {
                        filtro.Append(" AND Compras.campanha = @campanha");
                        p = new MySqlParameter("@campanha", pesquisa.Campanha);
                        p.MySqlDbType = MySqlDbType.Int32;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Situação

                    if (pesquisa.Situacao != Compra.SituacaoCompra.Indiferente)
                    {
                        filtro.Append(" AND Compras.situacao = @situacao");
                        p = new MySqlParameter("@situacao", (int)pesquisa.Situacao);
                        p.MySqlDbType = MySqlDbType.Int32;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Aplicação

                    if (pesquisa.Aplicacao != Compra.Aplicacao.Indiferente)
                    {
                        filtro.Append(" AND Compras.aplicacao = @aplicacao");
                        p = new MySqlParameter("@aplicacao", (int)pesquisa.Aplicacao);
                        p.MySqlDbType = MySqlDbType.Int32;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Categoria

                    if (pesquisa.Categoria != Compra.CategoriaCompra.Indiferente)
                    {
                        filtro.Append(" AND Compras.categoria = @categoria");
                        p = new MySqlParameter("@categoria", (int)pesquisa.Categoria);
                        p.MySqlDbType = MySqlDbType.Int32;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campos UF e Cidade

                    if (!String.IsNullOrEmpty(pesquisa.Uf) && pesquisa.Uf != "-1")
                    {
                        string siglaEstado = String.Format("{0}", pesquisa.Uf);
                        filtro.Append(" AND Clinicas.estado = @estado ");
                        p = new MySqlParameter("@estado", siglaEstado);
                        p.MySqlDbType = MySqlDbType.VarChar;
                        sql.Parameters.Add(p);

                        if (pesquisa.Cidade >= 0)
                        {
                            filtro.Append(" AND Clinicas.cidade = @cidade");
                            p = new MySqlParameter("@cidade", pesquisa.Cidade);
                            p.MySqlDbType = MySqlDbType.Int32;
                            sql.Parameters.Add(p);
                        }
                    }

                    #endregion

                    #region Campos Data Compra

                    if (pesquisa.DataInicio != Utils.dataPadrao && pesquisa.DataFim != Utils.dataPadrao)
                    {
                        filtro.Append(" AND Compras.datacompra >= @inicio");
                        p = new MySqlParameter("@inicio", Parser.parseDateTimeBD(pesquisa.DataInicio))
                        {
                            MySqlDbType = MySqlDbType.DateTime
                        };
                        sql.Parameters.Add(p);

                        filtro.Append(" AND Compras.datacompra <= @termino");
                        p = new MySqlParameter("@termino", Parser.parseDateTimeBD(pesquisa.DataFim))
                        {
                            MySqlDbType = MySqlDbType.DateTime
                        };
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Livre

                    if (!String.IsNullOrEmpty(pesquisa.CampoLivre.Trim()))
                    {
                        string campoLivre = String.Format("%{0}%", pesquisa.CampoLivre.Trim());
                        filtro.Append(" AND (Participantes.nome LIKE @campoLivre ");
                        filtro.Append(" OR TransacoesRealizadas.numeroComprovanteVenda LIKE @campoLivre");
                        filtro.Append(" OR TransacoesRealizadas.numeroCartao LIKE @campoLivre");
                        filtro.Append(" OR Participantes.cpf LIKE @campoLivre");
                        filtro.Append(")");
                        p = new MySqlParameter("@campoLivre", campoLivre)
                        {
                            MySqlDbType = MySqlDbType.VarChar
                        };
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Valor

                    if (pesquisa.Valor == true)
                        filtro.Append(" AND Compras.valor > 0");

                    #endregion

                    if (filtro.Length > 0)
                        filtro = filtro.Replace(" AND ", " WHERE ", 0, 5);

                    #region Ordenação

                    switch (pesquisa.Ordenacao)
                    {
                        case Utils.TipoOrdenacao.porCodigo:
                            {
                                ordenacao.Append("ORDER BY Compras.codigo");
                            }
                            break;
                        case Utils.TipoOrdenacao.porCodigoDesc:
                            {
                                ordenacao.Append("ORDER BY Compras.codigo DESC");
                            }
                            break;
                        case Utils.TipoOrdenacao.porData:
                            {
                                ordenacao.Append("ORDER BY Compras.datacompra, Campanhas.identificacao, Clinicas.nomefantasia");
                            }
                            break;
                        case Utils.TipoOrdenacao.porDataDesc:
                            {
                                ordenacao.Append("ORDER BY Compras.datacompra DESC, Campanhas.identificacao, Clinicas.nomefantasia");
                            }
                            break;
                        case Utils.TipoOrdenacao.randomico:
                            {
                                ordenacao.Append("ORDER BY RAND()");
                            }
                            break;
                        default:
                            {
                                ordenacao.Append("ORDER BY Compras.codigo");
                            }
                            break;
                    }

                    #endregion

                    sql.CommandText = String.Format("SELECT "
                        + "Campanhas.identificacao AS Campanha, Compras.codigo AS Pedido, Empresas.nomefantasia AS Empresa, "
                        + "TransacoesRealizadas.numeroComprovanteVenda AS Comprovante, Compras.datacompra AS DataCompra, Participantes.nome AS Titular, "
                        + "Participantes.cpf AS CpfCnpj, Participantes.email AS Email, Participantes.telefone AS Telefone, "
                        + "Clinicas.nomefantasia AS Clinica, Cidades.nome_cidade AS Cidade, Cidades.sigla_estado AS UF, "
                        + "Compras.situacao AS Situacao, Compras.categoria AS Categoria, Compras.dataaplicacao AS DataAplicacao, "
                        + "Compras.parcelas AS Parcelas, Compras.valor AS ValorCompra, "
                        + "(SELECT COUNT(*) FROM Vouchers WHERE Vouchers.compra = Compras.codigo AND Vouchers.situacao = 3) AS VouchersUtilizados, "
                        + "(SELECT COUNT(*) FROM Vouchers WHERE Vouchers.compra = Compras.codigo AND Vouchers.situacao IN(1, 2)) AS VouchersDemais, "
                        + "(SELECT SUM(ItensCompras.valor) FROM ItensCompras WHERE ItensCompras.compra = Compras.codigo) AS ValorItens "
                        + "FROM Compras "
                        + "INNER JOIN Campanhas ON Campanhas.codigo = Compras.campanha "
                        + "INNER JOIN Empresas ON Empresas.codigo = Campanhas.empresa "
                        + "INNER JOIN Clinicas ON Clinicas.codigo = Compras.clinica "
                        + "INNER JOIN Cidades ON Cidades.codigo_cidade = Clinicas.cidade "
                        + "INNER JOIN TransacoesRealizadas ON TransacoesRealizadas.compraTransacao = Compras.codigo "
                        + "INNER JOIN Participantes ON Participantes.codigo = Compras.participante"
                        + "{0} {1} {2}", filtro, ordenacao, top);
                    DataTable dt = new Data().getData(sql);
                    return MontarRetornoListagem(dt);
                }
            }
            catch (ImunneVacinas.ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Método interno que transforma o DATATABLE da consulta ao BD em um LIST do elemento
        /// </summary>
        /// <param name="dt">VALUE: DATATABLE com os dados consultados</param>
        /// <returns></returns>
        internal static List<ListagemCompra> MontarRetornoListagem(DataTable dt)
        {
            int i = -1;
            List<ListagemCompra> list = new List<ListagemCompra>();

            if (dt == null && dt.Rows.Count == 0)
                return list;
            else
            {
                ListagemCompra retorno;
                try
                {
                    for (i = 0; i < dt.Rows.Count; i++)
                    {
                        retorno = new ListagemCompra();
                        retorno.Campanha = ImunneVacinas.Parser.parseString(dt.Rows[i]["Campanha"]);
                        retorno.Empresa = ImunneVacinas.Parser.parseString(dt.Rows[i]["Empresa"]);
                        retorno.Pedido = ImunneVacinas.Parser.parseInteiro(dt.Rows[i]["Pedido"]);
                        retorno.Comprovante = ImunneVacinas.Parser.parseString(dt.Rows[i]["Comprovante"]);
                        retorno.Titular = ImunneVacinas.Parser.parseString(dt.Rows[i]["Titular"]);
                        retorno.CpfCnpj = ImunneVacinas.Parser.parseString(dt.Rows[i]["CpfCnpj"]);
                        retorno.Email = ImunneVacinas.Parser.parseString(dt.Rows[i]["Email"]);
                        retorno.Telefone = ImunneVacinas.Parser.parseString(dt.Rows[i]["Telefone"]);
                        retorno.Clinica = ImunneVacinas.Parser.parseString(dt.Rows[i]["Clinica"]);
                        retorno.Cidade = ImunneVacinas.Parser.parseString(dt.Rows[i]["Cidade"]);
                        retorno.UF = ImunneVacinas.Parser.parseString(dt.Rows[i]["UF"]);
                        retorno.Parcelas = ImunneVacinas.Parser.parseInteiro(dt.Rows[i]["Parcelas"]);
                        retorno.Valor = ImunneVacinas.Parser.parseDecimal(dt.Rows[i]["ValorCompra"]);
                        retorno.ValorItens = ImunneVacinas.Parser.parseDecimal(dt.Rows[i]["ValorItens"]);

                        #region SITUAÇÃO

                        Compra.SituacaoCompra situacaoItem = (ImunneVacinas.Compra.SituacaoCompra)Enum.Parse(typeof(ImunneVacinas.Compra.SituacaoCompra), dt.Rows[i]["Situacao"].ToString());
                        switch (situacaoItem)
                        {
                            case SituacaoCompra.Aberta:
                                {
                                    retorno.Situacao = "NÃO REALIZADA";
                                }
                                break;
                            case SituacaoCompra.Cancelada:
                                {
                                    retorno.Situacao = "NÃO AUTORIZADA";
                                }
                                break;
                            case SituacaoCompra.Concluida:
                                {
                                    retorno.Situacao = "AUTORIZADA";
                                }
                                break;
                        }

                        #endregion

                        #region CATEGORIA

                        Compra.CategoriaCompra categoriaItem = (ImunneVacinas.Compra.CategoriaCompra)Enum.Parse(typeof(ImunneVacinas.Compra.CategoriaCompra), dt.Rows[i]["Categoria"].ToString());
                        switch (categoriaItem)
                        {
                            case CategoriaCompra.Importada:
                                {
                                    retorno.Categoria = "IMPORTADA";
                                } break;
                            case CategoriaCompra.Paga:
                                {
                                    retorno.Categoria = "PAGA";
                                }
                                break;
                            case CategoriaCompra.Gratis:
                                {
                                    retorno.Categoria = "GRÁTIS";
                                }
                                break;
                        }

                        #endregion

                        retorno.VouchersUtilizados = ImunneVacinas.Parser.parseInteiro(dt.Rows[i]["VouchersUtilizados"]);
                        retorno.VouchersDemais = ImunneVacinas.Parser.parseInteiro(dt.Rows[i]["VouchersDemais"]);

                        // TRATAMENTO DAS DATAS
                        if (!String.IsNullOrEmpty(dt.Rows[i]["DataCompra"].ToString()) && Convert.ToDateTime(dt.Rows[i]["DataCompra"].ToString()) != ImunneVacinas.Utils.dataPadrao)
                            retorno.DataCompra = Convert.ToDateTime(dt.Rows[i]["DataCompra"]).ToString("dd/MM/yyyy HH:mm");
                        else retorno.DataCompra = "--";

                        if (!String.IsNullOrEmpty(dt.Rows[i]["DataAplicacao"].ToString()) && Convert.ToDateTime(dt.Rows[i]["DataAplicacao"].ToString()) != ImunneVacinas.Utils.dataPadrao)
                            retorno.DataAplicacao = Convert.ToDateTime(dt.Rows[i]["DataAplicacao"]).ToString("dd/MM/yyyy HH:mm");
                        else retorno.DataAplicacao = "NÃO APLICADO";

                        list.Add(retorno);
                    }
                    return list;
                }
                catch (Exception ex)
                {
                    ex.Data.Add("último retorno lido", i);
                    throw;
                }
            }
        }

        #endregion
    }
}