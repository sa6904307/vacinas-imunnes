﻿using MySql.Data.MySqlClient;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace ImunneVacinas
{
    /// <summary>
    /// Informações, rotinas e propriedades do elemento TipoUsuario
    /// </summary>
    [Serializable]
    public class TipoUsuario
    {
        private const string _tabela = "TiposUsuarios";
        private static List<BD.Columns> columns;
        private static DateTime columnsUpdated;

        #region Declaração das propriedades do elemento

        private int _codigo;
        private string _descricao;
        private Utils.SituacaoRegistro _situacao;
        private DateTime _dataCriacao;
        private DateTime _dataAlteracao;

        /// <summary>
        /// Campo: codigo
        /// </summary>
        public int Codigo
        {
            get { return _codigo; }
            set { _codigo = value; }
        }

        /// <summary>
        /// Campo: descricao
        /// </summary>
        public string Descricao
        {
            get { return _descricao; }
            set { _descricao = value; }
        }

        /// <summary>
        /// Campo: situacao
        /// </summary>
        public Utils.SituacaoRegistro Situacao
        {
            get { return _situacao; }
            set { _situacao = value; }
        }

        /// <summary>
        /// Campo: dataCriacao
        /// </summary>
        public DateTime DataCriacao
        {
            get { return _dataCriacao; }
            set { _dataCriacao = value; }
        }

        /// <summary>
        /// Campo: dataAlteracao
        /// </summary>
        public DateTime DataAlteracao
        {
            get { return _dataAlteracao; }
            set { _dataAlteracao = value; }
        }

        #endregion

        #region GetInfo de todas as propriedades

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos os campos da tabela.
        /// </summary>
        private static void GetInfoTabela()
        {
            if (columns == null || (DateTime.Now - columnsUpdated).Minutes > 0)
            {
                columns = BD.Columns.getColumnsInfo(_tabela);
                columnsUpdated = DateTime.Now;
            }
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo codigo.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoCodigo()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "codigo");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo descricao.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoDescricao()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "descricao");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo situacao.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoSituacao()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "situacao");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo dataCriacao.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoDataCriacao()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "dataCriacao");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo dataAlteracao.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoDataAlteracao()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "dataAlteracao");
        }

        #endregion

        /// <summary> 
        /// Rotina que cria um Array com os parâmetros utilizados nas ações de INSERT, UPDATE e DELETE
        /// <returns>Array com vários MySqlParameters</returns>
        /// </summary>
        private Array GetParametrosSQL()
        {
            MySqlParameter p;
            ArrayList ap = new ArrayList();

            p = new MySqlParameter("@codigo", Parser.parseInteiro(_codigo));
            p.MySqlDbType = GetInfoCodigo().Type;
            ap.Add(p);

            p = new MySqlParameter("@descricao", Parser.parseString(_descricao));
            p.MySqlDbType = GetInfoDescricao().Type;
            ap.Add(p);

            p = new MySqlParameter("@situacao", (int)_situacao);
            p.MySqlDbType = GetInfoSituacao().Type;
            ap.Add(p);

            p = new MySqlParameter("@dataCriacao", Parser.parseDateTimeBD(_dataCriacao));
            p.MySqlDbType = GetInfoDataCriacao().Type;
            ap.Add(p);

            p = new MySqlParameter("@dataAlteracao", Parser.parseDateTimeBD(_dataAlteracao));
            p.MySqlDbType = GetInfoDataAlteracao().Type;
            ap.Add(p);

            return ap.ToArray();
        }

        /// <summary>
        /// Inicialização das propriedades do elemento Usuario com seus valores padrão
        /// </summary>
        private void IniciarElemento()
        {
            GetInfoTabela();

            _dataAlteracao = Utils.dataPadrao;
            _dataCriacao = Utils.dataPadrao;
            _situacao = Utils.SituacaoRegistro.Ativo;
        }

        /// <summary>
        /// Inicialização do elemento TipoUsuario
        /// </summary>
        public TipoUsuario()
        {
            IniciarElemento();
        }

        /// <summary>
        /// Rotina que insere o registro no BD
        /// </summary>
        /// <returns>Código do registro inserido no BD</returns>
        public int Incluir()
        {
            if (String.IsNullOrEmpty(_descricao))
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    sql.Parameters.AddRange(GetParametrosSQL());
                    sql.CommandText = "INSERT INTO TiposUsuarios "
                                    + "(descricao, dataCriacao, dataAlteracao, situacao)  "
                                    + "VALUE (@descricao, @dataCriacao, @dataAlteracao, @situacao); "
                                    + "SELECT LAST_INSERT_ID();";
                    int resultado = Parser.parseInteiro(new Data().executeEscalar(sql));
                    return resultado;
                }
            }
            catch (ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Rotina que altera os dados do registro no BD
        /// </summary>
        /// <returns>Quantidade de linhas alteradas na ação junto ao BD</returns>
        public int Alterar()
        {
            if (_codigo < 0)
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    sql.Parameters.AddRange(GetParametrosSQL());
                    sql.CommandText = "UPDATE TiposUsuarios "
                                    + "SET "
                                    + "descricao = @descricao, dataAlteracao = @dataAlteracao, "
                                    + "situacao = @situacao "
                                    + "WHERE codigo = @codigo";
                    int resultado = Parser.parseInteiro(new Data().execute(sql));
                    return resultado;
                }
            }
            catch (ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Rotina que exclui o registro do BD
        /// </summary>
        /// <returns>Quantidade de linhas alteradas na ação junto ao BD</returns>
        public int Excluir()
        {
            if (_codigo < 0)
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    sql.Parameters.AddRange(GetParametrosSQL());
                    sql.CommandText = "DELETE FROM TiposUsuarios "
                                    + "WHERE codigo = @codigo";
                    int resultado = Parser.parseInteiro(new Data().execute(sql));
                    return resultado;
                }
            }
            catch (ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Rotina que verificar se já existe um registro (que não este) com o nome informado
        /// </summary>
        /// <param name="codigoAtual">VALUE: Código do usuário atual (obrigatório)</param>
        /// <param name="nome">VALUE: Nome do registro (obrigatório)</param>
        /// <returns></returns>
        public static bool ConsultarNome(int codigoAtual, string nome)
        {
            bool verificacaoNome = false;

            if (String.IsNullOrEmpty(nome))
                return verificacaoNome;

            ImunneVacinas.TipoUsuario registroSelecionado = ConsultarUnico(nome);
            if (registroSelecionado != null)
            {
                if (codigoAtual != registroSelecionado.Codigo)
                    verificacaoNome = true;
            }

            return verificacaoNome;
        }

        /// <summary>
        /// Rotina que localiza e retorna um único registro, mediante o código informado
        /// </summary>
        /// <param name="codigo">VALUE: código de identificação do registro (obrigatório)</param>
        /// <returns>Elemento Usuario com os dados retornados da consulta</returns>
        public static TipoUsuario ConsultarUnico(int codigo)
        {
            //if (codigo <= 0)
            //    throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            ImunneVacinas.PesquisaTipoUsuario pesquisa = new PesquisaTipoUsuario();
            pesquisa.Codigo = codigo;

            List<TipoUsuario> retorno = ConsultaGenerica(pesquisa);
            if (retorno == null || retorno.Count.Equals(0))
                return null;
            else
                return retorno[0];
        }

        /// <summary>
        /// Rotina que localiza e retorna um único registro, mediante o código informado
        /// </summary>
        /// <param name="nome">VALUE: nome do registro (obrigatório)</param>
        /// <returns>Elemento Usuario com os dados retornados da consulta</returns>
        public static TipoUsuario ConsultarUnico(string nome)
        {
            //if (codigo <= 0)
            //    throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            ImunneVacinas.PesquisaTipoUsuario pesquisa = new PesquisaTipoUsuario()
            {
                Nome = nome
            };

            List<TipoUsuario> retorno = ConsultaGenerica(pesquisa);
            if (retorno == null || retorno.Count.Equals(0))
                return null;
            else
                return retorno[0];
        }

        /// <summary>
        /// Rotina que realiza a pesquisa de N elementos TipoUsuario
        /// </summary>
        /// <param name="pesquisa">VALUE: Elemento com os parâmetros com os quais será realizada a pesquisa</param>
        /// <returns>Lista do elemento Usuario com os dados retornados da consulta</returns>
        public static List<TipoUsuario> ConsultarTiposUsuarios(PesquisaTipoUsuario pesquisa)
        {
            return ConsultaGenerica(pesquisa);
        }

        /// <summary>
        /// Rotina que retorna um elemento RetornoCBX para utilização em DropLists
        /// </summary>
        /// <param name="pesquisa">VALUE: Elemento com os parâmetros de pesquisa</param>
        /// <param name="InserirLinhaPadrao">VALUE: Informa se será adiconada a linha padrão</param>
        /// <param name="textoPadrao">VALUE: Informa o texto utilizado para linha padão (se utilizado NULL, será apresentado SELECIONE)</param>
        /// <returns>Lista do elemento RetornoCBX com os dados desejados</returns>
        public static List<RetornoComboBox> ConsultarComboBox(PesquisaTipoUsuario pesquisa, bool InserirLinhaPadrao, string textoPadrao)
        {
            DataTable dt = new DataTable();
            try
            {
                dt.Columns.Add("DisplayMember");
                dt.Columns.Add("ValueMember");

                List<TipoUsuario> retorno = ConsultaGenerica(pesquisa);
                if (retorno != null)
                {
                    foreach (TipoUsuario c in retorno)
                    {
                        dt.Rows.Add(new object[] { c.Descricao.ToUpper(), c.Codigo });
                    }
                }
                return Utils.MontarRetornoComboBox(dt, InserirLinhaPadrao, textoPadrao);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// Rotina interna utilizada para todas as pesquisas do elemento TipoUsuario
        /// </summary>
        /// <param name="pesquisa">VALUE: Elemento com os parâmetros com os quais será realizada a pesquisa</param>
        /// <returns>Lista do elemento Usuario com os dados retornados da consulta</returns>
        internal static List<TipoUsuario> ConsultaGenerica(PesquisaTipoUsuario pesquisa)
        {
            StringBuilder ordenacao = new StringBuilder();
            StringBuilder filtro = new StringBuilder();
            StringBuilder top = new StringBuilder();
            MySqlParameter p;

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    if (pesquisa.Quantidade > 0)
                        top.AppendFormat(" LIMIT {0} ", pesquisa.Quantidade);

                    #region Campo Código

                    if (pesquisa.Codigo >= 0)
                    {
                        filtro.Append(" AND TiposUsuarios.codigo = @codigo");
                        p = new MySqlParameter("@codigo", pesquisa.Codigo);
                        p.MySqlDbType = GetInfoCodigo().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Nome

                    if (!String.IsNullOrEmpty(pesquisa.Nome))
                    {
                        string campoLivre = String.Format("{0}", pesquisa.Nome);
                        filtro.Append(" AND TiposUsuarios.descricao LIKE @campoLivre ");
                        p = new MySqlParameter("@campoLivre", campoLivre);
                        p.MySqlDbType = MySqlDbType.VarChar;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Livre

                    if (!String.IsNullOrEmpty(pesquisa.CampoLivre.Trim()))
                    {
                        string campoLivre = String.Format("%{0}%", pesquisa.CampoLivre.Trim());
                        filtro.Append(" AND (TiposUsuarios.descricao LIKE @campoLivre ");
                        filtro.Append(")");
                        p = new MySqlParameter("@campoLivre", campoLivre);
                        p.MySqlDbType = MySqlDbType.VarChar;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Situação

                    if (pesquisa.Situacao != Utils.SituacaoRegistro.Indiferente)
                    {
                        filtro.Append(" AND TiposUsuarios.situacao = @situacao");
                        p = new MySqlParameter("@situacao", (int)pesquisa.Situacao);
                        p.MySqlDbType = GetInfoSituacao().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    if (filtro.Length > 0)
                        filtro = filtro.Replace(" AND ", " WHERE ", 0, 5);

                    #region Ordenação

                    switch (pesquisa.Ordenacao)
                    {
                        case Utils.TipoOrdenacao.porCodigo:
                            {
                                ordenacao.Append("ORDER BY TiposUsuarios.codigo");
                            }
                            break;
                        case Utils.TipoOrdenacao.porCodigoDesc:
                            {
                                ordenacao.Append("ORDER BY TiposUsuarios.codigo DESC");
                            }
                            break;
                        case Utils.TipoOrdenacao.porNome:
                            {
                                ordenacao.Append("ORDER BY TiposUsuarios.descricao");
                            }
                            break;
                        case Utils.TipoOrdenacao.porNomeDesc:
                            {
                                ordenacao.Append("ORDER BY TiposUsuarios.descricao DESC");
                            }
                            break;
                        case Utils.TipoOrdenacao.porData:
                            {
                                ordenacao.Append("ORDER BY TiposUsuarios.dataCriacao");
                            }
                            break;
                        case Utils.TipoOrdenacao.porDataDesc:
                            {
                                ordenacao.Append("ORDER BY TiposUsuarios.dataCriacao DESC");
                            }
                            break;
                        case Utils.TipoOrdenacao.randomico:
                            {
                                ordenacao.Append("ORDER BY RAND()");
                            }
                            break;
                        default:
                            {
                                ordenacao.Append("ORDER BY TiposUsuarios.codigo");
                            }
                            break;
                    }

                    #endregion

                    sql.CommandText = String.Format("SELECT "
                        + "TiposUsuarios.* "
                        + "FROM TiposUsuarios "
                        + "{0} {1} {2}", filtro, ordenacao, top);
                    DataTable dt = new Data().getData(sql);
                    return MontarRetorno(dt);
                }
            }
            catch (ImunneVacinas.ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Rotina que retorna uma lista genérica do elemento TipoUsuario, de acordo com o retorno da Consultar ao BD
        /// </summary>
        /// <param name="dt">VALUE: DataTable com o resultado da Consultar ao BD</param>
        /// <returns>Lista do elemento Usuario com os dados retornados da consulta</returns>
        internal static List<TipoUsuario> MontarRetorno(DataTable dt)
        {
            int i = -1;
            List<TipoUsuario> list = new List<TipoUsuario>();

            if (dt == null && dt.Rows.Count == 0)
                return list;
            else
            {
                TipoUsuario retorno;
                try
                {
                    for (i = 0; i < dt.Rows.Count; i++)
                    {
                        retorno = new TipoUsuario();
                        retorno._codigo = Parser.parseInteiro(dt.Rows[i]["codigo"]);
                        retorno._descricao = Parser.parseString(dt.Rows[i]["descricao"]);
                        retorno._situacao = (Utils.SituacaoRegistro)Enum.Parse(typeof(Utils.SituacaoRegistro), dt.Rows[i]["situacao"].ToString());
                        retorno._dataCriacao = Convert.ToDateTime(dt.Rows[i]["dataCriacao"]);

                        if (!String.IsNullOrEmpty(dt.Rows[i]["dataAlteracao"].ToString()))
                            retorno._dataAlteracao = Convert.ToDateTime(dt.Rows[i]["dataAlteracao"]);

                        list.Add(retorno);
                    }
                    return list;
                }
                catch (Exception ex)
                {
                    ex.Data.Add("último retorno lido", i);
                    throw;
                }
            }
        }
    }
}