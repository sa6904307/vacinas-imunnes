﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descrição resumida de PaymentReturn
/// </summary>
public class PaymentReturn
{
    public MessageResponse message_response { get; set; }
    public DataResponse data_response { get; set; }

    public class MessageResponse
    {
        public string message { get; set; }
    }

    public class DataResponse
    {
        public Transaction transaction { get; set; }
    }

    public class Transaction
    {
        public string order_number { get; set; }
        public string free { get; set; }
        public int transaction_id { get; set; }
        public string status_name { get; set; }
        public int status_id { get; set; }
        public string token_transaction { get; set; }
        public string max_days_to_keep_waiting_payment { get; set; }
        public Payment payment { get; set; }
        public Customer customer { get; set; }
    }
    public class Payment
    {
        public string price_payment { get; set; }
        public string price_original { get; set; }
        public string payment_response { get; set; }
        public string payment_response_code { get; set; }
        public string url_payment { get; set; }
        public string qrcode_path { get; set; }
        public string qrcode_original_path { get; set; }
        public string tid { get; set; }
        public int split { get; set; }
        public int payment_method_id { get; set; }
        public string payment_method_name { get; set; }
        public object linha_digitavel { get; set; }
        public string card_token { get; set; }
    }

    public class Customer
    {
        public string name { get; set; }
        public string company_name { get; set; }
        public string trade_name { get; set; }
        public string cnpj { get; set; }
    }
}