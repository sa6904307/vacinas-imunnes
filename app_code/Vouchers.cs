﻿using MySql.Data.MySqlClient;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace ImunneVacinas
{
    /// <summary>
    /// Descrição resumida de Vouchers
    /// </summary>
    [Serializable]
    public class Voucher
    {
        /// <summary>
        /// 
        /// </summary>
        public enum SituacaoVoucher
        {
            Indiferente = -1,
            Gerado = 1,
            Impresso = 2,
            Utilizado = 3
        }

        private static string _tabela = "Vouchers";
        private static List<BD.Columns> columns;
        private static DateTime columnsUpdated;

        #region Propriedades do elemento

        private Compra _idCompra;
        private Campanha _idCampanha;
        private Participante _idParticipante;
        private Clinica _idClinica;
        private int _codigo;
        private DateTime _dataCriacao;
        private DateTime _dataImpressao;
        private string _descricao;
        private SituacaoVoucher _situacao;
        private DateTime _dataRetirada;

        public Compra IdCompra
        {
            get
            {
                return _idCompra;
            }

            set
            {
                _idCompra = value;
            }
        }

        public Campanha IdCampanha
        {
            get
            {
                return _idCampanha;
            }

            set
            {
                _idCampanha = value;
            }
        }

        public Participante IdParticipante
        {
            get
            {
                return _idParticipante;
            }

            set
            {
                _idParticipante = value;
            }
        }

        public Clinica IdClinica
        {
            get
            {
                return _idClinica;
            }

            set
            {
                _idClinica = value;
            }
        }

        public int Codigo
        {
            get
            {
                return _codigo;
            }

            set
            {
                _codigo = value;
            }
        }

        public DateTime DataCriacao
        {
            get
            {
                return _dataCriacao;
            }

            set
            {
                _dataCriacao = value;
            }
        }

        public DateTime DataImpressao
        {
            get
            {
                return _dataImpressao;
            }

            set
            {
                _dataImpressao = value;
            }
        }

        public string Descricao
        {
            get
            {
                return _descricao;
            }

            set
            {
                _descricao = value;
            }
        }

        public SituacaoVoucher Situacao
        {
            get
            {
                return _situacao;
            }

            set
            {
                _situacao = value;
            }
        }

        public DateTime DataRetirada
        {
            get
            {
                return _dataRetirada;
            }

            set
            {
                _dataRetirada = value;
            }
        }

        #endregion

        #region GetInfos

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos os campos da tabela.
        /// </summary>
        private static void GetInfoTabela()
        {
            if (columns == null || (DateTime.Now - columnsUpdated).Minutes > 0)
            {
                columns = BD.Columns.getColumnsInfo(_tabela);
                columnsUpdated = DateTime.Now;
            }
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo compra.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoCompra()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "compra");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo participante.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoParticipante()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "participante");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo campanha.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoCampanha()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "campanha");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo clinica.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoClinica()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "clinica");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo codigo.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoCodigo()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "codigo");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo datacriacao.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoDataCriacao()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "datacriacao");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo dataimpressao.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoDataImpressao()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "dataimpressao");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo descricao.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoDescricao()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "descricao");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo situacao.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoSituacao()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "situacao");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo dataretirada.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoDataRetirada()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "dataretirada");
        }

        #endregion

        /// <summary> 
        /// Rotina que cria um Array com os parâmetros utilizados nas ações de INSERT, UPDATE e DELETE
        /// <returns>Array com vários MySqlParameters</returns>
        /// </summary>
        private Array GetParametrosSQL()
        {
            MySqlParameter p;
            ArrayList ap = new ArrayList();

            p = new MySqlParameter("@compra", Parser.parseInteiro(_idCompra.Codigo));
            p.MySqlDbType = GetInfoCompra().Type;
            ap.Add(p);

            p = new MySqlParameter("@participante", Parser.parseInteiro(_idParticipante.Codigo));
            p.MySqlDbType = GetInfoParticipante().Type;
            ap.Add(p);

            p = new MySqlParameter("@campanha", Parser.parseInteiro(_idCampanha.Codigo));
            p.MySqlDbType = GetInfoCampanha().Type;
            ap.Add(p);

            p = new MySqlParameter("@clinica", Parser.parseInteiro(_idClinica.Codigo));
            p.MySqlDbType = GetInfoCodigo().Type;
            ap.Add(p);

            p = new MySqlParameter("@codigo", Parser.parseInteiro(_codigo));
            p.MySqlDbType = GetInfoCodigo().Type;
            ap.Add(p);

            p = new MySqlParameter("@datacriacao", Parser.parseDateTimeBD(_dataCriacao));
            p.MySqlDbType = GetInfoDataCriacao().Type;
            ap.Add(p);

            p = new MySqlParameter("@dataimpressao", Parser.parseDateTimeBD(_dataImpressao));
            p.MySqlDbType = GetInfoDataImpressao().Type;
            ap.Add(p);

            p = new MySqlParameter("@dataretirada", Parser.parseDateTimeBD(_dataRetirada));
            p.MySqlDbType = GetInfoDataRetirada().Type;
            ap.Add(p);

            p = new MySqlParameter("@situacao", (int)_situacao);
            p.MySqlDbType = GetInfoSituacao().Type;
            ap.Add(p);

            p = new MySqlParameter("@descricao", Parser.parseString(_descricao));
            p.MySqlDbType = GetInfoDescricao().Type;
            ap.Add(p);

            return ap.ToArray();
        }

        /// <summary>
        /// Inicialização das propriedades do elemento com seus valores padrão
        /// </summary>
        private void IniciarElemento()
        {
            GetInfoTabela();

            _idCampanha = new Campanha();
            _idClinica = new Clinica();
            _idCompra = new Compra();
            _idParticipante = new Participante();

            _idCampanha.Codigo = -1;
            _idClinica.Codigo = -1;
            _idCompra.Codigo = -1;
            _idParticipante.Codigo = -1;
            _dataCriacao = Utils.dataPadrao;
            _dataImpressao = Utils.dataPadrao;
            _dataRetirada = Utils.dataPadrao;
            _descricao = String.Empty;
            _situacao = SituacaoVoucher.Indiferente;
        }

        /// <summary>
        /// Inicialização do elemento
        /// </summary>
        public Voucher()
        {
            IniciarElemento();
        }

        /// <summary>
        /// Rotina que insere o registro no BD
        /// </summary>
        /// <returns>Código do registro inserido no BD</returns>
        public int Incluir()
        {
            if (_idCampanha.Codigo <= 0 || _idClinica.Codigo <= 0 || _idCompra.Codigo <= 0 || _idParticipante.Codigo <= 0)
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    sql.Parameters.AddRange(GetParametrosSQL());
                    sql.CommandText = "INSERT INTO Vouchers "
                                    + "(compra, campanha, participante, "
                                    + "clinica, datacriacao, dataimpressao, "
                                    + "descricao, situacao, dataretirada) "
                                    + "VALUE( "
                                    + "@compra, @campanha, @participante, "
                                    + "@clinica, @datacriacao, @dataimpressao, "
                                    + "@descricao, @situacao, @dataretirada); "
                                    + "SELECT LAST_INSERT_ID();";
                    int resultado = Parser.parseInteiro(new Data().executeEscalar(sql));
                    return resultado;
                }
            }
            catch (ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Rotina que altera os dados do registro no BD
        /// </summary>
        /// <returns>Quantidade de linhas alteradas na ação junto ao BD</returns>
        public int Alterar()
        {
            if (_codigo < 0)
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    sql.Parameters.AddRange(GetParametrosSQL());
                    sql.CommandText = "UPDATE Vouchers "
                                    + "SET "
                                    + "dataimpressao = @dataimpressao, descricao = @descricao, "
                                    + "situacao = @situacao, dataretirada = @dataretirada "
                                    + "WHERE codigo = @codigo; ";
                    int resultado = Parser.parseInteiro(new Data().execute(sql));
                    return resultado;
                }
            }
            catch (ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Rotina que exclui o registro do BD
        /// </summary>
        /// <returns>Quantidade de linhas alteradas na ação junto ao BD</returns>
        public int Excluir()
        {
            if (_codigo < 0)
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    sql.Parameters.AddRange(GetParametrosSQL());
                    sql.CommandText = "DELETE FROM Vouchers "
                                    + "WHERE codigo = @codigo";
                    int resultado = Parser.parseInteiro(new Data().execute(sql));
                    return resultado;
                }
            }
            catch (ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Rotina que localiza e retorna um único registro, mediante o código informado
        /// </summary>
        /// <param name="codigo">VALUE: código de identificação do registro (obrigatório)</param>
        /// <returns>Elemento Usuario com os dados retornados da consulta</returns>
        public static Voucher ConsultarUnico(int codigo)
        {
            if (codigo <= 0)
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            ImunneVacinas.PesquisaVoucher pesquisa = new PesquisaVoucher()
            {
                Codigo = codigo
            };

            List<Voucher> retorno = ConsultaGenerica(pesquisa);
            if (retorno == null || retorno.Count.Equals(0))
                return null;
            else
                return retorno[0];
        }

        /// <summary>
        /// Rotina que localiza e retorna um único registro, mediante o código informado
        /// </summary>
        /// <param name="codigo">VALUE: código de identificação do registro (obrigatório)</param>
        /// <returns>Elemento Usuario com os dados retornados da consulta</returns>
        public static Voucher ConsultarUnico(int participante, int clinica, int campanha, int compra)
        {
            if (participante <= 0 || clinica <= 0 || campanha <= 0 || compra <= 0)
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            ImunneVacinas.PesquisaVoucher pesquisa = new PesquisaVoucher()
            {
                Compra = compra,
                Clinica = clinica,
                Campanha = campanha,
                Participante = participante
            };

            List<Voucher> retorno = ConsultaGenerica(pesquisa);
            if (retorno == null || retorno.Count.Equals(0))
                return null;
            else
                return retorno[0];
        }

        /// <summary>
        /// Rotina que realiza a pesquisa de elementos mediante os parametros de pesquisa informados
        /// </summary>
        /// <param name="pesquisa">VALUE: elemento de pesquisa com os parametros para consulta</param>
        /// <returns></returns>
        public static List<Voucher> ConsultarVouchers(PesquisaVoucher pesquisa)
        {
            return ConsultaGenerica(pesquisa);
        }

        /// <summary>
        /// Rotina que contabiliza o total de vouchers, de acordo com a pesquisa
        /// </summary>
        /// <param name="pesquisa">VALUE: Elemento com os parâmetros com os quais será realizada a pesquisa</param>
        /// <returns>Lista do elemento com os dados retornados da consulta</returns>
        public static int ContabilizarVouchers(PesquisaVoucher pesquisa)
        {
            int retorno = 0;
            StringBuilder filtro = new StringBuilder();
            MySqlParameter p;

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    #region Campo Código

                    if (pesquisa.Codigo >= 0)
                    {
                        filtro.Append(" AND Vouchers.codigo = @codigo");
                        p = new MySqlParameter("@codigo", pesquisa.Codigo);
                        p.MySqlDbType = GetInfoCodigo().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Participante

                    if (pesquisa.Participante >= 0)
                    {
                        filtro.Append(" AND Vouchers.participante = @participante");
                        p = new MySqlParameter("@participante", pesquisa.Participante);
                        p.MySqlDbType = GetInfoParticipante().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Clinica

                    if (pesquisa.Clinica >= 0)
                    {
                        filtro.Append(" AND Vouchers.clinica = @clinica");
                        p = new MySqlParameter("@clinica", pesquisa.Clinica);
                        p.MySqlDbType = GetInfoClinica().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Campanha

                    if (pesquisa.Campanha >= 0)
                    {
                        filtro.Append(" AND Vouchers.campanha = @campanha");
                        p = new MySqlParameter("@campanha", pesquisa.Campanha);
                        p.MySqlDbType = GetInfoCampanha().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Compra

                    if (pesquisa.Compra >= 0)
                    {
                        filtro.Append(" AND Vouchers.compra = @compra");
                        p = new MySqlParameter("@compra", pesquisa.Compra);
                        p.MySqlDbType = GetInfoCompra().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Situação

                    if (pesquisa.Situacao != SituacaoVoucher.Indiferente)
                    {
                        filtro.Append(" AND Vouchers.situacao = @situacao");
                        p = new MySqlParameter("@situacao", (int)pesquisa.Situacao);
                        p.MySqlDbType = GetInfoSituacao().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    if (filtro.Length > 0)
                        filtro = filtro.Replace(" AND ", " WHERE ", 0, 5);

                    sql.CommandText = String.Format("SELECT "
                        + "COUNT(Vouchers.codigo) AS Total "
                        + "FROM Vouchers "
                        + "INNER JOIN Participantes ON Participantes.codigo = Vouchers.participante "
                        + "INNER JOIN Clinicas ON Clinicas.codigo = Vouchers.clinica "
                        + "INNER JOIN Compras ON Compras.codigo = Vouchers.compra "
                        + "INNER JOIN Campanhas ON Campanhas.codigo = Vouchers.campanha "
                        + "{0}", filtro);
                    DataTable dt = new Data().getData(sql);
                    if (dt != null && dt.Rows.Count > 0)
                        retorno = Convert.ToInt32(dt.Rows[0]["Total"]);

                    return retorno;
                }
            }
            catch (ImunneVacinas.ImunneVacinasDataException)
            {
                return 0;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        /// <summary>
        /// Rotina interna utilizada para todas as pesquisas do elemento
        /// </summary>
        /// <param name="pesquisa">VALUE: Elemento com os parâmetros com os quais será realizada a pesquisa</param>
        /// <returns>Lista do elemento com os dados retornados da consulta</returns>
        internal static List<Voucher> ConsultaGenerica(PesquisaVoucher pesquisa)
        {
            StringBuilder ordenacao = new StringBuilder();
            StringBuilder filtro = new StringBuilder();
            StringBuilder top = new StringBuilder();
            MySqlParameter p;

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    if (pesquisa.Quantidade > 0)
                        top.AppendFormat(" LIMIT {0} ", pesquisa.Quantidade);

                    #region Campo Código

                    if (pesquisa.Codigo >= 0)
                    {
                        filtro.Append(" AND Vouchers.codigo = @codigo");
                        p = new MySqlParameter("@codigo", pesquisa.Codigo);
                        p.MySqlDbType = GetInfoCodigo().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Participante

                    if (pesquisa.Participante >= 0)
                    {
                        filtro.Append(" AND Vouchers.participante = @participante");
                        p = new MySqlParameter("@participante", pesquisa.Participante);
                        p.MySqlDbType = GetInfoParticipante().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Clinica

                    if (pesquisa.Clinica >= 0)
                    {
                        filtro.Append(" AND Vouchers.clinica = @clinica");
                        p = new MySqlParameter("@clinica", pesquisa.Clinica);
                        p.MySqlDbType = GetInfoClinica().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Campanha

                    if (pesquisa.Campanha >= 0)
                    {
                        filtro.Append(" AND Vouchers.campanha = @campanha");
                        p = new MySqlParameter("@campanha", pesquisa.Campanha);
                        p.MySqlDbType = GetInfoCampanha().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Compra

                    if (pesquisa.Compra >= 0)
                    {
                        filtro.Append(" AND Vouchers.compra = @compra");
                        p = new MySqlParameter("@compra", pesquisa.Compra);
                        p.MySqlDbType = GetInfoCompra().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Situação

                    if (pesquisa.Situacao != SituacaoVoucher.Indiferente)
                    {
                        filtro.Append(" AND Vouchers.situacao = @situacao");
                        p = new MySqlParameter("@situacao", (int)pesquisa.Situacao);
                        p.MySqlDbType = GetInfoSituacao().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    if (filtro.Length > 0)
                        filtro = filtro.Replace(" AND ", " WHERE ", 0, 5);

                    #region Ordenação

                    switch (pesquisa.Ordenacao)
                    {
                        case Utils.TipoOrdenacao.porCodigo:
                            {
                                ordenacao.Append("ORDER BY Vouchers.codigo");
                            }
                            break;
                        case Utils.TipoOrdenacao.porCodigoDesc:
                            {
                                ordenacao.Append("ORDER BY Vouchers.codigo DESC");
                            }
                            break;
                        case Utils.TipoOrdenacao.porData:
                            {
                                ordenacao.Append("ORDER BY Vouchers.datacriacao");
                            }
                            break;
                        case Utils.TipoOrdenacao.porDataDesc:
                            {
                                ordenacao.Append("ORDER BY Vouchers.datacriacao DESC");
                            }
                            break;
                        case Utils.TipoOrdenacao.randomico:
                            {
                                ordenacao.Append("ORDER BY RAND()");
                            }
                            break;
                        default:
                            {
                                ordenacao.Append("ORDER BY Vouchers.codigo");
                            }
                            break;
                    }

                    #endregion

                    sql.CommandText = String.Format("SELECT "
                        + "Participantes.codigo AS codigoParticipante, Participantes.nome AS nomeParticipante, "
                        + "Clinicas.codigo AS codigoClinica, Clinicas.nomefantasia AS nomeClinica, "
                        + "Campanhas.codigo AS codigoCampanha, Campanhas.nome AS nomeCampanha, "
                        + "Compras.codigo AS codigoCompra, "
                        + "Vouchers.* "
                        + "FROM Vouchers "
                        + "INNER JOIN Participantes ON Participantes.codigo = Vouchers.participante "
                        + "INNER JOIN Clinicas ON Clinicas.codigo = Vouchers.clinica "
                        + "INNER JOIN Compras ON Compras.codigo = Vouchers.compra "
                        + "INNER JOIN Campanhas ON Campanhas.codigo = Vouchers.campanha "
                        + "{0} {1} {2}", filtro, ordenacao, top);
                    DataTable dt = new Data().getData(sql);
                    return MontarRetorno(dt);
                }
            }
            catch (ImunneVacinas.ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Rotina que retorna uma lista genérica do elemento, de acordo com o retorno da Consultar ao BD
        /// </summary>
        /// <param name="dt">VALUE: DataTable com o resultado da Consultar ao BD</param>
        /// <returns>Lista do elemento com os dados retornados da consulta</returns>
        internal static List<Voucher> MontarRetorno(DataTable dt)
        {
            int i = -1;
            List<Voucher> list = new List<Voucher>();

            if (dt == null && dt.Rows.Count == 0)
                return list;
            else
            {
                Voucher retorno;
                try
                {
                    for (i = 0; i < dt.Rows.Count; i++)
                    {
                        retorno = new Voucher()
                        {
                            _codigo = Parser.parseInteiro(dt.Rows[i]["codigo"]),
                            _descricao = Parser.parseString(dt.Rows[i]["descricao"]),
                            _situacao = (ImunneVacinas.Voucher.SituacaoVoucher)Enum.Parse(typeof(ImunneVacinas.Voucher.SituacaoVoucher), dt.Rows[i]["situacao"].ToString()),
                            _dataCriacao = Convert.ToDateTime(dt.Rows[i]["datacriacao"])
                        };

                        retorno._idCampanha.Codigo = ImunneVacinas.Parser.parseInteiro(dt.Rows[i]["codigoCampanha"]);
                        retorno._idCampanha.Nome = ImunneVacinas.Parser.parseString(dt.Rows[i]["nomeCampanha"]);

                        retorno._idClinica.Codigo = ImunneVacinas.Parser.parseInteiro(dt.Rows[i]["codigoClinica"]);
                        retorno._idClinica.NomeFantasia = ImunneVacinas.Parser.parseString(dt.Rows[i]["nomeClinica"]);

                        retorno._idCompra.Codigo = ImunneVacinas.Parser.parseInteiro(dt.Rows[i]["codigoCompra"]);

                        retorno._idParticipante.Codigo = ImunneVacinas.Parser.parseInteiro(dt.Rows[i]["codigoParticipante"]);
                        retorno._idParticipante.Nome = ImunneVacinas.Parser.parseString(dt.Rows[i]["nomeParticipante"]);

                        if (!String.IsNullOrEmpty(dt.Rows[i]["dataimpressao"].ToString()))
                            retorno._dataImpressao = Convert.ToDateTime(dt.Rows[i]["dataimpressao"]);

                        if (!String.IsNullOrEmpty(dt.Rows[i]["dataretirada"].ToString()))
                            retorno._dataRetirada = Convert.ToDateTime(dt.Rows[i]["dataretirada"]);

                        list.Add(retorno);
                    }
                    return list;
                }
                catch (Exception ex)
                {
                    ex.Data.Add("último retorno lido", i);
                    throw;
                }
            }
        }
    }
}