﻿using MySql.Data.MySqlClient;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace ImunneVacinas
{
    /// <summary>
    /// Informações, rotinas e propriedades do elemento Usuario
    /// </summary>
    [Serializable]
    public class Usuario
    {
        private const string _tabela = "Usuarios";
        private static List<BD.Columns> columns;
        private static DateTime columnsUpdated;

        #region Declaração das propriedades do elemento

        private int _codigo;
        private TipoUsuario _tipo;
        private string _nome;
        private string _email;
        private string _login;
        private string _senha;
        private string _dica;
        private Utils.SituacaoRegistro _situacao;
        private DateTime _dataCriacao;
        private DateTime _dataAlteracao;
        private int _clinica;

        /// <summary>
        /// Campo: codigo
        /// </summary>
        public int Codigo
        {
            get { return _codigo; }
            set { _codigo = value; }
        }

        /// <summary>
        /// Campo: tipo
        /// </summary>
        public TipoUsuario Tipo
        {
            get { return _tipo; }
            set { _tipo = value; }
        }

        /// <summary>
        /// Campo: nome
        /// </summary>
        public string Nome
        {
            get { return _nome; }
            set { _nome = value; }
        }

        /// <summary>
        /// Campo: email
        /// </summary>
        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }

        /// <summary>
        /// Campo: login
        /// </summary>
        public string Login
        {
            get { return _login; }
            set { _login = value; }
        }

        /// <summary>
        /// Campo: dica
        /// </summary>
        public string Senha
        {
            get { return _senha; }
            set { _senha = value; }
        }

        /// <summary>
        /// Campo: dica
        /// </summary>
        public string Dica
        {
            get { return _dica; }
            set { _dica = value; }
        }

        /// <summary>
        /// Campo: situacao
        /// </summary>
        public Utils.SituacaoRegistro Situacao
        {
            get { return _situacao; }
            set { _situacao = value; }
        }

        /// <summary>
        /// Campo: datacriacao
        /// </summary>
        public DateTime DataCriacao
        {
            get { return _dataCriacao; }
            set { _dataCriacao = value; }
        }

        /// <summary>
        /// Campo: dataalteracao
        /// </summary>
        public DateTime DataAlteracao
        {
            get { return _dataAlteracao; }
            set { _dataAlteracao = value; }
        }

        /// <summary>
        /// Campo: clinica
        /// </summary>
        public int Clinica
        {
            get { return _clinica; }
            set { _clinica = value; }
        }

        #endregion

        #region GetInfo de todas as propriedades

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos os campos da tabela.
        /// </summary>
        private static void GetInfoTabela()
        {
            if (columns == null || (DateTime.Now - columnsUpdated).Minutes > 0)
            {
                columns = BD.Columns.getColumnsInfo(_tabela);
                columnsUpdated = DateTime.Now;
            }
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo codigo.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoCodigo()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "codigo");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo tipo.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoTipo()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "tipo");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo nome.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoNome()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "nome");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo login.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoLogin()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "login");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo email.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoEmail()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "email");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo senha.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoSenha()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "senha");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo dica.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoDica()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "dica");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo situacao.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoSituacao()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "situacao");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo datacriacao.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoDataCriacao()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "datacriacao");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo dataalteracao.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoDataAlteracao()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "dataalteracao");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo clinica.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoClinica()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "clinica");
        }

        #endregion

        /// <summary> 
        /// Rotina que cria um Array com os parâmetros utilizados nas ações de INSERT, UPDATE e DELETE
        /// <returns>Array com vários MySqlParameters</returns>
        /// </summary>
        private Array GetParametrosSQL()
        {
            MySqlParameter p;
            ArrayList ap = new ArrayList();

            p = new MySqlParameter("@codigo", Parser.parseInteiro(_codigo))
            {
                MySqlDbType = GetInfoCodigo().Type
            };
            ap.Add(p);

            p = new MySqlParameter("@tipo", Parser.parseInteiro(_tipo.Codigo))
            {
                MySqlDbType = GetInfoTipo().Type
            };
            ap.Add(p);

            p = new MySqlParameter("@nome", Parser.parseString(_nome))
            {
                MySqlDbType = GetInfoNome().Type
            };
            ap.Add(p);

            p = new MySqlParameter("@email", Parser.parseString(_email))
            {
                MySqlDbType = GetInfoEmail().Type
            };
            ap.Add(p);

            p = new MySqlParameter("@login", Parser.parseString(_login))
            {
                MySqlDbType = GetInfoLogin().Type
            };
            ap.Add(p);

            p = new MySqlParameter("@senha", Parser.parseString(_senha))
            {
                MySqlDbType = GetInfoSenha().Type
            };
            ap.Add(p);

            p = new MySqlParameter("@dica", Parser.parseString(_dica))
            {
                MySqlDbType = GetInfoDica().Type
            };
            ap.Add(p);

            p = new MySqlParameter("@situacao", (int)_situacao)
            {
                MySqlDbType = GetInfoSituacao().Type
            };
            ap.Add(p);

            p = new MySqlParameter("@datacriacao", Parser.parseDateTimeBD(_dataCriacao))
            {
                MySqlDbType = GetInfoDataCriacao().Type
            };
            ap.Add(p);

            p = new MySqlParameter("@dataalteracao", Parser.parseDateTimeBD(_dataAlteracao))
            {
                MySqlDbType = GetInfoDataAlteracao().Type
            };
            ap.Add(p);

            p = new MySqlParameter("@clinica", Parser.parseInteiro(_clinica))
            {
                MySqlDbType = GetInfoClinica().Type
            };
            ap.Add(p);

            return ap.ToArray();
        }

        /// <summary>
        /// Inicialização das propriedades do elemento Usuario com seus valores padrão
        /// </summary>
        private void IniciarElemento()
        {
            GetInfoTabela();

            _tipo = new TipoUsuario();

            _dataAlteracao = Utils.dataPadrao;
            _dataCriacao = Utils.dataPadrao;
            _dica = String.Empty;
            _email = String.Empty;
            _login = String.Empty;
            _nome = String.Empty;
            _senha = String.Empty;
            _tipo.Codigo = -1;
            _situacao = Utils.SituacaoRegistro.Ativo;
        }

        /// <summary>
        /// Inicialização do elemento Usuario
        /// </summary>
        public Usuario()
        {
            IniciarElemento();
        }

        /// <summary>
        /// Rotina que insere o registro no BD
        /// </summary>
        /// <returns>Código do registro inserido no BD</returns>
        public int Incluir()
        {
            if (String.IsNullOrEmpty(_nome) || String.IsNullOrEmpty(_email))
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    sql.Parameters.AddRange(GetParametrosSQL());
                    sql.CommandText = "INSERT INTO Usuarios "
                                    + "(senha, nome, email, login, tipo, "
                                    + "datacriacao, dataalteracao, situacao, dica, clinica)  "
                                    + "VALUE (@senha, @nome, @email, @login, @tipo, "
                                    + "@datacriacao, @dataalteracao, @situacao, @dica, @clinica); "
                                    + "SELECT LAST_INSERT_ID();";
                    int resultado = Parser.parseInteiro(new Data().executeEscalar(sql));
                    return resultado;
                }
            }
            catch (ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Rotina que altera os dados do registro no BD
        /// </summary>
        /// <returns>Quantidade de linhas alteradas na ação junto ao BD</returns>
        public int Alterar()
        {
            if (_codigo < 0)
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    sql.Parameters.AddRange(GetParametrosSQL());
                    sql.CommandText = "UPDATE Usuarios "
                                    + "SET "
                                    + "senha = @senha, nome = @nome, email = @email, login = @login, "
                                    + "dataalteracao = @dataalteracao, situacao = @situacao, "
                                    + "dica = @dica, tipo = @tipo, clinica = @clinica "
                                    + "WHERE codigo = @codigo";
                    int resultado = Parser.parseInteiro(new Data().execute(sql));
                    return resultado;
                }
            }
            catch (ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Rotina que exclui o registro do BD
        /// </summary>
        /// <returns>Quantidade de linhas alteradas na ação junto ao BD</returns>
        public int Excluir()
        {
            if (_codigo < 0)
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    sql.Parameters.AddRange(GetParametrosSQL());
                    sql.CommandText = "DELETE FROM Usuarios "
                                    + "WHERE codigo = @codigo";
                    int resultado = Parser.parseInteiro(new Data().execute(sql));
                    return resultado;
                }
            }
            catch (ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Rotina que localiza e retorna um único registro, mediante o código informado
        /// </summary>
        /// <param name="codigo">VALUE: código de identificação do registro (obrigatório)</param>
        /// <returns>Elemento Usuario com os dados retornados da consulta</returns>
        public static Usuario ConsultarUnico(int codigo)
        {
            if (codigo <= 0)
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            ImunneVacinas.PesquisaUsuario pesquisa = new PesquisaUsuario
            {
                Codigo = codigo
            };

            List<Usuario> retorno = ConsultaGenerica(pesquisa);
            if (retorno == null || retorno.Count.Equals(0))
                return null;
            else
                return retorno[0];
        }

        /// <summary>
        /// Rotina que localiza e retorna um único registro, mediante o NOME informado
        /// </summary>
        /// <param name="nome">VALUE: NOME do usuário (obrigatório)</param>
        /// <returns>Elemento Usuario com os dados retornados da consulta</returns>
        public static Usuario ConsultarUnicoNome(string nome)
        {
            if (String.IsNullOrEmpty(nome))
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            ImunneVacinas.PesquisaUsuario pesquisa = new PesquisaUsuario
            {
                CampoLivre = nome
            };

            List<Usuario> retorno = ConsultaGenerica(pesquisa);
            if (retorno == null || retorno.Count.Equals(0))
                return null;
            else
                return retorno[0];
        }

        /// <summary>
        /// Rotina que localiza e retorna um único registro, mediante o LOGIN informado
        /// </summary>
        /// <param name="login">VALUE:Login do usuário a ser pesquisado (obrigatório)</param>
        /// <returns>Elemento Usuario com os dados retornados da consulta</returns>
        public static Usuario ConsultarUnicoLogin(string login)
        {
            if (String.IsNullOrEmpty(login))
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            ImunneVacinas.PesquisaUsuario pesquisa = new PesquisaUsuario
            {
                Login = login
            };

            List<Usuario> retorno = ConsultaGenerica(pesquisa);
            if (retorno == null || retorno.Count.Equals(0))
                return null;
            else
                return retorno[0];
        }

        /// <summary>
        /// Rotina que localiza e retorna um único registro, mediante o login e a senha informados
        /// </summary>
        /// <param name="login">VALUE: Login do usuário (obrigatório)</param>
        /// <param name="senha">VALUE: Senha do usuário (obrigatório)</param>
        /// <returns>Elemento Usuario com os dados retornados da consulta</returns>
        public static Usuario ConsultarUnico(string login, string senha)
        {
            if (String.IsNullOrEmpty(login) || String.IsNullOrEmpty(senha))
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            ImunneVacinas.PesquisaUsuario pesquisa = new PesquisaUsuario
            {
                Login = login,
                Senha = senha
            };

            List<Usuario> retorno = ConsultaGenerica(pesquisa);
            if (retorno == null || retorno.Count.Equals(0))
                return null;
            else
                return retorno[0];
        }

        /// <summary>
        /// Rotina que realiza a pesquisa de N elementos do Usuario
        /// </summary>
        /// <param name="pesquisa">VALUE: Elemento com os parâmetros com os quais será realizada a pesquisa</param>
        /// <returns>Lista do elemento Usuario com os dados retornados da consulta</returns>
        public static List<Usuario> ConsultarUsuarios(PesquisaUsuario pesquisa)
        {
            return ConsultaGenerica(pesquisa);
        }

        /// <summary>
        /// Rotina que retorna um elemento RetornoCBX para utilização em DropLists
        /// </summary>
        /// <param name="pesquisa">VALUE: Elemento com os parâmetros de pesquisa</param>
        /// <param name="InserirLinhaPadrao">VALUE: Informa se será adiconada a linha padrão</param>
        /// <param name="textoPadrao">VALUE: Informa o texto utilizado para linha padão (se utilizado NULL, será apresentado SELECIONE)</param>
        /// <returns>Lista do elemento RetornoCBX com os dados desejados</returns>
        public static List<RetornoComboBox> ConsultarComboBox(PesquisaUsuario pesquisa, bool InserirLinhaPadrao, string textoPadrao)
        {
            DataTable dt = new DataTable();
            try
            {
                dt.Columns.Add("DisplayMember");
                dt.Columns.Add("ValueMember");

                List<Usuario> retorno = ConsultaGenerica(pesquisa);
                if (retorno != null)
                {
                    foreach (Usuario c in retorno)
                    {
                        dt.Rows.Add(new object[] { c.Nome.ToUpper(), c.Codigo });
                    }
                }
                return Utils.MontarRetornoComboBox(dt, InserirLinhaPadrao, textoPadrao);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// Rotina interna utilizada para todas as pesquisas do elemento Usuario
        /// </summary>
        /// <param name="pesquisa">VALUE: Elemento com os parâmetros com os quais será realizada a pesquisa</param>
        /// <returns>Lista do elemento Usuario com os dados retornados da consulta</returns>
        internal static List<Usuario> ConsultaGenerica(PesquisaUsuario pesquisa)
        {
            StringBuilder ordenacao = new StringBuilder();
            StringBuilder filtro = new StringBuilder();
            StringBuilder top = new StringBuilder();
            MySqlParameter p;

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    if (pesquisa.Quantidade > 0)
                        top.AppendFormat(" LIMIT {0} ", pesquisa.Quantidade);

                    #region Campo Código

                    if (pesquisa.Codigo >= 0)
                    {
                        filtro.Append(" AND Usuarios.codigo = @codigo");
                        p = new MySqlParameter("@codigo", pesquisa.Codigo)
                        {
                            MySqlDbType = GetInfoCodigo().Type
                        };
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Tipo

                    if (pesquisa.Tipo >= 0)
                    {
                        filtro.Append(" AND Usuarios.tipo = @tipo");
                        p = new MySqlParameter("@tipo", pesquisa.Tipo)
                        {
                            MySqlDbType = GetInfoTipo().Type
                        };
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campos Login/Senha

                    if (!String.IsNullOrEmpty(pesquisa.Login))
                    {
                        filtro.Append(" AND Usuarios.login = @LOGIN");
                        p = new MySqlParameter("@LOGIN", pesquisa.Login)
                        {
                            MySqlDbType = MySqlDbType.VarChar
                        };
                        sql.Parameters.Add(p);

                        if (!String.IsNullOrEmpty(pesquisa.Senha))
                        {
                            filtro.Append(" AND Usuarios.senha = @SENHA");
                            p = new MySqlParameter("@SENHA", pesquisa.Senha)
                            {
                                MySqlDbType = MySqlDbType.VarChar
                            };
                            sql.Parameters.Add(p);
                        }
                    }

                    #endregion

                    #region Campos Clínica

                    if (pesquisa.Clinica > 0)
                    {
                        filtro.Append(" AND Usuarios.clinica = @clinica");
                        p = new MySqlParameter("@clinica", pesquisa.Clinica)
                        {
                            MySqlDbType = GetInfoClinica().Type
                        };
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Livre

                    if (!String.IsNullOrEmpty(pesquisa.CampoLivre.Trim()))
                    {
                        string campoLivre = String.Format("%{0}%", pesquisa.CampoLivre.Trim());
                        filtro.Append(" AND (Usuarios.nome LIKE @campoLivre");
                        filtro.Append(" OR Usuarios.email LIKE @campoLivre");
                        filtro.Append(")");
                        p = new MySqlParameter("@campoLivre", campoLivre)
                        {
                            MySqlDbType = MySqlDbType.VarChar
                        };
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Situação

                    if (pesquisa.Situacao != Utils.SituacaoRegistro.Indiferente)
                    {
                        filtro.Append(" AND Usuarios.situacao = @situacao");
                        p = new MySqlParameter("@situacao", (int)pesquisa.Situacao)
                        {
                            MySqlDbType = GetInfoSituacao().Type
                        };
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    if (filtro.Length > 0)
                        filtro = filtro.Replace(" AND ", " WHERE ", 0, 5);

                    #region Ordenação

                    switch (pesquisa.Ordenacao)
                    {
                        case Utils.TipoOrdenacao.porCodigo:
                            {
                                ordenacao.Append("ORDER BY Usuarios.codigo");
                            }
                            break;
                        case Utils.TipoOrdenacao.porCodigoDesc:
                            {
                                ordenacao.Append("ORDER BY Usuarios.codigo DESC");
                            }
                            break;
                        case Utils.TipoOrdenacao.porNome:
                            {
                                ordenacao.Append("ORDER BY Usuarios.nome");
                            }
                            break;
                        case Utils.TipoOrdenacao.porNomeDesc:
                            {
                                ordenacao.Append("ORDER BY Usuarios.nome DESC");
                            }
                            break;
                        case Utils.TipoOrdenacao.porData:
                            {
                                ordenacao.Append("ORDER BY Usuarios.datacriacao");
                            }
                            break;
                        case Utils.TipoOrdenacao.porDataDesc:
                            {
                                ordenacao.Append("ORDER BY Usuarios.datacriacao DESC");
                            }
                            break;
                        case Utils.TipoOrdenacao.randomico:
                            {
                                ordenacao.Append("ORDER BY RAND()");
                            }
                            break;
                        default:
                            {
                                ordenacao.Append("ORDER BY Usuarios.codigo");
                            }
                            break;
                    }

                    #endregion

                    sql.CommandText = String.Format("SELECT "
                                    + "TiposUsuarios.codigo AS codigoTipo, TiposUsuarios.descricao AS nomeTipo, "
                                    + "Usuarios.* "
                                    + "FROM Usuarios "
                                    + "INNER JOIN TiposUsuarios ON TiposUsuarios.codigo = Usuarios.tipo "
                                    + "{0} {1} {2}", filtro, ordenacao, top);
                    DataTable dt = new Data().getData(sql);
                    return MontarRetorno(dt);
                }
            }
            catch (ImunneVacinas.ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Rotina que retorna uma lista genérica do elemento Usuario, de acordo com o retorno da Consultar ao BD
        /// </summary>
        /// <param name="dt">VALUE: DataTable com o resultado da Consultar ao BD</param>
        /// <returns>Lista do elemento Usuario com os dados retornados da consulta</returns>
        internal static List<Usuario> MontarRetorno(DataTable dt)
        {
            int i = -1;
            List<Usuario> list = new List<Usuario>();

            if (dt == null && dt.Rows.Count == 0)
                return list;
            else
            {
                Usuario retorno;
                try
                {
                    for (i = 0; i < dt.Rows.Count; i++)
                    {
                        retorno = new Usuario
                        {
                            _codigo = Parser.parseInteiro(dt.Rows[i]["codigo"]),
                            _nome = Parser.parseString(dt.Rows[i]["nome"]),
                            _email = Parser.parseString(dt.Rows[i]["email"]),
                            _login = Parser.parseString(dt.Rows[i]["login"]),
                            _senha = Parser.parseString(dt.Rows[i]["senha"]),
                            _situacao = (Utils.SituacaoRegistro)Enum.Parse(typeof(Utils.SituacaoRegistro), dt.Rows[i]["situacao"].ToString()),
                            _dataCriacao = Convert.ToDateTime(dt.Rows[i]["datacriacao"]),
                            _dica = Parser.parseString(dt.Rows[i]["dica"]),
                            _clinica = Parser.parseInteiro(dt.Rows[i]["clinica"])
                        };

                        retorno._tipo.Codigo = Parser.parseInteiro(dt.Rows[i]["codigoTipo"]);
                        retorno._tipo.Descricao = Parser.parseString(dt.Rows[i]["nomeTipo"]);

                        if (!String.IsNullOrEmpty(dt.Rows[i]["dataalteracao"].ToString()))
                            retorno._dataAlteracao = Convert.ToDateTime(dt.Rows[i]["dataalteracao"]);

                        list.Add(retorno);
                    }
                    return list;
                }
                catch (Exception ex)
                {
                    ex.Data.Add("último retorno lido", i);
                    throw;
                }
            }
        }
    }
}