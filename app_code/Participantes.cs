﻿using MySql.Data.MySqlClient;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace ImunneVacinas
{
    /// <summary>
    /// Descrição resumida de Participantes
    /// </summary>
    [Serializable]
    public class Participante
    {
        private static string _tabela = "Participantes";
        private static List<BD.Columns> columns;
        private static DateTime columnsUpdated;

        #region Propriedades do elemento

        private int _codigo;
        private string _nome;
        private string _cpf;
        private DateTime _dataNascimento;
        private string _email;
        private string _telefone;
        private GrauParentesco _parentesco;
        private int _participantePai;
        private DateTime _dataCriacao;
        private DateTime _dataAlteracao;

        /// <summary>
        /// Código de identificação do registro
        /// </summary>
        public int Codigo
        {
            get
            {
                return _codigo;
            }

            set
            {
                _codigo = value;
            }
        }

        /// <summary>
        /// Nome do participante
        /// </summary>
        public string Nome
        {
            get
            {
                return _nome;
            }

            set
            {
                _nome = value;
            }
        }

        /// <summary>
        /// CPF do participante
        /// </summary>
        public string Cpf
        {
            get
            {
                return _cpf;
            }

            set
            {
                _cpf = value;
            }
        }

        /// <summary>
        /// Data de nascimento do participante
        /// </summary>
        public DateTime DataNascimento
        {
            get
            {
                return _dataNascimento;
            }

            set
            {
                _dataNascimento = value;
            }
        }

        /// <summary>
        /// E-mail do participante
        /// </summary>
        public string Email
        {
            get
            {
                return _email;
            }

            set
            {
                _email = value;
            }
        }

        /// <summary>
        /// Telefone do participante
        /// </summary>
        public string Telefone
        {
            get
            {
                return _telefone;
            }

            set
            {
                _telefone = value;
            }
        }

        /// <summary>
        /// Identificação do grau de parentesco
        /// </summary>
        public GrauParentesco Parentesco
        {
            get
            {
                return _parentesco;
            }

            set
            {
                _parentesco = value;
            }
        }

        /// <summary>
        /// Identificação do participante "pai"
        /// </summary>
        public int ParticipantePai
        {
            get
            {
                return _participantePai;
            }

            set
            {
                _participantePai = value;
            }
        }

        /// <summary>
        /// Data de criação do registro
        /// </summary>
        public DateTime DataCriacao
        {
            get
            {
                return _dataCriacao;
            }

            set
            {
                _dataCriacao = value;
            }
        }

        /// <summary>
        /// Data da última alteração do registro
        /// </summary>
        public DateTime DataAlteracao
        {
            get
            {
                return _dataAlteracao;
            }

            set
            {
                _dataAlteracao = value;
            }
        }

        #endregion

        #region GetInfos

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos os campos da tabela.
        /// </summary>
        private static void GetInfoTabela()
        {
            if (columns == null || (DateTime.Now - columnsUpdated).Minutes > 0)
            {
                columns = BD.Columns.getColumnsInfo(_tabela);
                columnsUpdated = DateTime.Now;
            }
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo codigo.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoCodigo()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "codigo");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo nome.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoNome()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "nome");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo cpf.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoCPF()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "cpf");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo datanascimento.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoDataNascimento()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "datanascimento");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo email.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoEmail()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "email");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo telefone.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoTelefone()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "telefone");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo parentesco.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoParentesco()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "parentesco");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo participantepai.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoParticipantePai()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "participantepai");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo datacriacao.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoDataCriacao()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "datacriacao");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo dataalteracao.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoDataAlteracao()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "dataalteracao");
        }

        #endregion

        /// <summary> 
        /// Rotina que cria um Array com os parâmetros utilizados nas ações de INSERT, UPDATE e DELETE
        /// <returns>Array com vários MySqlParameters</returns>
        /// </summary>
        private Array GetParametrosSQL()
        {
            MySqlParameter p;
            ArrayList ap = new ArrayList();

            p = new MySqlParameter("@codigo", Parser.parseInteiro(_codigo));
            p.MySqlDbType = GetInfoCodigo().Type;
            ap.Add(p);

            p = new MySqlParameter("@nome", Parser.parseString(_nome));
            p.MySqlDbType = GetInfoNome().Type;
            ap.Add(p);

            p = new MySqlParameter("@cpf", Parser.parseString(_cpf));
            p.MySqlDbType = GetInfoCPF().Type;
            ap.Add(p);

            p = new MySqlParameter("@datanascimento", Parser.parseDateTimeBD(_dataNascimento));
            p.MySqlDbType = GetInfoDataNascimento().Type;
            ap.Add(p);

            p = new MySqlParameter("@email", Parser.parseString(_email));
            p.MySqlDbType = GetInfoEmail().Type;
            ap.Add(p);

            p = new MySqlParameter("@telefone", Parser.parseString(_telefone));
            p.MySqlDbType = GetInfoTelefone().Type;
            ap.Add(p);

            p = new MySqlParameter("@parentesco", Parser.parseInteiro(_parentesco.Codigo));
            p.MySqlDbType = GetInfoParentesco().Type;
            ap.Add(p);

            p = new MySqlParameter("@participantepai", Parser.parseInteiro(_participantePai));
            p.MySqlDbType = GetInfoParticipantePai().Type;
            ap.Add(p);

            p = new MySqlParameter("@datacriacao", Parser.parseDateTimeBD(_dataCriacao));
            p.MySqlDbType = GetInfoDataCriacao().Type;
            ap.Add(p);

            p = new MySqlParameter("@dataalteracao", Parser.parseDateTimeBD(_dataAlteracao));
            p.MySqlDbType = GetInfoDataAlteracao().Type;
            ap.Add(p);

            return ap.ToArray();
        }

        /// <summary>
        /// Inicialização das propriedades do elemento com seus valores padrão
        /// </summary>
        private void IniciarElemento()
        {
            GetInfoTabela();

            _parentesco = new GrauParentesco();

            _parentesco.Codigo = -1;
            _cpf = String.Empty;
            _dataAlteracao = Utils.dataPadrao;
            _dataCriacao = Utils.dataPadrao;
            _dataNascimento = Utils.dataPadrao;
            _email = String.Empty;
            _nome = String.Empty;
            _participantePai = -1;
            _telefone = String.Empty;
        }

        /// <summary>
        /// Inicialização do elemento
        /// </summary>
        public Participante()
        {
            IniciarElemento();
        }

        /// <summary>
        /// Rotina que insere o registro no BD
        /// </summary>
        /// <returns>Código do registro inserido no BD</returns>
        public int Incluir()
        {
            if (String.IsNullOrEmpty(_nome))
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    sql.Parameters.AddRange(GetParametrosSQL());
                    sql.CommandText = "INSERT INTO Participantes "
                                    + "(nome, cpf, datanascimento, "
                                    + "email, telefone, participantepai, "
                                    + "parentesco, datacriacao, dataalteracao) "
                                    + "VALUE (@nome, @cpf, @datanascimento, "
                                    + "@email, @telefone, @participantepai, "
                                    + "@parentesco, @datacriacao, @dataalteracao); "
                                    + "SELECT LAST_INSERT_ID();";
                    int resultado = Parser.parseInteiro(new Data().executeEscalar(sql));
                    return resultado;
                }
            }
            catch (ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Rotina que altera os dados do registro no BD
        /// </summary>
        /// <returns>Quantidade de linhas alteradas na ação junto ao BD</returns>
        public int Alterar()
        {
            if (_codigo < 0)
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    sql.Parameters.AddRange(GetParametrosSQL());
                    sql.CommandText = "UPDATE Participantes "
                                    + "SET "
                                    + "nome = @nome, cpf = @cpf, "
                                    + "datanascimento = @datanascimento, email = @email, "
                                    + "telefone = @telefone, participantepai = @participantepai, "
                                    + "parentesco = @parentesco, dataalteracao = @dataalteracao "
                                    + "WHERE codigo = @codigo";
                    int resultado = Parser.parseInteiro(new Data().execute(sql));
                    return resultado;
                }
            }
            catch (ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Rotina que exclui o registro do BD
        /// </summary>
        /// <returns>Quantidade de linhas alteradas na ação junto ao BD</returns>
        public int Excluir()
        {
            if (_codigo < 0)
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    sql.Parameters.AddRange(GetParametrosSQL());
                    sql.CommandText = "DELETE FROM Participantes "
                                    + "WHERE codigo = @codigo";
                    int resultado = Parser.parseInteiro(new Data().execute(sql));
                    return resultado;
                }
            }
            catch (ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Rotina que localiza e retorna um único registro, mediante o código informado
        /// </summary>
        /// <param name="codigo">VALUE: código de identificação do registro (obrigatório)</param>
        /// <returns>Elemento Usuario com os dados retornados da consulta</returns>
        public static Participante ConsultarUnico(int codigo)
        {
            if (codigo <= 0)
                throw new Exception("ImunneParticipantes - parâmetros com valores inválidos");

            ImunneVacinas.PesquisaParticipante pesquisa = new PesquisaParticipante()
            {
                Codigo = codigo
            };

            List<Participante> retorno = ConsultaGenerica(pesquisa);
            if (retorno == null || retorno.Count.Equals(0))
                return null;
            else
                return retorno[0];
        }

        /// <summary>
        /// Rotina que localiza e retorna um único registro, mediante o código informado
        /// </summary>
        /// <param name="valor">VALUE: campo de texto para pesquisa pelor nome e/ou cpf (obrigatório)</param>
        /// <returns>Elemento Usuario com os dados retornados da consulta</returns>
        public static Participante ConsultarUnico(string valor)
        {
            if (String.IsNullOrEmpty(valor))
                throw new Exception("ImunneParticipantes - parâmetros com valores inválidos");

            ImunneVacinas.PesquisaParticipante pesquisa = new PesquisaParticipante()
            {
                Nome = valor
            };

            List<Participante> retorno = ConsultaGenerica(pesquisa);
            if (retorno == null || retorno.Count.Equals(0))
            {
                pesquisa = new PesquisaParticipante()
                {
                    Cpf = valor
                };

                retorno = ConsultaGenerica(pesquisa);
                if (retorno == null || retorno.Count.Equals(0))
                    return null;
                else return retorno[0];
            }
            else
                return retorno[0];
        }

        /// <summary>
        /// Rotina que localiza e retorna um único registro, mediante o código informado
        /// </summary>
        /// <param name="valor">VALUE: campo de texto para pesquisa pelor nome e/ou cpf (obrigatório)</param>
        /// <returns>Elemento Usuario com os dados retornados da consulta</returns>
        public static Participante ConsultarUnico(int titular, string valor)
        {
            if (String.IsNullOrEmpty(valor))
                throw new Exception("ImunneParticipantes - parâmetros com valores inválidos");

            ImunneVacinas.PesquisaParticipante pesquisa = new PesquisaParticipante()
            {
                Nome = valor,
                ParticipantePai = titular
            };

            List<Participante> retorno = ConsultaGenerica(pesquisa);
            if (retorno == null || retorno.Count.Equals(0))
            {
                pesquisa = new PesquisaParticipante()
                {
                    Cpf = valor,
                    ParticipantePai = titular
                };

                retorno = ConsultaGenerica(pesquisa);
                if (retorno == null || retorno.Count.Equals(0))
                    return null;
                else return retorno[0];
            }
            else
                return retorno[0];
        }

        /// <summary>
        /// Rotina que realiza a pesquisa de elementos mediante os parametros de pesquisa informados
        /// </summary>
        /// <param name="pesquisa">VALUE: elemento de pesquisa com os parametros para consulta</param>
        /// <returns></returns>
        public static List<Participante> ConsultarParticipantes(PesquisaParticipante pesquisa)
        {
            return ConsultaGenerica(pesquisa);
        }

        /// <summary>
        /// Rotina interna utilizada para todas as pesquisas do elemento
        /// </summary>
        /// <param name="pesquisa">VALUE: Elemento com os parâmetros com os quais será realizada a pesquisa</param>
        /// <returns>Lista do elemento com os dados retornados da consulta</returns>
        internal static List<Participante> ConsultaGenerica(PesquisaParticipante pesquisa)
        {
            StringBuilder ordenacao = new StringBuilder();
            StringBuilder filtro = new StringBuilder();
            StringBuilder top = new StringBuilder();
            MySqlParameter p;

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    if (pesquisa.Quantidade > 0)
                        top.AppendFormat(" LIMIT {0} ", pesquisa.Quantidade);

                    #region Campo Código

                    if (pesquisa.Codigo >= 0)
                    {
                        filtro.Append(" AND Participantes.codigo = @codigo");
                        p = new MySqlParameter("@codigo", pesquisa.Codigo);
                        p.MySqlDbType = GetInfoCodigo().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Parentesco

                    if (pesquisa.Parentesco >= 0)
                    {
                        filtro.Append(" AND Participantes.parentesco = @parentesco");
                        p = new MySqlParameter("@parentesco", pesquisa.Parentesco);
                        p.MySqlDbType = GetInfoParentesco().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo ParticipantePai

                    if (pesquisa.ParticipantePai >= 0)
                    {
                        filtro.Append(" AND Participantes.participantepai = @participantepai");
                        p = new MySqlParameter("@participantepai", pesquisa.ParticipantePai);
                        p.MySqlDbType = GetInfoParticipantePai().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Nome

                    if (!String.IsNullOrEmpty(pesquisa.Nome.Trim()))
                    {
                        string nomeParticipante = String.Format("{0}", pesquisa.Nome.Trim());
                        filtro.Append(" AND (Participantes.nome = @nome ");
                        filtro.Append(")");
                        p = new MySqlParameter("@nome", nomeParticipante);
                        p.MySqlDbType = MySqlDbType.VarChar;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo CPF

                    if (!String.IsNullOrEmpty(pesquisa.Cpf.Trim()))
                    {
                        string cpfParticipante = String.Format("{0}", pesquisa.Cpf.Trim());
                        filtro.Append(" AND (Participantes.cpf = @cpf");
                        filtro.Append(")");
                        p = new MySqlParameter("@cpf", cpfParticipante);
                        p.MySqlDbType = MySqlDbType.VarChar;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Livre

                    if (!String.IsNullOrEmpty(pesquisa.CampoLivre.Trim()))
                    {
                        string campoLivre = String.Format("%{0}%", pesquisa.CampoLivre.Trim());
                        filtro.Append(" AND (Participantes.nome LIKE @campoLivre ");
                        filtro.Append(" OR Participantes.email LIKE @campoLivre ");
                        filtro.Append(" OR Participantes.telefone LIKE @campoLivre");
                        filtro.Append(")");
                        p = new MySqlParameter("@campoLivre", campoLivre);
                        p.MySqlDbType = MySqlDbType.VarChar;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    if (filtro.Length > 0)
                        filtro = filtro.Replace(" AND ", " WHERE ", 0, 5);

                    #region Ordenação

                    switch (pesquisa.Ordenacao)
                    {
                        case Utils.TipoOrdenacao.porCodigo:
                            {
                                ordenacao.Append("ORDER BY Participantes.codigo");
                            }
                            break;
                        case Utils.TipoOrdenacao.porCodigoDesc:
                            {
                                ordenacao.Append("ORDER BY Participantes.codigo DESC");
                            }
                            break;
                        case Utils.TipoOrdenacao.porNome:
                            {
                                ordenacao.Append("ORDER BY Participantes.nome");
                            }
                            break;
                        case Utils.TipoOrdenacao.porNomeDesc:
                            {
                                ordenacao.Append("ORDER BY Participantes.nome DESC");
                            }
                            break;
                        case Utils.TipoOrdenacao.porData:
                            {
                                ordenacao.Append("ORDER BY Participantes.datacriacao");
                            }
                            break;
                        case Utils.TipoOrdenacao.porDataDesc:
                            {
                                ordenacao.Append("ORDER BY Participantes.datacriacao DESC");
                            }
                            break;
                        case Utils.TipoOrdenacao.randomico:
                            {
                                ordenacao.Append("ORDER BY RAND()");
                            }
                            break;
                        default:
                            {
                                ordenacao.Append("ORDER BY Participantes.codigo");
                            }
                            break;
                    }

                    #endregion

                    sql.CommandText = String.Format("SELECT "
                        + "GrausParentescos.codigo AS codigoParentesco, GrausParentescos.nome AS nomeParentesco, "
                        + "Participantes.* "
                        + "FROM Participantes "
                        + "LEFT JOIN GrausParentescos ON GrausParentescos.codigo = Participantes.parentesco "
                        + "{0} {1} {2}", filtro, ordenacao, top);
                    DataTable dt = new Data().getData(sql);
                    return MontarRetorno(dt);
                }
            }
            catch (ImunneVacinas.ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Rotina que retorna uma lista genérica do elemento, de acordo com o retorno da Consultar ao BD
        /// </summary>
        /// <param name="dt">VALUE: DataTable com o resultado da Consultar ao BD</param>
        /// <returns>Lista do elemento com os dados retornados da consulta</returns>
        internal static List<Participante> MontarRetorno(DataTable dt)
        {
            int i = -1;
            List<Participante> list = new List<Participante>();

            if (dt == null && dt.Rows.Count == 0)
                return list;
            else
            {
                Participante retorno;
                try
                {
                    for (i = 0; i < dt.Rows.Count; i++)
                    {
                        retorno = new Participante()
                        {
                            _codigo = Parser.parseInteiro(dt.Rows[i]["codigo"]),
                            _nome = Parser.parseString(dt.Rows[i]["nome"]),
                            _cpf = Parser.parseString(dt.Rows[i]["cpf"]),
                            _email = Parser.parseString(dt.Rows[i]["email"]),
                            _telefone = Parser.parseString(dt.Rows[i]["telefone"]),
                            _dataNascimento = Convert.ToDateTime(dt.Rows[i]["datanascimento"]),
                            _participantePai = Parser.parseInteiro(dt.Rows[i]["participantePai"]),
                            _dataCriacao = Convert.ToDateTime(dt.Rows[i]["datacriacao"])
                        };

                        retorno._parentesco.Codigo = Parser.parseInteiro(dt.Rows[i]["codigoParentesco"]);
                        retorno._parentesco.Nome = Parser.parseString(dt.Rows[i]["nomeParentesco"]);

                        if (!String.IsNullOrEmpty(dt.Rows[i]["dataalteracao"].ToString()))
                            retorno._dataAlteracao = Convert.ToDateTime(dt.Rows[i]["dataalteracao"]);

                        list.Add(retorno);
                    }
                    return list;
                }
                catch (Exception ex)
                {
                    ex.Data.Add("último retorno lido", i);
                    throw;
                }
            }
        }
    }
}