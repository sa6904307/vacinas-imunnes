﻿using MySql.Data.MySqlClient;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace ImunneVacinas
{
    /// <summary>
    /// Descrição resumida de Campanhas
    /// </summary>
    [Serializable]
    public class Campanha
    {
        private static string _tabela = "Campanhas";
        private static List<BD.Columns> columns;
        private static DateTime columnsUpdated;

        #region PROPRIEDADES

        private Empresa _idEmpresa;
        private int _codigo;
        private string _nome;
        private string _identificacao;
        private DateTime _dataCriacao;
        private DateTime _dataAlteracao;
        private DateTime _dataInicio;
        private DateTime _dataTermino;
        private DateTime _dataAbertura;
        private DateTime _dataLimite;
        private string _termosImagem;
        private string _termosTexto;
        private string _nomeContato;
        private int _parcelas;
        private int _unicaVenda;
        private string _contatoWhatsApp;
        private string _linkAdesao;
        private int _surto;
        private int _totalAtendimentos;
        private int _horaInicio;
        private int _horaTermino;
        private int _incluirTitular;

        /// <summary>
        /// Código de identificação da empresa
        /// </summary>
        public Empresa IdEmpresa
        {
            get
            {
                return _idEmpresa;
            }

            set
            {
                _idEmpresa = value;
            }
        }

        /// <summary>
        /// Código de identificação do registro
        /// </summary>
        public int Codigo
        {
            get
            {
                return _codigo;
            }

            set
            {
                _codigo = value;
            }
        }

        /// <summary>
        /// Nome da campanha
        /// </summary>
        public string Nome
        {
            get
            {
                return _nome;
            }

            set
            {
                _nome = value;
            }
        }

        /// <summary>
        /// Identificação da campanha
        /// </summary>
        public string Identificacao
        {
            get
            {
                return _identificacao;
            }

            set
            {
                _identificacao = value;
            }
        }

        /// <summary>
        /// Data de criação do registro
        /// </summary>
        public DateTime DataCriacao
        {
            get
            {
                return _dataCriacao;
            }

            set
            {
                _dataCriacao = value;
            }
        }

        /// <summary>
        /// Data da última alteração do registro
        /// </summary>
        public DateTime DataAlteracao
        {
            get
            {
                return _dataAlteracao;
            }

            set
            {
                _dataAlteracao = value;
            }
        }

        /// <summary>
        /// Data de início da campanha
        /// </summary>
        public DateTime DataInicio
        {
            get
            {
                return _dataInicio;
            }

            set
            {
                _dataInicio = value;
            }
        }

        /// <summary>
        /// Data de término da campanha
        /// </summary>
        public DateTime DataTermino
        {
            get
            {
                return _dataTermino;
            }

            set
            {
                _dataTermino = value;
            }
        }

        /// <summary>
        /// Data de abertura das aplicações das vacinas
        /// </summary>
        public DateTime DataAbertura
        {
            get
            {
                return _dataAbertura;
            }

            set
            {
                _dataAbertura = value;
            }
        }

        /// <summary>
        /// Data de limite da campanha
        /// </summary>
        public DateTime DataLimite
        {
            get
            {
                return _dataLimite;
            }

            set
            {
                _dataLimite = value;
            }
        }

        /// <summary>
        /// Imagem com os termos da campanha
        /// </summary>
        public string TermosImagem
        {
            get
            {
                return _termosImagem;
            }

            set
            {
                _termosImagem = value;
            }
        }

        /// <summary>
        /// Texto com os termos da campanha
        /// </summary>
        public string TermosTexto
        {
            get
            {
                return _termosTexto;
            }

            set
            {
                _termosTexto = value;
            }
        }

        /// <summary>
        /// Nome da pessoa de contato da campanha
        /// </summary>
        public string NomeContato
        {
            get
            {
                return _nomeContato;
            }

            set
            {
                _nomeContato = value;
            }
        }

        /// <summary>
        /// Quantidade de parcelas que a campanha permite
        /// </summary>
        public int Parcelas
        {
            get
            {
                return _parcelas;
            }

            set
            {
                _parcelas = value;
            }
        }

        /// <summary>
        /// Informa se a campanha permite uma única venda para um CPF/CNPJ
        /// </summary>
        public int UnicaVenda
        {
            get
            {
                return _unicaVenda;
            }

            set
            {
                _unicaVenda = value;
            }
        }

        /// <summary>
        /// Informa um número de telefone de contato via WhatsApp
        /// </summary>
        public string ContatoWhatsApp
        {
            get
            {
                return _contatoWhatsApp;
            }

            set
            {
                _contatoWhatsApp = value;
            }
        }

        /// <summary>
        /// Link para adesão e escolha do local da campanha
        /// </summary>
        public string LinkAdesao
        {
            get
            {
                return _linkAdesao;
            }

            set
            {
                _linkAdesao = value;
            }
        }

        /// <summary>
        /// Campanha destinada para surtos, comportamento diferente na emissão de vouchers
        /// </summary>
        public int Surto
        {
            get
            {
                return _surto;
            }

            set
            {
                _surto = value;
            }
        }

        /// <summary>
        /// Total de atendimentos permitidos por hora
        /// </summary>
        public int TotalAtendimentos
        {
            get
            {
                return _totalAtendimentos;
            }

            set
            {
                _totalAtendimentos = value;
            }
        }

        /// <summary>
        /// Horário de inicio de atendimento no surto
        /// </summary>
        public int HoraInicio
        {
            get
            {
                return _horaInicio;
            }

            set
            {
                _horaInicio = value;
            }
        }

        /// <summary>
        /// Horário de termino de atendimento no surto
        /// </summary>
        public int HoraTermino
        {
            get
            {
                return _horaTermino;
            }

            set
            {
                _horaTermino = value;
            }
        }

        /// <summary>
        /// Informa se na campanha também será incluído o titular da compra
        /// </summary>
        public int IncluirTitular
        {
            get
            {
                return _incluirTitular;
            }

            set
            {
                _incluirTitular = value;
            }
        }

        #endregion

        #region GETINFOS

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos os campos da tabela.
        /// </summary>
        private static void GetInfoTabela()
        {
            if (columns == null || (DateTime.Now - columnsUpdated).Minutes > 2)
            {
                columns = BD.Columns.getColumnsInfo(_tabela);
                columnsUpdated = DateTime.Now;
            }
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo empresa.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoEmpresa()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "empresa");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo codigo.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoCodigo()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "codigo");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo nome.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoNome()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "nome");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo identificacao.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoIdentificacao()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "identificacao");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo datacriacao.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoDataCriacao()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "datacriacao");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo dataalteracao.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoDataAlteracao()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "dataalteracao");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo datatermino.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoDataTermino()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "datatermino");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo datainicio.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoDataInicio()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "datainicio");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo termosimagem.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoTermosImagem()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "termosimagem");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo termostexto.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoTermosTexto()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "termostexto");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo nomecontato.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoNomeContato()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "nomecontato");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo datalimite.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoDataLimite()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "datalimite");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo dataabertura.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoDataAbertura()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "dataabertura");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo parcelas.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoParcelas()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "parcelas");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo unicavenda.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoUnicaVenda()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "unicavenda");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo contatowhatsapp.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoContatoWhatsApp()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "contatowhatsapp");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo linkadesao.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoLinkAdesao()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "linkadesao");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo surto.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoSurto()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "surto");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo totalatendimentos.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoTotalAtendimentos()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "totalatendimentos");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo horainicio.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoHoraInicio()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "horainicio");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo horatermino.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoHoraTermino()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "horatermino");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo horatermino.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoIncluirTitular()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "incluirtitular");
        } 

        #endregion

        /// <summary> 
        /// Rotina que cria um Array com os parâmetros utilizados nas ações de INSERT, UPDATE e DELETE
        /// <returns>Array com vários MySqlParameters</returns>
        /// </summary>
        private Array GetParametrosSQL()
        {
            MySqlParameter p;
            ArrayList ap = new ArrayList();

            p = new MySqlParameter("@empresa", Parser.parseInteiro(_idEmpresa.Codigo));
            p.MySqlDbType = GetInfoEmpresa().Type;
            ap.Add(p);

            p = new MySqlParameter("@codigo", Parser.parseInteiro(_codigo));
            p.MySqlDbType = GetInfoCodigo().Type;
            ap.Add(p);

            p = new MySqlParameter("@nome", Parser.parseString(_nome));
            p.MySqlDbType = GetInfoNome().Type;
            ap.Add(p);

            p = new MySqlParameter("@identificacao", Parser.parseString(_identificacao));
            p.MySqlDbType = GetInfoIdentificacao().Type;
            ap.Add(p);

            p = new MySqlParameter("@datacriacao", Parser.parseDateTimeBD(_dataCriacao));
            p.MySqlDbType = GetInfoDataCriacao().Type;
            ap.Add(p);

            p = new MySqlParameter("@dataalteracao", Parser.parseDateTimeBD(_dataAlteracao));
            p.MySqlDbType = GetInfoDataAlteracao().Type;
            ap.Add(p);

            p = new MySqlParameter("@datainicio", Parser.parseDateTimeBD(_dataInicio));
            p.MySqlDbType = GetInfoDataInicio().Type;
            ap.Add(p);

            p = new MySqlParameter("@datatermino", Parser.parseDateTimeBD(_dataTermino));
            p.MySqlDbType = GetInfoDataTermino().Type;
            ap.Add(p);

            p = new MySqlParameter("@datalimite", Parser.parseDateTimeBD(_dataLimite));
            p.MySqlDbType = GetInfoDataLimite().Type;
            ap.Add(p);

            p = new MySqlParameter("@dataabertura", Parser.parseDateTimeBD(_dataAbertura));
            p.MySqlDbType = GetInfoDataAbertura().Type;
            ap.Add(p);

            p = new MySqlParameter("@termosimagem", Parser.parseString(_termosImagem));
            p.MySqlDbType = GetInfoTermosImagem().Type;
            ap.Add(p);

            p = new MySqlParameter("@termostexto", Parser.parseString(_termosTexto));
            p.MySqlDbType = GetInfoTermosTexto().Type;
            ap.Add(p);

            p = new MySqlParameter("@contato", Parser.parseString(_nomeContato));
            p.MySqlDbType = GetInfoNomeContato().Type;
            ap.Add(p);

            p = new MySqlParameter("@parcelas", Parser.parseInteiro(_parcelas));
            p.MySqlDbType = GetInfoParcelas().Type;
            ap.Add(p);

            p = new MySqlParameter("@unicavenda", Parser.parseInteiro(_unicaVenda));
            p.MySqlDbType = GetInfoUnicaVenda().Type;
            ap.Add(p);

            p = new MySqlParameter("@incluirtitular", Parser.parseInteiro(_incluirTitular));
            p.MySqlDbType = GetInfoIncluirTitular().Type;
            ap.Add(p);

            p = new MySqlParameter("@contatowhatsapp", Parser.parseString(_contatoWhatsApp));
            p.MySqlDbType = GetInfoContatoWhatsApp().Type;
            ap.Add(p);

            p = new MySqlParameter("@linkadesao", Parser.parseString(_linkAdesao));
            p.MySqlDbType = GetInfoLinkAdesao().Type;
            ap.Add(p);

            p = new MySqlParameter("@surto", Parser.parseInteiro(_surto));
            p.MySqlDbType = GetInfoSurto().Type;
            ap.Add(p);

            p = new MySqlParameter("@totalatendimentos", Parser.parseInteiro(_totalAtendimentos));
            p.MySqlDbType = GetInfoTotalAtendimentos().Type;
            ap.Add(p);

            p = new MySqlParameter("@horainicio", Parser.parseInteiro(_horaInicio));
            p.MySqlDbType = GetInfoHoraInicio().Type;
            ap.Add(p);

            p = new MySqlParameter("@horatermino", Parser.parseInteiro(_horaTermino));
            p.MySqlDbType = GetInfoHoraTermino().Type;
            ap.Add(p);

            return ap.ToArray();
        }

        /// <summary>
        /// Inicialização das propriedades do elemento com seus valores padrão
        /// </summary>
        private void IniciarElemento()
        {
            GetInfoTabela();

            _idEmpresa = new Empresa();

            _idEmpresa.Codigo = -1;
            _nome = String.Empty;
            _nomeContato = String.Empty;
            _identificacao = String.Empty;
            _termosTexto = String.Empty;
            _termosImagem = String.Empty;
            _dataAlteracao = Utils.dataPadrao;
            _dataCriacao = Utils.dataPadrao;
            _dataInicio = Utils.dataPadrao;
            _dataTermino = Utils.dataPadrao;
            _dataLimite = Utils.dataPadrao;
            _dataAbertura = Utils.dataPadrao;
            _parcelas = 1;
            _unicaVenda = 0;
            _contatoWhatsApp = String.Empty;
            _totalAtendimentos = 0;
            _surto = 0;
            _horaTermino = 0;
            _horaInicio = 0;
            _incluirTitular = 0;
        }

        /// <summary>
        /// Inicialização do elemento
        /// </summary>
        public Campanha()
        {
            IniciarElemento();
        }

        /// <summary>
        /// Rotina que insere o registro no BD
        /// </summary>
        /// <returns>Código do registro inserido no BD</returns>
        public int Incluir()
        {
            if (String.IsNullOrEmpty(_nome) || _dataTermino == Utils.dataPadrao || _dataInicio == Utils.dataPadrao)
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    sql.Parameters.AddRange(GetParametrosSQL());
                    sql.CommandText = "INSERT INTO Campanhas "
                                    + "(nome, identificacao, datacriacao, dataalteracao, datainicio, unicavenda, "
                                    + "contatowhatsapp, linkadesao, horainicio, horatermino, "
                                    + "datatermino, termosimagem, termostexto, nomecontato, empresa, "
                                    + "dataabertura, datalimite, parcelas, surto, totalatendimentos, incluirtitular)  "
                                    + "VALUE (@nome, @identificacao, @datacriacao, @dataalteracao, @datainicio, @unicavenda, "
                                    + "@contatowhatsapp, @linkadesao, @horainicio, @horatermino, "
                                    + "@datatermino, @termosimagem, @termostexto, @contato, @empresa, "
                                    + "@dataabertura, @datalimite, @parcelas, @surto, @totalatendimentos, @incluirtitular); "
                                    + "SELECT LAST_INSERT_ID();";
                    int resultado = Parser.parseInteiro(new Data().executeEscalar(sql));
                    return resultado;
                }
            }
            catch (ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Rotina que altera os dados do registro no BD
        /// </summary>
        /// <returns>Quantidade de linhas alteradas na ação junto ao BD</returns>
        public int Alterar()
        {
            if (_codigo < 0)
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    sql.Parameters.AddRange(GetParametrosSQL());
                    sql.CommandText = "UPDATE Campanhas "
                                    + "SET "
                                    + "identificacao = @identificacao, nome = @nome, termosimagem = @termosimagem, termostexto = @termostexto, "
                                    + "surto = @surto, totalatendimentos = @totalatendimentos, "
                                    + " horainicio = @horainicio, horatermino = @horatermino, incluirtitular = @incluirtitular, "
                                    + "unicavenda = @unicavenda, contatowhatsapp = @contatowhatsapp, linkadesao = @linkadesao, "
                                    + "dataalteracao = @dataalteracao, nomecontato = @contato, datalimite = @datalimite, dataabertura = @dataabertura, "
                                    + "datainicio = @datainicio, datatermino = @datatermino, empresa = @empresa, parcelas = @parcelas "
                                    + "WHERE codigo = @codigo";
                    int resultado = Parser.parseInteiro(new Data().execute(sql));
                    return resultado;
                }
            }
            catch (ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Rotina que exclui o registro do BD
        /// </summary>
        /// <returns>Quantidade de linhas alteradas na ação junto ao BD</returns>
        public int Excluir()
        {
            if (_codigo < 0)
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    sql.Parameters.AddRange(GetParametrosSQL());
                    sql.CommandText = "DELETE FROM Campanhas "
                                    + "WHERE codigo = @codigo";
                    int resultado = Parser.parseInteiro(new Data().execute(sql));
                    return resultado;
                }
            }
            catch (ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Rotina que localiza e retorna um único registro, mediante o código informado
        /// </summary>
        /// <param name="codigo">VALUE: código de identificação do registro (obrigatório)</param>
        /// <returns>Elemento Usuario com os dados retornados da consulta</returns>
        public static Campanha ConsultarUnico(int codigo)
        {
            if (codigo <= 0)
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            ImunneVacinas.PesquisaCampanha pesquisa = new PesquisaCampanha()
            {
                Codigo = codigo
            };

            List<Campanha> retorno = ConsultaGenerica(pesquisa);
            if (retorno == null || retorno.Count.Equals(0))
                return null;
            else
                return retorno[0];
        }

        /// <summary>
        /// Rotina que localiza e retorna um único registro, mediante o código informado
        /// </summary>
        /// <param name="valor">VALUE: campo textual para pesquisa por identificação ou nome (obrigatório)</param>
        /// <returns>Elemento Usuario com os dados retornados da consulta</returns>
        public static Campanha ConsultarUnico(string valor)
        {
            if (String.IsNullOrEmpty(valor))
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            ImunneVacinas.PesquisaCampanha pesquisa = new PesquisaCampanha()
            {
                Identificacao = valor
            };

            List<Campanha> retorno = ConsultaGenerica(pesquisa);
            if (retorno == null || retorno.Count.Equals(0))
            {
                pesquisa = new PesquisaCampanha()
                {
                    Nome = valor
                };

                retorno = ConsultaGenerica(pesquisa);
                if (retorno == null || retorno.Count.Equals(0))
                    return null;
                else
                    return retorno[0];
            }
            else
                return retorno[0];
        }

        /// <summary>
        /// Rotina que retorna um elemento RetornoCBX para utilização em DropLists
        /// </summary>
        /// <param name="InserirLinhaPadrao">VALUE: Informa se será adiconada a linha padrão</param>
        /// <param name="textoPadrao">VALUE: Informa o texto utilizado para linha padão (se utilizado NULL, será apresentado SELECIONE)</param>
        /// <returns>Lista do elemento RetornoCBX com os dados desejados</returns>
        public static List<RetornoComboBox> ConsultarComboBox(bool InserirLinhaPadrao, string textoPadrao)
        {
            DataTable dt = new DataTable();
            try
            {
                dt.Columns.Add("DisplayMember");
                dt.Columns.Add("ValueMember");

                ImunneVacinas.PesquisaCampanha pesquisa = new PesquisaCampanha()
                {
                    Ordenacao = Utils.TipoOrdenacao.porNome
                };

                List<Campanha> retorno = ConsultaGenerica(pesquisa);
                if (retorno != null)
                {
                    foreach (Campanha c in retorno)
                    {
                        dt.Rows.Add(new object[] { String.Format("{0} | {1}", c.Identificacao.ToUpper(), c.Nome.ToUpper()), c.Codigo });
                    }
                }
                return Utils.MontarRetornoComboBox(dt, InserirLinhaPadrao, textoPadrao);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// Rotina que retorna um elemento RetornoCBX para utilização em DropLists
        /// </summary>
        /// <param name="empresa">VALUE: Identificação da empresa (obrigatório)</param>
        /// <param name="InserirLinhaPadrao">VALUE: Informa se será adiconada a linha padrão</param>
        /// <param name="textoPadrao">VALUE: Informa o texto utilizado para linha padão (se utilizado NULL, será apresentado SELECIONE)</param>
        /// <returns>Lista do elemento RetornoCBX com os dados desejados</returns>
        public static List<RetornoComboBox> ConsultarComboBox(int empresa, bool InserirLinhaPadrao, string textoPadrao)
        {
            if (empresa <= 0)
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            DataTable dt = new DataTable();
            try
            {
                dt.Columns.Add("DisplayMember");
                dt.Columns.Add("ValueMember");

                ImunneVacinas.PesquisaCampanha pesquisa = new PesquisaCampanha()
                {
                    Empresa = empresa,
                    Ordenacao = Utils.TipoOrdenacao.porNome
                };

                List<Campanha> retorno = ConsultaGenerica(pesquisa);
                if (retorno != null)
                {
                    foreach (Campanha c in retorno)
                    {
                        StringBuilder displayCampanha = new StringBuilder();
                        displayCampanha.AppendFormat("{0} | ", c.Identificacao.ToUpper());

                        if (c.DataTermino < DateTime.Now.Date)
                            displayCampanha.AppendFormat(" (CONCLUÍDA)");
                        else if (c.DataTermino > DateTime.Now.Date)
                        {
                            if (c.DataInicio <= DateTime.Now.Date)
                                displayCampanha.AppendFormat(" (ATIVA)");
                            else
                                displayCampanha.AppendFormat(" (À REALIZAR)");
                        }
                        else displayCampanha.AppendFormat(" (ATIVA)");

                        displayCampanha.AppendFormat(" - DE {0} ATÉ {1}", c.DataInicio.ToString("dd/MM/yyyy"), c.DataTermino.ToString("dd/MM/yyyy"));

                        dt.Rows.Add(new object[] { displayCampanha.ToString(), c.Codigo });
                    }
                }
                return Utils.MontarRetornoComboBox(dt, InserirLinhaPadrao, textoPadrao);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// Rotina que realiza a pesquisa de elementos mediante os parametros de pesquisa informados
        /// </summary>
        /// <param name="pesquisa">VALUE: elemento de pesquisa com os parametros para consulta</param>
        /// <returns></returns>
        public static List<Campanha> ConsultarCampanhas(PesquisaCampanha pesquisa)
        {
            return ConsultaGenerica(pesquisa);
        }

        /// <summary>
        /// Rotina interna utilizada para todas as pesquisas do elemento
        /// </summary>
        /// <param name="pesquisa">VALUE: Elemento com os parâmetros com os quais será realizada a pesquisa</param>
        /// <returns>Lista do elemento com os dados retornados da consulta</returns>
        internal static List<Campanha> ConsultaGenerica(PesquisaCampanha pesquisa)
        {
            StringBuilder ordenacao = new StringBuilder();
            StringBuilder filtro = new StringBuilder();
            StringBuilder top = new StringBuilder();
            MySqlParameter p;

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    if (pesquisa.Quantidade > 0)
                        top.AppendFormat(" LIMIT {0} ", pesquisa.Quantidade);

                    #region Campo Empresa

                    if (pesquisa.Empresa >= 0)
                    {
                        filtro.Append(" AND Campanhas.empresa = @empresa");
                        p = new MySqlParameter("@empresa", pesquisa.Empresa);
                        p.MySqlDbType = GetInfoEmpresa().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Código

                    if (pesquisa.Codigo >= 0)
                    {
                        filtro.Append(" AND Campanhas.codigo = @codigo");
                        p = new MySqlParameter("@codigo", pesquisa.Codigo);
                        p.MySqlDbType = GetInfoCodigo().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Identificação

                    if (!String.IsNullOrEmpty(pesquisa.Identificacao))
                    {
                        string identificacao = String.Format("{0}", pesquisa.Identificacao);
                        filtro.Append(" AND Campanhas.identificacao = @identificacao ");
                        p = new MySqlParameter("@identificacao", identificacao);
                        p.MySqlDbType = MySqlDbType.VarChar;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Nome

                    if (!String.IsNullOrEmpty(pesquisa.Nome))
                    {
                        string nome = String.Format("{0}", pesquisa.Nome);
                        filtro.Append(" AND Campanhas.nome = @nome ");
                        p = new MySqlParameter("@nome", nome);
                        p.MySqlDbType = MySqlDbType.VarChar;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Surto

                    if (pesquisa.Surto >= 0)
                    {
                        filtro.Append(" AND Campanhas.surto = @surto ");
                        p = new MySqlParameter("@surto", pesquisa.Surto);
                        p.MySqlDbType = GetInfoSurto().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Livre

                    if (!String.IsNullOrEmpty(pesquisa.CampoLivre.Trim()))
                    {
                        string campoLivre = String.Format("%{0}%", pesquisa.CampoLivre.Trim());
                        filtro.Append(" AND (Campanhas.nome LIKE @campoLivre ");
                        filtro.Append(" OR Campanhas.identificacao LIKE @campoLivre ");
                        filtro.Append(" OR Campanhas.termosimagem LIKE @campoLivre ");
                        filtro.Append(" OR Campanhas.nomecontato LIKE @campoLivre ");
                        filtro.Append(" OR Campanhas.termostexto LIKE @campoLivre ");
                        filtro.Append(")");
                        p = new MySqlParameter("@campoLivre", campoLivre);
                        p.MySqlDbType = MySqlDbType.VarChar;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    if (filtro.Length > 0)
                        filtro = filtro.Replace(" AND ", " WHERE ", 0, 5);

                    #region Ordenação

                    switch (pesquisa.Ordenacao)
                    {
                        case Utils.TipoOrdenacao.porCodigo:
                            {
                                ordenacao.Append("ORDER BY Campanhas.codigo");
                            }
                            break;
                        case Utils.TipoOrdenacao.porCodigoDesc:
                            {
                                ordenacao.Append("ORDER BY Campanhas.codigo DESC");
                            }
                            break;
                        case Utils.TipoOrdenacao.porNome:
                            {
                                ordenacao.Append("ORDER BY Campanhas.nome");
                            }
                            break;
                        case Utils.TipoOrdenacao.porNomeDesc:
                            {
                                ordenacao.Append("ORDER BY Campanhas.nome DESC");
                            }
                            break;
                        case Utils.TipoOrdenacao.porData:
                            {
                                ordenacao.Append("ORDER BY Campanhas.datacriacao");
                            }
                            break;
                        case Utils.TipoOrdenacao.porDataDesc:
                            {
                                ordenacao.Append("ORDER BY Campanhas.datacriacao DESC");
                            }
                            break;
                        case Utils.TipoOrdenacao.randomico:
                            {
                                ordenacao.Append("ORDER BY RAND()");
                            }
                            break;
                        default:
                            {
                                ordenacao.Append("ORDER BY Campanhas.codigo");
                            }
                            break;
                    }

                    #endregion

                    sql.CommandText = String.Format("SELECT "
                                    + "Empresas.codigo AS codigoEmpresa, Empresas.nomefantasia AS nomeEmpresa, Empresas.cnpj AS cnpjEmpresa, "
                                    + "Campanhas.* "
                                    + "FROM Campanhas "
                                    + "LEFT JOIN Empresas ON Empresas.codigo = Campanhas.empresa "
                                    + "{0} {1} {2}", filtro, ordenacao, top);
                    DataTable dt = new Data().getData(sql);
                    return MontarRetorno(dt);
                }
            }
            catch (ImunneVacinas.ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Rotina que retorna uma lista genérica do elemento, de acordo com o retorno da Consultar ao BD
        /// </summary>
        /// <param name="dt">VALUE: DataTable com o resultado da Consultar ao BD</param>
        /// <returns>Lista do elemento com os dados retornados da consulta</returns>
        internal static List<Campanha> MontarRetorno(DataTable dt)
        {
            int i = -1;
            List<Campanha> list = new List<Campanha>();

            if (dt == null && dt.Rows.Count == 0)
                return list;
            else
            {
                Campanha retorno;
                try
                {
                    for (i = 0; i < dt.Rows.Count; i++)
                    {
                        retorno = new Campanha()
                        {
                            _codigo = Parser.parseInteiro(dt.Rows[i]["codigo"]),
                            _nome = Parser.parseString(dt.Rows[i]["nome"]),
                            _identificacao = Parser.parseString(dt.Rows[i]["identificacao"]),
                            _nomeContato = Parser.parseString(dt.Rows[i]["nomecontato"]),
                            _termosImagem = Parser.parseString(dt.Rows[i]["termosimagem"]),
                            _termosTexto = Parser.parseString(dt.Rows[i]["termostexto"]),
                            _dataCriacao = Convert.ToDateTime(dt.Rows[i]["datacriacao"]),
                            _dataInicio = Convert.ToDateTime(dt.Rows[i]["datainicio"]),
                            _dataTermino = Convert.ToDateTime(dt.Rows[i]["datatermino"]),
                            _dataLimite = Convert.ToDateTime(dt.Rows[i]["datalimite"]),
                            _dataAbertura = Convert.ToDateTime(dt.Rows[i]["dataabertura"]),
                            _dataAlteracao = Convert.ToDateTime(dt.Rows[i]["dataalteracao"]),
                            _parcelas = Convert.ToInt32(dt.Rows[i]["parcelas"]),
                            _contatoWhatsApp = Parser.parseString(dt.Rows[i]["contatowhatsapp"]),
                            _unicaVenda = Parser.parseInteiro(dt.Rows[i]["unicavenda"]),
                            _linkAdesao = Parser.parseString(dt.Rows[i]["linkadesao"]),
                            _surto = Parser.parseInteiro(dt.Rows[i]["surto"]),
                            _horaInicio = Parser.parseInteiro(dt.Rows[i]["horainicio"]),
                            _horaTermino = Parser.parseInteiro(dt.Rows[i]["horatermino"]),
                            _totalAtendimentos = Parser.parseInteiro(dt.Rows[i]["totalatendimentos"]),
                            _incluirTitular = Parser.parseInteiro(dt.Rows[i]["incluirtitular"])
                        };

                        retorno._idEmpresa.Codigo = Parser.parseInteiro(dt.Rows[i]["codigoEmpresa"]);
                        retorno._idEmpresa.NomeFantasia = Parser.parseString(dt.Rows[i]["nomeEmpresa"]);
                        retorno.IdEmpresa.Cnpj = Parser.parseString(dt.Rows[i]["cnpjEmpresa"]);

                        list.Add(retorno);
                    }
                    return list;
                }
                catch (Exception ex)
                {
                    ex.Data.Add("último retorno lido", i);
                    throw;
                }
            }
        }
    }
}