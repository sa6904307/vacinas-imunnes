﻿using System;
using System.Collections.Generic;

namespace ImunneVacinas
{
    /// <summary>
    /// Elemento para pesquisa de HorarioSurtos
    /// </summary>
    public class PesquisaHorarioSurto
    {
        /// <summary>
        /// Quantidade de itens a serem retornados na pesquisa (-1 para IGNORAR)
        /// </summary>
        public int Quantidade { get; set; }

        /// <summary>
        /// Código a ser pesquisado (-1 para IGNORAR) 
        /// </summary>
        public int Codigo { get; set; }

        /// <summary>
        /// Código a ser pesquisado (-1 para IGNORAR) 
        /// </summary>
        public int CodigoInicial { get; set; }

        /// <summary>
        /// Código da campanha a ser pesquisada (-1 para IGNORAR)
        /// </summary>
        public int Campanha { get; set; }

        /// <summary>
        /// Código do participante a ser pesquisada (-1 para IGNORAR)
        /// </summary>
        public int Participante { get; set; }

        /// <summary>
        /// Código da compra a ser pesquisada (-1 para IGNORAR)
        /// </summary>
        public int Compra { get; set; }

        /// <summary>
        /// Dia a ser pesquisado (Utils.dataPadrao para IGNORAR)
        /// </summary>
        public DateTime Dia { get; set; }

        /// <summary>
        /// Situação a ser pesquisada (INDIFERENTE PARA IGNORAR)
        /// </summary>
        public ImunneVacinas.Utils.SituacaoHorario Situacao { get; set; }

        /// <summary>
        /// Tipo de ordenação da consulta
        /// </summary>
        public Utils.TipoOrdenacao Ordenacao { get; set; }

        /// <summary>
        /// Lista de situações a serem pesquisadas (NULL PARA IGNORAR)
        /// </summary>
        public List<int> ListaSituacoes { get; set; }

        /// <summary>
        /// Inicializando o elemento com os valores padrão
        /// </summary>
        public PesquisaHorarioSurto()
        {
            Quantidade = -1;
            Campanha = -1;
            Participante = -1;
            Codigo = -1;
            CodigoInicial = -1;
            Compra = -1;
            Dia = Utils.dataPadrao;
            Situacao = Utils.SituacaoHorario.Indiferente;
            Ordenacao = Utils.TipoOrdenacao.porCodigo;
            ListaSituacoes = new List<int>();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class PesquisaAdesao
    {
        public int Quantidade { get; set; }

        public int Participante { get; set; }

        public int Campanha { get; set; }

        public int Local { get; set; }

        public int Codigo { get; set; }

        public string CampoLivre { get; set; }

        public Utils.TipoOrdenacao Ordenacao { get; set; }

        public PesquisaAdesao()
        {
            Quantidade = -1;
            Participante = -1;
            Campanha = -1;
            CampoLivre = String.Empty;
            Local = -1;
            Codigo = -1;
            Ordenacao = Utils.TipoOrdenacao.porCodigo;
        }
    }

    /// <summary>
    /// Elemento para pesquisa de PesquisaItemCampanha
    /// </summary>
    public class PesquisaItemCompra
    {
        /// <summary>
        /// Campo para informar a quantidade de retornos (-1 para retornas todos)
        /// </summary>
        public int Quantidade { get; set; }
        /// <summary>
        /// Campo para pesquisar por compra específica (-1 para ignorar)
        /// </summary>
        public int Compra { get; set; }
        /// <summary>
        /// Campo para pesquisar por vacina específica (-1 para ignorar)
        /// </summary>
        public int Vacina { get; set; }
        /// <summary>
        /// Pesquisa pelo código da empresa (-1 para ignorar)
        /// </summary>
        public int Empresa { get; set; }
        /// <summary>
        /// Sigla do estado da clínica (NULL para ignorar)
        /// </summary>
        public string Uf { get; set; }
        /// <summary>
        /// Pesquisa pelo código da cidade da clínica (-1 para ignorar)
        /// </summary>
        public int Cidade { get; set; }
        /// <summary>
        /// Campo para pesquisar por código específico (-1 para ignorar)
        /// </summary>
        public int Codigo { get; set; }
        /// <summary>
        /// Campo para pesquisar pelo clinica (-1 para ignorar)
        /// </summary>
        public int Clinica { get; set; }
        /// <summary>
        /// Campo para pesquisar pelo campanha (-1 para ignorar)
        /// </summary>
        public int Campanha { get; set; }
        /// <summary>
        /// Informa o tipo de ordenação do retorno da pesquisa (padrão porCodigo)
        /// </summary>
        public Utils.TipoOrdenacao Ordenacao { get; set; }
        /// <summary>
        /// Informa o tipo de ordenação do retorno da pesquisa (INDIFERENTE para ignorar)
        /// </summary>
        public ImunneVacinas.Compra.SituacaoCompra Situacao { get; set; }
        /// <summary>
        /// Informa o tipo de ordenação do retorno da pesquisa (INDIFERENTE para ignorar)
        /// </summary>
        public ImunneVacinas.Compra.CategoriaCompra Categoria { get; set; }

        /// <summary>
        /// Inicializando o elemento
        /// </summary>
        public PesquisaItemCompra()
        {
            Quantidade = -1;
            Campanha = -1;
            Clinica = -1;
            Situacao = ImunneVacinas.Compra.SituacaoCompra.Indiferente;
            Categoria = ImunneVacinas.Compra.CategoriaCompra.Indiferente;
            Codigo = -1;
            Empresa = -1;
            Cidade = -1;
            Uf = String.Empty;
            Vacina = -1;
            Compra = -1;
            Ordenacao = Utils.TipoOrdenacao.porCodigo;
        }
    }

    /// <summary>
    /// Elemento para pesquisa de PesquisaVoucher
    /// </summary>
    public class PesquisaVoucher
    {
        /// <summary>
        /// Campo para informar a quantidade de retornos (-1 para retornas todos)
        /// </summary>
        public int Quantidade { get; set; }
        /// <summary>
        /// Campo para pesquisar por campanha específica (-1 para ignorar)
        /// </summary>
        public int Campanha { get; set; }
        /// <summary>
        /// Campo para pesquisar por compra específica (-1 para ignorar)
        /// </summary>
        public int Compra { get; set; }
        /// <summary>
        /// Campo para pesquisar por clinica específica (-1 para ignorar)
        /// </summary>
        public int Clinica { get; set; }
        /// <summary>
        /// Campo para pesquisar por código específico (-1 para ignorar)
        /// </summary>
        public int Codigo { get; set; }
        /// <summary>
        /// Campo para pesquisar pelo participante (-1 para ignorar)
        /// </summary>
        public int Participante { get; set; }
        /// <summary>
        /// Informa o tipo de ordenação do retorno da pesquisa (padrão porCodigo)
        /// </summary>
        public Utils.TipoOrdenacao Ordenacao { get; set; }
        /// <summary>
        /// Informa o tipo de ordenação do retorno da pesquisa (INDIFERENTE para ignorar)
        /// </summary>
        public ImunneVacinas.Voucher.SituacaoVoucher Situacao { get; set; }

        /// <summary>
        /// Inicializando o elemento
        /// </summary>
        public PesquisaVoucher()
        {
            Quantidade = -1;
            Codigo = -1;
            Participante = -1;
            Campanha = -1;
            Clinica = -1;
            Compra = -1;
            Ordenacao = Utils.TipoOrdenacao.porCodigo;
            Situacao = Voucher.SituacaoVoucher.Indiferente;
        }
    }

    /// <summary>
    /// Elemento para pesquisa de Compras
    /// </summary>
    public class PesquisaCompra
    {
        /// <summary>
        /// Campo para informar a quantidade de retornos (-1 para retornas todos)
        /// </summary>
        public int Quantidade { get; set; }

        /// <summary>
        /// Identificação da empresa dona da campanha da compra
        /// </summary>
        public int Empresa { get; set; }

        /// <summary>
        /// Campo para pesquisar por código específico (-1 para ignorar)
        /// </summary>
        public int Codigo { get; set; }

        /// <summary>
        /// Campo para pesquisar pela sigla da UF (NULL para ignorar)
        /// </summary>
        public string Uf { get; set; }

        /// <summary>
        /// Campo para pesquisar pelo código da cidade (-1 para ignorar)
        /// </summary>
        public int Cidade { get; set; }

        /// <summary>
        /// Campo para pesquisar pelo participante (-1 para ignorar)
        /// </summary>
        public int Participante { get; set; }

        /// <summary>
        /// Campo para pesquisar pelo clinica (-1 para ignorar)
        /// </summary>
        public int Clinica { get; set; }

        /// <summary>
        /// Campo para pesquisar pelo campanha (-1 para ignorar)
        /// </summary>
        public int Campanha { get; set; }

        /// <summary>
        /// Campo para pesquisa via campo livre (NULL para ignorar)
        /// </summary>
        public string CampoLivre { get; set; }

        /// <summary>
        /// Informa o tipo de ordenação do retorno da pesquisa (padrão porCodigo)
        /// </summary>
        public Utils.TipoOrdenacao Ordenacao { get; set; }

        /// <summary>
        /// Informa o tipo de ordenação do retorno da pesquisa (INDIFERENTE para ignorar)
        /// </summary>
        public ImunneVacinas.Compra.SituacaoCompra Situacao { get; set; }

        /// <summary>
        /// Informa se a compra já teve aplicação realizada ou não
        /// </summary>
        public ImunneVacinas.Compra.Aplicacao Aplicacao { get; set; }

        /// <summary>
        /// Informa o tipo de ordenação do retorno da pesquisa (INDIFERENTE para ignorar)
        /// </summary>
        public ImunneVacinas.Compra.CategoriaCompra Categoria { get; set; }

        /// <summary>
        /// Data de inicio do período para pesquisa (DataPadrao para ignorar)
        /// </summary>
        public DateTime DataInicio { get; set; }

        /// <summary>
        /// Data de termino do período para pesquisa (DataPadrao para ignorar)
        /// </summary>
        public DateTime DataFim { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool Valor { get; set; }

        /// <summary>
        /// Considera compras (PAGAS) liberadas ou não (-1 para ignorar)
        /// </summary>
        public int Liberados { get; set; }

        /// <summary>
        /// Inicializando o elemento
        /// </summary>
        public PesquisaCompra()
        {
            Quantidade = -1;
            Empresa = -1;
            Codigo = -1;
            Participante = -1;
            Campanha = -1;
            Clinica = -1;
            Liberados = -1;
            Uf = String.Empty;
            Cidade = -1;
            CampoLivre = String.Empty;
            Ordenacao = Utils.TipoOrdenacao.porCodigo;
            Situacao = Compra.SituacaoCompra.Indiferente;
            Categoria = Compra.CategoriaCompra.Indiferente;
            DataInicio = Utils.dataPadrao;
            DataFim = Utils.dataPadrao;
            Aplicacao = Compra.Aplicacao.Indiferente;
            Valor = false;
        }
    }

    /// <summary>
    /// Elemento para pesquisa de Participante
    /// </summary>
    public class PesquisaParticipante
    {
        /// <summary>
        /// Campo para informar a quantidade de retornos (-1 para retornas todos)
        /// </summary>
        public int Quantidade { get; set; }
        /// <summary>
        /// Campo para pesquisar por código específico (-1 para ignorar)
        /// </summary>
        public int Codigo { get; set; }
        /// <summary>
        /// Campo para pequisa pelo grau de parentesco (-1 para ignorar)
        /// </summary>
        public int Parentesco { get; set; }
        /// <summary>
        /// Campo para peqsquisa pelo parente pai (-1 para ignorar)
        /// </summary>
        public int ParticipantePai { get; set; }
        /// <summary>
        /// Campo para pesquisar por CPF (NULL para ignorar)
        /// </summary>
        public string Cpf { get; set; }
        /// <summary>
        /// Campo para pesquisa por nome (NULL para ignorar)
        /// </summary>
        public string Nome { get; set; }
        /// <summary>
        /// Campo livre para pesquisar (NULL para ignorar)
        /// </summary>
        public string CampoLivre { get; set; }
        /// <summary>
        /// Informa o tipo de ordenação do retorno da pesquisa (padrão porCodigo)
        /// </summary>
        public Utils.TipoOrdenacao Ordenacao { get; set; }

        /// <summary>
        /// Inicializando o elemento
        /// </summary>
        public PesquisaParticipante()
        {
            Quantidade = -1;
            Codigo = -1;
            Cpf = String.Empty;
            ParticipantePai = -1;
            Nome = String.Empty;
            Parentesco = -1;
            CampoLivre = String.Empty;
            Ordenacao = Utils.TipoOrdenacao.porCodigo;
        }
    }

    /// <summary>
    /// Elemento para pesquisa de Clinica
    /// </summary>
    public class PesquisaClinica
    {
        /// <summary>
        /// Campo para informar a quantidade de retornos (-1 para retornas todos)
        /// </summary>
        public int Quantidade { get; set; }
        /// <summary>
        /// Campo para pesquisar por código específico (-1 para ignorar)
        /// </summary>
        public int Codigo { get; set; }
        /// <summary>
        /// Campo para pesquisar por sigla de UF específica (NULL para ignorar)
        /// </summary>
        public string Uf { get; set; }
        /// <summary>
        /// Campo para pesquisa pelo código da cidade (-1 para IGNORAR)
        /// </summary>
        public int Cidade { get; set; }
        /// <summary>
        /// Campo para pesquisar por CPF/CNPJ (NULL para ignorar)
        /// </summary>
        public string CpfCnpj { get; set; }
        /// <summary>
        /// Campo livre para pesquisar (NULL para ignorar)
        /// </summary>
        public string CampoLivre { get; set; }
        /// <summary>
        /// Informa o tipo de ordenação do retorno da pesquisa (padrão porCodigo)
        /// </summary>
        public Utils.TipoOrdenacao Ordenacao { get; set; }

        /// <summary>
        /// Inicializando o elemento
        /// </summary>
        public PesquisaClinica()
        {
            Quantidade = -1;
            Codigo = -1;
            Uf = String.Empty;
            Cidade = -1;
            CpfCnpj = String.Empty;
            CampoLivre = String.Empty;
            Ordenacao = Utils.TipoOrdenacao.porCodigo;
        }
    }

    /// <summary>
    /// Elemento para pesquisa de Cidade
    /// </summary>
    public class PesquisaCidade
    {
        /// <summary>
        /// Campo para informar a quantidade de retornos (-1 para retornas todos)
        /// </summary>
        public int Quantidade { get; set; }
        /// <summary>
        /// Campo para pesquisar por código específico (-1 para ignorar)
        /// </summary>
        public int Codigo { get; set; }
        /// <summary>
        /// Campo para pesquisar por sigla de UF específica (NULL para ignorar)
        /// </summary>
        public string Uf { get; set; }
        /// <summary>
        /// Campo livre para pesquisar (NULL para ignorar)
        /// </summary>
        public string CampoLivre { get; set; }
        /// <summary>
        /// Informa o tipo de ordenação do retorno da pesquisa (padrão porCodigo)
        /// </summary>
        public Utils.TipoOrdenacao Ordenacao { get; set; }

        /// <summary>
        /// Inicializando o elemento
        /// </summary>
        public PesquisaCidade()
        {
            Quantidade = -1;
            Codigo = -1;
            Uf = String.Empty;
            CampoLivre = String.Empty;
            Ordenacao = Utils.TipoOrdenacao.porCodigo;
        }
    }

    /// <summary>
    /// Elemento para pesquisa de UF
    /// </summary>
    public class PesquisaUF
    {
        /// <summary>
        /// Campo para informar a quantidade de retornos (-1 para retornas todos)
        /// </summary>
        public int Quantidade { get; set; }
        /// <summary>
        /// Campo para pesquisar por código específico (-1 para ignorar)
        /// </summary>
        public int Codigo { get; set; }
        /// <summary>
        /// Campo para pesquisar por sigla específica (NULL para ignorar)
        /// </summary>
        public string Sigla { get; set; }
        /// <summary>
        /// Campo livre para pesquisar (NULL para ignorar)
        /// </summary>
        public string CampoLivre { get; set; }
        /// <summary>
        /// Informa o tipo de ordenação do retorno da pesquisa (padrão porCodigo)
        /// </summary>
        public Utils.TipoOrdenacao Ordenacao { get; set; }

        /// <summary>
        /// Inicializando o elemento
        /// </summary>
        public PesquisaUF()
        {
            Quantidade = -1;
            Codigo = -1;
            Sigla = String.Empty;
            CampoLivre = String.Empty;
            Ordenacao = Utils.TipoOrdenacao.porCodigo;
        }
    }

    /// <summary>
    /// Elemento para pesquisa de CampanhaClinica
    /// </summary>
    public class PesquisaCampanhaClinica
    {
        /// <summary>
        /// Campo para informar a quantidade de retornos (-1 para retornas todos)
        /// </summary>
        public int Quantidade { get; set; }
        /// <summary>
        /// Campo para pesquisar por campanha específico (-1 para ignorar)
        /// </summary>
        public int Campanha { get; set; }
        /// <summary>
        /// Campo para pesquisar por vacina específico (-1 para ignorar)
        /// </summary>
        public int Clinica { get; set; }
        /// <summary>
        /// Campo para pesquisar por sigla de UF específica (NULL para ignorar)
        /// </summary>
        public string Uf { get; set; }
        /// <summary>
        /// Campo para pesquisa pelo código da cidade (-1 para IGNORAR)
        /// </summary>
        public int Cidade { get; set; }
        /// <summary>
        /// Campo livre para pesquisar (NULL para ignorar)
        /// </summary>
        public string CampoLivre { get; set; }
        /// <summary>
        /// Campo para pesquisar por código específico (-1 para ignorar)
        /// </summary>
        public int Codigo { get; set; }
        /// <summary>
        /// Informa o tipo de ordenação do retorno da pesquisa (padrão porCodigo)
        /// </summary>
        public Utils.TipoOrdenacao Ordenacao { get; set; }

        /// <summary>
        /// Inicializando o elemento
        /// </summary>
        public PesquisaCampanhaClinica()
        {
            Quantidade = -1;
            Campanha = -1;
            Uf = String.Empty;
            Cidade = -1;
            CampoLivre = String.Empty;
            Clinica = -1;
            Codigo = -1;
            Ordenacao = Utils.TipoOrdenacao.porCodigo;
        }
    }

    /// <summary>
    /// Elemento para pesquisa de ProdutoCampanha
    /// </summary>
    public class PesquisaProdutoCampanha
    {
        /// <summary>
        /// Campo para informar a quantidade de retornos (-1 para retornas todos)
        /// </summary>
        public int Quantidade { get; set; }
        /// <summary>
        /// Campo para pesquisar por campanha específico (-1 para ignorar)
        /// </summary>
        public int Campanha { get; set; }
        /// <summary>
        /// Campo para pesquisar por vacina específico (-1 para ignorar)
        /// </summary>
        public int Vacina { get; set; }
        /// <summary>
        /// Campo para pesquisar por código específico (-1 para ignorar)
        /// </summary>
        public int Codigo { get; set; }
        /// <summary>
        /// Campo livre para pesquisa (NULL para ignorar)
        /// </summary>
        public string CampoLivre { get; set; }
        /// <summary>
        /// Informa o tipo de ordenação do retorno da pesquisa (padrão porCodigo)
        /// </summary>
        public Utils.TipoOrdenacao Ordenacao { get; set; }

        /// <summary>
        /// Inicializando o elemento
        /// </summary>
        public PesquisaProdutoCampanha()
        {
            Quantidade = -1;
            Campanha = -1;
            Vacina = -1;
            Codigo = -1;
            CampoLivre = String.Empty;
            Ordenacao = Utils.TipoOrdenacao.porCodigo;
        }
    }

    /// <summary>
    /// Elemento para pesquisa de GrauParentesco
    /// </summary>
    public class PesquisaGrauParentesco
    {
        /// <summary>
        /// Campo para informar a quantidade de retornos (-1 para retornas todos)
        /// </summary>
        public int Quantidade { get; set; }
        /// <summary>
        /// Campo para pesquisar por código específico (-1 para ignorar)
        /// </summary>
        public int Codigo { get; set; }
        /// <summary>
        /// Razão social da GrauParentesco (NULL para ignorar)
        /// </summary>
        public string Nome { get; set; }
        /// <summary>
        /// Campo livre para pesquisa (NULL para ignorar)
        /// </summary>
        public string CampoLivre { get; set; }
        /// <summary>
        /// Informa o tipo de ordenação do retorno da pesquisa (padrão porCodigo)
        /// </summary>
        public Utils.TipoOrdenacao Ordenacao { get; set; }
        /// <summary>
        /// Informa o tipo de ordenação do retorno da pesquisa (INDIFERENTE para ignorar)
        /// </summary>
        public Utils.SituacaoRegistro Situacao { get; set; }

        /// <summary>
        /// Inicializando o elemento
        /// </summary>
        public PesquisaGrauParentesco()
        {
            Quantidade = -1;
            Codigo = -1;
            CampoLivre = String.Empty;
            Ordenacao = Utils.TipoOrdenacao.porCodigo;
            Situacao = Utils.SituacaoRegistro.Indiferente;
        }
    }

    /// <summary>
    /// Elemento para pesquisa de Vacinas
    /// </summary>
    public class PesquisaVacina
    {
        /// <summary>
        /// Campo para informar a quantidade de retornos (-1 para retornas todos)
        /// </summary>
        public int Quantidade { get; set; }
        /// <summary>
        /// Campo para pesquisar por código específico (-1 para ignorar)
        /// </summary>
        public int Codigo { get; set; }
        /// <summary>
        /// Razão social da Vacina (NULL para ignorar)
        /// </summary>
        public string Nome { get; set; }
        /// <summary>
        /// Campo livre para pesquisa (NULL para ignorar)
        /// </summary>
        public string CampoLivre { get; set; }
        /// <summary>
        /// Informa o tipo de ordenação do retorno da pesquisa (padrão porCodigo)
        /// </summary>
        public Utils.TipoOrdenacao Ordenacao { get; set; }
        /// <summary>
        /// Informa o tipo de ordenação do retorno da pesquisa (INDIFERENTE para ignorar)
        /// </summary>
        public Utils.SituacaoRegistro Situacao { get; set; }

        /// <summary>
        /// Inicializando o elemento
        /// </summary>
        public PesquisaVacina()
        {
            Quantidade = -1;
            Codigo = -1;
            CampoLivre = String.Empty;
            Ordenacao = Utils.TipoOrdenacao.porCodigo;
            Situacao = Utils.SituacaoRegistro.Indiferente;
        }
    }

    /// <summary>
    /// Elemento para pesquisa de Campanhas
    /// </summary>
    public class PesquisaCampanha
    {
        /// <summary>
        /// Campo para informar a quantidade de retornos (-1 para retornas todos)
        /// </summary>
        public int Quantidade { get; set; }

        /// <summary>
        /// Campo para pesquisar por empresa específica (-1 para ignorar)
        /// </summary>
        public int Empresa { get; set; }

        /// <summary>
        /// Campo para pesquisar por código específico (-1 para ignorar)
        /// </summary>
        public int Codigo { get; set; }

        /// <summary>
        /// Identificação da campanha (NULL para ignorar)
        /// </summary>
        public string Identificacao { get; set; }

        /// <summary>
        /// Razão social da Campanha (NULL para ignorar)
        /// </summary>
        public string Nome { get; set; }

        /// <summary>
        /// Campo livre para pesquisa (NULL para ignorar)
        /// </summary>
        public string CampoLivre { get; set; }

        /// <summary>
        /// Informa o tipo de ordenação do retorno da pesquisa (padrão porCodigo)
        /// </summary>
        public Utils.TipoOrdenacao Ordenacao { get; set; }

        /// <summary>
        /// Informa o tipo de ordenação do retorno da pesquisa (INDIFERENTE para ignorar)
        /// </summary>
        public Utils.SituacaoRegistro Situacao { get; set; }

        /// <summary>
        /// Identifica campanhas normais ou de surto (-1 para ignorar)
        /// </summary>
        public int Surto { get; set; }

        /// <summary>
        /// Inicializando o elemento
        /// </summary>
        public PesquisaCampanha()
        {
            Quantidade = -1;
            Codigo = -1;
            Empresa = -1;
            Surto = -1;
            Identificacao = String.Empty;
            CampoLivre = String.Empty;
            Ordenacao = Utils.TipoOrdenacao.porCodigo;
            Situacao = Utils.SituacaoRegistro.Indiferente;
        }
    }

    /// <summary>
    /// Elemento para pesquisa de Empresas
    /// </summary>
    public class PesquisaEmpresa
    {
        /// <summary>
        /// Campo para informar a quantidade de retornos (-1 para retornas todos)
        /// </summary>
        public int Quantidade { get; set; }
        /// <summary>
        /// Campo para pesquisar por código específico (-1 para ignorar)
        /// </summary>
        public int Codigo { get; set; }
        /// <summary>
        /// Campo para pesquisar por classificação da empresa (-1 para ignorar)
        /// </summary>
        public int Classificacao { get; set; }
        /// <summary>
        /// ID do Empresa (NULL para ignorar)
        /// </summary>
        public string CpfCnpj { get; set; }
        /// <summary>
        /// Razão social da Empresa (NULL para ignorar)
        /// </summary>
        public string RazaoSocial { get; set; }
        /// <summary>
        /// Campo para pesquisar por cidade específica (-1 para ignorar)
        /// </summary>
        public int Cidade { get; set; }
        /// <summary>
        /// Sigla do estado (NULL para ignorar)
        /// </summary>
        public string UF { get; set; }
        /// <summary>
        /// Campo livre para pesquisa (NULL para ignorar)
        /// </summary>
        public string CampoLivre { get; set; }
        /// <summary>
        /// Informa o tipo de ordenação do retorno da pesquisa (padrão porCodigo)
        /// </summary>
        public Utils.TipoOrdenacao Ordenacao { get; set; }
        /// <summary>
        /// Informa o tipo de ordenação do retorno da pesquisa (INDIFERENTE para ignorar)
        /// </summary>
        public Utils.SituacaoRegistro Situacao { get; set; }

        /// <summary>
        /// Inicializando o elemento
        /// </summary>
        public PesquisaEmpresa()
        {
            Quantidade = -1;
            Codigo = -1;
            Classificacao = -1;
            UF = String.Empty;
            Cidade = -1;
            CpfCnpj = String.Empty;
            CampoLivre = String.Empty;
            Ordenacao = Utils.TipoOrdenacao.porCodigo;
            Situacao = Utils.SituacaoRegistro.Indiferente;
        }
    }

    /// <summary>
    /// Elemento para pesquisa de Usuarios
    /// </summary>
    public class PesquisaUsuario
    {
        /// <summary>
        /// Campo para informar a quantidade de retornos (-1 para retornas todos)
        /// </summary>
        public int Quantidade { get; set; }
        /// <summary>
        /// Campo para pesquisar por código específico (-1 para ignorar)
        /// </summary>
        public int Codigo { get; set; }
        /// <summary>
        /// Campo para pesquisar por cliente específico (-1 para ignorar)
        /// </summary>
        public int Cliente { get; set; }
        /// <summary>
        /// Campo para pesquisar por clinica específica (-1 para ignorar)
        /// </summary>
        public int Clinica { get; set; }
        /// <summary>
        /// Campo para pesquisar por tipo específico (-1 para ignorar)
        /// </summary>
        public int Tipo { get; set; }
        /// <summary>
        /// Campo Login para pesquisa (NULL para ignorar)
        /// </summary>
        public string Login { get; set; }
        /// <summary>
        /// Campo Senha para pesquisa (NULL para ignorar)
        /// </summary>
        public string Senha { get; set; }
        /// <summary>
        /// CpfCnpj para pesquisa de usuário (NULL para ignorar)
        /// </summary>
        public string CpfCnpj { get; set; }
        /// <summary>
        /// Campo livre para pesquisa (NULL para ignorar)
        /// </summary>
        public string CampoLivre { get; set; }
        /// <summary>
        /// Informa o tipo de ordenação do retorno da pesquisa (padrão porCodigo)
        /// </summary>
        public Utils.TipoOrdenacao Ordenacao { get; set; }
        /// <summary>
        /// Informa o tipo de ordenação do retorno da pesquisa (INDIFERENTE para ignorar)
        /// </summary>
        public Utils.SituacaoRegistro Situacao { get; set; }

        /// <summary>
        /// Inicializando o elemento
        /// </summary>
        public PesquisaUsuario()
        {
            Quantidade = -1;
            Tipo = -1;
            Codigo = -1;
            Cliente = -1;
            Clinica = -1;
            CampoLivre = String.Empty;
            CpfCnpj = String.Empty;
            Login = String.Empty;
            Senha = String.Empty;
            Ordenacao = Utils.TipoOrdenacao.porCodigo;
            Situacao = Utils.SituacaoRegistro.Indiferente;
        }
    }

    /// <summary>
    /// Elemento para pesquisa de TiposUsuarios
    /// </summary>
    public class PesquisaTipoUsuario
    {
        /// <summary>
        /// Campo para informar a quantidade de retornos (-1 para retornas todos)
        /// </summary>
        public int Quantidade { get; set; }
        /// <summary>
        /// Campo para pesquisar por código específico (-1 para ignorar)
        /// </summary>
        public int Codigo { get; set; }
        /// <summary>
        /// Nome do registro (NULL para ignorar)
        /// </summary>
        public string Nome { get; set; }
        /// <summary>
        /// Campo livre para pesquisa (NULL para ignorar)
        /// </summary>
        public string CampoLivre { get; set; }
        /// <summary>
        /// Informa o tipo de ordenação do retorno da pesquisa (padrão porCodigo)
        /// </summary>
        public Utils.TipoOrdenacao Ordenacao { get; set; }
        /// <summary>
        /// Informa o tipo de ordenação do retorno da pesquisa (INDIFERENTE para ignorar)
        /// </summary>
        public Utils.SituacaoRegistro Situacao { get; set; }

        /// <summary>
        /// Inicializando o elemento
        /// </summary>
        public PesquisaTipoUsuario()
        {
            Quantidade = -1;
            Codigo = -1;
            Nome = String.Empty;
            CampoLivre = String.Empty;
            Ordenacao = Utils.TipoOrdenacao.porCodigo;
            Situacao = Utils.SituacaoRegistro.Indiferente;
        }
    }

    /// <summary>
    /// Elemento utilizado para pesquisa de Menus
    /// </summary>
    public class PesquisaMenu
    {
        /// <summary>
        /// Campo para informar a quantidade de retornos (-1 para retornas todos)
        /// </summary>
        public int Quantidade { get; set; }
        /// <summary>
        /// Campo para pesquisar por código específico (-1 para ignorar)
        /// </summary>
        public int Codigo { get; set; }
        /// <summary>
        /// Campo para pesquisar por código específico (-1 para ignorar)
        /// </summary>
        public int Grupo { get; set; }
        /// <summary>
        /// Nome para pesquisa (NULL para ignorar)
        /// </summary>
        public string Nome { get; set; }
        /// <summary>
        /// Campo livre para pesquisa (NULL para ignorar)
        /// </summary>
        public string CampoLivre { get; set; }
        /// <summary>
        /// Informa o tipo de ordenação do retorno da pesquisa (padrão porCodigo)
        /// </summary>
        public Utils.TipoOrdenacao Sequencia { get; set; }

        /// <summary>
        /// Inicializando o elemento
        /// </summary>
        public PesquisaMenu()
        {
            Quantidade = -1;
            Codigo = -1;
            Grupo = -1;
            CampoLivre = String.Empty;
            Sequencia = Utils.TipoOrdenacao.porCodigo;
        }
    }

    /// <summary>
    /// Elemento utilizado para pesquisa de Permissões
    /// </summary>
    public class PesquisaLinkCampanha
    {
        /// <summary>
        /// Campo para informar a quantidade de retornos (-1 para retornas todos)
        /// </summary>
        public int Quantidade { get; set; }
        /// <summary>
        /// Campo para pesquisar por campanha específica (-1 para ignorar)
        /// </summary>
        public int Campanha { get; set; }
        /// <summary>
        /// Campo para pesquisar por clinica específica (-1 para ignorar)
        /// </summary>
        public int Clinica { get; set; }
        /// <summary>
        /// Campo para pesquisar por código específico (-1 para ignorar)
        /// </summary>
        public int Codigo { get; set; }
        /// <summary>
        /// Informa o tipo de ordenação do retorno da pesquisa (padrão porCodigo)
        /// </summary>
        public Utils.TipoOrdenacao Ordenacao { get; set; }

        /// <summary>
        /// Inicializando o elemento
        /// </summary>
        public PesquisaLinkCampanha()
        {
            Quantidade = -1;
            Clinica = -1;
            Campanha = -1;
            Codigo = -1;
            Ordenacao = Utils.TipoOrdenacao.porCodigo;
        }
    }

    /// <summary>
    /// Elemento utilizado para pesquisa de Permissões
    /// </summary>
    public class PesquisaPermissao
    {
        /// <summary>
        /// Campo para informar a quantidade de retornos (-1 para retornas todos)
        /// </summary>
        public int Quantidade { get; set; }
        /// <summary>
        /// Campo para pesquisar por cliente específico (-1 para ignorar)
        /// </summary>
        public int Cliente { get; set; }
        /// <summary>
        /// Campo para pesquisar por grupo específico (-1 para ignorar)
        /// </summary>
        public int Grupo { get; set; }
        /// <summary>
        /// Campo para pesquisar por menu específico (-1 para ignorar)
        /// </summary>
        public int Menu { get; set; }
        /// <summary>
        /// Campo para pesquisar por tipoUsuario específico (-1 para ignorar)
        /// </summary>
        public int TipoUsuario { get; set; }
        /// <summary>
        /// Campo para pesquisar por código específico (-1 para ignorar)
        /// </summary>
        public int Codigo { get; set; }
        /// <summary>
        /// Informa o tipo de ordenação do retorno da pesquisa (padrão porCodigo)
        /// </summary>
        public Utils.TipoOrdenacao Sequencia { get; set; }

        /// <summary>
        /// Inicializando o elemento
        /// </summary>
        public PesquisaPermissao()
        {
            Quantidade = -1;
            Cliente = -1;
            Grupo = -1;
            Menu = -1;
            TipoUsuario = -1;
            Codigo = -1;
            Sequencia = Utils.TipoOrdenacao.porCodigo;
        }
    }

    /// <summary>
    /// Elemento utilizado para pesquisa de Log
    /// </summary>
    public class PesquisaLog
    {
        public int Quantidade { get; set; }
        public int Codigo { get; set; }
        public int IdCliente { get; set; }
        public int IdUsuario { get; set; }
        public string CampoLivre { get; set; }
        public Utils.AcaoLog AcaoRealizada { get; set; }
        public DateTime dataInicio { get; set; }
        public DateTime dataTermino { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public PesquisaLog()
        {
            Quantidade = -1;
            Codigo = -1;
            IdCliente = -1;
            IdUsuario = -1;
            CampoLivre = String.Empty;
            AcaoRealizada = Utils.AcaoLog.Indiferente;
            dataInicio = Utils.dataPadrao;
            dataTermino = Utils.dataPadrao;
        }
    }

    /// <summary>
    /// Elemento utilizado para pesquisa de TransacaoRealizada
    /// </summary>
    public class PesquisaTransacaoRealizada
    {
        /// <summary>
        /// Campo para informar a quantidade de retornos (-1 para retornas todos)
        /// </summary>
        public int Quantidade { get; set; }
        /// <summary>
        /// Campo para pesquisar por compra específica (-1 para ignorar)
        /// </summary>
        public int Compra { get; set; }
        /// <summary>
        /// Campo para pesquisar por código específico (-1 para ignorar)
        /// </summary>
        public int Codigo { get; set; }
        /// <summary>
        /// Informa o tipo de ordenação do retorno da pesquisa (padrão porCodigo)
        /// </summary>
        public Utils.TipoOrdenacao Ordenacao { get; set; }

        /// <summary>
        /// Inicializando o elemento
        /// </summary>
        public PesquisaTransacaoRealizada()
        {
            Quantidade = -1;
            Compra = -1;
            Codigo = -1;
            Ordenacao = Utils.TipoOrdenacao.porDataDesc;
        }
    }

    /// <summary>
    /// Elemento utilizado para pesquisa de MovimentacoesEstoque
    /// </summary>
    public class PesquisaMovimentacaoEstoque
    {
        /// <summary>
        /// Campo para informar a quantidade de retornos (-1 para retornas todos)
        /// </summary>
        public int Quantidade { get; set; }

        /// <summary>
        /// Campo para pesquisar por clinica específica (-1 para ignorar)
        /// </summary>
        public int Clinica { get; set; }

        /// <summary>
        /// Campo para pesquisar por usuário específico (-1 para ignorar)
        /// </summary>
        public int Usuario { get; set; }

        /// <summary>
        /// Campo para pesquisar por produto específico (-1 para ignorar)
        /// </summary>
        public int Produto { get; set; }

        /// <summary>
        /// Campo para pesquisar por código específico (-1 para ignorar)
        /// </summary>
        public int Codigo { get; set; }

        /// <summary>
        /// Campo livre para pesquisa
        /// </summary>
        public string CampoLivre { get; set; }

        /// <summary>
        /// Campo para pesquisar pelo tipo de movitenção (INDIFERENTE para ignorar)
        /// </summary>
        public Utils.TipoMovimentacao Tipo { get; set; }

        /// <summary>
        /// Ordenação dos resultados da consulta
        /// </summary>
        public Utils.TipoOrdenacao Ordenacao { get; set; }

        /// <summary>
        /// Data de inicio do período para pesquisa (DataPadrao para ignorar)
        /// </summary>
        public DateTime DataInicio { get; set; }

        /// <summary>
        /// Data de termino do período para pesquisa (DataPadrao para ignorar)
        /// </summary>
        public DateTime DataFim { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public PesquisaMovimentacaoEstoque()
        {
            Quantidade = -1;
            Clinica = -1;
            Usuario = -1;
            Produto = -1;
            Codigo = -1;
            CampoLivre = String.Empty;
            Tipo = Utils.TipoMovimentacao.Indiferente;
            Ordenacao = Utils.TipoOrdenacao.porCodigo;
            DataInicio = Utils.dataPadrao;
            DataFim = Utils.dataPadrao;
        }
    }

    /// <summary>
    /// Elemento utilizado para pesquisa de LocalAdesao
    /// </summary>
    public class PesquisaLocalAdesao
    {
        /// <summary>
        /// Campo para informar a quantidade de retornos (-1 para retornas todos)
        /// </summary>
        public int Quantidade { get; set; }

        /// <summary>
        /// Campo para pesquisar por campanha específica (-1 para ignorar)
        /// </summary>
        public int Campanha { get; set; }

        /// <summary>
        /// Campo para pesquisar por código específico (-1 para ignorar)
        /// </summary>
        public int Codigo { get; set; }
        
        /// <summary>
        /// Campo textual para pesquisa livre (NULL para ignorar)
        /// </summary>
        public string CampoLivre { get; set; }

        /// <summary>
        /// Nome do local de adesão (NULL para ignorar)
        /// </summary>
        public string Local { get; set; }

        /// <summary>
        /// Endereço do local de adesão (NULL para ignorar)
        /// </summary>
        public string Endereco { get; set; }

        /// <summary>
        ///  Tipo de ordenação da consulta
        /// </summary>
        public Utils.TipoOrdenacao Ordenacao { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public PesquisaLocalAdesao()
        {
            Quantidade = -1;
            Campanha = -1;
            Codigo = -1;
            CampoLivre = String.Empty;
            Local = String.Empty;
            Endereco = String.Empty;
            Ordenacao = Utils.TipoOrdenacao.porNome;
        }
    }
}