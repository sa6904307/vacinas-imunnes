﻿using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace ImunneVacinas
{
    /// <summary>
    /// Descrição resumida de TransacaoRealizada
    /// </summary>
    public class TransacaoRealizada
    {
        private static string _tabela = "TransacoesRealizadas";
        private static List<BD.Columns> columns;
        private static DateTime columnsUpdated;

        #region Propriedades do elemento

        public Compra compraTransacao { get; set; }
        public int numeroTransacao { get; set; }
        public string codigoEstabelecimento { get; set; }
        public int codigoFormaPagamento { get; set; }
        public decimal valor { get; set; }
        public decimal valorDesconto { get; set; }
        public int parcelas { get; set; }
        public int statusTransacao { get; set; }
        public string autorizacao { get; set; }
        public string codigoTransacaoOperadora { get; set; }
        public string dataAprovacaoOperadora { get; set; }
        public string numeroComprovanteVenda { get; set; }
        public string nsu { get; set; }
        public string mensagemVenda { get; set; }
        public string urlPagamento { get; set; }
        public List<string> cartoesUtilizados { get; set; }
        public string numeroCartao { get; set; }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        public TransacaoRealizada()
        {
            compraTransacao = new Compra();
        }

        /// <summary> 
        /// Rotina que cria um Array com os parâmetros utilizados nas ações de INSERT, UPDATE e DELETE
        /// <returns>Array com vários MySqlParameters</returns>
        /// </summary>
        private Array GetParametrosSQL()
        {
            MySqlParameter p;
            ArrayList ap = new ArrayList();

            p = new MySqlParameter("@compraTransacao", Parser.parseInteiro(compraTransacao.Codigo));
            p.MySqlDbType = MySqlDbType.Int32;
            ap.Add(p);

            p = new MySqlParameter("@numeroTransacao", Parser.parseInteiro(numeroTransacao));
            p.MySqlDbType = MySqlDbType.Int32;
            ap.Add(p);

            p = new MySqlParameter("@codigoEstabelecimento", Parser.parseString(codigoEstabelecimento));
            p.MySqlDbType = MySqlDbType.VarChar;
            ap.Add(p);

            // Como o retorno dos valores ignora ',' e '.', é necessário o tratamento dos mesmo para armazenamento na tabela
            if (valor > 0)
                valor = Convert.ToDecimal(valor / 100);

            p = new MySqlParameter("@valor", Parser.parseDecimal(valor));
            p.MySqlDbType = MySqlDbType.Decimal;
            ap.Add(p);

            if (valorDesconto > 0)
                valorDesconto = Convert.ToDecimal(valorDesconto / 100);
            p = new MySqlParameter("@valorDesconto", Parser.parseDecimal(valorDesconto));
            p.MySqlDbType = MySqlDbType.Decimal;
            ap.Add(p);

            p = new MySqlParameter("@parcelas", Parser.parseInteiro(parcelas));
            p.MySqlDbType = MySqlDbType.Int32;
            ap.Add(p);

            p = new MySqlParameter("@statusTransacao", Parser.parseInteiro(statusTransacao));
            p.MySqlDbType = MySqlDbType.Int32;
            ap.Add(p);

            p = new MySqlParameter("@autorizacao", Parser.parseString(autorizacao));
            p.MySqlDbType = MySqlDbType.VarChar;
            ap.Add(p);

            p = new MySqlParameter("@codigoTransacaoOperadora", Parser.parseString(codigoTransacaoOperadora));
            p.MySqlDbType = MySqlDbType.VarChar;
            ap.Add(p);

            // O retorno pode ser no formato MMDD somente e ai, é necessário o tratamento da data
            if (!String.IsNullOrEmpty(dataAprovacaoOperadora))
            {
                if (dataAprovacaoOperadora.Length == 4)
                    dataAprovacaoOperadora = String.Format("{0}-{1}-{2}", DateTime.Now.Year, dataAprovacaoOperadora.Substring(0, 2), dataAprovacaoOperadora.Substring(2, 2));
            }
            else dataAprovacaoOperadora = DateTime.Now.ToString("yyyy-MM-dd");

            p = new MySqlParameter("@dataAprovacaoOperadora", Parser.parseDateTimeBD(Convert.ToDateTime(dataAprovacaoOperadora)));
            p.MySqlDbType = MySqlDbType.DateTime;
            ap.Add(p);

            p = new MySqlParameter("@numeroComprovanteVenda", Parser.parseString(numeroComprovanteVenda));
            p.MySqlDbType = MySqlDbType.VarChar;
            ap.Add(p);

            p = new MySqlParameter("@nsu", Parser.parseString(nsu));
            p.MySqlDbType = MySqlDbType.VarChar;
            ap.Add(p);

            p = new MySqlParameter("@mensagemVenda", Parser.parseString(mensagemVenda));
            p.MySqlDbType = MySqlDbType.VarChar;
            ap.Add(p);

            p = new MySqlParameter("@urlPagamento", Parser.parseString(urlPagamento));
            p.MySqlDbType = MySqlDbType.VarChar;
            ap.Add(p);

            if(numeroCartao != null)
			{
                p = new MySqlParameter("@numeroCartao", Parser.parseString(numeroCartao).Substring(numeroCartao.ToString().Length - 4));
                p.MySqlDbType = MySqlDbType.VarChar;
                ap.Add(p);
            }
            else
			{
                p = new MySqlParameter("@numeroCartao", "");
                p.MySqlDbType = MySqlDbType.VarChar;
                ap.Add(p);
            }

            p = new MySqlParameter("@codigoFormaPagamento", Parser.parseInteiro(codigoFormaPagamento));
            p.MySqlDbType = MySqlDbType.Int32;
            ap.Add(p);

            return ap.ToArray();
        }

        /// <summary>
        /// Rotina que insere o registro no BD
        /// </summary>
        /// <returns>Código do registro inserido no BD</returns>
        public int Incluir()
        {
            if (numeroTransacao < 0 || compraTransacao.Codigo < 0)
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    sql.Parameters.AddRange(GetParametrosSQL());
                    sql.CommandText = "INSERT INTO TransacoesRealizadas "
                                    + "(compraTransacao, numeroTransacao, codigoEstabelecimento, valor, valorDesconto, "
                                    + "parcelas, statusTransacao, autorizacao, codigoTransacaoOperadora, dataAprovacaoOperadora, "
                                    + "numeroComprovanteVenda, nsu, mensagemVenda, urlPagamento, numeroCartao, codigoFormaPagamento) "
                                    + "VALUE(@compraTransacao, @numeroTransacao, @codigoEstabelecimento, @valor, @valorDesconto, "
                                    + "@parcelas, @statusTransacao, @autorizacao, @codigoTransacaoOperadora, @dataAprovacaoOperadora, "
                                    + "@numeroComprovanteVenda, @nsu, @mensagemVenda, @urlPagamento, @numeroCartao, @codigoFormaPagamento); ";
                    int resultado = Parser.parseInteiro(new Data().execute(sql));
                    return resultado;
                }
            }
            catch (ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Rotina que altera os dados do registro no BD
        /// </summary>
        /// <returns>Quantidade de linhas alteradas na ação junto ao BD</returns>
        public int Alterar()
        {
            if (numeroTransacao < 0 || compraTransacao.Codigo < 0)
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    sql.Parameters.AddRange(GetParametrosSQL());
                    sql.CommandText = "UPDATE TransacoesRealizadas "
                                    + "SET codigoEstabelecimento = @codigoEstabelecimento, "
                                    + "valor = @valor, valorDesconto = @valorDesconto, "
                                    + "parcelas = @parcelas, statusTransacao = @statusTransacao, codigoFormaPagamento = @codigoFormaPagamento, "
                                    + "autorizacao = @autorizacao, codigoTransacaoOperadora = @codigoTransacaoOperadora, "
                                    + "dataAprovacaoOperadora = @dataAprovacaoOperadora, numeroComprovanteVenda = @numeroComprovanteVenda, "
                                    + "nsu = @nsu, mensagemVenda = @mensagemVenda, urlPagamento = @urlPagamento, numeroCartao = @numeroCartao "
                                    + "WHERE compraTransacao = @compraTransacao AND numeroTransacao = @numeroTransacao";
                    int resultado = Parser.parseInteiro(new Data().execute(sql));
                    return resultado;
                }
            }
            catch (ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Rotina que exclui o registro no BD
        /// </summary>
        /// <returns>Código do registro inserido no BD</returns>
        public int Excluir()
        {
            if (String.IsNullOrEmpty(numeroComprovanteVenda))
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    sql.Parameters.AddRange(GetParametrosSQL());
                    sql.CommandText = "DELETE FROM TransacoesRealizadas WHERE numeroComprovanteVenda = @numeroComprovanteVenda ";
                    int resultado = Parser.parseInteiro(new Data().execute(sql));
                    return resultado;
                }
            }
            catch (ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="compra"></param>
        /// <returns></returns>
        public static TransacaoRealizada ConsultarUnicoCompra(int compra)
        {
            if (compra <= 0)
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            ImunneVacinas.PesquisaTransacaoRealizada pesquisa = new PesquisaTransacaoRealizada()
            {
                Compra = compra
            };

            List<TransacaoRealizada> retorno = ConsultaGenerica(pesquisa);
            if (retorno == null || retorno.Count.Equals(0))
                return null;
            else
                return retorno[0];
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="compra"></param>
        /// <returns></returns>
        public static TransacaoRealizada ConsultarUnico(int transacao)
        {
            if (transacao <= 0)
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            ImunneVacinas.PesquisaTransacaoRealizada pesquisa = new PesquisaTransacaoRealizada()
            {
                Codigo = transacao
            };

            List<TransacaoRealizada> retorno = ConsultaGenerica(pesquisa);
            if (retorno == null || retorno.Count.Equals(0))
                return null;
            else
                return retorno[0];
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="compra"></param>
        /// <param name="transacao"></param>
        /// <returns></returns>
        public static TransacaoRealizada ConsultarUnico(int compra, int transacao)
        {
            if (transacao <= 0 || compra <= 0)
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            ImunneVacinas.PesquisaTransacaoRealizada pesquisa = new PesquisaTransacaoRealizada()
            {
                Compra = compra,
                Codigo = transacao
            };

            List<TransacaoRealizada> retorno = ConsultaGenerica(pesquisa);
            if (retorno == null || retorno.Count.Equals(0))
                return null;
            else
                return retorno[0];
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool ExisteBD()
        {
            bool retorno = false;

            try
            {
                TransacaoRealizada controle = ConsultarUnico(compraTransacao.Codigo, numeroTransacao);
                if (controle != null)
                    retorno = true;
            }
            catch (Exception ex)
            {
                retorno = false;
            }

            return retorno;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pesquisa"></param>
        /// <returns></returns>
        public static List<TransacaoRealizada> ConsultarTransacoes(PesquisaTransacaoRealizada pesquisa)
        {
            return ConsultaGenerica(pesquisa);
        }

        /// <summary>
        /// Rotina interna utilizada para todas as pesquisas do elemento
        /// </summary>
        /// <param name="pesquisa">VALUE: Elemento com os parâmetros com os quais será realizada a pesquisa</param>
        /// <returns>Lista do elemento com os dados retornados da consulta</returns>
        internal static List<TransacaoRealizada> ConsultaGenerica(PesquisaTransacaoRealizada pesquisa)
        {
            StringBuilder ordenacao = new StringBuilder();
            StringBuilder filtro = new StringBuilder();
            StringBuilder top = new StringBuilder();
            MySqlParameter p;

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    if (pesquisa.Quantidade > 0)
                        top.AppendFormat(" LIMIT {0} ", pesquisa.Quantidade);

                    if (filtro.Length > 0)
                        filtro.Replace(" AND ", " WHERE ", 0, 5);

                    #region Campo Compra

                    if (pesquisa.Compra >= 0)
                    {
                        filtro.Append(" AND TransacoesRealizadas.compraTransacao = @compraTransacao");
                        p = new MySqlParameter("@compraTransacao", pesquisa.Compra);
                        p.MySqlDbType = MySqlDbType.Int32;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Transacao

                    if (pesquisa.Codigo >= 0)
                    {
                        filtro.Append(" AND TransacoesRealizadas.numeroTransacao = @numeroTransacao");
                        p = new MySqlParameter("@numeroTransacao", pesquisa.Codigo);
                        p.MySqlDbType = MySqlDbType.Int32;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    if (filtro.Length > 0)
                        filtro = filtro.Replace(" AND ", " WHERE ", 0, 5);

                    #region Ordenação

                    switch (pesquisa.Ordenacao)
                    {
                        case Utils.TipoOrdenacao.porCodigo:
                            {
                                ordenacao.Append("ORDER BY TransacoesRealizadas.numeroTransacao ASC");
                            }
                            break;
                        case Utils.TipoOrdenacao.porCodigoDesc:
                            {
                                ordenacao.Append("ORDER BY TransacoesRealizadas.numeroTransacao DESC");
                            }
                            break;
                        default:
                            {
                                ordenacao.Append("ORDER BY TransacoesRealizadas.dataAprovacaoOperadora DESC");
                            }
                            break;
                    }

                    #endregion

                    sql.CommandText = String.Format("SELECT "
                                    + "TransacoesRealizadas.* "
                                    + "FROM TransacoesRealizadas "
                                    + "{0} {1} {2}", filtro, ordenacao, top);
                    DataTable dt = new Data().getData(sql);
                    return MontarRetorno(dt);
                }
            }
            catch (ImunneVacinas.ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Rotina que retorna uma lista genérica do elemento, de acordo com o retorno da Consultar ao BD
        /// </summary>
        /// <param name="dt">VALUE: DataTable com o resultado da Consultar ao BD</param>
        /// <returns>Lista do elemento com os dados retornados da consulta</returns>
        internal static List<TransacaoRealizada> MontarRetorno(DataTable dt)
        {
            int i = -1;
            List<TransacaoRealizada> list = new List<TransacaoRealizada>();

            if (dt == null && dt.Rows.Count == 0)
                return list;
            else
            {
                try
                {
                    TransacaoRealizada retorno;
                    for (i = 0; i < dt.Rows.Count; i++)
                    {
                        retorno = new TransacaoRealizada()
                        {
                            autorizacao = Parser.parseString(dt.Rows[i]["autorizacao"]),
                            codigoEstabelecimento = Parser.parseString(dt.Rows[i]["codigoEstabelecimento"]),
                            codigoFormaPagamento = Parser.parseInteiro(dt.Rows[i]["codigoFormaPagamento"]),
                            codigoTransacaoOperadora = Parser.parseString(dt.Rows[i]["codigoTransacaoOperadora"]),
                            dataAprovacaoOperadora = Convert.ToDateTime(dt.Rows[i]["dataAprovacaoOperadora"]).ToString("dd/MM/yyyy"), //Convert.ToDateTime(dt.Rows[i]["dataAprovacaoOperadora"]),
                            mensagemVenda = Parser.parseString(dt.Rows[i]["mensagemVenda"]),
                            nsu = Parser.parseString(dt.Rows[i]["nsu"]),
                            numeroCartao = Parser.parseString(dt.Rows[i]["numeroCartao"]),
                            numeroComprovanteVenda = Parser.parseString(dt.Rows[i]["numeroComprovanteVenda"]),
                            numeroTransacao = Parser.parseInteiro(dt.Rows[i]["numeroTransacao"]),
                            parcelas = Parser.parseInteiro(dt.Rows[i]["parcelas"]),
                            statusTransacao = Parser.parseInteiro(dt.Rows[i]["statusTransacao"]),
                            urlPagamento = Parser.parseString(dt.Rows[i]["urlPagamento"]),
                            valor = Parser.parseDecimal(dt.Rows[i]["valor"]),
                            valorDesconto = Parser.parseDecimal(dt.Rows[i]["valorDesconto"])
                        };

                        retorno.compraTransacao.Codigo = Parser.parseInteiro(dt.Rows[i]["compraTransacao"]);

                        list.Add(retorno);
                    }

                    return list;
                }
                catch (Exception ex)
                {
                    ex.Data.Add("último retorno lido", i);
                    throw;
                }
            }
        }
    }
}