﻿using MySql.Data.MySqlClient;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace ImunneVacinas
{
    /// <summary>
    /// Descrição resumida de LinkCampanha
    /// </summary>
    [Serializable]
    public class LinkCampanha
    {
        private const string _tabela = "LinksCampanhas";
        private static List<BD.Columns> columns;
        private static DateTime columnsUpdated;

        #region Propriedades de todos os campos da tabela

        private int _codigo;
        private Campanha _campanha;
        private Clinica _clinica;
        private string _link;
        private DateTime _dataLimite;
        private DateTime _dataCriacao;

        public int Codigo
        {
            get
            {
                return _codigo;
            }

            set
            {
                _codigo = value;
            }
        }

        public Campanha Campanha
        {
            get
            {
                return _campanha;
            }

            set
            {
                _campanha = value;
            }
        }

        public Clinica Clinica
        {
            get
            {
                return _clinica;
            }

            set
            {
                _clinica = value;
            }
        }

        public string Link
        {
            get
            {
                return _link;
            }

            set
            {
                _link = value;
            }
        }

        public DateTime DataLimite
        {
            get
            {
                return _dataLimite;
            }

            set
            {
                _dataLimite = value;
            }
        }

        public DateTime DataCriacao
        {
            get
            {
                return _dataCriacao;
            }

            set
            {
                _dataCriacao = value;
            }
        }

        #endregion

        #region GetInfo para cada um campo da tabela

        /// <summary>
        /// Busca informações, no banco de dados, sobre os campos da tabela.
        /// </summary>
        private static void GetInfoTabela()
        {
            if (columns == null || (DateTime.Now - columnsUpdated).Minutes > 0) //Atualiza com mais de 24h de diferença
            {
                columns = ImunneVacinas.BD.Columns.getColumnsInfo(_tabela);
                columnsUpdated = DateTime.Now;
            }
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre o campo codigo
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoCodigo()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "codigo");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre o campo campanha
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoCampanha()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "campanha");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre o campo clinica
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoClinica()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "clinica");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre o campo link
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoLinkCampanha()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "link");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre o campo datalimite
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoDataLimite()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "datalimite");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre o campo datacriacao
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoDataCriacao()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "datacriacao");
        }

        #endregion

        /// <summary> 
        /// Monta o array com os parâmetros usados pelo objeto MySqlCommand em inserts, deletes e Updates
        /// <returns>Um array com vários SqlParameter</returns>
        /// </summary>
        private Array GetParametrosSQL()
        {
            MySqlParameter p;
            ArrayList ap = new ArrayList();

            p = new MySqlParameter("@codigo", Parser.parseInteiro(_codigo))
            {
                MySqlDbType = GetInfoCodigo().Type
            };
            ap.Add(p);

            p = new MySqlParameter("@campanha", Parser.parseInteiro(_campanha.Codigo))
            {
                MySqlDbType = GetInfoCampanha().Type
            };
            ap.Add(p);

            p = new MySqlParameter("@clinica", Parser.parseInteiro(_clinica.Codigo))
            {
                MySqlDbType = GetInfoCodigo().Type
            };
            ap.Add(p);

            p = new MySqlParameter("@link", Parser.parseString(_link))
            {
                MySqlDbType = GetInfoLinkCampanha().Type
            };
            ap.Add(p);

            p = new MySqlParameter("@dataLimite", Parser.parseDateTime(_dataLimite));
            p.MySqlDbType = GetInfoDataLimite().Type;
            ap.Add(p);

            p = new MySqlParameter("@dataCriacao", Parser.parseDateTime(_dataCriacao));
            p.MySqlDbType = GetInfoDataCriacao().Type;
            ap.Add(p);

            return ap.ToArray();
        }

        /// <summary>
        /// Inicializa as propriedades com os valores padrão
        /// </summary>
        private void PopularCampos()
        {
            GetInfoTabela();

            _campanha = new Campanha();
            _clinica = new Clinica();

            _campanha.Codigo = -1;
            _clinica.Codigo = -1;
            _link = String.Empty;
            _dataCriacao = Utils.dataPadrao;
            _dataLimite = Utils.dataPadrao;
        }

        /// <summary>
        /// Iniciliza elemento
        /// </summary>
        public LinkCampanha()
        {
            PopularCampos();
        }

        /// <summary>
        /// Rotina que insere o registro no BD
        /// </summary>
        /// <returns>Código do registro inserido no BD</returns>
        public int Incluir()
        {
            if (_campanha.Codigo <= 0 || _clinica.Codigo <= 0 || _dataLimite == Utils.dataPadrao)
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    sql.Parameters.AddRange(GetParametrosSQL());
                    sql.CommandText = "INSERT INTO LinksCampanhas "
                                    + "(campanha, clinica, codigo, link, datalimite, datacriacao) "
                                    + "VALUE(@campanha, @clinica, @codigo, @link, @datalimite, @datacriacao) "
                                    + "SELECT LAST_INSERT_ID();";
                    int resultado = Parser.parseInteiro(new Data().executeEscalar(sql));
                    return resultado;
                }
            }
            catch (ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Rotina que exclui o registro do BD
        /// </summary>
        /// <returns>Quantidade de linhas alteradas na ação junto ao BD</returns>
        public int Excluir()
        {
            if (_codigo < 0)
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    sql.Parameters.AddRange(GetParametrosSQL());
                    sql.CommandText = "DELETE FROM LinksCampanhas "
                                    + "WHERE codigo = @codigo";
                    int resultado = Parser.parseInteiro(new Data().execute(sql));
                    return resultado;
                }
            }
            catch (ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Localiza um registro único, por base do seu código
        /// </summary>
        /// <param name="codigo">VALUE: código de identificação do registro (obrigatório)</param>
        /// <returns></returns>
        public static LinkCampanha ConsultarUnico(int codigo)
        {
            if (codigo < 0)
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            PesquisaLinkCampanha pesquisa = new PesquisaLinkCampanha();
            pesquisa.Codigo = codigo;

            List<LinkCampanha> retornos = ConsultaGenerica(pesquisa);
            if (retornos != null && retornos.Count > 0)
                return retornos[0];
            else return null;
        }

        /// <summary>
        /// Localiza 1 ou N registro de acordo com os parâmetros de pesquisa
        /// </summary>
        /// <param name="pesquisa">VALUE: Parâmetros de pesquisa</param>
        /// <returns></returns>
        public static List<LinkCampanha> ConsultarLinks(PesquisaLinkCampanha pesquisa)
        {
            return ConsultaGenerica(pesquisa);
        }

        /// <summary>
        /// Pesquisa que realiza todas as Consultas no banco de dados
        /// </summary>
        /// <param name="pesquisa"></param>
        /// <returns></returns>
        internal static List<LinkCampanha> ConsultaGenerica(PesquisaLinkCampanha pesquisa)
        {
            StringBuilder ordenacao = new StringBuilder();
            StringBuilder filtro = new StringBuilder();
            StringBuilder top = new StringBuilder();
            MySqlParameter p;

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    if (pesquisa.Quantidade > 0)
                        top.AppendFormat(" LIMIT {0} ", pesquisa.Quantidade);

                    #region Campo Código

                    if (pesquisa.Codigo >= 0)
                    {
                        filtro.Append(" AND LinksCampanhas.codigo = @codigo");
                        p = new MySqlParameter("@codigo", pesquisa.Codigo);
                        p.MySqlDbType = GetInfoCodigo().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Campanha

                    if (pesquisa.Campanha >= 0)
                    {
                        filtro.Append(" AND LinksCampanhas.campanha = @campanha");
                        p = new MySqlParameter("@campanha", pesquisa.Campanha);
                        p.MySqlDbType = GetInfoCampanha().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Clínica

                    if (pesquisa.Clinica >= 0)
                    {
                        filtro.Append(" AND LinksCampanhas.clinica = @clinica");
                        p = new MySqlParameter("@clinica", pesquisa.Clinica);
                        p.MySqlDbType = GetInfoClinica().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    if (filtro.Length > 0)
                        filtro = filtro.Replace(" AND ", " WHERE ", 0, 5);

                    #region Ordenação

                    ordenacao.Append("ORDER BY LinksCampanhas.datalimite DESC");

                    #endregion

                    sql.CommandText = String.Format("SELECT "
                                    + "Campanhas.codigo AS codigoCampanha, Campanhas.nome AS nomeCampanha, Campanhas.identificacao AS idCampanha, "
                                    + "Clinicas.codigo AS codigoClinica, Clinicas.nomefantasia AS nomeClinica, "
                                    + "LinksCampanhas.* "
                                    + "FROM LinksCampanhas "
                                    + "INNER JOIN Campanhas ON Campanhas.codigo = LinksCampanhas.campanha "
                                    + "INNER JOIN Clinicas ON Clinicas.codigo = LinksCampanhas.clinica "
                                    + "{0} {1} {2}", filtro, ordenacao, top);
                    DataTable dt = new Data().getData(sql);
                    return MontarRetorno(dt);
                }
            }
            catch (ImunneVacinas.ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        internal static List<LinkCampanha> MontarRetorno(DataTable dt)
        {
            int i = -1;
            List<LinkCampanha> list = new List<LinkCampanha>();
            if (dt == null && dt.Rows.Count == 0)
                return list;
            else
            {
                LinkCampanha retorno;
                try
                {
                    for (i = 0; i < dt.Rows.Count; i++)
                    {
                        retorno = new LinkCampanha()
                        {
                            Codigo = Parser.parseInteiro(dt.Rows[i]["codigo"]),
                            Link = Parser.parseString(dt.Rows[i]["link"]),
                            DataLimite = Convert.ToDateTime(dt.Rows[i]["datalimite"]),
                            DataCriacao = Convert.ToDateTime(dt.Rows[i]["datacriacao"])
                        };

                        retorno.Campanha.Codigo = Parser.parseInteiro(dt.Rows[i]["codigoCampanha"]);
                        retorno.Campanha.Nome = Parser.parseString(dt.Rows[i]["nomeCampanha"]);
                        retorno.Campanha.Identificacao = Parser.parseString(dt.Rows[i]["idCampanha"]);

                        retorno.Clinica.Codigo = Parser.parseInteiro(dt.Rows[i]["codigoClinica"]);
                        retorno.Clinica.NomeFantasia = Parser.parseString(dt.Rows[i]["nomeClinica"]);

                        list.Add(retorno);
                    }
                    return list;
                }
                catch (Exception ex)
                {
                    ex.Data.Add("último retorno lido", i);
                    throw;
                }
            }
        }
    }
}