﻿using System;
using MySql.Data.MySqlClient;
using System.Data;
using System.Runtime.Serialization;

namespace ImunneVacinas
{
    /// <summary>
    /// 
    /// </summary>
    public class Data
    {
        private MySqlConnection MySQLConnection = null;
        private const string strName = "conexao";

        // Banco local - LOCALHOST
        private const string strConnectionLocal = "Server=127.0.0.1;Database=myDatabase;Uid=myUser;Pwd=myPassword;SslMode=none;";

        // Banco de Homologação BANCO ALTERNATIVO - TESTES COMBR
        private const string strConnectionTestes = "Server=mysql.vacinas.imunne.com.br;Database=vacinas01;Uid=vacinas01;Pwd=o6M4S3f7v9;SslMode=none;old guids=true;Connect Timeout=30";

        // Banco de Produção BANCO OFICIAL - COMBR
        private const string strConnectionProducao = "Server=mysql.vacinas.imunne.com.br;Database=vacinas;Uid=vacinas;Pwd=L6q8z6q9Z9;SslMode=none;old guids=true;";

        /// <summary>
        /// Rotina que realiza a tentativa de abertura de conexão
        /// </summary>
        /// <returns>Retorna TRUE para conexão realizada com sucesso</returns>
        private bool openConnection()
        {
            try
            {
                if (this.MySQLConnection == null)
                    this.MySQLConnection = new MySqlConnection(strConnectionProducao);
                if (this.MySQLConnection.State == ConnectionState.Closed)
                    this.MySQLConnection.Open();
                return true;
            }
            catch (Exception ex)
            {
                Erro erro = new Erro();
                erro.addArquivo(ex);
                return false;
            }
        }

        /// <summary>
        /// Rotina que encerra a conexão com a base de dados
        /// </summary>
        /// <returns>Retorna TRUE para desconexão realizada com sucesso</returns>
        private bool closeConnection()
        {
            try
            {
                if (this.MySQLConnection != null)
                    if (this.MySQLConnection.State != ConnectionState.Closed)
                        this.MySQLConnection.Close();
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// Executa um Select no banco de dados.
        /// <returns>Returns: Um DataTable resultante, 'null' em caso de erro.</returns>
        /// </summary>
        /// <param name="sql">Comando sql para Consultarr dados</param>
        /// <param name="tabela">Nome da tabela a ser retornada.</param>
        public DataTable getData(MySqlCommand sql)
        {
            if (!this.openConnection()) //abre a conexão com o arquivo 'padrao' do Banco de Dados
                return null;
            DataTable dt = new DataTable();
            MySqlDataAdapter da = null;
            sql.Connection = MySQLConnection;

            try
            {
                da = new MySqlDataAdapter(sql);
                da.Fill(dt); //preenche a tabela                
                da.Dispose();
                return dt;
            }
            catch (Exception ex)
            {
                string s = getQueryParsed(sql);
                Erro erro = new Erro(Erro.produtos.ImunneVacinas);
                erro.AdicionarLog(ex, new object[] { s });
                return null;
            }
            finally
            {
                da.Dispose();
                closeConnection();
            }
        }

        /// <summary>
        /// Rotina que executa comandos de INSERT/UPDATE/DELETE no banco de dados.
        /// <returns>RETURNS: Total de linhas alteradas</returns>
        /// </summary>
        /// <param name="sql">VALUE: Comando SQL a ser executado no banco de dados</param>
        public int execute(string sql)
        {
            int resultado = 0;
            MySqlCommand objCommand = null;

            try
            {
                if (!this.openConnection())
                    return resultado;

                objCommand = new MySqlCommand();
                objCommand.Connection = this.MySQLConnection;
                objCommand.CommandText = sql;
                resultado = objCommand.ExecuteNonQuery();
                objCommand.Dispose();
                return resultado;
            }
            catch (Exception ex)
            {
                this.closeConnection();
                throw;
            }
            finally
            {
                objCommand.Dispose();
                this.closeConnection();
            }
        }

        /// <summary>
        /// Rotina que executa comandos de INSERT/UPDATE/DELETE no banco de dados.
        /// <returns>RETURNS: Total de linhas alteradas</returns>
        /// </summary>
        /// <param name="sql">VALUE: Comando SQL a ser executado no banco de dados</param>
        public int execute(MySqlCommand sql)
        {
            try
            {
                if (!this.openConnection())
                    return 0;

                sql.Connection = this.MySQLConnection;
                int retorno = sql.ExecuteNonQuery();
                sql.Dispose();
                return retorno;
            }
            catch (Exception ex)
            {
                string s = getQueryParsed(sql);
                this.closeConnection();
                throw;
            }
            finally
            {
                sql.Dispose();
                this.closeConnection();
            }
        }

        /// <summary>
        /// Rotina que executa comando diretamento no banco de dados.
        /// <returns>RETURNS: Total de linhas alteradas ou Primeira coluna da primeira linha, de acordo com a query executada.</returns>
        /// </summary>
        /// <param name="sql">VALUE: Comando SQL a ser executado no banco de dados</param>
        public int executeEscalar(string sql)
        {
            int resultado = 0;
            MySqlCommand objCommand = null;

            try
            {
                if (!this.openConnection())
                    return resultado;
                objCommand = new MySqlCommand();
                objCommand.Connection = this.MySQLConnection;
                objCommand.CommandText = sql;
                resultado = Convert.ToInt32(objCommand.ExecuteScalar());
            }
            catch (Exception ex)
            {
                this.closeConnection();
                throw;
            }
            finally
            {
                objCommand.Dispose();
                this.closeConnection();
            }

            return resultado;
        }

        /// <summary>
        /// Rotina que executa comando diretamento no banco de dados.
        /// <returns>RETURNS: Total de linhas alteradas ou Primeira coluna da primeira linha, de acordo com a query executada.</returns>
        /// </summary>
        /// <param name="sql">VALUE: Comando SQL a ser executado no banco de dados</param>
        public int executeEscalar(MySqlCommand sql)
        {
            try
            {
                if (!this.openConnection())
                    return 0;

                sql.Connection = this.MySQLConnection;
                int retorno = Convert.ToInt32(sql.ExecuteScalar());
                sql.Dispose();
                return retorno;
            }
            catch (Exception ex)
            {
                string s = getQueryParsed(sql);
                ImunneVacinas.Erro erro = new Erro();
                erro.AdicionarLog(ex);
                this.closeConnection();
                throw;
            }
            finally
            {
                sql.Dispose();
                this.closeConnection();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        private static string queryParsed(string sql)
        {
            string command = "";
            try
            {
                if (sql == null)
                    return command;
                command = sql;
                foreach (char c in sql)
                {
                    command = command.Replace(c.ToString(), "'" + c + "'");
                }
            }
            catch
            {
                return "";
            }
            return command;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        private string getQueryParsed(MySqlCommand sql)
        {
            string s = "";
            try
            {

                if (sql == null)
                    return s;
                s = sql.CommandText;
                foreach (MySqlParameter p in sql.Parameters)
                {
                    s = s.Replace(p.ParameterName, "'" + p.Value.ToString() + "'");
                }

            }
            catch
            {
            }
            return s;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class ImunneVacinasDataException : Exception
    {
        /// <summary>
        /// Exceção gerada quando uma operação de conexão/acesso ao banco de dados falha
        /// </summary>
        public ImunneVacinasDataException()
        {

        }
        /// <summary>
        /// Exceção gerada quando uma operação de conexão/acesso ao banco de dados falha
        /// </summary>
        /// <param name="message">Texto da mensagem</param>
        public ImunneVacinasDataException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Exceção gerada quando uma operação de conexão/acesso ao banco de dados falha
        /// </summary>
        /// <param name="message">Texto da mensagem</param>
        /// <param name="inner">Inner exception</param>
        public ImunneVacinasDataException(string message, Exception inner)
            : base(message, inner)
        {
        }

        /// <summary>
        /// Exceção gerada quando uma operação de conexão/acesso ao banco de dados falha
        /// </summary>
        protected ImunneVacinasDataException(SerializationInfo info, StreamingContext contexto)
            : base(info, contexto)
        {
        }
    }
}