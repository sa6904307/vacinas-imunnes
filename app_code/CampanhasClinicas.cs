﻿using MySql.Data.MySqlClient;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace ImunneVacinas
{
    /// <summary>
    /// Descrição resumida de CampanhaClinica
    /// </summary>
    [Serializable]
    public class CampanhaClinica
    {
        private static string _tabela = "CampanhasClinicas";
        private static List<BD.Columns> columns;
        private static DateTime columnsUpdated;

        #region Propriedades do elemento

        private Campanha _idCampanha;
        private Clinica _idClinica;
        private int _codigo;
        private DateTime _dataCriacao;
        private DateTime _dataAlteracao;

        /// <summary>
        /// Identificação da Campanha
        /// </summary>
        public Campanha IdCampanha
        {
            get
            {
                return _idCampanha;
            }

            set
            {
                _idCampanha = value;
            }
        }

        /// <summary>
        /// Identificação da CampanhaClinica
        /// </summary>
        public Clinica IdClinica
        {
            get
            {
                return _idClinica;
            }

            set
            {
                _idClinica = value;
            }
        }

        /// <summary>
        /// Código de identificação do registro
        /// </summary>
        public int Codigo
        {
            get
            {
                return _codigo;
            }

            set
            {
                _codigo = value;
            }
        }

        /// <summary>
        /// Data de criação do registro
        /// </summary>
        public DateTime DataCriacao
        {
            get
            {
                return _dataCriacao;
            }

            set
            {
                _dataCriacao = value;
            }
        }

        /// <summary>
        /// Data da última alteração do registro
        /// </summary>
        public DateTime DataAlteracao
        {
            get
            {
                return _dataAlteracao;
            }

            set
            {
                _dataAlteracao = value;
            }
        }

        #endregion

        #region GetInfos

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos os campos da tabela.
        /// </summary>
        private static void GetInfoTabela()
        {
            if (columns == null || (DateTime.Now - columnsUpdated).Minutes > 0)
            {
                columns = BD.Columns.getColumnsInfo(_tabela);
                columnsUpdated = DateTime.Now;
            }
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo campanha.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoCampanha()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "campanha");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo clinica.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoClinica()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "clinica");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo codigo.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoCodigo()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "codigo");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo preco.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoPreco()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "preco");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo datacriacao.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoDataCriacao()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "datacriacao");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo dataalteracao.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoDataAlteracao()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "dataalteracao");
        }

        #endregion

        /// <summary> 
        /// Rotina que cria um Array com os parâmetros utilizados nas ações de INSERT, UPDATE e DELETE
        /// <returns>Array com vários MySqlParameters</returns>
        /// </summary>
        private Array GetParametrosSQL()
        {
            MySqlParameter p;
            ArrayList ap = new ArrayList();

            p = new MySqlParameter("@campanha", Parser.parseInteiro(_idCampanha.Codigo));
            p.MySqlDbType = GetInfoCampanha().Type;
            ap.Add(p);

            p = new MySqlParameter("@clinica", Parser.parseInteiro(_idClinica.Codigo));
            p.MySqlDbType = GetInfoClinica().Type;
            ap.Add(p);

            p = new MySqlParameter("@codigo", Parser.parseInteiro(_codigo));
            p.MySqlDbType = GetInfoCodigo().Type;
            ap.Add(p);

            p = new MySqlParameter("@datacriacao", Parser.parseDateTimeBD(_dataCriacao));
            p.MySqlDbType = GetInfoDataCriacao().Type;
            ap.Add(p);

            p = new MySqlParameter("@dataalteracao", Parser.parseDateTimeBD(_dataAlteracao));
            p.MySqlDbType = GetInfoDataAlteracao().Type;
            ap.Add(p);

            return ap.ToArray();
        }

        /// <summary>
        /// Inicialização das propriedades do elemento com seus valores padrão
        /// </summary>
        private void IniciarElemento()
        {
            GetInfoTabela();

            _idCampanha = new Campanha();
            _idClinica = new Clinica();

            _idCampanha.Codigo = -1;
            _idClinica.Codigo = -1;
            _dataAlteracao = Utils.dataPadrao;
            _dataCriacao = Utils.dataPadrao;
        }

        /// <summary>
        /// Inicialização do elemento
        /// </summary>
        public CampanhaClinica()
        {
            IniciarElemento();
        }

        /// <summary>
        /// Rotina que insere o registro no BD
        /// </summary>
        /// <returns>Código do registro inserido no BD</returns>
        public int Incluir()
        {
            if (_idCampanha.Codigo < 0 || _idClinica.Codigo < 0)
                throw new Exception("ImunneCampanhasClinicas - parâmetros com valores inválidos");

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    sql.Parameters.AddRange(GetParametrosSQL());
                    sql.CommandText = "INSERT INTO CampanhasClinicas "
                                    + "(campanha, clinica, datacriacao, dataalteracao)  "
                                    + "VALUE (@campanha, @clinica, @datacriacao, @dataalteracao); "
                                    + "SELECT LAST_INSERT_ID();";
                    int resultado = Parser.parseInteiro(new Data().executeEscalar(sql));
                    return resultado;
                }
            }
            catch (ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Rotina que altera os dados do registro no BD
        /// </summary>
        /// <returns>Quantidade de linhas alteradas na ação junto ao BD</returns>
        public int Alterar()
        {
            if (_codigo < 0)
                throw new Exception("ImunneCampanhasClinicas - parâmetros com valores inválidos");

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    sql.Parameters.AddRange(GetParametrosSQL());
                    sql.CommandText = "UPDATE CampanhasClinicas "
                                    + "SET "
                                    + "dataalteracao = @dataalteracao, preco = @preco "
                                    + "WHERE codigo = @codigo";
                    int resultado = Parser.parseInteiro(new Data().execute(sql));
                    return resultado;
                }
            }
            catch (ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Rotina que exclui o registro do BD
        /// </summary>
        /// <returns>Quantidade de linhas alteradas na ação junto ao BD</returns>
        public int Excluir()
        {
            if (_codigo < 0)
                throw new Exception("ImunneCampanhasClinicas - parâmetros com valores inválidos");

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    sql.Parameters.AddRange(GetParametrosSQL());
                    sql.CommandText = "DELETE FROM CampanhasClinicas "
                                    + "WHERE codigo = @codigo";
                    int resultado = Parser.parseInteiro(new Data().execute(sql));
                    return resultado;
                }
            }
            catch (ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Rotina que localiza e retorna um único registro, mediante o código informado
        /// </summary>
        /// <param name="codigo">VALUE: código de identificação do registro (obrigatório)</param>
        /// <returns>Elemento Usuario com os dados retornados da consulta</returns>
        public static CampanhaClinica ConsultarUnico(int codigo)
        {
            if (codigo <= 0)
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            ImunneVacinas.PesquisaCampanhaClinica pesquisa = new PesquisaCampanhaClinica()
            {
                Codigo = codigo
            };

            List<CampanhaClinica> retorno = ConsultaGenerica(pesquisa);
            if (retorno == null || retorno.Count.Equals(0))
                return null;
            else
                return retorno[0];
        }

        /// <summary>
        /// Rotina que localiza e retorna um único registro, mediante o código informado
        /// </summary>
        /// <param name="codigo">VALUE: código de identificação do registro (obrigatório)</param>
        /// <returns>Elemento Usuario com os dados retornados da consulta</returns>
        public static CampanhaClinica ConsultarUnico(int campanha, int clinica)
        {
            if (campanha <= 0 && clinica <= 0)
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            ImunneVacinas.PesquisaCampanhaClinica pesquisa = new PesquisaCampanhaClinica()
            {
                Campanha = campanha,
                Clinica = clinica
            };

            List<CampanhaClinica> retorno = ConsultaGenerica(pesquisa);
            if (retorno == null || retorno.Count.Equals(0))
                return null;
            else
                return retorno[0];
        }

        /// <summary>
        /// Rotina que realiza a pesquisa de elementos mediante os parametros de pesquisa informados
        /// </summary>
        /// <param name="pesquisa">VALUE: elemento de pesquisa com os parametros para consulta</param>
        /// <returns></returns>
        public static List<CampanhaClinica> ConsultarCampanhasClinicas(PesquisaCampanhaClinica pesquisa)
        {
            return ConsultaGenerica(pesquisa);
        }

        /// <summary>
        /// Rotina que retorna um elemento RetornoCBX para utilização em DropLists
        /// </summary>
        /// <param name="clinica">VALUE: Identificação da clínica (obrigatório)</param>
        /// <param name="InserirLinhaPadrao">VALUE: Informa se será adiconada a linha padrão</param>
        /// <param name="textoPadrao">VALUE: Informa o texto utilizado para linha padão (se utilizado NULL, será apresentado SELECIONE)</param>
        /// <returns>Lista do elemento RetornoCBX com os dados desejados</returns>
        public static List<RetornoComboBox> ConsultarComboBox(int clinica, bool InserirLinhaPadrao, string textoPadrao)
        {
            if (clinica <= 0)
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            DataTable dt = new DataTable();
            try
            {
                dt.Columns.Add("DisplayMember");
                dt.Columns.Add("ValueMember");

                ImunneVacinas.PesquisaCampanhaClinica pesquisa = new PesquisaCampanhaClinica()
                {
                    Clinica = clinica,
                    Ordenacao = Utils.TipoOrdenacao.porNome
                };

                List<CampanhaClinica> retorno = ConsultaGenerica(pesquisa);
                if (retorno != null)
                {
                    foreach (CampanhaClinica c in retorno)
                    {
                        dt.Rows.Add(new object[] { c.IdCampanha.Identificacao.ToUpper(), c.IdCampanha.Codigo });
                    }
                }
                return Utils.MontarRetornoComboBox(dt, InserirLinhaPadrao, textoPadrao);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// Rotina que retorna um elemento RetornoCBX para utilização em DropLists
        /// </summary>
        /// <param name="clinica">VALUE: Identificação da clínica (obrigatório)</param>
        /// <param name="InserirLinhaPadrao">VALUE: Informa se será adiconada a linha padrão</param>
        /// <param name="textoPadrao">VALUE: Informa o texto utilizado para linha padão (se utilizado NULL, será apresentado SELECIONE)</param>
        /// <returns>Lista do elemento RetornoCBX com os dados desejados</returns>
        public static List<RetornoComboBox> ConsultarClinicasComboBox(PesquisaCampanhaClinica pesquisa, bool InserirLinhaPadrao, string textoPadrao)
        {
            DataTable dt = new DataTable();
            try
            {
                dt.Columns.Add("DisplayMember");
                dt.Columns.Add("ValueMember");

                List<CampanhaClinica> retorno = ConsultaGenerica(pesquisa);
                if (retorno != null)
                {
                    foreach (CampanhaClinica c in retorno)
                    {
                        StringBuilder displayCombo = new StringBuilder();
                        displayCombo.AppendFormat("{0} - ", c.IdClinica.NomeFantasia.ToUpper());
                        displayCombo.AppendFormat("({0}/{1})", c.IdClinica.IdCidade.Nome.ToUpper(), c.IdClinica.IdUf.Sigla.ToUpper());
                        dt.Rows.Add(new object[] { displayCombo, c.IdClinica.Codigo });
                    }
                }
                return Utils.MontarRetornoComboBox(dt, InserirLinhaPadrao, textoPadrao);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// Rotina que retorna um elemento RetornoCBX para utilização em DropLists
        /// </summary>
        /// <param name="clinica">VALUE: Identificação da clínica (obrigatório)</param>
        /// <param name="InserirLinhaPadrao">VALUE: Informa se será adiconada a linha padrão</param>
        /// <param name="textoPadrao">VALUE: Informa o texto utilizado para linha padão (se utilizado NULL, será apresentado SELECIONE)</param>
        /// <returns>Lista do elemento RetornoCBX com os dados desejados</returns>
        public static List<RetornoComboBox> ConsultarCampanhasComboBox(PesquisaCampanhaClinica pesquisa, bool InserirLinhaPadrao, string textoPadrao)
        {
            DataTable dt = new DataTable();
            try
            {
                dt.Columns.Add("DisplayMember");
                dt.Columns.Add("ValueMember");

                List<CampanhaClinica> retorno = ConsultaGenerica(pesquisa);
                if (retorno != null)
                {
                    foreach (CampanhaClinica c in retorno)
                    {
                        ImunneVacinas.Campanha campanhaSelecionada = ImunneVacinas.Campanha.ConsultarUnico(c.IdCampanha.Codigo);

                        StringBuilder displayCampanha = new StringBuilder();
                        displayCampanha.AppendFormat("{0} | ", campanhaSelecionada.Identificacao.ToUpper());

                        if (campanhaSelecionada.DataLimite < DateTime.Now.Date)
                            displayCampanha.AppendFormat(" (CONCLUÍDA)");
                        else if (campanhaSelecionada.DataLimite > DateTime.Now.Date)
                        {
                            if (campanhaSelecionada.DataInicio <= DateTime.Now.Date)
                                displayCampanha.AppendFormat(" (ATIVA)");
                            else
                                displayCampanha.AppendFormat(" (À REALIZAR)");
                        }
                        else displayCampanha.AppendFormat(" (ATIVA)");

                        displayCampanha.AppendFormat(" - DE {0} ATÉ {1}", campanhaSelecionada.DataInicio.ToString("dd/MM/yyyy")
                            , campanhaSelecionada.DataLimite.ToString("dd/MM/yyyy"));

                        dt.Rows.Add(new object[] { displayCampanha, c.IdCampanha.Codigo });
                    }
                }
                return Utils.MontarRetornoComboBox(dt, InserirLinhaPadrao, textoPadrao);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// Rotina interna utilizada para todas as pesquisas do elemento
        /// </summary>
        /// <param name="pesquisa">VALUE: Elemento com os parâmetros com os quais será realizada a pesquisa</param>
        /// <returns>Lista do elemento com os dados retornados da consulta</returns>
        internal static List<CampanhaClinica> ConsultaGenerica(PesquisaCampanhaClinica pesquisa)
        {
            StringBuilder ordenacao = new StringBuilder();
            StringBuilder filtro = new StringBuilder();
            StringBuilder top = new StringBuilder();
            MySqlParameter p;

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    if (pesquisa.Quantidade > 0)
                        top.AppendFormat(" LIMIT {0} ", pesquisa.Quantidade);

                    #region Campo Código

                    if (pesquisa.Codigo >= 0)
                    {
                        filtro.Append(" AND CampanhasClinicas.codigo = @codigo");
                        p = new MySqlParameter("@codigo", pesquisa.Codigo);
                        p.MySqlDbType = GetInfoCodigo().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Campanha

                    if (pesquisa.Campanha >= 0)
                    {
                        filtro.Append(" AND CampanhasClinicas.campanha = @campanha");
                        p = new MySqlParameter("@campanha", pesquisa.Campanha);
                        p.MySqlDbType = GetInfoCampanha().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Clinica

                    if (pesquisa.Clinica >= 0)
                    {
                        filtro.Append(" AND CampanhasClinicas.clinica = @clinica");
                        p = new MySqlParameter("@clinica", pesquisa.Clinica);
                        p.MySqlDbType = GetInfoClinica().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campos UF e Cidade

                    if (!String.IsNullOrEmpty(pesquisa.Uf) && pesquisa.Uf != "-1")
                    {
                        string siglaEstado = String.Format("{0}", pesquisa.Uf);
                        filtro.Append(" AND Clinicas.estado = @estado ");
                        p = new MySqlParameter("@estado", siglaEstado);
                        p.MySqlDbType = MySqlDbType.VarChar;
                        sql.Parameters.Add(p);

                        if (pesquisa.Cidade >= 0)
                        {
                            filtro.Append(" AND Clinicas.cidade = @cidade");
                            p = new MySqlParameter("@cidade", pesquisa.Cidade);
                            p.MySqlDbType = MySqlDbType.Int32;
                            sql.Parameters.Add(p);
                        }
                    }

                    #endregion

                    #region Campo Livre

                    if (!String.IsNullOrEmpty(pesquisa.CampoLivre.Trim()))
                    {
                        string campoLivre = String.Format("%{0}%", pesquisa.CampoLivre.Trim());
                        filtro.Append(" AND (Clinicas.nomefantasia LIKE @campoLivre ");
                        filtro.Append(" OR Clinicas.endereco LIKE @campoLivre ");
                        filtro.Append(" OR Clinicas.bairro LIKE @campoLivre ");
                        filtro.Append(" OR Clinicas.contatocomercial LIKE @campoLivre ");
                        filtro.Append(" OR Clinicas.contatooperacional LIKE @campoLivre ");
                        filtro.Append(")");
                        p = new MySqlParameter("@campoLivre", campoLivre);
                        p.MySqlDbType = MySqlDbType.VarChar;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    if (filtro.Length > 0)
                        filtro = filtro.Replace(" AND ", " WHERE ", 0, 5);

                    #region Ordenação

                    switch (pesquisa.Ordenacao)
                    {
                        case Utils.TipoOrdenacao.porCodigo:
                            {
                                ordenacao.Append("ORDER BY CampanhasClinicas.codigo");
                            }
                            break;
                        case Utils.TipoOrdenacao.porCodigoDesc:
                            {
                                ordenacao.Append("ORDER BY CampanhasClinicas.codigo DESC");
                            }
                            break;
                        case Utils.TipoOrdenacao.porNome:
                            {
                                ordenacao.Append("ORDER BY Clinicas.nomefantasia ");
                            } break;
                        case Utils.TipoOrdenacao.porNomeDesc:
                            {
                                ordenacao.Append("ORDER BY Clinicas.nomefantasia DESC ");
                            }
                            break;
                        case Utils.TipoOrdenacao.porData:
                            {
                                ordenacao.Append("ORDER BY CampanhasClinicas.datacriacao");
                            }
                            break;
                        case Utils.TipoOrdenacao.porDataDesc:
                            {
                                ordenacao.Append("ORDER BY CampanhasClinicas.datacriacao DESC");
                            }
                            break;
                        case Utils.TipoOrdenacao.randomico:
                            {
                                ordenacao.Append("ORDER BY RAND()");
                            }
                            break;
                        default:
                            {
                                ordenacao.Append("ORDER BY CampanhasClinicas.codigo");
                            }
                            break;
                    }

                    #endregion

                    sql.CommandText = String.Format("SELECT "
                        + "Clinicas.codigo AS codigoClinica, Clinicas.nomefantasia AS nomeClinica, "
                        + "Clinicas.endereco AS enderecoClinica, Clinicas.numero AS numeroClinica, Clinicas.bairro AS bairroClinica,  "
                        + "Campanhas.codigo AS codigoCampanha, Campanhas.nome AS nomeCampanha, Campanhas.identificacao AS idCampanha, "
                        + "Cidades.nome_cidade AS nomeCidade, Cidades.sigla_estado AS ufCidade, "
                        + "CampanhasClinicas.* "
                        + "FROM CampanhasClinicas "
                        + "INNER JOIN Clinicas ON Clinicas.codigo = CampanhasClinicas.clinica "
                        + "INNER JOIN Campanhas ON Campanhas.codigo = CampanhasClinicas.campanha "
                        + "LEFT JOIN Cidades ON Cidades.codigo_cidade = Clinicas.cidade "
                        + "{0} {1} {2}", filtro, ordenacao, top);
                    DataTable dt = new Data().getData(sql);
                    return MontarRetorno(dt);
                }
            }
            catch (ImunneVacinas.ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Rotina que retorna uma lista genérica do elemento, de acordo com o retorno da Consultar ao BD
        /// </summary>
        /// <param name="dt">VALUE: DataTable com o resultado da Consultar ao BD</param>
        /// <returns>Lista do elemento com os dados retornados da consulta</returns>
        internal static List<CampanhaClinica> MontarRetorno(DataTable dt)
        {
            int i = -1;
            List<CampanhaClinica> list = new List<CampanhaClinica>();

            if (dt == null && dt.Rows.Count == 0)
                return list;
            else
            {
                CampanhaClinica retorno;
                try
                {
                    for (i = 0; i < dt.Rows.Count; i++)
                    {
                        retorno = new CampanhaClinica()
                        {
                            _codigo = Parser.parseInteiro(dt.Rows[i]["codigo"]),
                            _dataCriacao = Convert.ToDateTime(dt.Rows[i]["datacriacao"])
                        };

                        retorno._idCampanha.Codigo = Parser.parseInteiro(dt.Rows[i]["codigoCampanha"]);
                        retorno._idCampanha.Nome = Parser.parseString(dt.Rows[i]["nomeCampanha"]);
                        retorno._idCampanha.Identificacao = Parser.parseString(dt.Rows[i]["idCampanha"]);

                        retorno._idClinica.Codigo = Parser.parseInteiro(dt.Rows[i]["codigoClinica"]);
                        retorno._idClinica.NomeFantasia = Parser.parseString(dt.Rows[i]["nomeClinica"]);
                        retorno._idClinica.Bairro = Parser.parseString(dt.Rows[i]["bairroClinica"]);
                        retorno._idClinica.Endereco = Parser.parseString(dt.Rows[i]["enderecoClinica"]);
                        retorno._idClinica.Numero = Parser.parseString(dt.Rows[i]["numeroClinica"]);
                        retorno._idClinica.IdCidade.Nome = Parser.parseString(dt.Rows[i]["nomeCidade"]);
                        retorno._idClinica.IdUf.Sigla = Parser.parseString(dt.Rows[i]["ufCidade"]);

                        if (!String.IsNullOrEmpty(dt.Rows[i]["dataalteracao"].ToString()))
                            retorno._dataAlteracao = Convert.ToDateTime(dt.Rows[i]["dataalteracao"]);

                        list.Add(retorno);
                    }
                    return list;
                }
                catch (Exception ex)
                {
                    ex.Data.Add("último retorno lido", i);
                    throw;
                }
            }
        }
    }
}