﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ImunneVacinas
{
    /// <summary>
    /// Descrição resumida de Integracoes
    /// </summary>
    public class Integracao
    {
        public string token_account { get; set; }
        public Customer customer { get; set; }
        public List<TransactionProduct> transaction_product { get; set; }
        public Transaction transaction { get; set; }
        public TransactionTrace transaction_trace { get; set; }
        public Payment payment { get; set; }
    }

    public class Address
    {
        public string type_address { get; set; }
        public string postal_code { get; set; }
        public string street { get; set; }
        public string number { get; set; }
        public string completion { get; set; }
        public string neighborhood { get; set; }
        public string city { get; set; }
        public string state { get; set; }
    }

    public class Contact
    {
        public string type_contact { get; set; }
        public string number_contact { get; set; }
    }

    public class Customer
    {
        public List<Contact> contacts { get; set; }
        public List<Address> addresses { get; set; }
        public string name { get; set; }
        public string birth_date { get; set; }
        public string cpf { get; set; }
        public string email { get; set; }
    }

    public class Payment
    {
        public string payment_method_id { get; set; }
        public string card_name { get; set; }
        public string card_number { get; set; }
        public string card_expdate_month { get; set; }
        public string card_expdate_year { get; set; }
        public string card_cvv { get; set; }
        public string split { get; set; }
    }

    public class Transaction
    {
        public string available_payment_methods { get; set; }
        public string customer_ip { get; set; }
        public string shipping_type { get; set; }
        public string shipping_price { get; set; }
        public string price_discount { get; set; }
        public string url_notification { get; set; }
        public string free { get; set; }
    }

    public class TransactionProduct
    {
        public string description { get; set; }
        public string quantity { get; set; }
        public string price_unit { get; set; }
        public string code { get; set; }
        public string sku_code { get; set; }
        public string extra { get; set; }
    }

    public class TransactionTrace
    {
        public string estimated_date { get; set; }
    }
}