﻿using System;
using System.Text;
using System.IO;
using System.Security.Cryptography;

namespace ImunneVacinas
{
    public class Criptografia
    {

        private static byte[] chave = {

    };
        private static byte[] iv = {
        12,
        34,
        56,
        78,
        90,
        102,
        114,
        126

    };

        public Criptografia()
        {
        }

        public static string Criptografar(string valor)
        {
            DESCryptoServiceProvider des = null;
            MemoryStream ms = null;
            CryptoStream cs = null;
            byte[] input = null;
            try
            {
                des = new DESCryptoServiceProvider();
                ms = new MemoryStream();

                string chaveCriptografia = "#!$a48?@";
                //string chaveCriptografia = "#!$a36?@";
                input = Encoding.UTF8.GetBytes(valor);
                chave = Encoding.UTF8.GetBytes(chaveCriptografia.Substring(0, 8));
                cs = new CryptoStream(ms, des.CreateEncryptor(chave, iv), CryptoStreamMode.Write);
                cs.Write(input, 0, input.Length);
                cs.FlushFinalBlock();

                return Convert.ToBase64String(ms.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string Descriptografar(string valor)
        {
            DESCryptoServiceProvider des = null;
            MemoryStream ms = null;
            CryptoStream cs = null;
            byte[] input = null;
            string chaveCriptografia = "#!$a48?@";
            //string chaveCriptografia = "#!$a36?@";
            try
            {
                des = new DESCryptoServiceProvider();
                ms = new MemoryStream();

                input = new byte[valor.Length];
                input = Convert.FromBase64String(valor.Replace(" ", "+"));

                chave = Encoding.UTF8.GetBytes(chaveCriptografia.Substring(0, 8));

                cs = new CryptoStream(ms, des.CreateDecryptor(chave, iv), CryptoStreamMode.Write);
                cs.Write(input, 0, input.Length);
                cs.FlushFinalBlock();

                return Encoding.UTF8.GetString(ms.ToArray());
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }
}