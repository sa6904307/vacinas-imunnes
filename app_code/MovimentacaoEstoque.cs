﻿using MySql.Data.MySqlClient;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace ImunneVacinas
{
    /// <summary>
    /// Métodos e propriedades do elemento
    /// </summary>
    [Serializable]
    public class MovimentacaoEstoque
    {
        private static string _tabela = "MovimentacoesEstoques";
        private static List<BD.Columns> columns;
        private static DateTime columnsUpdated;

        #region PROPRIEDADES

        private Clinica _clinica;
        private Usuario _usuario;
        private Vacina _produto;
        private int _codigo;
        private Utils.TipoMovimentacao _tipo;
        private decimal _quantidade;
        private string _historico;
        private DateTime _dataCriacao;

        public Clinica Clinica
        {
            get
            {
                return _clinica;
            }

            set
            {
                _clinica = value;
            }
        }

        public Usuario Usuario
        {
            get
            {
                return _usuario;
            }

            set
            {
                _usuario = value;
            }
        }

        public Vacina Produto
        {
            get { return _produto;  }
            set { _produto = value; }
        }

        public int Codigo
        {
            get
            {
                return _codigo;
            }

            set
            {
                _codigo = value;
            }
        }

        public Utils.TipoMovimentacao Tipo
        {
            get
            {
                return _tipo;
            }

            set
            {
                _tipo = value;
            }
        }

        public decimal Quantidade
        {
            get
            {
                return _quantidade;
            }

            set
            {
                _quantidade = value;
            }
        }

        public string Historico
        {
            get
            {
                return _historico;
            }

            set
            {
                _historico = value;
            }
        }

        public DateTime DataCriacao
        {
            get
            {
                return _dataCriacao;
            }

            set
            {
                _dataCriacao = value;
            }
        }


        #endregion

        #region GETINFO

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos os campos da tabela.
        /// </summary>
        private static void GetInfoTabela()
        {
            if (columns == null || (DateTime.Now - columnsUpdated).Minutes > 2)
            {
                columns = BD.Columns.getColumnsInfo(_tabela);
                columnsUpdated = DateTime.Now;
            }
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo clinica.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoClinica()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "clinica");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo usuario.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoUsuario()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "usuario");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo produto.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoProduto()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "produto");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo codigo.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoCodigo()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "codigo");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo tipo.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoTipo()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "tipo");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo quantidade.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoQuantidade()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "quantidade");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo historico.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoHistorico()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "historico");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo datacriacao.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoDataCriacao()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "datacriacao");
        }

        #endregion

        /// <summary> 
        /// Rotina que cria um Array com os parâmetros utilizados nas ações de INSERT, UPDATE e DELETE
        /// <returns>Array com vários MySqlParameters</returns>
        /// </summary>
        private Array GetParametrosSQL()
        {
            MySqlParameter p;
            ArrayList ap = new ArrayList();

            p = new MySqlParameter("@clinica", Parser.parseInteiro(_clinica.Codigo));
            p.MySqlDbType = GetInfoClinica().Type;
            ap.Add(p);

            p = new MySqlParameter("@usuario", Parser.parseInteiro(_usuario.Codigo));
            p.MySqlDbType = GetInfoUsuario().Type;
            ap.Add(p);

            p = new MySqlParameter("@produto", Parser.parseInteiro(_produto.Codigo));
            p.MySqlDbType = GetInfoProduto().Type;
            ap.Add(p);

            p = new MySqlParameter("@codigo", Parser.parseInteiro(_codigo));
            p.MySqlDbType = GetInfoCodigo().Type;
            ap.Add(p);

            p = new MySqlParameter("@tipo", (int)_tipo);
            p.MySqlDbType = GetInfoTipo().Type;
            ap.Add(p);

            p = new MySqlParameter("@quantidade", Parser.parseInteiro(_quantidade));
            p.MySqlDbType = GetInfoCodigo().Type;
            ap.Add(p);

            p = new MySqlParameter("@historico", Parser.parseString(_historico));
            p.MySqlDbType = GetInfoHistorico().Type;
            ap.Add(p);

            p = new MySqlParameter("@datacriacao", Parser.parseDateTimeBD(_dataCriacao));
            p.MySqlDbType = GetInfoDataCriacao().Type;
            ap.Add(p);

            return ap.ToArray();
        }

        /// <summary>
        /// Inicialização das propriedades do elemento com seus valores padrão
        /// </summary>
        private void IniciarElemento()
        {
            GetInfoTabela();
            _clinica = new Clinica();
            _usuario = new Usuario();
            _produto = new Vacina();

            _clinica.Codigo = -1;
            _usuario.Codigo = -1;
            _produto.Codigo = -1;

            _quantidade = 1;
            _tipo = Utils.TipoMovimentacao.Indiferente;
            _historico = String.Empty;
            _dataCriacao = Utils.dataPadrao;
        }

        /// <summary>
        /// Inicialização do elemento
        /// </summary>
        public MovimentacaoEstoque()
        {
            IniciarElemento();
        }

        /// <summary>
        /// Rotina que insere o registro no BD
        /// </summary>
        /// <returns>Código do registro inserido no BD</returns>
        public int Incluir()
        {
            if (_clinica.Codigo <= 0 || _usuario.Codigo <= 0)
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    sql.Parameters.AddRange(GetParametrosSQL());
                    sql.CommandText = "INSERT INTO MovimentacoesEstoques "
                                    + "(clinica, usuario, quantidade, produto, tipo, historico, datacriacao) "
                                    + "VALUE (@clinica, @usuario, @quantidade, @produto, @tipo, @historico, @datacriacao); "
                                    + "SELECT LAST_INSERT_ID();";
                    int resultado = Parser.parseInteiro(new Data().executeEscalar(sql));
                    return resultado;
                }
            }
            catch (ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Rotina que exclui o registro do BD
        /// </summary>
        /// <returns>Quantidade de linhas alteradas na ação junto ao BD</returns>
        public int Excluir()
        {
            if (_codigo < 0)
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    sql.Parameters.AddRange(GetParametrosSQL());
                    sql.CommandText = "DELETE FROM MovimentacoesEstoques "
                                    + "WHERE codigo = @codigo";
                    int resultado = Parser.parseInteiro(new Data().execute(sql));
                    return resultado;
                }
            }
            catch (ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Rotina que localiza e retorna um único registro, mediante o código informado
        /// </summary>
        /// <param name="codigo">VALUE: código de identificação do registro (obrigatório)</param>
        /// <returns>Elemento Usuario com os dados retornados da consulta</returns>
        public static MovimentacaoEstoque ConsultarUnico(int codigo)
        {
            if (codigo < 0)
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            ImunneVacinas.PesquisaMovimentacaoEstoque pesquisa = new PesquisaMovimentacaoEstoque()
            {
                Codigo = codigo
            };

            List<MovimentacaoEstoque> retorno = ConsultaGenerica(pesquisa);
            if (retorno == null || retorno.Count.Equals(0))
                return null;
            else
                return retorno[0];
        }

        /// <summary>
        /// Rotina que contabiliza o total das quantidades de movimentações
        /// </summary>
        /// <param name="pesquisa">VALUE: parametros para realização da pesquisa</param>
        /// <returns></returns>
        public static decimal ConsultarTotaisMovimentacao(PesquisaMovimentacaoEstoque pesquisa)
        {
            decimal total = 0;

            if (pesquisa.Clinica > 0)
            {
                List<MovimentacaoEstoque> listaRetorno = ConsultaGenerica(pesquisa);
                if (listaRetorno != null && listaRetorno.Count > 0)
                {
                    foreach (MovimentacaoEstoque item in listaRetorno)
                    {
                        total += Parser.parseDecimal(item.Quantidade);
                    }
                }
            }

            return total;
        }

        /// <summary>
        /// Rotina que realiza a pesquisa de elementos mediante os parametros de pesquisa informados
        /// </summary>
        /// <param name="pesquisa">VALUE: elemento de pesquisa com os parametros para consulta</param>
        /// <returns></returns>
        public static List<MovimentacaoEstoque> ConsultarMovimentacoes(PesquisaMovimentacaoEstoque pesquisa)
        {
            return ConsultaGenerica(pesquisa);
        }

        /// <summary>
        /// Rotina interna utilizada para todas as pesquisas do elemento
        /// </summary>
        /// <param name="pesquisa">VALUE: Elemento com os parâmetros com os quais será realizada a pesquisa</param>
        /// <returns>Lista do elemento com os dados retornados da consulta</returns>
        internal static List<MovimentacaoEstoque> ConsultaGenerica(PesquisaMovimentacaoEstoque pesquisa)
        {
            StringBuilder ordenacao = new StringBuilder();
            StringBuilder filtro = new StringBuilder();
            StringBuilder top = new StringBuilder();
            MySqlParameter p;

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    if (pesquisa.Quantidade > 0)
                        top.AppendFormat(" LIMIT {0} ", pesquisa.Quantidade);

                    #region CAMPO CÓDIGO

                    if (pesquisa.Codigo >= 0)
                    {
                        filtro.Append(" AND MovimentacoesEstoques.codigo = @codigo");
                        p = new MySqlParameter("@codigo", pesquisa.Codigo);
                        p.MySqlDbType = GetInfoCodigo().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region CAMPO CLÍNICA

                    if (pesquisa.Clinica >= 0)
                    {
                        filtro.Append(" AND MovimentacoesEstoques.clinica = @clinica");
                        p = new MySqlParameter("@clinica", pesquisa.Clinica);
                        p.MySqlDbType = GetInfoClinica().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region CAMPO USUÁRIO

                    if (pesquisa.Usuario >= 0)
                    {
                        filtro.Append(" AND MovimentacoesEstoques.usuario = @usuario");
                        p = new MySqlParameter("@usuario", pesquisa.Usuario);
                        p.MySqlDbType = GetInfoUsuario().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region CAMPO PRODUTO

                    if (pesquisa.Produto >= 0)
                    {
                        filtro.Append(" AND MovimentacoesEstoques.produto = @produto");
                        p = new MySqlParameter("@produto", pesquisa.Produto);
                        p.MySqlDbType = GetInfoProduto().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region CAMPO TIPO

                    if (pesquisa.Tipo != ImunneVacinas.Utils.TipoMovimentacao.Indiferente)
                    {
                        filtro.Append(" AND MovimentacoesEstoques.tipo = @tipo");
                        p = new MySqlParameter("@tipo", (int)pesquisa.Tipo);
                        p.MySqlDbType = GetInfoTipo().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region CAMPO LIVRE

                    if (!String.IsNullOrEmpty(pesquisa.CampoLivre))
                    {
                        string campoLivre = String.Format("%{0}%", pesquisa.CampoLivre.Trim());
                        filtro.Append(" AND MovimentacoesEstoques.historico LIKE @campoLivre ");
                        p = new MySqlParameter("@campoLivre", campoLivre);
                        p.MySqlDbType = MySqlDbType.VarChar;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region CAMPOS DATA COMPRA

                    if (pesquisa.DataInicio != Utils.dataPadrao && pesquisa.DataFim != Utils.dataPadrao)
                    {
                        filtro.Append(" AND MovimentacoesEstoques.datacriacao >= @inicio");
                        p = new MySqlParameter("@inicio", Parser.parseDateTimeBD(pesquisa.DataInicio))
                        {
                            MySqlDbType = GetInfoDataCriacao().Type
                        };
                        sql.Parameters.Add(p);

                        filtro.Append(" AND MovimentacoesEstoques.datacriacao < @termino");
                        p = new MySqlParameter("@termino", Parser.parseDateTimeBD(pesquisa.DataFim))
                        {
                            MySqlDbType = GetInfoDataCriacao().Type
                        };
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    if (filtro.Length > 0)
                        filtro = filtro.Replace(" AND ", " WHERE ", 0, 5);

                    #region ORDENAÇÃO

                    switch (pesquisa.Ordenacao)
                    {
                        case Utils.TipoOrdenacao.porCodigo:
                            {
                                ordenacao.Append("ORDER BY MovimentacoesEstoques.codigo");
                            }
                            break;
                        case Utils.TipoOrdenacao.porCodigoDesc:
                            {
                                ordenacao.Append("ORDER BY MovimentacoesEstoques.codigo DESC");
                            }
                            break;
                        case Utils.TipoOrdenacao.porData:
                            {
                                ordenacao.Append("ORDER BY MovimentacoesEstoques.datacriacao");
                            }
                            break;
                        case Utils.TipoOrdenacao.porDataDesc:
                            {
                                ordenacao.Append("ORDER BY MovimentacoesEstoques.datacriacao DESC");
                            }
                            break;
                        case Utils.TipoOrdenacao.randomico:
                            {
                                ordenacao.Append("ORDER BY RAND()");
                            }
                            break;
                        default:
                            {
                                ordenacao.Append("ORDER BY MovimentacoesEstoques.codigo");
                            }
                            break;
                    }

                    #endregion

                    sql.CommandText = String.Format("SELECT "
                        + "MovimentacoesEstoques.* "
                        + "FROM MovimentacoesEstoques "
                        + "{0} {1} {2}", filtro, ordenacao, top);
                    DataTable dt = new Data().getData(sql);
                    return MontarRetorno(dt);
                }
            }
            catch (ImunneVacinas.ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Rotina que retorna uma lista genérica do elemento, de acordo com o retorno da Consultar ao BD
        /// </summary>
        /// <param name="dt">VALUE: DataTable com o resultado da Consultar ao BD</param>
        /// <returns>Lista do elemento com os dados retornados da consulta</returns>
        internal static List<MovimentacaoEstoque> MontarRetorno(DataTable dt)
        {
            int i = -1;
            List<MovimentacaoEstoque> list = new List<MovimentacaoEstoque>();

            if (dt == null && dt.Rows.Count == 0)
                return list;
            else
            {
                MovimentacaoEstoque retorno;
                try
                {
                    for (i = 0; i < dt.Rows.Count; i++)
                    {
                        retorno = new MovimentacaoEstoque()
                        {
                            _codigo = Parser.parseInteiro(dt.Rows[i]["codigo"]),
                            _quantidade = Parser.parseInteiro(dt.Rows[i]["quantidade"]),
                            _tipo = (Utils.TipoMovimentacao)Enum.Parse(typeof(Utils.TipoMovimentacao), dt.Rows[i]["tipo"].ToString()),
                            _historico = Parser.parseString(dt.Rows[i]["historico"]),
                            _dataCriacao = Convert.ToDateTime(dt.Rows[i]["datacriacao"])
                        };

                        retorno._clinica.Codigo = Parser.parseInteiro(dt.Rows[i]["clinica"]);
                        retorno._usuario.Codigo = Parser.parseInteiro(dt.Rows[i]["usuario"]);
                        retorno._produto.Codigo = Parser.parseInteiro(dt.Rows[i]["produto"]);

                        list.Add(retorno);
                    }
                    return list;
                }
                catch (Exception ex)
                {
                    ex.Data.Add("último retorno lido", i);
                    throw;
                }
            }
        }
    }
}