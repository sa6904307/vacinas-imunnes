﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;

namespace ImunneVacinas
{
    /// <summary>
    /// Descrição resumida de ExportacoesExcel
    /// </summary>
    public class ExportacoesExcel
    {
        /// <summary>
        /// Método que gera um documento EXCEL com os dados mais relevantes de clínicas
        /// </summary>
        /// <param name="listaClinicas">VALUE: LIST com os elementos a serem tratados e exibidos no documento</param>
        public static void ExportarClinicas(List<Clinica> listaClinicas)
        {
            string nomeArquivo = String.Format("clinicas-{0}.xlsx", Guid.NewGuid().ToString());
            string caminhoArquivo = HttpContext.Current.Server.MapPath("~/logs/") + nomeArquivo;
            FileInfo novoArquivo = new FileInfo(caminhoArquivo);

            using (ExcelPackage pacote = new ExcelPackage(novoArquivo))
            {
                ExcelWorksheet planilha = pacote.Workbook.Worksheets.Add("RELATORIO");
                planilha.Cells["A1"].Value = "CPF/CNPJ";
                planilha.Cells["B1"].Value = "CLINICA";
                planilha.Cells["C1"].Value = "CEP";
                planilha.Cells["D1"].Value = "ENDERECO";
                planilha.Cells["E1"].Value = "NUMERO";
                planilha.Cells["F1"].Value = "COMPLEMENTO";
                planilha.Cells["G1"].Value = "BAIRRO";
                planilha.Cells["H1"].Value = "UF";
                planilha.Cells["I1"].Value = "CIDADE";
                planilha.Cells["J1"].Value = "CONTATO COMERCIAL";
                planilha.Cells["K1"].Value = "CONTATO OPERACIONAL";
                planilha.Cells["L1"].Value = "EMAIL FINANCEIRO";
                planilha.Cells["M1"].Value = "EMAIL GERAL";
                planilha.Cells["N1"].Value = "TELEFONE";
                planilha.Cells["O1"].Value = "OBSERVACOES";
                planilha.Cells["A1:O1"].Style.Font.Bold = true;
                planilha.Cells["A1:O1"].Style.Font.Color.SetColor(System.Drawing.Color.White);
                planilha.Cells["A1:O1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                planilha.Cells["A1:O1"].Style.Fill.BackgroundColor.SetColor(0, 10, 125, 25);

                int contador = 1;
                foreach (Clinica item in listaClinicas)
                {
                    contador += 1;

                    if (!String.IsNullOrEmpty(item.CpfCnpj))
                        planilha.Cells["A" + contador].Value = ImunneVacinas.Utils.FormatarCpfCnpj(item.CpfCnpj);
                    else
                        planilha.Cells["A" + contador].Value = "--";

                    planilha.Cells["B" + contador].Value = item.NomeFantasia.ToUpper();
                    planilha.Cells["C" + contador].Value = item.Cep;
                    planilha.Cells["D" + contador].Value = item.Endereco.ToUpper();
                    planilha.Cells["E" + contador].Value = item.Numero.ToUpper();
                    planilha.Cells["F" + contador].Value = item.Complemento.ToUpper();
                    planilha.Cells["G" + contador].Value = item.Bairro.ToUpper();
                    planilha.Cells["H" + contador].Value = item.IdUf.Sigla;

                    if (item.IdCidade.Codigo > 0)
                        planilha.Cells["I" + contador].Value = item.IdCidade.Nome.ToUpper();
                    else
                        planilha.Cells["I" + contador].Value = "--";

                    planilha.Cells["J" + contador].Value = item.ContatoComercial.ToUpper();
                    planilha.Cells["K" + contador].Value = item.ContatoOperacional.ToUpper();
                    planilha.Cells["L" + contador].Value = item.EmailFinanceiro.ToLower();
                    planilha.Cells["M" + contador].Value = item.EmailGeral.ToLower();
                    planilha.Cells["N" + contador].Value = item.Telefone;
                    planilha.Cells["O" + contador].Value = item.Observacoes.ToUpper();
                }

                pacote.Save();
                pacote.Dispose();
            }

            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.AddHeader("content-disposition", "attachment;  filename=" + nomeArquivo);
            HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            HttpContext.Current.Response.WriteFile(caminhoArquivo);
            HttpContext.Current.Response.Flush();

            File.Delete(caminhoArquivo);

            HttpContext.Current.Response.End();
        }

        /// <summary>
        /// Método que gera um documento EXCEL com os dados mais relevantes de clínicas
        /// </summary>
        /// <param name="listaClinicas">VALUE: LIST com os elementos a serem tratados e exibidos no documento</param>
        public static void NovoExportarClinicas(List<Clinica> listaClinicas)
        {
            string nomeArquivo = String.Format("clinicas-{0}.xlsx", Guid.NewGuid().ToString());
            string caminhoArquivo = HttpContext.Current.Server.MapPath("~/logs/") + nomeArquivo;
            FileInfo novoArquivo = new FileInfo(caminhoArquivo);

            using (ExcelPackage pacote = new ExcelPackage(novoArquivo))
            {
                ExcelWorksheet planilha = pacote.Workbook.Worksheets.Add("RELATORIO");
                planilha.Cells["A1"].Value = "CPF/CNPJ";
                planilha.Cells["B1"].Value = "CLINICA";
                planilha.Cells["C1"].Value = "CEP";
                planilha.Cells["D1"].Value = "ENDERECO";
                planilha.Cells["E1"].Value = "NUMERO";
                planilha.Cells["F1"].Value = "COMPLEMENTO";
                planilha.Cells["G1"].Value = "BAIRRO";
                planilha.Cells["H1"].Value = "UF";
                planilha.Cells["I1"].Value = "CIDADE";
                planilha.Cells["J1"].Value = "CONTATO COMERCIAL";
                planilha.Cells["K1"].Value = "CONTATO OPERACIONAL";
                planilha.Cells["L1"].Value = "EMAIL FINANCEIRO";
                planilha.Cells["M1"].Value = "EMAIL GERAL";
                planilha.Cells["N1"].Value = "TELEFONE";
                planilha.Cells["O1"].Value = "LOGIN";
                planilha.Cells["P1"].Value = "OBSERVACOES";

                planilha.Cells["A1:P1"].Style.Font.Bold = true;
                planilha.Cells["A1:P1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                planilha.Cells["A1:P1"].Style.Font.Color.SetColor(System.Drawing.Color.White);
                planilha.Cells["A1:P1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                planilha.Cells["A1:P1"].Style.Fill.BackgroundColor.SetColor(0, 10, 125, 25);
                planilha.Row(1).Height = 20;

                planilha.Cells["E1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                planilha.Cells["H1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                int contador = 1;
                foreach (Clinica item in listaClinicas)
                {
                    contador += 1;

                    if (!String.IsNullOrEmpty(item.CpfCnpj))
                        planilha.Cells["A" + contador].Value = ImunneVacinas.Utils.FormatarCpfCnpj(item.CpfCnpj);
                    else
                        planilha.Cells["A" + contador].Value = "--";

                    planilha.Cells["B" + contador].Value = item.NomeFantasia.ToUpper();
                    planilha.Cells["C" + contador].Value = item.Cep;
                    planilha.Cells["D" + contador].Value = item.Endereco.ToUpper();
                    planilha.Cells["E" + contador].Value = item.Numero.ToUpper();
                    planilha.Cells["F" + contador].Value = item.Complemento.ToUpper();
                    planilha.Cells["G" + contador].Value = item.Bairro.ToUpper();
                    planilha.Cells["H" + contador].Value = item.IdUf.Sigla;

                    if (item.IdCidade.Codigo > 0)
                        planilha.Cells["I" + contador].Value = item.IdCidade.Nome.ToUpper();
                    else planilha.Cells["I" + contador].Value = "--";

                    planilha.Cells["J" + contador].Value = item.ContatoComercial.ToUpper();
                    planilha.Cells["K" + contador].Value = item.ContatoOperacional.ToUpper();
                    planilha.Cells["L" + contador].Value = item.EmailFinanceiro.ToLower();
                    planilha.Cells["M" + contador].Value = item.EmailGeral.ToLower();
                    planilha.Cells["N" + contador].Value = item.Telefone;
                    planilha.Cells["P" + contador].Value = item.Observacoes.ToUpper();

                    #region USUÁRIO CLÍNICA

                    ImunneVacinas.PesquisaUsuario pesquisaUsuario = new PesquisaUsuario()
                    {
                        Quantidade = 1,
                        Clinica = item.Codigo,
                        Situacao = Utils.SituacaoRegistro.Ativo,
                        Tipo = 7
                    };

                    List<ImunneVacinas.Usuario> usuariosClinica = ImunneVacinas.Usuario.ConsultarUsuarios(pesquisaUsuario);
                    if (usuariosClinica != null && usuariosClinica.Count > 0)
                        planilha.Cells["O" + contador].Value = usuariosClinica[0].Login.ToLower();
                    else planilha.Cells["O" + contador].Value = "--";

                    #endregion
                }

                pacote.Save();
                pacote.Dispose();
            }

            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.AddHeader("content-disposition", "attachment;  filename=" + nomeArquivo);
            HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            HttpContext.Current.Response.WriteFile(caminhoArquivo);
            HttpContext.Current.Response.Flush();

            File.Delete(caminhoArquivo);

            HttpContext.Current.Response.End();
        }

        /// <summary>
        /// Rotina que gera um XLS com os dados de compras para conciliação
        /// </summary>
        /// <param name="listaItens">VALUE: Lista com compras resultadas da pesquisa em tela</param>
        public static void ExportarConciliacoes(List<Compra> listaCompras)
        {
            string nomeArquivo = String.Format("CONCILIACOES-{0}.xlsx", Guid.NewGuid().ToString());
            string caminhoArquivo = HttpContext.Current.Server.MapPath("~/logs/") + nomeArquivo;
            FileInfo novoArquivo = new FileInfo(caminhoArquivo);

            using (ExcelPackage pacote = new ExcelPackage(novoArquivo))
            {
                ExcelWorksheet planilha = pacote.Workbook.Worksheets.Add("CONCILICAÇÕES");

                #region NOMES & COLUNAS

                planilha.Cells["A1"].Value = "DATA";
                planilha.Cells["B1"].Value = "CPF/CNPJ";
                planilha.Cells["C1"].Value = "COMPRADOR";
                planilha.Cells["D1"].Value = "Nº. CARTÃO";
                planilha.Cells["E1"].Value = "CARTÃO";
                planilha.Cells["F1"].Value = "AUTORIZAÇÃO";
                planilha.Cells["G1"].Value = "DESCRIÇÃO";
                planilha.Cells["H1"].Value = "PARCELAS";
                planilha.Cells["I1"].Value = "VALOR";

                #endregion

                #region ESTILOS E CUSTOMIZAÇÕES

                planilha.Cells["A1:I1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                planilha.Cells["A1:I1"].Style.Font.Bold = true;
                planilha.Cells["A1:I1"].Style.Font.Color.SetColor(System.Drawing.Color.White);
                planilha.Cells["A1:I1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                planilha.Cells["A1:I1"].Style.Fill.BackgroundColor.SetColor(0, 10, 125, 25);
                planilha.Cells["A1:I1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                planilha.Cells["A1:I1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                planilha.Row(1).Height = 20;

                #endregion

                int contador = 2;

                foreach (Compra item in listaCompras)
                {
                    StringBuilder numeroCartao = new StringBuilder();
                    StringBuilder nomeCartao = new StringBuilder();
                    StringBuilder descricaoCompra = new StringBuilder();

                    #region ESTILOS

                    planilha.Cells["A" + contador + ":I" + contador].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    planilha.Cells["A" + contador + ":I" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                    planilha.Row(contador).Height = 20;

                    planilha.Cells["A" + contador].AutoFitColumns(22);
                    planilha.Cells["B" + contador].AutoFitColumns(22);
                    planilha.Cells["C" + contador].AutoFitColumns(35);
                    planilha.Cells["D" + contador].AutoFitColumns(30);
                    planilha.Cells["E" + contador].AutoFitColumns(30);
                    planilha.Cells["F" + contador].AutoFitColumns(30);
                    planilha.Cells["G" + contador].AutoFitColumns(30);
                    planilha.Cells["H" + contador].AutoFitColumns(22);
                    planilha.Cells["I" + contador].AutoFitColumns(22);

                    #endregion

                    try
                    {
                        ImunneVacinas.TransacaoRealizada transacaoCompra = ImunneVacinas.TransacaoRealizada.ConsultarUnicoCompra(item.Codigo);

                        if (transacaoCompra != null)
                        {
                            #region TRATANDO O CARTÃO

                            if (!String.IsNullOrEmpty(transacaoCompra.numeroCartao))
                            {
                                nomeCartao.AppendFormat(ImunneVacinas.Cartao.RetornarNomeCartao(transacaoCompra.numeroCartao));

                                string cartaoTrabalho = transacaoCompra.numeroCartao.Replace(" ", "").Trim();

                                for (int i = 0; i < cartaoTrabalho.Trim().Length; i++)
                                {
                                    if (i <= 3 || i >= 12)
                                        numeroCartao.Append(cartaoTrabalho[i]);
                                    else numeroCartao.AppendFormat("*");
                                }

                            }

                            #endregion

                            if (item.Parcelas > 1)
                                descricaoCompra.AppendFormat("PARCELADO EM {0}X", item.Parcelas);
                            else descricaoCompra.Append("COMPRA À VISTA");

                            planilha.Cells["A" + contador].Value = item.DataCompra.ToString("dd/MM/yyyy HH:mm");
                            planilha.Cells["B" + contador].Value = ImunneVacinas.Utils.FormatarCpfCnpj(item.IdParticipante.Cpf);
                            planilha.Cells["C" + contador].Value = item.IdParticipante.Nome.ToUpper();
                            planilha.Cells["D" + contador].Value = numeroCartao.ToString();
                            planilha.Cells["E" + contador].Value = nomeCartao.ToString().ToUpper();
                            planilha.Cells["F" + contador].Value = transacaoCompra.autorizacao.ToString();
                            planilha.Cells["G" + contador].Value = descricaoCompra.ToString();
                            planilha.Cells["H" + contador].Value = item.Parcelas.ToString();
                            planilha.Cells["I" + contador].Value = ImunneVacinas.Utils.ToMoeda(item.Valor); ;

                            contador += 1;
                        }
                    }
                    catch (Exception ex)
                    {
                        Erro erro = new Erro();
                        erro.addArquivo(ex);
                    }
                }

                contador += 1;

                pacote.Save();
                pacote.Dispose();
            }

            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.AddHeader("content-disposition", "attachment;  filename=" + nomeArquivo);
            HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            HttpContext.Current.Response.WriteFile(caminhoArquivo);
            HttpContext.Current.Response.Flush();

            File.Delete(caminhoArquivo);

            HttpContext.Current.Response.SuppressContent = true;
            HttpContext.Current.ApplicationInstance.CompleteRequest();
        }

        /// <summary>
        /// Rotina que gera um XLS com os dados de compras
        /// </summary>
        /// <param name="listaItens">VALUE: Lista com compras resultadas da pesquisa em tela</param>
        public static void ExportarCompras(List<Compra> listaCompras)
        {
            string nomeArquivo = String.Format("compras-{0}.xlsx", Guid.NewGuid().ToString());
            string caminhoArquivo = HttpContext.Current.Server.MapPath("~/logs/") + nomeArquivo;
            FileInfo novoArquivo = new FileInfo(caminhoArquivo);

            using (ExcelPackage pacote = new ExcelPackage(novoArquivo))
            {
                ExcelWorksheet planilha = pacote.Workbook.Worksheets.Add("RELATORIO");

                #region NOMES & COLUNAS

                planilha.Cells["A1"].Value = "CAMPANHA";
                planilha.Cells["B1"].Value = "EMPRESA";
                planilha.Cells["C1"].Value = "PEDIDO";
                planilha.Cells["D1"].Value = "COMPROVANTE";
                planilha.Cells["E1"].Value = "SITUAÇÃO";
                planilha.Cells["F1"].Value = "CATEGORIA";
                planilha.Cells["G1"].Value = "DATA COMPRA";
                planilha.Cells["H1"].Value = "CPF/CNPJ";
                planilha.Cells["I1"].Value = "TITULAR";
                planilha.Cells["J1"].Value = "E-MAIL";
                planilha.Cells["K1"].Value = "TELEFONE";
                planilha.Cells["L1"].Value = "CLINICA";
                planilha.Cells["M1"].Value = "CIDADE";
                planilha.Cells["N1"].Value = "UF";
                planilha.Cells["O1"].Value = "APLICAÇÃO";

                // TOTAIS E VALORES
                planilha.Cells["P1"].Value = "APLICADOS";
                planilha.Cells["Q1"].Value = "VALOR";
                planilha.Cells["R1"].Value = "Ñ APLICADOS";
                planilha.Cells["S1"].Value = "VALOR";
                planilha.Cells["T1"].Value = "TOTAL";
                planilha.Cells["U1"].Value = "VALOR";
                planilha.Cells["V1"].Value = "PARCELAS";

                #endregion

                #region ESTILOS E CUSTOMIZAÇÕES

                planilha.Cells["A1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                planilha.Cells["B1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                planilha.Cells["C1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                planilha.Cells["D1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                planilha.Cells["E1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                planilha.Cells["F1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                planilha.Cells["G1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                planilha.Cells["H1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                planilha.Cells["I1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                planilha.Cells["J1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                planilha.Cells["K1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                planilha.Cells["L1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                planilha.Cells["M1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                planilha.Cells["N1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                planilha.Cells["O1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                planilha.Cells["P1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                planilha.Cells["Q1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                planilha.Cells["R1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                planilha.Cells["S1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                planilha.Cells["T1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                planilha.Cells["U1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                planilha.Cells["A1:V1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                planilha.Cells["A1:V1"].Style.Font.Bold = true;
                planilha.Cells["A1:V1"].Style.Font.Color.SetColor(System.Drawing.Color.White);
                planilha.Cells["A1:V1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                planilha.Cells["A1:V1"].Style.Fill.BackgroundColor.SetColor(0, 10, 125, 25);
                planilha.Cells["A1:V1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                planilha.Row(1).Height = 20;

                #endregion

                int contador = 1;
                int totalFinalAplicados = 0;
                int totalFinalNaoAplicados = 0;
                int totalFinalGeral = 0;

                decimal valorFinalAplicados = 0;
                decimal valorFinalNaoAplicados = 0;
                decimal valorFinalGeral = 0;

                foreach (Compra item in listaCompras)
                {
                    try
                    {
                        contador += 1;

                        planilha.Cells["A" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        planilha.Cells["B" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        planilha.Cells["C" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        planilha.Cells["D" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        planilha.Cells["E" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        planilha.Cells["F" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        planilha.Cells["G" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        planilha.Cells["H" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        planilha.Cells["I" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        planilha.Cells["J" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        planilha.Cells["K" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        planilha.Cells["L" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        planilha.Cells["M" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        planilha.Cells["N" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        planilha.Cells["O" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        planilha.Cells["P" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        planilha.Cells["Q" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        planilha.Cells["R" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        planilha.Cells["S" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        planilha.Cells["T" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        planilha.Cells["U" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        planilha.Cells["A" + contador + ":U" + contador].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        planilha.Row(contador).Height = 20;

                        planilha.Cells["A" + contador].Value = item.IdCampanha.Identificacao.ToUpper();
                        planilha.Cells["B" + contador].Value = item.IdCampanha.IdEmpresa.NomeFantasia.ToUpper();
                        planilha.Cells["C" + contador].Value = item.Codigo.ToString().PadLeft(5, '0');

                        #region TRANSAÇÕES

                        if (!String.IsNullOrEmpty(item.NumeroComprovante))
                            planilha.Cells["D" + contador].Value = item.NumeroComprovante;
                        else planilha.Cells["D" + contador].Value = "--";

                        switch (item.Situacao)
                        {
                            case Compra.SituacaoCompra.Aberta:
                                {
                                    planilha.Cells["E" + contador].Value = "NÃO REALIZADA";
                                }
                                break;
                            case Compra.SituacaoCompra.Cancelada:
                                {
                                    planilha.Cells["E" + contador].Value = "NÃO AUTORIZADA";
                                }
                                break;
                            case Compra.SituacaoCompra.Concluida:
                                {
                                    planilha.Cells["E" + contador].Value = "AUTORIZADA";
                                }
                                break;
                        }

                        switch (item.Categoria)
                        {
                            case Compra.CategoriaCompra.Importada:
                                {
                                    planilha.Cells["F" + contador].Value = "IMPORTADA";
                                }
                                break;
                            case Compra.CategoriaCompra.Paga:
                                {
                                    planilha.Cells["F" + contador].Value = "PAGA";
                                }
                                break;
                            case Compra.CategoriaCompra.Gratis:
                                {
                                    planilha.Cells["F" + contador].Value = "GRÁTIS";
                                }
                                break;
                        }

                        #endregion

                        #region DADOS TITULAR

                        planilha.Cells["H" + contador].Value = ImunneVacinas.Utils.FormatarCpfCnpj(item.IdParticipante.Cpf);
                        planilha.Cells["I" + contador].Value = item.IdParticipante.Nome.ToUpper();

                        if (!String.IsNullOrEmpty(item.IdParticipante.Email))
                            planilha.Cells["J" + contador].Value = item.IdParticipante.Email.ToLower();
                        else
                            planilha.Cells["J" + contador].Value = "";

                        if (!String.IsNullOrEmpty(item.IdParticipante.Telefone))
                            planilha.Cells["K" + contador].Value = item.IdParticipante.Telefone;
                        else
                            planilha.Cells["L" + contador].Value = "";

                        #endregion

                        #region CLÍNICAS

                        if (!String.IsNullOrEmpty(item.IdClinica.NomeFantasia))
                            planilha.Cells["L" + contador].Value = item.IdClinica.NomeFantasia.ToUpper();
                        else planilha.Cells["L" + contador].Value = "--";

                        if (!String.IsNullOrEmpty(item.IdClinica.NomeFantasia))
                            planilha.Cells["M" + contador].Value = item.IdClinica.IdCidade.Nome.ToUpper();
                        else planilha.Cells["M" + contador].Value = "--";

                        if (!String.IsNullOrEmpty(item.IdClinica.NomeFantasia))
                            planilha.Cells["N" + contador].Value = item.IdClinica.IdUf.Sigla.ToUpper();
                        else planilha.Cells["N" + contador].Value = "--";

                        #endregion

                        #region TOTALIZAÇÕES

                        int totalAplicados = 0;
                        int totalGeral = 0;
                        int totalNaoAplicados = 0;

                        decimal valorAplicados = 0;
                        decimal valorGeral = 0;
                        decimal valorNaoAplicados = 0;

                        ImunneVacinas.PesquisaItemCompra pesquisaItem = new ImunneVacinas.PesquisaItemCompra()
                        {
                            Campanha = item.IdCampanha.Codigo,
                            Compra = item.Codigo
                        };

                        decimal valorProdutos = 0;

                        List<ImunneVacinas.ItemCompra> listaItens = ImunneVacinas.ItemCompra.ConsultarItens(pesquisaItem);
                        foreach (ImunneVacinas.ItemCompra itemCompra in listaItens)
                        {
                            valorProdutos += itemCompra.Valor;
                        }

                        #region VOUCHERS UTILIZADOS

                        ImunneVacinas.PesquisaVoucher pesquisaVouchers = new ImunneVacinas.PesquisaVoucher()
                        {
                            Situacao = ImunneVacinas.Voucher.SituacaoVoucher.Utilizado,
                            Compra = item.Codigo,
                            Ordenacao = ImunneVacinas.Utils.TipoOrdenacao.porData
                        };
                        List<ImunneVacinas.Voucher> listaUtilizados = ImunneVacinas.Voucher.ConsultarVouchers(pesquisaVouchers);
                        if (listaUtilizados != null && listaUtilizados.Count > 0)
                            totalAplicados = listaUtilizados.Count;

                        #endregion

                        #region VOUCHERS GERAIS

                        pesquisaVouchers = new ImunneVacinas.PesquisaVoucher()
                        {
                            Compra = item.Codigo,
                            Ordenacao = ImunneVacinas.Utils.TipoOrdenacao.porData
                        };
                        List<ImunneVacinas.Voucher> listaGeral = ImunneVacinas.Voucher.ConsultarVouchers(pesquisaVouchers);
                        if (listaGeral != null && listaGeral.Count > 0)
                            totalGeral = listaGeral.Count;

                        #endregion

                        totalNaoAplicados = (totalGeral - totalAplicados);

                        valorAplicados = (valorProdutos * totalAplicados);
                        valorNaoAplicados = (valorProdutos * totalNaoAplicados);
                        valorGeral = valorAplicados + valorNaoAplicados;

                        switch (item.AplicacaoCompra)
                        {
                            case ImunneVacinas.Compra.Aplicacao.Aplicado:
                                {
                                    planilha.Cells["O" + contador].Value = String.Format("APLICADO | {0}", item.DataAplicacao.ToString("dd/MM/yyyy HH:ss"));
                                }
                                break;
                            default:
                                {
                                    if (totalAplicados == 0)
                                        planilha.Cells["O" + contador].Value = String.Format("NÃO APLICADO");
                                    else if ((totalGeral - totalAplicados) == 0 && listaUtilizados.Count > 0)
                                        planilha.Cells["O" + contador].Value = String.Format("APLICADO | {0}", listaUtilizados[0].DataRetirada.ToString("dd/MM/yyyy HH:ss"));
                                    else planilha.Cells["O" + contador].Value = String.Format("UTILIZADOS {0} DE {1}", totalAplicados, totalGeral);
                                }
                                break;
                        }

                        valorFinalAplicados += valorAplicados;
                        valorFinalNaoAplicados += valorNaoAplicados;
                        valorFinalGeral += valorGeral;

                        totalFinalAplicados += totalAplicados;
                        totalFinalNaoAplicados += totalNaoAplicados;
                        totalFinalGeral += totalGeral;

                        #endregion

                        planilha.Cells["P" + contador].Value = totalAplicados.ToString("N0");
                        planilha.Cells["Q" + contador].Value = valorAplicados.ToString("N2");
                        planilha.Cells["R" + contador].Value = totalNaoAplicados.ToString("N0");
                        planilha.Cells["S" + contador].Value = valorNaoAplicados.ToString("N2");
                        planilha.Cells["T" + contador].Value = totalGeral.ToString("N0");
                        planilha.Cells["U" + contador].Value = valorGeral.ToString("N2");
                        planilha.Cells["V" + contador].Value = item.Parcelas.ToString("N0");

                        planilha.Cells["G" + contador].Value = item.DataCompra.ToString("dd/MM/yyyy HH:mm");

                        planilha.Cells["A" + contador].AutoFitColumns(18);
                        planilha.Cells["B" + contador].AutoFitColumns(50);
                        planilha.Cells["C" + contador].AutoFitColumns(18);
                        planilha.Cells["D" + contador].AutoFitColumns(25);
                        planilha.Cells["E" + contador].AutoFitColumns(25);
                        planilha.Cells["F" + contador].AutoFitColumns(25);
                        planilha.Cells["G" + contador].AutoFitColumns(25);
                        planilha.Cells["H" + contador].AutoFitColumns(35);
                        planilha.Cells["I" + contador].AutoFitColumns(50);
                        planilha.Cells["J" + contador].AutoFitColumns(50);
                        planilha.Cells["K" + contador].AutoFitColumns(25);
                        planilha.Cells["L" + contador].AutoFitColumns(50);
                        planilha.Cells["M" + contador].AutoFitColumns(50);
                        planilha.Cells["N" + contador].AutoFitColumns(10);
                        planilha.Cells["O" + contador].AutoFitColumns(25);
                        planilha.Cells["P" + contador].AutoFitColumns(20);
                        planilha.Cells["Q" + contador].AutoFitColumns(20);
                        planilha.Cells["R" + contador].AutoFitColumns(20);
                        planilha.Cells["S" + contador].AutoFitColumns(20);
                        planilha.Cells["T" + contador].AutoFitColumns(20);
                        planilha.Cells["U" + contador].AutoFitColumns(20);
                        planilha.Cells["V" + contador].AutoFitColumns(20);
                    }
                    catch (Exception ex)
                    {
                        Erro erro = new Erro();
                        erro.addArquivo(ex);
                    }
                }

                contador += 1;

                #region TOTAL FINAL

                planilha.Cells["P" + contador].Value = totalFinalAplicados.ToString();
                planilha.Cells["Q" + contador].Value = valorFinalAplicados.ToString("N2");
                planilha.Cells["R" + contador].Value = totalFinalNaoAplicados.ToString();
                planilha.Cells["S" + contador].Value = valorFinalNaoAplicados.ToString("N2");
                planilha.Cells["T" + contador].Value = totalFinalGeral.ToString();
                planilha.Cells["U" + contador].Value = valorFinalGeral.ToString("N2");

                planilha.Cells["P" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                planilha.Cells["Q" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                planilha.Cells["R" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                planilha.Cells["S" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                planilha.Cells["T" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                planilha.Cells["U" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

                planilha.Cells["P" + contador + ":U" + contador].AutoFitColumns(15);
                planilha.Cells["P" + contador + ":U" + contador].Style.Font.Bold = true;
                planilha.Cells["P" + contador + ":U" + contador].Style.Font.Color.SetColor(System.Drawing.Color.White);
                planilha.Cells["P" + contador + ":U" + contador].Style.Fill.PatternType = ExcelFillStyle.Solid;
                planilha.Cells["P" + contador + ":U" + contador].Style.Fill.BackgroundColor.SetColor(0, 10, 125, 25);

                #endregion

                pacote.Save();
                pacote.Dispose();
            }

            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.AddHeader("content-disposition", "attachment;  filename=" + nomeArquivo);
            HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            HttpContext.Current.Response.WriteFile(caminhoArquivo);
            HttpContext.Current.Response.Flush();

            File.Delete(caminhoArquivo);

            HttpContext.Current.Response.SuppressContent = true;
            HttpContext.Current.ApplicationInstance.CompleteRequest();
        }

        /// <summary>
        /// Rotina que gera um XLS com os dados dos itens de compras
        /// </summary>
        /// <param name="listaItens">VALUE: Lista com os itens de compras resultados da pesquisa em tela</param>
        public static void ExportarItensCompras(List<Compra> listaCompras)
        {
            string nomeArquivo = String.Format("itens-{0}.xlsx", Guid.NewGuid().ToString());
            string caminhoArquivo = HttpContext.Current.Server.MapPath("~/logs/") + nomeArquivo;
            FileInfo novoArquivo = new FileInfo(caminhoArquivo);

            using (ExcelPackage pacote = new ExcelPackage(novoArquivo))
            {
                ExcelWorksheet planilha = pacote.Workbook.Worksheets.Add("RELATORIO");
                planilha.Cells["A1"].Value = "CAMPANHA";
                planilha.Cells["B1"].Value = "EMPRESA";
                planilha.Cells["C1"].Value = "PEDIDO";
                planilha.Cells["D1"].Value = "COMPROVANTE";
                planilha.Cells["E1"].Value = "TITULAR";
                planilha.Cells["F1"].Value = "E-MAIL";
                planilha.Cells["G1"].Value = "TELEFONE";
                planilha.Cells["H1"].Value = "BENEFICIARIO";
                planilha.Cells["I1"].Value = "CLINICA";
                planilha.Cells["J1"].Value = "CIDADE";
                planilha.Cells["K1"].Value = "UF";
                planilha.Cells["L1"].Value = "QTDE";
                planilha.Cells["M1"].Value = "PRODUTO";
                planilha.Cells["N1"].Value = "PREÇO";
                planilha.Cells["O1"].Value = "DATA COMPRA";
                planilha.Cells["P1"].Value = "LIBERAÇÃO";
                planilha.Cells["Q1"].Value = "APLICAÇÃO";

                // Estilos e Customizações
                planilha.Cells["A1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                planilha.Cells["C1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                planilha.Cells["D1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                planilha.Cells["K1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                planilha.Cells["L1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                planilha.Cells["N1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                planilha.Cells["O1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                planilha.Cells["P1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                planilha.Cells["Q1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                planilha.Cells["A1:Q1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                planilha.Cells["A1:Q1"].Style.Font.Bold = true;
                planilha.Cells["A1:Q1"].Style.Font.Color.SetColor(System.Drawing.Color.White);
                planilha.Cells["A1:Q1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                planilha.Cells["A1:Q1"].Style.Fill.BackgroundColor.SetColor(0, 10, 125, 25);

                // Altura e largura das linhas e colunas
                planilha.Row(1).Height = 20;

                int contador = 1;
                planilha.Cells["A" + contador].AutoFitColumns(18);
                planilha.Cells["B" + contador].AutoFitColumns(50);
                planilha.Cells["C" + contador].AutoFitColumns(15);
                planilha.Cells["D" + contador].AutoFitColumns(25);
                planilha.Cells["E" + contador].AutoFitColumns(50);
                planilha.Cells["F" + contador].AutoFitColumns(50);
                planilha.Cells["G" + contador].AutoFitColumns(25);
                planilha.Cells["H" + contador].AutoFitColumns(50);
                planilha.Cells["I" + contador].AutoFitColumns(50);
                planilha.Cells["J" + contador].AutoFitColumns(50);
                planilha.Cells["K" + contador].AutoFitColumns(10);
                planilha.Cells["L" + contador].AutoFitColumns(10);
                planilha.Cells["M" + contador].AutoFitColumns(50);
                planilha.Cells["N" + contador].AutoFitColumns(15);
                planilha.Cells["O" + contador].AutoFitColumns(20);
                planilha.Cells["P" + contador].AutoFitColumns(20);
                planilha.Cells["Q" + contador].AutoFitColumns(20);

                foreach (ImunneVacinas.Compra compra in listaCompras)
                {
                    try
                    {
                        //ImunneVacinas.Compra compraSelecionada = ImunneVacinas.Compra.ConsultarUnico(compra.Codigo);
                        //ImunneVacinas.Campanha campanhaSelecionada = ImunneVacinas.Campanha.ConsultarUnico(compraSelecionada.IdCampanha.Codigo);
                        //ImunneVacinas.TransacaoRealizada transacaoSelecionada = ImunneVacinas.TransacaoRealizada.ConsultarUnicoCompra(compra.codigo);
                        //imunnevacinas.clinica clinicaselecionada = imunnevacinas.clinica.consultarunico(compraselecionada.idclinica.codigo);
                        //ImunneVacinas.Participante participanteSelecionado = ImunneVacinas.Participante.ConsultarUnico(compraSelecionada.IdParticipante.Codigo);


                        ImunneVacinas.PesquisaItemCompra pesquisaItens = new PesquisaItemCompra()
                        {
                            Compra = compra.Codigo,
                            Ordenacao = Utils.TipoOrdenacao.porData
                        };

                        // Listando os itens da compra, para coleta de informações de produtos e valor dos mesmos
                        List<ImunneVacinas.ItemCompra> listaItens = ImunneVacinas.ItemCompra.ConsultarItens(pesquisaItens);
                        foreach (ImunneVacinas.ItemCompra item in listaItens)
                        {
                            ImunneVacinas.ProdutoCampanha produtoCampanha = ImunneVacinas.ProdutoCampanha.ConsultarUnico(compra.IdCampanha.Codigo, item.IdVacina.Codigo);

                            // Listando os beneficiários da compra para impressão, linha a linha, dos valores de cada um
                            ImunneVacinas.PesquisaVoucher pesquisaVales = new PesquisaVoucher()
                            {
                                Compra = compra.Codigo,
                                Clinica = compra.IdClinica.Codigo,
                                Ordenacao = Utils.TipoOrdenacao.porNome
                            };

                            List<ImunneVacinas.Voucher> listaVales = ImunneVacinas.Voucher.ConsultarVouchers(pesquisaVales);
                            foreach (ImunneVacinas.Voucher vale in listaVales)
                            {
                                contador += 1;
                                planilha.Cells["A" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                planilha.Cells["C" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                planilha.Cells["D" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                planilha.Cells["K" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                planilha.Cells["L" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                planilha.Cells["N" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                planilha.Cells["O" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                planilha.Cells["P" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                planilha.Cells["Q" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                planilha.Cells["A" + contador + ":Q" + contador].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                planilha.Cells["A" + contador + ":Q" + contador].AutoFitColumns();
                                planilha.Row(contador).Height = 20;

                                planilha.Cells["A" + contador].Value = compra.IdCampanha.Identificacao.ToUpper();
                                planilha.Cells["B" + contador].Value = compra.IdCampanha.IdEmpresa.NomeFantasia.ToUpper();
                                planilha.Cells["C" + contador].Value = compra.Codigo.ToString().PadLeft(5, '0');

                                if (!String.IsNullOrEmpty(compra.NumeroComprovante))
                                    planilha.Cells["D" + contador].Value = compra.NumeroComprovante.PadLeft(18, '0');
                                else
                                    planilha.Cells["D" + contador].Value = "--";

                                planilha.Cells["E" + contador].Value = compra.IdParticipante.Nome.ToUpper();
                                planilha.Cells["H" + contador].Value = vale.IdParticipante.Nome.ToUpper();

                                #region Participante

                                if (!String.IsNullOrEmpty(compra.IdParticipante.Email))
                                    planilha.Cells["F" + contador].Value = compra.IdParticipante.Email.ToLower();
                                else
                                    planilha.Cells["F" + contador].Value = "";

                                if (!String.IsNullOrEmpty(compra.IdParticipante.Telefone))
                                    planilha.Cells["G" + contador].Value = compra.IdParticipante.Telefone;
                                else
                                    planilha.Cells["G" + contador].Value = "";

                                #endregion

                                planilha.Cells["O" + contador].Value = compra.DataCompra.ToString("dd/MM/yyyy HH:mm");
                                planilha.Cells["P" + contador].Value = compra.IdCampanha.DataAbertura.ToString("dd/MM/yyyy");

                                if (!String.IsNullOrEmpty(compra.IdClinica.NomeFantasia))
                                    planilha.Cells["I" + contador].Value = compra.IdClinica.NomeFantasia.ToUpper();
                                else planilha.Cells["I" + contador].Value = "--";

                                if (!String.IsNullOrEmpty(compra.IdClinica.NomeFantasia))
                                    planilha.Cells["J" + contador].Value = compra.IdClinica.IdCidade.Nome.ToUpper();
                                else planilha.Cells["J" + contador].Value = "--";

                                if (!String.IsNullOrEmpty(compra.IdClinica.NomeFantasia))
                                    planilha.Cells["K" + contador].Value = compra.IdClinica.IdUf.Sigla.ToUpper();
                                else planilha.Cells["K" + contador].Value = "--";

                                planilha.Cells["M" + contador].Value = produtoCampanha.IdVacina.Nome.ToUpper();
                                planilha.Cells["N" + contador].Value = produtoCampanha.Preco.ToString("N2");
                                planilha.Cells["L" + contador].Value = 1;

                                // Aplicação
                                string pedidoAplicacao = "NÃO APLICADA";
                                if (vale.DataRetirada != ImunneVacinas.Utils.dataPadrao)
                                    pedidoAplicacao = vale.DataRetirada.ToString("dd/MM/yyyy HH:mm");

                                planilha.Cells["Q" + contador].Value = pedidoAplicacao;

                                planilha.Cells["A" + contador].AutoFitColumns(18);
                                planilha.Cells["B" + contador].AutoFitColumns(50);
                                planilha.Cells["C" + contador].AutoFitColumns(15);
                                planilha.Cells["D" + contador].AutoFitColumns(25);
                                planilha.Cells["E" + contador].AutoFitColumns(50);
                                planilha.Cells["F" + contador].AutoFitColumns(50);
                                planilha.Cells["G" + contador].AutoFitColumns(25);
                                planilha.Cells["H" + contador].AutoFitColumns(50);
                                planilha.Cells["I" + contador].AutoFitColumns(50);
                                planilha.Cells["J" + contador].AutoFitColumns(50);
                                planilha.Cells["K" + contador].AutoFitColumns(10);
                                planilha.Cells["L" + contador].AutoFitColumns(10);
                                planilha.Cells["M" + contador].AutoFitColumns(50);
                                planilha.Cells["N" + contador].AutoFitColumns(15);
                                planilha.Cells["O" + contador].AutoFitColumns(20);
                                planilha.Cells["P" + contador].AutoFitColumns(20);
                                planilha.Cells["Q" + contador].AutoFitColumns(20);
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        Erro erro = new Erro();
                        erro.addArquivo(ex);
                    }
                }

                pacote.Save();
                pacote.Dispose();
            }

            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.AddHeader("content-disposition", "attachment;  filename=" + nomeArquivo);
            HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            HttpContext.Current.Response.WriteFile(caminhoArquivo);
            HttpContext.Current.Response.Flush();

            HttpContext.Current.Response.SuppressContent = true;
            HttpContext.Current.ApplicationInstance.CompleteRequest();

            if (File.Exists(caminhoArquivo))
                File.Delete(caminhoArquivo);
        }

        /// <summary>
        /// Rotina que gera um XLS com os dados dos itens de compras
        /// </summary>
        /// <param name="listaItens">VALUE: Lista com os itens de compras resultados da pesquisa em tela</param>
        public static void NovoExportarItensCompras(List<ListagemItemCompra> listaImpressao)
        {
            string nomeArquivo = String.Format("itens-{0}.xlsx", Guid.NewGuid().ToString());
            string caminhoArquivo = HttpContext.Current.Server.MapPath("~/logs/") + nomeArquivo;
            FileInfo novoArquivo = new FileInfo(caminhoArquivo);

            using (ExcelPackage pacote = new ExcelPackage(novoArquivo))
            {
                ExcelWorksheet planilha = pacote.Workbook.Worksheets.Add("RELATORIO");
                planilha.Cells["A1"].Value = "CAMPANHA";
                planilha.Cells["B1"].Value = "EMPRESA";
                planilha.Cells["C1"].Value = "PEDIDO";
                planilha.Cells["D1"].Value = "COMPROVANTE";
                planilha.Cells["E1"].Value = "TITULAR";
                planilha.Cells["F1"].Value = "E-MAIL";
                planilha.Cells["G1"].Value = "TELEFONE";
                planilha.Cells["H1"].Value = "BENEFICIARIO";
                planilha.Cells["I1"].Value = "CLINICA";
                planilha.Cells["J1"].Value = "CIDADE";
                planilha.Cells["K1"].Value = "UF";
                planilha.Cells["L1"].Value = "QTDE";
                planilha.Cells["M1"].Value = "PRODUTO";
                planilha.Cells["N1"].Value = "PREÇO";
                planilha.Cells["O1"].Value = "DATA COMPRA";
                planilha.Cells["P1"].Value = "LIBERAÇÃO";
                planilha.Cells["Q1"].Value = "APLICAÇÃO";

                // Estilos e Customizações
                planilha.Cells["A1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                planilha.Cells["C1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                planilha.Cells["D1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                planilha.Cells["K1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                planilha.Cells["L1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                planilha.Cells["N1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                planilha.Cells["O1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                planilha.Cells["P1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                planilha.Cells["Q1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                planilha.Cells["A1:Q1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                planilha.Cells["A1:Q1"].Style.Font.Bold = true;
                planilha.Cells["A1:Q1"].Style.Font.Color.SetColor(System.Drawing.Color.White);
                planilha.Cells["A1:Q1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                planilha.Cells["A1:Q1"].Style.Fill.BackgroundColor.SetColor(0, 10, 125, 25);

                // Altura e largura das linhas e colunas
                planilha.Row(1).Height = 20;

                int contador = 1;
                planilha.Cells["A" + contador].AutoFitColumns(18);
                planilha.Cells["B" + contador].AutoFitColumns(50);
                planilha.Cells["C" + contador].AutoFitColumns(15);
                planilha.Cells["D" + contador].AutoFitColumns(25);
                planilha.Cells["E" + contador].AutoFitColumns(50);
                planilha.Cells["F" + contador].AutoFitColumns(50);
                planilha.Cells["G" + contador].AutoFitColumns(25);
                planilha.Cells["H" + contador].AutoFitColumns(50);
                planilha.Cells["I" + contador].AutoFitColumns(50);
                planilha.Cells["J" + contador].AutoFitColumns(50);
                planilha.Cells["K" + contador].AutoFitColumns(10);
                planilha.Cells["L" + contador].AutoFitColumns(10);
                planilha.Cells["M" + contador].AutoFitColumns(50);
                planilha.Cells["N" + contador].AutoFitColumns(15);
                planilha.Cells["O" + contador].AutoFitColumns(20);
                planilha.Cells["P" + contador].AutoFitColumns(20);
                planilha.Cells["Q" + contador].AutoFitColumns(20);

                foreach (ImunneVacinas.ListagemItemCompra item in listaImpressao)
                {
                    try
                    {
                        contador += 1;
                        planilha.Cells["A" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        planilha.Cells["C" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        planilha.Cells["D" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        planilha.Cells["K" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        planilha.Cells["L" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        planilha.Cells["N" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        planilha.Cells["O" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        planilha.Cells["P" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        planilha.Cells["Q" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        planilha.Cells["A" + contador + ":Q" + contador].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        planilha.Cells["A" + contador + ":Q" + contador].AutoFitColumns();
                        planilha.Row(contador).Height = 20;

                        planilha.Cells["A" + contador].Value = item.Campanha.ToUpper();
                        planilha.Cells["B" + contador].Value = item.Empresa.ToUpper();
                        planilha.Cells["C" + contador].Value = item.Pedido.ToString().PadLeft(5, '0');

                        if (!String.IsNullOrEmpty(item.Comprovante))
                            planilha.Cells["D" + contador].Value = item.Comprovante.PadLeft(18, '0');
                        else
                            planilha.Cells["D" + contador].Value = "--";

                        planilha.Cells["E" + contador].Value = item.Titular.ToUpper();
                        planilha.Cells["H" + contador].Value = item.Beneficiario.ToUpper();

                        #region Participante

                        if (!String.IsNullOrEmpty(item.Email))
                            planilha.Cells["F" + contador].Value = item.Email.ToLower();
                        else
                            planilha.Cells["F" + contador].Value = "";

                        if (!String.IsNullOrEmpty(item.Telefone))
                            planilha.Cells["G" + contador].Value = item.Telefone;
                        else
                            planilha.Cells["G" + contador].Value = "";

                        #endregion

                        planilha.Cells["O" + contador].Value = item.DataCompra;
                        planilha.Cells["P" + contador].Value = item.DataLiberacao;

                        if (!String.IsNullOrEmpty(item.Clinica))
                            planilha.Cells["I" + contador].Value = item.Clinica.ToUpper();
                        else planilha.Cells["I" + contador].Value = "--";

                        if (!String.IsNullOrEmpty(item.Cidade))
                            planilha.Cells["J" + contador].Value = item.Cidade.ToUpper();
                        else planilha.Cells["J" + contador].Value = "--";

                        if (!String.IsNullOrEmpty(item.UF))
                            planilha.Cells["K" + contador].Value = item.UF.ToUpper();
                        else planilha.Cells["K" + contador].Value = "--";

                        planilha.Cells["M" + contador].Value = item.Produto.ToUpper();
                        planilha.Cells["N" + contador].Value = item.Preco.ToString("N2");
                        planilha.Cells["L" + contador].Value = 1;

                        planilha.Cells["Q" + contador].Value = item.DataAplicacao;

                        planilha.Cells["A" + contador].AutoFitColumns(18);
                        planilha.Cells["B" + contador].AutoFitColumns(50);
                        planilha.Cells["C" + contador].AutoFitColumns(15);
                        planilha.Cells["D" + contador].AutoFitColumns(25);
                        planilha.Cells["E" + contador].AutoFitColumns(50);
                        planilha.Cells["F" + contador].AutoFitColumns(50);
                        planilha.Cells["G" + contador].AutoFitColumns(25);
                        planilha.Cells["H" + contador].AutoFitColumns(50);
                        planilha.Cells["I" + contador].AutoFitColumns(50);
                        planilha.Cells["J" + contador].AutoFitColumns(50);
                        planilha.Cells["K" + contador].AutoFitColumns(10);
                        planilha.Cells["L" + contador].AutoFitColumns(10);
                        planilha.Cells["M" + contador].AutoFitColumns(50);
                        planilha.Cells["N" + contador].AutoFitColumns(15);
                        planilha.Cells["O" + contador].AutoFitColumns(20);
                        planilha.Cells["P" + contador].AutoFitColumns(20);
                        planilha.Cells["Q" + contador].AutoFitColumns(20);
                    }
                    catch (Exception ex)
                    {
                        Erro erro = new Erro();
                        erro.addArquivo(ex);
                    }
                }

                pacote.Save();
                pacote.Dispose();
            }

            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.AddHeader("content-disposition", "attachment;  filename=" + nomeArquivo);
            HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            HttpContext.Current.Response.WriteFile(caminhoArquivo);
            HttpContext.Current.Response.Flush();

            HttpContext.Current.Response.SuppressContent = true;
            HttpContext.Current.ApplicationInstance.CompleteRequest();

            if (File.Exists(caminhoArquivo))
                File.Delete(caminhoArquivo);
        }

        /// <summary>
        /// Rotina que gera um XLS com os dados de compras
        /// </summary>
        /// <param name="listaItens">VALUE: Lista com compras resultadas da pesquisa em tela</param>
        public static void NovoExportarCompras(List<ListagemCompra> listaCompras)
        {
            string nomeArquivo = String.Format("compras-{0}.xlsx", Guid.NewGuid().ToString());
            string caminhoArquivo = HttpContext.Current.Server.MapPath("~/logs/") + nomeArquivo;
            FileInfo novoArquivo = new FileInfo(caminhoArquivo);

            using (ExcelPackage pacote = new ExcelPackage(novoArquivo))
            {
                ExcelWorksheet planilha = pacote.Workbook.Worksheets.Add("RELATORIO");

                #region NOMES & COLUNAS

                planilha.Cells["A1"].Value = "CAMPANHA";
                planilha.Cells["B1"].Value = "EMPRESA";
                planilha.Cells["C1"].Value = "PEDIDO";
                planilha.Cells["D1"].Value = "COMPROVANTE";
                planilha.Cells["E1"].Value = "SITUAÇÃO";
                planilha.Cells["F1"].Value = "CATEGORIA";
                planilha.Cells["G1"].Value = "DATA COMPRA";
                planilha.Cells["H1"].Value = "CPF/CNPJ";
                planilha.Cells["I1"].Value = "TITULAR";
                planilha.Cells["J1"].Value = "E-MAIL";
                planilha.Cells["K1"].Value = "TELEFONE";
                planilha.Cells["L1"].Value = "CLINICA";
                planilha.Cells["M1"].Value = "CIDADE";
                planilha.Cells["N1"].Value = "UF";
                planilha.Cells["O1"].Value = "APLICAÇÃO";

                // TOTAIS E VALORES
                planilha.Cells["P1"].Value = "APLICADOS";
                planilha.Cells["Q1"].Value = "VALOR";
                planilha.Cells["R1"].Value = "Ñ APLICADOS";
                planilha.Cells["S1"].Value = "VALOR";
                planilha.Cells["T1"].Value = "TOTAL";
                planilha.Cells["U1"].Value = "VALOR";
                planilha.Cells["V1"].Value = "PARCELAS";

                #endregion

                #region ESTILOS E CUSTOMIZAÇÕES

                planilha.Cells["A1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                planilha.Cells["B1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                planilha.Cells["C1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                planilha.Cells["D1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                planilha.Cells["E1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                planilha.Cells["F1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                planilha.Cells["G1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                planilha.Cells["H1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                planilha.Cells["I1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                planilha.Cells["J1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                planilha.Cells["K1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                planilha.Cells["L1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                planilha.Cells["M1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                planilha.Cells["N1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                planilha.Cells["O1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                planilha.Cells["P1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                planilha.Cells["Q1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                planilha.Cells["R1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                planilha.Cells["S1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                planilha.Cells["T1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                planilha.Cells["U1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                planilha.Cells["V1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                planilha.Cells["A1:V1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                planilha.Cells["A1:V1"].Style.Font.Bold = true;
                planilha.Cells["A1:V1"].Style.Font.Color.SetColor(System.Drawing.Color.White);
                planilha.Cells["A1:V1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                planilha.Cells["A1:V1"].Style.Fill.BackgroundColor.SetColor(0, 10, 125, 25);
                planilha.Cells["A1:V1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                planilha.Row(1).Height = 20;

                #endregion

                int contador = 1;
                int totalFinalAplicados = 0;
                int totalFinalNaoAplicados = 0;
                int totalFinalGeral = 0;

                decimal valorFinalAplicados = 0;
                decimal valorFinalNaoAplicados = 0;
                decimal valorFinalGeral = 0;

                foreach (ListagemCompra item in listaCompras)
                {
                    try
                    {
                        contador += 1;

                        planilha.Cells["A" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        planilha.Cells["B" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        planilha.Cells["C" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        planilha.Cells["D" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        planilha.Cells["E" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        planilha.Cells["F" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        planilha.Cells["G" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        planilha.Cells["H" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        planilha.Cells["I" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        planilha.Cells["J" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        planilha.Cells["K" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        planilha.Cells["L" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        planilha.Cells["M" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        planilha.Cells["N" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        planilha.Cells["O" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        planilha.Cells["P" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        planilha.Cells["Q" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        planilha.Cells["R" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        planilha.Cells["S" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        planilha.Cells["T" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        planilha.Cells["U" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        planilha.Cells["V" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        planilha.Cells["A" + contador + ":V" + contador].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        planilha.Row(contador).Height = 20;

                        planilha.Cells["A" + contador].Value = item.Campanha.ToUpper();
                        planilha.Cells["B" + contador].Value = item.Empresa.ToUpper();
                        planilha.Cells["C" + contador].Value = item.Pedido.ToString().PadLeft(5, '0');

                        #region TRANSAÇÕES

                        if (!String.IsNullOrEmpty(item.Comprovante))
                            planilha.Cells["D" + contador].Value = item.Comprovante;
                        else planilha.Cells["D" + contador].Value = "--";

                        #endregion

                        planilha.Cells["E" + contador].Value = item.Situacao;
                        planilha.Cells["F" + contador].Value = item.Categoria;

                        #region DADOS TITULAR

                        planilha.Cells["H" + contador].Value = ImunneVacinas.Utils.FormatarCpfCnpj(item.CpfCnpj);
                        planilha.Cells["I" + contador].Value = item.Titular.ToUpper();

                        if (!String.IsNullOrEmpty(item.Email))
                            planilha.Cells["J" + contador].Value = item.Email.ToLower();
                        else
                            planilha.Cells["J" + contador].Value = "";

                        if (!String.IsNullOrEmpty(item.Telefone))
                            planilha.Cells["K" + contador].Value = item.Telefone;
                        else
                            planilha.Cells["L" + contador].Value = "";

                        #endregion

                        #region CLÍNICAS

                        if (!String.IsNullOrEmpty(item.Clinica))
                            planilha.Cells["L" + contador].Value = item.Clinica.ToUpper();
                        else planilha.Cells["L" + contador].Value = "--";

                        if (!String.IsNullOrEmpty(item.Cidade))
                            planilha.Cells["M" + contador].Value = item.Cidade.ToUpper();
                        else planilha.Cells["M" + contador].Value = "--";

                        if (!String.IsNullOrEmpty(item.UF))
                            planilha.Cells["N" + contador].Value = item.UF.ToUpper();
                        else planilha.Cells["N" + contador].Value = "--";

                        #endregion

                        #region TOTALIZAÇÕES

                        int totalAplicados = 0;
                        int totalGeral = 0;
                        int totalNaoAplicados = 0;

                        decimal valorAplicados = 0;
                        decimal valorGeral = 0;
                        decimal valorNaoAplicados = 0;
                        decimal valorProdutos = item.ValorItens;

                        totalAplicados = item.VouchersUtilizados;
                        totalNaoAplicados = item.VouchersDemais;
                        totalGeral = totalAplicados + totalNaoAplicados;

                        valorAplicados = (valorProdutos * totalAplicados);
                        valorNaoAplicados = (valorProdutos * totalNaoAplicados);
                        valorGeral = valorAplicados + valorNaoAplicados;

                        planilha.Cells["O" + contador].Value = item.DataAplicacao;

                        valorFinalAplicados += valorAplicados;
                        valorFinalNaoAplicados += valorNaoAplicados;
                        valorFinalGeral += valorGeral;

                        totalFinalAplicados += totalAplicados;
                        totalFinalNaoAplicados += totalNaoAplicados;
                        totalFinalGeral += totalGeral;

                        #endregion

                        planilha.Cells["P" + contador].Value = totalAplicados.ToString("N0");
                        planilha.Cells["Q" + contador].Value = String.Format("R$ {0}", valorAplicados.ToString("N2"));
                        planilha.Cells["R" + contador].Value = totalNaoAplicados.ToString("N0");
                        planilha.Cells["S" + contador].Value = String.Format("R$ {0}", valorNaoAplicados.ToString("N2"));
                        planilha.Cells["T" + contador].Value = totalGeral.ToString("N0");
                        planilha.Cells["U" + contador].Value = String.Format("R$ {0}", valorGeral.ToString("N2"));
                        planilha.Cells["V" + contador].Value = item.Parcelas.ToString("N0");

                        planilha.Cells["G" + contador].Value = item.DataCompra;

                        planilha.Cells["A" + contador].AutoFitColumns(18);
                        planilha.Cells["B" + contador].AutoFitColumns(50);
                        planilha.Cells["C" + contador].AutoFitColumns(18);
                        planilha.Cells["D" + contador].AutoFitColumns(25);
                        planilha.Cells["E" + contador].AutoFitColumns(25);
                        planilha.Cells["F" + contador].AutoFitColumns(25);
                        planilha.Cells["G" + contador].AutoFitColumns(25);
                        planilha.Cells["H" + contador].AutoFitColumns(35);
                        planilha.Cells["I" + contador].AutoFitColumns(50);
                        planilha.Cells["J" + contador].AutoFitColumns(50);
                        planilha.Cells["K" + contador].AutoFitColumns(25);
                        planilha.Cells["L" + contador].AutoFitColumns(50);
                        planilha.Cells["M" + contador].AutoFitColumns(50);
                        planilha.Cells["N" + contador].AutoFitColumns(10);
                        planilha.Cells["O" + contador].AutoFitColumns(25);
                        planilha.Cells["P" + contador].AutoFitColumns(20);
                        planilha.Cells["Q" + contador].AutoFitColumns(20);
                        planilha.Cells["R" + contador].AutoFitColumns(20);
                        planilha.Cells["S" + contador].AutoFitColumns(20);
                        planilha.Cells["T" + contador].AutoFitColumns(20);
                        planilha.Cells["U" + contador].AutoFitColumns(20);
                        planilha.Cells["V" + contador].AutoFitColumns(20);
                    }
                    catch (Exception ex)
                    {
                        Erro erro = new Erro();
                        erro.addArquivo(ex);
                    }
                }

                contador += 1;

                #region TOTAL FINAL

                planilha.Cells["P" + contador].Value = totalFinalAplicados.ToString();
                planilha.Cells["Q" + contador].Value = String.Format("R$ {0}", valorFinalAplicados.ToString("N2"));
                planilha.Cells["R" + contador].Value = totalFinalNaoAplicados.ToString();
                planilha.Cells["S" + contador].Value = String.Format("R$ {0}", valorFinalNaoAplicados.ToString("N2"));
                planilha.Cells["T" + contador].Value = totalFinalGeral.ToString();
                planilha.Cells["U" + contador].Value = String.Format("R$ {0}", valorFinalGeral.ToString("N2"));

                planilha.Cells["P" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                planilha.Cells["Q" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                planilha.Cells["R" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                planilha.Cells["S" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                planilha.Cells["T" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                planilha.Cells["U" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                planilha.Cells["V" + contador].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

                planilha.Cells["P" + contador + ":U" + contador].AutoFitColumns(15);
                planilha.Cells["P" + contador + ":U" + contador].Style.Font.Bold = true;
                planilha.Cells["P" + contador + ":U" + contador].Style.Font.Color.SetColor(System.Drawing.Color.White);
                planilha.Cells["P" + contador + ":U" + contador].Style.Fill.PatternType = ExcelFillStyle.Solid;
                planilha.Cells["P" + contador + ":U" + contador].Style.Fill.BackgroundColor.SetColor(0, 10, 125, 25);

                #endregion

                pacote.Save();
                pacote.Dispose();
            }

            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.AddHeader("content-disposition", "attachment;  filename=" + nomeArquivo);
            HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            HttpContext.Current.Response.WriteFile(caminhoArquivo);
            HttpContext.Current.Response.Flush();

            File.Delete(caminhoArquivo);

            HttpContext.Current.Response.SuppressContent = true;
            HttpContext.Current.ApplicationInstance.CompleteRequest();
        }

        /// <summary>
        /// Rotina de movimentações da clínica
        /// </summary>
        /// <param name="codigoClinica">VALUE: código da clínica</param>
        /// <param name="listaMovimentos">VALUE: lista de eventos</param>
        public static void ExportarMovimentos(int codigoClinica, List<MovimentacaoEstoque> listaMovimentos)
        {
            int linha = 1;
            string nomeArquivo = String.Format("movimentos-{0}.xlsx", Guid.NewGuid().ToString());
            string caminhoArquivo = HttpContext.Current.Server.MapPath("~/logs/") + nomeArquivo;
            FileInfo novoArquivo = new FileInfo(caminhoArquivo);

            ImunneVacinas.Clinica clinicaSelecionada = Clinica.ConsultarUnico(codigoClinica);

            using (ExcelPackage pacote = new ExcelPackage(novoArquivo))
            {
                ExcelWorksheet planilha = pacote.Workbook.Worksheets.Add("MOVIMENTACOES");

                #region TITULO

                planilha.Cells["A" + linha + ":F" + linha].Merge = true;
                planilha.Cells["A" + linha].Value = String.Format("CLÍNICA {0}", clinicaSelecionada.NomeFantasia.ToUpper());

                planilha.Cells["A" + linha].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                planilha.Cells["A" + linha].Style.Font.Bold = true;
                planilha.Cells["A" + linha].Style.Font.Color.SetColor(System.Drawing.Color.White);
                planilha.Cells["A" + linha].Style.Fill.PatternType = ExcelFillStyle.Solid;
                planilha.Cells["A" + linha].Style.Fill.BackgroundColor.SetColor(0, 10, 125, 25);

                #endregion

                linha += 1;

                planilha.Cells["A" + linha].Value = "TIPO";
                planilha.Cells["B" + linha].Value = "DATA";
                planilha.Cells["C" + linha].Value = "QTDE";
                planilha.Cells["D" + linha].Value = "PRODUTO";
                planilha.Cells["E" + linha].Value = "USUÁRIO";
                planilha.Cells["F" + linha].Value = "HISTÓRICO";

                planilha.Cells["A" + linha].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                planilha.Cells["B" + linha].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                planilha.Cells["C" + linha].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                planilha.Cells["A" + linha + ":F" + linha].Style.Font.Bold = true;
                planilha.Cells["A" + linha + ":F" + linha].Style.Font.Color.SetColor(System.Drawing.Color.White);
                planilha.Cells["A" + linha + ":F" + linha].Style.Fill.PatternType = ExcelFillStyle.Solid;
                planilha.Cells["A" + linha + ":F" + linha].Style.Fill.BackgroundColor.SetColor(0, 10, 125, 25);

                linha += 1;

                decimal totalEntradas = 0;
                decimal totalSaidas = 0;
                decimal saldoTotal = 0;

                foreach (MovimentacaoEstoque item in listaMovimentos)
                {
                    ImunneVacinas.Usuario usuarioSelecionado = ImunneVacinas.Usuario.ConsultarUnico(item.Usuario.Codigo);

                    planilha.Cells["A" + linha].Value = item.Tipo.ToString().ToUpper();
                    planilha.Cells["B" + linha].Value = item.DataCriacao.ToString("dd/MM/yyyy HH:mm");

                    if (item.Tipo == Utils.TipoMovimentacao.Saida)
                    {
                        planilha.Cells["C" + linha].Value = String.Format("- {0}", item.Quantidade);
                        planilha.Cells["C" + linha].Style.Font.Color.SetColor(System.Drawing.Color.Red);
                        totalSaidas += item.Quantidade;
                    }
                    else
                    {
                        planilha.Cells["C" + linha].Value = String.Format("{0}", item.Quantidade);
                        planilha.Cells["C" + linha].Style.Font.Color.SetColor(System.Drawing.Color.Blue);
                        totalEntradas += item.Quantidade;
                    }

                    planilha.Cells["A" + linha].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    planilha.Cells["B" + linha].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    planilha.Cells["C" + linha].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                    if (item.Produto.Codigo > 0)
                    {
                        ImunneVacinas.Vacina produtoEstoque = ImunneVacinas.Vacina.ConsultarUnico(item.Produto.Codigo);
                        if (produtoEstoque != null)
                            planilha.Cells["D" + linha].Value = produtoEstoque.Nome.ToUpper();
                    }
                    else planilha.Cells["D" + linha].Value = "--";

                    planilha.Cells["E" + linha].Value = usuarioSelecionado.Nome.ToUpper();
                    planilha.Cells["F" + linha].Value = item.Historico.ToUpper();


                    planilha.Cells["A" + linha].AutoFitColumns();
                    planilha.Cells["B" + linha].AutoFitColumns();
                    planilha.Cells["C" + linha].AutoFitColumns();
                    planilha.Cells["D" + linha].AutoFitColumns();
                    planilha.Cells["E" + linha].AutoFitColumns();
                    planilha.Cells["F" + linha].AutoFitColumns();

                    linha += 1;
                }

                #region ENTRADAS

                planilha.Cells["A" + linha + ":B" + linha].Merge = true;
                planilha.Cells["C" + linha + ":F" + linha].Merge = true;

                planilha.Cells["A" + linha].Value = "TOTAL DE ENTRADAS";
                planilha.Cells["A" + linha].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                planilha.Cells["A" + linha].Style.Font.Bold = true;
                planilha.Cells["A" + linha].Style.Font.Color.SetColor(System.Drawing.Color.White);
                planilha.Cells["A" + linha].Style.Fill.PatternType = ExcelFillStyle.Solid;
                planilha.Cells["A" + linha].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Blue);

                planilha.Cells["C" + linha].Value = totalEntradas.ToString();
                planilha.Cells["C" + linha].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                planilha.Cells["C" + linha].Style.Font.Bold = true;
                planilha.Cells["C" + linha].Style.Font.Color.SetColor(System.Drawing.Color.White);
                planilha.Cells["C" + linha].Style.Fill.PatternType = ExcelFillStyle.Solid;
                planilha.Cells["C" + linha].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Blue);

                linha += 1;

                #endregion

                #region SAÍDAS

                planilha.Cells["A" + linha + ":B" + linha].Merge = true;
                planilha.Cells["C" + linha + ":F" + linha].Merge = true;

                planilha.Cells["A" + linha].Value = "TOTAL DE SAÍDAS";
                planilha.Cells["A" + linha].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                planilha.Cells["A" + linha].Style.Font.Bold = true;
                planilha.Cells["A" + linha].Style.Font.Color.SetColor(System.Drawing.Color.White);
                planilha.Cells["A" + linha].Style.Fill.PatternType = ExcelFillStyle.Solid;
                planilha.Cells["A" + linha].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Red);

                planilha.Cells["C" + linha].Value = (totalSaidas * -1).ToString();
                planilha.Cells["C" + linha].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                planilha.Cells["C" + linha].Style.Font.Bold = true;
                planilha.Cells["C" + linha].Style.Font.Color.SetColor(System.Drawing.Color.White);
                planilha.Cells["C" + linha].Style.Fill.PatternType = ExcelFillStyle.Solid;
                planilha.Cells["C" + linha].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Red);

                linha += 1;

                #endregion

                #region SALDO

                planilha.Cells["A" + linha + ":B" + linha].Merge = true;
                planilha.Cells["C" + linha + ":F" + linha].Merge = true;

                saldoTotal = totalEntradas + (totalSaidas * -1);

                planilha.Cells["A" + linha].Value = "SALDO FINAL";
                planilha.Cells["A" + linha].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                planilha.Cells["A" + linha].Style.Font.Bold = true;
                planilha.Cells["A" + linha].Style.Font.Color.SetColor(System.Drawing.Color.White);
                planilha.Cells["A" + linha].Style.Fill.PatternType = ExcelFillStyle.Solid;
                planilha.Cells["A" + linha].Style.Fill.BackgroundColor.SetColor(0, 10, 125, 25);

                planilha.Cells["C" + linha].Value = saldoTotal.ToString();
                planilha.Cells["C" + linha].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                planilha.Cells["C" + linha].Style.Font.Bold = true;
                planilha.Cells["C" + linha].Style.Font.Color.SetColor(System.Drawing.Color.White);
                planilha.Cells["C" + linha].Style.Fill.PatternType = ExcelFillStyle.Solid;
                planilha.Cells["C" + linha].Style.Fill.BackgroundColor.SetColor(0, 10, 125, 25);

                #endregion

                pacote.Save();
                pacote.Dispose();
            }

            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.AddHeader("content-disposition", "attachment;  filename=" + nomeArquivo);
            HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            HttpContext.Current.Response.WriteFile(caminhoArquivo);
            HttpContext.Current.Response.Flush();

            HttpContext.Current.Response.SuppressContent = true;
            HttpContext.Current.ApplicationInstance.CompleteRequest();

            if (File.Exists(caminhoArquivo))
                File.Delete(caminhoArquivo);
        }

        /// <summary>
        /// Gera um documento EXCEL com os dados da carga processada em importação de Empresas e Inscritos
        /// </summary>
        /// <param name="listaItens"></param>
        public static void GerarExcelRetornosOperacoes(RepeaterItemCollection listaItens)
        {
            try
            {
                string nomeArquivo = String.Format("retornos-{0}.xlsx", Guid.NewGuid().ToString());
                string caminhoArquivo = HttpContext.Current.Server.MapPath("~/temp/") + nomeArquivo;
                FileInfo novoArquivo = new FileInfo(caminhoArquivo);

                using (ExcelPackage pacote = new ExcelPackage(novoArquivo))
                {
                    ExcelWorksheet planilha = pacote.Workbook.Worksheets.Add("RELATORIO");
                    planilha.Cells["A1"].Value = "LINHA";
                    planilha.Cells["B1"].Value = "RESULTADO";
                    planilha.Cells["C1"].Value = "RESUMO";
                    planilha.Cells["A1:C1"].Style.Font.Bold = true;
                    planilha.Cells["A1:C1"].Style.Font.Color.SetColor(System.Drawing.Color.White);
                    planilha.Cells["A1:C1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    planilha.Cells["A1:C1"].Style.Fill.BackgroundColor.SetColor(0, 236, 102, 8);

                    int contador = 1;
                    foreach (RepeaterItem item in listaItens)
                    {
                        contador += 1;

                        planilha.Cells["A" + contador].Value = ((Label)item.FindControl("LblLinha")).Text;
                        planilha.Cells["B" + contador].Value = ((Label)item.FindControl("LblClassificacao")).Text;
                        planilha.Cells["C" + contador].Value = ((Label)item.FindControl("lblResumo")).Text.Replace("<b>", "").Replace("</b>", "");
                    }

                    pacote.Save();
                    pacote.Dispose();
                }

                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.AddHeader("content-disposition", "attachment;  filename=" + nomeArquivo);
                HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                HttpContext.Current.Response.WriteFile(caminhoArquivo);
                HttpContext.Current.Response.Flush();

                File.Delete(caminhoArquivo);

                HttpContext.Current.Response.SuppressContent = true;
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            catch (Exception ex)
            {
                //Erro erro = new Erro();
                //erro.AdicionarLog(ex);
            }
        }
    }
}