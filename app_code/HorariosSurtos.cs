﻿using MySql.Data.MySqlClient;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace ImunneVacinas
{
    /// <summary>
    /// Descrição resumida de HorariosSurtos
    /// </summary>
    public class HorarioSurto
    {
        private static string _tabela = "HorariosSurtos";
        private static List<BD.Columns> columns;
        private static DateTime columnsUpdated;

        #region PROPRIEDADES

        private int _campanha;
        private int _compra;
        private int _codigo;
        private int _participante;
        private DateTime _infoHoraro;
        private Utils.SituacaoHorario _situacao;

        public int Campanha
        {
            get
            {
                return _campanha;
            }

            set
            {
                _campanha = value;
            }
        }

        public int Compra
        {
            get
            {
                return _compra;
            }

            set
            {
                _compra = value;
            }
        }

        public int Participante
        {
            get
            {
                return _participante;
            }

            set
            {
                _participante = value;
            }
        }

        public int Codigo
        {
            get
            {
                return _codigo;
            }

            set
            {
                _codigo = value;
            }
        }

        public DateTime InfoHoraro
        {
            get
            {
                return _infoHoraro;
            }

            set
            {
                _infoHoraro = value;
            }
        }

        public Utils.SituacaoHorario Situacao
        {
            get
            {
                return _situacao;
            }

            set
            {
                _situacao = value;
            }
        }

        #endregion

        #region GETINFOS

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos os campos da tabela.
        /// </summary>
        private static void GetInfoTabela()
        {
            if (columns == null || (DateTime.Now - columnsUpdated).Minutes > 0)
            {
                columns = BD.Columns.getColumnsInfo(_tabela);
                columnsUpdated = DateTime.Now;
            }
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo campanha.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoCampanha()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "campanha");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo compra.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoCompra()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "compra");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo participante.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoParticipante()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "participante");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo codigo.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoCodigo()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "codigo");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo infohorario.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoHorario()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "infohorario");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo situacao.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoSituacao()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "situacao");
        }

        #endregion

        /// <summary> 
        /// Rotina que cria um Array com os parâmetros utilizados nas ações de INSERT, UPDATE e DELETE
        /// <returns>Array com vários MySqlParameters</returns>
        /// </summary>
        private Array GetParametrosSQL()
        {
            MySqlParameter p;
            ArrayList ap = new ArrayList();

            p = new MySqlParameter("@campanha", Parser.parseInteiro(_campanha));
            p.MySqlDbType = GetInfoCampanha().Type;
            ap.Add(p);

            p = new MySqlParameter("@compra", Parser.parseInteiro(_compra));
            p.MySqlDbType = GetInfoCompra().Type;
            ap.Add(p);

            p = new MySqlParameter("@participante", Parser.parseInteiro(_participante));
            p.MySqlDbType = GetInfoParticipante().Type;
            ap.Add(p);

            p = new MySqlParameter("@codigo", Parser.parseInteiro(_codigo));
            p.MySqlDbType = GetInfoCodigo().Type;
            ap.Add(p);

            p = new MySqlParameter("@infohorario", Parser.parseDateTimeBD(_infoHoraro));
            p.MySqlDbType = GetInfoHorario().Type;
            ap.Add(p);

            p = new MySqlParameter("@situacao", (int)_situacao);
            p.MySqlDbType = GetInfoSituacao().Type;
            ap.Add(p);

            return ap.ToArray();
        }

        /// <summary>
        /// Inicialização das propriedades do elemento com seus valores padrão
        /// </summary>
        private void IniciarElemento()
        {
            GetInfoTabela();

            _compra = -1;
            _participante = -1;
            _infoHoraro = Utils.dataPadrao;
            _situacao = Utils.SituacaoHorario.Indiferente;
        }

        /// <summary>
        /// Inicialização do elemento
        /// </summary>
        public HorarioSurto()
        {
            IniciarElemento();
        }

        /// <summary>
        /// Rotina que insere o registro no BD
        /// </summary>
        /// <returns>Código do registro inserido no BD</returns>
        public int Incluir()
        {
            if (_infoHoraro == Utils.dataPadrao)
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    sql.Parameters.AddRange(GetParametrosSQL());
                    sql.CommandText = "INSERT INTO HorariosSurtos "
                                    + "(campanha, compra, participante, infohorario, situacao) "
                                    + "VALUE (@campanha, @compra, @participante, @infohorario, @situacao); "
                                    + "SELECT LAST_INSERT_ID();";
                    int resultado = Parser.parseInteiro(new Data().executeEscalar(sql));
                    return resultado;
                }
            }
            catch (ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Rotina que altera os dados do registro no BD
        /// </summary>
        /// <returns>Quantidade de linhas alteradas na ação junto ao BD</returns>
        public int Alterar()
        {
            if (_codigo < 0)
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    sql.Parameters.AddRange(GetParametrosSQL());
                    sql.CommandText = "UPDATE HorariosSurtos "
                                    + "SET "
                                    + "campanha = @campanha, compra = @compra, infohorario = @infohorario, "
                                    + "situacao = @situacao, participante = @participante "
                                    + "WHERE codigo = @codigo";
                    int resultado = Parser.parseInteiro(new Data().execute(sql));
                    return resultado;
                }
            }
            catch (ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Rotina que exclui os dados do registro no BD
        /// </summary>
        /// <returns>Quantidade de linhas alteradas na ação junto ao BD</returns>
        public int Excluir()
        {
            if (_codigo < 0)
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    sql.Parameters.AddRange(GetParametrosSQL());
                    sql.CommandText = "DELETE FROM HorariosSurtos "
                                    + "WHERE codigo = @codigo";
                    int resultado = Parser.parseInteiro(new Data().execute(sql));
                    return resultado;
                }
            }
            catch (ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Rotina que exclui os dados do registro no BD
        /// </summary>
        /// <param name="campanha">VALUE: Código da campanha que terá horários excluídos</param>
        /// <returns>Quantidade de linhas alteradas na ação junto ao BD</returns>
        public static int ExcluirCampanha(int campanha)
        {
            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    sql.CommandText = "DELETE FROM HorariosSurtos "
                                    + "WHERE campanha = " + campanha;
                    int resultado = Parser.parseInteiro(new Data().execute(sql));
                    return resultado;
                }
            }
            catch (ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Rotina que consulta os horários de atendimentos em surtos
        /// </summary>
        /// <param name="pesquisa">VALUE: Elemento com os parâmetros de pesquisa</param>
        /// <returns></returns>
        public static List<HorarioSurto> ConsultarHorarios(PesquisaHorarioSurto pesquisa)
        {
            return ConsultaGenerica(pesquisa);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="codigo"></param>
        /// <returns></returns>
        public static HorarioSurto ConsultarUnico(int codigo)
        {
            if (codigo <= 0)
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            ImunneVacinas.PesquisaHorarioSurto pesquisa = new PesquisaHorarioSurto()
            {
                Codigo = codigo
            };

            List<ImunneVacinas.HorarioSurto> retorno = ConsultaGenerica(pesquisa);
            if (retorno == null || retorno.Count.Equals(0))
                return null;
            else
                return retorno[0];
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="codigo"></param>
        /// <returns></returns>
        public static HorarioSurto ConsultarUnicoCompra(int compra)
        {
            if (compra <= 0)
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            ImunneVacinas.PesquisaHorarioSurto pesquisa = new PesquisaHorarioSurto()
            {
                Compra = compra
            };

            List<ImunneVacinas.HorarioSurto> retorno = ConsultaGenerica(pesquisa);
            if (retorno == null || retorno.Count.Equals(0))
                return null;
            else
                return retorno[0];
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="codigo"></param>
        /// <returns></returns>
        public static HorarioSurto ConsultarUnico(int compra, int pessoa)
        {
            if (compra <= 0 || pessoa <= 0)
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            ImunneVacinas.PesquisaHorarioSurto pesquisa = new PesquisaHorarioSurto()
            {
                Compra = compra,
                Participante = pessoa
            };

            List<ImunneVacinas.HorarioSurto> retorno = ConsultaGenerica(pesquisa);
            if (retorno == null || retorno.Count.Equals(0))
                return null;
            else
                return retorno[0];
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pesquisa"></param>
        /// <returns></returns>
        internal static List<HorarioSurto> ConsultaGenerica(PesquisaHorarioSurto pesquisa)
        {
            StringBuilder ordenacao = new StringBuilder();
            StringBuilder filtro = new StringBuilder();
            StringBuilder top = new StringBuilder();
            MySqlParameter p;

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    if (pesquisa.Quantidade > 0)
                        top.AppendFormat(" LIMIT {0} ", pesquisa.Quantidade);

                    #region CAMPO CÓDIGO

                    if (pesquisa.Codigo > 0)
                    {
                        filtro.Append(" AND HorariosSurtos.codigo = @CODIGO");
                        p = new MySqlParameter("@CODIGO", pesquisa.Codigo);
                        p.MySqlDbType = GetInfoCodigo().Type;
                        sql.Parameters.Add(p);
                    }
                    else if (pesquisa.CodigoInicial > 0)
                    {
                        filtro.Append(" AND HorariosSurtos.codigo >= @INICIAL");
                        p = new MySqlParameter("@INICIAL", pesquisa.CodigoInicial);
                        p.MySqlDbType = GetInfoCodigo().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region CAMPO CAMPANHA

                    if (pesquisa.Campanha >= 0)
                    {
                        filtro.Append(" AND HorariosSurtos.campanha = @CAMPANHA");
                        p = new MySqlParameter("@CAMPANHA", pesquisa.Campanha);
                        p.MySqlDbType = GetInfoCampanha().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region CAMPO COMPRA

                    if (pesquisa.Compra >= 0)
                    {
                        filtro.Append(" AND HorariosSurtos.compra = @COMPRA");
                        p = new MySqlParameter("@COMPRA", pesquisa.Compra);
                        p.MySqlDbType = GetInfoCampanha().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region CAMPO PARTICIPANTE

                    if (pesquisa.Participante >= 0)
                    {
                        filtro.Append(" AND HorariosSurtos.participante = @PARTICIPANTE");
                        p = new MySqlParameter("@PARTICIPANTE", pesquisa.Participante);
                        p.MySqlDbType = GetInfoParticipante().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region CAMPO DATA

                    if (pesquisa.Dia != Utils.dataPadrao)
                    {
                        filtro.Append(" AND DATE(HorariosSurtos.infohorario) = @DATASURTO");
                        p = new MySqlParameter("@DATASURTO", pesquisa.Dia.Date);
                        p.MySqlDbType = GetInfoHorario().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region CAMPO SITUAÇÃO

                    if (pesquisa.Situacao != Utils.SituacaoHorario.Indiferente)
                    {
                        filtro.Append(" AND HorariosSurtos.SITUACAO = @SITUACAO");
                        p = new MySqlParameter("@situacao", (int)pesquisa.Situacao);
                        p.MySqlDbType = GetInfoSituacao().Type;
                        sql.Parameters.Add(p);
                    }
                    else if (pesquisa.ListaSituacoes != null && pesquisa.ListaSituacoes.Count > 0)
                    {
                        int i = 0;
                        filtro.Append(" AND HorariosSurtos.situacao in (");
                        foreach (int item in pesquisa.ListaSituacoes)
                        {
                            filtro.AppendFormat("@CODSITUACAO{0},", i);
                            p = new MySqlParameter("@CODSITUACAO" + i, item.ToString());
                            p.MySqlDbType = MySqlDbType.Int32;
                            sql.Parameters.Add(p);
                            i++;
                        }
                        filtro.Remove(filtro.ToString().LastIndexOf(","), 1);
                        filtro.Append(") ");
                    }

                    #endregion

                    if (filtro.Length > 0)
                        filtro = filtro.Replace(" AND ", " WHERE ", 0, 5);

                    #region Ordenação

                    switch (pesquisa.Ordenacao)
                    {
                        case Utils.TipoOrdenacao.porCodigo:
                            {
                                ordenacao.Append("ORDER BY HorariosSurtos.codigo");
                            }
                            break;
                        case Utils.TipoOrdenacao.porCodigoDesc:
                            {
                                ordenacao.Append("ORDER BY HorariosSurtos.codigo DESC");
                            }
                            break;
                        case Utils.TipoOrdenacao.porData:
                            {
                                ordenacao.Append("ORDER BY HorariosSurtos.infohorario");
                            }
                            break;
                        case Utils.TipoOrdenacao.porDataDesc:
                            {
                                ordenacao.Append("ORDER BY HorariosSurtos.infohorario DESC");
                            }
                            break;
                        case Utils.TipoOrdenacao.randomico:
                            {
                                ordenacao.Append("ORDER BY RAND()");
                            }
                            break;
                        default:
                            {
                                ordenacao.Append("ORDER BY HorariosSurtos.codigo");
                            }
                            break;
                    }

                    #endregion

                    sql.CommandText = String.Format("SELECT "
                                    + "HorariosSurtos.* "
                                    + "FROM HorariosSurtos "
                                    + "{0} {1} {2}", filtro, ordenacao, top);
                    DataTable dt = new Data().getData(sql);
                    return MontarRetorno(dt);
                }
            }
            catch (ImunneVacinas.ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Contabilizar total de horários com as características informadas
        /// </summary>
        /// <param name="pesquisa">VALUE: Elemento de pesquisa</param>
        /// <returns></returns>
        public static int ContabilizarHorarios(PesquisaHorarioSurto pesquisa)
        {
            int retorno = 0;
            StringBuilder filtro = new StringBuilder();
            MySqlParameter p;

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    #region CAMPO CÓDIGO

                    if (pesquisa.Codigo > 0)
                    {
                        filtro.Append(" AND HorariosSurtos.codigo = @CODIGO");
                        p = new MySqlParameter("@CODIGO", pesquisa.Codigo);
                        p.MySqlDbType = GetInfoCodigo().Type;
                        sql.Parameters.Add(p);
                    }
                    else if (pesquisa.CodigoInicial > 0)
                    {
                        filtro.Append(" AND HorariosSurtos.codigo >= @INICIAL");
                        p = new MySqlParameter("@INICIAL", pesquisa.CodigoInicial);
                        p.MySqlDbType = GetInfoCodigo().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region CAMPO CAMPANHA

                    if (pesquisa.Campanha >= 0)
                    {
                        filtro.Append(" AND HorariosSurtos.campanha = @CAMPANHA");
                        p = new MySqlParameter("@CAMPANHA", pesquisa.Campanha);
                        p.MySqlDbType = GetInfoCampanha().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region CAMPO COMPRA

                    if (pesquisa.Compra >= 0)
                    {
                        filtro.Append(" AND HorariosSurtos.compra = @COMPRA");
                        p = new MySqlParameter("@COMPRA", pesquisa.Compra);
                        p.MySqlDbType = GetInfoCampanha().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region CAMPO PARTICIPANTE

                    if (pesquisa.Participante >= 0)
                    {
                        filtro.Append(" AND HorariosSurtos.participante = @PARTICIPANTE");
                        p = new MySqlParameter("@PARTICIPANTE", pesquisa.Participante);
                        p.MySqlDbType = GetInfoParticipante().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region CAMPO DATA

                    if (pesquisa.Dia != Utils.dataPadrao)
                    {
                        filtro.Append(" AND DATE(HorariosSurtos.infohorario) = @DATASURTO");
                        p = new MySqlParameter("@DATASURTO", pesquisa.Dia.Date);
                        p.MySqlDbType = GetInfoHorario().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region CAMPO SITUAÇÃO

                    if (pesquisa.Situacao != Utils.SituacaoHorario.Indiferente)
                    {
                        filtro.Append(" AND HorariosSurtos.SITUACAO = @SITUACAO");
                        p = new MySqlParameter("@situacao", (int)pesquisa.Situacao);
                        p.MySqlDbType = GetInfoSituacao().Type;
                        sql.Parameters.Add(p);
                    }
                    else if (pesquisa.ListaSituacoes != null && pesquisa.ListaSituacoes.Count > 0)
                    {
                        int i = 0;
                        filtro.Append(" AND HorariosSurtos.situacao in (");
                        foreach (int item in pesquisa.ListaSituacoes)
                        {
                            filtro.AppendFormat("@CODSITUACAO{0},", i);
                            p = new MySqlParameter("@CODSITUACAO" + i, item.ToString());
                            p.MySqlDbType = MySqlDbType.Int32;
                            sql.Parameters.Add(p);
                            i++;
                        }
                        filtro.Remove(filtro.ToString().LastIndexOf(","), 1);
                        filtro.Append(") ");
                    }

                    #endregion

                    if (filtro.Length > 0)
                        filtro = filtro.Replace(" AND ", " WHERE ", 0, 5);

                    sql.CommandText = String.Format("SELECT COUNT(*) AS Total "
                                    + "FROM HorariosSurtos "
                                    + "{0}", filtro);
                    DataTable dt = new Data().getData(sql);
                    if (dt != null && dt.Rows.Count > 0)
                        retorno = Convert.ToInt32(dt.Rows[0]["Total"]);
                }
            }
            catch (Exception ex)
            {
                retorno = 0;
            }

            return retorno;
        }

        /// <summary>
        /// Rotina que retorna uma lista genérica do elemento, de acordo com o retorno da Consultar ao BD
        /// </summary>
        /// <param name="dt">VALUE: DataTable com o resultado da Consultar ao BD</param>
        /// <returns>Lista do elemento com os dados retornados da consulta</returns>
        internal static List<HorarioSurto> MontarRetorno(DataTable dt)
        {
            int i = -1;
            List<HorarioSurto> list = new List<HorarioSurto>();

            if (dt == null && dt.Rows.Count == 0)
                return list;
            else
            {
                HorarioSurto retorno;
                try
                {
                    for (i = 0; i < dt.Rows.Count; i++)
                    {
                        retorno = new HorarioSurto()
                        {
                            _codigo = Parser.parseInteiro(dt.Rows[i]["codigo"]),
                            _participante = Parser.parseInteiro(dt.Rows[i]["participante"]),
                            _infoHoraro = Convert.ToDateTime(dt.Rows[i]["infohorario"]),
                            _situacao = (ImunneVacinas.Utils.SituacaoHorario)Enum.Parse(typeof(ImunneVacinas.Utils.SituacaoHorario), dt.Rows[i]["situacao"].ToString()),
                            _compra = Parser.parseInteiro(dt.Rows[i]["compra"]),
                            _campanha = Parser.parseInteiro(dt.Rows[i]["campanha"]),
                        };

                        list.Add(retorno);
                    }
                    return list;
                }
                catch (Exception ex)
                {
                    ex.Data.Add("último retorno lido", i);
                    throw;
                }
            }
        }
    }
}