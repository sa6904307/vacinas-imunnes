﻿using MySql.Data.MySqlClient;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace ImunneVacinas
{
    /// <summary>
    /// Descrição resumida de ProdutoCampanha
    /// </summary>
    [Serializable]
    public class ProdutoCampanha
    {
        private static string _tabela = "ProdutosCampanhas";
        private static List<BD.Columns> columns;
        private static DateTime columnsUpdated;

        #region Propriedades do elemento

        private Campanha _idCampanha;
        private Vacina _idVacina;
        private int _codigo;
        private decimal _preco;
        private DateTime _dataCriacao;
        private DateTime _dataAlteracao;

        /// <summary>
        /// Identificação da Campanha
        /// </summary>
        public Campanha IdCampanha
        {
            get
            {
                return _idCampanha;
            }

            set
            {
                _idCampanha = value;
            }
        }

        /// <summary>
        /// Identificação da ProdutoCampanha
        /// </summary>
        public Vacina IdVacina
        {
            get
            {
                return _idVacina;
            }

            set
            {
                _idVacina = value;
            }
        }

        /// <summary>
        /// Código de identificação do registro
        /// </summary>
        public int Codigo
        {
            get
            {
                return _codigo;
            }

            set
            {
                _codigo = value;
            }
        }

        /// <summary>
        /// Preço do produto na campanha
        /// </summary>
        public decimal Preco
        {
            get
            {
                return _preco;
            }

            set
            {
                _preco = value;
            }
        }

        /// <summary>
        /// Data de criação do registro
        /// </summary>
        public DateTime DataCriacao
        {
            get
            {
                return _dataCriacao;
            }

            set
            {
                _dataCriacao = value;
            }
        }

        /// <summary>
        /// Data da última alteração do registro
        /// </summary>
        public DateTime DataAlteracao
        {
            get
            {
                return _dataAlteracao;
            }

            set
            {
                _dataAlteracao = value;
            }
        }

        #endregion

        #region GetInfos

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos os campos da tabela.
        /// </summary>
        private static void GetInfoTabela()
        {
            if (columns == null || (DateTime.Now - columnsUpdated).Minutes > 0)
            {
                columns = BD.Columns.getColumnsInfo(_tabela);
                columnsUpdated = DateTime.Now;
            }
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo campanha.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoCampanha()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "campanha");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo vacina.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoVacina()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "vacina");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo codigo.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoCodigo()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "codigo");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo preco.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoPreco()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "preco");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo datacriacao.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoDataCriacao()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "datacriacao");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo dataalteracao.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoDataAlteracao()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "dataalteracao");
        }

        #endregion

        /// <summary> 
        /// Rotina que cria um Array com os parâmetros utilizados nas ações de INSERT, UPDATE e DELETE
        /// <returns>Array com vários MySqlParameters</returns>
        /// </summary>
        private Array GetParametrosSQL()
        {
            MySqlParameter p;
            ArrayList ap = new ArrayList();

            p = new MySqlParameter("@campanha", Parser.parseInteiro(_idCampanha.Codigo));
            p.MySqlDbType = GetInfoCampanha().Type;
            ap.Add(p);

            p = new MySqlParameter("@vacina", Parser.parseInteiro(_idVacina.Codigo));
            p.MySqlDbType = GetInfoVacina().Type;
            ap.Add(p);

            p = new MySqlParameter("@codigo", Parser.parseInteiro(_codigo));
            p.MySqlDbType = GetInfoCodigo().Type;
            ap.Add(p);

            p = new MySqlParameter("@preco", Parser.parseDecimal(_preco));
            p.MySqlDbType = GetInfoPreco().Type;
            ap.Add(p);

            p = new MySqlParameter("@datacriacao", Parser.parseDateTimeBD(_dataCriacao));
            p.MySqlDbType = GetInfoDataCriacao().Type;
            ap.Add(p);

            p = new MySqlParameter("@dataalteracao", Parser.parseDateTimeBD(_dataAlteracao));
            p.MySqlDbType = GetInfoDataAlteracao().Type;
            ap.Add(p);

            return ap.ToArray();
        }

        /// <summary>
        /// Inicialização das propriedades do elemento com seus valores padrão
        /// </summary>
        private void IniciarElemento()
        {
            GetInfoTabela();

            _idCampanha = new Campanha();
            _idVacina = new Vacina();

            _idCampanha.Codigo = -1;
            _idVacina.Codigo = -1;
            _preco = 0;
            _dataAlteracao = Utils.dataPadrao;
            _dataCriacao = Utils.dataPadrao;
        }

        /// <summary>
        /// Inicialização do elemento
        /// </summary>
        public ProdutoCampanha()
        {
            IniciarElemento();
        }

        /// <summary>
        /// Rotina que insere o registro no BD
        /// </summary>
        /// <returns>Código do registro inserido no BD</returns>
        public int Incluir()
        {
            if (_idCampanha.Codigo < 0 || _idVacina.Codigo < 0)
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    sql.Parameters.AddRange(GetParametrosSQL());
                    sql.CommandText = "INSERT INTO ProdutosCampanhas "
                                    + "(campanha, vacina, preco, datacriacao, dataalteracao)  "
                                    + "VALUE (@campanha, @vacina, @preco, @datacriacao, @dataalteracao); "
                                    + "SELECT LAST_INSERT_ID();";
                    int resultado = Parser.parseInteiro(new Data().executeEscalar(sql));
                    return resultado;
                }
            }
            catch (ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Rotina que altera os dados do registro no BD
        /// </summary>
        /// <returns>Quantidade de linhas alteradas na ação junto ao BD</returns>
        public int Alterar()
        {
            if (_codigo < 0)
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    sql.Parameters.AddRange(GetParametrosSQL());
                    sql.CommandText = "UPDATE ProdutosCampanhas "
                                    + "SET "
                                    + "dataalteracao = @dataalteracao, preco = @preco "
                                    + "WHERE codigo = @codigo";
                    int resultado = Parser.parseInteiro(new Data().execute(sql));
                    return resultado;
                }
            }
            catch (ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Rotina que exclui o registro do BD
        /// </summary>
        /// <returns>Quantidade de linhas alteradas na ação junto ao BD</returns>
        public int Excluir()
        {
            if (_codigo < 0)
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    sql.Parameters.AddRange(GetParametrosSQL());
                    sql.CommandText = "DELETE FROM ProdutosCampanhas "
                                    + "WHERE codigo = @codigo";
                    int resultado = Parser.parseInteiro(new Data().execute(sql));
                    return resultado;
                }
            }
            catch (ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Rotina que localiza e retorna um único registro, mediante o código informado
        /// </summary>
        /// <param name="codigo">VALUE: código de identificação do registro (obrigatório)</param>
        /// <returns>Elemento Usuario com os dados retornados da consulta</returns>
        public static ProdutoCampanha ConsultarUnico(int codigo)
        {
            if (codigo <= 0)
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            ImunneVacinas.PesquisaProdutoCampanha pesquisa = new PesquisaProdutoCampanha()
            {
                Codigo = codigo
            };

            List<ProdutoCampanha> retorno = ConsultaGenerica(pesquisa);
            if (retorno == null || retorno.Count.Equals(0))
                return null;
            else
                return retorno[0];
        }

        /// <summary>
        /// Rotina que localiza e retorna um único registro, mediante o código informado
        /// </summary>
        /// <param name="codigo">VALUE: código de identificação do registro (obrigatório)</param>
        /// <returns>Elemento Usuario com os dados retornados da consulta</returns>
        public static ProdutoCampanha ConsultarUnico(int campanha, int produto)
        {
            if (campanha <= 0 || produto <= 0)
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            ImunneVacinas.PesquisaProdutoCampanha pesquisa = new PesquisaProdutoCampanha()
            {
                Vacina = produto,
                Campanha = campanha
            };

            List<ProdutoCampanha> retorno = ConsultaGenerica(pesquisa);
            if (retorno == null || retorno.Count.Equals(0))
                return null;
            else
                return retorno[0];
        }

        /// <summary>
        /// Rotina que retorna um elemento RetornoCBX para utilização em DropLists
        /// </summary>
        /// <param name="InserirLinhaPadrao">VALUE: Informa se será adiconada a linha padrão</param>
        /// <param name="textoPadrao">VALUE: Informa o texto utilizado para linha padão (se utilizado NULL, será apresentado SELECIONE)</param>
        /// <returns>Lista do elemento RetornoCBX com os dados desejados</returns>
        public static List<RetornoComboBox> ConsultarProdutosClinicaComboBox(int codigoClinica, int codigoCampanha, bool InserirLinhaPadrao, string textoPadrao)
        {
            StringBuilder ordenacao = new StringBuilder();
            StringBuilder filtro = new StringBuilder();
            MySqlParameter p;

            DataTable dt = new DataTable();
            DataTable dtRetorno = new DataTable();

            try
            {
                dtRetorno.Columns.Add("DisplayMember");
                dtRetorno.Columns.Add("ValueMember");

                ImunneVacinas.PesquisaCampanha pesquisa = new PesquisaCampanha()
                {
                    Ordenacao = Utils.TipoOrdenacao.porNome
                };

                using(MySqlCommand sql = new MySqlCommand())
                {
                    #region CAMPO CLINICA

                    if (codigoClinica >= 0)
                    {
                        filtro.Append(" AND CampanhasClinicas.clinica = @clinica");
                        p = new MySqlParameter("@clinica", codigoClinica);
                        p.MySqlDbType = MySqlDbType.Int32;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region CAMPO CAMPANHA

                    if (codigoCampanha >= 0)
                    {
                        filtro.Append(" AND CampanhasClinicas.campanha = @campanha");
                        p = new MySqlParameter("@campanha", codigoCampanha);
                        p.MySqlDbType = MySqlDbType.Int32;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    if (filtro.Length > 0)
                        filtro = filtro.Replace(" AND ", " WHERE ", 0, 5);

                    ordenacao.Append("ORDER BY Vacinas.nome");

                    sql.CommandText = String.Format("SELECT "
                                    + "DISTINCT(Vacinas.codigo) AS codigoVacina, Vacinas.nome AS nomeVacina "
                                    + "FROM Vacinas "
                                    + "LEFT JOIN ProdutosCampanhas ON Vacinas.codigo = ProdutosCampanhas.vacina "
                                    + "LEFT JOIN Campanhas ON Campanhas.codigo = ProdutosCampanhas.campanha "
                                    + "LEFT JOIN CampanhasClinicas ON CampanhasClinicas.campanha = Campanhas.codigo "
                                    + "LEFT JOIN Clinicas ON Clinicas.codigo = CampanhasClinicas.clinica "
                                    + "{0} {1}", filtro, ordenacao);

                    dt = new Data().getData(sql);
                    foreach (DataRow item in dt.Rows)
                    {
                        dtRetorno.Rows.Add(new object[] { String.Format("{0}", item["nomeVacina"].ToString().ToUpper()), item["codigoVacina"].ToString() });
                    }
                }

                return Utils.MontarRetornoComboBox(dtRetorno, InserirLinhaPadrao, textoPadrao);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// Rotina que realiza a pesquisa de elementos mediante os parametros de pesquisa informados
        /// </summary>
        /// <param name="pesquisa">VALUE: elemento de pesquisa com os parametros para consulta</param>
        /// <returns></returns>
        public static List<ProdutoCampanha> ConsultarProdutoCampanhas(PesquisaProdutoCampanha pesquisa)
        {
            return ConsultaGenerica(pesquisa);
        }

        /// <summary>
        /// Rotina interna utilizada para todas as pesquisas do elemento
        /// </summary>
        /// <param name="pesquisa">VALUE: Elemento com os parâmetros com os quais será realizada a pesquisa</param>
        /// <returns>Lista do elemento com os dados retornados da consulta</returns>
        internal static List<ProdutoCampanha> ConsultaGenerica(PesquisaProdutoCampanha pesquisa)
        {
            StringBuilder ordenacao = new StringBuilder();
            StringBuilder filtro = new StringBuilder();
            StringBuilder top = new StringBuilder();
            MySqlParameter p;

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    if (pesquisa.Quantidade > 0)
                        top.AppendFormat(" LIMIT {0} ", pesquisa.Quantidade);

                    #region Campo Código

                    if (pesquisa.Codigo >= 0)
                    {
                        filtro.Append(" AND ProdutosCampanhas.codigo = @codigo");
                        p = new MySqlParameter("@codigo", pesquisa.Codigo);
                        p.MySqlDbType = GetInfoCodigo().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Campanha

                    if (pesquisa.Campanha >= 0)
                    {
                        filtro.Append(" AND ProdutosCampanhas.campanha = @campanha");
                        p = new MySqlParameter("@campanha", pesquisa.Campanha);
                        p.MySqlDbType = GetInfoCampanha().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Vacina

                    if (pesquisa.Vacina >= 0)
                    {
                        filtro.Append(" AND ProdutosCampanhas.vacina = @vacina");
                        p = new MySqlParameter("@vacina", pesquisa.Vacina);
                        p.MySqlDbType = GetInfoVacina().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Livre

                    if (!String.IsNullOrEmpty(pesquisa.CampoLivre.Trim()))
                    {
                        string campoLivre = String.Format("%{0}%", pesquisa.CampoLivre.Trim());
                        filtro.Append(" AND (Vacinas.nome LIKE @campoLivre ");
                        filtro.Append(" OR Vacinas.observacoes LIKE @campoLivre ");
                        filtro.Append(")");
                        p = new MySqlParameter("@campoLivre", campoLivre);
                        p.MySqlDbType = MySqlDbType.VarChar;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    if (filtro.Length > 0)
                        filtro = filtro.Replace(" AND ", " WHERE ", 0, 5);

                    #region Ordenação

                    switch (pesquisa.Ordenacao)
                    {
                        case Utils.TipoOrdenacao.porCodigo:
                            {
                                ordenacao.Append("ORDER BY ProdutosCampanhas.codigo");
                            }
                            break;
                        case Utils.TipoOrdenacao.porCodigoDesc:
                            {
                                ordenacao.Append("ORDER BY ProdutosCampanhas.codigo DESC");
                            }
                            break;
                        case Utils.TipoOrdenacao.porData:
                            {
                                ordenacao.Append("ORDER BY ProdutosCampanhas.datacriacao");
                            }
                            break;
                        case Utils.TipoOrdenacao.porDataDesc:
                            {
                                ordenacao.Append("ORDER BY ProdutosCampanhas.datacriacao DESC");
                            }
                            break;
                        case Utils.TipoOrdenacao.randomico:
                            {
                                ordenacao.Append("ORDER BY RAND()");
                            }
                            break;
                        default:
                            {
                                ordenacao.Append("ORDER BY ProdutosCampanhas.codigo");
                            }
                            break;
                    }

                    #endregion

                    sql.CommandText = String.Format("SELECT "
                        + "Vacinas.codigo AS codigoVacina, Vacinas.nome AS nomeVacina, "
                        + "Campanhas.codigo AS codigoCampanha, Campanhas.nome AS nomeCampanha, "
                        + "ProdutosCampanhas.* "
                        + "FROM ProdutosCampanhas "
                        + "INNER JOIN Vacinas ON Vacinas.codigo = ProdutosCampanhas.vacina "
                        + "INNER JOIN Campanhas ON Campanhas.codigo = ProdutosCampanhas.campanha "
                        + "{0} {1} {2}", filtro, ordenacao, top);
                    DataTable dt = new Data().getData(sql);
                    return MontarRetorno(dt);
                }
            }
            catch (ImunneVacinas.ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Rotina que retorna uma lista genérica do elemento, de acordo com o retorno da Consultar ao BD
        /// </summary>
        /// <param name="dt">VALUE: DataTable com o resultado da Consultar ao BD</param>
        /// <returns>Lista do elemento com os dados retornados da consulta</returns>
        internal static List<ProdutoCampanha> MontarRetorno(DataTable dt)
        {
            int i = -1;
            List<ProdutoCampanha> list = new List<ProdutoCampanha>();

            if (dt == null && dt.Rows.Count == 0)
                return list;
            else
            {
                ProdutoCampanha retorno;
                try
                {
                    for (i = 0; i < dt.Rows.Count; i++)
                    {
                        retorno = new ProdutoCampanha()
                        {
                            _codigo = Parser.parseInteiro(dt.Rows[i]["codigo"]),
                            _preco = Parser.parseDecimal(dt.Rows[i]["preco"]),
                            _dataCriacao = Convert.ToDateTime(dt.Rows[i]["datacriacao"])
                        };

                        retorno._idCampanha.Codigo = Parser.parseInteiro(dt.Rows[i]["codigoCampanha"]);
                        retorno._idCampanha.Nome = Parser.parseString(dt.Rows[i]["nomeCampanha"]);

                        retorno._idVacina.Codigo = Parser.parseInteiro(dt.Rows[i]["codigoVacina"]);
                        retorno._idVacina.Nome = Parser.parseString(dt.Rows[i]["nomeVacina"]);

                        if (!String.IsNullOrEmpty(dt.Rows[i]["dataalteracao"].ToString()))
                            retorno._dataAlteracao = Convert.ToDateTime(dt.Rows[i]["dataalteracao"]);

                        list.Add(retorno);
                    }
                    return list;
                }
                catch (Exception ex)
                {
                    ex.Data.Add("último retorno lido", i);
                    throw;
                }
            }
        }
    }
}