﻿using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace ImunneVacinas
{
    /// <summary>
    /// 
    /// </summary>
    public class CidadeCep
    {
        public string ddd { get; set; }
        public string ibge { get; set; }
        public string nome { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public CidadeCep()
        {
            ddd = String.Empty;
            ibge = String.Empty;
            nome = String.Empty;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class EstadoCep
    {
        public string sigla { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public EstadoCep()
        {
            sigla = String.Empty;
        }
    }

    /// <summary>
    /// Descrição resumida de Ceps
    /// </summary>
    public class Cep
    {
        public string altitude { get; set; }
        public string cep { get; set; }
        public string latitude { get; set; }
        public string longitude { get; set; }
        public string logradouro { get; set; }
        public string bairro { get; set; }
        public CidadeCep cidade { get; set; }
        public EstadoCep estado { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Cep()
        {
            cidade = new CidadeCep();
            estado = new EstadoCep();

            altitude = String.Empty;
            cep = String.Empty;
            latitude = String.Empty;
            longitude = String.Empty;
            logradouro = String.Empty;
            bairro = String.Empty;
        }

        /// <summary>
        /// Consulta API para retorno de informações de endereçamento
        /// </summary>
        /// <param name="cep"></param>
        /// <param name="estado"></param>
        /// <param name="cidade"></param>
        /// <param name="bairro"></param>
        /// <param name="logradouro"></param>
        /// <param name="numero"></param>
        public static Cep ConsultarCep(string cep = null, string estado = null, string cidade = null, string bairro = null, string logradouro = null, string numero = null)
        {
            string endPoint = "http://www.cepaberto.com/api/v3/";
            ImunneVacinas.Cep cepRetorno = new Cep();

            try
            {
                HttpClient cliente = new HttpClient()
                {
                    Timeout = TimeSpan.FromMilliseconds(50000)
                };
                cliente.DefaultRequestHeaders.Add("Authorization", "Token token=5d379e601120202da1df859ca9aeeb90");

                string rota = "";
                if (!String.IsNullOrEmpty(cep))
                    rota = endPoint + "cep?cep=" + cep;
                else
                    rota = endPoint + "address?estado=" + estado + "&cidade=" + cidade + "&bairro=" + bairro + "&logradouro=" + logradouro + "&numero=" + numero;

                var result = cliente.GetAsync(rota).GetAwaiter().GetResult();
                if (result != null)
                {
                    string conteudo = result.Content.ReadAsStringAsync().Result.ToString();
                    cepRetorno = JsonConvert.DeserializeObject<ImunneVacinas.Cep>(conteudo);
                }

                return cepRetorno;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// Rotina que consulta a API CEP Aberto informando o CEP e retorna o JSON com os dados
        /// </summary>
        /// <param name="cep">VALUE: CEP a ser consultado</param>
        /// <returns></returns>
        public static Cep RequestCEP(string cep)
        {
            var token = "Token token=5d379e601120202da1df859ca9aeeb90";
            var url = "http://www.cepaberto.com/api/v3/cep?cep={0}";

            var client = new WebClient { Encoding = Encoding.UTF8 };
            client.Headers.Add(HttpRequestHeader.Authorization, token);

            var requestResult = client.DownloadString(string.Format(url, cep));

            var jss = new JavaScriptSerializer();
            var response = jss.Deserialize<Cep>(requestResult);

            return response;
        }
    }
}