﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace ImunneVacinas
{
    public class Uf
    {
        #region Propriedade do elemento

        private string _sigla;
        private string _nome;

        /// <summary>
        /// Sigla do estado
        /// </summary>
        public string Sigla
        {
            get { return _sigla; }
            set { _sigla = value; }
        }

        /// <summary>
        /// Nome do estado
        /// </summary>
        public string Nome
        {
            get { return _nome; }
            set { _nome = value; }
        }

        #endregion

        /// <summary>
        /// Rotina que localiza e retorna um único registro, mediante a sigla informada
        /// </summary>
        /// <param name="sigla">VALUE: sigla de identificação do registro (obrigatório)</param>
        /// <returns>Elemento Usuario com os dados retornados da consulta</returns>
        public static Uf ConsultarUnico(string sigla)
        {
            if (String.IsNullOrEmpty(sigla))
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            PesquisaUF pesquisa = new PesquisaUF()
            {
                Sigla = sigla,
                Ordenacao = Utils.TipoOrdenacao.porCodigo
            };

            List<Uf> retornos = ConsultaGenerica(pesquisa);
            if (retornos != null && retornos.Count > 0)
                return retornos[0];
            else
                return null;
        }

        /// <summary>
        /// Rotina que realiza a pesquisa de elementos mediante os parametros de pesquisa informados
        /// </summary>
        /// <param name="pesquisa">VALUE: elemento de pesquisa com os parametros para consulta</param>
        /// <returns></returns>
        public static List<Uf> ConsultarEstados(PesquisaUF pesquisa)
        {
            return ConsultaGenerica(pesquisa);
        }

        /// <summary>
        /// Rotina que retorna um elemento RetornoCBX para utilização em DropLists
        /// </summary>
        /// <param name="InserirLinhaPadrao">VALUE: Informa se será adiconada a linha padrão</param>
        /// <param name="textoPadrao">VALUE: Informa o texto utilizado para linha padão (se utilizado NULL, será apresentado SELECIONE)</param>
        /// <returns>Lista do elemento RetornoCBX com os dados desejados</returns>
        public static List<RetornoComboBox> ConsultarComboBox(bool InserirLinhaPadrao, string textoPadrao)
        {
            DataTable dt = new DataTable();
            try
            {
                dt.Columns.Add("DisplayMember");
                dt.Columns.Add("ValueMember");

                ImunneVacinas.PesquisaUF pesquisa = new PesquisaUF()
                {
                    Ordenacao = Utils.TipoOrdenacao.porNome
                };

                List<Uf> retorno = ConsultaGenerica(pesquisa);
                if (retorno != null)
                {
                    foreach (Uf c in retorno)
                    {
                        dt.Rows.Add(new object[] { c.Sigla.ToUpper(), c.Sigla });
                    }
                }
                return Utils.MontarRetornoComboBox(dt, InserirLinhaPadrao, textoPadrao);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// Rotina interna utilizada para todas as pesquisas do elemento
        /// </summary>
        /// <param name="pesquisa">VALUE: Elemento com os parâmetros com os quais será realizada a pesquisa</param>
        /// <returns>Lista do elemento com os dados retornados da consulta</returns>
        internal static List<Uf> ConsultaGenerica(PesquisaUF pesquisa)
        {
            StringBuilder ordenacao = new StringBuilder();
            StringBuilder filtro = new StringBuilder();
            StringBuilder top = new StringBuilder();
            MySqlParameter p;

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    if (pesquisa.Quantidade > 0)
                        top.AppendFormat(" LIMIT {0} ", pesquisa.Quantidade);

                    #region Campo Sigla

                    if (!String.IsNullOrEmpty(pesquisa.Sigla))
                    {
                        string sigla = String.Format("{0}", pesquisa.Sigla);
                        filtro.Append(" AND Estados.nome = @sigla");
                        p = new MySqlParameter("@sigla", sigla);
                        p.MySqlDbType = MySqlDbType.VarChar;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region Campo Livre

                    if (!String.IsNullOrEmpty(pesquisa.CampoLivre))
                    {
                        string campoLivre = String.Format("%{0}%", pesquisa.CampoLivre);
                        filtro.Append(" AND Estados.nome LIKE @pesquisa");
                        p = new MySqlParameter("@pesquisa", campoLivre);
                        p.MySqlDbType = MySqlDbType.VarChar;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    if (filtro.Length > 0)
                        filtro.Replace(" AND ", " WHERE ", 0, 5);

                    #region Ordenação

                    switch (pesquisa.Ordenacao)
                    {
                        case Utils.TipoOrdenacao.porCodigo:
                            {
                                ordenacao.Append("ORDER BY Estados.sigla");
                            }
                            break;
                        case Utils.TipoOrdenacao.porNome:
                            {
                                ordenacao.Append("ORDER BY Estados.nome");
                            }
                            break;
                        case Utils.TipoOrdenacao.porNomeDesc:
                            {
                                ordenacao.Append("ORDER BY Estados.nome DESC");
                            }
                            break;
                        case Utils.TipoOrdenacao.randomico:
                            {
                                ordenacao.Append("ORDER BY RAND()");
                            }
                            break;
                        default:
                            {
                                ordenacao.Append("ORDER BY Estados.sigla");
                            }
                            break;
                    }

                    #endregion

                    sql.CommandText = String.Format("SELECT "
                                    + "Estados.* "
                                    + "FROM Estados "
                                    + "{0} {1} {2}", filtro, ordenacao, top);
                    DataTable dt = new Data().getData(sql);
                    return MontarRetorno(dt);
                }
            }
            catch (ImunneVacinas.ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Rotina que retorna uma lista genérica do elemento, de acordo com o retorno da Consultar ao BD
        /// </summary>
        /// <param name="dt">VALUE: DataTable com o resultado da Consultar ao BD</param>
        /// <returns>Lista do elemento com os dados retornados da consulta</returns>
        internal static List<Uf> MontarRetorno(DataTable dt)
        {
            int i = -1;
            List<Uf> list = new List<Uf>();

            if (dt == null && dt.Rows.Count == 0)
                return list;
            else
            {
                try
                {
                    Uf item;
                    for (i = 0; i < dt.Rows.Count; i++)
                    {
                        item = new Uf()
                        {
                            _sigla = dt.Rows[i]["sigla"].ToString(),
                            _nome = dt.Rows[i]["nome"].ToString()
                        };
                        list.Add(item);
                    }

                    return list;
                }
                catch (Exception ex)
                {
                    ex.Data.Add("último retorno lido", i);
                    throw;
                }
            }
        }
    }
}