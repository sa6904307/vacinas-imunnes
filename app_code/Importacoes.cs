﻿using ExcelDataReader;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using System.Web;

namespace ImunneVacinas
{
    /// <summary>
    /// Descrição resumida de Importacoes
    /// </summary>
    public class Importacao
    {
        /// <summary>
        /// Rotina que converte arquivo CSV para DataTable
        /// </summary>
        /// <param name="arquivoCSV">VALUE: Caminho onde se encontra o arquivo CSV a ser tratado</param>
        /// <returns></returns>
        public static DataTable ConverterCSVParaDataTable(string arquivoCSV)
        {
            Encoding encodeArquivo = Utils.ConsultarEncodeArquivo(arquivoCSV);
            DataTable dtRetorno = new DataTable();
            string linha;

            StreamReader leitor = new System.IO.StreamReader(arquivoCSV, encodeArquivo);
            // Leitura do HEADER, para criação das colunas do DataTable
            string linhaCabecalho = leitor.ReadLine();
            string[] valoresCabecalho = linhaCabecalho.Split(';');
            int identificador = 1;
            foreach (string valor in valoresCabecalho)
            {
                dtRetorno.Columns.Add(identificador + valor);
                identificador += 1;
            }

            // Lendo todas as demais linhas
            while ((linha = leitor.ReadLine()) != null)
            {
                string[] valoresCampos = linha.Split(';');
                DataRow dr = dtRetorno.NewRow();
                for (int i = 0; i < dtRetorno.Columns.Count; i++)
                {
                    dr[i] = valoresCampos[i];
                }
                dtRetorno.Rows.Add(dr);
            }

            leitor.Close();

            return dtRetorno;
        }

        /// <summary>
        /// Rotina que converte arquivo XLS/XLSX para DataTable
        /// </summary>
        /// <param name="arquivoXLS">VALUE: Caminho onde se encontra o arquivo XLS/XLSX a ser tratato</param>
        /// <returns></returns>
        public static DataTable ConverterXLSParaDataTable(string arquivoXLS)
        {
            DataSet dtsRetorno = new DataSet();
            DataTable dtRetorno = new DataTable();

            using (var arquivo = File.Open(arquivoXLS, FileMode.Open, FileAccess.Read))
            {
                using (var leitor = ExcelReaderFactory.CreateReader(arquivo))
                {
                    dtsRetorno = leitor.AsDataSet();
                }

                arquivo.Close();
            }

            if (dtsRetorno != null && dtsRetorno.Tables.Count > 0)
                dtRetorno = dtsRetorno.Tables[0];

            return dtRetorno;
        }

        /// <summary>
        /// Rotina que processa um DATABLE com os dados de Adesoes a serem alimentados no BD
        /// </summary>
        /// <param name="codigoCampanha">VALUE: Código de identificação da campanha (obrigatório)</param>
        /// <param name="dtOrigem">VALUE: DATABLE de origem dos dados (obrigatório)</param>
        /// <returns>LIST Genérico com os retornos de cada linha processada do DATATABLE</returns>
        public static List<RetornoProcesso> ImportacaoAdesoes(int codigoCampanha, DataTable dtOrigem)
        {
            List<RetornoProcesso> listaRetorno = new List<RetornoProcesso>();
            RetornoProcesso linhaRetorno;

            int contador = 0;

            try
            {
                if (codigoCampanha > 0)
                {
                    Campanha campanhaSelecionada = Campanha.ConsultarUnico(codigoCampanha);

                    if (dtOrigem != null && dtOrigem.Rows.Count > 0)
                    {
                        foreach (DataRow linha in dtOrigem.Rows)
                        {
                            Empresa empresaProcessada = new Empresa();
                            Participante participanteProcessado = new Participante();

                            string cpfParticipante = Utils.LimparCpfCnpj(linha[0].ToString().Trim());       // 0 - COLUNA CPF/CNPJ
                            string nomeParticipante = linha[1].ToString().ToUpper().Trim();                 // 1 - COLUNA NOME PARTICIPANTE
                            string nascimentoParticipante = linha[2].ToString().ToUpper().Trim();           // 2 - COLUNA NASCIMENTO PARTICIPANTE
                            string localAdesao = linha[3].ToString().ToUpper().Trim();                      // 3 - COLUNA LOCAL ADESÃO
                            string enderecoAdesao = linha[4].ToString().ToUpper().Trim();                   // 4 - COLUNA ENDEREÇO ADESÃO

                            int resultado = -1;
                            int codigoLocal = -1;
                            int codigoParticipante = -1;

                            if (cpfParticipante.ToUpper() != "CPF/CNPJ" && cpfParticipante.ToUpper() != "CPFCNPJ")
                            {
                                contador += 1;

                                #region VALIDANDO CAMPOS VAZIOS

                                if (String.IsNullOrEmpty(cpfParticipante) || cpfParticipante == "")
                                {
                                    linhaRetorno = new RetornoProcesso()
                                    {
                                        Classificacao = "ERRO",
                                        Linha = String.Format("LINHA {0}", contador.ToString().PadLeft(2, '0')),
                                        Resumo = String.Format("NÃO FOI INFORMADO O VALOR DO CPF/CNPJ")
                                    };
                                    listaRetorno.Add(linhaRetorno);

                                    continue;
                                }

                                if (String.IsNullOrEmpty(nomeParticipante) || nomeParticipante == "")
                                {
                                    linhaRetorno = new RetornoProcesso()
                                    {
                                        Classificacao = "ERRO",
                                        Linha = String.Format("LINHA {0}", contador.ToString().PadLeft(2, '0')),
                                        Resumo = String.Format("NÃO FOI O NOME DO PARTICIPANTE")
                                    };
                                    listaRetorno.Add(linhaRetorno);

                                    continue;
                                }

                                if (String.IsNullOrEmpty(localAdesao) || localAdesao == "")
                                {
                                    linhaRetorno = new RetornoProcesso()
                                    {
                                        Classificacao = "ERRO",
                                        Linha = String.Format("LINHA {0}", contador.ToString().PadLeft(2, '0')),
                                        Resumo = String.Format("NÃO FOI INFORMADO O NOME DO LOCAL DE APLICAÇÃO")
                                    };
                                    listaRetorno.Add(linhaRetorno);

                                    continue;
                                }

                                if (String.IsNullOrEmpty(enderecoAdesao) || enderecoAdesao == "")
                                {
                                    linhaRetorno = new RetornoProcesso()
                                    {
                                        Classificacao = "ERRO",
                                        Linha = String.Format("LINHA {0}", contador.ToString().PadLeft(2, '0')),
                                        Resumo = String.Format("NÃO FOI INFORMADO O ENDEREÇO DA APLICAÇÃO")
                                    };
                                    listaRetorno.Add(linhaRetorno);

                                    continue;
                                }

                                #endregion

                                #region VALIDANDO CPF/CNPJ

                                if (!Utils.ValidarCpfCnpj(Utils.LimparCpfCnpj(cpfParticipante)))
                                {
                                    linhaRetorno = new RetornoProcesso()
                                    {
                                        Classificacao = "ERRO",
                                        Linha = String.Format("LINHA {0}", contador.ToString().PadLeft(2, '0')),
                                        Resumo = String.Format("CPF/CNPJ INFORMADO É INVÁLIDO {0}.", cpfParticipante)
                                    };
                                    listaRetorno.Add(linhaRetorno);

                                    continue;
                                }

                                #endregion

                                #region VALIDANDO PARTICIPANTE

                                Participante participante = Participante.ConsultarUnico(Utils.LimparCpfCnpj(cpfParticipante));
                                if (participante == null)
                                {
                                    participante = new Participante()
                                    {
                                        Cpf = Utils.LimparCpfCnpj(cpfParticipante),
                                        DataCriacao = DateTime.Now,
                                        Email = "",
                                        Nome = nomeParticipante
                                    };

                                    if (!String.IsNullOrEmpty(nascimentoParticipante) && Convert.ToDateTime(nascimentoParticipante) != Utils.dataPadrao)
                                        participante.DataNascimento = Convert.ToDateTime(nascimentoParticipante);

                                    codigoParticipante = participante.Incluir();
                                }
                                else codigoParticipante = participante.Codigo;

                                #endregion

                                #region VALIDANDO LOCAL

                                ImunneVacinas.LocalAdesao novoLocal = LocalAdesao.ConsultarUnico(codigoCampanha, localAdesao, enderecoAdesao);
                                if (novoLocal == null)
                                {
                                    novoLocal = new ImunneVacinas.LocalAdesao();
                                    novoLocal.Campanha.Codigo = codigoCampanha;
                                    novoLocal.Nome = localAdesao;
                                    novoLocal.Endereco = enderecoAdesao;
                                    novoLocal.DataCriacao = DateTime.Now;
                                    resultado = novoLocal.Incluir();
                                }
                                else resultado = novoLocal.Codigo;

                                if (resultado <= 0)
                                {
                                    linhaRetorno = new RetornoProcesso()
                                    {
                                        Classificacao = "ERRO",
                                        Linha = String.Format("LINHA {0}", contador.ToString().PadLeft(2, '0')),
                                        Resumo = String.Format("NÃO FOI POSSÍVEL CADASTRAR O LOCAL {0}.", localAdesao.ToUpper())
                                    };
                                    listaRetorno.Add(linhaRetorno);

                                    continue;
                                }

                                codigoLocal = resultado;

                                #endregion

                                if (codigoParticipante > 0)
                                {
                                    #region VERIFICANDO SE O PARTICIPANTE JÁ TEM ADESÃO NA CAMPANHA

                                    Adesao adesaoParticipante = Adesao.ConsultarUnico(codigoCampanha, codigoParticipante);
                                    if (adesaoParticipante != null)
                                    {
                                        linhaRetorno = new RetornoProcesso()
                                        {
                                            Classificacao = "AVISO",
                                            Linha = String.Format("LINHA {0}", contador.ToString().PadLeft(2, '0')),
                                            Resumo = String.Format("O PARTICIPANTE {0} JÁ REALIZOU A ADESÃO NA CAMPANHA {1}", nomeParticipante, campanhaSelecionada.Identificacao.ToUpper())
                                        };
                                        listaRetorno.Add(linhaRetorno);

                                        continue;
                                    }
                                    else
                                    {
                                        adesaoParticipante = new Adesao();
                                        adesaoParticipante.Campanha.Codigo = codigoCampanha;
                                        adesaoParticipante.Participante.Codigo = codigoParticipante;
                                        adesaoParticipante.DataAdesao = DateTime.Now;
                                        adesaoParticipante.Local.Codigo = codigoLocal;
                                        resultado = adesaoParticipante.Incluir();

                                        if (resultado > 0)
                                        {
                                            linhaRetorno = new RetornoProcesso()
                                            {
                                                Classificacao = "SUCESSO",
                                                Linha = String.Format("LINHA {0}", contador.ToString().PadLeft(2, '0')),
                                                Resumo = String.Format("ADESÃO REALIZADA COM SUCESSO - PARTICIPANTE {0}.", nomeParticipante.ToUpper())
                                            };
                                            listaRetorno.Add(linhaRetorno);
                                        }
                                        else
                                        {
                                            linhaRetorno = new RetornoProcesso()
                                            {
                                                Classificacao = "ERRO",
                                                Linha = String.Format("LINHA {0}", contador.ToString().PadLeft(2, '0')),
                                                Resumo = String.Format("NÃO FOI POSSÍVEL REALIZAR A ADESÃO - PARTICIPANTE {0}.", nomeParticipante.ToUpper())
                                            };
                                            listaRetorno.Add(linhaRetorno);
                                        }

                                    }

                                    #endregion
                                }
                            }
                        }
                    }
                }
                else
                {
                    linhaRetorno = new RetornoProcesso()
                    {
                        Classificacao = "ERRO",
                        Linha = String.Format("LINHA {0}", contador.ToString().PadLeft(2, '0')),
                        Resumo = String.Format("NÃO FOI INFORMADA UMA CAMPANHA PARA CRIAÇÃO DAS ADESÕES.")
                    };
                    listaRetorno.Add(linhaRetorno);
                }
            }
            catch (Exception ex)
            {
                linhaRetorno = new RetornoProcesso()
                {
                    Classificacao = "ERRO",
                    Linha = String.Format("LINHA {0}", contador.ToString().PadLeft(2, '0')),
                    Resumo = String.Format("OCORREU UM ERRO AO PROCESSAR A OPERAÇÃO {0}.", ex.Message)
                };
                listaRetorno.Add(linhaRetorno);
            }

            return listaRetorno;
        }

        /// <summary>
        /// Rotina que processa um DATABLE com os dados de Adesoes a serem alimentados no BD
        /// </summary>
        /// <param name="codigoClinica">VALUE: Código de identificação da clínica (obrigatório)</param>
        /// <param name="dtOrigem">VALUE: DATABLE de origem dos dados (obrigatório)</param>
        /// <returns>LIST Genérico com os retornos de cada linha processada do DATATABLE</returns>
        public static List<ImunneVacinas.RetornoProcesso> ImportarCompras(int codigoClinica, DataTable dtOrigem)
        {
            List<ImunneVacinas.RetornoProcesso> listaRetorno = new List<ImunneVacinas.RetornoProcesso>();
            ImunneVacinas.RetornoProcesso linhaRetorno;

            int contador = 0;

            try
            {
                if (codigoClinica > 0)
                {
                    ImunneVacinas.Clinica clinicaSelecionada = ImunneVacinas.Clinica.ConsultarUnico(codigoClinica);

                    if (dtOrigem != null && dtOrigem.Rows.Count > 0)
                    {
                        foreach (DataRow linha in dtOrigem.Rows)
                        {
                            ImunneVacinas.Empresa empresaProcessada = new ImunneVacinas.Empresa();
                            ImunneVacinas.Participante participanteProcessado = new ImunneVacinas.Participante();

                            string idCampanha = linha[0].ToString().Trim();
                            string cpfTitular = ImunneVacinas.Utils.LimparCpfCnpj(linha[1].ToString().Trim());
                            string nomeTitular = linha[2].ToString().Trim();
                            string nascimentoTitular = linha[4].ToString().Trim();
                            string atendimentoPara = linha[5].ToString().Trim();
                            string qtdePessoas = linha[6].ToString().Trim();
                            int totalQtde = 1;

                            int resultado = -1;
                            int codigoTitular = -1;
                            int codigoCampanha = -1;

                            if (cpfTitular.ToUpper() != "CPF TITULAR")
                            {
                                contador += 1;

                                #region VALIDANDO CAMPOS VAZIOS

                                if (String.IsNullOrEmpty(idCampanha) || idCampanha == "")
                                {
                                    linhaRetorno = new ImunneVacinas.RetornoProcesso()
                                    {
                                        Classificacao = "ERRO",
                                        Linha = String.Format("LINHA {0}", contador.ToString().PadLeft(2, '0')),
                                        Resumo = String.Format("NÃO FOI INFORMADA A CAMPANHA")
                                    };
                                    listaRetorno.Add(linhaRetorno);

                                    continue;
                                }

                                if (String.IsNullOrEmpty(cpfTitular) || cpfTitular == "")
                                {
                                    linhaRetorno = new ImunneVacinas.RetornoProcesso()
                                    {
                                        Classificacao = "ERRO",
                                        Linha = String.Format("LINHA {0}", contador.ToString().PadLeft(2, '0')),
                                        Resumo = String.Format("NÃO FOI INFORMADO O VALOR DO CPF/CNPJ")
                                    };
                                    listaRetorno.Add(linhaRetorno);

                                    continue;
                                }

                                if (String.IsNullOrEmpty(nomeTitular) || nomeTitular == "")
                                {
                                    linhaRetorno = new ImunneVacinas.RetornoProcesso()
                                    {
                                        Classificacao = "ERRO",
                                        Linha = String.Format("LINHA {0}", contador.ToString().PadLeft(2, '0')),
                                        Resumo = String.Format("NÃO FOI O NOME DO PARTICIPANTE")
                                    };
                                    listaRetorno.Add(linhaRetorno);

                                    continue;
                                }

                                if (String.IsNullOrEmpty(atendimentoPara) || atendimentoPara == "")
                                    atendimentoPara = "IMPORTAÇÃO";

                                if (String.IsNullOrEmpty(qtdePessoas) || qtdePessoas == "")
                                {
                                    try
                                    {
                                        totalQtde = Convert.ToInt32(qtdePessoas);
                                    }
                                    catch (Exception ex)
                                    {
                                        totalQtde = 1;
                                        //HttpContext.Current.SalvarException404(ex);
                                    }
                                    finally
                                    {
                                        totalQtde = 1;
                                    }
                                }

                                #endregion

                                #region VALIDANDO CPF/CNPJ

                                if (!ImunneVacinas.Utils.ValidarCpfCnpj(ImunneVacinas.Utils.LimparCpfCnpj(cpfTitular)))
                                {
                                    linhaRetorno = new ImunneVacinas.RetornoProcesso()
                                    {
                                        Classificacao = "ERRO",
                                        Linha = String.Format("LINHA {0}", contador.ToString().PadLeft(2, '0')),
                                        Resumo = String.Format("CPF/CNPJ INFORMADO É INVÁLIDO {0}.", cpfTitular)
                                    };
                                    listaRetorno.Add(linhaRetorno);

                                    continue;
                                }

                                #endregion

                                #region VALIDANDO PARTICIPANTE

                                ImunneVacinas.Participante participante = ImunneVacinas.Participante.ConsultarUnico(ImunneVacinas.Utils.LimparCpfCnpj(cpfTitular));
                                if (participante == null)
                                {
                                    participante = new ImunneVacinas.Participante()
                                    {
                                        Cpf = ImunneVacinas.Utils.LimparCpfCnpj(cpfTitular),
                                        DataCriacao = DateTime.Now,
                                        Email = "",
                                        Nome = nomeTitular
                                    };

                                    if (!String.IsNullOrEmpty(nascimentoTitular) && Convert.ToDateTime(nascimentoTitular) != ImunneVacinas.Utils.dataPadrao)
                                        participante.DataNascimento = Convert.ToDateTime(nascimentoTitular);

                                    codigoTitular = participante.Incluir();
                                }
                                else codigoTitular = participante.Codigo;

                                #endregion

                                if (codigoTitular > 0)
                                {
                                    #region VALIDANDO A CAMPANHA

                                    ImunneVacinas.Campanha campanhaSelecionada = ImunneVacinas.Campanha.ConsultarUnico(idCampanha);
                                    if (campanhaSelecionada == null)
                                    {
                                        linhaRetorno = new ImunneVacinas.RetornoProcesso()
                                        {
                                            Classificacao = "ERRO",
                                            Linha = String.Format("LINHA {0}", contador.ToString().PadLeft(2, '0')),
                                            Resumo = String.Format("NÃO FOI ENCONTRADA CAMPANHA COM A IDENTIFICAÇÃO {0}.", idCampanha)
                                        };
                                        listaRetorno.Add(linhaRetorno);

                                        continue;
                                    }
                                    else codigoCampanha = campanhaSelecionada.Codigo;

                                    #endregion

                                    #region VALIDANDO CAMPANHA PARA A CLÍNICA

                                    ImunneVacinas.CampanhaClinica campanhaClinica = ImunneVacinas.CampanhaClinica.ConsultarUnico(codigoCampanha, codigoClinica);
                                    if (campanhaClinica == null)
                                    {
                                        linhaRetorno = new ImunneVacinas.RetornoProcesso()
                                        {
                                            Classificacao = "ERRO",
                                            Linha = String.Format("LINHA {0}", contador.ToString().PadLeft(2, '0')),
                                            Resumo = String.Format("A CAMPANHA {0} NÃO SE ENCONTRA HABILITADA PARA A CLÍNICA {1}.", idCampanha, clinicaSelecionada.NomeFantasia.ToUpper())
                                        };
                                        listaRetorno.Add(linhaRetorno);

                                        continue;
                                    }

                                    #endregion

                                    #region GERANDO A COMPRA IMPORTADA

                                    ImunneVacinas.Compra novaCompra = new ImunneVacinas.Compra()
                                    {
                                        DataCompra = DateTime.Now,
                                        Parcelas = 1,
                                        Valor = 0,
                                        Situacao = ImunneVacinas.Compra.SituacaoCompra.Aberta,
                                        Categoria = ImunneVacinas.Compra.CategoriaCompra.Importada
                                    };

                                    novaCompra.IdCampanha.Codigo = codigoCampanha;
                                    novaCompra.IdClinica.Codigo = codigoClinica;
                                    novaCompra.IdParticipante.Codigo = codigoTitular;
                                    novaCompra.DataLiberacao = DateTime.Now;

                                    int codigoCompra = novaCompra.Incluir();

                                    #endregion

                                    #region PRODUTOS DA CAMPANHA

                                    decimal totalValorCompra = 0;

                                    ImunneVacinas.PesquisaProdutoCampanha pesquisaProdutos = new ImunneVacinas.PesquisaProdutoCampanha()
                                    {
                                        Campanha = codigoCampanha,
                                        Ordenacao = ImunneVacinas.Utils.TipoOrdenacao.porNome
                                    };

                                    List<ImunneVacinas.ProdutoCampanha> listaProdutos = ImunneVacinas.ProdutoCampanha.ConsultarProdutoCampanhas(pesquisaProdutos);
                                    if (listaProdutos == null || listaProdutos.Count <= 0)
                                    {
                                        linhaRetorno = new ImunneVacinas.RetornoProcesso()
                                        {
                                            Classificacao = "ERRO",
                                            Linha = String.Format("LINHA {0}", contador.ToString().PadLeft(2, '0')),
                                            Resumo = String.Format("A CAMPANHA {0} NÃO POSSUI PRODUTOS VINCULADOS.", "TESTES")
                                        };
                                        listaRetorno.Add(linhaRetorno);

                                        continue;
                                    }
                                    else
                                    {
                                        foreach (ImunneVacinas.ProdutoCampanha produto in listaProdutos)
                                        {
                                            ImunneVacinas.ItemCompra novoItem = new ImunneVacinas.ItemCompra();
                                            novoItem.IdCompra.Codigo = codigoCompra;
                                            novoItem.IdVacina.Codigo = produto.IdVacina.Codigo;
                                            novoItem.DataCriacao = novaCompra.DataCompra;
                                            novoItem.Quantidade = totalQtde;
                                            novoItem.Valor = totalQtde * produto.Preco;

                                            if (!String.IsNullOrEmpty(atendimentoPara))
                                                novoItem.Observacoes = String.Format("COMPRADAS {0} UNIDADE(S) DE {1}. ATENDIMENTO PARA {2}.", totalQtde, produto.IdVacina.Nome.ToUpper(), atendimentoPara.ToUpper());
                                            else
                                                novoItem.Observacoes = String.Format("COMPRADAS {0} UNIDADE(S) DE {1}.", totalQtde, produto.IdVacina.Nome.ToUpper());
                                            novoItem.Incluir();

                                            totalValorCompra += novoItem.Valor;
                                        }

                                        if (codigoCompra > 0)
                                        {
                                            #region ATUALIZANDO A COMPRA COM O VALOR FINAL

                                            novaCompra = ImunneVacinas.Compra.ConsultarUnico(codigoCompra);
                                            novaCompra.Valor = totalValorCompra;
                                            novaCompra.Alterar();

                                            #endregion

                                            #region TRANSAÇÃO

                                            string valorTransacao = ImunneVacinas.Utils.RetornaValorJson(totalValorCompra.ToString());
                                            int valorOficial = Convert.ToInt32(valorTransacao);

                                            ImunneVacinas.TransacaoRealizada novaTransacao = new ImunneVacinas.TransacaoRealizada();
                                            novaTransacao.autorizacao = "IMUNNEVACINAS";
                                            novaTransacao.codigoEstabelecimento = "1550561256295";
                                            novaTransacao.compraTransacao.Codigo = codigoCompra;
                                            novaTransacao.valor = valorOficial;
                                            novaTransacao.numeroComprovanteVenda = "IMPORTADA" + ImunneVacinas.Utils.GerarIdRandomica(10);
                                            novaTransacao.mensagemVenda = "IMPORTADA DIRETAMENTE PELA CLÍNICA";
                                            novaTransacao.Incluir();

                                            #endregion

                                            #region VOUCHER

                                            ImunneVacinas.Voucher voucherSelecionado = ImunneVacinas.Voucher.ConsultarUnico(codigoTitular, codigoClinica, codigoCampanha, codigoCompra);
                                            if (voucherSelecionado == null)
                                            {
                                                StringBuilder descricaoVoucher = new StringBuilder();
                                                descricaoVoucher.AppendFormat("COMPRA IMPORTADA VIA PLATAFORMA. MODALIDADE {0}.\r\n", atendimentoPara);
                                                descricaoVoucher.AppendFormat("{0} ADQUIRIU PRODUTOS DA CAMPANHA {1}", nomeTitular.ToUpper(), campanhaSelecionada.Identificacao.ToString());
                                                descricaoVoucher.AppendFormat(" QUE SERÃO APLICADOS NA CLÍNICA {0}.\r\nABAIXO OS PRODUTOS:\r\n\r\n", clinicaSelecionada.NomeFantasia.ToUpper());

                                                foreach (ImunneVacinas.ProdutoCampanha produto in listaProdutos)
                                                {
                                                    descricaoVoucher.AppendFormat("{0} UNIDADE(S) DE {0}.\r\n", totalQtde, produto.IdVacina.Nome.ToUpper());
                                                }

                                                descricaoVoucher.AppendFormat("\r\nCOMPRAS REALIZADAS POR {0}.", nomeTitular.ToUpper());

                                                ImunneVacinas.Voucher novoVoucher = new ImunneVacinas.Voucher();
                                                novoVoucher.IdCampanha.Codigo = codigoCampanha;
                                                novoVoucher.IdClinica.Codigo = codigoClinica;
                                                novoVoucher.IdCompra.Codigo = codigoCompra;
                                                novoVoucher.IdParticipante.Codigo = codigoTitular;
                                                novoVoucher.DataCriacao = DateTime.Now;
                                                novoVoucher.Descricao = descricaoVoucher.ToString();
                                                novoVoucher.Situacao = ImunneVacinas.Voucher.SituacaoVoucher.Gerado;
                                                novoVoucher.Incluir();
                                            }

                                            #endregion

                                            linhaRetorno = new ImunneVacinas.RetornoProcesso()
                                            {
                                                Classificacao = "SUCESSO",
                                                Linha = String.Format("LINHA {0}", contador.ToString().PadLeft(2, '0')),
                                                Resumo = String.Format("COMPRA DE {0} DA CAMPANHA {1} PROCESSADA COM SUCESSO.", idCampanha, nomeTitular.ToUpper())
                                            };
                                            listaRetorno.Add(linhaRetorno);
                                        }
                                    }

                                    #endregion
                                }
                            }
                        }
                    }
                }
                else
                {
                    linhaRetorno = new ImunneVacinas.RetornoProcesso()
                    {
                        Classificacao = "ERRO",
                        Linha = String.Format("LINHA {0}", contador.ToString().PadLeft(2, '0')),
                        Resumo = String.Format("NÃO FOI INFORMADA UMA CAMPANHA PARA CRIAÇÃO DAS ADESÕES.")
                    };
                    listaRetorno.Add(linhaRetorno);
                }
            }
            catch (Exception ex)
            {
                linhaRetorno = new ImunneVacinas.RetornoProcesso()
                {
                    Classificacao = "ERRO",
                    Linha = String.Format("LINHA {0}", contador.ToString().PadLeft(2, '0')),
                    Resumo = String.Format("OCORREU UM ERRO AO PROCESSAR A OPERAÇÃO {0}.", ex.Message)
                };
                listaRetorno.Add(linhaRetorno);
            }

            return listaRetorno;
        }

        /// <summary>
        /// Rotina que processa um DATABLE com os dados de Adesoes a serem alimentados no BD
        /// </summary>
        /// <param name="codigoClinica">VALUE: Código de identificação da clínica (obrigatório)</param>
        /// <param name="dtOrigem">VALUE: DATABLE de origem dos dados (obrigatório)</param>
        /// <returns>LIST Genérico com os retornos de cada linha processada do DATATABLE</returns>
        public static List<ImunneVacinas.RetornoProcesso> ImportarComprasDiretas(int codigoClinica, DataTable dtOrigem)
        {
            List<ImunneVacinas.RetornoProcesso> listaRetorno = new List<ImunneVacinas.RetornoProcesso>();
            ImunneVacinas.RetornoProcesso linhaRetorno;

            int contador = 0;

            try
            {
                if (codigoClinica > 0)
                {
                    ImunneVacinas.Clinica clinicaSelecionada = ImunneVacinas.Clinica.ConsultarUnico(codigoClinica);

                    if (dtOrigem != null && dtOrigem.Rows.Count > 0)
                    {
                        foreach (DataRow linha in dtOrigem.Rows)
                        {
                            ImunneVacinas.Empresa empresaProcessada = new ImunneVacinas.Empresa();
                            ImunneVacinas.Participante participanteProcessado = new ImunneVacinas.Participante();

                            string idCampanha = linha[0].ToString().Trim();
                            string cpfTitular = ImunneVacinas.Utils.LimparCpfCnpj(linha[1].ToString().Trim());
                            string nomeTitular = linha[2].ToString().Trim();
                            string nascimentoTitular = linha[4].ToString().Trim();
                            string atendimentoPara = linha[5].ToString().Trim();
                            int totalQtde = 1;

                            int codigoTitular = -1;
                            int codigoCampanha = -1;

                            if (cpfTitular.ToUpper() != "CPF TITULAR")
                            {
                                contador += 1;

                                #region VALIDANDO CAMPOS VAZIOS

                                if (String.IsNullOrEmpty(idCampanha) || idCampanha == "")
                                {
                                    linhaRetorno = new ImunneVacinas.RetornoProcesso()
                                    {
                                        Classificacao = "ERRO",
                                        Linha = String.Format("LINHA {0}", contador.ToString().PadLeft(2, '0')),
                                        Resumo = String.Format("NÃO FOI INFORMADA A CAMPANHA")
                                    };
                                    listaRetorno.Add(linhaRetorno);

                                    continue;
                                }

                                if (String.IsNullOrEmpty(nomeTitular) || nomeTitular == "")
                                {
                                    linhaRetorno = new ImunneVacinas.RetornoProcesso()
                                    {
                                        Classificacao = "ERRO",
                                        Linha = String.Format("LINHA {0}", contador.ToString().PadLeft(2, '0')),
                                        Resumo = String.Format("NÃO FOI O NOME DO PARTICIPANTE")
                                    };
                                    listaRetorno.Add(linhaRetorno);

                                    continue;
                                }

                                #endregion

                                #region VALIDANDO CPF/CNPJ

                                if (!String.IsNullOrEmpty(cpfTitular))
                                {
                                    if (!ImunneVacinas.Utils.ValidarCpfCnpj(ImunneVacinas.Utils.LimparCpfCnpj(cpfTitular)))
                                    {
                                        linhaRetorno = new ImunneVacinas.RetornoProcesso()
                                        {
                                            Classificacao = "ERRO",
                                            Linha = String.Format("LINHA {0}", contador.ToString().PadLeft(2, '0')),
                                            Resumo = String.Format("CPF/CNPJ INFORMADO É INVÁLIDO {0}.", cpfTitular)
                                        };
                                        listaRetorno.Add(linhaRetorno);

                                        continue;
                                    }
                                }

                                #endregion

                                #region VALIDANDO PARTICIPANTE

                                ImunneVacinas.Participante participante;

                                if (!String.IsNullOrEmpty(cpfTitular))
                                    participante = ImunneVacinas.Participante.ConsultarUnico(ImunneVacinas.Utils.LimparCpfCnpj(cpfTitular));
                                else
                                    participante = ImunneVacinas.Participante.ConsultarUnico(ImunneVacinas.Utils.LimparCpfCnpj(nomeTitular));

                                if (participante == null)
                                {
                                    participante = new ImunneVacinas.Participante()
                                    {
                                        Cpf = ImunneVacinas.Utils.LimparCpfCnpj(cpfTitular),
                                        DataCriacao = DateTime.Now,
                                        Email = "",
                                        Nome = nomeTitular
                                    };

                                    if (!String.IsNullOrEmpty(nascimentoTitular) && Convert.ToDateTime(nascimentoTitular) != ImunneVacinas.Utils.dataPadrao)
                                        participante.DataNascimento = Convert.ToDateTime(nascimentoTitular);

                                    codigoTitular = participante.Incluir();
                                }
                                else codigoTitular = participante.Codigo;

                                #endregion

                                if (codigoTitular > 0)
                                {
                                    #region VALIDANDO A CAMPANHA

                                    ImunneVacinas.Campanha campanhaSelecionada = ImunneVacinas.Campanha.ConsultarUnico(idCampanha);
                                    if (campanhaSelecionada == null)
                                    {
                                        linhaRetorno = new ImunneVacinas.RetornoProcesso()
                                        {
                                            Classificacao = "ERRO",
                                            Linha = String.Format("LINHA {0}", contador.ToString().PadLeft(2, '0')),
                                            Resumo = String.Format("NÃO FOI ENCONTRADA CAMPANHA COM A IDENTIFICAÇÃO {0}.", idCampanha)
                                        };
                                        listaRetorno.Add(linhaRetorno);

                                        continue;
                                    }
                                    else codigoCampanha = campanhaSelecionada.Codigo;

                                    #endregion

                                    #region VALIDANDO CAMPANHA PARA A CLÍNICA

                                    ImunneVacinas.CampanhaClinica campanhaClinica = ImunneVacinas.CampanhaClinica.ConsultarUnico(codigoCampanha, codigoClinica);
                                    if (campanhaClinica == null)
                                    {
                                        linhaRetorno = new ImunneVacinas.RetornoProcesso()
                                        {
                                            Classificacao = "ERRO",
                                            Linha = String.Format("LINHA {0}", contador.ToString().PadLeft(2, '0')),
                                            Resumo = String.Format("A CAMPANHA {0} NÃO SE ENCONTRA HABILITADA PARA A CLÍNICA {1}.", idCampanha, clinicaSelecionada.NomeFantasia.ToUpper())
                                        };
                                        listaRetorno.Add(linhaRetorno);

                                        continue;
                                    }

                                    #endregion

                                    #region GERANDO A COMPRA IMPORTADA

                                    ImunneVacinas.Compra novaCompra = new ImunneVacinas.Compra()
                                    {
                                        DataCompra = DateTime.Now,
                                        Parcelas = 1,
                                        Valor = 0,
                                        Situacao = ImunneVacinas.Compra.SituacaoCompra.Concluida,
                                        Categoria = ImunneVacinas.Compra.CategoriaCompra.Importada
                                    };

                                    novaCompra.IdCampanha.Codigo = codigoCampanha;
                                    novaCompra.IdClinica.Codigo = codigoClinica;
                                    novaCompra.IdParticipante.Codigo = codigoTitular;
                                    novaCompra.DataLiberacao = DateTime.Now;

                                    int codigoCompra = novaCompra.Incluir();

                                    #endregion

                                    #region PRODUTOS DA CAMPANHA

                                    decimal totalValorCompra = 0;

                                    ImunneVacinas.PesquisaProdutoCampanha pesquisaProdutos = new ImunneVacinas.PesquisaProdutoCampanha()
                                    {
                                        Campanha = codigoCampanha,
                                        Ordenacao = ImunneVacinas.Utils.TipoOrdenacao.porNome
                                    };

                                    List<ImunneVacinas.ProdutoCampanha> listaProdutos = ImunneVacinas.ProdutoCampanha.ConsultarProdutoCampanhas(pesquisaProdutos);
                                    if (listaProdutos == null || listaProdutos.Count <= 0)
                                    {
                                        linhaRetorno = new ImunneVacinas.RetornoProcesso()
                                        {
                                            Classificacao = "ERRO",
                                            Linha = String.Format("LINHA {0}", contador.ToString().PadLeft(2, '0')),
                                            Resumo = String.Format("A CAMPANHA {0} NÃO POSSUI PRODUTOS VINCULADOS.", "TESTES")
                                        };
                                        listaRetorno.Add(linhaRetorno);

                                        continue;
                                    }
                                    else
                                    {
                                        foreach (ImunneVacinas.ProdutoCampanha produto in listaProdutos)
                                        {
                                            ImunneVacinas.ItemCompra novoItem = new ImunneVacinas.ItemCompra();
                                            novoItem.IdCompra.Codigo = codigoCompra;
                                            novoItem.IdVacina.Codigo = produto.IdVacina.Codigo;
                                            novoItem.DataCriacao = novaCompra.DataCompra;
                                            novoItem.Quantidade = totalQtde;
                                            novoItem.Valor = totalQtde * produto.Preco;

                                            if (!String.IsNullOrEmpty(atendimentoPara))
                                                novoItem.Observacoes = String.Format("COMPRADAS {0} UNIDADE(S) DE {1}. ATENDIMENTO PARA {2}.", totalQtde, produto.IdVacina.Nome.ToUpper(), atendimentoPara.ToUpper());
                                            else
                                                novoItem.Observacoes = String.Format("COMPRADAS {0} UNIDADE(S) DE {1}.", totalQtde, produto.IdVacina.Nome.ToUpper());
                                            novoItem.Incluir();

                                            totalValorCompra += novoItem.Valor;
                                        }

                                        if (codigoCompra > 0)
                                        {
                                            #region ATUALIZANDO A COMPRA COM O VALOR FINAL

                                            novaCompra = ImunneVacinas.Compra.ConsultarUnico(codigoCompra);
                                            novaCompra.Valor = totalValorCompra;
                                            novaCompra.Alterar();

                                            #endregion

                                            #region TRANSAÇÃO

                                            string valorTransacao = ImunneVacinas.Utils.RetornaValorJson(totalValorCompra.ToString());
                                            int valorOficial = Convert.ToInt32(valorTransacao);

                                            ImunneVacinas.TransacaoRealizada novaTransacao = new ImunneVacinas.TransacaoRealizada();
                                            novaTransacao.autorizacao = "IMUNNEVACINAS";
                                            novaTransacao.codigoEstabelecimento = "1550561256295";
                                            novaTransacao.compraTransacao.Codigo = codigoCompra;
                                            novaTransacao.valor = valorOficial;
                                            novaTransacao.numeroComprovanteVenda = "IMPORTADA" + ImunneVacinas.Utils.GerarIdRandomica(10);
                                            novaTransacao.mensagemVenda = "IMPORTADA DIRETAMENTE PELA CLÍNICA";
                                            novaTransacao.Incluir();

                                            #endregion

                                            #region VOUCHER

                                            ImunneVacinas.Voucher voucherSelecionado = ImunneVacinas.Voucher.ConsultarUnico(codigoTitular, codigoClinica, codigoCampanha, codigoCompra);
                                            if (voucherSelecionado == null)
                                            {
                                                StringBuilder descricaoVoucher = new StringBuilder();

                                                //descricaoVoucher.AppendFormat("COMPRA IMPORTADA VIA PLATAFORMA.\r\n");
                                                //descricaoVoucher.AppendFormat("{0} ADQUIRIU PRODUTOS DA CAMPANHA {1}", item.Nome.ToUpper(), campanhaSelecionada.Identificacao.ToString());
                                                //descricaoVoucher.AppendFormat(" QUE SERÃO APLICADOS NA CLÍNICA {0}.\r\nABAIXO OS PRODUTOS:\r\n\r\n", clinicaSelecionada.NomeFantasia.ToUpper());

                                                //foreach (ImunneVacinas.ProdutoCampanha produto in listaProdutos)
                                                //{
                                                //    descricaoVoucher.AppendFormat("{0} UNIDADE(S) DE {0}.\r\n", "1", produto.IdVacina.Nome.ToUpper());
                                                //}

                                                //descricaoVoucher.AppendFormat("\r\nCOMPRAS REALIZADAS POR {0}.", item.Nome.ToUpper());

                                                descricaoVoucher.AppendFormat("AQUISIÇÃO VIA COMPRA IMPORTADA - {0}.", atendimentoPara);

                                                ImunneVacinas.Voucher novoVoucher = new ImunneVacinas.Voucher();
                                                novoVoucher.IdCampanha.Codigo = codigoCampanha;
                                                novoVoucher.IdClinica.Codigo = codigoClinica;
                                                novoVoucher.IdCompra.Codigo = codigoCompra;
                                                novoVoucher.IdParticipante.Codigo = codigoTitular;
                                                novoVoucher.DataCriacao = DateTime.Now;
                                                novoVoucher.Descricao = descricaoVoucher.ToString();
                                                novoVoucher.Situacao = ImunneVacinas.Voucher.SituacaoVoucher.Gerado;
                                                novoVoucher.Incluir();
                                            }

                                            #endregion

                                            linhaRetorno = new ImunneVacinas.RetornoProcesso()
                                            {
                                                Classificacao = "SUCESSO",
                                                Linha = String.Format("LINHA {0}", contador.ToString().PadLeft(2, '0')),
                                                Resumo = String.Format("COMPRA DA CAMPANHA <b>{0}</b> PROCESSADA COM SUCESSO EM NOME DE {1}. COMPROVANTE <b>{2}</b>.", idCampanha, nomeTitular.ToUpper(), novaTransacao.numeroComprovanteVenda)
                                            };
                                            listaRetorno.Add(linhaRetorno);
                                        }
                                    }

                                    #endregion
                                }
                            }
                        }
                    }
                }
                else
                {
                    linhaRetorno = new ImunneVacinas.RetornoProcesso()
                    {
                        Classificacao = "ERRO",
                        Linha = String.Format("LINHA {0}", contador.ToString().PadLeft(2, '0')),
                        Resumo = String.Format("NÃO FOI INFORMADA UMA CAMPANHA PARA CRIAÇÃO DAS ADESÕES.")
                    };
                    listaRetorno.Add(linhaRetorno);
                }
            }
            catch (Exception ex)
            {
                linhaRetorno = new ImunneVacinas.RetornoProcesso()
                {
                    Classificacao = "ERRO",
                    Linha = String.Format("LINHA {0}", contador.ToString().PadLeft(2, '0')),
                    Resumo = String.Format("OCORREU UM ERRO AO PROCESSAR A OPERAÇÃO {0}.", ex.Message)
                };
                listaRetorno.Add(linhaRetorno);
            }

            return listaRetorno;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="codigoClinica"></param>
        /// <param name="dtOrigem"></param>
        public static List<ImunneVacinas.RetornoProcesso> ImportarComprasAgrupadas(int codigoClinica, DataTable dtOrigem)
        {
            List<ImunneVacinas.RetornoProcesso> listaRetorno = new List<ImunneVacinas.RetornoProcesso>();
            ImunneVacinas.RetornoProcesso linhaRetorno;

            int contador = 0;
            int titular = 0;
            int codigoCampanha = -1;

            try
            {
                if (codigoClinica > 0)
                {
                    ImunneVacinas.Clinica clinicaSelecionada = ImunneVacinas.Clinica.ConsultarUnico(codigoClinica);

                    List<ImunneVacinas.TitularCompra> pessoasCompras = new List<TitularCompra>();
                    List<ImunneVacinas.DependenteCompra> dependentesCompras = new List<DependenteCompra>();

                    // Criando a lista de compras, com dados dos titular e dependentes
                    List<ImunneVacinas.TitularCompra> titularesCompras = new List<TitularCompra>();
                    string cpfPrincipal = String.Empty;

                    if (dtOrigem != null && dtOrigem.Rows.Count > 0)
                    {
                        foreach (DataRow linha in dtOrigem.Rows)
                        {
                            if (contador > 0)
                            {
                                ImunneVacinas.Empresa empresaProcessada = new ImunneVacinas.Empresa();
                                ImunneVacinas.Participante participanteProcessado = new ImunneVacinas.Participante();

                                string idCampanha = linha[0].ToString().Trim();
                                string cpfTitular = ImunneVacinas.Utils.LimparCpfCnpj(linha[1].ToString().Trim());
                                string nomeTitular = linha[2].ToString().Trim();
                                string nascimentoTitular = linha[4].ToString().Trim();
                                string atendimentoPara = linha[5].ToString().Trim();

                                ImunneVacinas.TitularCompra itemTitular = new TitularCompra();

                                /// Se titular, iniciar a lista
                                if (atendimentoPara.ToUpper() == "TITULAR")
                                {
                                    #region VALIDAÇÃO DA CAMPANHA

                                    if (String.IsNullOrEmpty(idCampanha) || idCampanha == "")
                                    {
                                        linhaRetorno = new ImunneVacinas.RetornoProcesso()
                                        {
                                            Classificacao = "ERRO",
                                            Linha = String.Format("LINHA {0}", titular.ToString().PadLeft(2, '0')),
                                            Resumo = String.Format("NÃO FOI INFORMADA A CAMPANHA")
                                        };
                                        listaRetorno.Add(linhaRetorno);

                                        continue;
                                    }
                                    else
                                    {
                                        #region VERIFICANDO SE CAMPANHA EXISTE CAMPANHA

                                        ImunneVacinas.Campanha campanhaSelecionada = ImunneVacinas.Campanha.ConsultarUnico(idCampanha);
                                        if (campanhaSelecionada == null)
                                        {
                                            linhaRetorno = new ImunneVacinas.RetornoProcesso()
                                            {
                                                Classificacao = "ERRO",
                                                Linha = String.Format("LINHA {0}", titular.ToString().PadLeft(2, '0')),
                                                Resumo = String.Format("NÃO FOI ENCONTRADA CAMPANHA COM A IDENTIFICAÇÃO <b>{0}</b>.", idCampanha)
                                            };
                                            listaRetorno.Add(linhaRetorno);

                                            continue;
                                        }
                                        else codigoCampanha = campanhaSelecionada.Codigo;

                                        #endregion

                                        #region VERIFICANDO SE CAMPANHA EXISTE PARA A CLÍNICA

                                        ImunneVacinas.CampanhaClinica campanhaClinica = ImunneVacinas.CampanhaClinica.ConsultarUnico(codigoCampanha, codigoClinica);
                                        if (campanhaClinica == null)
                                        {
                                            linhaRetorno = new ImunneVacinas.RetornoProcesso()
                                            {
                                                Classificacao = "ERRO",
                                                Linha = String.Format("LINHA {0}", titular.ToString().PadLeft(2, '0')),
                                                Resumo = String.Format("A CAMPANHA {0} NÃO SE ENCONTRA HABILITADA PARA A CLÍNICA {1}.", idCampanha, clinicaSelecionada.NomeFantasia.ToUpper())
                                            };
                                            listaRetorno.Add(linhaRetorno);

                                            continue;
                                        }

                                        #endregion
                                    }

                                    #endregion

                                    #region VALIDAÇÃO DO NOME

                                    if (String.IsNullOrEmpty(nomeTitular) || nomeTitular == "")
                                    {
                                        linhaRetorno = new ImunneVacinas.RetornoProcesso()
                                        {
                                            Classificacao = "ERRO",
                                            Linha = String.Format("LINHA {0}", contador.ToString().PadLeft(2, '0')),
                                            Resumo = String.Format("NÃO FOI INFORMADO O NOME DO TITULAR")
                                        };
                                        listaRetorno.Add(linhaRetorno);

                                        continue;
                                    }

                                    #endregion

                                    #region VALIDAÇÃO DO CPF

                                    // 04/04/2020 - GUILHERME PETRACA
                                    // SE EXISTIR INFORMAÇÃO NO CAMPO CPF, VALIDAMOS A EXISTÊNCIA DO MESMO
                                    // CASO NÃO EXISTE, CRIAMOS A CHAVE USANDO O NOME DO MESMO, COM A CLÍNICA E A
                                    // CAMPANHA. (CODCLINICA, CODCAMPANHA E NUMÉRICO DO NOME)

                                    if (String.IsNullOrEmpty(cpfTitular))
                                        cpfTitular = String.Format("{0}{1}", codigoCampanha, Utils.RetornaValorNome(nomeTitular));
                                    else
                                    {
                                        // SOMENTE A VALIDAÇÃO SE EXISTIR CPF INFORMADO
                                        if (!Utils.ValidarCpfCnpj(Utils.LimparCpfCnpj(cpfTitular)))
                                        {
                                            linhaRetorno = new ImunneVacinas.RetornoProcesso()
                                            {
                                                Classificacao = "ERRO",
                                                Linha = String.Format("LINHA {0}", contador.ToString().PadLeft(2, '0')),
                                                Resumo = String.Format("O CPF INFORMADO {0} NÃO É VÁLIDO.", cpfTitular)
                                            };
                                            listaRetorno.Add(linhaRetorno);
                                        }
                                    }

                                    #region DESUSO

                                    //linhaRetorno = new ImunneVacinas.RetornoProcesso()
                                    //{
                                    //    Classificacao = "ERRO",
                                    //    Linha = String.Format("LINHA {0}", contador.ToString().PadLeft(2, '0')),
                                    //    Resumo = String.Format("NÃO FOI INFORMADA A IDENTIFICAÇÃO (CPF/MATRICULA) DO TITULAR.", cpfTitular)
                                    //};
                                    //listaRetorno.Add(linhaRetorno);

                                    //continue;

                                    #endregion

                                    #endregion

                                    #region ATRIBUÇÃO DE VALORES DOS TITULARES

                                    cpfPrincipal = cpfTitular;
                                    itemTitular.idCampanha = idCampanha;
                                    itemTitular.Cpf = cpfPrincipal;
                                    itemTitular.Nome = nomeTitular;
                                    itemTitular.Nascimento = nascimentoTitular;
                                    cpfPrincipal = cpfTitular;
                                    titularesCompras.Add(itemTitular);

                                    #endregion

                                }
                                else
                                {
                                    ImunneVacinas.DependenteCompra itemDependente = new DependenteCompra();

                                    itemDependente.idCampanha = idCampanha;
                                    itemDependente.CpfTitular = cpfPrincipal;
                                    itemDependente.Cpf = cpfTitular;
                                    itemDependente.Nome = nomeTitular;
                                    itemDependente.Nascimento = nascimentoTitular;
                                    dependentesCompras.Add(itemDependente);
                                }
                            }

                            contador += 1;
                        }
                    }

                    /// Leitura
                    foreach (ImunneVacinas.TitularCompra item in titularesCompras)
                    {
                        titular += 1;

                        int codigoTitular = -1;

                        #region VALIDANDO PARTICIPANTE

                        ImunneVacinas.Participante participante = ImunneVacinas.Participante.ConsultarUnico(ImunneVacinas.Utils.LimparCpfCnpj(item.Cpf));
                        if (participante == null)
                        {
                            participante = new ImunneVacinas.Participante()
                            {
                                Cpf = ImunneVacinas.Utils.LimparCpfCnpj(item.Cpf),
                                DataCriacao = DateTime.Now,
                                Email = "",
                                Nome = item.Nome
                            };

                            if (!String.IsNullOrEmpty(item.Nascimento) && Convert.ToDateTime(item.Nascimento) != ImunneVacinas.Utils.dataPadrao)
                                participante.DataNascimento = Convert.ToDateTime(item.Nascimento);

                            codigoTitular = participante.Incluir();
                        }
                        else codigoTitular = participante.Codigo;

                        #endregion

                        if (codigoTitular > 0)
                        {
                            #region GERANDO A COMPRA IMPORTADA

                            ImunneVacinas.Compra novaCompra = new ImunneVacinas.Compra()
                            {
                                DataCompra = DateTime.Now,
                                Parcelas = 1,
                                Valor = 0,
                                Situacao = ImunneVacinas.Compra.SituacaoCompra.Concluida,
                                Categoria = ImunneVacinas.Compra.CategoriaCompra.Importada
                            };

                            novaCompra.IdCampanha.Codigo = codigoCampanha;
                            novaCompra.IdClinica.Codigo = codigoClinica;
                            novaCompra.IdParticipante.Codigo = codigoTitular;
                            novaCompra.DataLiberacao = DateTime.Now;


                            int codigoCompra = novaCompra.Incluir();

                            #endregion

                            #region PRODUTOS DA CAMPANHA

                            decimal totalValorCompra = 0;

                            ImunneVacinas.PesquisaProdutoCampanha pesquisaProdutos = new ImunneVacinas.PesquisaProdutoCampanha()
                            {
                                Campanha = codigoCampanha,
                                Ordenacao = ImunneVacinas.Utils.TipoOrdenacao.porNome
                            };

                            List<ImunneVacinas.ProdutoCampanha> listaProdutos = ImunneVacinas.ProdutoCampanha.ConsultarProdutoCampanhas(pesquisaProdutos);
                            if (listaProdutos == null || listaProdutos.Count <= 0)
                            {
                                linhaRetorno = new ImunneVacinas.RetornoProcesso()
                                {
                                    Classificacao = "ERRO",
                                    Linha = String.Format("LINHA {0}", titular.ToString().PadLeft(2, '0')),
                                    Resumo = String.Format("A CAMPANHA {0} NÃO POSSUI PRODUTOS VINCULADOS.", "TESTES")
                                };
                                listaRetorno.Add(linhaRetorno);

                                continue;
                            }

                            #endregion

                            if (codigoCompra > 0)
                            {
                                int totalErros = 0;
                                int totalItens = 0;

                                #region VOUCHER PESSOAL

                                StringBuilder descricaoVoucher = new StringBuilder();

                                descricaoVoucher.AppendFormat("AQUISIÇÃO VIA COMPRA IMPORTADA - {0}.", "TITULAR");

                                ImunneVacinas.Voucher novoVoucher = new ImunneVacinas.Voucher();
                                novoVoucher.IdCampanha.Codigo = codigoCampanha;
                                novoVoucher.IdClinica.Codigo = codigoClinica;
                                novoVoucher.IdCompra.Codigo = codigoCompra;
                                novoVoucher.IdParticipante.Codigo = codigoTitular;
                                novoVoucher.DataCriacao = DateTime.Now;
                                novoVoucher.Descricao = descricaoVoucher.ToString();
                                novoVoucher.Situacao = ImunneVacinas.Voucher.SituacaoVoucher.Gerado;
                                novoVoucher.Incluir();

                                #endregion

                                #region GERANDO OS VOUCHERS DE CADA UM

                                foreach (ImunneVacinas.DependenteCompra subItem in dependentesCompras)
                                {
                                    int codigoDependente = -1;

                                    if (subItem.CpfTitular == item.Cpf && subItem.idCampanha == item.idCampanha)
                                    {
                                        ImunneVacinas.Participante dependente;

                                        dependente = ImunneVacinas.Participante.ConsultarUnico(codigoTitular, ImunneVacinas.Utils.LimparCpfCnpj(subItem.Nome));

                                        if (dependente == null)
                                        {
                                            dependente = new ImunneVacinas.Participante()
                                            {
                                                Cpf = String.Empty,
                                                DataCriacao = DateTime.Now,
                                                Email = "",
                                                ParticipantePai = codigoTitular,
                                                Nome = subItem.Nome
                                            };

                                            try
                                            {
                                                if (!String.IsNullOrEmpty(subItem.Nascimento) && Convert.ToDateTime(subItem.Nascimento) != ImunneVacinas.Utils.dataPadrao)
                                                    dependente.DataNascimento = Convert.ToDateTime(subItem.Nascimento);
                                            }
                                            catch (Exception ex)
                                            {
                                                dependente.DataNascimento = ImunneVacinas.Utils.dataPadrao;
                                            }

                                            codigoDependente = dependente.Incluir();
                                        }
                                        else codigoDependente = dependente.Codigo;

                                        if (codigoDependente > 0)
                                        {
                                            totalItens += 1;
                                            ImunneVacinas.Voucher voucherSelecionado = ImunneVacinas.Voucher.ConsultarUnico(codigoDependente, codigoClinica, codigoCampanha, codigoCompra);
                                            if (voucherSelecionado == null)
                                            {
                                                descricaoVoucher = new StringBuilder();

                                                //descricaoVoucher.AppendFormat("COMPRA IMPORTADA VIA PLATAFORMA.\r\n");
                                                //descricaoVoucher.AppendFormat("{0} ADQUIRIU PRODUTOS DA CAMPANHA {1}", item.Nome.ToUpper(), campanhaSelecionada.Identificacao.ToString());
                                                //descricaoVoucher.AppendFormat(" QUE SERÃO APLICADOS NA CLÍNICA {0}.\r\nABAIXO OS PRODUTOS:\r\n\r\n", clinicaSelecionada.NomeFantasia.ToUpper());

                                                //foreach (ImunneVacinas.ProdutoCampanha produto in listaProdutos)
                                                //{
                                                //    descricaoVoucher.AppendFormat("{0} UNIDADE(S) DE {0}.\r\n", "1", produto.IdVacina.Nome.ToUpper());
                                                //}

                                                //descricaoVoucher.AppendFormat("\r\nCOMPRAS REALIZADAS POR {0}.", item.Nome.ToUpper());

                                                descricaoVoucher.AppendFormat("AQUISIÇÃO VIA COMPRA IMPORTADA - {0}.", "DEPENDENTE");

                                                novoVoucher = new ImunneVacinas.Voucher();
                                                novoVoucher.IdCampanha.Codigo = codigoCampanha;
                                                novoVoucher.IdClinica.Codigo = codigoClinica;
                                                novoVoucher.IdCompra.Codigo = codigoCompra;
                                                novoVoucher.IdParticipante.Codigo = codigoDependente;
                                                novoVoucher.DataCriacao = DateTime.Now;
                                                novoVoucher.Descricao = descricaoVoucher.ToString();
                                                novoVoucher.Situacao = ImunneVacinas.Voucher.SituacaoVoucher.Gerado;
                                                novoVoucher.Incluir();
                                            }
                                        }
                                        else totalErros += 1;
                                    }
                                }


                                #endregion

                                #region GERANDO ITENS DA COMPRA

                                int totalGeral = (totalItens + totalErros) + 1;

                                foreach (ImunneVacinas.ProdutoCampanha produto in listaProdutos)
                                {
                                    ImunneVacinas.ItemCompra novoItem = new ImunneVacinas.ItemCompra();
                                    novoItem.IdCompra.Codigo = codigoCompra;
                                    novoItem.IdVacina.Codigo = produto.IdVacina.Codigo;
                                    novoItem.DataCriacao = novaCompra.DataCompra;
                                    novoItem.Quantidade = (totalItens + 1);
                                    novoItem.Valor = (totalGeral) * produto.Preco;
                                    novoItem.Observacoes = String.Format("COMPRADAS {0} UNIDADE(S) DE {1}.", (totalGeral), produto.IdVacina.Nome.ToUpper());

                                    novoItem.Incluir();

                                    totalValorCompra += novoItem.Valor;
                                }

                                if (totalValorCompra <= 0)
                                {
                                    linhaRetorno = new ImunneVacinas.RetornoProcesso()
                                    {
                                        Classificacao = "ERRO",
                                        Linha = String.Format("LINHA {0}", titular.ToString().PadLeft(2, '0')),
                                        Resumo = String.Format("INFELIZMENTE NÃO FOI POSSÍVEL CONCLUIR A COMPRA. TITULAR {0}", item.Nome.ToUpper())
                                    };
                                    listaRetorno.Add(linhaRetorno);

                                    novaCompra.Excluir();

                                    continue;
                                }

                                #endregion

                                #region TRANSAÇÃO

                                string valorTransacao = ImunneVacinas.Utils.RetornaValorJson(totalValorCompra.ToString());
                                int valorOficial = Convert.ToInt32(valorTransacao);

                                ImunneVacinas.TransacaoRealizada novaTransacao = new ImunneVacinas.TransacaoRealizada();
                                novaTransacao.autorizacao = "IMUNNEVACINAS";
                                novaTransacao.codigoEstabelecimento = "1550561256295";
                                novaTransacao.compraTransacao.Codigo = codigoCompra;
                                novaTransacao.valor = valorOficial;
                                novaTransacao.numeroComprovanteVenda = "IMPORTADA" + ImunneVacinas.Utils.GerarIdRandomica(10);
                                novaTransacao.mensagemVenda = "IMPORTADA DIRETAMENTE PELA CLÍNICA";
                                novaTransacao.Incluir();

                                #endregion

                                #region ATUALIZANDO A COMPRA COM O VALOR FINAL

                                novaCompra = ImunneVacinas.Compra.ConsultarUnico(codigoCompra);
                                novaCompra.Valor = totalValorCompra;


                                novaCompra.Alterar();

                                #endregion

                                // RETORNO
                                linhaRetorno = new ImunneVacinas.RetornoProcesso()
                                {
                                    Classificacao = "SUCESSO",
                                    Linha = String.Format("LINHA {0}", titular.ToString().PadLeft(2, '0')),
                                    Resumo = String.Format("COMPRA DA CAMPANHA <b>{0}</b> PROCESSADA COM SUCESSO EM NOME DE {1}. COMPROVANTE <b>{2}</b>.", item.idCampanha, item.Nome.ToUpper(), novaTransacao.numeroComprovanteVenda)
                                };
                                listaRetorno.Add(linhaRetorno);
                            }
                            else
                            {
                                linhaRetorno = new ImunneVacinas.RetornoProcesso()
                                {
                                    Classificacao = "ERRO",
                                    Linha = String.Format("LINHA {0}", titular.ToString().PadLeft(2, '0')),
                                    Resumo = String.Format("A COMPRA NÃO PODE SER GERADA CORRETAMENTE. TENTE NOVAMENTE OU INFORME O SUPORTE.")
                                };
                                listaRetorno.Add(linhaRetorno);
                            }
                        }
                    }
                }
                else
                {
                    linhaRetorno = new ImunneVacinas.RetornoProcesso()
                    {
                        Classificacao = "ERRO",
                        Linha = String.Format("LINHA {0}", titular.ToString().PadLeft(2, '0')),
                        Resumo = String.Format("NÃO FOI INFORMADA UMA CAMPANHA PARA CRIAÇÃO DAS ADESÕES.")
                    };
                    listaRetorno.Add(linhaRetorno);
                }
            }
            catch (Exception ex)
            {
                linhaRetorno = new ImunneVacinas.RetornoProcesso()
                {
                    Classificacao = "ERRO",
                    Linha = String.Format("LINHA {0}", titular.ToString().PadLeft(2, '0')),
                    Resumo = String.Format("OCORREU UM ERRO AO PROCESSAR A OPERAÇÃO {0}.", ex.Message)
                };
                listaRetorno.Add(linhaRetorno);
            }

            return listaRetorno;
        }
    }
}