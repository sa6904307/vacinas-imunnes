﻿using MySql.Data.MySqlClient;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace ImunneVacinas
{
    /// <summary>
    /// Descrição resumida de LocaisAdesoes
    /// </summary>
    public class LocalAdesao
    {
        private static string _tabela = "LocaisAdesoes";
        private static List<BD.Columns> columns;
        private static DateTime columnsUpdated;

        #region PROPRIEDADES

        private Campanha _campanha;
        private int _codigo;
        private string _nome;
        private string _endereco;
        private DateTime _dataCriacao;
        private DateTime _dataAlteracao;

        public Campanha Campanha
        {
            get
            {
                return _campanha;
            }

            set
            {
                _campanha = value;
            }
        }

        public int Codigo
        {
            get
            {
                return _codigo;
            }

            set
            {
                _codigo = value;
            }
        }

        public string Nome
        {
            get
            {
                return _nome;
            }

            set
            {
                _nome = value;
            }
        }

        public string Endereco
        {
            get
            {
                return _endereco;
            }

            set
            {
                _endereco = value;
            }
        }

        public DateTime DataCriacao
        {
            get
            {
                return _dataCriacao;
            }

            set
            {
                _dataCriacao = value;
            }
        }

        public DateTime DataAlteracao
        {
            get
            {
                return _dataAlteracao;
            }

            set
            {
                _dataAlteracao = value;
            }
        }

        #endregion

        #region GETINFO

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos os campos da tabela.
        /// </summary>
        private static void GetInfoTabela()
        {
            if (columns == null || (DateTime.Now - columnsUpdated).Minutes > 2)
            {
                columns = BD.Columns.getColumnsInfo(_tabela);
                columnsUpdated = DateTime.Now;
            }
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo campanha.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoCampanha()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "campanha");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo codigo.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoCodigo()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "codigo");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo nome.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoNome()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "nome");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo endereco.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoEndereco()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "endereco");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo datacriacao.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoDataCriacao()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "datacriacao");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo dataalteracao.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoDataAlteracao()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "dataalteracao");
        }

        #endregion

        /// <summary> 
        /// Rotina que cria um Array com os parâmetros utilizados nas ações de INSERT, UPDATE e DELETE
        /// <returns>Array com vários MySqlParameters</returns>
        /// </summary>
        private Array GetParametrosSQL()
        {
            MySqlParameter p;
            ArrayList ap = new ArrayList();

            p = new MySqlParameter("@campanha", Parser.parseInteiro(_campanha.Codigo));
            p.MySqlDbType = GetInfoCampanha().Type;
            ap.Add(p);

            p = new MySqlParameter("@codigo", Parser.parseInteiro(_codigo));
            p.MySqlDbType = GetInfoCodigo().Type;
            ap.Add(p);

            p = new MySqlParameter("@nome", Parser.parseString(_nome));
            p.MySqlDbType = GetInfoNome().Type;
            ap.Add(p);

            p = new MySqlParameter("@endereco", Parser.parseString(_endereco));
            p.MySqlDbType = GetInfoEndereco().Type;
            ap.Add(p);

            p = new MySqlParameter("@datacriacao", Parser.parseDateTimeBD(_dataCriacao));
            p.MySqlDbType = GetInfoDataCriacao().Type;
            ap.Add(p);

            p = new MySqlParameter("@dataalteracao", Parser.parseDateTimeBD(_dataAlteracao));
            p.MySqlDbType = GetInfoDataAlteracao().Type;
            ap.Add(p);

            return ap.ToArray();
        }

        /// <summary>
        /// Inicialização das propriedades do elemento com seus valores padrão
        /// </summary>
        private void IniciarElemento()
        {
            GetInfoTabela();
            _campanha = new Campanha();

            _campanha.Codigo = -1;
            _nome = String.Empty;
            _endereco = String.Empty;
            _dataCriacao = Utils.dataPadrao;
            _dataAlteracao = Utils.dataPadrao;
        }

        /// <summary>
        /// Inicialização do elemento
        /// </summary>
        public LocalAdesao()
        {
            IniciarElemento();
        }

        /// <summary>
        /// Rotina que insere o registro no BD
        /// </summary>
        /// <returns>Código do registro inserido no BD</returns>
        public int Incluir()
        {
            if (_campanha.Codigo <= 0 || String.IsNullOrEmpty(_endereco) || String.IsNullOrEmpty(_nome))
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    sql.Parameters.AddRange(GetParametrosSQL());
                    sql.CommandText = "INSERT INTO LocaisAdesoes "
                                    + "(campanha, nome, endereco, datacriacao, dataalteracao) "
                                    + "VALUE (@campanha, @nome, @endereco, @datacriacao, @dataalteracao); "
                                    + "SELECT LAST_INSERT_ID();";
                    int resultado = Parser.parseInteiro(new Data().executeEscalar(sql));
                    return resultado;
                }
            }
            catch (ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Rotina que altera os dados do registro no BD
        /// </summary>
        /// <returns>Quantidade de linhas alteradas na ação junto ao BD</returns>
        public int Alterar()
        {
            if (_codigo <= 0 || String.IsNullOrEmpty(_endereco) || String.IsNullOrEmpty(_nome))
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    sql.Parameters.AddRange(GetParametrosSQL());
                    sql.CommandText = "UPDATE LocaisAdesoes SET "
                                    + "campanha = @campanha, nome = @nome, endereco = @endereco, "
                                    + "dataalteracao = @dataalteracao "
                                    + "WHERE codigo = @codigo";
                    int resultado = Parser.parseInteiro(new Data().execute(sql));
                    return resultado;
                }
            }
            catch (ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Rotina que exclui o registro do BD
        /// </summary>
        /// <returns>Quantidade de linhas alteradas na ação junto ao BD</returns>
        public int Excluir()
        {
            if (_codigo < 0)
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    sql.Parameters.AddRange(GetParametrosSQL());
                    sql.CommandText = "DELETE FROM LocaisAdesoes "
                                    + "WHERE codigo = @codigo";
                    int resultado = Parser.parseInteiro(new Data().execute(sql));
                    return resultado;
                }
            }
            catch (ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Rotina que localiza e retorna um único registro, mediante o código informado
        /// </summary>
        /// <param name="codigo">VALUE: código de identificação do registro (obrigatório)</param>
        /// <returns>Elemento Usuario com os dados retornados da consulta</returns>
        public static LocalAdesao ConsultarUnico(int codigo)
        {
            if (codigo < 0)
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            PesquisaLocalAdesao pesquisa = new PesquisaLocalAdesao()
            {
                Codigo = codigo
            };

            List<LocalAdesao> retorno = ConsultaGenerica(pesquisa);
            if (retorno == null || retorno.Count.Equals(0))
                return null;
            else
                return retorno[0];
        }

        /// <summary>
        /// Rotina que localiza e retorna um único registro, mediante os parâmetros informados
        /// </summary>
        /// <param name="campanha">VALUE: código de identificação da campanha (obrigatório)</param>
        /// <param name="local">VALUE: nome do local da adesão (obrigatório)</param>
        /// <returns>Elemento Usuario com os dados retornados da consulta</returns>
        public static LocalAdesao ConsultarUnico(int campanha, string local, string endereco)
        {
            if (campanha < 0 || String.IsNullOrEmpty(local) || String.IsNullOrEmpty(endereco))
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            PesquisaLocalAdesao pesquisa = new PesquisaLocalAdesao()
            {
                Campanha = campanha,
                Local = local,
                Endereco = endereco
            };

            List<LocalAdesao> retorno = ConsultaGenerica(pesquisa);
            if (retorno == null || retorno.Count.Equals(0))
                return null;
            else
                return retorno[0];
        }

        /// <summary>
        /// Rotina que realiza a pesquisa de elementos mediante os parametros de pesquisa informados
        /// </summary>
        /// <param name="pesquisa">VALUE: elemento de pesquisa com os parametros para consulta</param>
        /// <returns></returns>
        public static List<LocalAdesao> ConsultarLocaisAdesoes(PesquisaLocalAdesao pesquisa)
        {
            return ConsultaGenerica(pesquisa);
        }

        /// <summary>
        /// Rotina interna utilizada para todas as pesquisas do elemento
        /// </summary>
        /// <param name="pesquisa">VALUE: Elemento com os parâmetros com os quais será realizada a pesquisa</param>
        /// <returns>Lista do elemento com os dados retornados da consulta</returns>
        internal static List<LocalAdesao> ConsultaGenerica(PesquisaLocalAdesao pesquisa)
        {
            StringBuilder ordenacao = new StringBuilder();
            StringBuilder filtro = new StringBuilder();
            StringBuilder top = new StringBuilder();
            MySqlParameter p;

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    if (pesquisa.Quantidade > 0)
                        top.AppendFormat(" LIMIT {0} ", pesquisa.Quantidade);

                    #region CAMPO CÓDIGO

                    if (pesquisa.Codigo >= 0)
                    {
                        filtro.Append(" AND LocaisAdesoes.codigo = @codigo");
                        p = new MySqlParameter("@codigo", pesquisa.Codigo);
                        p.MySqlDbType = GetInfoCodigo().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region CAMPO CAMPANHA

                    if (pesquisa.Campanha >= 0)
                    {
                        filtro.Append(" AND LocaisAdesoes.campanha = @campanha");
                        p = new MySqlParameter("@campanha", pesquisa.Campanha);
                        p.MySqlDbType = GetInfoCampanha().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region CAMPO LOCAL

                    if (!String.IsNullOrEmpty(pesquisa.Local))
                    {
                        string local = String.Format("{0}", pesquisa.Local.Trim());
                        filtro.Append(" AND LocaisAdesoes.nome = @local ");
                        p = new MySqlParameter("@local", local);
                        p.MySqlDbType = MySqlDbType.VarChar;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region CAMPO ENDEREÇO

                    if (!String.IsNullOrEmpty(pesquisa.Endereco))
                    {
                        string endereco = String.Format("{0}", pesquisa.Endereco.Trim());
                        filtro.Append(" AND LocaisAdesoes.endereco = @endereco ");
                        p = new MySqlParameter("@endereco", endereco);
                        p.MySqlDbType = MySqlDbType.VarChar;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region CAMPO LIVRE

                    if (!String.IsNullOrEmpty(pesquisa.CampoLivre))
                    {
                        string campoLivre = String.Format("%{0}%", pesquisa.CampoLivre.Trim());
                        filtro.Append(" AND (LocaisAdesoes.nome LIKE @campoLivre ");
                        filtro.Append(" OR LocaisAdesoes.endereco LIKE @campoLivre ");
                        filtro.Append(")");
                        p = new MySqlParameter("@campoLivre", campoLivre);
                        p.MySqlDbType = MySqlDbType.VarChar;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    if (filtro.Length > 0)
                        filtro = filtro.Replace(" AND ", " WHERE ", 0, 5);

                    #region ORDENAÇÃO

                    switch (pesquisa.Ordenacao)
                    {
                        case Utils.TipoOrdenacao.porCodigo:
                            {
                                ordenacao.Append("ORDER BY LocaisAdesoes.codigo");
                            }
                            break;
                        case Utils.TipoOrdenacao.porCodigoDesc:
                            {
                                ordenacao.Append("ORDER BY LocaisAdesoes.codigo DESC");
                            }
                            break;
                        case Utils.TipoOrdenacao.porNome:
                            {
                                ordenacao.Append("ORDER BY LocaisAdesoes.nome, LocaisAdesoes.endereco");
                            }
                            break;
                        case Utils.TipoOrdenacao.porNomeDesc:
                            {
                                ordenacao.Append("ORDER BY LocaisAdesoes.nome DESC, LocaisAdesoes.endereco DESC");
                            }
                            break;
                        case Utils.TipoOrdenacao.porData:
                            {
                                ordenacao.Append("ORDER BY LocaisAdesoes.datacriacao");
                            }
                            break;
                        case Utils.TipoOrdenacao.porDataDesc:
                            {
                                ordenacao.Append("ORDER BY LocaisAdesoes.datacriacao DESC");
                            }
                            break;
                        case Utils.TipoOrdenacao.randomico:
                            {
                                ordenacao.Append("ORDER BY RAND()");
                            }
                            break;
                        default:
                            {
                                ordenacao.Append("ORDER BY LocaisAdesoes.nome, LocaisAdesoes.endereco");
                            }
                            break;
                    }

                    #endregion

                    sql.CommandText = String.Format("SELECT "
                                    + "LocaisAdesoes.* "
                                    + "FROM LocaisAdesoes "
                        + "{0} {1} {2}", filtro, ordenacao, top);
                    DataTable dt = new Data().getData(sql);
                    return MontarRetorno(dt);
                }
            }
            catch (ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Rotina que retorna uma lista genérica do elemento, de acordo com o retorno da Consultar ao BD
        /// </summary>
        /// <param name="dt">VALUE: DataTable com o resultado da Consultar ao BD</param>
        /// <returns>Lista do elemento com os dados retornados da consulta</returns>
        internal static List<LocalAdesao> MontarRetorno(DataTable dt)
        {
            int i = -1;
            List<LocalAdesao> list = new List<LocalAdesao>();

            if (dt == null && dt.Rows.Count == 0)
                return list;
            else
            {
                LocalAdesao retorno;
                try
                {
                    for (i = 0; i < dt.Rows.Count; i++)
                    {
                        retorno = new LocalAdesao()
                        {
                            _codigo = Parser.parseInteiro(dt.Rows[i]["codigo"]),
                            _dataCriacao = Convert.ToDateTime(dt.Rows[i]["datacriacao"]),
                            _dataAlteracao = Convert.ToDateTime(dt.Rows[i]["dataalteracao"]),
                            _nome = Parser.parseString(dt.Rows[i]["nome"]),
                            _endereco = Parser.parseString(dt.Rows[i]["endereco"])
                        };

                        retorno._campanha.Codigo = Parser.parseInteiro(dt.Rows[i]["campanha"]);

                        list.Add(retorno);
                    }
                    return list;
                }
                catch (Exception ex)
                {
                    ex.Data.Add("último retorno lido", i);
                    throw;
                }
            }
        }
    }
}