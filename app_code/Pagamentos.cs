﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;

namespace ImunneVacinas
{
    /// <summary>
    /// Descrição resumida de Pagamentos
    /// </summary>
    public class Pagamento
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="integracao"></param>
        /// <returns></returns>
        public static ImunneVacinas.TransacaoRealizada ValidarPagamento(ImunneVacinas.Integracao integracao, int codigoCompra)
        {
            ImunneVacinas.TransacaoRealizada transacaoEfetivada = new TransacaoRealizada();

            try
            {
                string usuarioAPI = String.Format("{0}", System.Configuration.ConfigurationManager.AppSettings.Get("USUARIO"));
                string senhaAPI = String.Format("{0}", System.Configuration.ConfigurationManager.AppSettings.Get("SENHA"));
                string urlAPI = System.Configuration.ConfigurationManager.AppSettings.Get("URLBASE");
                string endPoint = System.Configuration.ConfigurationManager.AppSettings.Get("ENDPOINT");

                ServicePointManager.ServerCertificateValidationCallback = (sender, cert, chain, sslPolicyErrors) => true;
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(urlAPI + endPoint);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";
                httpWebRequest.Headers.Add(HttpRequestHeader.Authorization, "Basic" + Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(string.Format("{0}:{1}", usuarioAPI, senhaAPI))));

                var jsonContent = JsonConvert.SerializeObject(integracao, Formatting.Indented);

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = JsonConvert.SerializeObject(integracao);
                    streamWriter.Write(jsonContent);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();

                    PaymentReturn paymentReturn = JsonConvert.DeserializeObject<PaymentReturn>(result);

                    if(paymentReturn.data_response.transaction.payment.price_payment.Split('.')[1].Length == 1)
					{
                        paymentReturn.data_response.transaction.payment.price_payment += '0';
                    }

                    transacaoEfetivada = new TransacaoRealizada()
                    {
                        compraTransacao = new Compra() {
                            Codigo = codigoCompra
                        },
                        numeroTransacao = int.Parse(paymentReturn.data_response.transaction.order_number),
                        
                        codigoEstabelecimento = integracao.token_account,
                        codigoFormaPagamento = paymentReturn.data_response.transaction.payment.payment_method_id,
                        valor = Decimal.Parse(paymentReturn.data_response.transaction.payment.price_payment, System.Globalization.CultureInfo.InvariantCulture),
                        valorDesconto = 0,
                        parcelas = paymentReturn.data_response.transaction.payment.split,
                        statusTransacao = paymentReturn.data_response.transaction.status_id,
                        autorizacao = "IMUNNEVACINAS",
                        codigoTransacaoOperadora = paymentReturn.data_response.transaction.payment.payment_response_code,
                        dataAprovacaoOperadora = DateTime.Now.ToString(),
                        numeroComprovanteVenda = paymentReturn.data_response.transaction.payment.tid,
                        nsu = paymentReturn.data_response.transaction.token_transaction,
                        mensagemVenda = paymentReturn.data_response.transaction.payment.payment_response,
                        urlPagamento = paymentReturn.data_response.transaction.payment.url_payment,
                        numeroCartao = integracao.payment.card_number
                    };
                }

                return transacaoEfetivada;
            }
            catch (WebException ex)
            {
                using (var streamReader = new StreamReader(ex.Response.GetResponseStream()))
                {
                    var responseBody = streamReader.ReadToEnd();
                    Erro erro = new Erro();

                    integracao.payment.card_number = $"{integracao.payment.card_number.Substring(0, 6)}************";
                    integracao.payment.card_cvv = "***";

                    var requestBody = JsonConvert.SerializeObject(integracao, Formatting.Indented);

                    erro.MsgExtra = $"Request: {requestBody}, Response: {responseBody}, WebExceptionResponse: {ex.Response}";
                    erro.addArquivo(ex);
                }

                return transacaoEfetivada;
            }
        }
    }
}