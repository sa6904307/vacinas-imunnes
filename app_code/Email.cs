﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;

namespace ImunneVacinas
{
    /// <summary>
    /// Nova classe a ser utilizada no envio dos emails
    /// </summary>
    public class Mail
    {
        /// <summary>
        /// Dados comuns a um cadastro
        /// </summary>
        public struct Usuario
        {
            /// <summary>
            /// Nome do usuário; Use null para ignorar.
            /// </summary>
            public string Nome;
            /// <summary>
            /// Email de contato do usuário; Use null para ignorar.
            /// </summary>
            public string Email;
            /// <summary>
            /// Telefone de contato do usuário. Normalmente um telefone fixo (preencher com DDD + telefone já formatado); Use null para ignorar.
            /// </summary>
            public string Telefone;
        }

        System.Net.Mail.MailMessage _oEmail;
        /// <summary>
        /// Dados do email. 
        /// </summary>
        public System.Net.Mail.MailMessage oEmail
        {
            get
            {
                return _oEmail;
            }
            set
            {
                _oEmail = value;
            }
        }

        System.Net.Mail.SmtpClient _oEnviar;
        /// <summary>
        /// Dados do envio
        /// </summary>
        public System.Net.Mail.SmtpClient oEnviar
        {
            get
            {
                return _oEnviar;
            }
            set
            {
                _oEnviar = value;
            }
        }


        #region Dispose
        private bool disposed = false;
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    // Free other state (managed objects).
                }
                // Free your own state (unmanaged objects). 
                // Set large fields to null.
                this._oEmail.Dispose();
                this._oEnviar = null;

                disposed = true;
            }
        }
        // Use C# destructor syntax for finalization code.
        ~Mail()
        {
            // Simply call Dispose(false).
            Dispose(false);
        }
        #endregion

        public Mail()
        {
            _oEmail = new MailMessage();
            _oEnviar = new SmtpClient();
        }

        /// <summary>
        /// Rotina do elemento Mail que dispara o envio da mensagem com os dados previamente informados
        /// </summary>
        /// <returns></returns>
        public bool SendMail()
        {
            bool enviado = false;

            if (_oEmail == null || _oEnviar == null)
                throw new ArgumentNullException("Dados não inializados");

            if (_oEmail.To == null || _oEmail.To.Count.Equals(0))
                throw new ArgumentException("Nenhum destinatário definido"); // "Error - Nenhum destinatário definido!";

            if (_oEmail.From == null || String.IsNullOrEmpty(_oEmail.From.Address))
                throw new ArgumentException("Nenhum remetente definido");

            if (_oEnviar.Host == null || String.IsNullOrEmpty(_oEnviar.Host))
                throw new ArgumentException("Dados de acesso não definidos");

            if (_oEnviar.Credentials == null)
                throw new ArgumentNullException("Dados de acesso não definidos");

            if (oEmail.Sender == null)
                oEmail.Sender = oEmail.From;

            if (oEmail.ReplyToList.Count == 0)
                oEmail.ReplyToList.Add(oEmail.From);

            oEmail.Priority = MailPriority.Normal;
            oEmail.BodyEncoding = System.Text.Encoding.GetEncoding("utf-8");
            oEmail.SubjectEncoding = System.Text.Encoding.GetEncoding("utf-8");

            //oEnviar.Port = 25;

            int arroba = oEmail.From.Address.IndexOf("@gmail");
            if (arroba > 0)
                oEnviar.EnableSsl = true;

            try
            {
                oEnviar.Send(oEmail);
                enviado = true;
            }
            catch (Exception ex)
            {
                enviado = false;
            }
            return enviado;
        }
    }

    /// <summary>
    /// Descrição resumida de Email
    /// </summary>
    public class Email
    {
        /// <summary>
        /// Rotina que preparo o conteúdo para envio de vales individuais
        /// </summary>
        /// <param name="compraSelecionada">VALUE: Dados da compra realizada</param>
        /// <param name="conteudoVales">VALUE: Dados dos vales gerados</param>
        /// <returns></returns>
        public static string ValeIndividual(ImunneVacinas.Compra compraSelecionada, string conteudoVales)
        {
            ImunneVacinas.Clinica clinicaSelecionada = ImunneVacinas.Clinica.ConsultarUnico(compraSelecionada.IdClinica.Codigo);
            ImunneVacinas.Campanha campanhaSelecionada = ImunneVacinas.Campanha.ConsultarUnico(compraSelecionada.IdCampanha.Codigo);
            ImunneVacinas.Empresa empresaSelecionada = ImunneVacinas.Empresa.ConsultarUnico(campanhaSelecionada.IdEmpresa.Codigo);
            ImunneVacinas.TransacaoRealizada transacaoSelecionada = ImunneVacinas.TransacaoRealizada.ConsultarUnicoCompra(compraSelecionada.Codigo);

            StringBuilder dadosClinica = new StringBuilder();
            dadosClinica.AppendFormat("{0} | ", clinicaSelecionada.NomeFantasia.ToUpper());
            dadosClinica.AppendFormat("{0} ", clinicaSelecionada.Endereco.ToUpper());
            dadosClinica.AppendFormat("{0} - ", clinicaSelecionada.Numero);
            dadosClinica.AppendFormat("{0}/", clinicaSelecionada.IdCidade.Nome);
            dadosClinica.Append(clinicaSelecionada.IdUf.Sigla);

            string conteudoEmail = Resources.Site.ValeIndividual
                                .Replace("#NOMECONTATO", compraSelecionada.IdParticipante.Nome.ToUpper())
                                .Replace("#NUMEROPEDIDO", String.Format("{0} | {1}", compraSelecionada.Codigo.ToString().PadLeft(5, '0'), transacaoSelecionada.numeroComprovanteVenda))
                                .Replace("#NOMECAMPANHA", campanhaSelecionada.Identificacao.ToUpper())
                                .Replace("#NOMEEMPRESA", empresaSelecionada.NomeFantasia.ToUpper())
                                .Replace("#NOMECLINICA", dadosClinica.ToString())
                                .Replace("#CONTEUDOVALES", conteudoVales)
                                .Replace("#ANOCOPYRIGHT ", "&copy; " + DateTime.Now.Year.ToString());

            return conteudoEmail;
        }

        /// <summary>
        /// Rotina que preparo o conteúdo para envio de vales individuais
        /// </summary>
        /// <param name="compraSelecionada">VALUE: Dados da compra realizada</param>
        /// <param name="conteudoVales">VALUE: Dados dos vales gerados</param>
        /// <returns></returns>
        public static string CompraLiberada(ImunneVacinas.Compra compraSelecionada, string conteudoVales)
        {
            string infoHorario = String.Empty;

            try
            {
                ImunneVacinas.Clinica clinicaSelecionada = ImunneVacinas.Clinica.ConsultarUnico(compraSelecionada.IdClinica.Codigo);
                ImunneVacinas.Campanha campanhaSelecionada = ImunneVacinas.Campanha.ConsultarUnico(compraSelecionada.IdCampanha.Codigo);
                ImunneVacinas.Empresa empresaSelecionada = ImunneVacinas.Empresa.ConsultarUnico(campanhaSelecionada.IdEmpresa.Codigo);
                ImunneVacinas.TransacaoRealizada transacaoSelecionada = ImunneVacinas.TransacaoRealizada.ConsultarUnicoCompra(compraSelecionada.Codigo);

                ImunneVacinas.HorarioSurto horarioSelecionado = ImunneVacinas.HorarioSurto.ConsultarUnicoCompra(compraSelecionada.Codigo);
                if (campanhaSelecionada.Surto == 1)
                {
                    if (horarioSelecionado != null)
                        infoHorario = String.Format("APLICAÇÃO SELECIONADA PARA {0}", horarioSelecionado.InfoHoraro.ToString("dd/MM/yyyy HH:mm"));
                    else infoHorario = String.Format("APLICAÇÃO SEM HORÁRIO PRÉ-SELECIONADO");
                }

                StringBuilder dadosClinica = new StringBuilder();
                dadosClinica.AppendFormat("{0} | ", clinicaSelecionada.NomeFantasia.ToUpper());
                dadosClinica.AppendFormat("{0} ", clinicaSelecionada.Endereco.ToUpper());
                dadosClinica.AppendFormat("{0} - ", clinicaSelecionada.Numero);
                dadosClinica.AppendFormat("{0}/", clinicaSelecionada.IdCidade.Nome);
                dadosClinica.Append(clinicaSelecionada.IdUf.Sigla);

                string linkImpressao = String.Format("https://vacinas.imunne.com.br/prints/compra.aspx?compra={0}", ImunneVacinas.Criptografia.Criptografar(compraSelecionada.Codigo.ToString()));

                string conteudoEmail = Resources.Site.EmailEnvioCompra
                                    .Replace("#NOMECONTATO", compraSelecionada.IdParticipante.Nome.ToUpper())
                                    .Replace("#NUMEROPEDIDO", String.Format("{0} | {1}", compraSelecionada.Codigo.ToString().PadLeft(5, '0'), transacaoSelecionada.numeroComprovanteVenda))
                                    .Replace("#NOMECAMPANHA", campanhaSelecionada.Identificacao.ToUpper())
                                    .Replace("#NOMEEMPRESA", empresaSelecionada.NomeFantasia.ToUpper())
                                    .Replace("#NOMECLINICA", dadosClinica.ToString())
                                    .Replace("#CONTEUDOVALES", conteudoVales)
                                    .Replace("#APLICACAO", infoHorario)
                                    .Replace("#LINK", linkImpressao)
                                    .Replace("#ANOCOPYRIGHT ", "&copy; " + DateTime.Now.Year.ToString());

                return conteudoEmail;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// Rotina que preparo o conteúdo para envio de vales individuais
        /// </summary>
        /// <param name="compraSelecionada">VALUE: Dados da compra realizada</param>
        /// <param name="conteudoVales">VALUE: Dados dos vales gerados</param>
        /// <returns></returns>
        public static string ValesSurto(ImunneVacinas.Compra compraSelecionada, string conteudoVales)
        {
            string infoHorario = String.Empty;

            try
            {
                ImunneVacinas.Clinica clinicaSelecionada = ImunneVacinas.Clinica.ConsultarUnico(compraSelecionada.IdClinica.Codigo);
                ImunneVacinas.Campanha campanhaSelecionada = ImunneVacinas.Campanha.ConsultarUnico(compraSelecionada.IdCampanha.Codigo);
                ImunneVacinas.Empresa empresaSelecionada = ImunneVacinas.Empresa.ConsultarUnico(campanhaSelecionada.IdEmpresa.Codigo);
                ImunneVacinas.TransacaoRealizada transacaoSelecionada = ImunneVacinas.TransacaoRealizada.ConsultarUnicoCompra(compraSelecionada.Codigo);

                ImunneVacinas.HorarioSurto horarioSelecionado = ImunneVacinas.HorarioSurto.ConsultarUnicoCompra(compraSelecionada.Codigo);
                if (horarioSelecionado != null)
                    infoHorario = String.Format("APLICAÇÃO SELECIONADA PARA {0}", horarioSelecionado.InfoHoraro.ToString("dd/MM/yyyy HH:mm"));
                else infoHorario = String.Format("APLICAÇÃO SEM HORÁRIO PRÉ-SELECIONADO");

                StringBuilder dadosClinica = new StringBuilder();
                dadosClinica.AppendFormat("{0} | ", clinicaSelecionada.NomeFantasia.ToUpper());
                dadosClinica.AppendFormat("{0} ", clinicaSelecionada.Endereco.ToUpper());
                dadosClinica.AppendFormat("{0} - ", clinicaSelecionada.Numero);
                dadosClinica.AppendFormat("{0}/", clinicaSelecionada.IdCidade.Nome);
                dadosClinica.Append(clinicaSelecionada.IdUf.Sigla);

                string linkImpressao = String.Format("https://vacinas.imunne.com.br/prints/compra.aspx?compra={0}", ImunneVacinas.Criptografia.Criptografar(compraSelecionada.Codigo.ToString()));

                string conteudoEmail = Resources.Site.EmailCompraConcluidaSurto
                                    .Replace("#NOMECONTATO", compraSelecionada.IdParticipante.Nome.ToUpper())
                                    .Replace("#NUMEROPEDIDO", String.Format("{0} | {1}", compraSelecionada.Codigo.ToString().PadLeft(5, '0'), transacaoSelecionada.numeroComprovanteVenda))
                                    .Replace("#NOMECAMPANHA", campanhaSelecionada.Identificacao.ToUpper())
                                    .Replace("#NOMEEMPRESA", empresaSelecionada.NomeFantasia.ToUpper())
                                    .Replace("#NOMECLINICA", dadosClinica.ToString())
                                    .Replace("#CONTEUDOVALES", conteudoVales)
                                    .Replace("#APLICACAO", infoHorario)
                                    .Replace("#LINK", linkImpressao)
                                    .Replace("#ANOCOPYRIGHT ", "&copy; " + DateTime.Now.Year.ToString());

                return conteudoEmail;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// Rotina que preparo o conteúdo para envio de vales individuais
        /// </summary>
        /// <param name="compraSelecionada">VALUE: Dados da compra realizada</param>
        /// <param name="conteudoVales">VALUE: Dados dos vales gerados</param>
        /// <returns></returns>
        public static string ValesNaoSurto(ImunneVacinas.Compra compraSelecionada, string conteudoVales)
        {
            string infoHorario = String.Empty;

            try
            {
                ImunneVacinas.Clinica clinicaSelecionada = ImunneVacinas.Clinica.ConsultarUnico(compraSelecionada.IdClinica.Codigo);
                ImunneVacinas.Campanha campanhaSelecionada = ImunneVacinas.Campanha.ConsultarUnico(compraSelecionada.IdCampanha.Codigo);
                ImunneVacinas.Empresa empresaSelecionada = ImunneVacinas.Empresa.ConsultarUnico(campanhaSelecionada.IdEmpresa.Codigo);
                ImunneVacinas.TransacaoRealizada transacaoSelecionada = ImunneVacinas.TransacaoRealizada.ConsultarUnicoCompra(compraSelecionada.Codigo);

                StringBuilder dadosClinica = new StringBuilder();
                dadosClinica.AppendFormat("{0} | ", clinicaSelecionada.NomeFantasia.ToUpper());
                dadosClinica.AppendFormat("{0} ", clinicaSelecionada.Endereco.ToUpper());
                dadosClinica.AppendFormat("{0} - ", clinicaSelecionada.Numero);
                dadosClinica.AppendFormat("{0}/", clinicaSelecionada.IdCidade.Nome);
                dadosClinica.Append(clinicaSelecionada.IdUf.Sigla);

                string conteudoEmail = Resources.Site.EmailCompraConcluida
                                    .Replace("#NOMECONTATO", compraSelecionada.IdParticipante.Nome.ToUpper())
                                    .Replace("#NUMEROPEDIDO", String.Format("{0} | {1}", compraSelecionada.Codigo.ToString().PadLeft(5, '0'), transacaoSelecionada.numeroComprovanteVenda))
                                    .Replace("#NOMECAMPANHA", campanhaSelecionada.Identificacao.ToUpper())
                                    .Replace("#NOMEEMPRESA", empresaSelecionada.NomeFantasia.ToUpper())
                                    .Replace("#NOMECLINICA", dadosClinica.ToString())
                                    .Replace("#CONTEUDOVALES", conteudoVales)
                                    .Replace("#ANOCOPYRIGHT ", "&copy; " + DateTime.Now.Year.ToString());

                return conteudoEmail;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// Rotina que preparo o conteúdo para envio de todos os vales da compra ao adquirente
        /// </summary>
        /// <param name="compraSelecionada">VALUE: Dados da compra realizada</param>
        /// <param name="conteudoVales">VALUE: Dados dos vales gerados</param>
        /// <returns></returns>
        public static string ValesGerados(ImunneVacinas.Compra compraSelecionada, string conteudoVales)
        {
            ImunneVacinas.Clinica clinicaSelecionada = ImunneVacinas.Clinica.ConsultarUnico(compraSelecionada.IdClinica.Codigo);
            ImunneVacinas.Campanha campanhaSelecionada = ImunneVacinas.Campanha.ConsultarUnico(compraSelecionada.IdCampanha.Codigo);
            ImunneVacinas.Empresa empresaSelecionada = ImunneVacinas.Empresa.ConsultarUnico(campanhaSelecionada.IdEmpresa.Codigo);
            ImunneVacinas.TransacaoRealizada transacaoSelecionada = ImunneVacinas.TransacaoRealizada.ConsultarUnicoCompra(compraSelecionada.Codigo);

            StringBuilder dadosClinica = new StringBuilder();
            dadosClinica.AppendFormat("{0} | ", clinicaSelecionada.NomeFantasia.ToUpper());
            dadosClinica.AppendFormat("{0} ", clinicaSelecionada.Endereco.ToUpper());
            dadosClinica.AppendFormat("{0} - ", clinicaSelecionada.Numero);
            dadosClinica.AppendFormat("{0}/", clinicaSelecionada.IdCidade.Nome);
            dadosClinica.Append(clinicaSelecionada.IdUf.Sigla);

            string conteudoEmail = Resources.Site.EmailValesAdquirente
                                .Replace("#NOMECONTATO", compraSelecionada.IdParticipante.Nome.ToUpper())
                                .Replace("#NUMEROPEDIDO", String.Format("{0} | {1}", compraSelecionada.Codigo.ToString().PadLeft(5, '0'), transacaoSelecionada.numeroComprovanteVenda))
                                .Replace("#NOMECAMPANHA", campanhaSelecionada.Identificacao.ToUpper())
                                .Replace("#NOMEEMPRESA", empresaSelecionada.NomeFantasia.ToUpper())
                                .Replace("#NOMECLINICA", dadosClinica.ToString())
                                .Replace("#CONTEUDOVALES", conteudoVales)
                                .Replace("#ANOCOPYRIGHT ", "&copy; " + DateTime.Now.Year.ToString());

            return conteudoEmail;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="enderecoDestino"></param>
        /// <param name="tituloEmail"></param>
        /// <param name="conteudoEmail"></param>
        /// <returns></returns>
        public static bool EnviarEmail(string enderecoDestino, string tituloEmail, string conteudoEmail)
        {
            bool confirmacaoEnvio = false;
            string hostMail = System.Configuration.ConfigurationManager.AppSettings.Get("HOSTEMAIL");
            string portaEmail = System.Configuration.ConfigurationManager.AppSettings.Get("PORTEMAIL");
            string userMail = System.Configuration.ConfigurationManager.AppSettings.Get("USEREMAIL");
            string passEmail = System.Configuration.ConfigurationManager.AppSettings.Get("PASSEMAIL");
            string replyEmail = System.Configuration.ConfigurationManager.AppSettings.Get("REPLYEMAIL");

            try
            {
                ImunneVacinas.Mail emailEnvio = new ImunneVacinas.Mail();

                emailEnvio.oEmail.To.Add(enderecoDestino);
                emailEnvio.oEmail.From = new System.Net.Mail.MailAddress(userMail, tituloEmail);
                emailEnvio.oEmail.ReplyToList.Add(new System.Net.Mail.MailAddress(replyEmail));
                emailEnvio.oEmail.Subject = tituloEmail;
                emailEnvio.oEmail.Body = conteudoEmail;

                emailEnvio.oEmail.IsBodyHtml = true;
                emailEnvio.oEnviar.Host = hostMail;
                emailEnvio.oEnviar.Credentials = new System.Net.NetworkCredential(userMail, passEmail);
                if (String.IsNullOrEmpty(portaEmail))
                    emailEnvio.oEnviar.Port = 587;
                else
                    emailEnvio.oEnviar.Port = Convert.ToInt32(portaEmail);

                confirmacaoEnvio = emailEnvio.SendMail();

                return confirmacaoEnvio;
            }
            catch (Exception ex)
            {
                Erro erro = new Erro();
                erro.addArquivo(ex);
                return false;
            }
        }

        /// <summary>
        /// Rotina que envia o e-mail de Boas-Vindas
        /// </summary>
        /// <param name="enderecosDestino">VALUE: lista de endereços de destino</param>
        /// <param name="tituloEmail">VALUE: Título do e-mail a ser enviado</param>
        /// <param name="conteudoEmail">VALUE: Conteúdo do e-mail a ser enviado</param>
        /// <returns></returns>
        public static bool EnviarEmailBoasVindas(string enderecosDestino, string tituloEmail, string conteudoEmail)
        {
            bool confirmacaoEnvio = false;
            string hostMail = System.Configuration.ConfigurationManager.AppSettings.Get("HOSTEMAIL");
            string portaEmail = System.Configuration.ConfigurationManager.AppSettings.Get("PORTEMAIL");
            string userMail = System.Configuration.ConfigurationManager.AppSettings.Get("USEREMAIL");
            string passEmail = System.Configuration.ConfigurationManager.AppSettings.Get("PASSEMAIL");
            string replyEmail = System.Configuration.ConfigurationManager.AppSettings.Get("REPLYEMAIL");

            try
            {
                ImunneVacinas.Mail emailEnvio = new ImunneVacinas.Mail();

                string[] arrayDestinatarios = enderecosDestino.Split(';');

                foreach (string item in arrayDestinatarios)
                {
                    if (Utils.ValidarEmail(item))
                        emailEnvio.oEmail.To.Add(item);
                }

                if (emailEnvio.oEmail.To.Count <= 0)
                    return false;

                emailEnvio.oEmail.From = new System.Net.Mail.MailAddress(userMail, tituloEmail);
                emailEnvio.oEmail.ReplyToList.Add(new System.Net.Mail.MailAddress(replyEmail));
                emailEnvio.oEmail.Subject = tituloEmail;
                emailEnvio.oEmail.Body = Resources.Site.BoasVindas.Replace("#CONTEUDO", conteudoEmail).Replace("#ANOCOPYRIGHT", "&copy; " + DateTime.Now.Year.ToString());

                emailEnvio.oEmail.IsBodyHtml = true;
                emailEnvio.oEnviar.Host = hostMail;
                emailEnvio.oEnviar.Credentials = new System.Net.NetworkCredential(userMail, passEmail);
                if (String.IsNullOrEmpty(portaEmail))
                    emailEnvio.oEnviar.Port = 587;
                else
                    emailEnvio.oEnviar.Port = Convert.ToInt32(portaEmail);

                confirmacaoEnvio = emailEnvio.SendMail();

                return confirmacaoEnvio;
            }
            catch (Exception ex)
            {
                Erro erro = new Erro();
                erro.addArquivo(ex);
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="enderecoDestino"></param>
        /// <param name="tituloEmail"></param>
        /// <param name="conteudoEmail"></param>
        /// <returns></returns>
        public static bool TesteEnviarEmail(string enderecoDestino, string tituloEmail, string conteudoEmail)
        {
            bool confirmacaoEnvio = false;
            string hostMail = System.Configuration.ConfigurationManager.AppSettings.Get("HOSTEMAIL");
            string portaEmail = System.Configuration.ConfigurationManager.AppSettings.Get("PORTEMAIL");
            string userMail = System.Configuration.ConfigurationManager.AppSettings.Get("USEREMAIL");
            string passEmail = System.Configuration.ConfigurationManager.AppSettings.Get("PASSEMAIL");
            string replyEmail = System.Configuration.ConfigurationManager.AppSettings.Get("REPLYEMAIL");

            //try
            //{
            ImunneVacinas.Mail emailEnvio = new ImunneVacinas.Mail();

            emailEnvio.oEmail.To.Add(enderecoDestino);
            emailEnvio.oEmail.From = new System.Net.Mail.MailAddress(userMail, tituloEmail);
            emailEnvio.oEmail.ReplyToList.Add(new System.Net.Mail.MailAddress(replyEmail));
            emailEnvio.oEmail.Subject = tituloEmail;
            emailEnvio.oEmail.Body = conteudoEmail;

            emailEnvio.oEmail.IsBodyHtml = true;
            emailEnvio.oEnviar.Host = hostMail;
            emailEnvio.oEnviar.Credentials = new System.Net.NetworkCredential(userMail, passEmail);
            if (String.IsNullOrEmpty(portaEmail))
                emailEnvio.oEnviar.Port = 587;
            else
                emailEnvio.oEnviar.Port = Convert.ToInt32(portaEmail);

            confirmacaoEnvio = emailEnvio.SendMail();

            return confirmacaoEnvio;
            //}
            //catch (Exception ex)
            //{
            //    Erro erro = new Erro();
            //    erro.addArquivo(ex);
            //    return false;
            //}
        }
    }
}