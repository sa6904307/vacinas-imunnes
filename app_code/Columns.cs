﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;

namespace ImunneVacinas.BD
{
    public enum Dados
    {
        /// <summary>
        /// Nome da tabela
        /// </summary>
        table_name,
        /// <summary>
        /// nome da coluna
        /// </summary>
        column_name,
        /// <summary>
        /// Tipo de dado
        /// </summary>
        type,
        /// <summary>
        /// Tamanho máximo suportado (em caracteres)
        /// </summary>
        max_length,
        /// <summary>
        /// Precisão
        /// </summary>
        precision,
        /// <summary>
        /// Escalar
        /// </summary>
        scale,
        /// <summary>
        /// Comentário/descrição do campo
        /// </summary>
        description,
        /// <summary>
        /// Campo identity: 1 = sim; 0 = não
        /// </summary>
        is_identity,
        /// <summary>
        /// Campo Chave: 1 = sim; 0 = não
        /// </summary>
        is_key,
        /// <summary>
        /// Aceita nulos: 1 = sim; 0 = não
        /// </summary>
        is_nullable,
        /// <summary>
        /// Valor padrão definido para o campo
        /// </summary>
        defaultValue, 
        /// <summary>
        /// Caracteres extras, caso o campo contenha uma máscara previsível.
        /// Por ex.: campos decimais normalmente são formatados com uma máscara de milhares, então, 
        /// se o campo for decimal(10,2), e ele fosse formatado como 00.000.000,00 o "mask" seria 3, representando os pontos e vírgula.
        /// </summary>
        mask
    }

    public class Columns
    {
        private string table_name;
        public string Table_name
        {
            get
            {
                return table_name;
            }
            set
            {
                table_name = value;
            }
        }

        private string column_name;
        public string Column_name
        {
            get
            {
                return column_name;
            }
            set
            {
                column_name = value;
            }
        }

        private MySqlDbType type;
        public MySqlDbType Type
        {
            get
            {
                return type;
            }
            set
            {
                type = value;
            }
        }

        private int max_length;
        public int Max_length
        {
            get
            {
                return max_length;
            }
            set
            {
                max_length = value;
            }
        }

        private int precision;
        public int Precision
        {
            get
            {
                return precision;
            }
            set
            {
                precision = value;
            }
        }

        private int scale;
        public int Scale
        {
            get
            {
                return scale;
            }
            set
            {
                scale = value;
            }
        }

        private string description;
        public string Description
        {
            get
            {
                return description;
            }
            set
            {
                description = value;
            }
        }

        private bool is_identity;
        public bool Is_identity
        {
            get
            {
                return is_identity;
            }
            set
            {
                is_identity = value;
            }
        }

        private bool is_key;
        public bool Is_key
        {
            get
            {
                return is_key;
            }
            set
            {
                is_key = value;
            }
        }

        private bool is_nullable;
        public bool Is_nullable
        {
            get
            {
                return is_nullable;
            }
            set
            {
                is_nullable = value;
            }
        }

        private string defaultValue;
        public string DefaultValue
        {
            get
            {
                return defaultValue;
            }
            set
            {
                defaultValue = value;
            }
        }

        private int mask;
        /// <summary>
        /// Digitos adicionais para uma máscara do tipo ###.###,##
        /// </summary>
        public int Mask
        {
            get
            {
                return mask;
            }
            set
            {
                mask = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tabela"></param>
        /// <param name="coluna"></param>
        /// <returns></returns>
        internal static List<Columns> getColumnsInfo(string tabela, string coluna)
        {
            StringBuilder filtro = new StringBuilder();
            MySqlParameter p;
            MySqlCommand sql = new MySqlCommand();
            try
            {
                if (!string.IsNullOrEmpty(coluna))
                {
                    filtro.Append(" AND sys.columns.name = @coluna");
                    p = new MySqlParameter("@coluna", coluna);
                    p.MySqlDbType = MySqlDbType.VarChar;
                    sql.Parameters.Add(p);
                }

                sql.CommandText = String.Format(@"SELECT t.table_name, c.column_name, c.data_type type, "
                                + "CASE	WHEN c.numeric_precision > 0 THEN c.numeric_precision WHEN c.character_maximum_length = -1 THEN c.numeric_precision "
                                + "ELSE c.character_maximum_length END max_length, c.numeric_precision num_precision, c.numeric_scale num_scale, "
                                + "CASE	WHEN c.column_comment IS NULL THEN '' ELSE c.column_comment END description, "
                                + "CASE WHEN c.extra = 'auto_increment' THEN 1 ELSE 0 END is_identity, "
                                + "CASE WHEN c.column_key = 'pri' THEN 1 ELSE 0 END is_key, "
                                + "CASE WHEN c.is_nullable = 'YES' THEN 1 ELSE 0 END is_nullable, "
                                + "CASE	WHEN c.column_default IS NOT NULL THEN REPLACE(REPLACE(REPLACE(REPLACE(c.column_default, ''')',''), '(''',''), '))',''), '((','') "
                                + "WHEN c.data_type LIKE '%date%' THEN '1900-01-01' WHEN c.data_type LIKE '%char%' THEN '' "
                                + "ELSE '0' END defaultValue, CASE WHEN c.numeric_scale = 0 THEN 0 "
                                + "END mask FROM information_schema.tables t INNER JOIN information_schema.columns c ON c.table_name = t.table_name "
                                + "WHERE t.table_name = '{0}' {1} order by t.table_name, c.column_name ", tabela, filtro.ToString());
                                //+"WHERE t.table_schema = 'imunne' AND t.table_name = '{0}' {1} order by t.table_name, c.column_name ", tabela, filtro.ToString());

                DataTable dt = new Data().getData(sql);
                if (dt == null || dt.Rows.Count == 0)
                    return null;
                else
                    return MontarRetorno(dt);

            }
            catch (Exception ex)
            {
                //WebGuess.Log erro = new WebGuess.Log(WebGuess.Log.produtoGuess.webGuessDll);
                //erro.AdicionarLog(ex, new object[] { });
                return null;
            }
            finally
            {
                sql.Dispose();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tabela"></param>
        /// <returns></returns>
        public static List<Columns> getColumnsInfo(string tabela)
        {
            return getColumnsInfo(tabela, null);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="c"></param>
        /// <param name="dado"></param>
        /// <returns></returns>
        public static object getColumnValue(Columns c, ImunneVacinas.BD.Dados dado)
        {
            switch (dado)
            {
                case BD.Dados.table_name:
                    {
                        return c.Table_name;
                    }
                case BD.Dados.column_name:
                    {
                        return c.Column_name;
                    }
                case BD.Dados.max_length:
                    {
                        return c.Max_length;
                    }
                case BD.Dados.precision:
                    {
                        return c.Precision;
                    }
                case BD.Dados.scale:
                    {
                        return c.Scale;
                    }
                case BD.Dados.type:
                    {
                        return c.Type;
                    }
                case BD.Dados.description:
                    {
                        return c.Description;
                    }
                case BD.Dados.is_identity:
                    {
                        return c.Is_identity;
                    }
                case BD.Dados.is_key:
                    {
                        return c.Is_key;
                    }
                case BD.Dados.is_nullable:
                    {
                        return c.Is_nullable;
                    }
                case BD.Dados.defaultValue:
                    {
                        return c.DefaultValue;
                    }
                case BD.Dados.mask:
                    {
                        return c.Mask;
                    }
                default:
                    return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        private static List<Columns> MontarRetorno(DataTable dt)
        {
            List<Columns> list = new List<Columns>();

            if (dt == null || dt.Rows.Count.Equals(0))
                return list;
            else
            {
                Columns retorno;
                try
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        retorno = new Columns();
                        retorno.table_name = Parser.parseString(dt.Rows[i]["table_name"]);
                        retorno.column_name = Parser.parseString(dt.Rows[i]["column_name"]);
                        retorno.type = getMySqlDbType(Parser.parseString(dt.Rows[i]["type"]));
                        retorno.max_length = Parser.parseInteiro(dt.Rows[i]["max_length"]);
                        retorno.precision = Parser.parseInteiro(dt.Rows[i]["num_precision"]);
                        retorno.scale = Parser.parseInteiro(dt.Rows[i]["num_scale"]);
                        retorno.description = Parser.parseString(dt.Rows[i]["description"]);
                        retorno.is_identity = Convert.ToBoolean(dt.Rows[i]["is_identity"]);
                        retorno.is_key = Convert.ToBoolean(dt.Rows[i]["is_key"]);
                        retorno.is_nullable = Convert.ToBoolean(dt.Rows[i]["is_nullable"]);
                        retorno.defaultValue = dt.Rows[i]["defaultValue"].ToString();
                        retorno.mask = Parser.parseInteiro(dt.Rows[i]["mask"].ToString());
                        list.Add(retorno);
                    }

                    return list;
                }
                catch
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tipo"></param>
        /// <returns></returns>
        private static MySqlDbType getMySqlDbType(string tipo)
        {
            switch (tipo.ToLower())
            {
                // Texto não-binários
                case "tinytext":
                    return MySqlDbType.TinyText;
                case "text":
                    return MySqlDbType.Text;
                case "mediumtext":
                    return MySqlDbType.MediumText;
                case "longtext":
                    return MySqlDbType.LongText;
                case "varchar":
                    return MySqlDbType.VarChar;
                case "char":
                    return MySqlDbType.VarChar;
                // Texto binários
                case "tinyblob":
                    return MySqlDbType.TinyBlob;
                case "blob":
                    return MySqlDbType.Blob;
                case "mediumblob":
                    return MySqlDbType.MediumBlob;
                case "longblob":
                    return MySqlDbType.LongBlob;
                case "varbinary":
                    return MySqlDbType.VarBinary;
                case "binary":
                    return MySqlDbType.Binary;
                // Texto em lista
                case "enum":
                    return MySqlDbType.Enum;
                case "set":
                    return MySqlDbType.Set;
                // Números inteiros
                case "tinyint":
                    return MySqlDbType.Int32;
                case "smallint":
                    return MySqlDbType.Int32;
                case "mediumint":
                    return MySqlDbType.Int32;
                case "int":
                    return MySqlDbType.Int32;
                case "bigint":
                    return MySqlDbType.Int64;
                // Numéricos
                case "bit":
                    return MySqlDbType.Bit;
                // Pontos flutuante e fixos
                case "float":
                    return MySqlDbType.Float;
                case "double":
                    return MySqlDbType.Double;
                case "decimal":
                    return MySqlDbType.Decimal;
                // Dados temporais
                case "date":
                    return MySqlDbType.Date;
                case "datetime":
                    return MySqlDbType.DateTime;
                case "timestamp":
                    return MySqlDbType.Timestamp;
                case "year":
                    return MySqlDbType.Year;
                case "time":
                    return MySqlDbType.Time;
                default:
                    return MySqlDbType.Set;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string toString()
        {
            return column_name;
        }

      

    }
}
