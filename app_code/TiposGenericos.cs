﻿using System;

namespace ImunneVacinas
{
    /// <summary>
    /// 
    /// </summary>
    public class ListagemCompra
    {
        /// <summary>
        /// Identificação da campanha da compra
        /// </summary>
        public string Campanha { get; set; }

        /// <summary>
        /// Empresa proprietária da campanha
        /// </summary>
        public string Empresa { get; set; }

        /// <summary>
        /// Número do pedido/compra
        /// </summary>
        public int Pedido { get; set; }

        /// <summary>
        /// Número do comprovante de compra
        /// </summary>
        public string Comprovante { get; set; }

        /// <summary>
        /// Nome completo do titular da compra
        /// </summary>
        public string Titular { get; set; }

        /// <summary>
        /// CPF do titular da compra
        /// </summary>
        public string CpfCnpj { get; set; }

        /// <summary>
        /// E-mail de contato do titular da compra
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Telefone de contato do titular da compra
        /// </summary>
        public string Telefone { get; set; }

        /// <summary>
        /// Nome da clínica onde será aplicada a imunização
        /// </summary>
        public string Clinica { get; set; }

        /// <summary>
        /// Cidade da clínica
        /// </summary>
        public string Cidade { get; set; }

        /// <summary>
        /// UF da clínica
        /// </summary>
        public string UF { get; set; }

        /// <summary>
        /// Data de realização da compra
        /// </summary>
        public string DataCompra { get; set; }

        /// <summary>
        /// Situação atual da compra
        /// </summary>
        public string Situacao { get; set; }

        /// <summary>
        /// Categoria da compra
        /// </summary>
        public string Categoria { get; set; }

        /// <summary>
        /// Data de aplicação da compra
        /// </summary>
        public string DataAplicacao { get; set; }

        /// <summary>
        /// Qtde de parcelas da compra
        /// </summary>
        public int Parcelas { get; set; }

        /// <summary>
        /// Valor total da compra
        /// </summary>
        public decimal Valor { get; set; }

        /// <summary>
        /// Valor total dos itens da compra
        /// </summary>
        public decimal ValorItens { get; set; }

        /// <summary>
        /// Qtde de vouchers já utilizados
        /// </summary>
        public int VouchersUtilizados { get; set; }

        /// <summary>
        /// Qtde de vouchers não utilizados
        /// </summary>
        public int VouchersDemais { get; set; }
    }

    /// <summary>
    /// Elemento utilizado para impressão da listagem de itens de compras de um determinado intervalo
    /// </summary>
    public class ListagemItemCompra
    {
        /// <summary>
        /// 
        /// </summary>
        public string Campanha { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Empresa { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int Pedido { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Comprovante { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Titular { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Telefone { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Beneficiario { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Clinica { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Cidade { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string UF { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int Quantidade { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Produto { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal Preco { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string DataCompra { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string DataLiberacao { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string DataAplicacao { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class TitularCompra
    {
        public string idCampanha { get; set; }
        public string Cpf { get; set; }
        public string Nome { get; set; }
        public string Nascimento { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class DependenteCompra
    {
        public string idCampanha { get; set; }
        public string CpfTitular { get; set; }
        public string Cpf { get; set; }
        public string Nome { get; set; }
        public string Nascimento { get; set; }
    }

    /// <summary>
    /// Elemento que recebe informações das linhas processadas nas importações
    /// </summary>
    public class RetornoProcesso
    {
        public string Linha { get; set; }
        public string Classificacao { get; set; }
        public string Resumo { get; set; }

        /// <summary>
        /// Inicializando o elemento
        /// </summary>
        public RetornoProcesso()
        {
            Linha = String.Empty;
            Classificacao = String.Empty;
            Resumo = String.Empty;
        }
    }
}