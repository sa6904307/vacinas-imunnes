﻿using System;
using System.Text.RegularExpressions;

namespace ImunneVacinas
{
    /// <summary>
    /// Descrição resumida de Cartoes
    /// </summary>
    public class Cartao
    {
        public Cartao()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="numeroCartao"></param>
        /// <returns></returns>
        public static int RetornarOperadoraGetNet(string numeroCartao)
        {
            int codigoOperadora = 0;
            numeroCartao = numeroCartao.Replace(" ", "");

            Regex regexAmex = new Regex(@"^3[47][0-9]{13}");
            Regex regexHypercard = new Regex(@"^(606282\d{10}(\d{3})?)|(3841\d{15})");
            Regex regexElo = new Regex(@"^(40117[8-9]|431274|438935|451416|457393|45763[1-2]|506(699|7[0-6][0-9]|77[0-8])|509\d{3}|504175|627780|636297|636368|65003[1-3]|6500(3[5-9]|4[0-9]|5[0-1])|6504(0[5-9]|[1-3][0-9])|650(4[8-9][0-9]|5[0-2][0-9]|53[0-8])|6505(4[1-9]|[5-8][0-9]|9[0-8])|6507(0[0-9]|1[0-8])|65072[0-7]|6509(0[1-9]|1[0-9]|20)|6516(5[2-9]|[6-7][0-9])|6550([0-1][0-9]|2[1-9]|[3-4][0-9]|5[0-8]))");
            Regex regexMaster = new Regex(@"^5[1-5][0-9]{14}");
            //Regex regexMaestro = new Regex(@"^(?:5[0678]\d\d|6304|6390|67\d\d)\d{8,15}");
            Regex regexVisa = new Regex(@"^4[0-9]{12}(?:[0-9]{3})");

            if (regexHypercard.IsMatch(numeroCartao))
                codigoOperadora = 276;
            else if (regexElo.IsMatch(numeroCartao))
                codigoOperadora = 273;
            else if (regexAmex.IsMatch(numeroCartao))
                codigoOperadora = 272;
            else if (regexMaster.IsMatch(numeroCartao))
                codigoOperadora = 271;
            else if (regexVisa.Match(numeroCartao).Success)
                codigoOperadora = 270;

            return codigoOperadora;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="numeroCartao"></param>
        /// <returns></returns>
        public static int RetornarOperadoraCielo(string numeroCartao)
        {
            int codigoOperadora = 0;

            Regex regexAmex = new Regex(@"^3[47][0-9]{13}");
            Regex regexAura = new Regex(@"^(5078\d{2})(\d{2})(\d{11})");
            Regex regexDiners = new Regex(@"^3(?:0[0-5]|[68][0-9])[0-9]{11}");
            Regex regexDiscover = new Regex(@"^6(?:011|5[0-9]{2})[0-9]{12}");
            Regex regexHypercard = new Regex(@"^(606282\d{10}(\d{3})?)|(3841\d{15})");
            Regex regexElo = new Regex(@"^(40117[8-9]|431274|438935|451416|457393|45763[1-2]|506(699|7[0-6][0-9]|77[0-8])|509\d{3}|504175|627780|636297|636368|65003[1-3]|6500(3[5-9]|4[0-9]|5[0-1])|6504(0[5-9]|[1-3][0-9])|650(4[8-9][0-9]|5[0-2][0-9]|53[0-8])|6505(4[1-9]|[5-8][0-9]|9[0-8])|6507(0[0-9]|1[0-8])|65072[0-7]|6509(0[1-9]|1[0-9]|20)|6516(5[2-9]|[6-7][0-9])|6550([0-1][0-9]|2[1-9]|[3-4][0-9]|5[0-8]))");
            Regex regexJCB = new Regex(@"^(?:2131|1800|35\d{3})\d{11}");
            Regex regexMaster = new Regex(@"^5[1-5][0-9]{14}");
            Regex regexMaestro = new Regex(@"^(?:5[0678]\d\d|6304|6390|67\d\d)\d{8,15}");
            Regex regexVisa = new Regex(@"^4[0-9]{12}(?:[0-9]{3})");

            if (regexElo.IsMatch(numeroCartao))
                codigoOperadora = 173;
            else if (regexAmex.IsMatch(numeroCartao))
                codigoOperadora = 172;
            else if (regexAura.IsMatch(numeroCartao))
                codigoOperadora = 176;
            else if (regexDiners.IsMatch(numeroCartao))
                codigoOperadora = 174;
            else if (regexDiscover.IsMatch(numeroCartao))
                codigoOperadora = 175;
            else if (regexHypercard.IsMatch(numeroCartao))
                codigoOperadora = 180;
            else if (regexJCB.IsMatch(numeroCartao))
                codigoOperadora = 177;
            else if (regexMaster.IsMatch(numeroCartao))
                codigoOperadora = 171;
            else if (regexMaestro.IsMatch(numeroCartao))
                codigoOperadora = 178;
            else if (regexVisa.Match(numeroCartao).Success)
                codigoOperadora = 170;

            return codigoOperadora;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="numeroCartao"></param>
        /// <returns></returns>
        public static string RetornarNomeCartao(string numeroCartao)
        {
            numeroCartao = numeroCartao.Replace(" ", "");
            string nomeCartao = String.Empty;

            Regex regexAmex = new Regex(@"^3[47][0-9]{13}");
            Regex regexAura = new Regex(@"^(5078\d{2})(\d{2})(\d{11})");
            Regex regexDiners = new Regex(@"^3(?:0[0-5]|[68][0-9])[0-9]{11}");
            Regex regexDiscover = new Regex(@"^6(?:011|5[0-9]{2})[0-9]{12}");
            Regex regexHypercard = new Regex(@"^(606282\d{10}(\d{3})?)|(3841\d{15})");
            Regex regexElo = new Regex(@"^(40117[8-9]|431274|438935|451416|457393|45763[1-2]|506(699|7[0-6][0-9]|77[0-8])|509\d{3}|504175|627780|636297|636368|65003[1-3]|6500(3[5-9]|4[0-9]|5[0-1])|6504(0[5-9]|[1-3][0-9])|650(4[8-9][0-9]|5[0-2][0-9]|53[0-8])|6505(4[1-9]|[5-8][0-9]|9[0-8])|6507(0[0-9]|1[0-8])|65072[0-7]|6509(0[1-9]|1[0-9]|20)|6516(5[2-9]|[6-7][0-9])|6550([0-1][0-9]|2[1-9]|[3-4][0-9]|5[0-8]))");
            Regex regexJCB = new Regex(@"^(?:2131|1800|35\d{3})\d{11}");
            Regex regexMaster = new Regex(@"^5[1-5][0-9]{14}");
            Regex regexMaestro = new Regex(@"^(?:5[0678]\d\d|6304|6390|67\d\d)\d{8,15}");
            Regex regexVisa = new Regex(@"^4[0-9]{12}(?:[0-9]{3})");

            if (regexElo.IsMatch(numeroCartao))
                nomeCartao = "ELO CRÉDITO";
            else if (regexAmex.IsMatch(numeroCartao))
                nomeCartao = "AMEX CRÉDITO";
            else if (regexAura.IsMatch(numeroCartao))
                nomeCartao = "AURA CRÉDITO";
            else if (regexDiners.IsMatch(numeroCartao))
                nomeCartao = "DINNERS CRÉDITO";
            else if (regexDiscover.IsMatch(numeroCartao))
                nomeCartao = "DISCOVER CRÉDITO";
            else if (regexHypercard.IsMatch(numeroCartao))
                nomeCartao = "HYPERCARD CRÉDITO";
            else if (regexJCB.IsMatch(numeroCartao))
                nomeCartao = "JCB CRÉDITO";
            else if (regexMaster.IsMatch(numeroCartao))
                nomeCartao = "MASTER CRÉDITO";
            else if (regexMaestro.IsMatch(numeroCartao))
                nomeCartao = "MAESTRO CRÉDITO";
            else if (regexVisa.Match(numeroCartao).Success)
                nomeCartao = "VISA CRÉDITO";

            return nomeCartao;
        }
    }
}