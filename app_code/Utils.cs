﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public static class Page_Extensions
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="page"></param>
    private static void TestarSession(Page page)
    {
        if (page.Session == null)
            page.Response.Redirect("~/401.aspx", true);
    }

    /// <summary>
    /// Inicializa a Session com os campos default
    /// </summary>
    /// <param name="page"></param>
    public static void InicializarSession(this Page page)
    {
        page.Session["codigoUsuario"] = 0;
        page.Session["nomeUsuario"] = "";
        page.Session["loginUsuario"] = "";
        page.Session["clinica"] = -1;
        page.Session["ultimoAcesso"] = "<b>Este é seu primeiro acesso</b>.";
        page.Session["ultimaAtualizacao"] = DateTime.Now;
        page.Session["ultimaPaginaVisitada"] = "";
        page.Session["navegadorAcesso"] = "";
        page.Session["tipoUsuario"] = 0;
        page.Session["avatarUsuario"] = "";
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="page"></param>
    /// <returns></returns>
    public static int ConsultarSessionCodigoUsuario(this Page page)
    {
        if (page.Session["codigoUsuario"] == null)
        {
            return -1;
        }
        else
        {
            int temp = -1;
            if (int.TryParse(page.Session["codigoUsuario"].ToString(), out temp))
                temp = Convert.ToInt32(page.Session["codigoUsuario"].ToString());
            return temp;
        }

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="page"></param>
    /// <returns></returns>
    public static string ConsultarSessionNomeUsuario(this Page page)
    {
        if (page.Session["nomeUsuario"] == null)
        {
            return "";
        }
        else
            return page.Session["nomeUsuario"].ToString();

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="page"></param>
    /// <returns></returns>
    public static int ConsultarClinicaUsuario(this Page page)
    {
        if (page.Session["clinica"] == null)
        {
            return -1;
        }
        else
            return Convert.ToInt32(page.Session["clinica"]);

    }

    /// <summary>
    /// Retorna o código do tipo (role) do usuário.
    /// </summary>
    /// <param name="page"></param>
    /// <returns>Valor inteiro que representa o tipo de usuário</returns>
    public static int ConsultarSessionTipoUsuario(this Page page)
    {
        TestarSession(page);

        if (page.Session["tipoUsuario"] == null)
        {
            return -1;
        }
        else
            return Convert.ToInt32(page.Session["tipoUsuario"].ToString());

    }

    /// <summary>
    /// Retorna data e hora da última interação do usuário com o WSB.
    /// </summary>
    /// <param name="page"></param>
    /// <returns></returns>
    public static DateTime ConsultarSessionUltimaAtualizacao(this Page page)
    {
        TestarSession(page);

        if (page.Session["ultimaAtualizacao"] == null)
        {
            return new DateTime(1900, 01, 01);
        }
        else
            return Convert.ToDateTime(page.Session["ultimaAtualizacao"]);

    }

    /// <summary>
    /// Armazena data e hora da última interação do usuário com o WSB.
    /// </summary>
    /// <param name="page"></param>
    public static void AlterarSessionUltimaAtualizacai(this Page page)
    {
        TestarSession(page);

        page.Session["ultimaAtualizacao"] = DateTime.Now;
    }

    /// <summary>
    /// Retorna a última página do WSB visitada pelo usuário.
    /// </summary>
    /// <param name="page"></param>
    /// <returns></returns>
    public static string ConsultarSessionUltimaPaginaVisitada(this Page page)
    {
        TestarSession(page);

        if (page.Session["ultimaPaginaVisitada"] == null)
        {
            return "";
        }
        else
            return page.Session["ultimaPaginaVisitada"].ToString();

    }

    /// <summary>
    /// Armazena a última página do WSB visitada pelo usuário.
    /// </summary>
    /// <param name="page"></param>
    public static void AlterarSessionUltimaPaginaVisitada(this Page page)
    {
        TestarSession(page);

        page.Session["ultimaPaginaVisitada"] = page.Request.AppRelativeCurrentExecutionFilePath;
    }

    /// <summary>
    /// Armazena do tipo de navegador utilizado pelo usuário.
    /// </summary>
    /// <param name="page"></param>
    public static void AlterarSessionBrowser(this Page page)
    {
        TestarSession(page);

        System.Web.HttpBrowserCapabilities browser = page.Request.Browser;
        page.Session["browser"] = browser.Type + " | " + (browser.IsMobileDevice ? "Mobile " : "Desktop") + " (" + browser.Platform + ")";
    }

    /// <summary>
    /// Salva a exceção no banco, sem redirecionar
    /// </summary>
    /// <param name="page"></param>
    /// <param name="ex"></param>
    public static void SalvarException(this Page page, Exception ex)
    {
        //ImunneVacinas.Erro erro = new ImunneVacinas.Erro(ImunneVacinas.Erro.produtos.ImunneVacinas)
        //{
        //    Usuario = ConsultarSessionCodigoUsuario(page)
        //};
        //erro.AdicionarLog(ex);
    }

    /// <summary>
    /// salva a exceção no banco e redireciona pra a página padrão de erro
    /// </summary>
    /// <param name="page"></param>
    /// <param name="ex"></param>
    public static void SalvarException404(this Page page, Exception ex)
    {
        //ImunneVacinas.Erro erro = new ImunneVacinas.Erro(ImunneVacinas.Erro.produtos.ImunneVacinas)
        //{
        //    Usuario = ConsultarSessionCodigoUsuario(page)
        //};
        //erro.AdicionarLog(ex);

        //page.Response.Redirect("~/404.aspx", true);
    }

    /// <summary>
    /// salva a exceção no banco e redireciona pra a página padrão de erro
    /// </summary>
    /// <param name="page"></param>
    /// <param name="ex"></param>
    public static void SalvarException500(this Page page, Exception ex)
    {
        //ImunneVacinas.Erro erro = new ImunneVacinas.Erro(ImunneVacinas.Erro.produtos.ImunneVacinas)
        //{
        //    Usuario = ConsultarSessionCodigoUsuario(page)
        //};
        //erro.AdicionarLog(ex);

        //page.Response.Redirect("~/500.aspx", true);
    }
}

namespace ImunneVacinas
{
    /// <summary>
    /// 
    /// </summary>
    public static class Utils
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static string EncurtadorUrl(string url)
        {
            try
            {
                string urlMigreMe = string.Format("http://tinyurl.com/api-create.php?url={0}", url);

                var client = new WebClient();

                string response = client.DownloadString(urlMigreMe);

                client.Dispose();

                return response;
            }
            catch (WebException ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static string EncurtadorUrl(Uri url)
        {
            return EncurtadorUrl(url.AbsoluteUri);
        }

        /// <summary>
        /// Tipo utilizado no retorno de listas de Enum
        /// </summary>
        public class retornoEnum
        {
            /// <summary>
            /// Valor/código
            /// </summary>
            public int ValueMember { get; set; }
            /// <summary>
            /// Valor de exibição
            /// </summary>
            public string DisplayMember { get; set; }
        }

        /// <summary>
        /// Rotina que retorna o textual a ser exibido na linha de totais das páginas com listas
        /// </summary>
        /// <param name="totalRegistros">VALUE: Total de registros encontrados</param>
        /// <returns></returns>
        public static string MontarLinhaTotalizadora(int totalRegistros)
        {
            StringBuilder retornoTotal = new StringBuilder();

            if (totalRegistros <= 0)
                retornoTotal.Append("Nenhum registro encontrado.");
            else if (totalRegistros == 1)
                retornoTotal.Append("Foi encontrado <b>1</b> registro.");
            else
                retornoTotal.AppendFormat("Foram encontrados <b>{0}</b> registros.", totalRegistros);

            return retornoTotal.ToString();
        }

        /// <summary>
        /// Rotina que retorna o textual com o título e label do formulário (inserções e edições)
        /// </summary>
        /// <param name="acao">VALUE: Ação que está sendo realizada pelo formulário</param>
        /// <returns></returns>
        public static string MontarTituloFormulario(string acao)
        {
            string tituloRetorno = String.Empty;

            switch (acao)
            {
                case "edit":
                    {
                        tituloRetorno = "Alterando dados do registro";
                    }
                    break;
                default:
                    {
                        tituloRetorno = "Inserindo novo registro";
                    }
                    break;
            }

            return tituloRetorno;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entrada"></param>
        /// <returns></returns>
        public static bool VerificarDataValida(object entrada)
        {
            string dataVerificacao = entrada.ToString().Trim();
            Regex r = new Regex(@"(\d{2}\/\d{2}\/\d{4})");
            if (r.Match(dataVerificacao).Success)
                return true;
            else return false;
        }

        /// <summary>
        /// Monta as linhas a serem utilizadas em um DropDownList
        /// </summary>
        /// <param name="dt">VALUE: DataTable contendo o retorno da consulta e que será exibido no DropDown (obrigatório)</param>
        /// <param name="texto">VALUE: Texto a ser exibido para a linha padrão. Caso nada seja informado, será utilizado SELECIONE por padrão</param>
        /// <returns></returns>
        public static DataTable AdicionarLinhas(DataTable dt, string texto)
        {
            string Texto = String.Empty;

            if (String.IsNullOrEmpty(texto))
                Texto = "Selecione...";
            else
                Texto = texto;

            if (dt == null)
            {
                dt = new DataTable();
                dt.Columns.Add("DisplayMember");
                dt.Columns.Add("ValueMember");
            }
            DataRow dr = dt.NewRow();
            dr["DisplayMember"] = texto;
            dr["ValueMember"] = "-1";
            dt.Rows.InsertAt(dr, 0);
            return dt;
        }

        /// <summary>
        /// Enumeração de tipos de movimentações
        /// </summary>
        public enum TipoMovimentacao
        {
            Indiferente = -1,
            Entrada = 1,
            Saida = 2
        }

        /// <summary>
        /// Enumeração de tipos de ordenção para resultados de consultas
        /// </summary>
        public enum TipoOrdenacao
        {
            porCodigo = 0,
            porCodigoDesc = 1,
            porNome = 2,
            porNomeDesc = 3,
            porData = 4,
            porDataDesc = 5,
            porOrdem = 6,
            porPreco = 7,
            porPrecoDesc = 8,
            porSequencia = 9,
            randomico = 10,
            porInstrutor = 11
        }

        /// <summary>
        /// Enumerador com as possíveis situações dos registros
        /// </summary>
        public enum SituacaoRegistro
        {
            Indiferente = -1,
            Inativo = 0,
            Ativo = 1
        }

        /// <summary>
        /// Enumerador com as possíveis situações dos treinamentos
        /// </summary>
        public enum SituacaoTreinamento
        {
            Indiferente = -1,
            Inativo = 0,
            Ativo = 1,
            Concluido = 2,
            Cancelado = 3
        }

        /// <summary>
        /// Enumerador com as possíveis ações que podem ocorrer no sistema
        /// </summary>
        public enum AcaoLog
        {
            Indiferente = -1,
            Insercao = 1,
            Edicao = 2,
            Exclusao = 3,
            Consulta = 4,
            Upload = 5,
            Download = 6,
            Acesso = 7,
            Envio_Email = 8
        }

        /// <summary>
        /// 
        /// </summary>
        public enum SituacaoHorario
        {
            Indiferente = -1,
            Livre = 0,
            Reservado = 1,
            Confirmado = 2,
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="acao"></param>
        /// <returns></returns>
        public static string RetornaNomeAcao(AcaoLog acao)
        {
            string nome = "";

            switch (acao)
            {
                case AcaoLog.Edicao:
                    {
                        nome = "EDIÇÃO";
                    }
                    break;
                case AcaoLog.Exclusao:
                    {
                        nome = "EXCLUSÃO";
                    }
                    break;
                case AcaoLog.Insercao:
                    {
                        nome = "INSERÇÃO";
                    }
                    break;
                case AcaoLog.Consulta:
                    {
                        nome = "PESQUISA";
                    }
                    break;
                //case AcaoLog.EnvioEmail:
                //    {
                //        nome = "E-MAIL";
                //    }
                //    break;
                //case AcaoLog.EnvioSMS:
                //    {
                //        nome = "SMS";
                //    }
                //    break;
                default:
                    {
                        nome = "INDIFERENTE";
                    }
                    break;
            }

            return nome;
        }

        /// <summary>
        /// 
        /// </summary>
        public class ListCEP
        {
            private string _logradouro;

            public string Logradouro
            {
                get { return _logradouro; }
                set { _logradouro = value; }
            }
            private string _endereco;

            public string Endereco
            {
                get { return _endereco; }
                set { _endereco = value; }
            }
            private string _bairro;

            public string Bairro
            {
                get { return _bairro; }
                set { _bairro = value; }
            }
            private string _cidade;

            public string Cidade
            {
                get { return _cidade; }
                set { _cidade = value; }
            }
            private string _uf;

            public string Uf
            {
                get { return _uf; }
                set { _uf = value; }
            }
        }

        /// <summary>
        /// Estrutura de dados de Data
        /// </summary>
        public class DataCompleta
        {
            public string Dia { get; set; }
            public string Mes { get; set; }
            public string Ano { get; set; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mes"></param>
        /// <returns></returns>
        public static string RetornarNomeMes(int mes)
        {
            CultureInfo culturaPais = new CultureInfo("pt-BR");
            DateTimeFormatInfo infoData = culturaPais.DateTimeFormat;

            return culturaPais.TextInfo.ToTitleCase(infoData.GetMonthName(mes));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nomeCidade"></param>
        /// <returns></returns>
        public static string RetornarCidadeDataHeader(string nomeCidade)
        {
            CultureInfo culturaPais = new CultureInfo("pt-BR");
            DateTimeFormatInfo infoData = culturaPais.DateTimeFormat;
            string nomeMes = culturaPais.TextInfo.ToTitleCase(infoData.GetMonthName(DateTime.Now.Month));

            StringBuilder dataCompleta = new StringBuilder();
            dataCompleta.AppendFormat("{0}", Utils.RetornarTextoCapitalizado(nomeCidade));
            dataCompleta.AppendFormat(", {0} de {1} de {2}", DateTime.Now.Day.ToString().PadLeft(2, '0'), nomeMes, DateTime.Now.Year);

            return dataCompleta.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="valor"></param>
        /// <returns></returns>
        public static string RetornarValorExtenso(decimal valor)
        {
            string retorno = String.Empty;

            if (valor <= 0 | valor >= 1000000000000000)
                return "Valor não suportado pelo sistema.";
            else
            {
                //string strValor = valor.ToString("000000000000000.00");
                string strValor = valor.ToString();
                string valor_por_extenso = string.Empty;

                for (int i = 0; i <= 15; i += 3)
                {
                    valor_por_extenso += Escrever_Valor_Extenso(Convert.ToDecimal(strValor.Substring(i, 3)));

                    if (i == 0 & valor_por_extenso != string.Empty)
                    {
                        if (Convert.ToInt32(strValor.Substring(0, 3)) == 1)
                            valor_por_extenso += " TRILHÃO" + ((Convert.ToDecimal(strValor.Substring(3, 12)) > 0) ? " E " : string.Empty);
                        else if (Convert.ToInt32(strValor.Substring(0, 3)) > 1)
                            valor_por_extenso += " TRILHÕES" + ((Convert.ToDecimal(strValor.Substring(3, 12)) > 0) ? " E " : string.Empty);
                    }
                    else if (i == 3 & valor_por_extenso != string.Empty)
                    {
                        if (Convert.ToInt32(strValor.Substring(3, 3)) == 1)
                            valor_por_extenso += " BILHÃO" + ((Convert.ToDecimal(strValor.Substring(6, 9)) > 0) ? " E " : string.Empty);
                        else if (Convert.ToInt32(strValor.Substring(3, 3)) > 1)
                            valor_por_extenso += " BILHÕES" + ((Convert.ToDecimal(strValor.Substring(6, 9)) > 0) ? " E " : string.Empty);
                    }
                    else if (i == 6 & valor_por_extenso != string.Empty)
                    {
                        if (Convert.ToInt32(strValor.Substring(6, 3)) == 1)
                            valor_por_extenso += " MILHÃO" + ((Convert.ToDecimal(strValor.Substring(9, 6)) > 0) ? " E " : string.Empty);
                        else if (Convert.ToInt32(strValor.Substring(6, 3)) > 1)
                            valor_por_extenso += " MILHÕES" + ((Convert.ToDecimal(strValor.Substring(9, 6)) > 0) ? " E " : string.Empty);
                    }
                    else if (i == 9 & valor_por_extenso != string.Empty)
                        if (Convert.ToInt32(strValor.Substring(9, 3)) > 0)
                            valor_por_extenso += " MIL" + ((Convert.ToDecimal(strValor.Substring(12, 3)) > 0) ? " E " : string.Empty);

                    if (i == 12)
                    {
                        if (valor_por_extenso.Length > 8)
                            if (valor_por_extenso.Substring(valor_por_extenso.Length - 6, 6) == "BILHÃO" | valor_por_extenso.Substring(valor_por_extenso.Length - 6, 6) == "MILHÃO")
                                valor_por_extenso += " DE";
                            else
                                if (valor_por_extenso.Substring(valor_por_extenso.Length - 7, 7) == "BILHÕES" | valor_por_extenso.Substring(valor_por_extenso.Length - 7, 7) == "MILHÕES"
| valor_por_extenso.Substring(valor_por_extenso.Length - 8, 7) == "TRILHÕES")
                                valor_por_extenso += " DE";
                            else
                                    if (valor_por_extenso.Substring(valor_por_extenso.Length - 8, 8) == "TRILHÕES")
                                valor_por_extenso += " DE";

                        if (Convert.ToInt64(strValor.Substring(0, 15)) == 1)
                            valor_por_extenso += " REAL";
                        else if (Convert.ToInt64(strValor.Substring(0, 15)) > 1)
                            valor_por_extenso += " REAIS";

                        if (Convert.ToInt32(strValor.Substring(16, 2)) > 0 && valor_por_extenso != string.Empty)
                            valor_por_extenso += " E ";
                    }

                    if (i == 15)
                        if (Convert.ToInt32(strValor.Substring(16, 2)) == 1)
                            valor_por_extenso += " CENTAVO";
                        else if (Convert.ToInt32(strValor.Substring(16, 2)) > 1)
                            valor_por_extenso += " CENTAVOS";
                }
                retorno = valor_por_extenso;
            }

            return Utils.RetornarTextoCapitalizado(retorno.ToLower());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="valor"></param>
        /// <returns></returns>
        static string Escrever_Valor_Extenso(decimal valor)
        {
            if (valor <= 0)
                return string.Empty;
            else
            {
                string montagem = string.Empty;
                if (valor > 0 & valor < 1)
                {
                    valor *= 100;
                }
                string strValor = valor.ToString("000");
                int a = Convert.ToInt32(strValor.Substring(0, 1));
                int b = Convert.ToInt32(strValor.Substring(1, 1));
                int c = Convert.ToInt32(strValor.Substring(2, 1));

                if (a == 1) montagem += (b + c == 0) ? "CEM" : "CENTO";
                else if (a == 2) montagem += "DUZENTOS";
                else if (a == 3) montagem += "TREZENTOS";
                else if (a == 4) montagem += "QUATROCENTOS";
                else if (a == 5) montagem += "QUINHENTOS";
                else if (a == 6) montagem += "SEISCENTOS";
                else if (a == 7) montagem += "SETECENTOS";
                else if (a == 8) montagem += "OITOCENTOS";
                else if (a == 9) montagem += "NOVECENTOS";

                if (b == 1)
                {
                    if (c == 0) montagem += ((a > 0) ? " E " : string.Empty) + "DEZ";
                    else if (c == 1) montagem += ((a > 0) ? " E " : string.Empty) + "ONZE";
                    else if (c == 2) montagem += ((a > 0) ? " E " : string.Empty) + "DOZE";
                    else if (c == 3) montagem += ((a > 0) ? " E " : string.Empty) + "TREZE";
                    else if (c == 4) montagem += ((a > 0) ? " E " : string.Empty) + "QUATORZE";
                    else if (c == 5) montagem += ((a > 0) ? " E " : string.Empty) + "QUINZE";
                    else if (c == 6) montagem += ((a > 0) ? " E " : string.Empty) + "DEZESSEIS";
                    else if (c == 7) montagem += ((a > 0) ? " E " : string.Empty) + "DEZESSETE";
                    else if (c == 8) montagem += ((a > 0) ? " E " : string.Empty) + "DEZOITO";
                    else if (c == 9) montagem += ((a > 0) ? " E " : string.Empty) + "DEZENOVE";
                }
                else if (b == 2) montagem += ((a > 0) ? " E " : string.Empty) + "VINTE";
                else if (b == 3) montagem += ((a > 0) ? " E " : string.Empty) + "TRINTA";
                else if (b == 4) montagem += ((a > 0) ? " E " : string.Empty) + "QUARENTA";
                else if (b == 5) montagem += ((a > 0) ? " E " : string.Empty) + "CINQUENTA";
                else if (b == 6) montagem += ((a > 0) ? " E " : string.Empty) + "SESSENTA";
                else if (b == 7) montagem += ((a > 0) ? " E " : string.Empty) + "SETENTA";
                else if (b == 8) montagem += ((a > 0) ? " E " : string.Empty) + "OITENTA";
                else if (b == 9) montagem += ((a > 0) ? " E " : string.Empty) + "NOVENTA";

                if (strValor.Substring(1, 1) != "1" & c != 0 & montagem != string.Empty) montagem += " E ";

                if (strValor.Substring(1, 1) != "1")
                    if (c == 1) montagem += "UM";
                    else if (c == 2) montagem += "DOIS";
                    else if (c == 3) montagem += "TRÊS";
                    else if (c == 4) montagem += "QUATRO";
                    else if (c == 5) montagem += "CINCO";
                    else if (c == 6) montagem += "SEIS";
                    else if (c == 7) montagem += "SETE";
                    else if (c == 8) montagem += "OITO";
                    else if (c == 9) montagem += "NOVE";

                return montagem;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cep"></param>
        /// <returns></returns>
        public static List<ListCEP> ConsultarCEP(string cep)
        {
            List<ListCEP> Retornos = new List<ListCEP>();

            // Verifica na API PostComBR
            Retornos = ConsultarPostMOMBR(cep);

            // Verifica na API da RepublicaVirtual caso nada tenha sido encontrado no PostMOM
            if (Retornos == null || Retornos.Count == 0)
                Retornos = ConsultarRepulicaVirtual(cep);

            // Verifica no WS dos Correios caso nada tenha sido retornado
            if (Retornos == null || Retornos.Count == 0)
                Retornos = ConsultarCorreios(cep);

            return Retornos;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cep"></param>
        /// <returns></returns>
        private static List<ListCEP> ConsultarCorreios(string cep)
        {
            List<ListCEP> Retornos = new List<ListCEP>();

            if (!String.IsNullOrEmpty(cep))
            {
                string ParametrosEntrada = "cepEntrada=" + cep.Replace("-", "").Trim() + "&tipoCep=&cepTemp=&metodo=buscarCep";

                WebRequest RequestWS = WebRequest.Create("http://m.correios.com.br/movel/buscaCepConfirma.do?" + ParametrosEntrada);
                HttpWebResponse ResponseWS = (HttpWebResponse)RequestWS.GetResponse();
                StreamReader StreamWS = new StreamReader(ResponseWS.GetResponseStream(), Encoding.GetEncoding("ISO-8859-1"));

                string Dados = StreamWS.ReadToEnd();

                if (Dados == null || Dados.Length == 0)
                    return Retornos;

                int Count = 0;
                string ExpressaoRegular = "<span class=respostadestaque>(.*?)</span>";
                MatchCollection Endereco = Regex.Matches(Dados, ExpressaoRegular, RegexOptions.Singleline | RegexOptions.IgnoreCase);

                ListCEP Item = new ListCEP();

                if (Endereco.Count == 2) // Aqui temos somente o retorno da Cidade/UF
                {
                    Item.Logradouro = String.Empty;
                    Item.Endereco = String.Empty;
                    Item.Bairro = String.Empty;

                    foreach (Match Resultado in Endereco)
                    {
                        Count++;
                        switch (Count)
                        {
                            case 1:
                                {
                                    if (!String.IsNullOrEmpty(Resultado.Groups[1].Value.ToString()))
                                    {
                                        string NomeCidade = Utils.RemoverCaracteres(Resultado.Groups[1].Value.Trim().Split('/')[0]);
                                        Item.Cidade = Utils.LimparString(NomeCidade.ToUpper());

                                        Item.Uf = RemoverCaracteres(Resultado.Groups[1].Value.Trim().Split('/')[1]);
                                    }
                                }
                                break;
                        }
                    }
                }
                else
                {
                    foreach (Match Resultado in Endereco)
                    {
                        Count++;
                        switch (Count)
                        {
                            case 1: // Resultado para popular informações de Logradouro/Endereço
                                {
                                    string[] palavras = Utils.RemoverCaracteres(Resultado.Groups[1].Value).Split(Convert.ToChar(" "));
                                    Item.Logradouro = palavras[0].ToString().ToUpper();

                                    string EnderecoCompleto = Utils.LimparString(Utils.RemoverCaracteres((Resultado.Groups[1].Value).Substring(palavras[0].ToString().Length).Trim()).ToUpper());

                                    // Aqui verifica e remove as informações dos endereços com hífen
                                    #region IndexesHifen

                                    int IndexHifen = EnderecoCompleto.IndexOf(" -");

                                    if (IndexHifen > 0)
                                        EnderecoCompleto = EnderecoCompleto.Substring(0, IndexHifen);

                                    #endregion

                                    // Aqui verifica e remove as informações dos endereços que tenham indicadores de numerações
                                    #region IndexesNumerico

                                    int IndexZero = EnderecoCompleto.IndexOf(" DE 0");
                                    int IndexUm = EnderecoCompleto.IndexOf(" DE 1");
                                    int IndexDois = EnderecoCompleto.IndexOf(" DE 2");
                                    int IndexTres = EnderecoCompleto.IndexOf(" DE 3");
                                    int IndexQuatro = EnderecoCompleto.IndexOf(" DE 4");
                                    int IndexCinco = EnderecoCompleto.IndexOf(" DE 5");
                                    int IndexSeis = EnderecoCompleto.IndexOf(" DE 6");
                                    int IndexSete = EnderecoCompleto.IndexOf(" DE 7");
                                    int IndexOito = EnderecoCompleto.IndexOf(" DE 8");
                                    int IndexNove = EnderecoCompleto.IndexOf(" DE 9");

                                    if (IndexZero > 0)
                                        EnderecoCompleto = EnderecoCompleto.Substring(0, IndexZero);

                                    if (IndexUm > 0)
                                        EnderecoCompleto = EnderecoCompleto.Substring(0, IndexUm);

                                    if (IndexDois > 0)
                                        EnderecoCompleto = EnderecoCompleto.Substring(0, IndexDois);

                                    if (IndexTres > 0)
                                        EnderecoCompleto = EnderecoCompleto.Substring(0, IndexTres);

                                    if (IndexQuatro > 0)
                                        EnderecoCompleto = EnderecoCompleto.Substring(0, IndexQuatro);

                                    if (IndexCinco > 0)
                                        EnderecoCompleto = EnderecoCompleto.Substring(0, IndexCinco);

                                    if (IndexSeis > 0)
                                        EnderecoCompleto = EnderecoCompleto.Substring(0, IndexSeis);

                                    if (IndexSete > 0)
                                        EnderecoCompleto = EnderecoCompleto.Substring(0, IndexSete);

                                    if (IndexOito > 0)
                                        EnderecoCompleto = EnderecoCompleto.Substring(0, IndexOito);

                                    if (IndexNove > 0)
                                        EnderecoCompleto = EnderecoCompleto.Substring(0, IndexNove);

                                    #endregion

                                    Item.Endereco = EnderecoCompleto; //.Replace(Item.Logradouro + " ", "");
                                }
                                break;
                            case 2:
                                {
                                    if (!String.IsNullOrEmpty(Resultado.Groups[1].Value.ToString()))
                                        Item.Bairro = Utils.LimparString(Utils.RemoverCaracteres(Resultado.Groups[1].Value).ToUpper());
                                }
                                break;
                            case 3:
                                {
                                    if (!String.IsNullOrEmpty(Resultado.Groups[1].Value.ToString()))
                                    {
                                        string NomeCidade = Utils.RemoverCaracteres(Resultado.Groups[1].Value.Trim().Split('/')[0]);
                                        Item.Cidade = Utils.LimparString(NomeCidade.ToUpper());

                                        Item.Uf = Utils.RemoverCaracteres(Resultado.Groups[1].Value.Trim().Split('/')[1]);
                                    }
                                }
                                break;
                        }
                    }
                }

                Retornos.Add(Item);
            }
            return Retornos;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cep"></param>
        /// <returns></returns>
        private static List<ListCEP> ConsultarPostMOMBR(string cep)
        {
            List<ListCEP> Retorno = new List<ListCEP>();

            try
            {
                DataSet dsCep = new DataSet();
                dsCep.ReadXml("http://api.postmon.com.br/v1/cep/" + cep + "?format=xml");

                if (dsCep.Tables[0] != null || dsCep.Tables.Count > 0)
                {
                    ListCEP Item = new ListCEP();

                    if (dsCep.Tables[0].Columns.Contains("estado"))
                        Item.Uf = dsCep.Tables[0].Rows[0]["estado"].ToString();

                    if (dsCep.Tables[0].Columns.Contains("cidade"))
                        Item.Cidade = dsCep.Tables[0].Rows[0]["cidade"].ToString();

                    if (dsCep.Tables[0].Columns.Contains("bairro"))
                        Item.Bairro = dsCep.Tables[0].Rows[0]["bairro"].ToString();

                    if (dsCep.Tables[0].Columns.Contains("logradouro"))
                    {
                        int PosicaoEspaco = dsCep.Tables[0].Rows[0]["logradouro"].ToString().IndexOf(" ");
                        int CaracteresRestantes = (dsCep.Tables[0].Rows[0]["logradouro"].ToString().Length) - (PosicaoEspaco + 1);

                        Item.Logradouro = dsCep.Tables[0].Rows[0]["logradouro"].ToString().Substring(0, PosicaoEspaco);
                        Item.Endereco = dsCep.Tables[0].Rows[0]["logradouro"].ToString().Substring(0, PosicaoEspaco) + " "
                                      + dsCep.Tables[0].Rows[0]["logradouro"].ToString().Substring((PosicaoEspaco + 1), CaracteresRestantes);
                    }
                    Retorno.Add(Item);
                }

                return Retorno;
            }
            catch (Exception)
            {
                return Retorno;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cep"></param>
        /// <returns></returns>
        private static List<ListCEP> ConsultarRepulicaVirtual(string cep)
        {
            List<ListCEP> Retorno = new List<ListCEP>();

            DataSet dsCep = new DataSet();
            dsCep.ReadXml("http://cep.republicavirtual.com.br/web_cep.php?cep=" + cep);

            if (dsCep.Tables[0].Rows[0][0].ToString() == "1")
            {
                ListCEP Item = new ListCEP();

                if (dsCep.Tables[0].Columns.Contains("uf"))
                    Item.Uf = dsCep.Tables[0].Rows[0]["uf"].ToString();

                if (dsCep.Tables[0].Columns.Contains("cidade"))
                    Item.Cidade = dsCep.Tables[0].Rows[0]["cidade"].ToString();

                if (dsCep.Tables[0].Columns.Contains("bairro"))
                    Item.Bairro = dsCep.Tables[0].Rows[0]["bairro"].ToString();

                if (dsCep.Tables[0].Columns.Contains("logradouro"))
                    Item.Endereco = dsCep.Tables[0].Rows[0]["tipo_logradouro"].ToString() + " " + dsCep.Tables[0].Rows[0]["logradouro"].ToString();

                Retorno.Add(Item);
            }

            return Retorno;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parametros"></param>
        /// <returns></returns>
        public static System.Collections.Specialized.NameValueCollection RecuperarQueryString(string parametros)
        {
            System.Collections.Specialized.NameValueCollection n = new System.Collections.Specialized.NameValueCollection();
            try
            {
                if (parametros != null)
                {
                    string[] col = parametros.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string c in col)
                        n.Add(c.Split(':')[0], c.Split(':')[1]);
                }
            }
            catch (Exception ex)
            {
            }
            return n;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static List<ImunneVacinas.RetornoComboBox> ConsultaComboBoxSituacaoComboBox()
        {
            var statusItens = typeof(ImunneVacinas.Utils.SituacaoRegistro);
            List<ImunneVacinas.RetornoComboBox> retornos = new List<ImunneVacinas.RetornoComboBox>();
            ImunneVacinas.RetornoComboBox item = new RetornoComboBox
            {
                ValueMember = "-1",
                DisplayMember = "SITUAÇÃO"
            };
            retornos.Add(item);

            foreach (int valor in Enum.GetValues(statusItens))
            {
                if (valor > -1)
                {
                    item = new RetornoComboBox();
                    var nome = Enum.GetName(statusItens, valor);
                    item.ValueMember = valor.ToString();
                    item.DisplayMember = nome.ToUpper();
                    retornos.Add(item);
                }
            }

            return retornos;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static List<ImunneVacinas.RetornoComboBox> ConsultaComboBoxSituacaoTreinamentosComboBox()
        {
            var statusItens = typeof(ImunneVacinas.Utils.SituacaoTreinamento);
            List<ImunneVacinas.RetornoComboBox> retornos = new List<ImunneVacinas.RetornoComboBox>();
            ImunneVacinas.RetornoComboBox item = new RetornoComboBox
            {
                ValueMember = "-1",
                DisplayMember = "SITUAÇÃO"
            };
            retornos.Add(item);

            foreach (int valor in Enum.GetValues(statusItens))
            {
                if (valor > -1)
                {
                    item = new RetornoComboBox();
                    var nome = Enum.GetName(statusItens, valor);
                    item.ValueMember = valor.ToString();
                    item.DisplayMember = nome.ToUpper();
                    retornos.Add(item);
                }
            }

            return retornos;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static List<ImunneVacinas.RetornoComboBox> ConsultaComboBoxAcoesLog()
        {
            var acaoLog = typeof(ImunneVacinas.Utils.AcaoLog);
            List<ImunneVacinas.RetornoComboBox> retornos = new List<ImunneVacinas.RetornoComboBox>();
            ImunneVacinas.RetornoComboBox item = new RetornoComboBox
            {
                ValueMember = "-1",
                DisplayMember = "AÇÕES"
            };
            retornos.Add(item);

            foreach (int valor in Enum.GetValues(acaoLog))
            {
                if (valor > -1)
                {
                    item = new RetornoComboBox();
                    var nome = Enum.GetName(acaoLog, valor);
                    item.ValueMember = valor.ToString();
                    item.DisplayMember = nome.ToUpper();
                    retornos.Add(item);
                }
            }

            return retornos;
        }

        /// <summary>
        /// 
        /// </summary>
        public static string[] meses = { "JANEIRO", "FEVEREIRO", "MARCO", "ABRIL", "MAIO", "JUNHO", "JULHO", "AGOSTO", "SETEMBRO", "OUTUBRO", "NOVEMBRO", "DEZEMBRO" };

        /// <summary>
        /// 
        /// </summary>
        private static System.Globalization.CultureInfo cult = new System.Globalization.CultureInfo("pt-br");

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sts"></param>
        /// <returns></returns>
        public static SituacaoRegistro AnalisarStatus(object sts)
        {
            if (sts == null)
                return SituacaoRegistro.Inativo;
            int t = Parser.parseInteiro(sts);

            if (t == 1)
                return SituacaoRegistro.Ativo;
            else
                return SituacaoRegistro.Inativo;
        }

        /// <summary>
        /// Retorna um número no formato "R$ 000.000,00" ou texto
        /// </summary>
        /// <param name="retornoPadrao">Texto retornado caso o valor seja 0 ou inválido</param>        
        public static string ToMoeda(object valor, string retornoPadrao)
        {
            if (Parser.parseDecimal(valor) == 0)
                return retornoPadrao;
            else
                return System.Convert.ToDecimal(valor).ToString("C", cult);
        }

        /// <summary>
        /// Retorna um número no formato "R$ 000.000,00" ou texto
        /// </summary>
        public static string ToMoeda(object valor)
        {
            if (Parser.parseDecimal(valor) == 0)
                valor = 0;
            return System.Convert.ToDecimal(valor).ToString("C", cult);
        }

        /// <summary>
        /// Retorna um número no formato "000.000,00 m²" ou texto
        /// </summary>
        /// <param name="retornoPadrao">Texto retornado caso o valor seja 0 ou inválido</param>        
        public static string ToMetro(object valor, string retornoPadrao)
        {
            if (Parser.parseDecimal(valor) == 0)
                return retornoPadrao;
            else
                return System.Convert.ToDecimal(valor) + " m²";
        }

        /// <summary>
        /// Retorna um número no formato "000.000,00 m²" ou texto
        /// </summary>
        public static string ToMetro(object valor)
        {
            if (Parser.parseDecimal(valor) == 0)
                valor = 0;
            return System.Convert.ToDecimal(valor).ToString("G") + " m²";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="valor"></param>
        /// <returns></returns>
        public static string RetornaValorJson(string valor)
        {
            string valorOperacao;
            if (valor.Contains("R$"))
                valorOperacao = valor;
            else
                valorOperacao = ImunneVacinas.Utils.ToMoeda(valor);

            return valorOperacao.Replace(",", "").Replace(".", "").Replace("R$", "");
        }

        /// <summary>
        /// Deixa a string com a primeira letra maiuscula
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string TitleCaseString(string s)
        {
            if (s == null)
                return s;

            string[] words = s.Split(' ');
            for (int i = 0; i < words.Length; i++)
            {
                if (words[i].Length == 0)
                    continue;
                char firstChar = char.ToUpper(words[i][0]);
                string rest = "";
                if (words[i].Length > 1)
                {
                    rest = words[i].Substring(1).ToLower();
                }
                words[i] = firstChar + rest;
            }
            return string.Join(" ", words);
        }

        /// <summary>
        /// Função de mensagens de aviso
        /// </summary>
        /// <param name="page"></param>
        /// <param name="text"></param>
        public static void MostrarAviso(Page page, string text)
        {
            string script = "";
            script = "<script language='JavaScript'>alert('" + text + "'); </script>";
            ScriptManager.RegisterStartupScript(page, page.GetType(), "error", script, false);
        }

        /// <summary>
        /// Remove todos os acentos de um conjunto string
        /// </summary>
        /// <param name="texto"></param>
        /// <returns></returns>
        public static string RemoverAcentos(string texto)
        {
            StringBuilder sbReturn = new StringBuilder();
            var arrayText = texto.Normalize(NormalizationForm.FormD).ToCharArray();
            foreach (char letter in arrayText)
            {
                if (CharUnicodeInfo.GetUnicodeCategory(letter) != UnicodeCategory.NonSpacingMark)
                    sbReturn.Append(letter);
            }
            return sbReturn.ToString();
        }

        [Obsolete("É usado??", true)]
        public static bool VerificarCaracteresInvalidos(string strIn/*,  bool remover*/)
        {
#warning Verificar caracteres invalidos antes de armazenar no banco!!
            //strOut = "";
            bool valido = false;
            string exp = @"^[^<>`~!/@\\#}$%:;)(_^{&*=|'+]+$";

            valido = System.Text.RegularExpressions.Regex.IsMatch(strIn, exp);

            return valido;
        }

        /// <summary>
        /// Retorna os meses do ano
        /// </summary>
        /// <param name="nomeMes"></param>
        /// <returns></returns>
        public static int ConsultarM(string nomeMes)
        {
            nomeMes = RemoverAcentos(nomeMes.Trim()).ToUpper();
            int i = 1;
            foreach (string mes in meses)
            {
                if (mes.Equals(nomeMes))
                    break;
                i++;
            }
            return i;

        }

        /// <summary>
        /// Retorna os meses do ano
        /// </summary>
        /// <param name="Mes"></param>
        /// <returns></returns>
        public static string ConsultarM(int Mes)
        {
            if (Mes < 1 || Mes > 12)
                return "";
            return TitleCaseString(meses[Mes - 1]);

        }

        /// <summary>
        /// Formata uma string.
        /// </summary>
        /// <param name="mascara">Estilo da formatação. Usar o caractere '#'. 
        /// Ex.:
        /// cep: #####-###, telefone: (##) ####-####</param>
        /// <param name="valor">a string a ser formatada</param>
        public static string FormatarString(string mascara, string valor)
        {
            try
            {
                StringBuilder novoValor = new StringBuilder();
                int posicao = 0;

                for (int i = 0; mascara.Length > i; i++)
                {
                    if (mascara[i] == '#')
                    {
                        if (valor.Length > posicao)
                        {
                            novoValor.Append(valor[posicao]);
                            posicao++;
                        }
                        else
                            break;
                    }
                    else
                    {
                        if (valor.Length > posicao)
                            novoValor.Append(mascara[i]);
                        else
                            break;
                    }
                }

                return novoValor.ToString();
            }
            catch (Exception ex)
            {
                Erro erro = new Erro(Erro.produtos.ImunneVacinas);
                //erro.AdicionarLog(ex);
                return "";
            }
        }

        /// <summary>
        /// Remove caracteres.
        /// </summary>
        /// <param name="texto"></param>
        /// <param name="sobra">Caracteres que serão removidos</param>
        /// <param name="removerZerosIniciais">remover zeros iniciais (caso a string seja um número)</param>
        public static string RemoverFormatacao(string texto, string[] sobra, bool removerZerosIniciais)
        {
            //string textosaida = "";
            try
            {
                if (String.IsNullOrEmpty(texto))
                    return "";
                //return texto.Replace(".", "").Replace("/", "").Replace("-", "").TrimStart(new char[] { '0' });
                if (removerZerosIniciais)
                    texto = texto.TrimStart(new char[] { '0' });
                if (sobra != null)
                    foreach (string s in sobra)
                    {
                        texto = texto.Replace(s, "");
                    }
                return texto.Trim();
            }
            catch (Exception ex)
            {
                Erro erro = new Erro(Erro.produtos.ImunneVacinas);
                //erro.AdicionarLog(ex);
                return texto;
            }
        }

        /// <summary>
        /// Trunca a string para a quantidade de caracteres desejados, preservando o inicio da mesma
        /// </summary>
        /// <param name="texto">Texto a ser truncado</param>
        /// <param name="tamanho">Quantidades de caracteres no texto de saída.</param>
        public static string TruncarString(string texto, int tamanho)
        {
            try
            {
                if (String.IsNullOrEmpty(texto))
                    return "";
                if (texto.Length > tamanho)
                {
                    texto = texto.Substring(0, tamanho);
                }
                return texto;
            }
            catch (Exception ex)
            {
                Erro erro = new Erro(Erro.produtos.ImunneVacinas);
                //erro.AdicionarLog(ex);
                return "";
            }
        }

        /// <summary>
        /// Trunca a string para a quantidade de caracteres desejados, preservando o final da mesma
        /// </summary>
        /// <param name="texto">Texto a ser truncado</param>
        /// <param name="tamanho">Quantidades de caracteres no texto de saída.</param>
        public static string TruncarStringNoInicio(string texto, int tamanho)
        {
            try
            {
                if (String.IsNullOrEmpty(texto))
                    return "";
                if (texto.Length > tamanho)
                {
                    texto = texto.Substring(texto.Length - tamanho, tamanho);
                }
                return texto;
            }
            catch (Exception ex)
            {
                Erro erro = new Erro(Erro.produtos.ImunneVacinas);
                //erro.AdicionarLog(ex);
                return "";
            }
        }

        /// <summary>
        /// Trata todos os caracteres que podem deformar uma query
        /// </summary>
        /// <param name="valor"></param>
        public static string AlterarStringQuery(string valor)
        {
            ///Método muito importante. Cuidado ao alterar. Ou várias strings do banco poderão se perder.
            try
            {
                return Parser.parseString(valor).Replace("'", "''"); //tratando aspa simples
            }
            catch (Exception ex)
            {
                Erro erro = new Erro(Erro.produtos.ImunneVacinas);
                //erro.AdicionarLog(ex);
                return valor.ToString();
            }
            //return "";
        }

        /// <summary>
        /// Rotina que formata o CPF/CNPJ para visualização
        /// </summary>
        /// <param name="cpfCnpj">VALUE: Valor do documento a ser exibido</param>
        /// <returns></returns>
        public static string FormatarCpfCnpj(string cpfCnpj)
        {
            string retornoCpfCnpj = String.Empty;

            try
            {
                cpfCnpj = LimparCpfCnpj(cpfCnpj);

                if (!String.IsNullOrEmpty(cpfCnpj))
                {
                    if (cpfCnpj.Length <= 11)
                        retornoCpfCnpj = Convert.ToUInt64(cpfCnpj).ToString(@"000\.000\.000\-00");
                    else if (cpfCnpj.Length > 11 && cpfCnpj.Length <= 14)
                        retornoCpfCnpj = Convert.ToUInt64(cpfCnpj).ToString(@"00\.000\.000\/0000\-00");
                    else retornoCpfCnpj = cpfCnpj;
                }
            }
            catch (Exception ex)
            {
                string mensagem = ex.Message;
            }

            return retornoCpfCnpj;
        }

        /// <summary>
        /// Rotina que formata o RG para visualização
        /// </summary>
        /// <param name="numeroRg">VALUE: valor do documento a ser exibido</param>
        /// <returns></returns>
        public static string FormatarRG(string numeroRg)
        {
            string retornoRg = String.Empty;
            numeroRg = LimparCpfCnpj(numeroRg);

            retornoRg = String.Format(@"{0:00\.000\.000\-0}", numeroRg);

            return retornoRg;
        }

        /// <summary>
        /// Rotina que limpa o CPF/CNPJ dos caracteres ., - e /
        /// </summary>
        /// <param name="cpfCnpj"></param>
        /// <returns></returns>
        public static string LimparCpfCnpj(string cpfCnpj)
        {
            return cpfCnpj.Replace(".", string.Empty).Replace("-", string.Empty).Replace("/", string.Empty);
        }

        /// <summary>
        /// Rotina que limpa e retorna o telefone para uso via WhatsApp
        /// </summary>
        /// <param name="contato"></param>
        /// <returns></returns>
        internal static string LimparWhatsApp(string contato)
        {
            return contato.Replace("-", "").Replace("(", "").Replace(")", "").Replace(" ", "");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="contato"></param>
        /// <returns></returns>
        public static string PrepararLinkWhatsApp(string contato)
        {
            string retorno = String.Empty;

            if (VerificarAgent())
                retorno = String.Format("https://api.whatsapp.com/send?phone=55{0}", LimparWhatsApp(contato));
            else
                retorno = String.Format("https://web.whatsapp.com/send?phone=55{0}", LimparWhatsApp(contato));

            return retorno;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public static bool ValidarEmail(string email)
        {
            try
            {
                var addr = new MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Rotina que retorna se CPF/CNPJ informado é válido
        /// </summary>
        /// <param name="cpfCnpj">VALUE: CPF/CNPJ a ser validado</param>
        /// <returns>TRUE ou FALSE para a validação</returns>
        public static bool ValidarCpfCnpj(string cpfCnpj)
        {
            try
            {
                bool retorno = false;

                if (cpfCnpj.Length > 11)
                    retorno = VerificarCNPJ(cpfCnpj);
                else
                    retorno = VerificarCPF(cpfCnpj);

                return retorno;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// Rotina interna que valida documentos do tipo CPF
        /// </summary>
        /// <param name="cpf">VALUE: CPF a ser validado</param>
        /// <returns></returns>
        internal static bool VerificarCPF(string cpf)
        {
            int[] multiplicador1 = new int[9] { 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            int[] multiplicador2 = new int[10] { 11, 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            string tempCpf;
            string digito;
            int soma;
            int resto;

            cpf = cpf.Trim();
            cpf = cpf.Replace(".", "").Replace("-", "");

            if (cpf.Length != 11)
                return false;

            tempCpf = cpf.Substring(0, 9);
            soma = 0;

            for (int i = 0; i < 9; i++)
                soma += int.Parse(tempCpf[i].ToString()) * multiplicador1[i];

            resto = soma % 11;
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;

            digito = resto.ToString();

            tempCpf = tempCpf + digito;

            soma = 0;
            for (int i = 0; i < 10; i++)
                soma += int.Parse(tempCpf[i].ToString()) * multiplicador2[i];

            resto = soma % 11;
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;

            digito = digito + resto.ToString();

            return cpf.EndsWith(digito);
        }

        /// <summary>
        /// Rotina interna que valida documentos do tipo CNPJ
        /// </summary>
        /// <param name="cpf">VALUE: CNPJ a ser validado</param>
        /// <returns></returns>
        internal static bool VerificarCNPJ(string cnpj)
        {
            int[] multiplicador1 = new int[12] { 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };
            int[] multiplicador2 = new int[13] { 6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };
            int soma;
            int resto;
            string digito;
            string tempCnpj;

            cnpj = cnpj.Trim();
            cnpj = cnpj.Replace(".", "").Replace("-", "").Replace("/", "");

            if (cnpj.Length != 14)
                return false;

            tempCnpj = cnpj.Substring(0, 12);

            soma = 0;
            for (int i = 0; i < 12; i++)
                soma += int.Parse(tempCnpj[i].ToString()) * multiplicador1[i];

            resto = (soma % 11);
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;

            digito = resto.ToString();

            tempCnpj = tempCnpj + digito;
            soma = 0;
            for (int i = 0; i < 13; i++)
                soma += int.Parse(tempCnpj[i].ToString()) * multiplicador2[i];

            resto = (soma % 11);
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;

            digito = digito + resto.ToString();

            return cnpj.EndsWith(digito);
        }

        /// <summary>
        /// Rotina que verifica se o acesso é via agent Mobile ou não
        /// </summary>
        /// <returns></returns>
        internal static bool VerificarAgent()
        {
            //GETS THE CURRENT USER CONTEXT
            HttpContext context = HttpContext.Current;

            //FIRST TRY BUILT IN ASP.NT CHECK
            if (context.Request.Browser.IsMobileDevice)
            {
                return true;
            }
            //THEN TRY CHECKING FOR THE HTTP_X_WAP_PROFILE HEADER
            if (context.Request.ServerVariables["HTTP_X_WAP_PROFILE"] != null)
            {
                return true;
            }
            //THEN TRY CHECKING THAT HTTP_ACCEPT EXISTS AND CONTAINS WAP
            if (context.Request.ServerVariables["HTTP_ACCEPT"] != null &&
                context.Request.ServerVariables["HTTP_ACCEPT"].ToLower().Contains("wap"))
            {
                return true;
            }
            //AND FINALLY CHECK THE HTTP_USER_AGENT 
            //HEADER VARIABLE FOR ANY ONE OF THE FOLLOWING
            if (context.Request.ServerVariables["HTTP_USER_AGENT"] != null)
            {
                //Create a list of all mobile types
                string[] mobiles =
                    new[]
                    {
                    "midp", "j2me", "avant", "docomo",
                    "novarra", "palmos", "palmsource",
                    "240x320", "opwv", "chtml",
                    "pda", "windows ce", "mmp/",
                    "blackberry", "mib/", "symbian",
                    "wireless", "nokia", "hand", "mobi",
                    "phone", "cdm", "up.b", "audio",
                    "SIE-", "SEC-", "samsung", "HTC",
                    "mot-", "mitsu", "sagem", "sony"
                    , "alcatel", "lg", "eric", "vx",
                    "NEC", "philips", "mmm", "xx",
                    "panasonic", "sharp", "wap", "sch",
                    "rover", "pocket", "benq", "java",
                    "pt", "pg", "vox", "amoi",
                    "bird", "compal", "kg", "voda",
                    "sany", "kdd", "dbt", "sendo",
                    "sgh", "gradi", "jb", "dddi",
                    "moto", "iphone"
                    };

                //Loop through each item in the list created above 
                //and check if the header contains that text
                foreach (string s in mobiles)
                {
                    if (context.Request.ServerVariables["HTTP_USER_AGENT"].ToLower().Contains(s.ToLower()))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Troca os caracteres HTML para ASCII
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string StripHTML(string text)
        {
            text = text.Replace("&nbsp;", "");
            text = text.Replace("&amp;", "&");
            return Regex.Replace(text, @"<(.|\n)*?>", string.Empty);
        }

        /// <summary>
        /// Analisa o arquivo para retornar e utilizar o ENCODE Original do mesmo
        /// </summary>
        /// <param name="arquivo"></param>
        /// <returns></returns>
        public static Encoding ConsultarEncodeArquivo(string arquivo)
        {
            Encoding enc = Encoding.Default;

            byte[] buffer = new byte[5];
            FileStream file = new FileStream(arquivo, FileMode.Open);
            file.Read(buffer, 0, 5);
            file.Close();

            if (buffer[0] == 0xef && buffer[1] == 0xbb && buffer[2] == 0xbf)
                enc = Encoding.UTF8;
            else if (buffer[0] == 0xfe && buffer[1] == 0xff)
                enc = Encoding.Unicode;
            else if (buffer[0] == 0 && buffer[1] == 0 && buffer[2] == 0xfe && buffer[3] == 0xff)
                enc = Encoding.UTF32;
            else if (buffer[0] == 0x2b && buffer[1] == 0x2f && buffer[2] == 0x76)
                enc = Encoding.UTF7;

            return enc;
        }

        /// <summary>
        /// Data mínima do sqlServer, que usaremos como default (1900-01-01 00:00:00.000)
        /// </summary>
        public readonly static DateTime dataPadrao = new DateTime(1900, 01, 01);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="texto"></param>
        /// <returns></returns>
        public static string RemoverCaracteres(string texto)
        {
            string resultado = texto;

            resultado = resultado.Replace("\n", "");
            resultado = resultado.Replace("\r", "");
            resultado = resultado.Replace("\t", "");
            resultado = resultado.Trim();

            return resultado;
        }

        /// <summary>
        /// Limpa o campo, removendo espaços em branco e caracteres especiais
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string LimparString(string str)
        {
            if (!String.IsNullOrEmpty(str))
            {
                /** Troca os caracteres acentuados por não acentuados **/
                string[] acentos = new string[] { "ç", "Ç", "á", "é", "í", "ó", "ú", "ý", "Á", "É", "Í", "Ó", "Ú", "Ý", "à", "è", "ì", "ò", "ù", "À", "È", "Ì", "Ò", "Ù", "ã", "õ", "ñ", "ä", "ë", "ï", "ö", "ü", "ÿ", "Ä", "Ë", "Ï", "Ö", "Ü", "Ã", "Õ", "Ñ", "â", "ê", "î", "ô", "û", "Â", "Ê", "Î", "Ô", "Û", "&#39;" };
                string[] semAcento = new string[] { "c", "C", "a", "e", "i", "o", "u", "y", "A", "E", "I", "O", "U", "Y", "a", "e", "i", "o", "u", "A", "E", "I", "O", "U", "a", "o", "n", "a", "e", "i", "o", "u", "y", "A", "E", "I", "O", "U", "A", "O", "N", "a", "e", "i", "o", "u", "A", "E", "I", "O", "U", "'" };
                for (int i = 0; i < acentos.Length; i++)
                {
                    str = str.Replace(acentos[i], semAcento[i]);
                }

                /** Troca ' por '' para permitir inserção no banco de dados **/
                str = str.Replace("'", "''");

                /** Troca palavras reservadas, evitando SQL Injection **/
                string[] injection = { "SELECT ", "INSERT ", "UPDATE ", "DELETE ", "WHERE ",
                                   "JOIN ", "LEFT ", "INNER ", "NOT ", "IN ", "LIKE ",
                                   "TRUNCATE ", "DROP ", "CREATE ", " ALTER ", "DELIMITER " };
                for (int i = 0; i < injection.Length; i++)
                {
                    str = str.Replace(injection[i], "");
                }

                /** Troca os espaços no início por "" **/
                str = str.Replace("^\\s+", "");
                /** Troca os espaços no início por "" **/
                str = str.Replace("\\s+$", "");
                /** Troca os espaços duplicados, tabulações e etc por  " " **/
                str = str.Replace("\\s+", " ");
                return str;
            }
            else
                return str;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="endereco"></param>
        /// <returns></returns>
        public static DataTable ConsultarCoordenadas(string endereco)
        {
            try
            {
                string url = "http://maps.google.com/maps/api/geocode/xml?address=" + endereco + "&sensor=false";
                WebRequest request = WebRequest.Create(url);
                using (WebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    using (StreamReader reader = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
                    {
                        DataSet dsResult = new DataSet();
                        dsResult.ReadXml(reader);
                        DataTable dtCoordinates = new DataTable();
                        dtCoordinates.Columns.AddRange(new DataColumn[4] { new DataColumn("Id", typeof(int)),
                        new DataColumn("Address", typeof(string)),
                        new DataColumn("Latitude",typeof(string)),
                        new DataColumn("Longitude",typeof(string)) });

                        if (dsResult != null && dsResult.Tables.Count > 0)
                        {
                            foreach (DataRow row in dsResult.Tables["result"].Rows)
                            {
                                string geometry_id = dsResult.Tables["geometry"].Select("result_id = " + row["result_id"].ToString())[0]["geometry_id"].ToString();
                                DataRow location = dsResult.Tables["location"].Select("geometry_id = " + geometry_id)[0];
                                dtCoordinates.Rows.Add(row["result_id"], row["formatted_address"], location["lat"], location["lng"]);
                            }
                        }
                        return dtCoordinates;
                    }
                }
            }
            catch (Exception ex)
            {
                ImunneVacinas.Erro NovoErro = new ImunneVacinas.Erro();
                //NovoErro.AdicionarLog(ex);
                return null;
            }

        }

        /// <summary>
        /// Retorna um list para uso em comboboxes com um valor inicial e um final, geralmente para ordenações
        /// </summary>
        /// <param name="inicio">VALUE: Valor inicial dos retornos</param>
        /// <param name="final">VALUE: Valor final dos retornos</param>
        /// <param name="InserirLinhaPadrao">VALUE: Flag que informa se o list será retornado com o valor padrão</param>
        /// <param name="texto">VALUE: Texto do valor padrão</param>
        /// <returns></returns>
        public static List<ImunneVacinas.RetornoComboBox> MontarComboNumeros(int inicio, int final, bool InserirLinhaPadrao, string texto)
        {
            DataTable dt = new DataTable();

            try
            {
                dt.Columns.Add("DisplayMember");
                dt.Columns.Add("ValueMember");

                int valorInicial = -1;
                int valorFinal = -1;

                if (inicio <= 0 && final <= 0)
                {
                    valorInicial = 0;
                    valorFinal = 100;
                }
                else
                {
                    if (final < inicio)
                    {
                        valorFinal = inicio;
                        valorInicial = final;
                    }
                    else
                    {
                        valorInicial = inicio;
                        valorFinal = final;
                    }
                }

                for (int i = valorInicial; i <= valorFinal; i++)
                {
                    dt.Rows.Add(new object[] { i.ToString(), i.ToString() });
                }

                return MontarRetornoComboBox(dt, InserirLinhaPadrao, texto);
            }
            catch (Exception ex)
            {
                //WebGuess.Log erro = new WebGuess.Log(WebGuess.produtoGuess.webGuessDll);
                //erro.AdicionarLog(ex, new object[] { 0, InserirLinhaPadrao, texto });
                return null;
            }
        }

        /// <summary>
        /// Monta o retorno para Combo box e inclui a linha default.
        /// Retorna uma array do tipo RetornoComboBox
        /// </summary>
        /// <param name="dt">Para dt populados, deve possuir OBRIGATÓRIAMENTE 2 colunas: DisplayMember e ValueMember.</param>
        /// <param name="InserirLinhaPadrao">TRUE para adicionar linha inicial (-1 | Selecione)</param>
        /// <param name="texto">Um texto personalizado para a linha default. Use null para ignorar</param>
        public static List<RetornoComboBox> MontarRetornoComboBox(System.Data.DataTable dt, bool linhaDefault, string texto)
        {
            List<RetornoComboBox> list = new List<RetornoComboBox>();

            try
            {
                if (linhaDefault)
                    dt = InserirLinhaPadrao(dt, texto);

                if (dt == null || dt.Rows.Count.Equals(0))
                    return null;
                else
                {
                    RetornoComboBox retorno;

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        retorno = new RetornoComboBox
                        {
                            DisplayMember = dt.Rows[i][0].ToString(),
                            ValueMember = dt.Rows[i][1].ToString()
                        };

                        list.Add(retorno);
                    }
                    return list;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="texto"></param>
        /// <returns></returns>
        public static DataTable InserirLinhaPadrao(DataTable dt, string texto)
        {
            try
            {
                if (dt == null)
                {
                    dt = new DataTable();
                    dt.Columns.Add("DisplayMember");
                    dt.Columns.Add("ValueMember");
                }
                DataRow dr = dt.NewRow();
                // vazio ("") pode ser um texto válido, então testamos apenas ser for null
                if (texto == null)
                    texto = "Selecione";
                dr["DisplayMember"] = texto;
                dr["ValueMember"] = "-1";
                dt.Rows.InsertAt(dr, 0);
                return dt;
            }
            catch (Exception ex)
            {
                //Erro erro = new Erro(Erro.produtoGuess.webhair);
                //erro.AdicionarLog(ex);
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public struct UserMail
        {
            public string[] destinatario;
            public string remetente;
            public string assunto;
            public string body;
            public string smtp;
            public string smtpuser;
            public string smtppass;
            public string[] files;
            public string hostname;
            public bool ssl;
            public bool gmail;
            public int porta;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="conteudo"></param>
        /// <returns></returns>
        public static string RetornarTextoCapitalizado(string conteudo)
        {
            CultureInfo informacaoCultural = Thread.CurrentThread.CurrentCulture;
            TextInfo informacaoTexto = informacaoCultural.TextInfo;

            return informacaoTexto.ToTitleCase(conteudo);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="infoCulture"></param>
        /// <returns></returns>
        public static string RetornarDataExtenso(CultureInfo infoCulture)
        {
            DateTimeFormatInfo formatoData = infoCulture.DateTimeFormat;
            string retorno = String.Empty;

            retorno = ", " + DateTime.Now.Day.ToString().PadLeft(2, '0')
                           + " de " + infoCulture.TextInfo.ToTitleCase(formatoData.GetMonthName(DateTime.Now.Month))
                           + " de " + DateTime.Now.Year + ".";

            return retorno;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataCompleta"></param>
        /// <returns></returns>
        public static DataCompleta AnalisarDataCompleta(DateTime dataCompleta)
        {
            DataCompleta novaData = new DataCompleta
            {
                Mes = RetornarMes(dataCompleta.Month.ToString().PadLeft(2, '0')),
                Dia = dataCompleta.Day.ToString().PadLeft(2, '0'),
                Ano = dataCompleta.Year.ToString().PadLeft(4, '0')
            };

            return novaData;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mes"></param>
        /// <returns></returns>
        public static string RetornarMes(string mes)
        {
            string Mes = mes.Replace("01", "JAN").Replace("02", "FEB").Replace("03", "MAR").Replace("04", "ABR")
                            .Replace("05", "MAI").Replace("06", "JUN").Replace("07", "JUL").Replace("08", "AGO")
                            .Replace("09", "SET").Replace("10", "OUT").Replace("11", "NOV").Replace("12", "DEZ");
            return Mes;
        }

        #region Cast internos do site

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string LimparHTMLString(string input)
        {
            return Regex.Replace(input, @"<(\w+)\b(?:\s+[\w\-.:]+(?:\s*=\s*(?:""[^""]*""|'[^']*'|[\w\-.:]+))?)*\s*/?>\s*</\1\s*>", string.Empty, RegexOptions.Multiline);
        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        public class ArquivoFoto
        {
            private FileUpload _nomearquivo { get; set; }
            public FileUpload NomeArquivo
            {
                get { return _nomearquivo; }
                set { _nomearquivo = value; }
            }

            private string _descricaoArquivo { get; set; }
            public string DescricaoArquivo
            {
                get { return _descricaoArquivo; }
                set { _descricaoArquivo = value; }
            }
        }

        /// <summary>
        /// Estrutura de dados para upload de fotos
        /// </summary>
        public class UploadFotoImovel
        {
            private HttpPostedFile _postedFile { get; set; }
            public HttpPostedFile PostedFile { get; set; }
        }

        /// <summary>
        /// Redimensiona a imagem para 226x170 pixels.
        /// </summary>
        public static bool GerarMiniaturaDaImagem(string caminhoArquivo, string caminhoArquivoMini)
        {
            if (String.IsNullOrEmpty(caminhoArquivo))
                throw new ArgumentException("O parametro é nulo ou inválido", caminhoArquivo);

            string nomeArquivo = Path.GetFileName(caminhoArquivo);
            string dirArquivo = Path.GetDirectoryName(caminhoArquivo);

            string sExtension = System.IO.Path.GetExtension(caminhoArquivo);

            //if (sExtension.ToLower() != ".jpg" && sExtension.ToLower() != ".gif" && sExtension.ToLower() != ".bmp" && sExtension.ToLower() != ".jpeg")
            if (sExtension.ToLower() != ".jpg" && sExtension.ToLower() != ".gif" && sExtension.ToLower() != ".bmp" && sExtension.ToLower() != ".jpeg" && sExtension.ToLower() != ".png")
                throw new BadImageFormatException("O arquivo não possui um formato válido. Formatos esperados: jpg/gif/bmp/jpeg/png");

            try
            {
                using (System.Drawing.Image imagesize = System.Drawing.Image.FromFile(caminhoArquivo))
                {
                    //int larguraImagem = Convert.ToInt32((imagesize.Width / 2));
                    //int alturaImagem = Convert.ToInt32((imagesize.Height / 2));
                    int larguraImagem = Convert.ToInt32((imagesize.Width * 0.2));
                    int alturaImagem = Convert.ToInt32((imagesize.Height * 0.2));
                    using (System.Drawing.Image imagemfinal = new Bitmap(imagesize, new Size(larguraImagem, alturaImagem)))
                    {
                        imagemfinal.Save(caminhoArquivoMini);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                ex.Data.Add("Arquivo", caminhoArquivo);
                throw;
            }
        }


        /// <summary>
        /// Rotina que gera ID randômica e única
        /// </summary>
        /// <param name="count">VALUE: Tamanho da lista a ser retornada</param>
        /// <returns></returns>
        public static string GerarIdRandomica(int count)
        {
            string caracteresID = Guid.NewGuid().ToString().Replace("-", "").ToUpper();
            int valorMaximo = caracteresID.Length;

            Random random = new Random(DateTime.Now.Millisecond);

            StringBuilder senha = new StringBuilder(count);

            for (int indice = 0; indice < count; indice++)
                senha.Append(caracteresID[random.Next(0, (valorMaximo - 1))]);

            return senha.ToString();
        }

        /// <summary>
        /// Retorna uma lista genérica com informações do Enum
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static List<ImunneVacinas.Utils.retornoEnum> ConsultarListaEnum<T>()
        {
            List<ImunneVacinas.Utils.retornoEnum> list = new List<ImunneVacinas.Utils.retornoEnum>();
            foreach (var e in Enum.GetValues(typeof(T)))
            {
                ImunneVacinas.Utils.retornoEnum item = new ImunneVacinas.Utils.retornoEnum();
                item.ValueMember = (int)e;
                item.DisplayMember = e.ToString().Replace("_", " ").ToUpper();

                list.Add(item);
            }
            return list;
        }

        /// <summary>
        /// Retorna as datas para filtros de acordo com a opção selecionada
        /// </summary>
        /// <param name="opcao"></param>
        public static string[] retornoDatasFiltros(int opcao)
        {
            string[] retornosData = new string[2];

            switch (opcao)
            {
                // Hoje
                case 1:
                    {
                        retornosData[0] = DateTime.Now.ToString("dd/MM/yyyy");
                        retornosData[1] = DateTime.Now.ToString("dd/MM/yyyy");
                    }
                    break;
                // Ontem
                case 2:
                    {
                        retornosData[0] = DateTime.Now.AddDays(-1).ToString("dd/MM/yyyy");
                        retornosData[1] = DateTime.Now.AddDays(-1).ToString("dd/MM/yyyy");
                    }
                    break;
                // Semana
                case 3:
                    {
                        retornosData[0] = DateTime.Now.AddDays(-7).ToString("dd/MM/yyyy");
                        retornosData[1] = DateTime.Now.ToString("dd/MM/yyyy");
                    }
                    break;
                // Mês
                case 4:
                    {
                        retornosData[0] = DateTime.Now.AddMonths(-1).ToString("dd/MM/yyyy");
                        retornosData[1] = DateTime.Now.ToString("dd/MM/yyyy");
                    }
                    break;
                // 2 Meses
                case 5:
                    {
                        retornosData[0] = DateTime.Now.AddMonths(-2).ToString("dd/MM/yyyy");
                        retornosData[1] = DateTime.Now.ToString("dd/MM/yyyy");
                    }
                    break;
                // 6 Meses
                case 6:
                    {
                        retornosData[0] = DateTime.Now.AddMonths(-6).ToString("dd/MM/yyyy");
                        retornosData[1] = DateTime.Now.ToString("dd/MM/yyyy");
                    }
                    break;
                // Ano
                case 7:
                    {
                        retornosData[0] = DateTime.Now.AddYears(-1).ToString("dd/MM/yyyy");
                        retornosData[1] = DateTime.Now.ToString("dd/MM/yyyy");
                    }
                    break;
                // Personalizado
                case 8:
                    {
                        retornosData[0] = DateTime.Now.ToString("dd/MM/yyyy");
                        retornosData[1] = DateTime.Now.ToString("dd/MM/yyyy");
                    }
                    break;
            }

            return retornosData;
        }

        /// <summary>
        /// Retorna os valores números de uma STRING (correspondentes)
        /// </summary>
        /// <param name="nome">VALUE: Nome a ser tratado</param>
        /// <returns>STRING com os correspondentes númericos das letras do nome</returns>
        public static string RetornaValorNome(string nome)
        {
            RegexOptions options = RegexOptions.None;
            Regex regex = new Regex("[ ]{2,}", options);

            string retorno = String.Empty;

            string tratamento = "";
            nome = regex.Replace(nome, " ");
            string[] arrayNome = nome.ToUpper().Replace(" ", "|").Split('|');

            // CRIA UMA STRING COM O ARRAY DO NOME COMPLETO E COM PARCIAIS DOS BLOCOS DO ARRAY
            foreach (string item in arrayNome)
            {
                if (item.Length > 5)
                    tratamento += item.Substring(0, 5);
                else tratamento += item.Substring(0, (item.Length - 1));
            }

            // LÊ A STRING RETORNADA DO ARRAY E CONVERT A MESMA EM NUMÉRICO
            foreach (char c in tratamento)
            {
                if (Char.IsDigit(c)) retorno += c.ToString();
                else retorno += ((int)c).ToString();
            }

            return retorno;
        }
    }

    /// <summary>
    /// Classe que irá concentrar todas as chamadas de registros de logs
    /// </summary>
    public static class RegistraLog
    {
    }

    /// <summary>
    /// 
    /// </summary>
    public class RetornoComboBox
    {
        private string _valueMember;
        private string _displayMember;

        /// <summary>
        /// Valor ao selecionar a opção no DropList
        /// </summary>
        public string ValueMember
        {
            get
            {
                return _valueMember;
            }

            set
            {
                _valueMember = value;
            }
        }

        /// <summary>
        /// Valor exibido no DropList
        /// </summary>
        public string DisplayMember
        {
            get
            {
                return _displayMember;
            }

            set
            {
                _displayMember = value;
            }
        }

        public RetornoComboBox()
        {
            _valueMember = String.Empty;
            _displayMember = String.Empty;
        }
    }
}