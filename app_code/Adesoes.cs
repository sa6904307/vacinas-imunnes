﻿using MySql.Data.MySqlClient;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace ImunneVacinas
{
    /// <summary>
    /// Métodos e propriedades do elemento Adesao
    /// </summary>
    public class Adesao
    {
        private static string _tabela = "Adesoes";
        private static List<BD.Columns> columns;
        private static DateTime columnsUpdated;

        #region PROPRIEDADES

        private Participante _participante;
        private Campanha _campanha;
        private LocalAdesao _local;
        private int _codigo;
        private DateTime _dataAdesao;

        public Participante Participante
        {
            get
            {
                return _participante;
            }

            set
            {
                _participante = value;
            }
        }

        public Campanha Campanha
        {
            get
            {
                return _campanha;
            }

            set
            {
                _campanha = value;
            }
        }

        public LocalAdesao Local
        {
            get
            {
                return _local;
            }

            set
            {
                _local = value;
            }
        }

        public int Codigo
        {
            get
            {
                return _codigo;
            }

            set
            {
                _codigo = value;
            }
        }

        public DateTime DataAdesao
        {
            get
            {
                return _dataAdesao;
            }

            set
            {
                _dataAdesao = value;
            }
        }

        #endregion

        #region GETINFO

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos os campos da tabela.
        /// </summary>
        private static void GetInfoTabela()
        {
            if (columns == null || (DateTime.Now - columnsUpdated).Minutes > 0)
            {
                columns = BD.Columns.getColumnsInfo(_tabela);
                columnsUpdated = DateTime.Now;
            }
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo participante.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoParticipante()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "participante");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo campanha.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoCampanha()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "campanha");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo local.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoLocal()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "local");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo codigo.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoCodigo()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "codigo");
        }

        /// <summary>
        /// Busca informações, no banco de dados, sobre todos o campo dataadesao.
        /// </summary>
        /// <returns></returns>
        public static BD.Columns GetInfoDataAdesao()
        {
            GetInfoTabela();
            return columns.Find(x => x.Column_name == "dataadesao");
        }

        #endregion

        /// <summary> 
        /// Rotina que cria um Array com os parâmetros utilizados nas ações de INSERT, UPDATE e DELETE
        /// <returns>Array com vários MySqlParameters</returns>
        /// </summary>
        private Array GetParametrosSQL()
        {
            MySqlParameter p;
            ArrayList ap = new ArrayList();

            p = new MySqlParameter("@campanha", Parser.parseInteiro(_campanha.Codigo));
            p.MySqlDbType = GetInfoCampanha().Type;
            ap.Add(p);

            p = new MySqlParameter("@participante", Parser.parseInteiro(_participante.Codigo));
            p.MySqlDbType = GetInfoParticipante().Type;
            ap.Add(p);

            p = new MySqlParameter("@local", Parser.parseInteiro(_local.Codigo));
            p.MySqlDbType = GetInfoLocal().Type;
            ap.Add(p);

            p = new MySqlParameter("@codigo", Parser.parseInteiro(_codigo));
            p.MySqlDbType = GetInfoCodigo().Type;
            ap.Add(p);

            p = new MySqlParameter("@dataadesao", Parser.parseDateTimeBD(_dataAdesao));
            p.MySqlDbType = GetInfoDataAdesao().Type;
            ap.Add(p);

            return ap.ToArray();
        }

        /// <summary>
        /// Inicialização das propriedades do elemento com seus valores padrão
        /// </summary>
        private void IniciarElemento()
        {
            GetInfoTabela();
            _campanha = new Campanha();
            _participante = new Participante();
            _local = new LocalAdesao();

            _campanha.Codigo = -1;
            _participante.Codigo = -1;
            _local.Codigo = -1;
            _dataAdesao = Utils.dataPadrao;
        }

        /// <summary>
        /// Inicialização do elemento
        /// </summary>
        public Adesao()
        {
            IniciarElemento();
        }

        /// <summary>
        /// Rotina que insere o registro no BD
        /// </summary>
        /// <returns>Código do registro inserido no BD</returns>
        public int Incluir()
        {
            if (_campanha.Codigo <= 0 || _participante.Codigo <= 0 || _local.Codigo <= 0)
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    sql.Parameters.AddRange(GetParametrosSQL());
                    sql.CommandText = "INSERT INTO Adesoes "
                                    + "(campanha, participante, local, dataadesao) "
                                    + "VALUE (@campanha, @participante, @local, @dataadesao); "
                                    + "SELECT LAST_INSERT_ID();";
                    int resultado = Parser.parseInteiro(new Data().executeEscalar(sql));
                    return resultado;
                }
            }
            catch (ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Rotina que exclui o registro do BD
        /// </summary>
        /// <returns>Quantidade de linhas alteradas na ação junto ao BD</returns>
        public int Excluir()
        {
            if (_codigo < 0)
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    sql.Parameters.AddRange(GetParametrosSQL());
                    sql.CommandText = "DELETE FROM Adesoes "
                                    + "WHERE codigo = @codigo";
                    int resultado = Parser.parseInteiro(new Data().execute(sql));
                    return resultado;
                }
            }
            catch (ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Rotina que localiza e retorna um único registro, mediante o código informado
        /// </summary>
        /// <param name="codigo">VALUE: código de identificação do registro (obrigatório)</param>
        /// <returns>Elemento Usuario com os dados retornados da consulta</returns>
        public static Adesao ConsultarUnico(int codigo)
        {
            if (codigo < 0)
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            ImunneVacinas.PesquisaAdesao pesquisa = new PesquisaAdesao()
            {
                Codigo = codigo
            };

            List<Adesao> retorno = ConsultaGenerica(pesquisa);
            if (retorno == null || retorno.Count.Equals(0))
                return null;
            else
                return retorno[0];
        }


        /// <summary>
        /// Rotina que localiza e retorna um único registro, mediante o código informado
        /// </summary>
        /// <param name="codigo">VALUE: código de identificação do registro (obrigatório)</param>
        /// <returns>Elemento Usuario com os dados retornados da consulta</returns>
        public static Adesao ConsultarUnico(int campanha, int participante)
        {
            if (campanha < 0 || participante < 0)
                throw new Exception("S&A Imunizações | parâmetros com valores inválidos");

            ImunneVacinas.PesquisaAdesao pesquisa = new PesquisaAdesao()
            {
                Campanha = campanha,
                Participante = participante
            };

            List<Adesao> retorno = ConsultaGenerica(pesquisa);
            if (retorno == null || retorno.Count.Equals(0))
                return null;
            else
                return retorno[0];
        }

        /// <summary>
        /// Rotina que realiza a pesquisa de elementos mediante os parametros de pesquisa informados e retorna o total de itens encontrados
        /// </summary>
        /// <param name="pesquisa">VALUE: elemento de pesquisa com os parametros para consulta</param>
        /// <returns></returns>
        public static int ConsultarTotalAdesoes(PesquisaAdesao pesquisa)
        {
            int retorno = 0;

            List<Adesao> listaRetorno = ConsultaGenerica(pesquisa);
            if (listaRetorno != null && listaRetorno.Count > 0)
                retorno = listaRetorno.Count;

            return retorno;
        }

        /// <summary>
        /// Rotina que realiza a pesquisa de elementos mediante os parametros de pesquisa informados
        /// </summary>
        /// <param name="pesquisa">VALUE: elemento de pesquisa com os parametros para consulta</param>
        /// <returns></returns>
        public static List<Adesao> ConsultarAdesoes(PesquisaAdesao pesquisa)
        {
            return ConsultaGenerica(pesquisa);
        }

        /// <summary>
        /// Rotina interna utilizada para todas as pesquisas do elemento
        /// </summary>
        /// <param name="pesquisa">VALUE: Elemento com os parâmetros com os quais será realizada a pesquisa</param>
        /// <returns>Lista do elemento com os dados retornados da consulta</returns>
        internal static List<Adesao> ConsultaGenerica(PesquisaAdesao pesquisa)
        {
            StringBuilder ordenacao = new StringBuilder();
            StringBuilder filtro = new StringBuilder();
            StringBuilder top = new StringBuilder();
            MySqlParameter p;

            try
            {
                using (MySqlCommand sql = new MySqlCommand())
                {
                    if (pesquisa.Quantidade > 0)
                        top.AppendFormat(" LIMIT {0} ", pesquisa.Quantidade);

                    #region CAMPO CÓDIGO

                    if (pesquisa.Codigo >= 0)
                    {
                        filtro.Append(" AND Adesoes.codigo = @codigo");
                        p = new MySqlParameter("@codigo", pesquisa.Codigo);
                        p.MySqlDbType = GetInfoCodigo().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region CAMPO CAMPANHA

                    if (pesquisa.Campanha >= 0)
                    {
                        filtro.Append(" AND Adesoes.campanha = @campanha");
                        p = new MySqlParameter("@campanha", pesquisa.Campanha);
                        p.MySqlDbType = GetInfoCampanha().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region CAMPO CAMPO LIVRE

                    if (!String.IsNullOrEmpty(pesquisa.CampoLivre))
                    {
                        string campoLivre = String.Format("%{0}%", pesquisa.CampoLivre.Trim());
                        filtro.Append(" AND (LocaisAdesoes.nome LIKE @campoLivre ");
                        filtro.Append(" OR LocaisAdesoes.endereco LIKE @campoLivre ");
                        filtro.Append(" OR Participantes.nome LIKE @campoLivre ");
                        filtro.Append(")");
                        p = new MySqlParameter("@campoLivre", campoLivre);
                        p.MySqlDbType = MySqlDbType.VarChar;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region CAMPO PARTICIPANTE

                    if (pesquisa.Participante >= 0)
                    {
                        filtro.Append(" AND Adesoes.participante = @participante");
                        p = new MySqlParameter("@participante", pesquisa.Participante);
                        p.MySqlDbType = GetInfoParticipante().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    #region CAMPO LOCAL

                    if (pesquisa.Local >= 0)
                    {
                        filtro.Append(" AND Adesoes.local = @local");
                        p = new MySqlParameter("@local", pesquisa.Local);
                        p.MySqlDbType = GetInfoCampanha().Type;
                        sql.Parameters.Add(p);
                    }

                    #endregion

                    if (filtro.Length > 0)
                        filtro = filtro.Replace(" AND ", " WHERE ", 0, 5);

                    #region ORDENAÇÃO

                    switch (pesquisa.Ordenacao)
                    {
                        case Utils.TipoOrdenacao.porCodigo:
                            {
                                ordenacao.Append("ORDER BY Adesoes.codigo");
                            }
                            break;
                        case Utils.TipoOrdenacao.porCodigoDesc:
                            {
                                ordenacao.Append("ORDER BY Adesoes.codigo DESC");
                            }
                            break;
                        case Utils.TipoOrdenacao.porData:
                            {
                                ordenacao.Append("ORDER BY Adesoes.dataadesao");
                            }
                            break;
                        case Utils.TipoOrdenacao.porDataDesc:
                            {
                                ordenacao.Append("ORDER BY Adesoes.dataadesao DESC");
                            }
                            break;
                        case Utils.TipoOrdenacao.randomico:
                            {
                                ordenacao.Append("ORDER BY RAND()");
                            }
                            break;
                        default:
                            {
                                ordenacao.Append("ORDER BY Adesoes.codigo");
                            }
                            break;
                    }

                    #endregion

                    sql.CommandText = String.Format("SELECT "
                                    + "Adesoes.* "
                                    + "FROM Adesoes "
                                    + "LEFT JOIN LocaisAdesoes ON LocaisAdesoes.codigo = Adesoes.local "
                                    + "LEFT JOIN Participantes ON Participantes.codigo = Adesoes.participante "
                                    + "{0} {1} {2}", filtro, ordenacao, top);
                    DataTable dt = new Data().getData(sql);
                    return MontarRetorno(dt);
                }
            }
            catch (ImunneVacinas.ImunneVacinasDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ocorreu um erro ao executar o comando SQL.", ex);
            }
        }

        /// <summary>
        /// Rotina que retorna uma lista genérica do elemento, de acordo com o retorno da Consultar ao BD
        /// </summary>
        /// <param name="dt">VALUE: DataTable com o resultado da Consultar ao BD</param>
        /// <returns>Lista do elemento com os dados retornados da consulta</returns>
        internal static List<Adesao> MontarRetorno(DataTable dt)
        {
            int i = -1;
            List<Adesao> list = new List<Adesao>();

            if (dt == null && dt.Rows.Count == 0)
                return list;
            else
            {
                Adesao retorno;
                try
                {
                    for (i = 0; i < dt.Rows.Count; i++)
                    {
                        retorno = new Adesao()
                        {
                            _codigo = Parser.parseInteiro(dt.Rows[i]["codigo"]),
                            _dataAdesao = Convert.ToDateTime(dt.Rows[i]["dataadesao"])
                        };

                        retorno._participante.Codigo = Parser.parseInteiro(dt.Rows[i]["participante"]);
                        retorno._campanha.Codigo = Parser.parseInteiro(dt.Rows[i]["campanha"]);
                        retorno._local.Codigo = Parser.parseInteiro(dt.Rows[i]["local"]);

                        list.Add(retorno);
                    }
                    return list;
                }
                catch (Exception ex)
                {
                    ex.Data.Add("último retorno lido", i);
                    throw;
                }
            }
        }
    }
}