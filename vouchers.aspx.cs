﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class imunne_vouchers : System.Web.UI.Page
{
    protected StringBuilder urlPagina = new StringBuilder();

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            IniciarPagina();
    }

    /// <summary>
    /// 
    /// </summary>
    private void IniciarPagina()
    {
        int contadorInclusoes = 0;

        try
        {
            if (!String.IsNullOrEmpty(Request.QueryString["campanha"]))
            {
                int codigoParticipante = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["participante"]));
                int codigoCompra = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["compra"]));
                int codigoCampanha = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["campanha"]));
                int codigoClinica = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["clinica"]));
                int qtde = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["qtde"]));
                string[] membros = new string[1];
                string[] itens = new string[1];

                if (!String.IsNullOrEmpty(Request.QueryString["membros"]))
                    membros = ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["membros"]).Split('|');

                if (!String.IsNullOrEmpty(Request.QueryString["itens"]))
                    itens = ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["itens"]).Split('|');

                ImunneVacinas.Participante participanteSelecionado = ImunneVacinas.Participante.ConsultarUnico(codigoParticipante);
                ImunneVacinas.Compra compraSelecionada = ImunneVacinas.Compra.ConsultarUnico(codigoCompra);
                ImunneVacinas.Campanha campanhaSelecionada = ImunneVacinas.Campanha.ConsultarUnico(codigoCampanha);
                ImunneVacinas.Empresa empresaSelecionada = ImunneVacinas.Empresa.ConsultarUnico(campanhaSelecionada.IdEmpresa.Codigo);
                ImunneVacinas.Clinica clinicaSelecionada = ImunneVacinas.Clinica.ConsultarUnico(codigoClinica);

                Page.Title = "S&A Imunizações | Impressão de Vouchers";

                LblNascimento.Text = participanteSelecionado.DataNascimento.ToString("dd/MM/yyyy");
                LblNome.Text = participanteSelecionado.Nome.ToUpper();
                LblCPF.Text = ImunneVacinas.Utils.FormatarCpfCnpj(participanteSelecionado.Cpf);

                LblCampanha.Text = campanhaSelecionada.Identificacao.ToUpper() + " | " + campanhaSelecionada.Nome.ToUpper();
                LblValidade.Text = campanhaSelecionada.DataLimite.ToString("dd/MM/yyyy");
                LblClinica.Text = clinicaSelecionada.NomeFantasia.ToUpper() + "<br />"
                                + clinicaSelecionada.Endereco.ToUpper() + ", "
                                + clinicaSelecionada.Numero + " - "
                                + clinicaSelecionada.IdCidade.Nome.ToUpper() + "/"
                                + clinicaSelecionada.IdUf.Sigla;
                LblEmpresa.Text = campanhaSelecionada.IdEmpresa.NomeFantasia.ToUpper();

                ImunneVacinas.PesquisaVoucher pesquisaVoucher = new ImunneVacinas.PesquisaVoucher()
                {
                    Campanha = codigoCampanha,
                    Clinica = codigoClinica,
                    Compra = codigoCompra
                };

                List<ImunneVacinas.Voucher> listaVales = ImunneVacinas.Voucher.ConsultarVouchers(pesquisaVoucher);

                if (listaVales != null && listaVales.Count > 0)
                {
                    RptPrincipal.DataSource = listaVales;
                    RptPrincipal.DataBind();
                }
                else
                {
                    PnlBack.Visible = false;

                    #region VOUCHERS

                    foreach (string membro in membros)
                    {
                        ImunneVacinas.Participante membroSelecionado = ImunneVacinas.Participante.ConsultarUnico(Convert.ToInt32(membro));

                        StringBuilder descricaoVoucher = new StringBuilder();
                        descricaoVoucher.AppendFormat("{0} ADQUIRIU PRODUTOS DA CAMPANHA {1}", membroSelecionado.Nome.ToUpper(), campanhaSelecionada.Identificacao.ToUpper().ToString());
                        descricaoVoucher.AppendFormat(" QUE SERÃO APLICADOS NA CLÍNICA {0}.\r\nABAIXO OS PRODUTOS:\r\n\r\n", clinicaSelecionada.NomeFantasia.ToUpper());

                        foreach (string item in itens)
                        {
                            ImunneVacinas.ProdutoCampanha produtoSelecionado = ImunneVacinas.ProdutoCampanha.ConsultarUnico(Convert.ToInt32(item));
                            descricaoVoucher.AppendFormat("1 UNIDADE(S) DE {0}.\r\n", produtoSelecionado.IdVacina.Nome.ToUpper());
                        }

                        descricaoVoucher.AppendFormat("\r\nCOMPRAS REALIZADAS POR {0}.", participanteSelecionado.Nome.ToUpper());

                        ImunneVacinas.Voucher novoVoucher = new ImunneVacinas.Voucher();
                        novoVoucher.IdCampanha.Codigo = codigoCampanha;
                        novoVoucher.IdClinica.Codigo = codigoClinica;
                        novoVoucher.IdCompra.Codigo = codigoCompra;
                        novoVoucher.IdParticipante.Codigo = membroSelecionado.Codigo;
                        novoVoucher.DataCriacao = DateTime.Now;
                        novoVoucher.Descricao = descricaoVoucher.ToString();
                        novoVoucher.Situacao = ImunneVacinas.Voucher.SituacaoVoucher.Gerado;

                        try
                        {
                            contadorInclusoes = 1;
                            novoVoucher.Incluir();
                        }
                        catch (Exception ex)
                        {
                            ImunneVacinas.Erro erro = new ImunneVacinas.Erro();
                            erro.AdicionarLog(ex);
                        }
                    }

                    ImunneVacinas.PesquisaVoucher pesquisa = new ImunneVacinas.PesquisaVoucher()
                    {
                        Campanha = codigoCampanha,
                        Clinica = codigoClinica,
                        Compra = codigoCompra,
                        Ordenacao = ImunneVacinas.Utils.TipoOrdenacao.porData
                    };

                    listaVales = ImunneVacinas.Voucher.ConsultarVouchers(pesquisa);
                    RptPrincipal.DataSource = listaVales;
                    RptPrincipal.DataBind();

                    #endregion
                }

                contadorInclusoes = 1;

                // Envio dos e-mails ao realizar nova compra
                if (contadorInclusoes > 0)
                {
                    // Validando o e-mail do participante
                    if (!String.IsNullOrEmpty(participanteSelecionado.Email) && ImunneVacinas.Utils.ValidarEmail(participanteSelecionado.Email))
                    {
                        // 04/12/2019 - GUILHERME PETRACA
                        // SÃO DUAS ESTRUTURAS DE ENVIO, UMA PARA SURTOS COM A LIBERAÇÃO JÁ PRONTA E UMA PARA CAMPANHAS QUE TERÃO A LIBERAÇÃO
                        // PELA CLÍNICA.

                        try
                        {
                            StringBuilder nomesBeneficiarios = new StringBuilder();
                            StringBuilder tituloEmail = new StringBuilder();

                            // Verificando todos os vales da compra
                            foreach (ImunneVacinas.Voucher item in listaVales)
                            {
                                ImunneVacinas.Participante beneficiario = ImunneVacinas.Participante.ConsultarUnico(item.IdParticipante.Codigo);
                                nomesBeneficiarios.AppendFormat("{0} | {1}<br />", beneficiario.Nome.ToUpper(), beneficiario.Parentesco.Nome.ToUpper());
                            }

                            StringBuilder conteudoCorpo = new StringBuilder();
                            bool envioEmail = false;

                            if (campanhaSelecionada.Surto == 1)
                                envioEmail = ImunneVacinas.Email.EnviarEmail(participanteSelecionado.Email, "S&A Imunizações | Confirmação de compra", ImunneVacinas.Email.ValesSurto(compraSelecionada, nomesBeneficiarios.ToString()));
                            else
                                envioEmail = ImunneVacinas.Email.EnviarEmail(participanteSelecionado.Email, "S&A Imunizações | Confirmação de compra", ImunneVacinas.Email.ValesNaoSurto(compraSelecionada, nomesBeneficiarios.ToString()));

                            if (envioEmail)
                            {
                                if (campanhaSelecionada.Surto == 1)
                                {
                                    compraSelecionada.DataLiberacao = DateTime.Now;
                                    compraSelecionada.Alterar();
                                }

                                ImunneVacinas.Alerta.showMensagemSucesso(this.Page, "Sucesso", String.Format("Foi encaminhado para seu e-mail {0} uma mensagem com as informações de sua compra.", participanteSelecionado.Email));
                            }
                        }
                        catch (Exception ex)
                        {
                            ImunneVacinas.Erro erro = new ImunneVacinas.Erro();
                            erro.addArquivo(ex);
                        }
                    }
                    else
                    {
                        LnkImpressao.Visible = false;

                        ImunneVacinas.Alerta.showMensagemAviso(this.Page, "S&A Imunizações", "Você não possui um e-mail cadastro para receber a confirmação da compra ~e/ou a liberação da mesma.");
                        return;
                    }
                }
            }
            else
            {
                Response.Redirect("~/", false);
                Context.ApplicationInstance.CompleteRequest();
            }
        }
        catch (Exception ex)
        {
            ImunneVacinas.Erro erro = new ImunneVacinas.Erro();
            erro.addArquivo(ex);
            Response.Redirect("~/", false);
            Context.ApplicationInstance.CompleteRequest();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void RptPrincipal_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void RptPrincipal_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        StringBuilder infoBeneficiario = new StringBuilder();
        ImunneVacinas.Voucher item = (ImunneVacinas.Voucher)e.Item.DataItem;
        urlPagina = new StringBuilder();

        Label LblNomeParticipante = (Label)e.Item.FindControl("LblNomeParticipante");
        Label LblNomeCampanha = (Label)e.Item.FindControl("LblNomeCampanha");
        Label LblNomeClinica = (Label)e.Item.FindControl("LblNomeClinica");
        HyperLink LnkImprimir = (HyperLink)e.Item.FindControl("LnkImprimir");

        string transacaoID = ImunneVacinas.TransacaoRealizada.ConsultarUnicoCompra(item.IdCompra.Codigo).numeroComprovanteVenda;

        infoBeneficiario.AppendFormat("{0}", item.IdParticipante.Nome.ToUpper());

        #region DADOS HORÁRIOS

        ImunneVacinas.HorarioSurto horarioAgendado = ImunneVacinas.HorarioSurto.ConsultarUnico(item.IdCompra.Codigo, item.IdParticipante.Codigo);
        if (horarioAgendado != null)
            infoBeneficiario.AppendFormat(" - AGENDADO PARA {0}", horarioAgendado.InfoHoraro.ToString("dd/MM/yyyy HH:mm"));

        #endregion

        LblNomeParticipante.Text = infoBeneficiario.ToString().ToUpper();
        LblNomeCampanha.Text = item.IdCampanha.Nome;
        LblNomeClinica.Text = item.IdClinica.NomeFantasia;

        LnkImprimir.NavigateUrl = String.Format("~/prints/voucher.aspx?codigo={0}", ImunneVacinas.Criptografia.Criptografar(item.Codigo.ToString()));

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void LnkImpressao_Click(object sender, EventArgs e)
    {
        ReenviarEmail();
    }

    /// <summary>
    /// 
    /// </summary>
    private void ReenviarEmail()
    {
        int codigoParticipante = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["participante"]));
        int codigoCompra = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["compra"]));
        int codigoCampanha = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["campanha"]));
        int codigoClinica = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["clinica"]));
        int qtde = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["qtde"]));
        string[] membros = new string[1];
        string[] itens = new string[1];

        if (!String.IsNullOrEmpty(Request.QueryString["membros"]))
            membros = ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["membros"]).Split('|');

        if (!String.IsNullOrEmpty(Request.QueryString["itens"]))
            itens = ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["itens"]).Split('|');

        ImunneVacinas.Participante participanteSelecionado = ImunneVacinas.Participante.ConsultarUnico(codigoParticipante);
        ImunneVacinas.Compra compraSelecionada = ImunneVacinas.Compra.ConsultarUnico(codigoCompra);
        ImunneVacinas.Campanha campanhaSelecionada = ImunneVacinas.Campanha.ConsultarUnico(codigoCampanha);
        ImunneVacinas.Empresa empresaSelecionada = ImunneVacinas.Empresa.ConsultarUnico(campanhaSelecionada.IdEmpresa.Codigo);
        ImunneVacinas.Clinica clinicaSelecionada = ImunneVacinas.Clinica.ConsultarUnico(codigoClinica);

        Page.Title = "S&A Imunizações | Impressão de Vouchers";

        LblNascimento.Text = participanteSelecionado.DataNascimento.ToString("dd/MM/yyyy");
        LblNome.Text = participanteSelecionado.Nome.ToUpper();
        LblCPF.Text = ImunneVacinas.Utils.FormatarCpfCnpj(participanteSelecionado.Cpf);

        LblCampanha.Text = campanhaSelecionada.Identificacao.ToUpper() + " | " + campanhaSelecionada.Nome.ToUpper();
        LblValidade.Text = campanhaSelecionada.DataLimite.ToString("dd/MM/yyyy");
        LblClinica.Text = clinicaSelecionada.NomeFantasia.ToUpper() + "<br />"
                        + clinicaSelecionada.Endereco.ToUpper() + ", "
                        + clinicaSelecionada.Numero + " - "
                        + clinicaSelecionada.IdCidade.Nome.ToUpper() + "/"
                        + clinicaSelecionada.IdUf.Sigla;
        LblEmpresa.Text = campanhaSelecionada.IdEmpresa.NomeFantasia.ToUpper();

        ImunneVacinas.PesquisaVoucher pesquisaVoucher = new ImunneVacinas.PesquisaVoucher()
        {
            Campanha = codigoCampanha,
            Clinica = codigoClinica,
            Compra = codigoCompra
        };

        List<ImunneVacinas.Voucher> listaVales = ImunneVacinas.Voucher.ConsultarVouchers(pesquisaVoucher);

        // Validando o e-mail do participante
        if (!String.IsNullOrEmpty(participanteSelecionado.Email) && ImunneVacinas.Utils.ValidarEmail(participanteSelecionado.Email))
        {
            // 04/12/2019 - GUILHERME PETRACA
            // SÃO DUAS ESTRUTURAS DE ENVIO, UMA PARA SURTOS COM A LIBERAÇÃO JÁ PRONTA E UMA PARA CAMPANHAS QUE TERÃO A LIBERAÇÃO
            // PELA CLÍNICA.

            try
            {
                StringBuilder nomesBeneficiarios = new StringBuilder();
                StringBuilder tituloEmail = new StringBuilder();

                // Verificando todos os vales da compra
                foreach (ImunneVacinas.Voucher item in listaVales)
                {
                    ImunneVacinas.Participante beneficiario = ImunneVacinas.Participante.ConsultarUnico(item.IdParticipante.Codigo);
                    nomesBeneficiarios.AppendFormat("{0} | {1}<br />", beneficiario.Nome.ToUpper(), beneficiario.Parentesco.Nome.ToUpper());
                }

                StringBuilder conteudoCorpo = new StringBuilder();
                bool envioEmail = false;

                if (campanhaSelecionada.Surto == 1)
                    envioEmail = ImunneVacinas.Email.EnviarEmail(participanteSelecionado.Email, "S&A Imunizações | Confirmação de compra", ImunneVacinas.Email.ValesSurto(compraSelecionada, nomesBeneficiarios.ToString()));
                else
                    envioEmail = ImunneVacinas.Email.EnviarEmail(participanteSelecionado.Email, "S&A Imunizações | Confirmação de compra", ImunneVacinas.Email.ValesNaoSurto(compraSelecionada, nomesBeneficiarios.ToString()));

                if (envioEmail)
                {
                    if (campanhaSelecionada.Surto == 1)
                    {
                        compraSelecionada.DataLiberacao = DateTime.Now;
                        compraSelecionada.Alterar();
                    }

                    ImunneVacinas.Alerta.showMensagemSucesso(this.Page, "Sucesso", String.Format("Foi encaminhado para seu e-mail {0} uma mensagem com as informações de sua compra.", participanteSelecionado.Email));
                }
            }
            catch (Exception ex)
            {
                ImunneVacinas.Erro erro = new ImunneVacinas.Erro();
                erro.addArquivo(ex);
            }
        }
        else
        {
            LnkImpressao.Visible = false;

            ImunneVacinas.Alerta.showMensagemAviso(this.Page, "S&A Imunizações", "Você não possui um e-mail cadastro para receber a confirmação da compra ~e/ou a liberação da mesma.");
            return;
        }
    }
}