﻿using System;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class imunne_produtos : System.Web.UI.Page
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            IniciarPagina();
    }

    /// <summary>
    /// 
    /// </summary>
    private void IniciarPagina()
    {
        int codigoCampanha = -1;

        try
        {
            if (!String.IsNullOrEmpty(Request.QueryString["campanha"]))
            {
                codigoCampanha = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["campanha"]));
                int codigoParticipante = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["participante"]));
                int codigoClinica = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["clinica"]));

                ImunneVacinas.Participante participanteSelecionado = ImunneVacinas.Participante.ConsultarUnico(codigoParticipante);
                ImunneVacinas.Campanha campanhaSelecionada = ImunneVacinas.Campanha.ConsultarUnico(codigoCampanha);
                ImunneVacinas.Clinica clinicaSelecionada = ImunneVacinas.Clinica.ConsultarUnico(codigoClinica);

                LblNascimento.Text = participanteSelecionado.DataNascimento.ToString("dd/MM/yyyy");
                LblNome.Text = participanteSelecionado.Nome.ToUpper();
                LblCPF.Text = ImunneVacinas.Utils.FormatarCpfCnpj(participanteSelecionado.Cpf);

                LblCampanha.Text = campanhaSelecionada.Identificacao.ToUpper() + " | " + campanhaSelecionada.Nome.ToUpper();
                LblValidade.Text = campanhaSelecionada.DataLimite.ToString("dd/MM/yyyy");
                LblClinica.Text = clinicaSelecionada.NomeFantasia.ToUpper() + "<br />"
                                + clinicaSelecionada.Endereco.ToUpper() + ", "
                                + clinicaSelecionada.Numero + " - "
                                + clinicaSelecionada.IdCidade.Nome.ToUpper() + "/"
                                + clinicaSelecionada.IdUf.Sigla;
                LblEmpresa.Text = campanhaSelecionada.IdEmpresa.NomeFantasia.ToUpper();


                ImunneVacinas.PesquisaProdutoCampanha pesquisa = new ImunneVacinas.PesquisaProdutoCampanha()
                {
                    Campanha = codigoCampanha,
                    Ordenacao = ImunneVacinas.Utils.TipoOrdenacao.porNome
                };

                RptPrincipal.DataSource = ImunneVacinas.ProdutoCampanha.ConsultarProdutoCampanhas(pesquisa);
                RptPrincipal.DataBind();

                Page.Title = "S&A Imunizações | Produtos da Campanha " + campanhaSelecionada.Nome.ToUpper();

                DdlQuantidades.Items.Add(new ListItem("Selecione", "-1"));
                for (int i = 1; i <= 30; i++)
                {
                    DdlQuantidades.Items.Add(new ListItem(i.ToString(), i.ToString()));
                }
            }
            else
            {
                Response.Redirect("~/", false);
                Context.ApplicationInstance.CompleteRequest();
            }
        }
        catch (Exception ex)
        {
            Response.Redirect("~/", false);
            Context.ApplicationInstance.CompleteRequest();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void RptPrincipal_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        int codigoCampanha = -1;
        int codigoClinica = -1;

        codigoCampanha = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["campanha"]));
        codigoClinica = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["clinica"]));

        ImunneVacinas.ProdutoCampanha item = (ImunneVacinas.ProdutoCampanha)e.Item.DataItem;
        ImunneVacinas.Vacina vacinaSelecionada = ImunneVacinas.Vacina.ConsultarUnico(item.IdVacina.Codigo);
        ImunneVacinas.Clinica clinicaSelecionada = ImunneVacinas.Clinica.ConsultarUnico(codigoClinica);

        Label LblNomeProduto = (Label)e.Item.FindControl("LblNomeProduto");
        Label LblPreco = (Label)e.Item.FindControl("LblPreco");
        Label LblNomeCampanha = (Label)e.Item.FindControl("LblNomeCampanha");
        Label LblNomeClinica = (Label)e.Item.FindControl("LblNomeClinica");
        HiddenField HdfCodigo = (HiddenField)e.Item.FindControl("HdfCodigo");

        LblNomeProduto.Text = vacinaSelecionada.Nome;
        LblPreco.Text = ImunneVacinas.Utils.ToMoeda(item.Preco);
        LblNomeCampanha.Text = item.IdCampanha.Nome;
        LblNomeClinica.Text = clinicaSelecionada.NomeFantasia;

        // Armazenando o código do item da campanha
        HdfCodigo.Value = item.Codigo.ToString();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void LnkContinuar_Click(object sender, EventArgs e)
    {
        int totalChecks = 0;
        int qtde = Convert.ToInt32(DdlQuantidades.SelectedValue);
        StringBuilder codigosProdutos = new StringBuilder();
        StringBuilder urlPagina = new StringBuilder();
        StringBuilder parametros = new StringBuilder();

        foreach (RepeaterItem item in RptPrincipal.Items)
        {
            HiddenField HdfCodigo = (HiddenField)item.FindControl("HdfCodigo");
            CheckBox ChkSelecionar = (CheckBox)item.FindControl("ChkSelecionar");
            if (ChkSelecionar.Checked)
            {
                codigosProdutos.AppendFormat("{0}|", HdfCodigo.Value);
                totalChecks += 1;
            }
        }

        if (totalChecks <= 0)
        {
            ImunneVacinas.Alerta.showMensagemErro(this.Page, "Aviso", "Selecione ao menos um dos produtos disponíveis.");
            return;
        }

        codigosProdutos.Remove((codigosProdutos.Length - 1), 1);

        urlPagina.AppendFormat("~/beneficiarios.aspx?campanha={0}", Request.QueryString["campanha"]);
        urlPagina.AppendFormat("&clinica={0}", Request.QueryString["clinica"]);
        urlPagina.AppendFormat("&participante={0}", Request.QueryString["participante"]);
        urlPagina.AppendFormat("&itens={0}", ImunneVacinas.Criptografia.Criptografar(codigosProdutos.ToString()));
        urlPagina.AppendFormat("&qtde={0}", ImunneVacinas.Criptografia.Criptografar(DdlQuantidades.SelectedValue));
        Response.Redirect(urlPagina.ToString(), false);
        Context.ApplicationInstance.CompleteRequest();
    }
}