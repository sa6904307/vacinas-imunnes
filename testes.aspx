﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="testes.aspx.cs" Inherits="testes" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style>
        * {
            margin: 0;
            padding: 0;
        }
        body {
            font-family: Arial;
        }
        .form-control {
            padding: 5px;
            border: solid 1px #666666;
            width: 600px;
        }

        .btn {
            background-color: #154887;
            color: #fff;
            text-transform: uppercase;
            text-align: center;
            padding: 15px;
            border: none;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:TextBox ID="TxtEntrada"  runat="server" CssClass="form-control"></asp:TextBox><br /><br />
            <asp:TextBox ID="TxtSaida"  runat="server" CssClass="form-control"></asp:TextBox><br /><br />
            <asp:Button ID="BtnTratar" runat="server" Text="Tratar" CssClass="btn" OnClick="BtnTratar_Click" />
        </div>
    </form>
</body>
</html>
