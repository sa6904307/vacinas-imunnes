﻿<%@ Page Title="" Language="C#" MasterPageFile="~/imunne.master" AutoEventWireup="true" CodeFile="identificacao.aspx.cs" Inherits="imunne_identificacao" MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js" type="text/javascript"></script>
    <link href="../vendors/datepicker/css/datepicker3.css" rel="stylesheet" type="text/css" />
    <link href="../vendors/daterangepicker/css/daterangepicker-bs3.css" rel="stylesheet"
        type="text/css" />
    <link href="../vendors/timeticker/css/bootstrap-timepicker.min.css" rel="stylesheet"
        type="text/css" />
    <script src="../vendors/daterangepicker/js/daterangepicker.js" type="text/javascript"></script>
    <script src="../vendors/datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
    <script src="../vendors/timeticker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
    <script src="../vendors/datepicker/locales/bootstrap-datepicker.pt-BR.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section class="subheader">
        <div class="container">
            <div class="row">
                <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                    <i class="fa fa-users"></i>&nbsp;Identificação
                </div>
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 text-right">
                    <a href="javascript:history.back()" title="Voltar à página anterior">
                        <i class="fa fa-chevron-left"></i>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
                <div class="caixa-branca">
                    <div class="step-header">
                        INFORMAÇÕES DA CAMPANHA
                    </div>
                    <div class="box-inside">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                            <label>CAMPANHA</label>
                            <asp:Label ID="LblCampanha" runat="server" CssClass="label-identificacao"></asp:Label>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                            <label>CLÍNICA</label>
                            <asp:Label ID="LblClinica" runat="server" CssClass="label-identificacao"></asp:Label>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                            <label>EMPRESA</label>
                            <asp:Label ID="LblEmpresa" runat="server" CssClass="label-identificacao"></asp:Label>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                            <label>VALIDADE</label>
                            <asp:Label ID="LblValidade" runat="server" CssClass="label-identificacao"></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                <div class="caixa-branca">
                    <asp:UpdatePanel ID="UpdIdentificacao" runat="server">
                        <ContentTemplate>
                            <script type="text/javascript">
                                Sys.Application.add_load(SetDatePicker);
                                function SetDatePicker() {
                                    $('.datepicker').datepicker({
                                        todayHighlight: true,
                                        language: "pt-BR"
                                    });
                                }
                            </script>
                            <asp:Panel ID="PnlIdentificacao" runat="server">
                                <div class="step-header">
                                    IDENTIFICAÇÃO
                                </div>
                                <div class="box-inside">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group text-center">
                                        <p>
                                            Para continuar precisamos localizar seu cadastro. Informe os números de seu <b>CPF</b> sem pontos ou traços.
                                        </p>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                        <label>Informe seu CPF *</label>
                                        <asp:TextBox ID="TxtCpfCnpjIdentificacao" runat="server" CssClass="form-control" placeHolder="CPF" OnKeyPress="mascara(this,cpfCnpj)" MaxLength="18"></asp:TextBox>
                                    </div>
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="PnlCadastro" runat="server" Visible="false">
                                <div class="step-header">
                                    DADOS CADASTRAIS
                                </div>
                                <div class="box-inside">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group text-center">
                                        <p>
                                            Não localizamos seu cadastro de acordo com o <b>CPF</b> informado.
                                        Preencha os demais campos para concluir seu registro e continuar a aquisição de vacinas.
                                        </p>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 form-group">
                                        <label>Informe seu CPF *</label>
                                        <asp:TextBox ID="TxtCpfCadastro" runat="server" CssClass="form-control" OnKeyPress="mascara(this,cpfCnpj)" MaxLength="18"></asp:TextBox>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 form-group text-left">
                                        <label>Data de Nascimento *</label>
                                        <asp:TextBox ID="TxtDataNascimento" runat="server" CssClass="form-control text-center" data-date-format="dd/mm/yyyy" OnKeyPress="mascara(this, mdata)" MaxLength="10"></asp:TextBox>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group text-left">
                                        <label>Nome *</label>
                                        <asp:TextBox ID="TxtNome" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 form-group text-left">
                                        <label>E-mail *</label>
                                        <asp:TextBox ID="txtEmail"  placeholder="teste@teste.com" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 form-group text-left">
                                        <label>Telefone *</label>
                                        <asp:TextBox ID="TxtTelefone" placeholder="ex.: 11933221122" runat="server" CssClass="form-control" onkeypress="mascara(this, mtel)" MaxLength="15"></asp:TextBox>
                                    </div>
                                </div>
                            </asp:Panel>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="TxtCpfCnpjIdentificacao" />
                            <asp:AsyncPostBackTrigger ControlID="TxtCpfCadastro" />
                        </Triggers>
                    </asp:UpdatePanel>
                    <asp:UpdatePanel ID="UpdValidacao" runat="server">
                        <ContentTemplate>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group text-center">
                                <asp:LinkButton ID="LnkContinuar" runat="server" CssClass="btn btn-block btn-primary" OnClick="LnkContinuar_Click">
                                    <i class="fa fa-chevron-right"></i>&nbsp;Continuar
                                </asp:LinkButton>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="TxtCpfCnpjIdentificacao" />
                            <asp:AsyncPostBackTrigger ControlID="TxtCpfCadastro" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
