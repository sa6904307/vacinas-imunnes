﻿using System;
using System.Text;
using System.Web.UI.WebControls;

public partial class opcoes : System.Web.UI.Page
{
    protected StringBuilder urlPagina = new StringBuilder();

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            IniciarPagina();
    }

    /// <summary>
    /// 
    /// </summary>
    private void IniciarPagina()
    {
        int codigoCampanha = -1;

        try
        {
            if (!String.IsNullOrEmpty(Request.QueryString["campanha"]))
            {
                codigoCampanha = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["campanha"]));
                int codigoParticipante = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["participante"]));
                int codigoClinica = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["clinica"]));

                ImunneVacinas.Participante participanteSelecionado = ImunneVacinas.Participante.ConsultarUnico(codigoParticipante);
                ImunneVacinas.Campanha campanhaSelecionada = ImunneVacinas.Campanha.ConsultarUnico(codigoCampanha);
                ImunneVacinas.Clinica clinicaSelecionada = ImunneVacinas.Clinica.ConsultarUnico(codigoClinica);

                LblNascimento.Text = participanteSelecionado.DataNascimento.ToString("dd/MM/yyyy");
                LblNome.Text = participanteSelecionado.Nome.ToUpper();
                LblCPF.Text = ImunneVacinas.Utils.FormatarCpfCnpj(participanteSelecionado.Cpf);

                LblCampanha.Text = campanhaSelecionada.Identificacao.ToUpper() + " | " + campanhaSelecionada.Nome.ToUpper();
                LblValidade.Text = campanhaSelecionada.DataLimite.ToString("dd/MM/yyyy");
                LblClinica.Text = clinicaSelecionada.NomeFantasia.ToUpper() + "<br />"
                                + clinicaSelecionada.Endereco.ToUpper() + ", "
                                + clinicaSelecionada.Numero + " - "
                                + clinicaSelecionada.IdCidade.Nome.ToUpper() + "/"
                                + clinicaSelecionada.IdUf.Sigla;
                LblEmpresa.Text = campanhaSelecionada.IdEmpresa.NomeFantasia.ToUpper();

                ImunneVacinas.PesquisaCompra pesquisaCompra = new ImunneVacinas.PesquisaCompra()
                {
                    Clinica = codigoClinica,
                    Campanha = codigoCampanha,
                    Participante = codigoParticipante,
                    Categoria = ImunneVacinas.Compra.CategoriaCompra.Paga,
                    Ordenacao = ImunneVacinas.Utils.TipoOrdenacao.porDataDesc,
                    Valor = true
                };

                Page.Title = "S&A Imunizações | Escolha a opção desejada";

                RptPrincipal.DataSource = ImunneVacinas.Compra.ConsultarComprar(pesquisaCompra);
                RptPrincipal.DataBind();

                //urlPagina = new StringBuilder();
                //urlPagina.AppendFormat("~/prints/vale.aspx?campanha={0}", Request.QueryString["campanha"]);
                //urlPagina.AppendFormat("&clinica={0}", Request.QueryString["clinica"]);
                //urlPagina.AppendFormat("&participante={0}", Request.QueryString["participante"]);
                //LnkMeuVale.NavigateUrl = urlPagina.ToString();
            }
            else
            {
                Response.Redirect("~/", false);
                Context.ApplicationInstance.CompleteRequest();
            }
        }
        catch (Exception ex)
        {

        }
    }

    /// <summary>
    /// 
    /// </summary>
    protected void LnkComprarNovos_Click(object sender, EventArgs e)
    {
        urlPagina.AppendFormat("~/produtos.aspx?campanha={0}", Request.QueryString["campanha"]);
        urlPagina.AppendFormat("&clinica={0}", Request.QueryString["clinica"]);
        urlPagina.AppendFormat("&participante={0}", Request.QueryString["participante"]);
        Response.Redirect(urlPagina.ToString(), false);
        Context.ApplicationInstance.CompleteRequest();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void RptPrincipal_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
    {
        ImunneVacinas.Compra registroSelecionado = (ImunneVacinas.Compra)e.Item.DataItem;
        ImunneVacinas.TransacaoRealizada transacaoSelecionada = ImunneVacinas.TransacaoRealizada.ConsultarUnicoCompra(registroSelecionado.Codigo);

        Label LblData = (Label)e.Item.FindControl("LblData");
        Label LblID = (Label)e.Item.FindControl("LblID");
        Label LblValor = (Label)e.Item.FindControl("LblValor");
        HyperLink LnkCompra = (HyperLink)e.Item.FindControl("LnkCompra");

        LblID.Text = "NÃO CONCLUÍDA";

        if (transacaoSelecionada != null)
        {
            if (!String.IsNullOrEmpty(transacaoSelecionada.numeroComprovanteVenda))
                LblID.Text = transacaoSelecionada.numeroComprovanteVenda;
        }

        LblValor.Text = ImunneVacinas.Utils.ToMoeda(registroSelecionado.Valor);
        LblData.Text = registroSelecionado.DataCompra.ToString("dd/MM/yyyy");

        urlPagina = new StringBuilder();
        urlPagina.AppendFormat("~/vouchers.aspx?campanha={0}", Request.QueryString["campanha"]);
        urlPagina.AppendFormat("&clinica={0}", Request.QueryString["clinica"]);
        urlPagina.AppendFormat("&compra={0}", ImunneVacinas.Criptografia.Criptografar(registroSelecionado.Codigo.ToString()));
        urlPagina.AppendFormat("&qtde={0}", ImunneVacinas.Criptografia.Criptografar("0"));
        urlPagina.AppendFormat("&participante={0}", Request.QueryString["participante"]);
        LnkCompra.NavigateUrl = urlPagina.ToString();

        if (LblID.Text == "NÃO CONCLUÍDA")
            LnkCompra.Enabled = false;
    }
}