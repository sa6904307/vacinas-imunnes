﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.WebControls;

public partial class popups_detalhes_cursos : System.Web.UI.Page
{
    decimal totalQtde = 0;
    decimal totalValor = 0;

    public class ItemPedido
    {
        public string Pedido { get; set; }
        public string Comprovante { get; set; }
        public string Data { get; set; }
        public string Beneficiario { get; set; }
        public string Vacina { get; set; }
        public string Qtde { get; set; }
        public string Valor { get; set; }
        public string Aplicacao { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            IniciarPagina();
    }

    /// <summary>
    /// 
    /// </summary>
    private void IniciarPagina()
    {
        try
        {
            StringBuilder htmlConteudo = new StringBuilder();

            int codigoRegistro = Convert.ToInt32(Request.QueryString["compra"]);
            ImunneVacinas.Compra compraSelecionada = ImunneVacinas.Compra.ConsultarUnico(codigoRegistro);
            ImunneVacinas.TransacaoRealizada transacaoSelecionada = ImunneVacinas.TransacaoRealizada.ConsultarUnicoCompra(compraSelecionada.Codigo);
            ImunneVacinas.Campanha campanhaSelecionada = ImunneVacinas.Campanha.ConsultarUnico(compraSelecionada.IdCampanha.Codigo);
            ImunneVacinas.Clinica clinicaSelecionada = ImunneVacinas.Clinica.ConsultarUnico(compraSelecionada.IdClinica.Codigo);
            ImunneVacinas.Empresa empresaSelecionada = ImunneVacinas.Empresa.ConsultarUnico(campanhaSelecionada.IdEmpresa.Codigo);

            LblCampanha.Text = String.Format("{0} | {1}", campanhaSelecionada.Identificacao, campanhaSelecionada.Nome.ToUpper());

            StringBuilder dadosClinica = new StringBuilder();
            StringBuilder dadosEmpresa = new StringBuilder();

            #region DADOS COMPRA

            if (transacaoSelecionada != null)
                LblComprovante.Text = transacaoSelecionada.numeroComprovanteVenda;
            else LblComprovante.Text = "NÃO LOCALIZADO";

            LblDataCompra.Text = compraSelecionada.DataCompra.ToString("dd/MM/yyyy HH:mm:ss");
            LblValorCompra.Text = ImunneVacinas.Utils.ToMoeda(compraSelecionada.Valor);

            #endregion

            #region DADOS COMPRA

            LblNomeTitular.Text = compraSelecionada.IdParticipante.Nome.ToUpper();
            LblCategoriaCompra.Text = compraSelecionada.Categoria.ToString().ToUpper();

            ImunneVacinas.PesquisaVoucher pesquisaVoucher = new ImunneVacinas.PesquisaVoucher()
            {
                Compra = compraSelecionada.Codigo,
                Situacao = ImunneVacinas.Voucher.SituacaoVoucher.Utilizado
            };
            List<ImunneVacinas.Voucher> listaVouchers = ImunneVacinas.Voucher.ConsultarVouchers(pesquisaVoucher);
            if (listaVouchers != null && listaVouchers.Count > 0)
                LblDataAplicacao.Text = String.Format("{0}", listaVouchers[0].DataRetirada.ToString("dd/MM/yyyy HH:ss"));
            else
                LblDataAplicacao.Text = String.Format("NÃO APLICADOS");

            #endregion

            #region DADOS CLINICA

            dadosClinica.AppendFormat(clinicaSelecionada.NomeFantasia.ToUpper());
            if (!String.IsNullOrEmpty(clinicaSelecionada.Endereco))
            {
                dadosClinica.AppendFormat(" - {0}", clinicaSelecionada.Endereco.ToUpper());
                dadosClinica.AppendFormat(", {0}", clinicaSelecionada.Numero);
            }

            if (clinicaSelecionada.IdCidade.Codigo > 0)
            {
                dadosClinica.AppendFormat(" - {0}", clinicaSelecionada.IdCidade.Nome.ToUpper());
                dadosClinica.AppendFormat("/{0}", clinicaSelecionada.IdUf.Sigla);
            }

            if (!String.IsNullOrEmpty(clinicaSelecionada.Telefone))
                dadosClinica.AppendFormat(" - TEL.: {0}", clinicaSelecionada.Telefone);

            LblClinica.Text = dadosClinica.ToString();

            #endregion

            #region DADOS EMPRESA

            dadosEmpresa.AppendFormat(empresaSelecionada.NomeFantasia.ToUpper());
            if (!String.IsNullOrEmpty(empresaSelecionada.Endereco))
            {
                dadosEmpresa.AppendFormat(" - {0}", empresaSelecionada.Endereco.ToUpper());
                dadosEmpresa.AppendFormat(", {0}", empresaSelecionada.Numero);
            }

            if (empresaSelecionada.IdCidade.Codigo > 0)
            {
                dadosEmpresa.AppendFormat(" - {0}", empresaSelecionada.IdCidade.Nome.ToUpper());
                dadosEmpresa.AppendFormat("/{0}", empresaSelecionada.IdUf.Sigla);
            }

            if (!String.IsNullOrEmpty(empresaSelecionada.Telefone))
                dadosEmpresa.AppendFormat(" - TEL.: {0}", empresaSelecionada.Telefone);

            LblEmpresa.Text = dadosEmpresa.ToString();

            #endregion

            #region PESSOAS

            ImunneVacinas.PesquisaVoucher pesquisaVales = new ImunneVacinas.PesquisaVoucher()
            {
                Compra = compraSelecionada.Codigo,
                Clinica = compraSelecionada.IdClinica.Codigo,
                Ordenacao = ImunneVacinas.Utils.TipoOrdenacao.porNome
            };

            List<ImunneVacinas.Voucher> listaVales = ImunneVacinas.Voucher.ConsultarVouchers(pesquisaVales);
            foreach (ImunneVacinas.Voucher vale in listaVales)
            {
                string nascimento = String.Empty;
                ImunneVacinas.Participante participante = ImunneVacinas.Participante.ConsultarUnico(vale.IdParticipante.Codigo);
                if (participante != null)
                {
                    if (participante.DataNascimento != ImunneVacinas.Utils.dataPadrao)
                        nascimento = participante.DataNascimento.ToString("dd/MM/yyyy");
                }

                htmlConteudo.Append(Resources.Site.InternoDetalhesDependentes.Replace("#NASCIMENTO", nascimento).Replace("#NOME", String.Format("{0}<br/>{1}", vale.IdParticipante.Nome.ToUpper(), vale.Descricao.Replace("\r\n", "<br/>"))));
            }

            DvItens.InnerHtml = htmlConteudo.ToString();

            #endregion

            #region PRODUTOS

            ImunneVacinas.PesquisaItemCompra pesquisaItens = new ImunneVacinas.PesquisaItemCompra()
            {
                Compra = codigoRegistro,
                Ordenacao = ImunneVacinas.Utils.TipoOrdenacao.porNome
            };

            htmlConteudo = new StringBuilder();
            List<ImunneVacinas.ItemCompra> listaItens = ImunneVacinas.ItemCompra.ConsultarItens(pesquisaItens);
            foreach (ImunneVacinas.ItemCompra item in listaItens)
            {
                string valorProduto = String.Format("{0} ({1}x {2})", ImunneVacinas.Utils.ToMoeda(item.Valor), item.Quantidade, ImunneVacinas.Utils.ToMoeda(Convert.ToInt32(item.Valor / item.Quantidade)));
                htmlConteudo.Append(Resources.Site.InternoDetalhesProdutos.Replace("#NOME", item.IdVacina.Nome.ToUpper()).Replace("#VALOR", valorProduto));
            }

            DvProdutos.InnerHtml = htmlConteudo.ToString();

            #endregion
        }
        catch (Exception ex)
        {

        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void IniciarPagina2()
    {
        int codigoRegistro = Convert.ToInt32(Request.QueryString["compra"]);
        List<ItemPedido> pedidosTela = new List<ItemPedido>();
        ImunneVacinas.Compra compraSelecionada = ImunneVacinas.Compra.ConsultarUnico(codigoRegistro);
        ImunneVacinas.TransacaoRealizada transacaoSelecionada = ImunneVacinas.TransacaoRealizada.ConsultarUnicoCompra(compraSelecionada.Codigo);

        // Listando os itens de cada compra
        ImunneVacinas.PesquisaItemCompra pesquisaItens = new ImunneVacinas.PesquisaItemCompra()
        {
            Compra = codigoRegistro,
            Ordenacao = ImunneVacinas.Utils.TipoOrdenacao.porCodigo
        };

        List<ImunneVacinas.ItemCompra> listaItens = ImunneVacinas.ItemCompra.ConsultarItens(pesquisaItens);
        foreach (ImunneVacinas.ItemCompra item in listaItens)
        {
            ImunneVacinas.ProdutoCampanha produtoCampanha = ImunneVacinas.ProdutoCampanha.ConsultarUnico(compraSelecionada.IdCampanha.Codigo, item.IdVacina.Codigo);

            // Listando os beneficiários da compra para impressão, linha a linha, dos valores de cada um
            ImunneVacinas.PesquisaVoucher pesquisaVales = new ImunneVacinas.PesquisaVoucher()
            {
                Compra = compraSelecionada.Codigo,
                Clinica = compraSelecionada.IdClinica.Codigo,
                Ordenacao = ImunneVacinas.Utils.TipoOrdenacao.porNome
            };

            List<ImunneVacinas.Voucher> listaVales = ImunneVacinas.Voucher.ConsultarVouchers(pesquisaVales);
            foreach (ImunneVacinas.Voucher vale in listaVales)
            {
                ItemPedido pedido = new ItemPedido()
                {
                    Pedido = compraSelecionada.Codigo.ToString().PadLeft(5, '0'),
                    Data = compraSelecionada.DataCompra.ToString("dd/MM/yyyy HH:mm"),
                    Beneficiario = vale.IdParticipante.Nome.ToUpper(),
                    Vacina = produtoCampanha.IdVacina.Nome.ToUpper(),
                    Qtde = "1",
                    Valor = produtoCampanha.Preco.ToString("N2")
                };

                if (transacaoSelecionada != null)
                    pedido.Comprovante = transacaoSelecionada.numeroComprovanteVenda.PadLeft(18, '0');
                else pedido.Comprovante = "NÃO LOCALIZADO";

                // Aplicação
                pedido.Aplicacao = "NÃO APLICADA";
                if (vale.DataRetirada != ImunneVacinas.Utils.dataPadrao)
                    pedido.Aplicacao = vale.DataRetirada.ToString("dd/MM/yyyy HH:mm");

                pedidosTela.Add(pedido);
            }
        }


        RptPrincipal.DataSource = pedidosTela;
        RptPrincipal.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void RptPrincipal_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        ItemPedido item = (ItemPedido)e.Item.DataItem;

        Label LblPedido = (Label)e.Item.FindControl("LblPedido");
        Label LblComprovante = (Label)e.Item.FindControl("LblComprovante");
        Label LblData = (Label)e.Item.FindControl("LblData");
        Label LblBeneficiario = (Label)e.Item.FindControl("LblBeneficiario");
        Label LblVacina = (Label)e.Item.FindControl("LblVacina");
        Label LblQtde = (Label)e.Item.FindControl("LblQtde");
        Label LblValor = (Label)e.Item.FindControl("LblValor");
        Label LblAplicacao = (Label)e.Item.FindControl("LblAplicacao");

        LblPedido.Text = item.Pedido;
        LblComprovante.Text = item.Comprovante;
        LblData.Text = item.Data;
        LblBeneficiario.Text = item.Beneficiario;
        LblVacina.Text = item.Vacina;
        LblQtde.Text = item.Qtde;
        LblValor.Text = item.Valor;
        LblAplicacao.Text = item.Aplicacao;
    }
}