﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/popups.master" AutoEventWireup="true" CodeFile="detalhes-compras.aspx.cs" Inherits="popups_detalhes_cursos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <div class="row">
            <div class="box-grids">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding" style="float: left; width: 100%; border-bottom: solid 1px #6c6c6c; margin-bottom: 5px;">
                    <h3>Compra</h3>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding" style="padding: 5px 0px; font-size: 12px; line-height: 18px; margin-bottom: 5px;">
                    <!-- DADOS COMPRA -->
                    <div class="col-xs-12 col-sm-6 col-md-7 col-lg-7 no-padding" style="margin-bottom: 5px;">
                        <b>COMPRA</b><br />
                        <asp:Label ID="LblComprovante" runat="server"></asp:Label>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 no-padding" style="margin-bottom: 5px;">
                        <b>DATA</b><br />
                        <asp:Label ID="LblDataCompra" runat="server"></asp:Label>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-2 col-lg-2 no-padding" style="margin-bottom: 5px;">
                        <b>VALOR</b><br />
                        <asp:Label ID="LblValorCompra" runat="server"></asp:Label>
                    </div>
                    <div class="separator"></div>
                    <!-- DADOS TITULAR -->
                    <div class="col-xs-12 col-sm-6 col-md-7 col-lg-7 no-padding" style="margin-bottom: 5px;">
                        <b>TITULAR</b><br />
                        <asp:Label ID="LblNomeTitular" runat="server"></asp:Label>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 no-padding" style="margin-bottom: 5px;">
                        <b>CATEGORIA</b><br />
                        <asp:Label ID="LblCategoriaCompra" runat="server"></asp:Label>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-2 col-lg-2 no-padding" style="margin-bottom: 5px;">
                        <b>APLICAÇÃO</b><br />
                        <asp:Label ID="LblDataAplicacao" runat="server"></asp:Label>
                    </div>
                    <div class="separator"></div>
                    <!-- CAMPANHA -->
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding" style="margin-bottom: 5px;">
                        <b>CAMPANHA</b><br />
                        <asp:Label ID="LblCampanha" runat="server"></asp:Label>
                    </div>
                    <div class="separator"></div>
                    <!-- EMPRESA -->
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding" style="margin-bottom: 5px;">
                        <b>EMPRESA</b><br />
                        <asp:Label ID="LblEmpresa" runat="server"></asp:Label>
                    </div>
                    <div class="separator"></div>
                    <!-- CLÍNICA -->
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding" style="margin-bottom: 5px;">
                        <b>CLÍNICA</b><br />
                        <asp:Label ID="LblClinica" runat="server"></asp:Label>
                    </div>
                </div>
                <!-- PESSOAS -->
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding" style="border-bottom: solid 1px #6c6c6c; margin-bottom: 5px;">
                    <h3>Atendidos</h3>
                </div>
                <div id="DvItens" runat="server"></div>
                <!-- PRODUTOS -->
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding" style="border-bottom: solid 1px #6c6c6c; margin-bottom: 5px;">
                    <h3>Produtos</h3>
                </div>
                <div id="DvProdutos" runat="server"></div>
                <div class="dv-grid-f-header hidden">
                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="display: inline-block; float: none;">
                        pedido
                    </div>
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" style="display: inline-block; float: none;">
                        comprovante
                    </div>
                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="display: inline-block; float: none;">
                        data
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="display: inline-block; float: none;">
                        beneficiário
                    </div>
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" style="display: inline-block; float: none;">
                        vacina
                    </div>
                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="display: inline-block; float: none;">
                        qtde
                    </div>
                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="display: inline-block; float: none;">
                        valor
                    </div>
                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="display: inline-block; float: none;">
                        aplicação
                    </div>
                </div>
                <div class="dv-grid-f hidden" style="height: 200px !important;">
                    <asp:Repeater ID="RptPrincipal" runat="server" OnItemDataBound="RptPrincipal_ItemDataBound">
                        <ItemTemplate>
                            <div id="dvItem" runat="server">
                                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="display: inline-block; float: none;">
                                    <div class="truncate">
                                        <asp:Label ID="LblPedido" runat="server" CssClass="text-uppercase"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" style="display: inline-block; float: none;">
                                    <div class="truncate">
                                        <asp:Label ID="LblComprovante" runat="server" CssClass="text-uppercase"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="display: inline-block; float: none;">
                                    <div class="truncate">
                                        <asp:Label ID="LblData" runat="server" CssClass="text-uppercase"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="display: inline-block; float: none;">
                                    <div class="truncate">
                                        <asp:Label ID="LblBeneficiario" runat="server" CssClass="text-uppercase"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" style="display: inline-block; float: none;">
                                    <div class="truncate">
                                        <asp:Label ID="LblVacina" runat="server" CssClass="text-uppercase"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="display: inline-block; float: none;">
                                    <div class="truncate">
                                        <asp:Label ID="LblQtde" runat="server" CssClass="text-uppercase"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="display: inline-block; float: none;">
                                    <div class="truncate">
                                        <asp:Label ID="LblValor" runat="server" CssClass="text-uppercase"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center" style="display: inline-block; float: none;">
                                    <div class="truncate">
                                        <asp:Label ID="LblAplicacao" runat="server" CssClass="text-uppercase"></asp:Label>
                                    </div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
