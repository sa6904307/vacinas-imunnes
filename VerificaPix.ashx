<%@ WebHandler Language="C#" Class="VerificaPix" %>

using System;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using ImunneVacinas;
using Newtonsoft.Json;

public class VerificaPix : IHttpHandler
{
    public void ProcessRequest(HttpContext context)
    {
        try
        {

            using (StreamReader reader = new StreamReader(HttpContext.Current.Request.InputStream))
            {
                // Token da loja
                string storeCode = System.Configuration.ConfigurationManager.AppSettings.Get("STORECODE");

                // Converte o JSON do corpo da solicitação para o objeto PaymentRequest
                PaymentRequestPix paymentRequestPix = JsonConvert.DeserializeObject<PaymentRequestPix>(reader.ReadToEnd());

                // Obtém o token do pagamento do objeto PaymentRequest
                string token = paymentRequestPix.data.token;
                string campanha = paymentRequestPix.data.campanha;
                int codigoCampanha = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(paymentRequestPix.data.campanha));
                string clinica = paymentRequestPix.data.clinica;
                string participante = paymentRequestPix.data.participante;
                string itens = paymentRequestPix.data.itens;
                string qtde = paymentRequestPix.data.qtde;
                int codigoCompra = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(paymentRequestPix.data.compra));
                string compra = paymentRequestPix.data.compra;
                string membros = paymentRequestPix.data.membros;

                if (string.IsNullOrEmpty(token))
                {
                    // Se o token não foi fornecido, retorna uma mensagem de erro
                    context.Response.ContentType = "application/json";
                    context.Response.Write("{\"status\": \"error\", \"mensagem\": \"Token não fornecido.\"}");
                    return;
                }

                // Constrói a URL da API com base no token recebido
                string apiUrl = $"https://api.intermediador.yapay.com.br/api/v3/transactions/get_by_token_brief?token_account={storeCode}&token_transaction={token}";

                try
                {
                    ServicePointManager.ServerCertificateValidationCallback = (sender, cert, chain, sslPolicyErrors) => true;
                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    // Faz a requisição GET para a API
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(apiUrl);
                    request.Method = "GET";

                    using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                    {
                        // Lê a resposta da API
                        using (Stream stream = response.GetResponseStream())
                        using (StreamReader apiReader = new StreamReader(stream))
                        {
                            PaymentReturn paymentReturn = JsonConvert.DeserializeObject <PaymentReturn> (apiReader.ReadToEnd());
                            StringBuilder urlPagina= new StringBuilder();
                            object dado;
                            // Caso status_id 6, pagamento "Aprovado"
                            if (paymentReturn.data_response.transaction.status_id == 6) {
                                // Verifica e ajusta o preço do pagamento
                                if (paymentReturn.data_response.transaction.payment.price_payment.Split('.')[1].Length == 1) {
                                    paymentReturn.data_response.transaction.payment.price_payment += '0';
                                }

                                // Preenche os dados na instância de TransacaoRealizada
                                TransacaoRealizada transacaoEfetivada = new TransacaoRealizada() {
                                    compraTransacao = new Compra() {
                                        Codigo = codigoCompra
                                    },
                                    numeroTransacao = int.Parse(paymentReturn.data_response.transaction.order_number),
                                    codigoEstabelecimento = storeCode,
                                    codigoFormaPagamento = paymentReturn.data_response.transaction.payment.payment_method_id,
                                    valor = Decimal.Parse(paymentReturn.data_response.transaction.payment.price_payment, System.Globalization.CultureInfo.InvariantCulture),
                                    valorDesconto = 0,
                                    parcelas = paymentReturn.data_response.transaction.payment.split,
                                    statusTransacao = paymentReturn.data_response.transaction.status_id,
                                    autorizacao = "IMUNNEVACINAS",
                                    codigoTransacaoOperadora = paymentReturn.data_response.transaction.payment.payment_response_code,
                                    dataAprovacaoOperadora = DateTime.Now.ToString(),
                                    numeroComprovanteVenda = paymentReturn.data_response.transaction.payment.tid,
                                    nsu = paymentReturn.data_response.transaction.token_transaction,
                                    mensagemVenda = paymentReturn.data_response.transaction.payment.payment_response,
                                    urlPagamento = paymentReturn.data_response.transaction.payment.url_payment,
                            };
                            ImunneVacinas.Compra compraSelecionada = ImunneVacinas.Compra.ConsultarUnico(codigoCompra);
                            ImunneVacinas.Campanha campanhaSelecionada = ImunneVacinas.Campanha.ConsultarUnico(codigoCampanha);
                            if (transacaoEfetivada != null) {
                                transacaoEfetivada.compraTransacao.Codigo = codigoCompra;

                                bool resultado = false;

                                try {
                                    resultado = transacaoEfetivada.ExisteBD();
                                } catch (Exception ex) {
                                    resultado = false;
                                }

                                if (resultado == true)
                                    transacaoEfetivada.Alterar();
                                else transacaoEfetivada.Incluir();

                                // TRANSAÇÕES JÁ APROVADAS/PAGAS
                                // SE SEGUE O FLUXO DE DIRECIONAMENTO DAS TELAS DE AGENDAMENTO OU FINALIZAÇÃO,
                                // DE ACORDO COM TIPO DA CAMPANHA (SURTO OU REGULAR)
                                compraSelecionada.Situacao = ImunneVacinas.Compra.SituacaoCompra.Concluida;
                                compraSelecionada.Alterar();
                                if (campanhaSelecionada.Surto == 0) {
                                    // CAMPANHAS DO TIPO 'REGULAR' SÃO DIRECIONADAS PARA A FINALIZAÇÃO,
                                    // ONDE SÃO VISUALIZADOS OS BENEFICIÁRIOS. AS APLICAÇÕES DESTAS COMPRAS
                                    // SÃO INFORMADAS, POSTERIORMENTE, PELA CLÍNICA, PODEMOS O USUÁRIO IMPRIMIR
                                    // UM COMPROVANTE DAS AQUISIÇÕES.

                                    urlPagina.AppendFormat("~/vouchers.aspx?campanha={0}", paymentRequestPix.data.campanha);
                                    urlPagina.AppendFormat("&clinica={0}", clinica);
                                    urlPagina.AppendFormat("&participante={0}", participante);
                                    urlPagina.AppendFormat("&itens={0}", itens);
                                    urlPagina.AppendFormat("&qtde={0}", qtde);
                                    urlPagina.AppendFormat("&compra={0}", compra);
                                    urlPagina.AppendFormat("&membros={0}", membros);
                                } else {
                                    // CAMPANHAS DO TIPO 'SURTO' SÃO DIRECIONADAS PARA A TELA DE AGENDAMENTO,
                                    // ONDE O USUÁRIO PODE ESCOLHER OS HORÁRIOS PARA APLICAÇÃO DOS ITENS ADQUIRIDOS.
                                    // ELE TAMBÉM PODE SAIR SEM AGENDAMENTO, FICANDO A CRITÉRIO DA CLÍNICA TAL ESCOLHA.

                                    urlPagina.AppendFormat("~/horarios.aspx?campanha={0}", paymentRequestPix.data.campanha);
                                    urlPagina.AppendFormat("&clinica={0}", clinica);
                                    urlPagina.AppendFormat("&participante={0}", participante);
                                    urlPagina.AppendFormat("&itens={0}", itens);
                                    urlPagina.AppendFormat("&qtde={0}", qtde);
                                    urlPagina.AppendFormat("&compra={0}", compra);
                                    urlPagina.AppendFormat("&membros={0}", membros);
                                }
                                dado = new {
                                    status = "Aprovado",
                                    mensagem = "Pagamento feito com sucesso",
                                    data = new {
                                        urlRedirect = urlPagina.ToString()
                                    }
                                };
                            } else {
                                dado = new {
                                    status = "error",
                                    mensagem = "Algum erro relacionado a transação"
                                };
                            }
                        } else {
                            // AVISO E ATUALIZAÇÃO DE DADOS DE COMPRAS NÃO AUTORIZADAS PELA PLATAFORMA DE PGTOS
                            dado = new {
                                status = $"{paymentReturn.data_response.transaction.status_name}",
                                mensagem = "Aguardando o pagamento ser aprovado"
                            };
                        }
                        // Responde com os dados processados
                        context.Response.ContentType = "application/json";
                        context.Response.Write(JsonConvert.SerializeObject(dado));
                    }
                }
                }
                catch (WebException ex)
        {
            // Manipule erros de requisição aqui, se necessário
            ImunneVacinas.Erro erro = new ImunneVacinas.Erro();
            erro.addArquivo(ex);
            context.Response.ContentType = "application/json";
            context.Response.Write($"{{\"status\": \"error\", \"mensagem web\": \"{ex.Message}\"}}");
        }
    }
}
        catch (Exception ex)
        {
    ImunneVacinas.Erro erro = new ImunneVacinas.Erro();
    erro.addArquivo(ex);
    context.Response.ContentType = "application/json";
    context.Response.Write($"{{\"status\": \"error\", \"trace\": {ex.StackTrace}, \"mensagem server\": \"{ex.Message}\"}}");
}
    }

    // Defina a classe PaymentRequest aqui, se ainda não estiver definida
    public class PaymentRequestPix
{
    public Data data { get; set; }
}

public class Data
{
    public string token { get; set; }
    public string campanha { get; set; }
    public string clinica { get; set; }
    public string participante { get; set; }
    public string itens { get; set; }
    public string qtde { get; set; }
    public string compra { get; set; }
    public string membros { get; set; }
}
public bool IsReusable
{
    get { return false; }
}
}
