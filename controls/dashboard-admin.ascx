﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="dashboard-admin.ascx.cs" Inherits="controls_dashboard_admin" %>

<asp:Panel ID="PnlCampanhas" runat="server">
    <section>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 animated fadeRightIn">
                <h3>Resumo de campanhas
                </h3>
            </div>
            <div class="dv-spc-10"></div>
            <!-- A REALIZAR -->
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 animated fadeRightIn">
                <div class="info-box bg-yellow">
                    <span class="info-box-icon"><i class="ionicons ion-ios-medkit-outline"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">A REALIZAR</span>
                        <span class="info-box-number">
                            <asp:Label ID="LblTotalCampanhasRealizar" runat="server" Text="0"></asp:Label>
                        </span>
                        <div class="progress">
                            <div class="progress-bar"></div>
                            <div id="PgrTotalCampanhasRealizar" runat="server" class="progress-bar"></div>
                        </div>
                        <span class="progress-description">
                            <asp:Label ID="LblPgrTotalCampanhasRealizar" runat="server"></asp:Label>
                        </span>
                    </div>
                </div>
            </div>
            <!-- ATIVAS -->
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 animated fadeRightIn">
                <div class="info-box bg-aqua">
                    <span class="info-box-icon"><i class="ionicons ion-ios-medkit-outline"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">ATIVAS</span>
                        <span class="info-box-number">
                            <asp:Label ID="LblTotalCampanhasAtivas" runat="server" Text="0"></asp:Label>
                        </span>
                        <div class="progress">
                            <div class="progress-bar"></div>
                            <div id="PgrTotalCampanhasAtivas" runat="server" class="progress-bar"></div>
                        </div>
                        <span class="progress-description">
                            <asp:Label ID="LblPgrTotalCampanhasAtivas" runat="server"></asp:Label>
                        </span>
                    </div>
                </div>
            </div>
            <!-- CONCLUIDAS -->
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 animated fadeRightIn">
                <div class="info-box bg-green">
                    <span class="info-box-icon"><i class="ionicons ion-ios-medkit-outline"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">CONCLUÍDAS</span>
                        <span class="info-box-number">
                            <asp:Label ID="LblTotalCampanhasConcluidas" runat="server" Text="0"></asp:Label>
                        </span>
                        <div class="progress">
                            <div class="progress-bar"></div>
                            <div id="PgrTotalCampanhasConcluidas" runat="server" class="progress-bar"></div>
                        </div>
                        <span class="progress-description">
                            <asp:Label ID="LblPgrTotalCampanhasConcluidas" runat="server"></asp:Label>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3 col-xs-6 animated fadeRightIn">
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3><asp:Label ID="LblTotalClinicas" runat="server" Text="0"></asp:Label></h3>
                        <p>Clínicas</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-hospital-o"></i>
                    </div>
                    <a href="../admin/clinicas.aspx" class="small-box-footer">Visualizar <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-lg-3 col-xs-6 animated fadeRightIn">
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3><asp:Label ID="LblTotalVacinas" runat="server" Text="0"></asp:Label></h3>
                        <p>Vacinas</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-medkit"></i>
                    </div>
                    <a href="../admin/vacinas.aspx" class="small-box-footer">Visualizar <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-lg-3 col-xs-6 animated fadeRightIn">
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3><asp:Label ID="LblTotalEmpresas" runat="server" Text="0"></asp:Label></h3>
                        <p>Empresas</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-briefcase"></i>
                    </div>
                    <a href="../admin/empresas.aspx" class="small-box-footer">Visualizar <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-lg-3 col-xs-6 animated fadeRightIn">
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3><asp:Label ID="LblTotalCampanhas" runat="server" Text="0"></asp:Label></h3>
                        <p>Campanhas</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-ambulance"></i>
                    </div>
                    <a href="../admin/campanhas.aspx" class="small-box-footer">Visualizar <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
        </div>
    </section>
</asp:Panel>
