﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="whatsapp.ascx.cs" Inherits="controls_whatsapp" %>

<div class="bloco-whats">
    <asp:Panel ID="pnlWhatsApp" runat="server">
        <asp:HyperLink ID="lnkWhatsApp" runat="server" CssClass="click-whats" Target="_blank">
        <i class="fa fa-whatsapp"></i><span>Fale pelo WhatsApp</span>
        </asp:HyperLink>
    </asp:Panel>
</div>