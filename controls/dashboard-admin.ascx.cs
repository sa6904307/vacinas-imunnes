﻿using System;
using System.Collections.Generic;

public partial class controls_dashboard_admin : System.Web.UI.UserControl
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            IniciarPagina();
    }

    /// <summary>
    /// 
    /// </summary>
    private void IniciarPagina()
    {
        try
        {
            #region Totalização Campanhas

            int totalCampanhas = 0;
            int totalCampanhasAtivas = 0;
            int totalCampanhasConcluidas = 0;
            int totalCampanhasRealizar = 0;

            ImunneVacinas.PesquisaCampanha pesquisaCampanhas = new ImunneVacinas.PesquisaCampanha()
            {
                Ordenacao = ImunneVacinas.Utils.TipoOrdenacao.porData
            };

            List<ImunneVacinas.Campanha> listaCampanhas = ImunneVacinas.Campanha.ConsultarCampanhas(pesquisaCampanhas);

            // Totalizando Campanhas
            foreach (ImunneVacinas.Campanha item in listaCampanhas)
            {
                if (item.DataTermino < DateTime.Now.Date)
                    totalCampanhasConcluidas += 1;
                else if (item.DataTermino > DateTime.Now.Date)
                {
                    if (item.DataInicio <= DateTime.Now.Date)
                        totalCampanhasAtivas += 1;
                    else
                        totalCampanhasRealizar += 1;
                }
            }

            totalCampanhas = totalCampanhasAtivas + totalCampanhasConcluidas + totalCampanhasRealizar;

            // Cálculos
            double percentualTotalCampanhasConcluidas;
            double percentualTotalCampanhasRealizar;
            double percentualTotalCampanhasAtivas;

            if (totalCampanhas > 0)
            {
                if (totalCampanhasAtivas > 0)
                {
                    percentualTotalCampanhasAtivas = Convert.ToDouble((Convert.ToDecimal(totalCampanhasAtivas) / Convert.ToDecimal(totalCampanhas) * 100));
                    LblTotalCampanhasAtivas.Text = (totalCampanhasAtivas).ToString() + " campanhas";
                    PgrTotalCampanhasAtivas.Style.Add("width", percentualTotalCampanhasAtivas.ToString().Replace(",", ".") + "%");
                    LblPgrTotalCampanhasAtivas.Text = String.Format("({0} % de {1}).", percentualTotalCampanhasAtivas.ToString("N2"), totalCampanhas);
                }

                if (totalCampanhasRealizar > 0)
                {
                    percentualTotalCampanhasRealizar = Convert.ToDouble((Convert.ToDecimal(totalCampanhasRealizar) / Convert.ToDecimal(totalCampanhas) * 100));
                    LblTotalCampanhasRealizar.Text = (totalCampanhasRealizar).ToString() + " campanhas";
                    PgrTotalCampanhasRealizar.Style.Add("width", percentualTotalCampanhasRealizar.ToString().Replace(",", ".") + "%");
                    LblPgrTotalCampanhasRealizar.Text = String.Format("({0} % de {1}).", percentualTotalCampanhasRealizar.ToString("N2"), totalCampanhas);
                }

                if (totalCampanhasConcluidas > 0)
                {
                    percentualTotalCampanhasConcluidas = Convert.ToDouble((Convert.ToDecimal(totalCampanhasConcluidas) / Convert.ToDecimal(totalCampanhas) * 100));
                    LblTotalCampanhasConcluidas.Text = (totalCampanhasConcluidas).ToString() + " campanhas";
                    PgrTotalCampanhasConcluidas.Style.Add("width", percentualTotalCampanhasConcluidas.ToString().Replace(",", ".") + "%");
                    LblPgrTotalCampanhasConcluidas.Text = String.Format("({0} % de {1}).", percentualTotalCampanhasConcluidas.ToString("N2"), totalCampanhas);
                }
            }

            #endregion

            LblTotalClinicas.Text = ImunneVacinas.Clinica.ConsultarClinicas(new ImunneVacinas.PesquisaClinica()).Count.ToString().PadLeft(2, '0');
            LblTotalEmpresas.Text = ImunneVacinas.Empresa.ConsultarEmpresas(new ImunneVacinas.PesquisaEmpresa()).Count.ToString().PadLeft(2, '0');
            LblTotalVacinas.Text = ImunneVacinas.Vacina.ConsultarVacinas(new ImunneVacinas.PesquisaVacina()).Count.ToString().PadLeft(2, '0');
            LblTotalCampanhas.Text = ImunneVacinas.Campanha.ConsultarCampanhas(new ImunneVacinas.PesquisaCampanha()).Count.ToString().PadLeft(2, '0');
        }
        catch (Exception ex)
        {

        }
    }
}