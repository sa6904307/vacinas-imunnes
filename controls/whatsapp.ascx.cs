﻿using System;

public partial class controls_whatsapp : System.Web.UI.UserControl
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            IniciarPagina();
    }

    /// <summary>
    /// 
    /// </summary>
    private void IniciarPagina()
    {
        if (!String.IsNullOrEmpty(Request.QueryString["campanha"]))
        {
            int codigoCampanha = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["campanha"]));

            ImunneVacinas.Campanha campanhaSelecionada = ImunneVacinas.Campanha.ConsultarUnico(codigoCampanha);

            if (!String.IsNullOrEmpty(campanhaSelecionada.ContatoWhatsApp))
            {
                string telefone = campanhaSelecionada.ContatoWhatsApp;
                string url = ImunneVacinas.Utils.PrepararLinkWhatsApp(telefone);
                lnkWhatsApp.NavigateUrl = url;
                pnlWhatsApp.Visible = true;
            }
            else pnlWhatsApp.Visible = false;
        }
        else pnlWhatsApp.Visible = false;
    }
}