﻿<%@ Page Title="" Language="C#" MasterPageFile="~/imunne.master" AutoEventWireup="true" CodeFile="horarios.aspx.cs" Inherits="horarios" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        // Chamada de alerta para confirmação de exclusão
        function ConfirmacaoExclucao(ctl, event, id) {
            var defaultAction = $(ctl).prop("href");
            event.preventDefault();
            swal({
                title: "Confirmar o horário?",
                text: 'Verifique se o horário selecionado é o realmente desejado antes de continuar a operação. Uma vez escolhido, o mesmo não poderá ser trocado.',
                type: 'question',
                showCancelButton: true,
                confirmButtonText: "<i class='fa fa-check'></i>&nbsp;Sim!",
                cancelButtonText: "<i class='fa fa-times'></i>&nbsp;Não!",
                confirmButtonColor: '#00a65a',
                cancelButtonColor: '#dd4b39',
                showLoaderOnConfirm: true,
                animation: 'slide-from-top',
                allowEscapeKey: false,
                allowOutsideClick: false
            }).then(result => {
                if (result.value) {
                    document.getElementById('<%=HdfCodigo.ClientID%>').value = id;
                    var btn = document.getElementById('<%=BtnConfirmar.ClientID%>');
                    btn.click();
                    return true;
                }
            })
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section class="subheader">
        <div class="container">
            <div class="row">
                <div class="col-xs-10 col-sm-10 col-md-12 col-lg-12">
                    <i class="fa fa-calendar"></i>&nbsp;Escolher horário
                </div>
            </div>
        </div>
    </section>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
                <div class="caixa-branca">
                    <div class="step-header">
                        DADOS DO ADQUIRENTE
                    </div>
                    <div class="box-inside">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                            <label>Nome</label>
                            <asp:Label ID="LblNome" runat="server" CssClass="label-identificacao"></asp:Label>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 form-group">
                            <label>CPF</label>
                            <asp:Label ID="LblCPF" runat="server" CssClass="label-identificacao"></asp:Label>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 form-group">
                            <label>Nascimento</label>
                            <asp:Label ID="LblNascimento" runat="server" CssClass="label-identificacao"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="caixa-branca">
                    <div class="step-header">
                        INFORMAÇÕES DA CAMPANHA
                    </div>
                    <div class="box-inside">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                            <label>CAMPANHA</label>
                            <asp:Label ID="LblCampanha" runat="server" CssClass="label-identificacao"></asp:Label>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                            <label>CLÍNICA</label>
                            <asp:Label ID="LblClinica" runat="server" CssClass="label-identificacao"></asp:Label>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                            <label>EMPRESA</label>
                            <asp:Label ID="LblEmpresa" runat="server" CssClass="label-identificacao"></asp:Label>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                            <label>VALIDADE</label>
                            <asp:Label ID="LblValidade" runat="server" CssClass="label-identificacao"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="caixa-branca">
                    <div class="step-header">
                        SEGUIR SEM AGENDAMENTO ATENDIMENTO
                    </div>
                    <div class="box-inside">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 pull-right form-group">
                            <p>Caso queira seguir sem uma agendamento prévio, <b>clique</b> no botão logo abaixo.</p>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 pull-right form-group">
                            <asp:LinkButton ID="LnkSemAgenda" runat="server" CssClass="btn btn-block btn-primary" OnClick="LnkSemAgenda_Click">
                                    <i class="fa fa-envelope"></i>&nbsp;Seguir sem agendamento
                            </asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                <div class="caixa-branca">
                    <div class="step-header">
                        DADOS DA COMPRA
                    </div>
                    <div class="box-inside">
                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 form-group">
                            <label>Situação</label>
                            <asp:Label ID="LblSituacao" runat="server" CssClass="label-identificacao"></asp:Label>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 form-group">
                            <label>Data</label>
                            <asp:Label ID="LblDataCompra" runat="server" CssClass="label-identificacao"></asp:Label>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 form-group">
                            <label>Valor</label>
                            <asp:Label ID="LblValor" runat="server" CssClass="label-identificacao"></asp:Label>
                        </div>
                    </div>
                </div>
                <asp:UpdatePanel ID="UpdHorarios" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:HiddenField ID="HdfOpcoesHorarios" runat="server" />
                        <!-- OPÇÕES DE AGENDAMENTOS -->
                        <asp:Panel ID="PnlOpcoes" runat="server">
                            <div class="caixa-branca">
                                <div class="step-header">
                                    SELECIONE A OPÇÃO DE AGENDAMENTO
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                    <asp:DropDownList ID="DdlOpcoesHorarios" runat="server" CssClass="form-control" DataValueField="ValueMember" DataTextField="DisplayMember" AutoPostBack="True" OnSelectedIndexChanged="DdlOpcoesHorarios_SelectedIndexChanged"></asp:DropDownList>
                                </div>
                            </div>
                        </asp:Panel>
                        <!-- CAIXA DE BENEFICIÁRIOS -->
                        <asp:Panel ID="PnlBeneficiarios" runat="server" Visible="false">
                            <div class="caixa-branca">
                                <div class="step-header">
                                    SELECIONE O BENEFICIÁRIO
                                </div>
                                <div class="box-inside">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                        <label>Nome</label>
                                        <asp:DropDownList ID="DdlBeneficiarios" runat="server" CssClass="form-control" DataValueField="ValueMember" DataTextField="DisplayMember"></asp:DropDownList>
                                    </div>
                                    <div class="box-inside">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 text-center">
                                            <label>Escolhidos</label><br />
                                            <asp:Label ID="LblTotalEscolhidos" runat="server" Text="00" Font-Size="26" CssClass="label-identificacao"></asp:Label>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 text-center">
                                            <label>Total</label><br />
                                            <asp:Label ID="LblTotalBeneficiarios" runat="server" Text="00" Font-Size="26" CssClass="label-identificacao"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                        <!-- FILTRO DE DATAS -->
                        <asp:Panel ID="PnlDatasPesquisa" runat="server" Visible="false">
                            <div class="caixa-branca">
                                <div class="step-header">
                                    VISUALIZAR AGENDA DE HORÁRIOS
                                </div>
                                <div class="box-inside">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                        <p>
                                            Selecione o dia na caixa de opções e escolha o horário na listagem para visualizar a 
                                            disponibilidade de horários.
                                            Os <strong style="color: #197425;">VERDES</strong> são os disponíveis.
                                        </p>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                        <label>Dia</label>
                                        <asp:DropDownList ID="DdlDias" runat="server" CssClass="form-control text-center" DataValueField="ValueMember" DataTextField="DisplayMember" OnSelectedIndexChanged="DdlDias_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                        <!-- QUADRO DE DATAS & HORÁRIOS -->
                        <asp:Panel ID="PnlHorarios" runat="server" Visible="false">
                            <div class="caixa-branca">
                                <asp:DataList ID="DtlHorarios" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal" OnItemCommand="DtlHorarios_ItemCommand" OnItemDataBound="DtlHorarios_ItemDataBound">
                                    <ItemTemplate>
                                        <div id="DvItemHorario" runat="server" class="item-horario">
                                            <asp:LinkButton ID="LnkHorario" runat="server">
                                                <asp:Label ID="LblHorario" runat="server"></asp:Label><br />
                                                <small>
                                                    <asp:Label ID="LblData" runat="server"></asp:Label></small><br />
                                                <asp:HiddenField ID="HdfCodigo" runat="server" />
                                                <small>
                                                    <asp:Label ID="LblSituacao" runat="server"></asp:Label></small>
                                            </asp:LinkButton>
                                        </div>
                                    </ItemTemplate>
                                </asp:DataList>
                            </div>
                        </asp:Panel>
                        <asp:LinkButton ID="BtnConfirmar" runat="server" CssClass="hidden" OnClick="BtnConfirmar_Click">
                        </asp:LinkButton>
                        <asp:HiddenField ID="HdfCodigo" runat="server" />
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="DdlOpcoesHorarios" />
                        <asp:AsyncPostBackTrigger ControlID="DtlHorarios" />
                        <asp:AsyncPostBackTrigger ControlID="BtnConfirmar" />
                        <asp:AsyncPostBackTrigger ControlID="DdlDias" />
                        <%--<asp:AsyncPostBackTrigger ControlID="LnkPesquisar" />--%>
                        <asp:AsyncPostBackTrigger ControlID="DtlHorarios" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
</asp:Content>
