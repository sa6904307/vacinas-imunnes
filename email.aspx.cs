﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class email : System.Web.UI.Page
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            IniciarPagina();
    }

    /// <summary>
    /// 
    /// </summary>
    private void IniciarPagina()
    {
        string hostMail = System.Configuration.ConfigurationManager.AppSettings.Get("HOSTEMAIL");
        string portaEmail = System.Configuration.ConfigurationManager.AppSettings.Get("PORTEMAIL");
        string userMail = System.Configuration.ConfigurationManager.AppSettings.Get("USEREMAIL");
        string passEmail = System.Configuration.ConfigurationManager.AppSettings.Get("PASSEMAIL");
        string replyEmail = System.Configuration.ConfigurationManager.AppSettings.Get("REPLYEMAIL");

        TxtHost.Text = hostMail;
        TxtPass.Text = passEmail;
        TxtPorta.Text = portaEmail;
        TxtUser.Text = userMail;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BtnEnviar_Click(object sender, EventArgs e)
    {
        if (String.IsNullOrEmpty(TxtDestino.Text))
        {
            TxtResultado.Text = "Informe o destinatário";
            return;
        }

        ImunneVacinas.Mail emailEnvio = new ImunneVacinas.Mail();

        emailEnvio.oEmail.To.Add(TxtDestino.Text);
        emailEnvio.oEmail.From = new System.Net.Mail.MailAddress(TxtUser.Text, TxtAssunto.Text);
        emailEnvio.oEmail.ReplyToList.Add(new System.Net.Mail.MailAddress("gpetraca@gmail.com"));
        emailEnvio.oEmail.Subject = TxtAssunto.Text;
        emailEnvio.oEmail.Body = TxtConteudo.Text;

        emailEnvio.oEmail.IsBodyHtml = true;
        emailEnvio.oEnviar.Host = TxtHost.Text;
        emailEnvio.oEnviar.Credentials = new System.Net.NetworkCredential(TxtUser.Text, TxtPass.Text);
        if (String.IsNullOrEmpty(TxtPorta.Text))
            emailEnvio.oEnviar.Port = 587;
        else
            emailEnvio.oEnviar.Port = Convert.ToInt32(TxtPorta.Text);

        emailEnvio.SendMail();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BtnEnviar2_Click(object sender, EventArgs e)
    {
        //if (String.IsNullOrEmpty(TxtDestino.Text))
        //{
        //    TxtResultado.Text = "Informe o destinatário";
        //    return;
        //}

        //JactoTreinamentos.Mail emailEnvio = new JactoTreinamentos.Mail();

        //emailEnvio.oEmail.To.Add(TxtDestino.Text);
        //emailEnvio.oEmail.From = new System.Net.Mail.MailAddress(TxtUser.Text, TxtAssunto.Text);
        //emailEnvio.oEmail.ReplyToList.Add(new System.Net.Mail.MailAddress("gpetraca@gmail.com"));
        //emailEnvio.oEmail.Subject = TxtAssunto.Text;
        //emailEnvio.oEmail.Body = TxtConteudo.Text;

        //emailEnvio.oEmail.IsBodyHtml = true;
        //emailEnvio.oEnviar.Host = TxtHost.Text;
        //emailEnvio.oEnviar.Credentials = new System.Net.NetworkCredential(TxtUser.Text, TxtPass.Text);
        //if (String.IsNullOrEmpty(TxtPorta.Text))
        //    emailEnvio.oEnviar.Port = 587;
        //else
        //    emailEnvio.oEnviar.Port = Convert.ToInt32(TxtPorta.Text);

        //TxtResultado.Text = emailEnvio.SendMailNovo();
    }
}