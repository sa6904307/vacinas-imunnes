﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI;
using ImunneVacinas;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Math;

public partial class imunne_identificacao : System.Web.UI.Page
{
    /// <summary>
    ///  Tipo usado para retorno de validação de compras
    /// </summary>
    public class ValidacaoCompra
    {
        public bool Validacao { get; set; }
        public string DataCompra { get; set; }
        public string CpfCnpj { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            IniciarPagina();
    }

    /// <summary>
    /// 
    /// </summary>
    private void IniciarPagina()
    {
        StringBuilder textoMensagem = new StringBuilder();
        NameValueCollection campanha = new NameValueCollection();
        int codigoCampanha = -1;
        int codigoClinica = -1;

        try
        {
            if (!String.IsNullOrEmpty(Request.QueryString["campanha"]))
            {
                codigoCampanha = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["campanha"]));
                codigoClinica = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["clinica"]));

                ImunneVacinas.Campanha campanhaSelecionada = ImunneVacinas.Campanha.ConsultarUnico(codigoCampanha);
                ImunneVacinas.Clinica clinicaSelecionada = ImunneVacinas.Clinica.ConsultarUnico(codigoClinica);

                LblCampanha.Text = campanhaSelecionada.Identificacao.ToUpper() + " | " + campanhaSelecionada.Nome.ToUpper();
                LblValidade.Text = campanhaSelecionada.DataLimite.ToString("dd/MM/yyyy");
                LblClinica.Text = clinicaSelecionada.NomeFantasia.ToUpper() + "<br />"
                                + clinicaSelecionada.Endereco.ToUpper() + ", "
                                + clinicaSelecionada.Numero + " - "
                                + clinicaSelecionada.IdCidade.Nome.ToUpper() + "/"
                                + clinicaSelecionada.IdUf.Sigla;
                LblEmpresa.Text = campanhaSelecionada.IdEmpresa.NomeFantasia.ToUpper();
                Page.Title = "S&A Imunizações | " + campanhaSelecionada.Nome.ToUpper();
            }
            else
            {
                Response.Redirect("~/", false);
                Context.ApplicationInstance.CompleteRequest();
            }
        }
        catch (Exception ex)
        {
            Response.Redirect("~/", false);
            Context.ApplicationInstance.CompleteRequest();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void LnkContinuar_Click(object sender, EventArgs e)
    {
        StringBuilder urlPagina = new StringBuilder();

        int codigoCampanha = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["campanha"]));
        Participante participante = null;

        if (PnlIdentificacao.Visible == true)
        {
            // Validação do CPF/CNPJ
            if (!ImunneVacinas.Utils.ValidarCpfCnpj(ImunneVacinas.Utils.LimparCpfCnpj(TxtCpfCnpjIdentificacao.Text)))
            {
                ImunneVacinas.Alerta.showMensagemErro(this.Page, "Aviso", "CPF informado é inválido");
                return;
            }

            // 17/12/2019 - VALIDAÇÃO DE ÚNICA COMPRA POR CPF/CNPJ
            ValidacaoCompra retornoValidacao = ValidarCompraUnica(codigoCampanha, ImunneVacinas.Utils.LimparCpfCnpj(TxtCpfCnpjIdentificacao.Text));
            if (retornoValidacao.Validacao)
            {
                ImunneVacinas.Alerta.showMensagemAviso(this.Page, "Aviso!", String.Format("Esta campanha permite somente uma compra pelo CPF/CNPJ {1} em {0}.", retornoValidacao.DataCompra, TxtCpfCnpjIdentificacao.Text));
                return;
            }

            participante = ImunneVacinas.Participante.ConsultarUnico(ImunneVacinas.Utils.LimparCpfCnpj(TxtCpfCnpjIdentificacao.Text));

            if (participante == null)
            {
                TxtCpfCadastro.Text = ImunneVacinas.Utils.FormatarCpfCnpj(TxtCpfCnpjIdentificacao.Text);
                PnlCadastro.Visible = true;
                PnlIdentificacao.Visible = false;
                return;
            }

            if (participante.Email == null || participante.Email == "" || participante.Telefone == null || participante.Telefone == "")
            {
                TxtCpfCadastro.Text = ImunneVacinas.Utils.FormatarCpfCnpj(TxtCpfCnpjIdentificacao.Text);
                TxtDataNascimento.Text = participante.DataNascimento.ToShortDateString();
                TxtNome.Text = participante.Nome;
                TxtTelefone.Text = participante.Telefone;
                txtEmail.Text = participante.Email;
                PnlCadastro.Visible = true;
                PnlIdentificacao.Visible = false;
                return;
            }

            urlPagina.AppendFormat("~/opcoes.aspx?campanha={0}", Request.QueryString["campanha"]);
            urlPagina.AppendFormat("&clinica={0}", Request.QueryString["clinica"]);
            urlPagina.AppendFormat("&participante={0}", ImunneVacinas.Criptografia.Criptografar(participante.Codigo.ToString()));
            Response.Redirect(urlPagina.ToString(), false);
            Context.ApplicationInstance.CompleteRequest();
        }

        if (PnlCadastro.Visible == true)
        {
            participante = ImunneVacinas.Participante.ConsultarUnico(ImunneVacinas.Utils.LimparCpfCnpj(TxtCpfCnpjIdentificacao.Text));
            // Validação do CPF/CNPJ
            if (!ImunneVacinas.Utils.ValidarCpfCnpj(ImunneVacinas.Utils.LimparCpfCnpj(TxtCpfCadastro.Text)))
            {
                ImunneVacinas.Alerta.showMensagemErro(this.Page, "Aviso", "CPF informado é inválido.");
                return;
            }

            // 17/12/2019 - VALIDAÇÃO DE ÚNICA COMPRA POR CPF/CNPJ
            ValidacaoCompra retornoValidacao = ValidarCompraUnica(codigoCampanha, ImunneVacinas.Utils.LimparCpfCnpj(TxtCpfCadastro.Text));
            if (retornoValidacao.Validacao)
            {
                ImunneVacinas.Alerta.showMensagemAviso(this.Page, "Aviso!", String.Format("Esta campanha permite somente uma compra pelo CPF/CNPJ {1} em {0}.", retornoValidacao.DataCompra, TxtCpfCadastro.Text));
                return;
            }

            // Validação de data de nascimento
            if (String.IsNullOrEmpty(TxtDataNascimento.Text) || ImunneVacinas.Utils.VerificarDataValida(TxtDataNascimento.Text) == false)
            {
                ImunneVacinas.Alerta.showMensagemErro(this.Page, "Aviso", "Informe uma data de nascimento válida.");
                return;
            }

            // Validação de nome
            if (String.IsNullOrEmpty(TxtNome.Text))
            {
                ImunneVacinas.Alerta.showMensagemErro(this.Page, "Aviso", "Informe seu nome completo.");
                return;
            }

            if (!isValidEmail(txtEmail.Text))
            {
                ImunneVacinas.Alerta.showMensagemErro(this.Page, "Aviso", "Informe o email ou verifique se o formato está correto!");
            }

            // Validação de Telefone;
            if (String.IsNullOrEmpty(TxtTelefone.Text))
            {
                ImunneVacinas.Alerta.showMensagemErro(this.Page, "Aviso", "Informe seu telefone.");
                return;
            }

            // Cadastro de novo participante
            int resultado;

            if (participante == null)
            {
                participante = new ImunneVacinas.Participante()
                {
                    Nome = TxtNome.Text,
                    Cpf = ImunneVacinas.Utils.LimparCpfCnpj(TxtCpfCnpjIdentificacao.Text),
                    DataNascimento = Convert.ToDateTime(TxtDataNascimento.Text),
                    Email = txtEmail.Text,
                    DataCriacao = DateTime.Now,
                    Telefone = TxtTelefone.Text
                };

                resultado = participante.Incluir();
                if (resultado <= 0)
                {
                    ImunneVacinas.Alerta.showMensagemErro(this.Page, "Aviso", "Infelizmente não foi possível concluir seu cadastro, tente novamente ou informe nosso suporte.");
                    return;
                }
            }
            else
            {
                participante.Email = txtEmail.Text;
                participante.Telefone = TxtTelefone.Text;
                participante.DataAlteracao = DateTime.Now;

                resultado = participante.Alterar();
                if (resultado <= 0)
                {
                    Erro erro = new Erro();
                    erro.addArquivo(new InvalidOperationException($"Erro ao atualizar o participante {participante}"));

                    ImunneVacinas.Alerta.showMensagemErro(this.Page, "Aviso", "Infelizmente não foi possível concluir seu cadastro, tente novamente ou informe nosso suporte.");
                    return;
                }

                resultado = participante.Codigo;
            }

            // Redirecionamento
            urlPagina.AppendFormat("~/produtos.aspx?campanha={0}", Request.QueryString["campanha"]);
            urlPagina.AppendFormat("&clinica={0}", Request.QueryString["clinica"]);
            urlPagina.AppendFormat("&participante={0}", ImunneVacinas.Criptografia.Criptografar(resultado.ToString()));
            Response.Redirect(urlPagina.ToString(), false);
            Context.ApplicationInstance.CompleteRequest();
        }
    }

    private bool isValidEmail(string email)
    {
        if (String.IsNullOrEmpty(email)) return false;

        if (!Regex.IsMatch(email, "^[\\w\\.-]+@[a-zA-Z\\d\\.-]+\\.[a-zA-Z]{2,}$")) return false;

        return true;

    }


    /// <summary>
    /// Rotina que valida se a campanha permite somente uma compra por CPF/CNPJ
    /// </summary>
    /// <param name="codigoCampanha">VALUE: Código de identificação da campanha</param>
    /// <param name="cpfCnpj">VALUE: CPF/CNPJ a ser consultado</param>
    /// <returns></returns>
    private ValidacaoCompra ValidarCompraUnica(int codigoCampanha, string cpfCnpj)
    {
        ValidacaoCompra retorno = new ValidacaoCompra();

        string dataCompra = String.Empty;
        bool flagValidacao = false;

        ImunneVacinas.Campanha campanhaSelecionada = ImunneVacinas.Campanha.ConsultarUnico(codigoCampanha);
        ImunneVacinas.Participante participanteSelecionado = ImunneVacinas.Participante.ConsultarUnico(cpfCnpj);

        if (participanteSelecionado != null)
        {
            if (campanhaSelecionada.UnicaVenda == 1)
            {
                ImunneVacinas.PesquisaCompra pesquisaCompra = new ImunneVacinas.PesquisaCompra()
                {
                    Campanha = codigoCampanha,
                    Participante = participanteSelecionado.Codigo,
                    Situacao = ImunneVacinas.Compra.SituacaoCompra.Concluida,
                    Valor = true
                };

                List<ImunneVacinas.Compra> listaCompras = ImunneVacinas.Compra.ConsultarComprar(pesquisaCompra);
                if (listaCompras != null && listaCompras.Count > 0)
                {
                    dataCompra = listaCompras[0].DataCompra.ToString("dd/MM/yyyy");
                    flagValidacao = true;
                }
            }
        }

        retorno.Validacao = flagValidacao;
        retorno.DataCompra = dataCompra;

        return retorno;
    }
}