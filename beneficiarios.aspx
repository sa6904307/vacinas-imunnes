﻿<%@ Page Title="" Language="C#" MasterPageFile="~/imunne.master" AutoEventWireup="true" CodeFile="beneficiarios.aspx.cs" Inherits="imunne_beneficiarios" MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js" type="text/javascript"></script>
    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.5/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" />
    <%--    <script src='https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.5/jquery-ui.min.js' type='text/javascript'></script>--%>
    <link href="<%= Page.ResolveUrl("vendors/datepicker/css/datepicker3.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%= Page.ResolveUrl("vendors/daterangepicker/css/daterangepicker-bs3.css") %>" rel="stylesheet"
        type="text/css" />
    <script src="<%= Page.ResolveUrl("vendors/daterangepicker/js/daterangepicker.js") %>" type="text/javascript"></script>
    <script src="<%= Page.ResolveUrl("vendors/datepicker/js/bootstrap-datepicker.js") %>" type="text/javascript"></script>
    <script src="<%= Page.ResolveUrl("vendors/datepicker/locales/bootstrap-datepicker.pt-BR.js") %>" type="text/javascript"></script>

    <style type="text/css">
        .btn-aviso {
            padding: 12px 0px !important;
            font-size: 22px !important;
            font-weight: bold !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section class="subheader">
        <div class="container">
            <div class="row">
                <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                    <i class="fa fa-users"></i>&nbsp;Beneficiários da Campanha
                </div>
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 text-right">
                    <a href="javascript:history.back()" title="Voltar à página anterior">
                        <i class="fa fa-chevron-left"></i>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <div class="container">
        <div class="row">
            <asp:UpdatePanel ID="UpdProdutos" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <script type="text/javascript">
                        Sys.Application.add_load(SetDatePicker);
                        function SetDatePicker() {
                            $('.datepicker').datepicker({
                                todayHighlight: true,
                                language: "pt-BR"
                            });
                        }
                    </script>
                    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
                        <div class="caixa-branca">
                            <div class="step-header">
                                DADOS DO ADQUIRENTE
                            </div>
                            <div class="box-inside">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                    <label>Nome</label>
                                    <asp:Label ID="LblNome" runat="server" CssClass="label-identificacao"></asp:Label>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 form-group">
                                    <label>CPF</label>
                                    <asp:Label ID="LblCPF" runat="server" CssClass="label-identificacao"></asp:Label>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 form-group">
                                    <label>Nascimento</label>
                                    <asp:Label ID="LblNascimento" runat="server" CssClass="label-identificacao"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="caixa-branca">
                            <div class="step-header">
                                INFORMAÇÕES DA CAMPANHA
                            </div>
                            <div class="box-inside">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                    <label>CAMPANHA</label>
                                    <asp:Label ID="LblCampanha" runat="server" CssClass="label-identificacao"></asp:Label>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                    <label>CLÍNICA</label>
                                    <asp:Label ID="LblClinica" runat="server" CssClass="label-identificacao"></asp:Label>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                    <label>EMPRESA</label>
                                    <asp:Label ID="LblEmpresa" runat="server" CssClass="label-identificacao"></asp:Label>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                    <label>VALIDADE</label>
                                    <asp:Label ID="LblValidade" runat="server" CssClass="label-identificacao"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                        <div class="caixa-branca">
                            <div class="step-header">
                                DADOS DOS BENEFICIÁRIOS
                            </div>
                            <div class="box-inside">
                                <asp:Panel ID="PnlTitular" runat="server">
                                    <div class="alert alert-warning alert-dismissible text-center">
                                        <h2 style="color: #fff !important;"><i class="icon fa fa-warning"></i>Aviso importante!</h2>
                                    </div>
                                    <div class="alert alert-warning alert-dismissible" style="font-size: 18px; text-align: center;">
                                        As compras para esta campanha incluem o participante titular como um dos beneficiários, 
                                        sem a necessidade de cadastrá-lo abaixo.
                                        <br />
                                        <br />
                                        Será gerado um voucher e respectivamente 
                                        uma cobrança para o mesmo no valor final da compra.
                                        <br />
                                        <br />
                                        Caso opte por não participar como beneficiário, 
                                        clicar no botão abaixo <b>"NÃO INCLUIR TITULAR NA COMPRA"</b>.
                                        <br />
                                    </div>
                                </asp:Panel>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left">
                                    <asp:LinkButton ID="LnkTitular" runat="server" CssClass="btn btn-block btn-success" OnClick="LnkTitular_Click" Text="NÃO INCLUIR O TITULAR NA COMPRA" CausesValidation="false">
                                    </asp:LinkButton>
                                    <asp:CheckBox ID="ChkIncluirTitular" runat="server" CssClass="hidden" Text="NÃO INCLUIR O TITULAR NA COMPRA" Checked="true" />
                                </div>
                                <div class="dv-spc-10"></div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left">
                                    <p>Por favor, informe os dados dos beneficários que irão receber as vacinas adquiridas.</p>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="box-grids" style="max-height: 450px; overflow-y: auto;">
                                        <asp:Repeater ID="RptPrincipal" runat="server" OnItemDataBound="RptPrincipal_ItemDataBound">
                                            <ItemTemplate>
                                                <div class="dv-spc-10"></div>
                                                <div class="xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                                    <asp:TextBox ID="TxtNome" runat="server" CssClass="form-control text-uppercase" placeHolder="* Nome do beneficiário"></asp:TextBox>
                                                </div>
                                                <div class="xs-12 col-sm-12 col-md-4 col-lg-4 form-group">
                                                    <asp:TextBox ID="TxtCpf" runat="server" CssClass="form-control" placeHolder="CPF" OnKeyPress="mascara(this,cpfCnpj)" MaxLength="18"></asp:TextBox>
                                                </div>
                                                <div class="xs-12 col-sm-12 col-md-4 col-lg-4 form-group">
                                                    <asp:TextBox ID="TxtNascimento" runat="server" CssClass="form-control text-center" data-date-format="dd/mm/yyyy" OnKeyPress="mascara(this, mdata)" MaxLength="10" placeHolder="* Nascimento"></asp:TextBox>
                                                </div>
                                                <div class="xs-12 col-sm-12 col-md-4 col-lg-4 form-group">
                                                    <asp:DropDownList ID="DdlParentesco" runat="server" CssClass="form-control" DataValueField="ValueMember" DataTextField="DisplayMember"></asp:DropDownList>
                                                </div>
                                                <div class="dv-spc-10"></div>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>
                                <div class="dv-spc-10"></div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left">
                                    <p>Escolha forma de pagamento:</p>
                                    <asp:RadioButton id="RdCartao" Text="cartao" Checked="True" GroupName="tipoPagamento" runat="server" />
                                    <asp:RadioButton id="RdPix" Text="pix" GroupName="tipoPagamento" runat="server" />
                                </div>
                                <div class="dv-spc-10"></div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                                    <asp:LinkButton ID="LnkContinuar" runat="server" CssClass="btn btn-block btn-primary" OnClick="LnkContinuar_Click" CausesValidation="false">
                            <i class="fa fa-chevron-right"></i>&nbsp;
                            Continuar
                                    </asp:LinkButton>
                                </div>
                            </div>
                        </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="LnkTitular" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
