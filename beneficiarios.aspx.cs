﻿using System;
using System.Data;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class imunne_beneficiarios : System.Web.UI.Page
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            IniciarPagina();
    }

    /// <summary>
    /// 
    /// </summary>
    private void IniciarPagina()
    {
        Page.Title = "S&A Imunizações | Identificação de Beneficiários";

        try
        {
            if (!String.IsNullOrEmpty(Request.QueryString["campanha"]))
            {
                int codigoCampanha = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["campanha"]));
                int codigoParticipante = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["participante"]));
                int codigoClinica = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["clinica"]));

                ImunneVacinas.Participante participanteSelecionado = ImunneVacinas.Participante.ConsultarUnico(codigoParticipante);
                ImunneVacinas.Campanha campanhaSelecionada = ImunneVacinas.Campanha.ConsultarUnico(codigoCampanha);
                ImunneVacinas.Clinica clinicaSelecionada = ImunneVacinas.Clinica.ConsultarUnico(codigoClinica);

                LblNascimento.Text = participanteSelecionado.DataNascimento.ToString("dd/MM/yyyy");
                LblNome.Text = participanteSelecionado.Nome.ToUpper();
                LblCPF.Text = ImunneVacinas.Utils.FormatarCpfCnpj(participanteSelecionado.Cpf);

                LblCampanha.Text = campanhaSelecionada.Identificacao.ToUpper() + " | " + campanhaSelecionada.Nome.ToUpper();
                LblValidade.Text = campanhaSelecionada.DataLimite.ToString("dd/MM/yyyy");
                LblClinica.Text = clinicaSelecionada.NomeFantasia.ToUpper() + "<br />"
                                + clinicaSelecionada.Endereco.ToUpper() + ", "
                                + clinicaSelecionada.Numero + " - "
                                + clinicaSelecionada.IdCidade.Nome.ToUpper() + "/"
                                + clinicaSelecionada.IdUf.Sigla;
                LblEmpresa.Text = campanhaSelecionada.IdEmpresa.NomeFantasia.ToUpper();

                DataTable DtTemp = new DataTable();
                DtTemp.Columns.Add("temporario");
                DataRow DrTemp;

                int qtde = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["qtde"]));

                PnlTitular.Visible = false;
                ChkIncluirTitular.Checked = false;
                LnkTitular.Visible = false;

                if (campanhaSelecionada.IncluirTitular > 0)
                {
                    PnlTitular.Visible = true;
                    ChkIncluirTitular.Checked = true;
                    LnkTitular.Visible = true;
                }

                if (ChkIncluirTitular.Checked == true)
                {
                    LnkTitular.Text = "<i class='fa fa-minus'></i>&nbsp;NÃO INCLUIR O TITULAR NA COMPRA";
                    LnkTitular.CssClass = "btn btn-block btn-warning btn-aviso";
                }

                for (int i = 1; i <= qtde; i++)
                {
                    DrTemp = DtTemp.NewRow();
                    DrTemp[0] = i;
                    DtTemp.Rows.Add(DrTemp);
                }

                RptPrincipal.DataSource = DtTemp;
                RptPrincipal.DataBind();

                if (campanhaSelecionada.IncluirTitular == 1)
                {
                    ImunneVacinas.Alerta.showMensagemAviso(this.Page, "Aviso Importante!", "As compras para esta campanha incluem o participante titular como um dos beneficiários, sem a necessidade de cadastrá-lo abaixo. Será gerado um voucher e respectivamente uma cobrança para o mesmo no valor final da compra. Caso opte por não participar como beneficiário, clicar no botão NÃO INCLUIR O TITULAR NA COMPRA.");
                }
            }
            else
            {
                Response.Redirect("~/", false);
                Context.ApplicationInstance.CompleteRequest();
            }
        }
        catch (Exception ex)
        {
            Response.Redirect("~/", false);
            Context.ApplicationInstance.CompleteRequest();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void RptPrincipal_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        DropDownList DdlParentesco = (DropDownList)e.Item.FindControl("DdlParentesco");

        DdlParentesco.DataSource = ImunneVacinas.GrauParentesco.ConsultarComboBox(true, "PARENTESCO");
        DdlParentesco.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void LnkContinuar_Click(object sender, EventArgs e)
    {
        try
        {
            int totalBeneficiarios = 0;
            int qtde = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["qtde"]));
            int qtdeCompras = 0;
            StringBuilder urlPagina = new StringBuilder();
            StringBuilder parametros = new StringBuilder();
            StringBuilder codigoParticipantes = new StringBuilder();

            int codigoParticipante = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["participante"]));
            int codigoCampanha = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["campanha"]));
            int codigoClinica = Convert.ToInt32(ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["clinica"]));

            string tipoPagamento ;

            #region VERIFICANDO CAMPOS NECESSÁRIOS

            ImunneVacinas.Participante participantePai = ImunneVacinas.Participante.ConsultarUnico(codigoParticipante);

            foreach (RepeaterItem item in RptPrincipal.Items)
            {
                TextBox TxtCpf = (TextBox)item.FindControl("TxtCpf");
                if (!String.IsNullOrEmpty(TxtCpf.Text))
                {
                    if (ImunneVacinas.Utils.LimparCpfCnpj(TxtCpf.Text) == participantePai.Cpf)
                    {
                        ImunneVacinas.Alerta.showMensagemAviso(this.Page, "Aviso!", "Você está tentando se cadastrar como um de seus beneficários. Reveja as informações digitadas e em caso de dúvidas, acione o suporte para maiores informações.");
                        return;
                    }
                }
            }

            foreach (RepeaterItem item in RptPrincipal.Items)
            {
                TextBox TxtNome = (TextBox)item.FindControl("TxtNome");

                if (String.IsNullOrEmpty(TxtNome.Text))
                {
                    totalBeneficiarios += 1;
                    break;
                }
            }

            foreach (RepeaterItem item in RptPrincipal.Items)
            {
                TextBox TxtNascimento = (TextBox)item.FindControl("TxtNascimento");

                if (String.IsNullOrEmpty(TxtNascimento.Text) || ImunneVacinas.Utils.VerificarDataValida(TxtNascimento.Text) == false)
                {
                    totalBeneficiarios += 1;
                    break;
                }
            }

            if (totalBeneficiarios > 0)
            {
                ImunneVacinas.Alerta.showMensagemErro(this.Page, "Aviso", "Você precisa informar todos os campos obrigatórios dos beneficiários.");
                return;
            }

            #endregion

            #region CRIANDO BENEFICIÁRIOS

            int resultado = 0;
            foreach (RepeaterItem item in RptPrincipal.Items)
            {
                qtdeCompras += 1;
                TextBox TxtCpf = (TextBox)item.FindControl("TxtCpf");
                TextBox TxtNome = (TextBox)item.FindControl("TxtNome");
                TextBox TxtNascimento = (TextBox)item.FindControl("TxtNascimento");
                DropDownList DdlParentesco = (DropDownList)item.FindControl("DdlParentesco");

                ImunneVacinas.Participante novoRegistro = ImunneVacinas.Participante.ConsultarUnico(TxtNome.Text);
                if (novoRegistro == null)
                {
                    novoRegistro = ImunneVacinas.Participante.ConsultarUnico(TxtNome.Text);
                    if (novoRegistro == null)
                    {
                        novoRegistro = new ImunneVacinas.Participante();
                        novoRegistro.Nome = TxtNome.Text;
                        if (ImunneVacinas.Utils.VerificarDataValida(TxtNascimento.Text))
                            novoRegistro.DataNascimento = Convert.ToDateTime(TxtNascimento.Text);
                        novoRegistro.ParticipantePai = codigoParticipante;
                        novoRegistro.Parentesco.Codigo = Convert.ToInt32(DdlParentesco.SelectedValue);
                        novoRegistro.DataCriacao = DateTime.Now;
                        resultado = novoRegistro.Incluir();
                    }
                    else
                    {
                        novoRegistro.Nome = TxtNome.Text;
                        if (ImunneVacinas.Utils.VerificarDataValida(TxtNascimento.Text))
                            novoRegistro.DataNascimento = Convert.ToDateTime(TxtNascimento.Text);
                        novoRegistro.ParticipantePai = codigoParticipante;
                        novoRegistro.Parentesco.Codigo = Convert.ToInt32(DdlParentesco.SelectedValue);
                        novoRegistro.DataAlteracao = DateTime.Now;
                        resultado = novoRegistro.Codigo;
                    }
                }
                else
                {
                    novoRegistro.Nome = TxtNome.Text;
                    if (ImunneVacinas.Utils.VerificarDataValida(TxtNascimento.Text))
                        novoRegistro.DataNascimento = Convert.ToDateTime(TxtNascimento.Text);
                    novoRegistro.ParticipantePai = codigoParticipante;
                    novoRegistro.Parentesco.Codigo = Convert.ToInt32(DdlParentesco.SelectedValue);
                    novoRegistro.DataAlteracao = DateTime.Now;
                    resultado = novoRegistro.Codigo;
                }

                if (codigoParticipantes.Length > 0)
                    codigoParticipantes.Append("|");
                codigoParticipantes.AppendFormat("{0}", resultado);
            }

            #endregion

            #region INSERINDO O TITULAR NA COMPRA

            if (PnlTitular.Visible == true && ChkIncluirTitular.Checked == true)
            {
                qtdeCompras += 1;

                if (codigoParticipantes.Length > 0)
                    codigoParticipantes.Append("|");
                codigoParticipantes.AppendFormat("{0}", codigoParticipante);
            }

            #endregion

            #region GERANDO COMPRA

            ImunneVacinas.Compra novaCompra = new ImunneVacinas.Compra();
            novaCompra.IdCampanha.Codigo = codigoCampanha;
            novaCompra.IdClinica.Codigo = codigoClinica;
            novaCompra.IdParticipante.Codigo = codigoParticipante;
            novaCompra.DataCompra = DateTime.Now;
            novaCompra.Parcelas = 1;
            novaCompra.Valor = 0;
            int codigoCompra = novaCompra.Incluir();

            decimal totalValorCompra = 0;
            string[] codigosItens = ImunneVacinas.Criptografia.Descriptografar(Request.QueryString["itens"]).Split('|');

            foreach (string item in codigosItens)
            {
                ImunneVacinas.ProdutoCampanha produtoSelecionado = ImunneVacinas.ProdutoCampanha.ConsultarUnico(Convert.ToInt32(item));
                ImunneVacinas.ItemCompra novoItem = new ImunneVacinas.ItemCompra();
                novoItem.IdCompra.Codigo = codigoCompra;
                novoItem.IdVacina.Codigo = produtoSelecionado.IdVacina.Codigo;
                novoItem.DataCriacao = novaCompra.DataCompra;
                novoItem.Quantidade = qtdeCompras;
                novoItem.Valor = qtdeCompras * produtoSelecionado.Preco;
                novoItem.Observacoes = String.Format("COMPRADAS {0} UNIDADE(S) DE {1}.", qtde, produtoSelecionado.IdVacina.Nome.ToUpper());

                novoItem.Incluir();

                totalValorCompra += novoItem.Valor;
            }

            novaCompra = ImunneVacinas.Compra.ConsultarUnico(codigoCompra);
            novaCompra.Valor = totalValorCompra;
            novaCompra.Alterar();

            #endregion

            #region VERIFICACAO TIPO DE PAGAMENTO
            // Criar cobrança pix
            
            if(RdPix.Checked == true){
                tipoPagamento = "pix";
            } else {
                tipoPagamento = "cartao";
            }
            #endregion
            
            #region DIRECIONANDO PARA TELA DE PGTOS

            urlPagina.AppendFormat("~/pagamentos.aspx?campanha={0}", Request.QueryString["campanha"]);
            urlPagina.AppendFormat("&clinica={0}", Request.QueryString["clinica"]);
            urlPagina.AppendFormat("&participante={0}", Request.QueryString["participante"]);
            urlPagina.AppendFormat("&itens={0}", Request.QueryString["itens"]);
            //urlPagina.AppendFormat("&qtde={0}", Request.QueryString["qtde"]);
            urlPagina.AppendFormat("&qtde={0}", ImunneVacinas.Criptografia.Criptografar(qtdeCompras.ToString()));
            urlPagina.AppendFormat("&compra={0}", ImunneVacinas.Criptografia.Criptografar(codigoCompra.ToString()));
            urlPagina.AppendFormat("&membros={0}", ImunneVacinas.Criptografia.Criptografar(codigoParticipantes.ToString()));
            urlPagina.AppendFormat("&tipoPagamento={0}", ImunneVacinas.Criptografia.Criptografar(tipoPagamento));
            Response.Redirect(urlPagina.ToString(), false);
            Context.ApplicationInstance.CompleteRequest();

            #endregion
        }
        catch (Exception ex)
        {
            ImunneVacinas.Alerta.showMensagemErro(this.Page, "Aviso", "Não foi possível seguir para o próximo passo da compra. Verifique os dados e tente novamente ou, se persistir o erro, solicite suporte.");
            ImunneVacinas.Erro erro = new ImunneVacinas.Erro();
            erro.addArquivo(ex);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void LnkTitular_Click(object sender, EventArgs e)
    {
        if (LnkTitular.Text.ToUpper().IndexOf("NÃO INCLUIR") <= 0)
        {
            ChkIncluirTitular.Checked = true;
            LnkTitular.Text = "<i class='fa fa-minus'></i>&nbsp;NÃO INCLUIR O TITULAR NA COMPRA";
            LnkTitular.CssClass = "btn btn-block btn-warning btn-aviso";
        }
        else
        {
            ChkIncluirTitular.Checked = false;
            LnkTitular.Text = "<i class='fa fa-plus'></i>&nbsp;INCLUIR O TITULAR NA COMPRA";
            LnkTitular.CssClass = "btn btn-block btn-success btn-aviso";
        }
    }
}