﻿<%@ Page Title="" Language="C#" MasterPageFile="~/imunne.master" AutoEventWireup="true" CodeFile="opcoes.aspx.cs" Inherits="opcoes" MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section class="subheader">
        <div class="container">
            <div class="row">
                <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                    <i class="fa fa-check-square-o"></i>&nbsp;Escolha a opção desejada
                </div>
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 text-right">
                    <a href="javascript:history.back()" title="Voltar à página anterior">
                        <i class="fa fa-chevron-left"></i>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <div class="container">
        <div class="row">
            <asp:UpdatePanel ID="UpdMain" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
                        <div class="caixa-branca">
                            <div class="step-header">
                                DADOS DO ADQUIRENTE
                            </div>
                            <div class="box-inside">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                    <label>Nome</label>
                                    <asp:Label ID="LblNome" runat="server" CssClass="label-identificacao"></asp:Label>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 form-group">
                                    <label>CPF</label>
                                    <asp:Label ID="LblCPF" runat="server" CssClass="label-identificacao"></asp:Label>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 form-group">
                                    <label>Nascimento</label>
                                    <asp:Label ID="LblNascimento" runat="server" CssClass="label-identificacao"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="caixa-branca">
                            <div class="step-header">
                                INFORMAÇÕES DA CAMPANHA
                            </div>
                            <div class="box-inside">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                    <label>CAMPANHA</label>
                                    <asp:Label ID="LblCampanha" runat="server" CssClass="label-identificacao"></asp:Label>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                    <label>CLÍNICA</label>
                                    <asp:Label ID="LblClinica" runat="server" CssClass="label-identificacao"></asp:Label>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                    <label>EMPRESA</label>
                                    <asp:Label ID="LblEmpresa" runat="server" CssClass="label-identificacao"></asp:Label>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                    <label>VALIDADE</label>
                                    <asp:Label ID="LblValidade" runat="server" CssClass="label-identificacao"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                        <div class="caixa-branca hidden">
                            <div class="step-header">
                                MEU VALE
                            </div>
                            <div class="box-inside">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left">
                                    <p>Caso não tenha intenção de adquirir nenhum produto adicional, clique abaixo para impressão de seu vale.</p>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                                    <asp:HyperLink ID="LnkMeuVale" runat="server" Target="_blank" CssClass="btn btn-block btn-primary">
                            <i class="fa fa-chevron-right"></i>&nbsp;
                            Gerar meu Vale
                                    </asp:HyperLink>
                                </div>
                            </div>
                        </div>
                        <div class="caixa-branca">
                            <div class="step-header">
                                COMPRAR NOVOS VALES
                            </div>
                            <div class="box-inside">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left">
                                    <p>Selecione a opção abaixo para realizar compras para outros beneficiários.</p>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                                    <asp:LinkButton ID="LnkComprarNovos" runat="server" CssClass="btn btn-block btn-primary" OnClick="LnkComprarNovos_Click" CausesValidation="false">
                            <i class="fa fa-chevron-right"></i>&nbsp;
                            Novos Vales
                                    </asp:LinkButton>
                                </div>
                            </div>
                        </div>
                        <div class="caixa-branca">
                            <div class="step-header">
                                COMPRAS REALIZADAS
                            </div>
                            <div class="box-inside">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left">
                                    <p>Visualize os vales de suas compras já realizadas para esta campanha e clínica.</p>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="box-grids" style="width: 100% !important; height: 450px; overflow-y: auto; overflow-x: hidden;">
                                        <div class="dv-grid-f-header" style="width: 100% !important;">
                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 text-center" style="display: inline-block; float: none;">
                                                ID
                                            </div>
                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 text-center" style="display: inline-block; float: none;">
                                                DATA
                                            </div>
                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 text-center" style="display: inline-block; float: none;">
                                                VALOR
                                            </div>
                                        </div>
                                        <div class="dv-grid-f" style="width: 100% !important; overflow-x: hidden;">
                                            <asp:Repeater ID="RptPrincipal" runat="server" OnItemDataBound="RptPrincipal_ItemDataBound">
                                                <ItemTemplate>
                                                    <div id="dvItem" runat="server" style="width: 100% !important;">
                                                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 text-center" style="display: inline-block; float: none;">
                                                            <div class="truncate">
                                                                <asp:HyperLink ID="LnkCompra" runat="server">
                                                                    <asp:Label ID="LblID" runat="server"></asp:Label>
                                                                </asp:HyperLink>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 text-center" style="display: inline-block; float: none;">
                                                            <div class="truncate">
                                                                <asp:Label ID="LblData" runat="server"></asp:Label>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 text-center" style="display: inline-block; float: none;">
                                                            <div class="truncate">
                                                                <asp:Label ID="LblValor" runat="server"></asp:Label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>

